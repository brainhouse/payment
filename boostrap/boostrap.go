package boostrap

import (
	"bytes"
	"fmt"
	"funzone_pay/app/service"
	"funzone_pay/dao"
	"funzone_pay/utils"
	"github.com/spf13/viper"
	"github.com/wonderivan/logger"
	"os"
	"path"
)

var Conf AppConf

type AppConf struct {
	Host    string
	Port    int
	LogPath string
}

func init() {
	InitConf()
	InitLog()
	dao.RedisInit()
	service.ServiceInit()
}

func InitConf() {
	paymentConfig := os.Getenv("PAYMENT_CONFIG")
	filename := "config_test"
	key := os.Getenv("KEY")
	if key == "" {
		key = "1234xxx.test.com"
	}
	env := os.Getenv("PAYMENT_ENV")
	if env == "" {
		name := os.Getenv("CONFIG_NAME")
		if name != "" {
			filename = name
		} else {
			filename = "config"
		}
	}

	fmt.Printf("filename --->%v/%v\n", paymentConfig, filename)
	viper.SetConfigName(filename)
	viper.AddConfigPath(paymentConfig)
	viper.SetConfigType("toml")
	//err := viper.ReadInConfig()

	data, err := decryptConf(path.Join(paymentConfig, fmt.Sprintf("%v.toml", filename)), key)
	if err != nil {
		fmt.Println("dec configure error: ", err)
		panic(env)
	}

	err = viper.ReadConfig(bytes.NewReader(data))
	if err != nil {
		fmt.Println("load configure error: ", err)
		panic(env)
	}
	Conf.Host = ""
	Conf.Port = 6063
}

func decryptConf(fname string, key string) ([]byte, error) {
	data, err := os.ReadFile(fname)
	if err != nil {
		return nil, err
	}

	if len(data) < 1 {
		return nil, fmt.Errorf("empty file data")
	}

	if key == "" {
		return data, nil
	}

	return utils.AesDecrypt(data, []byte(key))
}

func InitLog() {
	// 通过配置参数直接配置
	err := logger.SetLogger(`{"Console": {"level": "DEBG"}}`)
	// 通过配置文件配置
	paymentConfig := os.Getenv("PAYMENT_CONFIG")
	fmt.Println(paymentConfig)
	conf := fmt.Sprintf("%s/log.json", paymentConfig)
	err = logger.SetLogger(conf)
	if err != nil {
		fmt.Println(err)
	}
}
