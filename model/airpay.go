package model

type AirPayCollect struct {
	MERCID                   int     `json:"MERCID"`
	APTRANSACTIONID          string  `json:"APTRANSACTIONID"`
	AMOUNT                   int     `json:"AMOUNT"`
	TRANSACTIONSTATUS        int     `json:"TRANSACTIONSTATUS"`
	MESSAGE                  string  `json:"MESSAGE"`
	ApSecureHash             string  `json:"ap_SecureHash"`
	TRANSACTIONID            string  `json:"TRANSACTIONID"`
	CUSTOMVAR                string  `json:"CUSTOMVAR"`
	CHMOD                    string  `json:"CHMOD"`
	BANKNAME                 string  `json:"BANKNAME"`
	CARDISSUER               string  `json:"CARDISSUER"`
	CUSTOMER                 string  `json:"CUSTOMER"`
	CUSTOMEREMAIL            string  `json:"CUSTOMEREMAIL"`
	CUSTOMERPHONE            int64   `json:"CUSTOMERPHONE"`
	CURRENCYCODE             int     `json:"CURRENCYCODE"`
	RISK                     string  `json:"RISK"`
	TRANSACTIONTYPE          int     `json:"TRANSACTIONTYPE"`
	TRANSACTIONPAYMENTSTATUS string  `json:"TRANSACTIONPAYMENTSTATUS"`
	CARDNUMBER               string  `json:"CARD_NUMBER"`
	CARDTYPE                 string  `json:"CARDTYPE"`
	EMITENURE                string  `json:"EMITENURE"`
	TRANSACTIONTIME          string  `json:"TRANSACTIONTIME"`
	MERCHANTNAME             string  `json:"MERCHANT_NAME"`
	WALLETBALANCE            int     `json:"WALLETBALANCE"`
	SURCHARGE                int     `json:"SURCHARGE"`
	BILLEDAMOUNT             int     `json:"BILLEDAMOUNT"`
	RRN                      string  `json:"RRN"`
	TOKEN                    string  `json:"TOKEN"`
	CARDUNIQUECODE           string  `json:"CARDUNIQUECODE"`
	REASON                   string  `json:"REASON"`
	CARDCOUNTRY              string  `json:"CARDCOUNTRY"`
	CONVERSIONRATE           float64 `json:"CONVERSIONRATE"`
}
