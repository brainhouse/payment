package model

/**
`id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
 `uid` bigint(20) NOT NULL DEFAULT '0' COMMENT '用户uid',
 `balance` bigint(20) NOT NULL DEFAULT '0' COMMENT '用户余额',
 `created` bigint(20) NOT NULL DEFAULT '0' COMMENT '创建时间',
 `updated` bigint(20) NOT NULL DEFAULT '0' COMMENT '更新时间',
*/
type Account struct {
	Id               int64   `xorm:"id"`
	Uid              int64   `xorm:"uid"`
	Balance          float64 `xorm:"balance"`
	UnsettledBalance float64 `xorm:"unsettled_balance"`
	Created          int64   `xorm:"created"`
	Updated          int64   `xorm:"updated"`
}
