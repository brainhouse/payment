package model

type GlobPayNotify struct {
	Code           string `json:"code"`
	MchId          string `json:"mchId"`
	MchOrderNo     string `json:"mchOrderNo"`
	ProductId      string `json:"productId"`
	OrderAmount    int64  `json:"orderAmount"`
	PayOrderId     string `json:"payOrderId"`
	PaySuccessTime string `json:"paySuccessTime"`
	Message        string `json:"message"`
	//Extra          string `json:"extra"`
	//Sign           string `json:"sign"`
}
