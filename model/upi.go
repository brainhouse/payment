package model

type UPIInfo struct {
	UserName string `json:"user_name"`
	Status   bool   `json:"status"`
}
