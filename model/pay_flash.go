package model

type PayFlashNotify struct {
	TransactionId   string `json:"transaction_id"`
	PaymentMode     string `json:"payment_mode"`     // "credit card","debit card", "netbanking"
	PaymentChannel  string `json:"payment_channel"`  // Visa", "HDFCBank", "Paytm",
	PaymentDatetime string `json:"payment_datetime"` // DD-MM-YYYY HH:MM:S
	ResponseCode    string `json:"response_code"`
	ResponseMessage string `json:"response_message"`
	ErrorDesc       string `json:"error_desc"`
	OrderId         string `json:"order_id"`
	Amount          string `json:"amount"`
	//Currency          string `json:"currency"`
	//Description       string `json:"description"`
	//Name              string `json:"name"`
	//Email             string `json:"email"`
	//Phone             string `json:"phone"`
	//AddressLine1      string `json:"address_line_1"`
	//AddressLine2      string `json:"address_line_2"`
	//City              string `json:"city"`
	//State             string `json:"state"`
	//Country           string `json:"country"`
	//ZipCode           string `json:"zip_code"`
	//UDF1              string `json:"udf_1"`
	//UDF2              string `json:"udf_2"`
	//UDF3              string `json:"udf_3"`
	//UDF4              string `json:"udf_4"`
	//UDF5              string `json:"udf_5"`
	//TdrAmount         string `json:"tdr_amount"`
	//TaxOnTdrAmount    string `json:"tax_on_tdr_amount"`
	//AmountOrig        string `json:"amount_orig"`
	//Cardmasked        string `json:"cardmasked"`
	//EmiTenure         string `json:"emi_tenure"`
	//EmiRateOfInterest string `json:"emi_rate_of_interest"`
	Hash string `json:"hash"`
}

type PayFlashOutNotify struct {
	Status                     string `json:"status"`
	ErrorMessage               string `json:"error_message"`
	BankReferenceNumber        string `json:"bank_reference_number"`
	TransactionReferenceNumber string `json:"transaction_reference_number"`
	MerchantReferenceNumber    string `json:"merchant_reference_number"` // order id
	ApiKey                     string `json:"api_key"`
	TransferDate               string `json:"transfer_date"`
	TransferAmount             string `json:"transfer_amount"`
	Hash                       string `json:"hash"`
}
