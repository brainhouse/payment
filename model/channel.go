package model

type Channel struct {
	ID           int64     `xorm:"id"`             // 主键id
	PayAccountID string    `xorm:"pay_account_id"` // 支付渠道账号 如：payu001 payu002
	PayChannel   string    `xorm:"pay_channel"`    // 支付渠道 如：payu | razarpay | cashfree | paytm
	Info         string    `xorm:"info"`           // 账号 json 信息
	CountryCode  int       `xorm:"country_code"`   // 国家 code
	Status       int8      `xorm:"status"`         // 状态 0:有效 1:无效
}