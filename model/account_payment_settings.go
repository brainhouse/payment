package model

/**
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `uid` bigint(20) NOT NULL DEFAULT '0' COMMENT '用户uid',
  `pay_account_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '支付渠道账号 如：payu001 payu002',
  `pay_channel` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '支付渠道 如：payu | razarpay | cashfree | paytm',
`third_pay_channel` varchar(20) NOT NULL DEFAULT '' COMMENT '三级支付渠道 如：payu | razarpay | cashfree | paytm',
  `third_pay_account_id` varchar(50) NOT NULL DEFAULT '' COMMENT '三级支付渠道账号 如：payu001 payu002',
  `third_pay_min` decimal(20,4) DEFAULT '0.0000' COMMENT '三级最小支付额',
  `type` tinyint(2) NOT NULL COMMENT '类型 1充值 2提现',
  `created` bigint(20) NOT NULL COMMENT '创建时间',
  `updated` bigint(20) NOT NULL COMMENT '更新时间',
*/
type AccountPaymentSettings struct {
	Id                    int64      `xorm:"id"`
	Uid                   int64      `xorm:"uid"`
	PayAccountId          string     `xorm:"pay_account_id"`
	PayChannel            PayChannel `xorm:"pay_channel"`
	PayoutChannel         PayChannel `xorm:"payout_channel"`
	PayoutAccountId       string     `xorm:"payout_account_id"`
	CountryCode           int        `xorm:"country_code"`
	FeeRate               float64    `xorm:"fee_rate"`               // 充值费率
	PayoutFeeRate         float64    `xorm:"payout_fee_rate"`        // 提现费率
	ClientWithdrawRate    float64    `xorm:"client_withdraw_rate"`   // 客户提现费率
	ClientWithdrawFix     float64    `xorm:"client_withdraw_fix"`    // 客户提现固定额度
	UserWithdrawMin       float64    `xorm:"user_withdraw_min"`      // 用户最小提现额度
	UserChargeMin         float64    `xorm:"user_charge_min"`        // 用户最小充值额度
	UserChargeMax         float64    `xorm:"user_charge_max"`        // 用户最大充值额度
	UserLessFee           float64    `xorm:"user_less_fee"`          // 充值低于最小额度收取的手续费
	UserLessMin           float64    `xorm:"user_less_min"`          // 充值低于最小额度
	UserWithdrawLessFee   float64    `xorm:"user_withdraw_less_fee"` // 提现低于最小额度收取的手续费
	UserWithdrawLess      float64    `xorm:"user_withdraw_less"`     // 提现低于最小额度
	BackupPayoutChannel   PayChannel `xorm:"backup_payout_channel"`
	BackupPayoutAccountId string     `xorm:"backup_payout_account_id"`
	BackupWithdrawMin     float64    `xorm:"backup_withdraw_min"`
	ThirdPayChannel       PayChannel `xorm:"third_pay_channel"`
	ThirdPayAccountId     string     `xorm:"third_pay_account_id"`
	ThirdPayMin           float64    `xorm:"third_pay_min"`
	Type                  int        `xorm:"type"`
	Created               int64      `xorm:"created"`
	Updated               int64      `xorm:"updated"`
}
