package model

/**
`id` bigint(20) NOT NULL AUTO_INCREMENT,
  `uid` bigint(20) NOT NULL COMMENT 'userid',
  `user_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `withdraw_amount` bigint(20) NOT NULL DEFAULT '0' COMMENT '提现总数',
  `withdraw_count` bigint(20) NOT NULL DEFAULT '0',
  `withdraw_fee` bigint(20) NOT NULL DEFAULT '0' COMMENT '提现费',
  `pay_amount` bigint(20) NOT NULL DEFAULT '0' COMMENT '已支付金额',
  `receive_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '收账人姓名',
  `bank_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '银行名称',
  `account` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '账户id',
  `ifsc` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '转账码',
  `reason` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '描述',
  `admin_option_reason` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT 'admin 操作记录',
  `status` tinyint(4) NOT NULL COMMENT '状态 0 发起，1 通过成功，2 驳回',
  `created` bigint(20) DEFAULT NULL,
*/
type UserWithdrawFlow struct {
	Id                int64   `xorm:"id"`
	Uid               int64   `xorm:"uid"`
	UserName          string  `xorm:"user_name"`
	WithdrawAmount    float64 `xorm:"withdraw_amount"`
	WithdrawCount     int64   `xorm:"withdraw_count"`
	WithdrawFee       float64 `xorm:"withdraw_fee"`
	ReceiveEmail      string  `xorm:"receive_email"`
	ReceiveNumber     string  `xorm:"receive_number"`
	PayAmount         float64 `xorm:"pay_amount"`
	ReceiveName       string  `xorm:"receive_name"`
	BankName          string  `xorm:"bank_name"`
	Account           string  `xorm:"account"`
	IFSC              string  `xorm:"ifsc"`
	Reason            string  `xorm:"reason"`
	AdminOptionReason string  `xorm:"admin_option_reason"`
	Status            int     `xorm:"status"`
	PayId             string  `xorm:"pay_id"`
	PaymentId         string  `xorm:"payment_id"`
	CountryCode       int     `xorm:"country_code"`
	Created           int64   `xorm:"created"`
}
