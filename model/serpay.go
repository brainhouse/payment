package model

//orgNo=8211000787
//custId=21100900002351
//custOrderNo=SPIN1634047113052607
//prdOrdNo=20211012215835252172KqiVtchV7
//ordStatus=01
//ordAmt=6000
//payAmt=6000
//ordTime=20211012215835
//version=2.1
//sign=3631F5377AC387BC2AF0D41F05E2E3BE
type SerInCallback struct {
	CustOrderNo string `json:"custOrderNo"`
	OrgNo       string `json:"orgNo"`
	CustId      string `json:"custId"`
	PrdOrdNo    string `json:"prdOrdNo"`
	OrdStatus   string `json:"ordStatus"`
	OrdAmt      string `json:"ordAmt"`
	PayAmt      string `json:"payAmt"`
	OrdTime     string `json:"ordTime"`
	Version     string `json:"version"`
	OrderDesc   string `json:"orderDesc"`
	Clabe       string `json:"clabe"`
	Sign        string `json:"sign"`
	OrgStr      string
}
const MXMultiOrderDesc = "userTransferViaClabe"

func (in *SerInCallback) IsMXMultiPay() bool {
	return in.OrderDesc == MXMultiOrderDesc
}

func (in *SerInCallback) FunZoneThirdDesc() string {
	if  in.OrderDesc == MXMultiOrderDesc {
		return "multiTransferViaClabe"
	}

	return in.OrderDesc
}

type SerOutCallback struct {
	CustOrderNo string `json:"custOrderNo"`
	OrgNo       string `json:"orgNo"`
	CustId      string `json:"custId"`
	PrdOrdNo    string `json:"prdOrdNo"`
	OrdStatus   string `json:"ordStatus"`
	PayAmt      string `json:"payAmt"`
	Sign        string `json:"sign"`
	OrgStr      string
}
