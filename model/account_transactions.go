package model

/**
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'id',
  `uid` bigint NOT NULL DEFAULT '0' COMMENT '用户uid',
  `data_id` bigint NOT NULL DEFAULT '0' COMMENT '数据id',
  `type` tinyint NOT NULL DEFAULT '0' COMMENT '类型 1 充值 2提现',
  `amount` bigint NOT NULL DEFAULT '0' COMMENT '金额',
  `created` bigint NOT NULL DEFAULT '0' COMMENT '创建时间',
*/

type AccountTSType int

const (
	TransIn       AccountTSType = 1 //充值入金
	TransOut      AccountTSType = 2 //提现金额
	TransOutFee   AccountTSType = 3 //提现手续费
	TransCoInFee  AccountTSType = 4 //合作账号充值分成
	TransCoOutFee AccountTSType = 5 //合作账号提现分成

	TransAgentInFee  AccountTSType = 6 //代理充值分成
	TransAgentOutFee AccountTSType = 7 //代理提现分成

	TransReversed AccountTSType = 8 //提现回滚金额

	TransSettlement AccountTSType = 9 //充值结算

	TransCoOutFeeRev    AccountTSType = 10 //合作账号提现分成回滚
	TransAgentOutFeeRev AccountTSType = 11 //代理提现分成回滚

	TransEntryReversed    AccountTSType = 12 //充值回滚金额
	TransCoEntryFeeRev    AccountTSType = 13 //合作账号充值分成回滚
	TransAgentEntryFeeRev AccountTSType = 14 //代理充值分成回滚

	TransEntryManualReversed AccountTSType = 15 // 手动回滚退费,不退手续费

	TransAutoReversed AccountTSType = 20 //提现失败回滚冻结金额
	TransFrozen       AccountTSType = 21 //提现冻结金额

	TransAdminInFee       AccountTSType = 100 //平台充值手续费
	TransAdminOutFee      AccountTSType = 101 //平台提现手续费
	TransAdminReversedFee AccountTSType = 102 //平台提现手续费回滚
	TransAdminEntryRevFee AccountTSType = 103 //平台充值手续费回滚

)

type AccountTransactions struct {
	Id                   int64         `xorm:"id"`
	Uid                  int64         `xorm:"uid"`
	DataId               int64         `xorm:"data_id"`
	Type                 AccountTSType `xorm:"type"`
	Amount               float64       `xorm:"amount"`
	StartBalance         float64       `xorm:"start_balance"`
	EndBalance           float64       `xorm:"end_balance"`
	StartUnSettleBalance float64       `xorm:"start_unsettled_balance"`
	EndUnSettleBalance   float64       `xorm:"end_unsettled_balance"`
	Created              int64         `xorm:"created"`
}
