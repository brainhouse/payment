package model

type AppId string

const (
	PUBG    AppId = "pubg"
	RUMMY   AppId = "rummy"
	FUN     AppId = "fun"
	JHRUMMY AppId = "rummy_josh"
	GameBR  AppId = "game_br"
	GameMX  AppId = "game_mx"
	GameCL  AppId = "game_cl"
)

type PayChannel string

const (
	PAYDEFAULT PayChannel = ""
	PAYTM      PayChannel = "paytm"
	RAZORPAY   PayChannel = "razorpay"
	CASHFREE   PayChannel = "cashfree"
	EASEBUZZ   PayChannel = "easebuzz"
	EAGLEPAY   PayChannel = "eaglepay"
	ONIONPAY   PayChannel = "onionpay"
	SERPAY     PayChannel = "serpay"
	FIDYPAY    PayChannel = "fidypay"
	PAYU       PayChannel = "payu"
	PAYU2      PayChannel = "payu2"
	PAYU3      PayChannel = "payu3"
	PAGSMILE   PayChannel = "pagsmile"
	GLOBPAY    PayChannel = "globpay"
	PAYFLASH   PayChannel = "payflash"
	PDKPAY     PayChannel = "pdkpay"
	ZWPAY      PayChannel = "zwpay"
	FMPAY      PayChannel = "fmpay"
	YBPAY      PayChannel = "ybpay"
	YDGPAY     PayChannel = "ydgpay"
	LOOGPAY    PayChannel = "loogpay"
	XPAY       PayChannel = "xpay"
	AIRPAY     PayChannel = "airpay"
	CLICKNCASH PayChannel = "clickncash"
	PAYG       PayChannel = "pgpay"
	BHTPAY     PayChannel = "bhtpay"
	OEPAY      PayChannel = "oepay"
	UPPAY      PayChannel = "uppay"
	OPAY       PayChannel = "opay"
	HOPEPAY    PayChannel = "hopepay"
	HDPAY      PayChannel = "hdpay"
	CASHPAY    PayChannel = "cashpay"
	WDLPAY     PayChannel = "wdlpay"
	PhonePay   PayChannel = "phonepay"
)

const (
	CashPayAccountID            = "cashpay_br"
	CashPayAccountIDCustomizeH5 = "cashpay_br2"
)

type SettleStatus int

const (
	UnSettled SettleStatus = 0
	Settled   SettleStatus = 1
	Frozen    SettleStatus = 2
)

type PayStatus int

const (
	PAYING       PayStatus = 0
	PAYSUCCESS   PayStatus = 1
	PAYFAILD     PayStatus = 2
	PAYCANCELLED PayStatus = 3
	PAYROLLBACK  PayStatus = 4
	PAYINACTIVE  PayStatus = 5 // air pay生成支付订单，订单先进入非活跃状态，支付页面拉起之后进入活跃
)

type PayMethodType int

const (
	PAY_IN  PayMethodType = 1 //充值
	PAY_OUT PayMethodType = 2 //提现
)

/*
*
入金表
*/
type PayEntry struct {
	Id             int64      `json:"id"`
	Uid            int64      `xorm:"uid" json:"-"`
	AppId          string     `xorm:"app_id" json:"app_id"`
	PayChannel     PayChannel `xorm:"pay_channel" json:"pay_channel"`
	PayAccountId   string     `xorm:"pay_account_id" json:"-"`
	CountryCode    int        `xorm:"country_code" json:"country_code"`
	AppOrderId     string     `xorm:"app_order_id" json:"app_order_id"`
	OrderId        string     `xorm:"order_id" json:"order_id"`
	PaymentOrderId string     `xorm:"payment_order_id" json:"payment_order_id"`
	PaymentId      string     `xorm:"payment_id" json:"payment_id"`
	Status         PayStatus  `xorm:"status" json:"status"`
	SettleStatus   int        `xorm:"settle_status" json:"settle_status"`
	Amount         float64    `xorm:"amount" json:"amount"`
	Fee            float64    `xorm:"fee" json:"fee"`
	//Discount        float64    `xorm:"discount" json:"discount"`
	CooperateFee    float64 `xorm:"cooperate_fee" json:"cooperate_fee"`
	AgentFee        float64 `xorm:"agent_fee" json:"agent_fee"`
	UserId          string  `xorm:"user_id" json:"user_id"`
	UserName        string  `xorm:"user_name" json:"user_name"`
	Phone           string  `xorm:"phone" json:"phone"`
	Email           string  `xorm:"email" json:"email"`
	Address         int64   `xorm:"address" json:"address"`
	ThirdCode       string  `xorm:"third_code" json:"third_code"`
	ThirdDesc       string  `xorm:"third_desc" json:"third_desc"`
	FinishTime      int64   `xorm:"finish_time" json:"finish_time"`
	SuccessTime     int64   `xorm:"success_time" json:"success_time"`
	Created         int64   `xorm:"created" json:"created"`
	Updated         int64   `xorm:"updated" json:"updated"`
	PayAppId        string  `xorm:"-" json:"pay_app_id"`
	Signature       string  `xorm:"-" json:"-"`
	SignatureNew    string  `xorm:"-" json:"-"`
	PaymentLinkHost string  `xorm:"-" json:"-"`
	PK              string  `xorm:"-" json:"-"`
	Mid             string  `xorm:"-" json:"-"`
	MultiPay        bool    `xorm:"-" json:"-"` // 墨西哥电汇多次支付
}

type EntryCallbackList struct {
}

/*
*
回调数据结构体

	{
	        "pay_channel": "cashfree",
	        "app_order_id": "1123123123",
	        "order_id": "CF1585385678060488",
	        "payment_order_id": "gC9JCN4MzUIJiOicGbhJCLiQ1VKJiOiAXe0Jye.Yb9JyNyUGNzU2YwEjZ3UWNiojI0xWYz9lIsgzN2czN5cDO1EjOiAHelJCLiIlTJJiOik3YuVmcyV3QyVGZy9mIsADMwUjOiQnb19WbBJXZkJ3biwiI4gDNwYDM4cjN1gzM1gTNxY0QiojIklkclRmcvJye.rlaBW2H4v_IZu4BzDyIaK-lX7aE2i6SuFwsDpqxAcsPAb-DtFPcN4irM8dX8HLDmJF",
	        "payment_id": "",
	        "status": 0,  #0支付中 1支付成功 2支付失败
	        "amount": 5000,
	        "user_id": "",
	        "phone": "",
	        "email": "",
	    }
*/
type OrderCallbackData struct {
	PayChannel     PayChannel `json:"pay_channel,omitempty"`
	AppOrderId     string     `json:"app_order_id"`
	OrderId        string     `json:"order_id"`
	Amount         float64    `json:"amount"`
	PaymentOrderId string     `json:"payment_order_id,omitempty"` // 墨西哥支付就算clabe
	ThirdDesc      string     `json:"third_desc"`                 // 墨西哥支付，会有一个订单多次支付的情况，这个有特殊值
	PaymentId      string     `json:"payment_id,omitempty"`
	Status         PayStatus  `json:"status"`
	UserId         string     `json:"user_id,omitempty"`
	Phone          string     `json:"phone,omitempty"`
	Email          string     `json:"email,omitempty"`
}

type PlatCollectData struct {
	AppOrderId  string  `json:"app_order_id"`
	OrderId     string  `json:"order_id"`
	Amount      float64 `json:"amount"`
	PaymentLink string  `json:"payment_link"`
}
