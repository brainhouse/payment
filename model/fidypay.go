package model

/**
{
	"date": "2022-01-13 20:51:27.0",
	"npciTrxnId": "YESBD5792432FDB10BB9E05400144FFA511",
	"trxnNote": "charge",
	"amount": "100.0",
	"code": "0x0200",
	"payeeVPA": "funshinetech@yesbank",
	"description": "Transaction success",
	"payerMobileNO": "NA",
	"type": "COLLECT_AUTH",
	"payerVPA": "7845286022@paytm",
	"customerRefId": "201300020640",
	"merchantTrxnRefId": "YDdle+GX0qDiFaxvHYwETlKDebbq\/uHnGswD1KdlS\/Q=",
	"payerAccName": "THANEESH V",
	"responseCode": "0x0200",
	"payerAccNo": "XXXXXX1914",
	"originalOrderId": "test123123124",
	"payerIfsc": "LAVB0000537",
	"bankTrxnId": "5437036034"
}
*/
type FidyPayCollectCallback struct {
	Date              string `json:"date"`
	NpciTrxnId        string `json:"npciTrxnId"`
	TrxnNote          string `json:"trxnNote"`
	Amount            string `json:"amount"`
	Code              string `json:"code"`
	PayeeVPA          string `json:"payeeVPA"`
	Description       string `json:"description"`
	PayerMobileNO     string `json:"payerMobileNO"`
	Type              string `json:"type"`
	PayerVPA          string `json:"payerVPA"`
	CustomerRefId     string `json:"customerRefId"`
	MerchantTrxnRefId string `json:"merchantTrxnRefId"`
	PayerAccName      string `json:"payerAccName"`
	ResponseCode      string `json:"responseCode"`
	PayerAccNo        string `json:"payerAccNo"`
	OriginalOrderId   string `json:"originalOrderId"`
	PayerIfsc         string `json:"payerIfsc"`
	BankTrxnId        string `json:"bankTrxnId"`
}

/**
{
	"country": "IN",
	"amount": "10.0",
	"code": "0x0202",
	"address": "ABCStreet,N\/A",
	"description": "Invalid Benificiary MMID\/Mobile Number",
	"beneficiaryIfscCode": "LAVB0000537",
	"merchantTrxnRefId": "FDOUT1642489335033564",
	"trxn_id": "FPA1642489340322",
	"debitAccNo": "XXXXXXXXXXX4567",
	"utr": "NA",
	"beneficiaryAccNo": null,
	"beneficiaryName": "ThaneeshV",
	"instructionIdentification": "FDOUT1642489335033564",
	"status": "FAILED",
	"transactionIdentification": null,
	"creationDateTime": "2022-01-18T12:32:21.000+05:30"
}
*/
type FidyPayPayoutCallback struct {
	Country                   string `json:"country"`
	Amount                    string `json:"amount"`
	Code                      string `json:"code"`
	Address                   string `json:"address"`
	Description               string `json:"description"`
	BeneficiaryIfscCode       string `json:"beneficiaryIfscCode"`
	MerchantTrxnRefId         string `json:"merchantTrxnRefId"`
	TrxnId                    string `json:"trxn_id"`
	DebitAccNo                string `json:"debitAccNo"`
	Utr                       string `json:"utr"`
	BeneficiaryAccNo          string `json:"beneficiaryAccNo"`
	BeneficiaryName           string `json:"beneficiaryName"`
	InstructionIdentification string `json:"instructionIdentification"`
	Status                    string `json:"status"`
	TransactionIdentification string `json:"transactionIdentification"`
	CreationDateTime          string `json:"creationDateTime"`
}
