package model

/**
id` bigint(20) NOT NULL AUTO_INCREMENT,
  `recommend_email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '用户名',
  `recommend_uid` bigint(20) NOT NULL DEFAULT '0' COMMENT '推荐人uid',
  `target_email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '用户名',
  `target_uid` bigint(20) NOT NULL COMMENT '被推荐人uid',
  `charge_rate` decimal(11,2) NOT NULL DEFAULT '0.00' COMMENT '充值分成费率',
  `withdraw_rate` decimal(11,2) NOT NULL DEFAULT '0.00' COMMENT '提现分成费率',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '状态',
  `created` bigint(20) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `updated` bigint(20) NOT NULL DEFAULT '0' COMMENT '更新时间',
*/
type RecommendSettings struct {
	Id             int64
	RecommendEmail string  `xorm:"recommend_email"`
	RecommendUid   int64   `xorm:"recommend_uid"`
	TargetEmail    string  `xorm:"target_email"`
	TargetUid      int64   `xorm:"target_uid"`
	CountryCode    int     `xorm:"country_code"`
	ChargeRate     float64 `xorm:"charge_rate"`
	WithdrawRate   float64 `xorm:"withdraw_rate"`
	Status         int     `xorm:"status"`
	Created        int64   `xorm:"created"`
	Updated        int64   `xorm:"updated"`
}
