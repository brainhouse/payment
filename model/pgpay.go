package model

type PGPayCollect struct {
	OrderKeyID                    string      `json:"OrderKeyId"`
	MerchantKeyID                 int         `json:"MerchantKeyId"`
	UniqueRequestID               string      `json:"UniqueRequestId"`
	OrderType                     string      `json:"OrderType"`
	OrderAmount                   float64     `json:"OrderAmount"`
	OrderID                       interface{} `json:"OrderId"`
	OrderStatus                   string      `json:"OrderStatus"`
	OrderPaymentStatus            int         `json:"OrderPaymentStatus"`
	OrderPaymentStatusText        string      `json:"OrderPaymentStatusText"`
	PaymentStatus                 int         `json:"PaymentStatus"`
	PaymentTransactionID          string      `json:"PaymentTransactionId"`
	PaymentResponseCode           int         `json:"PaymentResponseCode"`
	PaymentApprovalCode           interface{} `json:"PaymentApprovalCode"`
	PaymentTransactionRefNo       interface{} `json:"PaymentTransactionRefNo"`
	PaymentResponseText           string      `json:"PaymentResponseText"`
	PaymentMethod                 string      `json:"PaymentMethod"`
	PaymentAccount                string      `json:"PaymentAccount"`
	OrderNotes                    string      `json:"OrderNotes"`
	PaymentDateTime               string      `json:"PaymentDateTime"`
	UpdatedDateTime               string      `json:"UpdatedDateTime"`
	PaymentProcessURL             interface{} `json:"PaymentProcessUrl"`
	CustomerData                  interface{} `json:"CustomerData"`
	ProductData                   interface{} `json:"ProductData"`
	OrderPaymentCustomerData      interface{} `json:"OrderPaymentCustomerData"`
	UpiLink                       interface{} `json:"UpiLink"`
	OrderPaymentTransactionDetail interface{} `json:"OrderPaymentTransactionDetail"`
}

type BHTUPICollect struct {
	Success int    `json:"success"`
	Message string `json:"message"`
	Data    struct {
		OrderID        string      `json:"order_id"`
		CustomerName   string      `json:"customer_name"`
		CustomerEmail  string      `json:"customer_email"`
		CustomerMobile string      `json:"customer_mobile"`
		Status         string      `json:"status"`
		Amount         string      `json:"amount"`
		CustomerVpa    interface{} `json:"customer_vpa"`
		UpiTxnID       interface{} `json:"upi_txn_id"`
		Remark         interface{} `json:"remark"`
		Udf1           string      `json:"udf1"`
		Udf2           string      `json:"udf2"`
		Udf3           string      `json:"udf3"`
		CreatedAt      string      `json:"created_at"`
	} `json:"data"`
}

type BHTPayout struct {
	Data struct {
		OrderID       string `json:"order_id"`
		BankAccountID string `json:"bank_account_id"`
		TxnType       string `json:"txn_type"`
		Amount        string `json:"amount"`
		Status        string `json:"status"`
		Purpose       string `json:"purpose"`
		BankRemark    string `json:"bank_remark"`
		RefID         string `json:"ref_id"`
		CreatedAt     string `json:"created_at"`
		Type          string `json:"type"`
	} `json:"data"`
	Success int    `json:"success"`
	Message string `json:"message"`
}

type OECollect struct {
	PlatFormOrderNo string `json:"platformOrderNo"`
	OrderNo         string `json:"orderNo"`
	Status          string `json:"status"`
}

type OEPayout struct {
	PlatFormOrderNo string `json:"platformOrderNo"`
	OrderNo         string `json:"orderNo"`
	Status          string `json:"status"`
	UtrNo           string `json:"utrNo"`
}

/**
{
"event":"TRANSFER_STATUS_UPDATE",
"status":"success",
"data":{
    "amount":"10",
    "remarks":"Withdraw from Test",
    "created_at":"2022-07-10T08:03:55.",
    "payment_mode":"IMPS",
    "transfer_date":"2022-07-10T08:03:55.",
    "beneficiary_bank_name":"test bank",
    "payout_id":"HOAD974138602500",
    "beneficiary_account_ifsc":"TEST0123",
    "beneficiary_account_name":"Test",
    "beneficiary_account_number":"1234567890",
    "beneficiary_upi_handle":"Null",
    "UTR":"12345678"
    }
}

{	"event":"TRANSFER_STATUS_UPDATE",
	"status":"SUCCESS",
	"data":{
		"payout_id":"H11092304141240208848912","amount":"100.00","remarks":"payout","created_at":"2023-04-14T12:40:20",
		"payment_mode":"IMPS","transfer_date":"Null","beneficiary_bank_name":"ptm bank","beneficiary_account_ifsc":"SBIN0040241",
		"beneficiary_account_name":"DHANUSH B B","beneficiary_account_number":"64186818245",
		"beneficiary_upi_handle":null,"UTR":"310418502377",
		"reference":"HOUT168147602003674131b0",
		"message":"Credited to beneficiary Mr  DHANUSH  B B on 14-04-2023 18:10:52",
		"checksum":"6a97e5306042adddc90e53c4fafba2a68067ca946fcc3e1682d13ce352566824"}}

{
	"event":"TRANSFER_STATUS_UPDATE",
	"status":"FAILED",
	"data":{
		"payout_id":"H11092304180111043183243",
		"amount":"500.00","remarks":"payout","created_at":"2023-04-18T13:11:04",
		"payment_mode":"IMPS","transfer_date":"Null","beneficiary_bank_name":"ptm bank",
		"beneficiary_account_ifsc":"PYTM0123456","beneficiary_account_name":"jamaluddin",
		"beneficiary_account_number":"8303177833","beneficiary_upi_handle":null,"UTR":null,"reference":"HOUT16818234640651410afb",
		"message":"Invalid account (Credit not allowed to account type)",
		"checksum":"0d2b11d13a37150456cc364cec5e7b27ff5550590e5b3f3133062b0973fd7396"}}
*/

type HDPayout struct {
	Event  string `json:"event"`
	Status string `json:"status"`
	Data   struct {
		Amount                   string `json:"amount"`
		Remarks                  string `json:"remarks"`
		CreatedAt                string `json:"created_at"`
		PaymentMode              string `json:"payment_mode"`
		TransferDate             string `json:"transfer_date"`
		BeneficiaryBankName      string `json:"beneficiary_bank_name"`
		PayoutID                 string `json:"payout_id"`
		BeneficiaryAccountIfsc   string `json:"beneficiary_account_ifsc"`
		BeneficiaryAccountName   string `json:"beneficiary_account_name"`
		BeneficiaryAccountNumber string `json:"beneficiary_account_number"`
		BeneficiaryUpiHandle     string `json:"beneficiary_upi_handle"`
		UTR                      string `json:"UTR"`
		Reference                string `json:"reference"`
		Message                  string `json:"message"`
		Checksum                 string `json:"checksum"`
	} `json:"data"`
}
