package model

type YDGCollectCallback struct {
	OrderNo      string `json:"orderNo"`
	MerchantNo   string `json:"merchantNo"`
	MerTradeNo   string `json:"merTradeNo"`
	Amount       string `json:"amount"`
	ActualAmount string `json:"actualAmount"`
	Poundage     string `json:"poundage"`
	OrderStatus  string `json:"orderStatus"`
	CreateTime   string `json:"createTime"`
	Sign         string `json:"sign"`
}

type YDGPayoutCallback struct {
	OrderNo     string `json:"orderNo"`
	MerchantNo  string `json:"merchantNo"`
	MerTradeNo  string `json:"merTradeNo"`
	Amount      string `json:"amount"`
	Poundage    string `json:"poundage"`
	OrderStatus string `json:"orderStatus"`
	CreateTime  string `json:"createTime"`
	Type        string `json:"type"`
	Sign        string `json:"sign"`
}
