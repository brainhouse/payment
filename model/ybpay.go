package model

/**
{
    "code":"1",
    "msg":"success",
    "order_status":"4",
    "mer_no":"21040760685101",
    "mer_order_no":"TEST_2021042207285315562",
    "order_amount":"40000.00",
    "pay_amount":"40000.23",
    "order_no":"2021042207285321281",
    "order_time":"1619056733",
    "sign":"2ebaa1a8bcef67b411d5e88f7f61c328"
}
*/
type YBCollectCallback struct {
	Code        int    `json:"code"`
	Msg         string `json:"msg"`
	MerNo       string `json:"mer_no"`
	MerOrderNo  string `json:"mer_order_no"`
	OrderStatus string `json:"order_status"`
	OrderAmount string `json:"order_amount"`
	PayAmount   string `json:"pay_amount"`
	OrderNo     string `json:"order_no"`
	OrderTime   int64  `json:"order_time"`
	Sign        string `json:"sign"`
}

/**
 {
    "code":"1",
    "msg":"success",
    "order_status":"3",
    "mer_no":"21040760685101",
    "mer_order_no":"TEST_2021042207435986182",
    "order_amount":"100.00",
    "order_no":"2021042207435993406",
    "order_time":"1619057640",
    "sign":"4420ecffb74d99b9eaf949befa0eddbc"
}
*/

type YBPayoutCallback struct {
	Code        int    `json:"code"`
	Msg         string `json:"msg"`
	OrderStatus string `json:"order_status"`
	MerNo       string `json:"mer_no"`
	MerOrderNo  string `json:"mer_order_no"`
	OrderAmount string `json:"order_amount"`
	OrderNo     string `json:"order_no"`
	OrderTime   int64  `json:"order_time"`
	Sign        string `json:"sign"`
}
