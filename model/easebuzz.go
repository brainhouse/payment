package model

/**
{
	"event": "TRANSFER_STATUS_UPDATE",
	"data": {
		"id": "tr680edcb53d454426ba59b65b25aab5",
		"unique_request_number": "EBOUT1619018652051495",
		"created_at": "2021-04-21T20:54:25.697006+05:30",
		"beneficiary_id": "bene59b4bc4e459bafb59a9a0966b2f2",
		"beneficiary_bank_name": "Karur Vysya Bank",
		"beneficiary_account_name": "Thaneeshv",
		"beneficiary_account_number": "1769155000016710",
		"beneficiary_account_ifsc": "KVBL0001769",
		"beneficiary_upi_handle": null,
		"unique_transaction_reference": "111120748961",
		"payment_mode": "IMPS",
		"currency": "INR",
		"amount": 10.0,
		"service_charge": 3.0,
		"gst_amount": 0.54,
		"service_charge_with_gst": 3.54,
		"narration": "",
		"status": "success",
		"failure_reason": null,
		"queue_on_low_balance": false,
		"transfer_date": "2021-04-21T20:54:25.707532+05:30",
		"created_by": null,
		"udf1": null,
		"udf2": null,
		"udf3": null,
		"udf4": null,
		"udf5": null
	}
}
*/
type EaseBuzzPayoutCallback struct {
	Event string                     `json:"event"`
	Data  EaseBuzzPayoutCallbackData `json:"data"`
}

type EaseBuzzPayoutCallbackData struct {
	Id                         string `json:"id"`
	UniqueRequestNumber        string `json:"unique_request_number"`
	UniqueTransactionReference string `json:"unique_transaction_reference"`
	Status                     string `json:"status"`
	FailureReason              string `json:"failure_reason"`
}
