package model

/**
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `data_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '关联数据id',
  `uid` bigint(20) NOT NULL DEFAULT '0' COMMENT '用户uid',
  `amount` bigint(20) NOT NULL DEFAULT '0' COMMENT '冻结金额',
  `status` tinyint(20) NOT NULL DEFAULT '0' COMMENT '状态 0 冻结 1释放',
  `created` bigint(20) NOT NULL DEFAULT '0' COMMENT '创建时间',
*/

type AFTransType int

const (
	FrozenDefault AFTransType = 0
	FrozenRelease AFTransType = 1
)

type AccountFrozenTransactions struct {
	Id      int64
	DataId  int64       `xorm:"data_id"`
	Uid     int64       `xorm:"uid"`
	Amount  float64     `xorm:"amount"`
	Status  AFTransType `xorm:"status"`
	Created int64       `xorm:"created"`
}
