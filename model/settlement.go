package model

/**
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `total` bigint NOT NULL DEFAULT '0' COMMENT '总金额',
  `nums` bigint NOT NULL COMMENT '总笔数',
  `ext` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '备注',
  `created` bigint NOT NULL DEFAULT '0' COMMENT '结算时间',
*/
type SettlementHistory struct {
	Id           int64   `json:"id"`
	Total        float64 `xorm:"total" json:"total"`
	Nums         int64   `xorm:"nums" json:"nums"`
	PayAccountId string  `xorm:"pay_account_id" json:"pay_account_id"`
	Ext          string  `xorm:"ext" json:"ext"`
	AdminUser    string  `xorm:"admin_user" json:"admin_user"`
	Created      int64   `xorm:"created" json:"created"`
}

type UserSettlementHistory struct {
	Id        int64   `json:"id"`
	Uid       int64   `xorm:"uid" json:"uid"`
	Total     float64 `xorm:"total" json:"total"`
	Ext       string  `xorm:"ext" json:"ext"`
	AdminUser string  `xorm:"admin_user" json:"admin_user"`
	Created   int64   `xorm:"created" json:"created"`

	CountryCode int `xorm:"country_code" json:"country_code"`
}
