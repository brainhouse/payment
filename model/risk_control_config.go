package model

/**
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT '0' COMMENT '用户uid',
  `type` varchar(50) NOT NULL DEFAULT '' COMMENT '风控类型',
  `time_range` varchar(50) NOT NULL DEFAULT '' COMMENT '时间维度',
  `threshold` varchar(50) NOT NULL DEFAULT '0' COMMENT '值',
  `created` int(10) NOT NULL DEFAULT '0' COMMENT '创建时间',
*/
type RiskControlConfig struct {
	Id        int64  `json:"id"`
	Uid       int64  `xorm:"uid" json:"uid"`
	Type      string `xorm:"type" json:"type"`
	TimeRange string `xorm:"time_range" json:"time_range"`
	Threshold string `xorm:"threshold" json:"threshold"`
	Sort      int    `xorm:"sort" json:"sort"`
	Created   int64  `xorm:"created" json:"created"`
	Updated   int64  `xorm:"updated" json:"updated"`
}
