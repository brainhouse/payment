package model

type PayEntryCallbackList struct {
	Id             int64
	AppId          string     `xorm:"app_id" json:"app_id"`
	PayChannel     PayChannel `xorm:"pay_channel" json:"pay_channel"`
	AppOrderId     string     `xorm:"app_order_id" json:"app_order_id"`
	OrderId        string     `xorm:"order_id" json:"order_id"`
	PaymentOrderId string     `xorm:"payment_order_id" json:"payment_order_id"`
	Status         PayStatus  `xorm:"status" json:"status"`
	ReturnCode     string     `xorm:"return_code" json:"return_code"`
	Response       string     `xorm:"response" json:"response"`
	Times          int        `xorm:"times" json:"times"`
	Created        int64      `xorm:"created" json:"created"`
	Updated        int64      `xorm:"Updated" json:"Updated"`
}
