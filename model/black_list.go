package model

/**
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '类型',
  `data` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '值',
  `created` bigint(20) DEFAULT '0' COMMENT '创建时间',
  `updated` bigint(20) DEFAULT '0' COMMENT '更新时间',
*/
type BlackList struct {
	Id      int64  `json:"id"`
	Type    string `xorm:"type" json:"type"`
	Data    string `xorm:"data" json:"data"`
	Created int64  `xorm:"created" json:"created"`
	Updated int64  `xorm:"updated" json:"updated"`

	CountryCode int `xorm:"country_code" json:"country_code"`
}
