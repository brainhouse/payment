package model

/**
`id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
	`uid` bigint(20) NOT NULL DEFAULT '0' COMMENT '用户uid',
	`pay_channel` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '付款渠道 paytm',
	`pay_account_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '渠道账号',
	`fee_rate` decimal(11,2) NOT NULL DEFAULT '0.00' COMMENT '充值费率',
	`payout_fee_rate` decimal(11,2) NOT NULL DEFAULT '0.00' COMMENT '提现费率',
	`user_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '用户名',
	`desc` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '描述',
	`status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '状态',
	`created` bigint(20) NOT NULL DEFAULT '0' COMMENT '创建时间',
	`updated` bigint(20) NOT NULL DEFAULT '0' COMMENT '更新时间',
*/
type CorporationSetting struct {
	Id            int64
	Uid           int64   `xorm:"uid"`
	PayChannel    string  `xorm:"pay_channel"`
	PayAccountId  string  `xorm:"pay_account_id"`
	CountryCode   int     `xorm:"country_code"`
	FeeRate       float64 `xorm:"fee_rate"`
	PayoutFeeRate float64 `xorm:"payout_fee_rate"`
	UserName      string  `xorm:"user_name"`
	Desc          string  `xorm:"desc"`
	Status        int     `xorm:"status"`
	Created       int64   `xorm:"created"`
	Updated       int64   `xorm:"updated"`
}
