package model

type PayOutStatus int

const PAY_OUT_ING PayOutStatus = 0
const PAY_OUT_SUCCESS PayOutStatus = 1
const PAY_OUT_FAILD PayOutStatus = 2
const PAY_OUT_REVERSED PayOutStatus = 3

type PayType string

const PT_Default PayType = ""
const PT_PayTm PayType = "paytm"
const PT_BANK PayType = "bank"
const PT_UPI PayType = "upi"
const PT_CLABE PayType = "clabe"   // 墨西哥
const PT_PIX PayType = "PIX"       // 巴西
const PT_WALLET PayType = "wallet" // 巴西-钱包-pagsmile

const (
	None     = ""
	India    = "IN"
	Brazil   = "BR"
	Mexico   = "MX"
	Pakistan = "PK"
)

type CardType string

const (
	CardTypePixCPF   = "CPF"
	CardTypePixCNPJ  = "CNPJ"
	CardTypePixPHONE = "PHONE"
	CardTypePixEMAIL = "EMAIL"
	CardTypePixEVP   = "EVP"

	CardTypeBankCheckIn = "CHECKING"
	CardTypeBankSaving  = "Savings"
)

type FeeDesc struct {
	Fixed float64 `json:"fixed"`
	Fee   float64 `json:"fee"`
}

/*
*
出金表
*/
type PayOut struct {
	Id           int64        `json:"id"`
	Uid          int64        `xorm:"uid" json:"uid"`
	AppId        string       `xorm:"app_id" json:"app_id"`
	PayChannel   PayChannel   `xorm:"pay_channel" json:"pay_channel"`
	PayAccountId string       `xorm:"pay_account_id" json:"-"`
	CountryCode  int          `xorm:"country_code" json:"country_code"`
	FrozenId     int64        `xorm:"frozen_id" json:"frozen_id"`
	AppOrderId   string       `xorm:"app_order_id" json:"app_order_id"`
	PayType      PayType      `xorm:"pay_type" json:"pay_type"`
	Amount       float64      `xorm:"amount" json:"amount"`
	Fee          float64      `xorm:"fee" json:"fee"`
	CooperateFee float64      `xorm:"cooperate_fee" json:"cooperate_fee"`
	AgentFee     float64      `xorm:"agent_fee" json:"agent_fee"`
	OrderId      string       `xorm:"order_id" json:"order_id"`
	PaymentId    string       `xorm:"payment_id" json:"payment_id"`
	Utr          string       `xorm:"utr" json:"utr"`
	UserId       string       `xorm:"user_id" json:"user_id"`
	UserName     string       `xorm:"user_name" json:"user_name"`
	Phone        string       `xorm:"phone" json:"phone"`
	Email        string       `xorm:"email" json:"email"`
	Paytm        string       `xorm:"paytm" json:"paytm"`
	VPA          string       `xorm:"vpa" json:"vpa"` //提现到upi需要
	IFSC         string       `xorm:"ifsc" json:"ifsc"`
	BankCard     string       `xorm:"bank_card" json:"bank_card"`
	BeneId       string       `xorm:"bene_id" json:"bene_id"`
	Address      string       `xorm:"address" json:"address"`
	Status       PayOutStatus `xorm:"status" json:"status"`
	ThirdCode    string       `xorm:"third_code" json:"third_code"`
	ThirdDesc    string       `xorm:"third_desc" json:"third_desc"`
	FeeDesc      string       `xorm:"fee_desc" json:"fee_desc"`
	FinishTime   int64        `xorm:"finish_time" json:"finish_time"`
	SuccessTime  int64        `xorm:"success_time" json:"success_time"`
	Created      int64        `xorm:"created" json:"created"`
	Updated      int64        `xorm:"updated" json:"updated"`
}

type PayOutCallbacks struct {
}

/*
*
回调数据结构体
*/
type PayOutCallbackData struct {
	PayChannel PayChannel   `json:"pay_channel,omitempty"`
	AppOrderId string       `json:"app_order_id"`
	OrderId    string       `json:"order_id"`
	Amount     float64      `json:"amount,omitempty"`
	PaymentId  string       `json:"payment_id,omitempty"`
	Status     PayOutStatus `json:"status"`
	UserId     string       `json:"user_id,omitempty"`
	Phone      string       `json:"phone,omitempty"`
	Email      string       `json:"email,omitempty"`
}

type PayOutCallbackList struct {
	Id         int64
	AppId      string       `xorm:"app_id" json:"app_id"`
	PayChannel PayChannel   `xorm:"pay_channel" json:"pay_channel"`
	OrderId    string       `xorm:"order_id" json:"order_id"`
	PaymentId  string       `xorm:"payment_id" json:"payment_id"`
	AppOrderId string       `xorm:"app_order_id" json:"app_order_id"`
	Status     PayOutStatus `xorm:"status" json:"status"`
	ReturnCode string       `xorm:"return_code" json:"return_code"`
	Times      int          `xorm:"times" json:"times"`
	Response   string       `xorm:"response" json:"response"`
	Created    int64        `xorm:"created" json:"created"`
	Updated    int64        `xorm:"Updated" json:"Updated"`
}

type PlatPayoutData struct {
	AppOrderId string       `json:"app_order_id"`
	OrderId    string       `json:"order_id"`
	Amount     float64      `json:"amount"`
	Status     PayOutStatus `json:"status"`
}
