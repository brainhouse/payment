package model

//orgNo=8211000787
//custId=21100900002351
//custOrderNo=SPIN1634047113052607
//prdOrdNo=20211012215835252172KqiVtchV7
//ordStatus=01
//ordAmt=6000
//payAmt=6000
//ordTime=20211012215835
//version=2.1
//sign=3631F5377AC387BC2AF0D41F05E2E3BE
type OnionInCallback struct {
	MerTransNo  string `json:"merTransNo"`
	TransNo     string `json:"transNo"`
	TransStatus string `json:"transStatus"`
	Sign        string `json:"sign"`
	OrgStr      string
}

type OnionOutCallback struct {
	Message     string `json:"message"`
	MerTransNo  string `json:"merTransNo"`
	TransNo     string `json:"transNo"`
	TransStatus string `json:"transStatus"`
	Sign        string `json:"sign"`
	Utr         string `json:"utr"`
	OrgStr      string
}
