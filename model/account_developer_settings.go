package model

/**
  `id` int(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `uid` bigint(20) NOT NULL DEFAULT '0' COMMENT '用户uid',
  `app_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '应用id',
  `app_secret` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '调用秘钥',
  `collect_callback_url` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '收款结果回调',
  `payout_callback_url` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '提现结果回调',
  `created` bigint(20) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `updated` bigint(20) NOT NULL DEFAULT '0' COMMENT '更新时间',
*/
type AccountDeveloperSettings struct {
	Id                 int64  `xorm:"id"`
	Uid                int64  `xorm:"uid"`
	AppId              string `xorm:"app_id"`
	AppSecret          string `xorm:"app_secret"`
	SecretEncrypt      string `xorm:"secret_encrypt"`
	CollectCallbackUrl string `xorm:"collect_callback_url"`
	PayoutCallbackUrl  string `xorm:"payout_callback_url"`
	IpWhiteList        string `xorm:"ip_white_list"`
	Created            int64  `xorm:"created"`
	Updated            int64  `xorm:"updated"`
}
