package model


type PagSmilePayNotifyUser struct {
	Identify struct {
		Number string `json:"number"`
		Type   string `json:"type"`
	} `json:"identify"`
	Phone string `json:"phone"`
	Email string `json:"email"`
}

type PagSmilePayNotify struct {
	Amount       string                `json:"amount"`
	OutTradeNO   string                `json:"out_trade_no"`
	Method       string                `json:"method"`
	TradeStatus  string                `json:"trade_status"`
	TradeNO      string                `json:"trade_no"`
	Currency     string                `json:"currency"`
	OutRequestNO string                `json:"out_request_no"`
	AppID        string                `json:"app_id"`
	User         PagSmilePayNotifyUser `json:"user"`
	Card         struct {
		CardNO string `json:"card_no"`
	} `json:"card"`
}

const PagSmilePayStatusPAID = "PAID"
const PagSmilePayStatusREJECTED = "REJECTED"

type PagSmilePayOutNotify struct {
	PayoutId   string `json:"payoutId"`
	CustomCode string `json:"custom_code"`
	Status     string `json:"status"`
	Msg        string `json:"msg"`
	Timestamp  int    `json:"timestamp"`
}