package model

type CFOrderCallBack struct {
	OrderId     string
	OrderAmount string
	ReferenceId string
	TxStatus    string
	PaymentMode string
	TxMsg       string
	TxTime      string
	Signature   string
}

/**
 * event=TRANSFER_SUCCESS&transferId=CFOUT1585581836034664&referenceId=100386&acknowledged=1&eventTime=2020-03-30+20:55:06&utr=W1585580871&signature=fd/gES1C7d49bqmvJb8L1geXGTZppuzLkWL+iFNq6bs=
 */
type CFPayOutCallBack struct {
	Event        string `json:"event"`
	TransferId   string `json:"transfer_id"`
	ReferenceId  string `json:"reference_id"`
	Reason       string `json:"reason"`
	Acknowledged string `json:"acknowledged"`
	EventTime    string `json:"eventTime"`
	Utr          string `json:"utr"`
	Signature    string `json:"signature"`
}
