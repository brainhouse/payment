package model

/**
  `id` bigint NOT NULL AUTO_INCREMENT,
  `action` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `channel` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `pay_app_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `order_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `ip` varchar(255) NOT NULL DEFAULT '',
  `created` bigint NOT NULL,
*/
type PayWebLog struct {
	Id       int64
	Action   string `xorm:"action"`
	Channel  string `xorm:"channel"`
	PayAppId string `xorm:"pay_app_id"`
	OrderId  string `xorm:"order_id"`
	IP       string `xorm:"ip"`
	Created  int64  `xorm:"created"`
}
