package model

/**
 * razorpay json回调解析结构体
 *
{
 "entity": "event",
 "account_id": "acc_E0NutbblLtVzEv",
 "event": "order.paid",
 "contains": ["payment", "order"],
 "payload": {
  "payment": {
   "entity": {
    "id": "pay_ERmJm0047GqQXz",
    "entity": "payment",
    "amount": 50000,
    "currency": "INR",
    "status": "captured",
    "order_id": "order_ERmIiHvBhNC0Mc",
    "invoice_id": null,
    "international": false,
    "method": "upi",
    "amount_refunded": 0,
    "refund_status": null,
    "captured": true,
    "description": "A Wild Sheep Chase is the third novel by Japanese author Haruki Murakami",
    "card_id": null,
    "bank": null,
    "wallet": null,
    "vpa": "success@razorpay",
    "email": "gaurav.kumar@example.com",
    "contact": "+919999999999",
    "notes": [],
    "fee": 1180,
    "tax": 180,
    "error_code": null,
    "error_description": null,
    "created_at": 1584118955
   }
  },
  "order": {
   "entity": {
    "id": "order_ERmIiHvBhNC0Mc",
    "entity": "order",
    "amount": 50000,
    "amount_paid": 50000,
    "amount_due": 0,
    "currency": "INR",
    "receipt": "RZ1584118893965722000693406",
    "offer_id": null,
    "status": "paid",
    "attempts": 1,
    "notes": [],
    "created_at": 1584118894
   }
  }
 },
 "created_at": 1584118955
}
*/

type HookEvent string

const (
	OrderPaid       HookEvent = "order.paid"
	PaymentFailed   HookEvent = "payment.failed"
	PaymentCaptured HookEvent = "payment.captured"
	PayoutProcessed HookEvent = "payout.processed"
	PayoutFailed    HookEvent = "payout.failed"
	PayoutReversed  HookEvent = "payout.reversed"
	PayoutRejected  HookEvent = "payout.rejected"

	RefundsProcessed      HookEvent = "refund.processed" // 充值退款
)

type PaymentStatus string

const (
	Captured PaymentStatus = "captured"
	Failed   PaymentStatus = "failed"
	Refunded PaymentStatus = "refunded"
)

type OrderStatus string

const (
	Paid OrderStatus = "paid"
)

type PayLoad struct {
	Payment Payment `json:"payment"`
	Order   Order   `json:"order"`
}

type Payment struct {
	Entity PaymentEntity `json:"entity"`
}

type PaymentEntity struct {
	Id               string        `json:"id"`
	Entity           string        `json:"entity"`
	Amount           int64         `json:"amount"`
	Currency         string        `json:"currency"`
	Status           PaymentStatus `json:"status"`
	OrderId          string        `json:"order_id"`
	InvoiceId        string        `json:"invoice_id"`
	International    bool          `json:"international"`
	Method           string        `json:"method"`
	Captured         bool          `json:"captured"`
	Description      string        `json:"description"`
	Fee              int64         `json:"fee"`
	Tax              int64         `json:"tax"`
	ErrorCode        string        `json:"error_code"`
	ErrorDescription string        `json:"error_description"`
	CreatedAt        int64         `json:"created_at"`
}

type Order struct {
	Entity OrderEntity `json:"entity"`
}

type OrderEntity struct {
	Id         string      `json:"id"`
	Entity     string      `json:"entity"`
	Amount     int64       `json:"amount"`
	AmountPaid int64       `json:"amount_paid"`
	AmountDue  int64       `json:"amount_due"`
	Currency   string      `json:"currency"`
	Receipt    string      `json:"receipt"`
	OfferId    string      `json:"offer_id"`
	Status     OrderStatus `json:"status"`
	Attempts   int         `json:"attempts"`
	CreatedAt  int64       `json:"created_at"`
}
type CallBackWebHook struct {
	Entity    string    `json:"entity"`
	AccountId string    `json:"account_id"`
	Event     HookEvent `json:"event"`
	PayLoad   PayLoad   `json:"payload"`
}

/**
 *
{
	"id": "order_EPhIvAjVwIqqpL",
	"entity": "order",
	"amount": 50000,
	"amount_paid": 50000,
	"amount_due": 0,
	"currency": "INR",
	"receipt": "order_test1123213123",
	"offer_id": null,
	"status": "paid",
	"attempts": 1,
	"notes": [],
	"created_at": 1583664618
}
*/

type RazorPayOrderEntity struct {
	Id         string      `json:"id"`
	Entity     string      `json:"entity"`
	Amount     int64       `json:"amount"`
	AmountPaid int64       `json:"amount_paid"`
	AmountDue  int64       `json:"amount_due"`
	Currency   string      `json:"currency"`
	Receipt    string      `json:"receipt"`
	Status     OrderStatus `json:"status"`
	CreateAt   int64       `json:"create_at"`
}

/**
 *
{
  "id": "pay_29QQoUBi66xm2f",
  "entity": "payment",
  "amount": 5000,
  "currency": "INR",
  "status": "captured",
  "method": "card",
  "order_id": "order_CjCr5oKh4AVC51",
  "description": "Payment for Adidas shoes",
  "amount_refunded": 0,
  "refund_status": null,
  "email": "gaurav.kumar@example.com",
  "contact": "9364591752",
  "notes": [],
  "fee": 1145,
  "tax": 145,
  "error_code": null,
  "error_description": null,
  "created_at": 1400826750
}
*/
type RazorPaymentEntity struct {
	Id               string        `json:"id"`
	Entity           string        `json:"entity"`
	Amount           int64         `json:"amount"`
	Currency         string        `json:"currency"`
	OrderId          string        `json:"order_id"`
	Status           PaymentStatus `json:"status"`
	Fee              int64         `json:"fee"`
	Tax              int64         `json:"tax"`
	ErrorCode        string        `json:"error_code"`
	ErrorDescription string        `json:"error_description"`
	CreateAt         int64         `json:"create_at"`
}

/**
{
  "entity":"event",
  "account_id":"acc_BfVUrG6tDiL7H0",
  "event":"payout.processed",
  "contains":[
    "payout"
  ],
  "payload":{
    "payout":{
      "entity":{
        "id":"pout_1Aa00000000001",
        "entity":"payout",
        "fund_account_id":"fa_1Aa00000000001",
        "amount":100,
        "currency":"INR",
        "notes":{
          "note_key 1":"Tea. Earl Gray. Hot.",
          "note_key 2":"Tea. Earl Gray. Decaf."
        },
        "fees":3,
        "tax":0,
        "status":"processed",
        "purpose":"payout",
        "utr":"qwer1yuijaaasss",
        "mode":"IMPS",
        "reference_id":null,
        "narration":"Acme Fund Transfer",
        "batch_id":null,
        "failure_reason":null,
        "created_at":1579175640
      }
    }
  },
  "created_at":1579175674
}
*/

type PayOutLoad struct {
	Payout PayoutData `json:"payout"`
}

type PayoutData struct {
	Entity PayOutEntity `json:"entity"`
}

type PayOutEntity struct {
	Id            string `json:"id"`
	ReferenceId   string `json:"reference_id"`
	FailureReason string `json:"failure_reason"`
}

type PayoutCallBackWebHook struct {
	Event   HookEvent  `json:"event"`
	PayLoad PayOutLoad `json:"payload"`
}
