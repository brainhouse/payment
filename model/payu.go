package model

type PayuOutEvent string

const (
	PayUSuccess          PayuOutEvent = "TRANSFER_SUCCESS"
	PayUReserved         PayuOutEvent = "TRANSFER_REVERSED"
	PayUFailed           PayuOutEvent = "TRANSFER_FAILED"
	PayUProcessingFailed PayuOutEvent = "REQUEST_PROCESSING_FAILED"
)

/**
{
	“split_info”: ”59017743″,
	“customerName”: ”Test user”,
	“additionalCharges”: ””,
	“paymentMode”: ”DC”,
	“hash”: ”64700 c3a13f37c1271df8c0ebe67c4aad2d29a2086e80aaa3d16580bbe38f9ffd0c3996eab241aa730a4efe512c6c15730ca66020064d71d85dcb68631119f29″,
	“status”: ”Success”,
	“error_Message”: ”No Error”,
	“paymentId”: ”59017743″,
	“productInfo”: ”Description1″,
	“customerEmail”: ”storedcard8 @yopmail.com”,
	“customerPhone”: ”6709133497″,
	“merchantTransactionId”: ”4826753 - 59017743″,
	“amount”: ”100.0″,
	“udf2″: ””,
	“notificationId”: ”37208″,
	“udf1″: ””,
	“udf5″: ””,
	“udf4″: ””,
	“udf3″: ””
}
*/
type PayUOrderData struct {
	Status                string `json:"status"`
	PaymentId             string `json:"paymentId"`
	MerchantTransactionId string `json:"merchantTransactionId"`
	ErrorMessage          string `json:"error_Message"`
}

/**
{
"event" : "TRANSFER_SUCCESS",
"msg" : "Low Balance",
"payuRefId" : "3212312ds",
"merchantReferenceId" : "3212312ds",
"bankReferenceId" : "3212312ds"
}
*/
type PayUOutCallback struct {
	Event               PayuOutEvent `json:"event"`
	Msg                 string       `json:"msg"`
	PayuRefId           string       `json:"payuRefId"`
	MerchantReferenceId string       `json:"merchantReferenceId"`
	BankReferenceId     string       `json:"bankReferenceId"`
}
