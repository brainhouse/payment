package main

import (
	"crypto/tls"
	"fmt"
	"funzone_pay/app/router"
	"funzone_pay/app/service"
	"funzone_pay/boostrap"
	"funzone_pay/centerlog"
	"funzone_pay/common"
	"funzone_pay/crontab"
	"github.com/gin-gonic/gin"
	"net/http"
)

func main() {
	//go crontab.Start()
	common.InitPublicIP()
	r := router.GetRouter()
	crontab.Start()
	common.InitAlarmRoutine()
	err := service.Init()
	if err != nil {
		panic(fmt.Sprintf("start error %v", err))
	}
	centerlog.Init("172.17.18.134", "12201", "172.17.18.134", "12201")
	crontab.InitPhonePayStatusRoutine()
	// 指定地址和端口号
	conf := boostrap.Conf
	s := &http.Server{
		Addr:         fmt.Sprintf("%s:%d", conf.Host, conf.Port),
		Handler:      r,
		TLSNextProto: map[string]func(*http.Server, *tls.Conn, http.Handler){}, // disable http2
	}

	defer func() {
		debugPrintError(err)
	}()

	err = s.ListenAndServe()
}

func debugPrintError(err error) {
	if err != nil {
		if gin.IsDebugging() {
			fmt.Fprintf(gin.DefaultErrorWriter, "[GIN-debug] [ERROR] %v\n", err)
		}
	}
}
