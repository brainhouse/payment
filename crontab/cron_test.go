package crontab

import (
	"fmt"
	"funzone_pay/app/service"
	"funzone_pay/boostrap"
	"funzone_pay/model"
	"testing"
)

func TestCrontab(t *testing.T) {
	var ch = make(chan string)
	Start()
	<-ch
}

func TestCollectSettlement(t *testing.T) {
	boostrap.InitConf()
	CollectSettlement()
}

func TestPayuOrderInquiry(t *testing.T) {
	boostrap.InitConf()
	PayuOrderInquiry()
}

func TestPayUPayoutStatus1(t *testing.T) {
	boostrap.InitConf()
	PayoutStatus()
}

func TestPayUPayoutStatus(t *testing.T) {
	boostrap.InitConf()
	payIns, _ := service.PayAccountConfServe.GetSpecificPayInstance(model.PAYU, "payu3")

	payout, err := payIns.InquiryPayout("PU1610685441055856", "")
	fmt.Println(payout)
	fmt.Println(err)
}

func TestLock(t *testing.T) {
	for i := 0; i < 100; i++ {
		go func(i int) {
			PayOrderInquiryLock.Lock()
			defer PayOrderInquiryLock.Unlock()
			fmt.Println(i)
		}(i)
	}
}
