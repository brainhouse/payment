package crontab

import (
	"errors"
	"fmt"
	"funzone_pay/dao"
	"funzone_pay/model"
	"github.com/wonderivan/logger"
	"time"
	"xorm.io/xorm"
)

/**
充值T+2结算
*/
func CollectSettlement() {
	//周末不结算
	if time.Now().Weekday() == time.Saturday || time.Now().Weekday() == time.Sunday {
		logger.Error("CollectSettlement_Ignore | day=%v", time.Now().Weekday())
		return
	}
	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("CollectSettlement_DBError | err=%v", err)
		return
	}
	var list []*model.PayEntry
	timeStr := time.Now().Format("2006-01-02")
	t, _ := time.Parse("2006-01-02", timeStr)
	end := t.Unix() - 86400
	if time.Now().Weekday() == time.Monday {
		//周一是T+4
		end -= 2 * 86400
	}
	start := end - 3*86400
	err = engine.Where("created between ? AND ? AND status=1 AND settle_status=0", start, end).Limit(20000).Find(&list)
	if err != nil {
		logger.Error("CollectSettlement_Query_Error | err=%v", err)
		return
	}
	if len(list) == 0 {
		return
	}
	logger.Debug("S_CollectSettlementStart | len=%v", len(list))
	for _, v := range list {
		if v.Uid == 0 {
			continue
		}
		//开始结算
		_, err := engine.Transaction(func(session *xorm.Session) (i interface{}, e error) {
			var exist bool
			account := new(model.Account)
			exist, e = session.Where("uid=?", v.Uid).ForUpdate().Get(account)
			if e != nil {
				return
			}
			if !exist {
				e = errors.New(fmt.Sprintf("AccountNotExist"))
				return
			}
			settleMoney := v.Amount - v.Fee
			//未结算的不能减到负
			if (account.UnsettledBalance - settleMoney) < 0 {
				e = errors.New(fmt.Sprintf("UnsettledBalanceErr"))
				return
			}
			//修改为已结算
			_, e = session.Where("id=? AND status=1", v.Id).Update(&model.PayEntry{
				SettleStatus: 1,
			})
			if e != nil {
				logger.Debug("CollectSettlement_Settle[%v], Error=%v ", v.OrderId, err)
				return
			}

			//修改用户账户余额
			_, e = session.Where("uid=?", v.Uid).Incr("balance", settleMoney).Decr("unsettled_balance", settleMoney).Update(&model.Account{})
			return
		})
		logger.Debug("CollectSettlement_Transaction | err=%v | data=%v", err, v)
	}
	logger.Debug("S_CollectSettlementEnd | len=%v", len(list))
}
