package crontab

import (
	"fmt"
	"funzone_pay/app/service"
	"funzone_pay/dao"
	"funzone_pay/model"
	"github.com/wonderivan/logger"
	"sync"
	"time"
)

var PayOrderInquiryLock sync.Mutex
var PayCFOrderInquiryLock sync.Mutex
var PayPTMOrderInquiryLock sync.Mutex
var PayFlashOrderInquiryLock sync.Mutex
var PaySerPayOrderInquiryLock sync.Mutex
var OrderStatusInquiryLock sync.Mutex
var AirOrderStatusInquiryLock sync.Mutex
var AirOrderStatusInquiryLock2 sync.Mutex

/*
*
同步未支付的充值单状态
*/
func PayuOrderInquiry() {
	PayOrderInquiryLock.Lock()
	defer PayOrderInquiryLock.Unlock()

	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("PayuOrderInquiry_DBError | err=%v", err)
		return
	}
	var list []*model.PayEntry
	//timeStr := time.Now().Format("2006-01-02")
	//t, _ := time.Parse("2006-01-02", timeStr)
	end := time.Now().Unix() - 3600
	start := end - 20*86400
	err = engine.Where("created between ? AND ? AND status=0", start, end).In("pay_channel", model.PAYU).OrderBy("updated").Limit(5000).Find(&list)
	if err != nil {
		logger.Error("PayuOrderInquiry_Query_Error | err=%v", err)
		return
	}
	if len(list) == 0 {
		return
	}
	logger.Debug("PayuOrderInquiry_Info | len=%v", len(list))
	for _, v := range list {
		if v.AppId == "rummy" {
			_, v.PayAccountId = service.PayAccountConfServe.GetPayChannelAccountId(v.AppId, v.PayChannel, "order")
		}
		payInstance, _ := service.PayAccountConfServe.GetSpecificPayInstance(v.PayChannel, v.PayAccountId)
		data, err := payInstance.Inquiry(v.OrderId, "")
		if err != nil {
			logger.Error("PayuOrderInquiry_Inquiry_Error | err=%v | data=%v", err, v)
			continue
		}
		if data.Status == model.PAYING {
			continue
		}
		_, err = engine.Where("id=? AND status=0", data.Id).Update(&model.PayEntry{
			Status:     data.Status,
			PaymentId:  data.PaymentId,
			ThirdDesc:  data.ThirdDesc,
			FinishTime: time.Now().Unix(),
		})
		if err != nil {
			logger.Error("PayuOrderInquiry_Update_Error | err=%v | data=%v", err, v)
			continue
		}
		go service.GetPayEntryService().SendEntryCallback(data)
	}
}

func CFOrderInquiry() {
	PayCFOrderInquiryLock.Lock()
	defer PayCFOrderInquiryLock.Unlock()

	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("CFOrderInquiry_DBError | err=%v", err)
		return
	}
	var list []*model.PayEntry
	//timeStr := time.Now().Format("2006-01-02")
	//t, _ := time.Parse("2006-01-02", timeStr)
	end := time.Now().Unix() - 1800
	start := end - 2*86400
	err = engine.Where("created between ? AND ? AND status=0 AND pay_channel=?", start, end, model.CASHFREE).OrderBy("updated").Limit(5000).Find(&list)
	if err != nil {
		logger.Error("CFOrderInquiry_Query_Error | err=%v", err)
		return
	}
	if len(list) == 0 {
		return
	}
	logger.Debug("CFOrderInquiry_Start | len=%v", len(list))
	for _, v := range list {
		if v.AppId == "rummy" && v.PayChannel == model.CASHFREE {
			continue
		}
		if v.AppId == "rummy" {
			_, v.PayAccountId = service.PayAccountConfServe.GetPayChannelAccountId(v.AppId, v.PayChannel, "order")
			logger.Error("CFOrderInquiry_Inquiry_Debug | err=%v | data=%v", err, v)
		}
		payInstance, _ := service.PayAccountConfServe.GetSpecificPayInstance(v.PayChannel, v.PayAccountId)
		data, err := payInstance.Inquiry(v.OrderId, "")
		if err != nil {
			logger.Error("CFOrderInquiry_Inquiry_Error | err=%v | data=%v", err, v)
			continue
		}
		if data.Status == model.PAYING {
			continue
		}
		rowNum, err := engine.Where("id=? AND status=0", v.Id).Cols("status,payment_id,third_desc,finish_time").Update(&model.PayEntry{
			Status:     data.Status,
			PaymentId:  data.PaymentId,
			ThirdDesc:  data.ThirdDesc,
			FinishTime: data.FinishTime,
		})
		if err != nil {
			logger.Error("CFOrderInquiry_Update_Error | err=%v | data=%v", err, v)
			continue
		}
		logger.Error("CFOrderInquiry_SendEntryCallback_Data | data=%v | row=%v", data, rowNum)
		go service.GetPayEntryService().SendEntryCallback(data)
	}
	logger.Debug("CFOrderInquiry_End | len=%v", len(list))
}

func OnionOrderInquiry() {
	PayCFOrderInquiryLock.Lock()
	defer PayCFOrderInquiryLock.Unlock()

	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("OnionOrderInquiry_DBError | err=%v", err)
		return
	}
	var list []*model.PayEntry
	//timeStr := time.Now().Format("2006-01-02")
	//t, _ := time.Parse("2006-01-02", timeStr)
	end := time.Now().Unix() - 1800
	start := end - 2*86400
	err = engine.Where("created between ? AND ? AND status=0 AND pay_channel=?", start, end, model.ONIONPAY).OrderBy("updated").Limit(5000).Find(&list)
	if err != nil {
		logger.Error("OnionOrderInquiry_Query_Error | err=%v", err)
		return
	}
	if len(list) == 0 {
		return
	}
	logger.Debug("OnionOrderInquiry_Start | len=%v", len(list))
	for _, v := range list {
		if v.AppId == "rummy" && v.PayChannel == model.ONIONPAY {
			continue
		}
		if v.AppId == "rummy" {
			_, v.PayAccountId = service.PayAccountConfServe.GetPayChannelAccountId(v.AppId, v.PayChannel, "order")
			logger.Error("OnionOrderInquiry_Inquiry_Debug | err=%v | data=%v", err, v)
		}
		payInstance, _ := service.PayAccountConfServe.GetSpecificPayInstance(v.PayChannel, v.PayAccountId)
		data, err := payInstance.Inquiry(v.OrderId, "")
		if err != nil {
			logger.Error("OnionOrderInquiry_Inquiry_Error | err=%v | data=%v", err, v)
			continue
		}
		if data.Status == model.PAYING {
			continue
		}
		rowNum, err := engine.Where("id=? AND status=0", v.Id).Cols("status,payment_id,third_desc,finish_time").Update(&model.PayEntry{
			Status:     data.Status,
			PaymentId:  data.PaymentId,
			ThirdDesc:  data.ThirdDesc,
			FinishTime: time.Now().Unix(),
		})
		if err != nil {
			logger.Error("OnionOrderInquiry_Update_Error | err=%v | data=%v", err, v)
			continue
		}
		logger.Error("OnionOrderInquiry_SendEntryCallback_Data | data=%v | row=%v", data, rowNum)
		go service.GetPayEntryService().SendEntryCallback(data)
	}
	logger.Debug("OnionOrderInquiry_End | len=%v", len(list))
}

func PayFlashOrderInquiry() {
	PayFlashOrderInquiryLock.Lock()
	defer PayFlashOrderInquiryLock.Unlock()

	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("PayFlashOrderInquiry_DBError | err=%v", err)
		return
	}
	var list []*model.PayEntry
	//timeStr := time.Now().Format("2006-01-02")
	//t, _ := time.Parse("2006-01-02", timeStr)
	end := time.Now().Unix() - 1800
	start := end - 2*86400
	err = engine.Where("created between ? AND ? AND status=0 AND pay_channel=?", start, end, model.PAYFLASH).OrderBy("updated").Limit(5000).Find(&list)
	if err != nil {
		logger.Error("PayFlashOrderInquiry_Query_Error | err=%v", err)
		return
	}
	if len(list) == 0 {
		return
	}
	logger.Debug("PayFlashOrderInquiry_Start | len=%v", len(list))
	for _, v := range list {
		payInstance, _ := service.PayAccountConfServe.GetSpecificPayInstance(v.PayChannel, v.PayAccountId)
		data, err := payInstance.Inquiry(v.OrderId, "")
		if err != nil {
			logger.Error("PayFlashOrderInquiry_Inquiry_Error | err=%v | data=%v", err, v)
			continue
		}
		if data.Status == model.PAYING {
			continue
		}
		rowNum, err := engine.Where("id=? AND status=0", v.Id).Cols("status,payment_id,third_code,third_desc,finish_time").Update(&model.PayEntry{
			Status:     data.Status,
			PaymentId:  data.PaymentId,
			ThirdCode:  data.ThirdCode,
			ThirdDesc:  data.ThirdDesc,
			FinishTime: time.Now().Unix(),
		})
		if err != nil {
			logger.Error("PayFlashOrderInquiry_Update_Error | err=%v | data=%v", err, v)
			continue
		}
		logger.Debug("PayFlashOrderInquiry_SendEntryCallback_Data | data=%v | row=%v", data, rowNum)
		go service.GetPayEntryService().SendEntryCallback(data)
	}
	logger.Debug("PayFlashOrderInquiry_End | len=%v", len(list))
}

func PaySerPayOrderInquiry() {
	PaySerPayOrderInquiryLock.Lock()
	defer PaySerPayOrderInquiryLock.Unlock()

	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("PaySerPayOrderInquiry_DBError | err=%v", err)
		return
	}
	var list []*model.PayEntry
	//timeStr := time.Now().Format("2006-01-02")
	//t, _ := time.Parse("2006-01-02", timeStr)
	// 只检测 now-36000 < created < now-1800
	// 超过这个时间的订单不再从三方服务器获取订单状态
	end := time.Now().Unix() - 1800
	start := end - 36000
	err = engine.Where("created between ? AND ? AND status=0 AND pay_channel=?", start, end, model.SERPAY).OrderBy("updated").Limit(5000).Find(&list)
	if err != nil {
		logger.Error("PaySerPayOrderInquiry_Query_Error | err=%v", err)
		return
	}
	if len(list) == 0 {
		return
	}
	logger.Debug("PaySerPayOrderInquiry_Start | len=%v", len(list))
	for _, v := range list {
		payInstance, _ := service.PayAccountConfServe.GetSpecificPayInstance(v.PayChannel, v.PayAccountId)
		data, err := payInstance.Inquiry(v.OrderId, "")
		if err != nil {
			logger.Error("PaySerPayOrderInquiry_Inquiry_Error | err=%v | data=%v", err, v)
			continue
		}
		if data.Status == model.PAYING {
			continue
		}

		rowNum, err := engine.Where("id=? AND status=0", v.Id).Cols("status,payment_id,third_code,third_desc,finish_time").Update(&model.PayEntry{
			Status:     data.Status,
			PaymentId:  data.PaymentId,
			ThirdCode:  data.ThirdCode,
			ThirdDesc:  data.ThirdDesc,
			FinishTime: time.Now().Unix(),
		})
		if err != nil {
			logger.Error("PaySerPayOrderInquiry_Update_Error | err=%v | data=%v", err, v)
			continue
		}
		logger.Debug("PaySerPayOrderInquiry_SendEntryCallback_Data | data=%v | row=%v", data, rowNum)
		go service.GetPayEntryService().SendEntryCallback(data)
	}
	logger.Debug("PaySerPayOrderInquiry_End | len=%v", len(list))
}

func InitPhonePayStatusRoutine() {
	go func() {
		time.Sleep(time.Second * 30)
		defer func() {
			if r := recover(); r != nil {
				logger.Error("recovered from ", r)
			}
		}()

		var lastTic int64 = 0
		for {
			n := time.Now().Unix()
			if n-lastTic < 300 {
				time.Sleep(time.Second)
				continue
			}

			lastTic = n
			phonePayCheckStatus()
		}
	}()
}

var phonePayCheckCount int64 = 0
var phonePayMaxSecond int64 = 1800
var phonePayMaxExpired int64 = 1200

func phonePayCheckStatus() {
	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("PhonePayOrderInquiry_DBError | err=%v", err)
		return
	}
	var list []*model.PayEntry

	unixSec := time.Now().Unix()
	end := unixSec - 20
	start := end - phonePayMaxSecond - phonePayMaxSecond
	phonePayCheckCount += 1

	err = engine.Where("created between ? AND ? AND status=0 AND pay_channel=?", start, end, model.PhonePay).OrderBy("updated").Limit(1000).Find(&list)
	if err != nil {
		logger.Error("PhonePayOrderInquiry_Query_Error | err=%v", err)
		return
	}
	if len(list) == 0 {
		return
	}
	logger.Debug("PhonePayPayOrderInquiry_Start | len=%v", len(list))
	for _, v := range list {
		payInstance, _ := service.PayAccountConfServe.GetSpecificPayInstance(v.PayChannel, v.PayAccountId)
		paymentOrderId := v.PaymentOrderId

		//interval := unixSec - v.Created
		/*
		If the payment status is Pending, then Check Status API should be called in the following interval:
		The first status check at 20-25 seconds post transaction start, then
		Every 3 seconds once for the next 30 seconds,
		Every 6 seconds once for the next 60 seconds,
		Every 10 seconds for the next 60 seconds,
		Every 30 seconds for the next 60 seconds, and then
		Every 1 min until timeout (15 mins).
		*/
		// 60, 120, 180, 240,
		//if interval >= 240 && phonePayCheckCount%20 != 0 ||
		//	interval >= 180 && interval < 240 && phonePayCheckCount%10 != 0 ||
		//	interval >= 120 && interval < 180 && phonePayCheckCount%3 != 0 ||
		//	interval >= 60 && interval < 120 && phonePayCheckCount%2 != 0 {
		//	continue
		//}

		data, err := payInstance.Inquiry(v.OrderId, paymentOrderId)
		if err != nil {
			//logger.Error("PhonePayPayOrderInquiry_Inquiry_Error | err=%v | data=%v", err, v)
			continue
		}

		expired := false
		unix := time.Now().Unix()
		if data.Created > 0 && unix-data.Created > phonePayMaxExpired {
			expired = true
		}

		if data.Status == model.PAYING {
			if !expired {
				continue
			}

			// 超时失败
			data.Status = model.PAYFAILD
			data.ThirdDesc = fmt.Sprintf("order expired %v", phonePayMaxExpired)
		}

		rowNum, err := engine.Where("id=?", v.Id).Cols("status,payment_id,third_code,third_desc,finish_time").Update(&model.PayEntry{
			Status:     data.Status,
			PaymentId:  data.PaymentId,
			ThirdCode:  data.ThirdCode,
			ThirdDesc:  data.ThirdDesc,
			FinishTime: unix,
		})
		if err != nil {
			logger.Error("PhonePayOrderInquiry_Update_Error | err=%v | data=%v", err, v)
			continue
		}
		logger.Debug("PhonePayOrderInquiry_SendEntryCallback_Data | data=%v | row=%v", data, rowNum)
		go service.GetPayEntryService().SendEntryCallback(data)
	}
	logger.Debug("PhonePayPayOrderInquiry_End | len=%v", len(list))
}

const ThreeDayOff = 86400 * 3

type InquiryOrder struct {
	Ch model.PayChannel
}

func (io *InquiryOrder) GetTime() (start, end int64) {
	if io.Ch == model.RAZORPAY {
		end = time.Now().Unix() - 1800
		return end - ThreeDayOff, end
	}

	return 0, 0
}

var inquirys = []InquiryOrder{
	{model.ZWPAY},
	{model.YBPAY},
	{model.LOOGPAY},
	{model.YDGPAY},
	//{model.UPPAY},
	{model.CASHPAY},
	{model.HOPEPAY},
}

func PayOrderInquiry() {
	OrderStatusInquiryLock.Lock()
	defer OrderStatusInquiryLock.Unlock()

	for _, item := range inquirys {
		start, end := item.GetTime()
		orderStatusSync(item.Ch, start, end)
	}
}

var maxExpiredTime int64 = 86400 * 2
var maxQueryTime int64 = 86400 * 3

func orderStatusSync(ch model.PayChannel, start, end int64) {
	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("Pay%vOrderInquiry_DBError | err=%v", ch, err)
		return
	}
	var list []*model.PayEntry
	//timeStr := time.Now().Format("2006-01-02")
	//t, _ := time.Parse("2006-01-02", timeStr)
	// 只检测 now-36000 < created < now-1800
	// 超过这个时间的订单不再从三方服务器获取订单状态
	if end == 0 {
		end = time.Now().Unix() - 1800
		start = end - 72000
	}

	hour := time.Now().Hour()
	// 北京时间5:00，印度时间2:30，伦敦时间21点,到北京时间7点,印度时间4:30,伦敦时间23,查询3天以内订单
	if hour >= 21 && hour <= 23 {
		start = end - maxQueryTime
	}

	date := time.Now().Format("02-01-2006")
	err = engine.Where("created between ? AND ? AND status=0 AND pay_channel=?", start, end, ch).OrderBy("updated").Limit(5000).Find(&list)
	if err != nil {
		logger.Error("Pay%vOrderInquiry_Query_Error | err=%v", ch, err)
		return
	}
	if len(list) == 0 {
		return
	}
	logger.Debug("Pay%vPayOrderInquiry_Start | len=%v", ch, len(list))
	for _, v := range list {
		payInstance, _ := service.PayAccountConfServe.GetSpecificPayInstance(v.PayChannel, v.PayAccountId)
		paymentOrderId := v.PaymentOrderId
		if v.PayChannel == model.WDLPAY {
			paymentOrderId = date
		}
		data, err := payInstance.Inquiry(v.OrderId, paymentOrderId)
		if err != nil {
			logger.Error("Pay%vPayOrderInquiry_Inquiry_Error | err=%v | data=%v", ch, err, v)
			continue
		}

		expired := false
		unix := time.Now().Unix()
		if data.Created > 0 && unix-data.Created > maxExpiredTime {
			expired = true
		}

		if data.Status == model.PAYING {
			if !expired {
				continue
			}

			// 超时失败
			data.Status = model.PAYFAILD
			data.ThirdDesc = fmt.Sprintf("order expired 24*3hour")
		}

		rowNum, err := engine.Where("id=?", v.Id).Cols("status,payment_id,third_code,third_desc,finish_time").Update(&model.PayEntry{
			Status:     data.Status,
			PaymentId:  data.PaymentId,
			ThirdCode:  data.ThirdCode,
			ThirdDesc:  data.ThirdDesc,
			FinishTime: unix,
		})
		if err != nil {
			logger.Error("Pay%vPayOrderInquiry_Update_Error | err=%v | data=%v", ch, err, v)
			continue
		}
		logger.Debug("Pay%vPayOrderInquiry_SendEntryCallback_Data | data=%v | row=%v", ch, data, rowNum)
		go service.GetPayEntryService().SendEntryCallback(data)
	}
	logger.Debug("Pay%vPayOrderInquiry_End | len=%v", ch, len(list))
}

var airpayCount int64 = 0
var lastCheckLongDay int = 0
var daySecond int64 = 86400

// 特殊处理airpay 的订单检测

func AirPayOrderInquiry() {
	AirOrderStatusInquiryLock.Lock()
	defer AirOrderStatusInquiryLock.Unlock()

	ch := model.AIRPAY
	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("Pay%vOrderInquiry_DBError | err=%v", ch, err)
		return
	}

	// 超过这个时间的订单不再从三方服务器获取订单状态

	now := time.Now()
	end := now.Unix() - 600 // 10 min 之前的订单
	start := end - 1800

	if airpayCount%16 == 0 {
		start = end - daySecond*3 //  1天
	} else if airpayCount%8 == 0 {
		start = end - 36000
	} else if airpayCount%4 == 0 {
		start = end - 7200
	} else if airpayCount%2 == 0 {
		start = end - 3600
	}

	airpayCount += 1

	pendingId := int64(0)
	inquiryCount := 0

	for {
		list := make([]*model.PayEntry, 0, 1024)
		err = engine.Where("created between ? AND ? AND id > ? AND status=0 AND pay_channel=?", start, end, pendingId, ch).OrderBy("id").Limit(1024).Find(&list)
		if err != nil {
			logger.Error("Pay%vOrderInquiry_Query_Error | err=%v", ch, err)
			break
		}
		if len(list) == 0 {
			break
		}
		inquiryCount += len(list)
		logger.Debug("Pay%vPayOrderInquiry_Start | len=%v", ch, inquiryCount)
		for _, v := range list {
			pendingId = v.Id
			payInstance, _ := service.PayAccountConfServe.GetSpecificPayInstance(v.PayChannel, v.PayAccountId)
			data, err := payInstance.Inquiry(v.OrderId, v.PaymentOrderId)
			if err != nil {
				logger.Error("Pay%vPayOrderInquiry_Inquiry_Error | err=%v | data=%v", ch, err, v)
				continue
			}
			//expired := false
			//unix := time.Now().Unix()
			//if data.Created > 0 && unix-data.Created > maxExpiredTime {
			//	expired = true
			//}

			if data.Status == model.PAYING {
				continue
				//if !expired {
				//	continue
				//}
				//
				//// 超时失败
				//data.Status = model.PAYFAILD
				//data.ThirdDesc = fmt.Sprintf("order expired 24*3hour")
			}

			rowNum, err := engine.Where("id=? AND status=0", v.Id).Cols("status,payment_id,third_code,third_desc,finish_time").Update(&model.PayEntry{
				Status:     data.Status,
				PaymentId:  data.PaymentId,
				ThirdCode:  data.ThirdCode,
				ThirdDesc:  data.ThirdDesc,
				FinishTime: time.Now().Unix(),
			})
			if err != nil {
				logger.Error("Pay%vPayOrderInquiry_Update_Error | err=%v | data=%v", ch, err, v)
				continue
			}
			logger.Debug("Pay%vPayOrderInquiry_SendEntryCallback_Data | data=%v|%v | row=%v", ch, data.OrderId, data.Status, rowNum)
			go service.GetPayEntryService().SendEntryCallback(data)
		}
	}

	logger.Debug("Pay%vPayOrderInquiry_End | total=%v", ch, inquiryCount)
}

var id int64 = 15948638
var AirPayCheckOrderStatusInquiryLock sync.Mutex

func AirPayCheckFailOrder() {
	AirPayCheckOrderStatusInquiryLock.Lock()
	defer AirPayCheckOrderStatusInquiryLock.Unlock()

	ch := model.AIRPAY
	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("AirPayCheckOrderInquiry_DBError | err=%v", ch, err)
		return
	}

	now := time.Now()
	end := now.Unix() - 86400*2
	start := end - 86400*7

	var list []*model.PayEntry
	err = engine.Where("id > ? AND created between ? AND ? AND status=2 AND pay_channel=?", id, start, end, ch).OrderBy("id").Limit(2000).Find(&list)
	if err != nil {
		logger.Error("AirPayCheckOrderInquiry_Query_Error | err=%v", ch, err)
		return
	}
	if len(list) == 0 {
		return
	}
	logger.Debug("AirPayCheckPayOrderInquiry_Start | len=%v", ch, len(list))
	for _, v := range list {
		id = v.Id

		payInstance, _ := service.PayAccountConfServe.GetSpecificPayInstance(v.PayChannel, v.PayAccountId)
		data, err := payInstance.Inquiry(v.OrderId, v.PaymentOrderId)
		if err != nil {
			logger.Error("AirPayCheckPayOrderInquiry_Inquiry_Error | err=%v | data=%v", ch, err, v)
			continue
		}

		if data.Status != model.PAYSUCCESS {
			continue
		}

		rowNum, err := engine.Where("id=? AND status=2", v.Id).Cols("status,payment_id,third_code,third_desc,finish_time").Update(&model.PayEntry{
			Status:     data.Status,
			PaymentId:  data.PaymentId,
			ThirdCode:  "",
			ThirdDesc:  "",
			FinishTime: time.Now().Unix(),
		})
		if err != nil {
			logger.Error("AirPayCheckPayOrderInquiry_Update_Error | err=%v | data=%v", ch, err, v)
			continue
		}
		logger.Debug("AirPayCheckPayOrderInquiry_SendEntryCallback_Data | data=%v | row=%v", ch, data.OrderId, rowNum)
		go service.GetPayEntryService().SendEntryCallback(data)
	}
	logger.Debug("AirPayCheckPayOrderInquiry_End | len=%v | id=%d", ch, len(list), id)
}

var airPayCheckOld = false
var lastCheckOld int64 = 0

func AirPayOrderInquiryMoreThan24() {
	if airPayCheckOld {
		return
	}

	AirOrderStatusInquiryLock2.Lock()
	airPayCheckOld = true

	defer func() {
		airPayCheckOld = false
		lastCheckOld = time.Now().Unix()
		AirOrderStatusInquiryLock2.Unlock()
	}()

	nowUnix := time.Now().Unix()
	if nowUnix-lastCheckOld < 3600*3 {
		return
	}

	end := nowUnix - daySecond - 3600
	start := end - daySecond*3

	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("AirPayOrderInquiry_DBError | err=%v", err)
		return
	}

	pendingId := int64(0)
	inquiryCount := 0

	for {
		list := make([]*model.PayEntry, 0, 1024)
		err = engine.Where("created between ? AND ? AND id > ? AND status=0 AND pay_channel=?", start, end, pendingId, model.AIRPAY).Asc("id").Limit(1024).Find(&list)
		if err != nil {
			logger.Error("OlderAirPayOrderInquiry_Query_Error | err=%v", err)
			break
		}
		if len(list) == 0 {
			break
		}
		inquiryCount += len(list)
		logger.Debug("OlderAirPayOrderInquiry_Start | len=%v", inquiryCount)
		for _, v := range list {
			pendingId = v.Id
			payInstance, _ := service.PayAccountConfServe.GetSpecificPayInstance(v.PayChannel, v.PayAccountId)
			data, err := payInstance.Inquiry(v.OrderId, v.PaymentOrderId)
			if err != nil {
				logger.Error("OlderAirPayOrderInquiry_Inquiry_Error | err=%v | data=%v", err, v)
				continue
			}
			//expired := false
			//unix := time.Now().Unix()
			//if data.Created > 0 && unix-data.Created > maxExpiredTime {
			//	expired = true
			//}

			if data.Status == model.PAYING {
				continue
				//if !expired {
				//	continue
				//}
				//
				//// 超时失败
				//data.Status = model.PAYFAILD
				//data.ThirdDesc = fmt.Sprintf("order expired 24*3hour")
			}

			rowNum, err := engine.Where("id=? AND status=0", v.Id).Cols("status,payment_id,third_code,third_desc,finish_time").Update(&model.PayEntry{
				Status:     data.Status,
				PaymentId:  data.PaymentId,
				ThirdCode:  data.ThirdCode,
				ThirdDesc:  data.ThirdDesc,
				FinishTime: time.Now().Unix(),
			})
			if err != nil {
				logger.Error("OlderAirPayOrderInquiry_Update_Error | err=%v | data=%v", err, v)
				continue
			}
			logger.Debug("OlderAirPayOrderInquiry_SendEntryCallback_Data | data=%v|%v | row=%v", data.OrderId, data.Status, rowNum)
			go service.GetPayEntryService().SendEntryCallback(data)
		}
	}

	logger.Debug("OlderAirPayOrderInquiry_End | total=%v", inquiryCount)
}

func PTMOrderInquiry() {
	PayPTMOrderInquiryLock.Lock()
	defer PayPTMOrderInquiryLock.Unlock()

	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("PTMOrderInquiry_DBError | err=%v", err)
		return
	}
	var list []*model.PayEntry
	//timeStr := time.Now().Format("2006-01-02")
	//t, _ := time.Parse("2006-01-02", timeStr)
	end := time.Now().Unix() - 3600
	start := end - 30*86400
	err = engine.Where("created between ? AND ? AND status=0 AND pay_channel=?", start, end, model.PAYTM).OrderBy("updated").Limit(5000).Find(&list)
	if err != nil {
		logger.Error("PTMOrderInquiry_Query_Error | err=%v", err)
		return
	}
	if len(list) == 0 {
		return
	}
	logger.Debug("PTMOrderInquiry_Info | len=%v", len(list))
	for _, v := range list {
		if v.AppId == "rummy" {
			_, v.PayAccountId = service.PayAccountConfServe.GetPayChannelAccountId(v.AppId, v.PayChannel, "order")
			logger.Error("PTMOrderInquiry_Inquiry_Debug | err=%v | data=%v", err, v)
		}
		payInstance, _ := service.PayAccountConfServe.GetSpecificPayInstance(v.PayChannel, v.PayAccountId)
		data, err := payInstance.Inquiry("", v.OrderId)
		if err != nil {
			logger.Error("PTMOrderInquiry_Inquiry_Error | err=%v | data=%v", err, v)
			continue
		}
		if data.Status == model.PAYING {
			continue
		}
		_, err = engine.Where("id=? AND status=0", data.Id).Update(&model.PayEntry{
			Status:     data.Status,
			PaymentId:  data.PaymentId,
			ThirdDesc:  data.ThirdDesc,
			FinishTime: time.Now().Unix(),
		})
		if err != nil {
			logger.Error("PTMOrderInquiry_Update_Error | err=%v | data=%v", err, v)
			continue
		}
		go service.GetPayEntryService().SendEntryCallback(data)
	}
}

/*
*
razorpay支付状态同步
*/
func RazorPayOrderInquiry() {
	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("RazorPayOrderInquiry_DBError | err=%v", err)
		return
	}
	var list []*model.PayEntry
	timeStr := time.Now().Format("2006-01-02")
	t, _ := time.Parse("2006-01-02", timeStr)
	end := t.Unix()
	start := end - 3*86400
	err = engine.Where("created between ? AND ? AND status=0 AND pay_channel=?", start, end, model.RAZORPAY).OrderBy("updated").Limit(1000).Find(&list)
	if err != nil {
		logger.Error("RazorPayOrderInquiry_Query_Error | err=%v", err)
		return
	}
	if len(list) == 0 {
		return
	}
	logger.Debug("RazorPayOrderInquiry_Info | len=%v", len(list))
	for _, v := range list {
		payInstance, _ := service.PayAccountConfServe.GetSpecificPayInstance(v.PayChannel, v.PayAccountId)
		data, err := payInstance.Inquiry(v.OrderId, "")
		if err != nil {
			logger.Error("RazorPayOrderInquiry_Inquiry_Error | err=%v | data=%v", err, v)
			continue
		}
		_, err = engine.Where("id=?", data.Id).Update(&model.PayEntry{
			Status:    data.Status,
			PaymentId: data.PaymentId,
		})
		if err != nil {
			logger.Error("PayuOrderInquiry_Update_Error | err=%v | data=%v", err, v)
			continue
		}
		if data.Status == model.PAYING {
			continue
		}
		service.GetPayEntryService().SendEntryCallback(data)
	}
}
