package crontab

import (
	"fmt"
	"funzone_pay/utils"
	"time"

	"github.com/robfig/cron"
)

var CronTab *cron.Cron

func Start() {
	if utils.GetLocalIp() != "172.31.35.219" {
		return
	}
	loc, _ := time.LoadLocation("Asia/Shanghai")
	CronTab = cron.NewWithLocation(loc)
	//_ = CronTab.AddFunc("0 */1 * * * *", PayTmPayoutStatus)
	//定时结算
	//_ = CronTab.AddFunc("0 0 17 * * *", CollectSettlement)
	//payu充值状态同步
	_ = CronTab.AddFunc("0 */5 * * * *", PayuOrderInquiry)
	//cashfree充值状态同步
	_ = CronTab.AddFunc("0 */10 * * * *", CFOrderInquiry)
	//paytm充值状态同步
	_ = CronTab.AddFunc("0 */5 * * * *", PTMOrderInquiry)
	_ = CronTab.AddFunc("0 */5 * * * *", OnionOrderInquiry)
	_ = CronTab.AddFunc("0 */5 * * * *", PayFlashOrderInquiry)
	_ = CronTab.AddFunc("0 */5 * * * *", PaySerPayOrderInquiry)
	_ = CronTab.AddFunc("0 */10 * * * *", PayOrderInquiry) // 新的充值订单异步查询添加到这里
	_ = CronTab.AddFunc("0 */10 * * * *", AirPayOrderInquiry)
	//_ = CronTab.AddFunc("0 */5 * * * *", AirPayCheckFailOrder)
	_ = CronTab.AddFunc("0 */10 * * * *", AirPayOrderInquiryMoreThan24)
	//payu支付状态同步
	_ = CronTab.AddFunc("0 */5 * * * *", PayoutStatus)
	_ = CronTab.AddFunc("0 */5 * * * *", PayoutCheckExpired100day)
	//callback充值回调检查重发
	_ = CronTab.AddFunc("0 */1 * * * *", CheckPayEntryCallback)
	//razorpay充值状态同步
	//_ = CronTab.AddFunc("0 0 */1 * * *", RazorPayOrderInquiry)
	_ = CronTab.AddFunc("0 */10 * * * *", CheckPayoutData)
	CronTab.Start()
	fmt.Printf("cron table start : %v", utils.GetLocalIp())
}
