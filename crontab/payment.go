package crontab

import (
	"fmt"
	"funzone_pay/app/service"
	"funzone_pay/common"
	"funzone_pay/dao"
	"funzone_pay/model"
	"github.com/wonderivan/logger"
	"time"
)

//var PayoutStatusLock sync.Mutex

//func PayTmPayoutStatus() {
//	engine, err := dao.GetMysql()
//	if err != nil {
//		logger.Error("PayTmPayoutStatus_DBError | err=%v", err)
//		return
//	}
//	var list []*model.PayOut
//	err = engine.Where("pay_channel=? AND created>? AND status=0", model.PT_PayTm, time.Now().Unix()-86400).Limit(10).OrderBy("updated").Find(&list)
//	if err != nil {
//		logger.Error("PayTmPayoutStatus_Query_Error | err=%v", err)
//		return
//	}
//	for _, v := range list {
//		logger.Debug("PayTmPayoutStatus_Info | data=%v", v)
//		payOut, err := channel.GetPayInstance(model.PAYTM).InquiryPayout(v.OrderId)
//		if err != nil {
//			logger.Error("PayTmPayoutStatus_Error | data=%v | err=%v", v, err)
//			continue
//		}
//		if v.Status != model.PAY_OUT_ING {
//			logger.Error("PayTmPayoutStatus_Query_Repeat | err=%v | data=%v", err, v)
//			continue
//		}
//		if payOut.Status != model.PAY_OUT_ING {
//			//发起回调
//			v.Status = payOut.Status
//			//go service.GetPayOutService().SendPayOutCallback(v)
//		}
//	}
//}

func PayoutStatus() {

	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("CRON_PayUPayoutStatus_DBError | err=%v", err)
		return
	}
	var list []*model.PayOut
	unix := time.Now().Unix()
	err = engine.Where("created between ? AND ? AND status=0", time.Now().Unix()-7*86400, time.Now().Unix()-600).
		In("pay_channel", model.PAYU, model.RAZORPAY, model.CASHFREE, model.EASEBUZZ, model.ONIONPAY, model.SERPAY, model.PAYFLASH, model.ZWPAY, model.FMPAY, model.YBPAY, model.YDGPAY, model.OEPAY, model.OPAY, model.HOPEPAY, model.HDPAY, model.CASHPAY).
		Limit(2000).
		OrderBy("updated").
		Find(&list)

	if err != nil {
		logger.Error("CRON_PayoutStatus_Query_Error | err=%v", err)
		return
	}
	if len(list) == 0 {
		return
	}
	for _, v := range list {
		payIns, err := service.PayAccountConfServe.GetSpecificPayInstance(v.PayChannel, v.PayAccountId)
		if err != nil {
			logger.Error("CRON_PayoutStatus_Error | data=%v | err=%v", v, err)
			continue
		}
		logger.Debug("CRON_PayoutStatus_Data | data=%v", v)
		//再查下状态
		//payout := new(PayOut)
		//engine.Where("order_id=?", v.OrderId).Get(payout)
		payout, err := payIns.InquiryPayout(v.OrderId, v.PaymentId)
		if err != nil {
			logger.Error("CRON_PayoutStatus_Update_Error | err=%v | data=%v", err, v)
			continue
		}
		if payout.Status == model.PAY_OUT_ING {
			if unix-payout.Created > 432000 { // pending > 5天，通知一下
				common.PushAlarmEvent(common.Warning, fmt.Sprintf("%v", v.Uid),
					fmt.Sprintf("%v-check-expired", unix), "", fmt.Sprintf("Payout[%v-%v] Order[%v] May Falied, Please Check!! > 5day", v.Uid, v.PayAccountId, v.OrderId))
			}
			continue
		}
		_, err = engine.Where("id=? AND status=0", payout.Id).Update(&model.PayOut{
			Status:     payout.Status,
			FinishTime: time.Now().Unix(),
			PaymentId:  payout.PaymentId,
		})
		if err != nil {
			logger.Error("CRON_PayoutStatus_Update_Error | err=%v | data=%v", err, v)
			continue
		}
		go service.GetPayOutService().SendPayOutCallback(payout)
	}
}

// 检测1个月之前的pending状态的订单

func PayoutCheckExpired100day() {
	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("CRON_PayoutCheckExpiredStatus_DBError | err=%v", err)
		return
	}
	var list []*model.PayOut

	tm := time.Now().Unix() - 86400*30 // 30天前
	err = engine.Where("created > 100 AND created < ?  AND  status=0 AND uid > 0", tm).
		In("pay_channel", model.PAYU, model.RAZORPAY, model.CASHFREE, model.EASEBUZZ, model.ONIONPAY, model.SERPAY, model.PAYFLASH, model.ZWPAY, model.FMPAY, model.YBPAY, model.YDGPAY, model.OEPAY, model.OPAY, model.HOPEPAY, model.HDPAY, model.CASHPAY).
		Limit(100).
		OrderBy("updated").
		Find(&list)

	if err != nil {
		logger.Error("CRON_PayoutCheckExpiredStatus_Query_Error | err=%v", err)
		return
	}
	if len(list) == 0 {
		return
	}
	for _, v := range list {
		var payout *model.PayOut = nil
		payIns, err := service.PayAccountConfServe.GetSpecificPayInstance(v.PayChannel, v.PayAccountId)
		if err == nil {
			payout, _ = payIns.InquiryPayout(v.OrderId, v.PaymentId)
			if payout == nil {
				payout = v
				payout.Status = model.PAY_OUT_FAILD
				payout.ThirdDesc = "expired 30 day and query error"
			}
		} else {
			payout = v
			payout.Status = model.PAY_OUT_FAILD
			payout.ThirdDesc = "expired 30 day and no channel"
		}
		logger.Debug("CRON_PayoutCheckExpiredStatus_Data | data=%v", v)

		if payout.Status == model.PAY_OUT_ING {
			payout.Status = model.PAY_OUT_FAILD
			payout.ThirdDesc = "expired 30 day"
		}
		_, err = engine.Where("id=? AND status=0", payout.Id).Update(&model.PayOut{
			Status:     payout.Status,
			FinishTime: time.Now().Unix(),
			PaymentId:  payout.PaymentId,
			ThirdDesc:  payout.ThirdDesc,
		})
		if err != nil {
			logger.Error("CRON_PayoutCheckExpiredStatus_Update_Error | err=%v | data=%v", err, v)
			continue
		}
		go service.GetPayOutService().SendPayOutCallback(payout)
	}
}
