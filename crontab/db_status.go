package crontab

import (
	"fmt"
	"funzone_pay/common"
	"funzone_pay/dao"
	"funzone_pay/model"
	"github.com/wonderivan/logger"
	"time"
)

var diff int64 = -1
var minID int64 = -1

// CheckPayoutData 检测数据是否有丢失
func CheckPayoutData() {
	var message string = ""
	defer func() {
		if len(message) < 1 {
			return
		}

		logger.Error("check payout data error: %v", message)
		common.SendAlarmByLevel(common.Warning, message)
	}()

	engine, err := dao.GetMysql()
	if err != nil {
		message = fmt.Sprintf("check payout data error: %v", err)
		return
	}

	end := time.Now().Unix() - 600
	count, err := engine.Where("created < ?", end).Count(&model.PayOut{})
	if err != nil {
		message = fmt.Sprintf("check payout data QueryCount error: %v", err)
		return
	}

	if minID == -1 {
		var first model.PayOut
		b, err := engine.Limit(1).Asc("id").Get(&first)
		if !b || err != nil {
			message = fmt.Sprintf("check payout data QueryMin error: %v-%v", b, err)
			return
		}

		minID = first.Id
	}

	var last model.PayOut
	b, err := engine.Where("created < ?", end).Limit(1).Desc("id").Get(&last)
	if !b || err != nil {
		message = fmt.Sprintf("check payout data QueryMax error: %v-%v", b, err)
		return
	}

	//fmt.Printf("status:---->%d--%d--%d--%d\n\n\n", diff, count, last.Id, minID)
	if diff == -1 {
		diff = last.Id - minID - count + 1
		return
	}

	nowDiff := last.Id - minID - count + 1
	if nowDiff != diff {
		message = fmt.Sprintf("check payout data: 可能数据丢失[%d-%d]", diff, nowDiff)
	}

	logger.Info("check payout data: last:%d, min:%d, count:%d, shouldCount:%d, diff:%d, tm:%d",
		last.Id, minID, count, last.Id-minID+1, diff, end)
}
