package crontab

import (
	"funzone_pay/app/service"
	"funzone_pay/dao"
	"funzone_pay/model"
	"github.com/wonderivan/logger"
	"time"
)

func CheckPayEntryCallback() {
	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("CheckPayEntryCallback_DBError | err=%v", err)
		return
	}
	var list []*model.PayEntryCallbackList
	end := time.Now().Unix() - 600
	start := end - 3*86400
	err = engine.Where("created between ? AND ? AND status=0", start, end).Limit(1000).Find(&list)
	if err != nil {
		logger.Error("CheckPayEntryCallback_Query_Error | err=%v", err)
		return
	}
	logger.Debug("CheckPayEntryCallback_list | len=%v", len(list))
	if len(list) == 0 {
		return
	}
	for _, v := range list {
		payEntry := new(model.PayEntry)
		_, err = engine.Where("order_id=?", v.OrderId).Get(payEntry)
		if err != nil {
			logger.Error("CheckPayEntryCallback_Update_Error | err=%v | data=%v", err, v)
			continue
		}
		go service.GetPayEntryService().SendEntryPlatFromCallback(payEntry)
	}
}
