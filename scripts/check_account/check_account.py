#!/usr/bin/python3
# -*- coding: UTF-8 -*-

import pymysql
# from os import path

def connect_db(host, user, pwd, database):
    db = pymysql.connect(host=host,
                         user=user,
                         password=pwd,
                         database=database,
                         port=3306,
                         charset='utf8')

    return db

def get_all_payout(db, app_id, status, start, end):
    ret = []
    cursor = db.cursor()
    try:
        # sql = "SELECT a.uid, gold, statement_gold FROM account as a LEFT JOIN user as u ON a.uid = u.uid WHERE a.createtime >= %d AND a.createtime <=%d AND u.channel = '144'" % (start, end)
        where = "WHERE app_id = '%s' AND status = %d" % (app_id, status)
        if start > 0 or end > 0 :
            where = "WHERE created > %d AND created < %d AND app_id = '%s' AND status = %d" % (start, end, app_id, status)

        sql = "SELECT id, amount, fee, status, created FROM pay_out " + where

        row_cnt = cursor.execute(sql)
        print("sql[%s]--> %d" % (sql, row_cnt))

        while row_cnt > 0:
            res = cursor.fetchone()
            ret.append({"id": res[0], "amount": res[1], "fee": res[2], "status": res[3], "created": res[4]})
            row_cnt -= 1

    except Exception as err:
        print("select pay out error", err)

    cursor.close()
    return ret


def get_all_payin(db, app_id, status, start, end):
    ret = []
    cursor = db.cursor()
    try:
        # sql = "SELECT a.uid, gold, statement_gold FROM account as a LEFT JOIN user as u ON a.uid = u.uid WHERE a.createtime >= %d AND a.createtime <=%d AND u.channel = '144'" % (start, end)
        where = "WHERE app_id = '%s' AND status = %d" % (app_id, status)
        if start > 0 or end > 0 :
            where = "WHERE created > %d AND created < %d AND app_id = '%s' AND status = %d" % (start, end, app_id, status)

        sql = "SELECT id, amount, fee, status FROM pay_entry " + where

        row_cnt = cursor.execute(sql)
        print("sql[%s]--> %d" % (sql, row_cnt))

        while row_cnt > 0:
            res = cursor.fetchone()
            ret.append({"id": res[0], "amount": res[1], "fee": res[2], "status": res[3]})
            row_cnt -= 1

    except Exception as err:
        print("select pay entry error", err)

    cursor.close()
    return ret

def get_all_frozen_transactions(db, uid, start, end):
    ret = []
    cursor = db.cursor()
    try:
        where = "WHERE uid = %d AND status = 0" % uid
        if start > 0 or end > 0 :
            where = "WHERE created > %d AND created < %d AND uid = %d AND status =0" % (start, end, uid)
        #start_balance, end_balance, start_unsettle_balance, end_unsettled_balance
        sql = "SELECT id, data_id, status, amount  From account_frozen_transactions "
        sql = sql + where
        row_cnt = cursor.execute(sql)
        print("sql[%s]--> %d" % (sql, row_cnt))

        while row_cnt > 0:
            res = cursor.fetchone()
            ret.append({"id": res[0], "data_id": res[1], "status": res[2], "amount": res[3]})
            row_cnt -= 1

    except Exception as err:
        print("select pay entry error", err)

    cursor.close()
    return ret


def get_account_balance(db, uid):
    ret = []
    cursor = db.cursor()
    try:
        sql = "SELECT id, uid, balance, unsettled_balance, frozen_balance, withdraw FROM account  WHERE uid = %d" % uid
        row_cnt = cursor.execute(sql)
        print("sql[%s]--> %d" % (sql, row_cnt))

        while row_cnt > 0:
            res = cursor.fetchone()
            ret.append({"id": res[0], "uid": res[1], "balance": res[2], "unsettled_balance": res[3], "frozen_balance": res[4], "withdraw": res[5]})
            row_cnt -= 1

    except Exception as err:
        print("select account  error", err)

    cursor.close()
    return ret[0]

# connect
db = connect_db("payment-instance-1.cqynyhwicvd6.ap-south-1.rds.amazonaws.com", "payment", "$O1gRVu3vV#9qK*m", "payment")

# search
start = 0
end = 0
payout_list = get_all_payout(db, 'rummymaster2123', 1, start, end)

payin_list = get_all_payin(db, 'rummymaster2123', 1, start, end)

frozen_list = get_all_frozen_transactions(db, 143, start, end)

account_balance = get_account_balance(db, 143)

# frozen
frozen_map = {}
no_data = 0
for frozen in frozen_list:
    # al = r_dict.get(item, -1)
    id = frozen["data_id"]
    if id == 0:
        print(frozen)
        no_data += frozen["amount"]
        continue

    if id in frozen_map:
        print("repeated->", frozen)
        continue

    frozen_map[id] = frozen

# check payout
out_amount = 0
out_fee = 0
for out in payout_list:
    id = out["id"]
    out_amount += out["amount"]
    out_fee += out["fee"]

    v = frozen_map.get(id, None)
    if v is None:
        print("payout not found in frozen_map", id, out)
        continue

    frozen_map.pop(id)

# in
in_amount = 0
in_fee = 0
for i in payin_list:
    in_amount += i["amount"]
    in_fee += i["fee"]

remain_amount = 0
for k, v in frozen_map.items():
    print(v)
    remain_amount += v["amount"]

print("frozen remain: %d-->%f,%f" % (len(frozen_map), remain_amount, no_data))

print("account:", account_balance)
print(" in-out: ", in_amount - in_fee - out_amount - out_fee)
