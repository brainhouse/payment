#!/usr/bin/python3
# -*- coding: UTF-8 -*-

import pymysql
# from os import path

def connect_db(host, user, pwd, database):
    db = pymysql.connect(host=host,
                         user=user,
                         password=pwd,
                         database=database,
                         port=3306,
                         charset='utf8')

    return db

def get_all_frozen_transaction(db, status, start, end):
    ret = []
    cursor = db.cursor()
    try:
        # sql = "SELECT a.uid, gold, statement_gold FROM account as a LEFT JOIN user as u ON a.uid = u.uid WHERE a.createtime >= %d AND a.createtime <=%d AND u.channel = '144'" % (start, end)
        where = "WHERE status = %d" % status
        if start > 0 or end > 0 :
            where = "WHERE created > %d AND created < %d AND status = %d" % (start, end, status)

        sql = "SELECT id, data_id, uid, amount, created FROM account_frozen_transactions " + where

        row_cnt = cursor.execute(sql)
        print("sql[%s]--> %d" % (sql, row_cnt))

        while row_cnt > 0:
            res = cursor.fetchone()
            ret.append({"id": res[0], "data_id": res[1], "uid": res[2], "amount": res[3], "created": res[4]})
            row_cnt -= 1

    except Exception as err:
        print("select account_frozen_transactions error", err)

    cursor.close()
    return ret


def is_payout_fail(db, id):
    ret = True
    cursor = db.cursor()
    try:
        sql = "SELECT id, uid, amount, status FROM pay_out where id = %d" % id
        row_cnt = cursor.execute(sql)
        if row_cnt < 0:
             print("payout order[%d] not exist" % id)
             ret = False
        else:
            res = cursor.fetchone()
            if res[3] == 2:
                ret = True

    except Exception as err:
        print("select pay out error", err, id)

    cursor.close()
    return ret

# connect
db = connect_db("payment-instance-1.cqynyhwicvd6.ap-south-1.rds.amazonaws.com", "payment", "$O1gRVu3vV#9qK*m", "payment")

# search
start = 0
end = 1683648000
status = 0
frozen_list = get_all_frozen_transaction(db, status, start, end)

fail = []
amount = {}
for frozen in frozen_list:
    # al = r_dict.get(item, -1)
    id = frozen["data_id"]
    if id == 0:
        fail.append(frozen)
        continue

    if is_payout_fail(db, id):
        fail.append(frozen)
        uid = frozen["uid"]
        if uid in amount:
            amount[uid] =  amount[uid] + frozen["amount"]
        else:
            amount[uid] = frozen["amount"]

for item in fail:
    print(item)

print("\n\n\n")
print(len(fail))
for k, v in amount.items():
    print("frozen: %d-->%d" % (k, v))
