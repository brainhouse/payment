#!/usr/bin/python3
# -*- coding: UTF-8 -*-

import pymysql
# from os import path

def connect_db(host, user, pwd, database):
    db = pymysql.connect(host=host,
                         user=user,
                         password=pwd,
                         database=database,
                         port=3306,
                         charset='utf8')

    return db

def get_all_payout(db, app_id, start, end):
    ret = []
    cursor = db.cursor()
    try:
        # sql = "SELECT a.uid, gold, statement_gold FROM account as a LEFT JOIN user as u ON a.uid = u.uid WHERE a.createtime >= %d AND a.createtime <=%d AND u.channel = '144'" % (start, end)
        where = "WHERE app_id = '%s'" % (app_id)
        if start > 0 or end > 0 :
            where = "WHERE created > %d AND created < %d AND app_id = '%s'" % (start, end, app_id)

        sql = "SELECT id, amount, fee, status, created FROM pay_out " + where

        row_cnt = cursor.execute(sql)
        print("sql[%s]--> %d" % (sql, row_cnt))

        while row_cnt > 0:
            res = cursor.fetchone()
            ret.append({"id": res[0], "amount": res[1], "fee": res[2], "status": res[3], "created": res[4]})
            row_cnt -= 1

    except Exception as err:
        print("select pay out error", err)

    cursor.close()
    return ret


def get_all_frozen_transactions(db, uid, start, end):
    ret = []
    cursor = db.cursor()
    try:
        where = "WHERE uid = %d" % uid
        if start > 0 or end > 0 :
            where = "WHERE created > %d AND created < %d AND uid = %d" % (start, end, uid)
        #start_balance, end_balance, start_unsettle_balance, end_unsettled_balance
        sql = "SELECT id, data_id, status, amount, created From account_frozen_transactions "
        sql = sql + where
        row_cnt = cursor.execute(sql)
        print("sql[%s]--> %d" % (sql, row_cnt))

        while row_cnt > 0:
            res = cursor.fetchone()
            ret.append({"id": res[0], "data_id": res[1], "status": res[2], "amount": res[3], "created": res[4]})
            row_cnt -= 1

    except Exception as err:
        print("select frozen error", err)

    cursor.close()
    return ret

# connect
db = connect_db("payment-instance-1.cqynyhwicvd6.ap-south-1.rds.amazonaws.com", "payment", "$O1gRVu3vV#9qK*m", "payment")

# search
start = 0
end = 0
payout_list = get_all_payout(db, 'rummymaster2123', start, end)

frozen_list = get_all_frozen_transactions(db, 143, start, end)

# frozen
frozen_map = {}
no_data = 0
for frozen in frozen_list:
    # al = r_dict.get(item, -1)
    id = frozen["data_id"]
    if id == 0:
        print(frozen)
        no_data += frozen["amount"]
        continue

    if id in frozen_map:
        print("repeated->", frozen)
        continue

    frozen_map[id] = frozen

# check payout
for out in payout_list:
    id = out["id"]
    if id in frozen_map:
        frozen_map.pop(id)
    else:
        print("payout not found in frozen_map", out)

#
for k, v in frozen_map.items():
    print(v)

print("data_id == 0 ->>", no_data)

