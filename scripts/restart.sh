#!/bin/sh
paymentPath="/data/app/payment"
while true; do
        server=`ps aux | grep payment.server | grep -v grep`
        if [ ! "$server" ]; then
            cd $paymentPath ; nohup $paymentPath/payment.server > /data/logs/payment/run.log 2>&1 &
            sleep 10
        fi
        sleep 5
done
