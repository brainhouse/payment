#!/usr/bin/python3
# -*- coding: UTF-8 -*-

import pymysql
import openpyxl
import time
from os import path

def read_file(fname):
    if not path.exists(fname):
        return []

    fio = open(fname, 'r')
    lines = fio.readlines()
    body = []
    for line in lines:
        lin = line.strip()
        body.append(lin)

    fio.close()
    return body

def list_to_dict(lines):
    r_dict = {}
    for line in lines:
        values = line.split(',')
        order_id = values[0]
        r_dict[order_id] = 0

    return r_dict


def connect_db(host, user, pwd, database):
    db = pymysql.connect(host=host,
                         user=user,
                         password=pwd,
                         database=database,
                         port=3306,
                         charset='utf8')

    return db


def get_payin(db, order_id):
    ret = []
    cursor = db.cursor()
    try:
        sql = "SELECT app_id, uid, pay_account_id, app_order_id, order_id, amount, created, status FROM pay_entry where order_id = '%s'" % order_id

        row_cnt = cursor.execute(sql)
        #print("sql[%s]--> %d" % (order_id, row_cnt))

        while row_cnt > 0:
            res = cursor.fetchone()
            ret.append({"app_id": res[0], "uid": res[1], "pay_account_id": res[2], "app_order_id": res[3], "order_id": res[4], "amount": res[5], "created": res[6], "status":res[7]})
            row_cnt -= 1

    except Exception as err:
        print("select pay entry error", err)

    cursor.close()
    return ret[0]

#def unixToBJ(unix):
#    time_tuple = time.localtime(unix + 3600*8)
#    return time.strftime("%Y/%m/%d %H:%M:%S", time_tuple)

def output_excel(excel, head, data):
    wb=openpyxl.Workbook()
    sheet=wb.active

    sheet.append(head)
    for row in data:
        sheet.append(row)

    wb.save(excel)

# connect
db = connect_db("payment-instance-1.cqynyhwicvd6.ap-south-1.rds.amazonaws.com", "payment", "$O1u3vV#9qK*m", "payment")

# search
order_ids = read_file('order_list.txt')
# orders = list_to_dict(lines)

result_dict = {}
for order_id in order_ids:
    #print('kv---%s-->%s' % (order_id, number))
    order_info = get_payin(db, order_id)
    print('%s,%s' %(order_id, order_info['status']))
    result_dict[order_id] = order_info['status']


print("total order:%d" % len(order_ids))
print("unique order:%d" % len(result_dict))

