#!/usr/bin/python3
# -*- coding: UTF-8 -*-

import pymysql
# from os import path

def connect_db(host, user, pwd, database):
    db = pymysql.connect(host=host,
                         user=user,
                         password=pwd,
                         database=database,
                         port=3306,
                         charset='utf8')

    return db

def get_payout(db, start, end, limit):
    ret = []
    cursor = db.cursor()
    try:
        where = ""
        if start > 0 or end > 0 :
           where = "WHERE created > %d AND created < %d" % (start, end)

        if limit > 0:
           where = where + " order by id asc limit %d" % limit

        sql = "SELECT id, order_id FROM pay_out " + where

        row_cnt = cursor.execute(sql)

        print("sql[%s]--> %d" % (sql, row_cnt))

        while row_cnt > 0:
            res = cursor.fetchone()
            ret.append({"id": res[0], "order_id": res[1]})
            row_cnt -= 1

    except Exception as err:
        print("select pay out error", err)

    cursor.close()
    return ret

def get_payin(db, start, end, limit):
    ret = []
    cursor = db.cursor()
    try:
        where = ""
        if start > 0 or end > 0 :
            where = "WHERE created > %d AND created < %d" % (start, end)

        if limit > 0:
            where = where + " order by id asc limit %d" % limit

        sql = "SELECT id, order_id FROM pay_entry " + where

        row_cnt = cursor.execute(sql)
        print("sql[%s]--> %d" % (sql, row_cnt))

        while row_cnt > 0:
            res = cursor.fetchone()
            ret.append({"id": res[0], "order_id": res[1]})
            row_cnt -= 1

    except Exception as err:
        print("select pay entry error", err)

    cursor.close()
    return ret


def clean_payin(db, id, order_id):
    cursor = db.cursor()
    try:

        sql = "DELETE FROM pay_entry WHERE id = %d" % id

        row_cnt = cursor.execute(sql)
        db.commit()

        print("sql[%s]--> %d" % (sql, row_cnt))

    except Exception as err:
        print("clean pay entry error", err, order_id)

    cursor.close()


def clean_payout(db, id, order_id):
    cursor = db.cursor()
    try:

        sql = "DELETE FROM pay_out WHERE id = %d" % id

        row_cnt = cursor.execute(sql)
        db.commit()

        print("sql[%s]--> %d" % (sql, row_cnt))

    except Exception as err:
        print("clean payout error", err, order_id)

    cursor.close()


def clean_frozen_transactions(db, id):
    cursor = db.cursor()
    try:
        sql = "DELETE From account_frozen_transactions WHERE data_id = %d" % id
        row_cnt = cursor.execute(sql)
        db.commit()
        print("sql[%s]--> %d" % (sql, row_cnt))

    except Exception as err:
        print("select clean frozen_transaction error", err)

    cursor.close()


def clean_account_transaction(db, id):
    cursor = db.cursor()
    try:
        # where = "WHERE app_id = '%s' AND status = %d" % (app_id, status)
        sql = "DELETE FROM account_transactions WHERE data_id = %d" % id

        row_cnt = cursor.execute(sql)
        db.commit()
        print("sql[%s]--> %d" % (sql, row_cnt))

    except Exception as err:
        print("clean account_transaction error", err)

    cursor.close()


def clean_callback_list(db, start, end):
    cursor = db.cursor()
    try:
        # where = "WHERE app_id = '%s' AND status = %d" % (app_id, status)
        sql = "DELETE FROM pay_entry_callback_list WHERE created > %d AND created < %d" % (start, end)

        row_cnt = cursor.execute(sql)
        db.commit()
        print("sql[%s]--> %d" % (sql, row_cnt))


        sql = "DELETE FROM pay_out_callback_list WHERE created > %d AND created < %d" % (start, end)

        row_cnt = cursor.execute(sql)
        db.commit()
        print("sql[%s]--> %d" % (sql, row_cnt))

    except Exception as err:
        print("clean pan entry callback error", err)

    cursor.close()


# connect
db = connect_db("payment-instance-1.cqynyhwicvd6.ap-south-1.rds.amazonaws.com", "payment", "$O1gRVu3vV#9qK*m", "payment")

# search
start = 0
end = 0

payout_list = get_payout(db, start, end, 1024)
for out in payout_list:
    # al = r_dict.get(item, -1)
    id = out["id"]
    order_id = out["order_id"]
    if id == 0:
        continue

    clean_payout(db, id, order_id)

    clean_account_transaction(db, id)
    clean_frozen_transactions(db, id)



payin_list = get_payin(db, start, end, 1024)
for i in payin_list:
    id = i["id"]
    order_id = i["order_id"]
    if id == 0:
        continue

    clean_account_transaction(db, id)


#
clean_callback_list(db, start, end)
