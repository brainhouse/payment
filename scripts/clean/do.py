#!/usr/bin/python3
# -*- coding: UTF-8 -*-

import pymysql
# from os import path

def connect_db(host, user, pwd, database):
    db = pymysql.connect(host=host,
                         user=user,
                         password=pwd,
                         database=database,
                         port=3306,
                         charset='utf8')

    return db

def clean(db, table_name, end_time, limit):
    cursor = db.cursor()

    row_cnt = 1
    while(row_cnt > 0):
        try:
            # where = "WHERE app_id = '%s' AND status = %d" % (app_id, status)
            sql = "DELETE FROM %s WHERE created < %d limit %d" % (table_name, end_time, limit)

            #row_cnt -= 1
            row_cnt = cursor.execute(sql)
            db.commit()
            print("sql[%s]--> %d" % (sql, row_cnt))

        except Exception as err:
            print("clean pan entry callback error", err)
            break

    cursor.close()

def get_silence_user(db):
    cursor = db.cursor()
    ret = []
    try:
        # where = "WHERE app_id = '%s' AND status = %d" % (app_id, status)
        sql = "SELECT id, parent_id, user_name FROM user WHERE is_silence = 1"

        #row_cnt -= 1
        row_cnt = cursor.execute(sql)
        print("sql[%s]--> %d" % (sql, row_cnt))

        while row_cnt > 0:
            res = cursor.fetchone()
            ret.append({"uid": res[0], "parent_id": res[1], "email": res[2]})
            row_cnt -= 1

    except Exception as err:
        print("silence error", err)

    cursor.close()
    return ret

def clean_user(db, tables, uid):
    cursor = db.cursor()

    for table_name in tables:
        sql = ""
        try:
            # where = "WHERE app_id = '%s' AND status = %d" % (app_id, status)
            sql = "DELETE FROM %s WHERE uid= %d" % (table_name, uid)
            if table_name == "user":
                sql = "DELETE FROM %s WHERE id= %d" % (table_name, uid)

            row_cnt = cursor.execute(sql)
            if row_cnt < 1:
                print("no effect-->%s" % sql)

            db.commit()

        except Exception as err:
            print("%s-->[%s]" % (sql, err))

    cursor.close()

def clean_client(db):
    cursor = db.cursor()

    parent_ids = []
    try:
        # where = "WHERE app_id = '%s' AND status = %d" % (app_id, status)
        sql = "SELECT id, parent_id, user_name FROM user"

        #row_cnt -= 1
        row_cnt = cursor.execute(sql)
        print("sql[%s]--> %d" % (sql, row_cnt))

        while row_cnt > 0:
            res = cursor.fetchone()
            parent_ids.append(str(res[1]))
            row_cnt -= 1

    except Exception as err:
        print("get user parent_id error", err)


    parent_id_str = ",".join(parent_ids)
    row_cnt = 0
    try:
        # where = "WHERE app_id = '%s' AND status = %d" % (app_id, status)
        sql = "DELETE FROM client WHERE id NOT IN (%s)" % parent_id_str

        row_cnt = cursor.execute(sql)
        print("%s-->%d" % (sql, row_cnt))

        db.commit()

    except Exception as err:
        print("del client error", err)

    cursor.close()

# connect
#db = connect_db("payment-instance-1.cqynyhwicvd6.ap-south-1.rds.amazonaws.com", "payment", "$O1gRVu3vV#9qK*m", "payment")
# db = pymysql.connect(host='13.127.108.170',
#                      user='test',
#                      password='29WkHqFe81/91ZdWfgm6QIYbZ+o=',
#                      database='payment_new',
#                      port=3306,
#                      charset='utf8')

# end_time = 1672532154
# limit = 10000
# clean(db, "pay_entry", end_time, limit)
# clean(db, "pay_out", end_time, limit)
# clean(db, "pay_entry_callback_list" , end_time, limit)
# clean(db, "pay_out_callback_list" , end_time, limit)
# clean(db, "account_transactions" , end_time, limit)
# clean(db, "user_settlement_history" , end_time, limit)
# clean(db, "user_withdraw_flow" , end_time, limit)
# clean(db, "settlement_history" , end_time, limit)
# clean(db, "pay_web_log" , end_time, limit)
# clean(db, "login_flow" , end_time, limit)
# clean(db, "frozen_balance" , end_time, limit)
# clean(db, "account_frozen_transactions" , end_time, limit)
# clean(db, "account_transactions" , end_time, limit)
# clean(db, "admin_add_balance" , end_time, limit)

user_list = get_silence_user(db)
# "account_frozen_transactions",
tables = ["user", "account", "account_developer_settings",  "account_payment_settings", "account_transactions", "admin_add_balance"]

print("\n\n\n\n----uid------------------->")
for user in user_list:
    print("%s-->%s" % (user["parent_id"], user["uid"]))

print("\n\n\n\n----email------------------->")
for user in user_list:
    print("%s" % user["email"])

print("\n\n\n\n----delete------------------->")
for user in user_list:
    clean_user(db, tables, user["uid"])

#clean_client(db)