#!/usr/bin/python3
# -*- coding: UTF-8 -*-

import pymysql
from os import path


def read_file(fname):
    if not path.exists(fname):
        return []

    fio = open(fname, 'r')
    lines = fio.readlines()
    body = []
    for line in lines:
        lin = line.strip()
        body.append(lin)

    fio.close()
    return body

def filter_strip(phones):
    result = []
    for phone in phones:
        if phone[:3] == '+91':
            result.append(phone[3:])
        elif phone[:2] == '91' and len(phone) > 10:
            result.append(phone[2:])
        else:
            result.append(phone)

    return result

def list_to_dict(data_list):
    r_dict = {}
    repetition = 0
    for item in data_list:
        val = r_dict.get(item, -1)
        if val == -1:
            r_dict[item] = 1
        else:
            repetition += 1
            #print("repetition key[%s]" %  item)

    print("repetition count[%d]" %  repetition)
    return r_dict

def insert_black(data_dict, data_type):
    db = pymysql.connect(host='payment-instance-1.cqynyhwicvd6.ap-south-1.rds.amazonaws.com',
                         user='payment',
                         password='$O1gRVu3vV#9qK*m',
                         database='payment',
                         port=3306,
                         charset='utf8')

    cursor = db.cursor()

    success = 0
    failure = 0
    space   = 0
    #for phone in phone_list:
    for key in data_dict:
        no_space = key.strip()
        if len(no_space) < 5:
            print("space value[%s-%s]" %  (data_type, no_space))
            space += 1
            continue

        try:
            sql = "INSERT into black_list(type,data) values('%s','%s');" % (data_type, no_space)
            effect_row = cursor.execute(sql)
            if effect_row < 1:
                print("insert %s-%s error" %  (data_type, no_space))

            db.commit()
            success += 1
        except Exception as err:
            db.rollback()
            print("insert %s-%s except %s" %  (data_type, no_space, err))
            failure += 1

    print("failure %d, success: %d, space: %d" % (failure, success, space))

    cursor.close()
    db.close()


body = read_file('./phone.txt')
phone_list = filter_strip(body)
bank_list = read_file('./bank.txt')
id_list = read_file('./identify.txt')
email_list = read_file('./email.txt')

phone_dict = list_to_dict(phone_list)
print("phone-> %d:%d" % (len(phone_list), len(phone_dict)))
insert_black(phone_dict, 'phone')

bank_dict = list_to_dict(bank_list)
print("bank-> %d:%d" % (len(bank_list), len(bank_dict)))
insert_black(bank_dict, 'bank_card')

id_dict = list_to_dict(id_list)
print("id-> %d:%d" % (len(id_list), len(id_dict)))
insert_black(id_dict, 'id')

email_dict = list_to_dict(email_list)
print("email-> %d:%d" % (len(email_list), len(email_dict)))
insert_black(email_dict, 'email')


