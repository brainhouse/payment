#!/usr/bin/python3
# -*- coding: UTF-8 -*-

import pymysql
import openpyxl
import time
from os import path


db = pymysql.connect(host='payment-instance-1.cqynyhwicvd6.ap-south-1.rds.amazonaws.com',
                     user='payment',
                     password='$O1gRVu3vV#9qK*m',
                     database='payment',
                     port=3306,
                     charset='utf8')

cursor = db.cursor()

#for phone in phone_list:
i = 0
unix = time.time()
while i < 375620:
    i+=1
    try:
        sql = "SELECT type, data, created FROM black_list WHERE id = %d" % i
        row_cnt = cursor.execute(sql)
        if row_cnt < 1:
            continue

        res = cursor.fetchone()
        type = res[0]
        data = res[1]
        created = res[2]

#         print(type)
#         print(data)
#         print(created)
#         print(res)

        if created > 0:
            break

        if type != "email" and type != "bank_card":
            continue

        data_values = data.split("@")
        lower_value = data_values[0].strip('"').strip().lower()

        # check data
        sql = "SELECT id, type, data FROM black_list WHERE data = '%s'" % lower_value
        row_cnt = cursor.execute(sql)
        if row_cnt > 0:
            continue

        # new value not exist, insert new one, then delete old
        sql = "INSERT into black_list(type, data, created, updated) values('%s','%s',%d, %d)" % (type, lower_value, unix, unix)
        effect_row = cursor.execute(sql)
        if effect_row < 1:
            print("[%s] error" % sql)
            continue

        sql = "DELETE FROM black_list WHERE id = %d" % i
        effect_row = cursor.execute(sql)
        if effect_row < 1:
            print("[%s] error" %  sql)

        print("convert[%s]-->[%s]" %(data, lower_value))

        db.commit()
    except Exception as err:
        db.rollback()
        print("del[%s]-->%s" %  (sql, err))

print("i--->%d" % i)
cursor.close()
db.close()