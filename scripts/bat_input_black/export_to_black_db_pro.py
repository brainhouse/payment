#!/usr/bin/python3
# -*- coding: UTF-8 -*-

import pymysql
import openpyxl
import time
from os import path

def read_file(fname):
    if not path.exists(fname):
        return []

    fio = open(fname, 'r')
    lines = fio.readlines()
    body = []
    for line in lines:
        lin = line.strip()
        body.append(lin)

    fio.close()
    return body

def filter_strip(phones):
    result = []
    for phone in phones:
        if phone[:3] == '+91':
            result.append(phone[3:])
        elif phone[:2] == '91' and len(phone) > 10:
            result.append(phone[2:])
        else:
            result.append(phone)

    return result

def list_to_dict(data_list):
    r_dict = {}
    repetition = 0
    for item in data_list:
        val = r_dict.get(item, -1)
        if val == -1:
            r_dict[item] = 1
        else:
            repetition += 1
            #print("repetition key[%s]" %  item)

    print("repetition count[%d]" %  repetition)
    return r_dict

def insert_black(data_dict, data_type):
    db = pymysql.connect(host='payment-instance-1.cqynyhwicvd6.ap-south-1.rds.amazonaws.com',
                         user='payment',
                         password='$O1gRVu3vV#9qK*m',
                         database='payment',
                         port=3306,
                         charset='utf8')

    cursor = db.cursor()

    success = 0
    failure = 0
    space   = 0
    #for phone in phone_list:
    for key in data_dict:
        no_space = key.strip()
        if len(no_space) < 5:
            #print("space value[%s-%s]" %  (data_type, no_space))
            space += 1
            continue

        try:
            sql = "INSERT into black_list(type,data) values('%s','%s');" % (data_type, no_space)
            effect_row = cursor.execute(sql)
            if effect_row < 1:
                print("insert %s-%s error" %  (data_type, no_space))

            db.commit()
            success += 1
        except Exception as err:
            db.rollback()
            print("insert %s-%s except %s" %  (data_type, no_space, err))
            failure += 1

    print("failure %d, success: %d, space: %d" % (failure, success, space))

    cursor.close()
    db.close()

def load_zw_excel(filename):
    wb = openpyxl.load_workbook(filename = filename,  read_only=True)
    ws = wb.get_sheet_by_name("main")
    row_list = []
    for row in ws.iter_rows(min_row=2, max_col=10):
        phone = row[0].value
        email = row[1].value
        upi = row[2].value
        bank = row[3].value
        ip = row[4].value
        count = row[6].value
        amount = row[7].value

        if phone == None:
            phone = ""
        if email == None:
            email = ""
        if upi == None:
            upi = ""
        if bank == None:
            bank = ""
        if ip == None:
            ip = ""
        if amount == None:
            amount = ""
        if count == None:
            count = ""

        if len(phone) > 0:
            phone = phone.strip('"').strip()

        email_raw = ""
        if len(email) > 0:
            email_raw = email.strip('"').strip().lower()

            email_values = email.split("@")
            email = email_values[0].strip('"').strip().lower()


        upi_raw = ""
        if len(upi) > 0:
            upi_raw = upi.strip('"').strip().lower()

            upi_values = upi.split("@")
            upi = upi_values[0].strip('"').strip().lower()

        if len(bank) > 0:
            bank = bank.strip('"').strip().lower()

        if len(ip) > 0:
            ip = ip.strip('"').strip()

        if len(amount) > 0:
            amount = amount.strip('"').strip()

        if len(count) > 0:
            count = count.strip('"').strip()


        #print("%s,%s,%s,%s,%s,%s" %(phone, email, upi, bank, ip, amount))
        row_list.append({"phone": phone, "email": email, "email_raw": email_raw, "upi": upi, "upi_raw": upi_raw, "bank": bank, "ip": ip, "count": count, "amount": amount})
    print("%s--->%d" % (filename,ws.max_row))
    return row_list


def load_ff_excel(filename):
    wb = openpyxl.load_workbook(filename = filename,  read_only=True)
    sheet_names = wb.sheetnames
    ws = wb[sheet_names[0]]
    row_list = []
    for row in ws.iter_rows(min_row=2, max_col=10):
        bank = row[1].value
        upi = ""
        phone = row[2].value
        email = row[3].value
        ip = row[4].value

        count = "1"
        amount = ""

        if phone == None:
            phone = ""
        if email == None:
            email = ""
        if upi == None:
            upi = ""
        if bank == None:
            bank = ""
        if ip == None:
            ip = ""
        if amount == None:
            amount = ""

        if len(phone) > 0:
            phone = phone.strip('"').strip()

        email_raw = ""
        if len(email) > 0:
            email_raw = email.strip('"').strip().lower()
            email_values = email.split("@")
            email = email_values[0].strip('"').strip().lower()

        upi_raw = ""
        if len(upi) > 0:
            upi_raw = upi.strip('"').strip().lower()
            upi_values = upi.split("@")
            upi = upi_values[0].strip('"').strip().lower()

        if len(bank) > 0:
            bank = bank.strip('"').strip().lower()

        if len(ip) > 0:
            ip = ip.strip('"').strip()

        if len(amount) > 0:
            amount = amount.strip('"').strip()

        #print("%s,%s,%s,%s,%s,%s" %(phone, email, upi, bank, ip, amount))
        row_list.append({"phone": phone, "email": email, "email_raw": email_raw, "upi": upi, "upi_raw": upi_raw, "bank": bank, "ip": ip, "count": count, "amount": amount})
    print("%s--->%d" % (filename,ws.max_row))
    return row_list

def load_ff_txt(filename):
    row_list = []
    data = read_file(filename)
    for row in data:
        row_values = row.split(",")

        bank = ""
        upi = ""
        phone = ""
        email = ""
        ip = ""
        count = "1"
        amount = ""

        row_type = row_values[0]
        row_value = row_values[1]
        if "phone" == row_type:
            phone = row_value
        elif "email" == row_type:
            email = row_value
        elif "upi" == row_type:
            upi = row_value
        elif "bank" == row_type:
            bank = row_value
        elif "ip" == row_type:
            ip = row_value
        elif "amount" == row_type:
            amount = row_value

        if len(phone) > 0:
            phone = phone.strip('"').strip()

        email_raw = ""
        if len(email) > 0:
            email_raw = email.strip('"').strip().lower()
            email_values = email.split("@")
            email = email_values[0].strip('"').strip().lower()

        upi_raw = ""
        if len(upi) > 0:
            upi_raw = upi.strip('"').strip().lower()
            upi_values = upi.split("@")
            upi = upi_values[0].strip('"').strip().lower()

        if len(bank) > 0:
            bank = bank.strip('"').strip().lower()

        if len(ip) > 0:
            ip = ip.strip('"').strip()

        if len(amount) > 0:
            amount = amount.strip('"').strip()

        #print("%s,%s,%s,%s,%s,%s" %(phone, email, upi, bank, ip, amount))
        row_list.append({"phone": phone, "email": email, "email_raw": email_raw, "upi": upi, "upi_raw": upi_raw, "bank": bank, "ip": ip, "count": count, "amount": amount})
    print("%s--->%d" % (filename,len(data)))
    return row_list



def input_db(rows):
    db = pymysql.connect(host='payment-instance-1.cqynyhwicvd6.ap-south-1.rds.amazonaws.com',
                         user='payment',
                         password='$O1gRVu3vV#9qK*m',
                         database='payment',
                         port=3306,
                         charset='utf8')

#     db = pymysql.connect(host='13.127.108.170',
#                          user='test',
#                          password='29WkHqFe81/91ZdWfgm6QIYbZ+o=',
#                          database='payment_new',
#                          port=3306,
#                          charset='utf8')
    cursor = db.cursor()

    success = 0
    failure = 0
    space   = 0
    unix = time.time()
    #for phone in phone_list:
    for row in rows:
        ok = False
        for key, value in row.items():
            if len(value) < 3 or key == "amount" or key == "count":
                #print("space value[%s-%s]" %  (key, value))
                space += 1
                continue


            data_type = key
            if key == "upi":
                data_type = "bank"

#             	BlackTypeIP       = "ip"
#             	BlackTypeBankCard = "bank_card"
#             	BlackTypeID       = "id"
#             	BlackTypePhone    = "phone"

            try:
                sql = "INSERT into black_list(type, data, created, updated) values('%s','%s',%d,%d);" % (data_type, value, unix, unix)
                effect_row = cursor.execute(sql)
                if effect_row < 1:
                    print("insert %s-%s error" %  (data_type, value))

                db.commit()
                success += 1
                ok = True
            except Exception as err:
                db.rollback()
                print("insert %s-%s except %s" %  (data_type, value, err))
                failure += 1

        if ok :
            # insert in to detail
            try:
                amount = row["amount"]
                if len(amount) < 1:
                    amount = "0"

                sql = "INSERT into black_list_detail(phone, email, bank, vpa, ip, count, amount, created) values('%s','%s','%s','%s','%s',%s,%s,%d);" % (row["phone"], row["email_raw"], row["bank"], row["upi_raw"], row["ip"], row["count"], amount, unix)
                effect_row = cursor.execute(sql)
                if effect_row < 1:
                    print("%s error" %  sql)

                db.commit()
                ok = True
            except Exception as err:
                db.rollback()
                print("%s except %s" %  (sql, err))


    print("failure %d, success: %d, space: %d" % (failure, success, space))

    cursor.close()
    db.close()

zw_rows = load_zw_excel("zw0621.xlsx")
ff_rows = load_ff_excel('fp.xlsx')
ff_txt_rows = load_ff_txt("fp.txt")

input_db(zw_rows)
input_db(ff_rows)
input_db(ff_txt_rows)

print("zw:%d, ff:%d, txt:%d" % (len(zw_rows), len(ff_rows), len(ff_txt_rows)))

# body = read_file('./phone.txt')
# phone_list = filter_strip(body)
# bank_list = read_file('./bank.txt')
# id_list = read_file('./identify.txt')
# email_list = read_file('./email.txt')

# phone_dict = list_to_dict(phone_list)
# print("phone-> %d:%d" % (len(phone_list), len(phone_dict)))
# insert_black(phone_dict, 'phone')
#
# bank_dict = list_to_dict(bank_list)
# print("bank-> %d:%d" % (len(bank_list), len(bank_dict)))
# insert_black(bank_dict, 'bank_card')
#
# id_dict = list_to_dict(id_list)
# print("id-> %d:%d" % (len(id_dict), len(id_list)))
# insert_black(id_dict, 'id')
#
# email_dict = list_to_dict(email_list)
# print("email-> %d:%d" % (len(email_dict), len(email_list)))
# insert_black(email_dict, 'email')
