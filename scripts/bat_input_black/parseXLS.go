package main

import (
	"bufio"
	"errors"
	"fmt"
	"github.com/shopspring/decimal"
	"github.com/tealeg/xlsx"
	"io"
	"io/fs"
	"io/ioutil"
	"os"
	"reflect"
	"strconv"
	"strings"
)

func LoadExcelTbl(path string, sheetName string ) (phone []string, email []string, bank []string, vpa []string, id []string, err error) {
	if len(path) <= 0 || len(sheetName) <= 0 {
		err = errors.New("path or sheet name is nil")
		return
	}

	f, err := xlsx.OpenFile(path)
	if err != nil {
		return
	}

	for _, sheet := range f.Sheets {
		if sheet.Name != sheetName {
			continue
		}

		phone = make([]string, 0, 10240)
		email = make([]string, 0, 10240)
		bank = make([]string, 0, 10240)
		vpa = make([]string, 0, 10240)
		id = make([]string, 0, 10240)

		for rowIndex, row := range sheet.Rows {
			if rowIndex <= 0 {
				continue
			}

			indCnt := len(row.Cells)
			if indCnt < 1 {
				continue
			}
			val := strings.TrimSpace(row.Cells[0].Value)
			if len(val) > 4 {
				phone = append(phone, val)
			}

			if indCnt < 2 {
				continue
			}
			val = strings.TrimSpace(row.Cells[1].Value)
			if len(val) > 4 {
				email = append(email, val)
			}

			if indCnt < 3 {
				continue
			}
			val = strings.TrimSpace(row.Cells[2].Value)
			if len(val) > 4 {
				bank = append(bank, val)
			}

			if indCnt < 4 {
				continue
			}
			val = strings.TrimSpace(row.Cells[3].Value)
			if len(val) > 4 {
				vpa = append(vpa, val)
			}

			if indCnt < 5 {
				continue
			}
			val = strings.TrimSpace(row.Cells[4].Value)
			if len(val) > 4 {
				id = append(id, val)
			}

			fmt.Printf("index:%v, phone:%v, email:%v, bank:%v, id:%v\n", rowIndex, row.Cells[0].Value, row.Cells[1].Value, row.Cells[2].Value, row.Cells[3].Value)
		}
		return
	}
	return
}


func LoadSheet(filename string, sheetName string) (rows map[string][]string) {
	f, err := xlsx.OpenFile(filename)
	if err != nil {
		return
	}

	rows = make(map[string][]string)
	for _, sheet := range f.Sheets {
		if sheet.Name != sheetName {
			continue
		}

		for rowIndex, row := range sheet.Rows {
			if rowIndex <= 0 {
				continue
			}

			indCnt := len(row.Cells)
			if indCnt < 1 {
				continue
			}

			values := make([]string, 0, indCnt)
			for i:=0; i< indCnt; i++ {
				values = append(values,  strings.TrimSpace(row.Cells[i].Value))
			}

			key := strings.Join(values, ",")
			rows[key] = values
		}
	}

	return
}

func OutDiff(sourceFile string, sourceSheet string, targetFile string, targetSheet string) (phone []string, email []string, bank []string, vpa []string, id []string, err error) {
	if len(sourceFile) <= 0 || len(sourceFile) <= 0 || len(targetFile) < 1 || len(targetSheet) < 1{
		err = errors.New("path or sheet name is nil")
		return
	}

	srcRows := LoadSheet(sourceFile, sourceSheet)
	targetRows := LoadSheet(targetFile, targetSheet)

	for k, v := range targetRows {
		_, ok := srcRows[k]
		if ok {
			continue
		}

		fmt.Printf("%v-->%v\n", k, v)

		val := strings.TrimSpace(v[0])
		if len(val) > 4 {
			phone = append(phone, val)
		}


		val = strings.TrimSpace(v[1])
		if len(val) > 4 {
			email = append(email, val)
		}


		val = strings.TrimSpace(v[2])
		if len(val) > 4 {
			vpa = append(vpa, val)
		}

		val = strings.TrimSpace(v[3])
		if len(val) > 4 {
			bank = append(bank, val)
		}

		//val = strings.TrimSpace(v[4])
		//if len(val) > 4 {
		//	id = append(id, val)
		//}
	}
	return
}

func LoadTbl(path string) error {
	if len(path) <= 0 {
		return errors.New("path is nil")
	}
	f, err := os.Open(path)
	if err != nil {
		return err
	}
	defer f.Close()

	br := bufio.NewReader(f)
	for line, prefix, __ := br.ReadLine(); __ != io.EOF; line, prefix, __ = br.ReadLine() {
		if __ != nil {
			return errors.New(fmt.Sprintf("the table is too long table:%s line:%s \n  error:%v  prefix:%v", path, line, __, prefix))
		}

		if prefix {
			return errors.New(fmt.Sprintf("the table is too long table:%s line:%s \n  error:%v  prefix:%v", path, line, __, prefix))
		}

		var linestr string = string(line)
		if len(linestr) <= 0 {
			return nil
		}
		if linestr[0] == '#' {
			continue
		}
		slic := strings.Split(linestr, "\t")
		if len(slic) <= 0 {
			return errors.New(fmt.Sprintf("split error path:%s", path))
		}
		//tbl := inter.GetTableStruct()
		//
		//er := parseSlice(tbl, slic)
		//if er != nil {
		//	return er
		//}
		//inter.InsertTable(tbl)
	}
	return nil
}

func parseSlice(any interface{}, slic []string) error {
	tp := reflect.TypeOf(any)
	if tp.Kind() != reflect.Ptr {
		return errors.New("param is not ptr")
	}
	tp = tp.Elem()
	if tp.Kind() != reflect.Struct {
		return errors.New("param is not struct")
	}

	//tagTp := reflect.TypeOf(tp)

	v := reflect.ValueOf(any)
	v = v.Elem()

	//if len(slic) < tp.NumField() {
	//	return errors.New(fmt.Sprintf("length %d numfield %d \n", len(slic), v.NumField()))
	//}

	for i := 0; i < tp.NumField() && i < len(slic); i++ {
		if !v.Field(i).CanSet() {
			continue
		}
		tags := tp.Field(i).Tag
		tag := tags.Get("table")
		if tag == "ignore" {
			continue
		}
		switch v.Field(i).Kind() {
		case reflect.Int:
			if slic[i] == "#" {
				v.Field(i).SetInt(0)
				break
			}
			value, __ := strconv.Atoi(slic[i])
			if __ != nil {
				return __
			}
			v.Field(i).SetInt(int64(value))
			break
		case reflect.Int32:
			if slic[i] == "#" {
				v.Field(i).SetInt(0)
				break
			}
			value, __ := strconv.Atoi(slic[i])
			if __ != nil {
				return __
			}
			v.Field(i).SetInt(int64(value))
			break
		case reflect.Int64:
			if slic[i] == "#" {
				v.Field(i).SetInt(0)
				break
			}
			value, __ := strconv.ParseInt(slic[i], 10, 64)
			if __ != nil {
				return __
			}
			v.Field(i).SetInt(value)
			break
		case reflect.Float32:
			if slic[i] == "#" {
				v.Field(i).SetFloat(0)
				break
			}
			value, __ := decimal.NewFromString(slic[i])
			//value, __ := strconv.ParseFloat(slic[i], 32)
			if __ != nil {
				return __
			}
			f, _ := value.Float64()
			//if !is {
			//	return errors.New(fmt.Sprintf("table load float prase not exactly %v", slic))
			//}
			//fmt.Println(fmt.Sprintf("%", f))
			v.Field(i).SetFloat(f)
			break
		case reflect.Float64:
			if slic[i] == "#" {
				v.Field(i).SetFloat(0)
				break
			}
			value, __ := decimal.NewFromString(slic[i])
			//value, __ := strconv.ParseFloat(slic[i], 64)
			if __ != nil {
				return __
			}
			f, _ := value.Float64()
			//if !is {
			//	return errors.New(fmt.Sprintf("table load float prase not exactly %v", slic))
			//}
			//fmt.Println(fmt.Sprintf("%v", f))
			v.Field(i).SetFloat(f)
			break
		case reflect.String:
			if slic[i] == "#" {
				v.Field(i).SetString("")
				break
			}
			v.Field(i).SetString(slic[i])
			break
		case reflect.Uint:
			if slic[i] == "#" {
				v.Field(i).SetInt(0)
				break
			}

			value, __ := strconv.ParseUint(slic[i], 10, 64)
			if __ != nil {
				return __
			}
			v.Field(i).SetUint(value)
		default:

			fmt.Println(v.Field(i).Kind())
			return errors.New("not support type")
		}
	}
	return nil
}

func main() {

	fileName := "./Zw0503.xlsx"
	sheetName := "main"
	targetFile := "./zw0515.xlsx"
	targetSheet := sheetName


	phone, email, bank, vpa, id, err := OutDiff(fileName, sheetName, targetFile, targetSheet)
	if err != nil {
		fmt.Printf("load excel error %v", err)
		return
	}

	emailStr := strings.Join(email, "\n")
	phoneStr := strings.Join(phone, "\n")
	IDStr := strings.Join(id, "\n")
	bankStr := strings.Join(bank, "\n")
	vpaStr := strings.Join(vpa, "\n")

	fmt.Printf("write bank.txt %v:%v\n", ioutil.WriteFile("./bank.txt", []byte(bankStr),  fs.ModePerm), bankStr)
	fmt.Printf("write vpa.txt %v:%v\n", ioutil.WriteFile("./vpa.txt", []byte(vpaStr),  fs.ModePerm), vpaStr)
	fmt.Printf("write bank.txt %v:%v\n", ioutil.WriteFile("./identify.txt", []byte(IDStr),  fs.ModePerm), IDStr)
	fmt.Printf("write email.txt %v:%v\n", ioutil.WriteFile("./email.txt", []byte(emailStr),  fs.ModePerm), emailStr)
	fmt.Printf("write phone.txt %v:%v\n", ioutil.WriteFile("./phone.txt", []byte(phoneStr),  fs.ModePerm), phoneStr)
}
