module parseXLS

go 1.16

require (
	github.com/shopspring/decimal v1.3.1
	github.com/tealeg/xlsx v1.0.5
)
