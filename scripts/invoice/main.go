package main

import (
	"bufio"
	"fmt"
	"github.com/go-ole/go-ole"
	"github.com/go-ole/go-ole/oleutil"
	"github.com/xuri/excelize/v2"
	"io"
	"io/fs"
	"os"
	"path"
	"strconv"
	"strings"
	"time"
)

const invOutFileFmt = "./output/%v/%v-inv.xlsx"
const invOutPDFFmt = "./output/%v/%v-inv.pdf"
const invTemplateFmt = "invoice-%v.xlsx"

const argumentDetailFmt = "./data/%v.xlsx"
const argumentDetailFmt2 = "./data/%v.xls"
const argumentUidDetailFmt = "./data/%v/%v.xlsx"
const argumentUidDetailFmt2 = "./data/%v/%v.xls"

// localTime := time.Now()
// // 8*60*60 也可用 int((8 * time.Hour).Seconds()) 表示
var IndiaZone = time.FixedZone("UTC+5.5", 5.5*60*60) // 印度时间
//cusTime := localTime.In(cusZone)
//fmt.Println(cusTime)

type Invoice struct {
	Channel       string
	Uid           string
	OrderId       string
	OrderUserName string
	CBNo          string
	Amount        string
	Date          string
	Raw           string
}

func (i Invoice) OutFileName() string {
	return fmt.Sprintf(invOutFileFmt, i.Uid, i.OrderId)
}

func (i Invoice) PDFFileName() string {
	return fmt.Sprintf(invOutPDFFmt, i.Uid, i.OrderId)
}

func (i Invoice) TemplateFileName() string {
	return fmt.Sprintf(invTemplateFmt, i.Channel)
}

func read(filename string) map[string]Invoice {
	f, err := os.Open(filename)
	if err != nil {
		panic(fmt.Sprintf("read file[%v] error:%v", filename, err))
	}

	defer f.Close()

	ret := make(map[string]Invoice)
	reader := bufio.NewReaderSize(f, 1024*1024*8)
	for {
		line, _, err := reader.ReadLine()
		if err == io.EOF {
			break
		}

		s := strings.TrimSpace(string(line))
		if s == "" || s[0] == '#' {
			continue
		}

		values := strings.Split(s, ",")
		if len(values) != 7 {
			panic(fmt.Sprintf("invalid invoice data %v", s))
		}

		v := make([]string, 0, 16)
		for idx, item := range values {
			iv := strings.TrimSpace(item)
			if iv == "" {
				panic(fmt.Sprintf("[%v] has invalid field value[%v]", s, idx))
			}

			v = append(v, iv)
		}

		orderId := strings.TrimSpace(v[2])
		amount, _ := strconv.ParseFloat(v[5], 64)
		if amount < 1 {
			panic(fmt.Sprintf("[%v] has invalid amount value[%v]", s, v[4]))
		}
		//fmt.Printf("%v-->%.2f\n", v[4], amount)

		unix, _ := strconv.ParseInt(strings.TrimSpace(v[6]), 10, 64)
		if unix < 0 { // 印度时差20小时
			panic(fmt.Sprintf("[%v] has invalid time value[%v]", s, v[5]))
		}

		date := time.Unix(unix, 0).In(IndiaZone).Format("2006-01-02 15:04:05")
		//fmt.Printf("%v-->%v\n", v[5], date)
		ret[orderId] = Invoice{
			Channel:       strings.TrimSpace(v[0]),
			Uid:           strings.TrimSpace(v[1]),
			OrderId:       strings.TrimSpace(v[2]),
			OrderUserName: strings.TrimSpace(v[3]),
			CBNo:          strings.TrimSpace(v[4]),
			Amount:        fmt.Sprintf("%.2f", amount),
			Date:          date,
			Raw:           s,
		}
	}

	return ret
}

func Log(fname string, successCnt, failCnt int, success map[string][]string, failOut map[string][]string, failFmt map[string][]string, failNoData map[string][]string) {
	f, err := os.Create(fname)
	if err != nil {
		panic(fmt.Sprintf("read file[%v] error:%v", fname, err))
	}

	defer f.Close()

	f.WriteString("************************************************************************\n")
	f.WriteString(fmt.Sprintf("-->>success count:%v\n\n", successCnt))
	for k, v := range success {
		f.WriteString(fmt.Sprintf("uid:%v\n%v%v\n\n", k, space, strings.Join(v, "\n"+space)))
	}

	f.WriteString(fmt.Sprintf("\n\n\n-->>fail count :%v\n\n", failCnt))
	for k, v := range failNoData {
		f.WriteString(fmt.Sprintf("%v-->没有提供玩家游戏数据:\n%v%v\n\n", k, space, strings.Join(v, "\n"+space)))
	}
	for k, v := range failFmt {
		f.WriteString(fmt.Sprintf("%v-->玩家游戏数据文件格式不支持:\n%v%v\n\n", k, space, strings.Join(v, "\n"+space)))
	}
	for k, v := range failOut {
		f.WriteString(fmt.Sprintf("%v-->生成invoice文件错误:\n%v%v\n\n", k, space, strings.Join(v, "\n"+space)))
	}

	f.WriteString("**************************************************************************\n\n\n")
}

type Argument struct {
	UserName           string
	BeneficiaryAccount string
	Mobile             string
	Email              string
	IP                 string
}

func LoadArgument(filename string, sheetIndex int) (a Argument, b bool) {
	f, err := excelize.OpenFile(filename)
	if err != nil {
		fmt.Printf("open file[%v] error:%v \n", filename, err)
		return
	}

	defer f.Close()

	sheetName := f.GetSheetName(sheetIndex)

	userName, _ := f.GetCellValue(sheetName, "A3")
	userName = strings.TrimSpace(userName)
	if userName == "" {
		fmt.Printf("%v not found user name\n", filename)
		return
	}

	account, _ := f.GetCellValue(sheetName, "B3")
	account = strings.TrimSpace(account)
	//if account == "" {
	//	fmt.Printf("%v not found account\n", filename)
	//	return
	//}

	mobile, _ := f.GetCellValue(sheetName, "C3")
	mobile = strings.TrimSpace(mobile)
	//if mobile == "" {
	//	fmt.Printf("%v not found mobile\n", filename)
	//	return
	//}

	email, _ := f.GetCellValue(sheetName, "D3")
	email = strings.TrimSpace(email)
	//if email == "" {
	//	fmt.Printf("%v not found email\n", filename)
	//	return
	//}

	IP, _ := f.GetCellValue(sheetName, "E3")
	IP = strings.TrimSpace(IP)

	a = Argument{
		UserName:           userName,
		BeneficiaryAccount: account,
		Mobile:             mobile,
		Email:              email,
		IP:                 IP,
	}
	b = true

	return
}

// func OutInvoice(inv Invoice, arg Argument) error {
func OutInvoice(inv Invoice) error {
	templateFile := inv.TemplateFileName()
	f, err := excelize.OpenFile(templateFile)
	if err != nil {
		return fmt.Errorf("open file[%v] error:%v\n", templateFile, err)
	}

	sheetName := f.GetSheetName(0)

	//
	//BillTo := sheet.Rows[6].Cells[1]
	//BillTo.SetString(BillTo.Value+arg.UserName)
	srcValue, err := f.GetCellValue(sheetName, "B7")
	if err != nil {
		fmt.Printf("[%v] Get BillTo error", templateFile)
		return err
	}
	//f.SetCellStr(sheetName, "B7", srcValue+arg.UserName)
	f.SetCellStr(sheetName, "B7", srcValue+inv.OrderUserName)

	//
	//InvoiceNo := sheet.Rows[6].Cells[6]
	//InvoiceNo.SetString(InvoiceNo.Value+inv.OrderId)
	srcValue, err = f.GetCellValue(sheetName, "G7")
	if err != nil {
		fmt.Printf("[%v] Get InvoiceNo error", templateFile)
		return err
	}
	f.SetCellStr(sheetName, "G7", srcValue+inv.OrderId)

	//
	//CBNo := sheet.Rows[8].Cells[6]
	//CBNo.SetString(CBNo.Value + inv.CBNo)
	srcValue, err = f.GetCellValue(sheetName, "B9")
	if err != nil {
		fmt.Printf("[%v] Get CBNo error", templateFile)
		return err
	}
	f.SetCellStr(sheetName, "B9", srcValue+inv.CBNo)

	//
	//Date := sheet.Rows[8].Cells[2]
	//Date.SetString(Date.Value+inv.Date)
	srcValue, err = f.GetCellValue(sheetName, "G9")
	if err != nil {
		fmt.Printf("[%v] Get Date error", templateFile)
		return err
	}
	f.SetCellStr(sheetName, "G9", srcValue+inv.Date)

	//
	//Amount1 := sheet.Rows[11].Cells[8]
	//Amount2 := sheet.Rows[11].Cells[10]
	//Amount3 := sheet.Rows[12].Cells[8]
	//Amount4 := sheet.Rows[15].Cells[9]
	//Amount5 := sheet.Rows[16].Cells[9]
	//Amount1.SetString(Amount1.Value+inv.Amount)
	//Amount2.SetString(Amount2.Value+inv.Amount)
	//Amount3.SetString(Amount3.Value+inv.Amount)
	//Amount4.SetString(Amount4.Value+inv.Amount)
	//Amount5.SetString(Amount5.Value+inv.Amount)
	srcValue, err = f.GetCellValue(sheetName, "I12")
	if err != nil {
		fmt.Printf("[%v] Get I12 Amount error", templateFile)
		return err
	}
	f.SetCellStr(sheetName, "I12", srcValue+inv.Amount)

	srcValue, err = f.GetCellValue(sheetName, "K12")
	if err != nil {
		fmt.Printf("[%v] Get K12 Amount error", templateFile)
		return err
	}
	f.SetCellStr(sheetName, "K12", srcValue+inv.Amount)

	srcValue, err = f.GetCellValue(sheetName, "I13")
	if err != nil {
		fmt.Printf("[%v] Get I13 Amount error", templateFile)
		return err
	}
	f.SetCellStr(sheetName, "I13", srcValue+inv.Amount)

	srcValue, err = f.GetCellValue(sheetName, "J16")
	if err != nil {
		fmt.Printf("[%v] Get J16 Amount error", templateFile)
		return err
	}
	f.SetCellStr(sheetName, "J16", srcValue+inv.Amount)

	srcValue, err = f.GetCellValue(sheetName, "J17")
	if err != nil {
		fmt.Printf("[%v] Get J17 Amount error", templateFile)
		return err
	}
	f.SetCellStr(sheetName, "J17", srcValue+inv.Amount)

	//
	//OrderId := sheet.Rows[16].Cells[1]
	//OrderId.SetString(OrderId.Value+inv.OrderId)
	srcValue, err = f.GetCellValue(sheetName, "B17")
	if err != nil {
		fmt.Printf("[%v] Get B17 OrderId error", templateFile)
		return err
	}
	f.SetCellStr(sheetName, "B17", srcValue+inv.OrderId)

	outFile := inv.OutFileName()

	outDir := path.Dir(outFile)
	err = os.MkdirAll(outDir, fs.ModePerm)
	if err != nil {
		return err
	}

	err = f.SaveAs(outFile)
	if err != nil {
		return err
	}

	dir, err := os.Getwd()
	if err != nil {
		return err
	}

	outFile = path.Join(dir, outFile)
	pdfFile := path.Join(dir, inv.PDFFileName())
	fmt.Printf("%v--->%v\n", outFile, pdfFile)
	office_excel2pdf(outFile, pdfFile)
	return nil
}

func office_excel2pdf(fileName string, pdfPath string) {
	ole.CoInitialize(0)
	defer ole.CoUninitialize()
	unknown, _ := oleutil.CreateObject("Excel.Application")
	defer unknown.Release()
	excel, _ := unknown.QueryInterface(ole.IID_IDispatch)
	defer excel.Release()
	oleutil.PutProperty(excel, "Visible", false)
	workbooks := oleutil.MustGetProperty(excel, "Workbooks").ToIDispatch()
	defer workbooks.Release()
	workbook := oleutil.MustCallMethod(workbooks, "Open", fileName).ToIDispatch()
	defer workbook.Release()
	worksheet := oleutil.MustGetProperty(workbook, "Worksheets", 1).ToIDispatch()
	defer worksheet.Release()
	oleutil.MustCallMethod(worksheet, "ExportAsFixedFormat", 0, pdfPath).ToIDispatch()
	oleutil.CallMethod(workbook, "Close")
	oleutil.CallMethod(excel, "Quit")
	println("success")
}

//func DebugFile(filename string, sheetIndex int) {
//	f, err := xlsx.OpenFile(filename)
//	if err != nil {
//		fmt.Printf("open file[%v] error:%v \n", filename, err)
//		return
//	}
//
//	if len(f.Sheets) <= sheetIndex {
//		fmt.Printf("[%v] invalid file formate, no found detail sheet\n", filename)
//		return
//	}
//
//	sheet := f.Sheets[sheetIndex]
//
//	for rowIndex, row := range sheet.Rows {
//		values := make([]string, 0, 128)
//		for idx, cell := range row.Cells {
//			values = append(values, fmt.Sprintf("%v-%v", idx, cell.Value))
//		}
//
//		fmt.Printf("%v-->%v\n", rowIndex, strings.Join(values, ","))
//	}
//}

func Add(data map[string][]string, uid string, value string) {
	v, ok := data[uid]
	if !ok {
		v = make([]string, 0, 128)
	}
	v = append(v, value)

	data[uid] = v
}

func argumentFileName(uid string, orderId string) string {
	argueFile := fmt.Sprintf(argumentDetailFmt, orderId)
	_, err := os.Stat(argueFile)
	if err == nil {
		return argueFile
	}

	if os.IsNotExist(err) {
		argueFile = fmt.Sprintf(argumentDetailFmt2, orderId)
	}
	_, err = os.Stat(argueFile)
	if err == nil {
		return argueFile
	}

	if os.IsNotExist(err) {
		argueFile = fmt.Sprintf(argumentUidDetailFmt, uid, orderId)
	}
	_, err = os.Stat(argueFile)
	if err == nil {
		return argueFile
	}

	if os.IsNotExist(err) {
		argueFile = fmt.Sprintf(argumentUidDetailFmt2, uid, orderId)
	}
	_, err = os.Stat(argueFile)
	if err == nil {
		return argueFile
	}

	return ""
}

const space = "  "

func main() {
	fmt.Printf("\n-->>run log-----------\n")

	invoices := read("./data/invoice.txt")

	success := make(map[string][]string)
	failOut := make(map[string][]string)
	failFmt := make(map[string][]string)
	failNoData := make(map[string][]string)

	successCount := 0
	failCount := 0
	for _, v := range invoices {
		//argueFile := argumentFileName(v.Uid, v.OrderId)
		//if argueFile == "" {
		//	Add(failNoData, v.Uid, v.Raw)
		//	failCount += 1
		//	continue
		//}
		//
		//argument, b := LoadArgument(argueFile, 0)
		//if !b {
		//	Add(failFmt, v.Uid, v.Raw)
		//	failCount += 1
		//	continue
		//}

		//err := OutInvoice(v, argument)
		err := OutInvoice(v)
		if err != nil {
			Add(failOut, v.Uid, fmt.Sprintf("%v,%v", v.Raw, err))
			failCount += 1
			continue
		}

		Add(success, v.Uid, v.OrderId)
		successCount += 1
	}

	fname := fmt.Sprintf("inv-%v-result.log", time.Now().Format("2006-01-02-15"))
	Log(fname, successCount, failCount, success, failOut, failFmt, failNoData)

	//fmt.Printf("\n\n************************************************************************\n")
	fmt.Printf("\n\n-->>success count:%v\n", successCount)
	//for k, v := range success {
	//	fmt.Printf("uid:%v\n%v%v\n\n", k, space, strings.Join(v, "\n"+space))
	//}
	//
	fmt.Printf("-->>fail count :%v\n\n", failCount)
	fmt.Printf("-->>运行详情 [%v]\n\n\n", fname)
	//for k, v := range failNoData {
	//	fmt.Printf("%v-->没有提供玩家游戏数据:\n%v%v\n\n", k, space, strings.Join(v, "\n"+space))
	//}
	//for k, v := range failFmt {
	//	fmt.Printf("%v-->玩家游戏数据文件格式不支持:\n%v%v\n\n", k, space, strings.Join(v, "\n"+space))
	//}
	//for k, v := range fail {
	//	fmt.Printf("%v-->生成invoice文件错误:\n%v%v\n\n", k, space, strings.Join(v, "\n"+space))
	//}
	//
	//fmt.Printf("**************************************************************************\n\n\n")
	//DebugFile("./invoice-airpay2.xlsx", 0)
}
