module inv

go 1.16

require (
	github.com/go-ole/go-ole v1.2.6
	github.com/xuri/excelize/v2 v2.7.1
)
