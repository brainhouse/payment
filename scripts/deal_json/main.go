package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

func read(filename string) [][]byte {
	f, err := os.Open(filename)
	if err != nil {

	}

	defer f.Close()

	lines := make([][]byte, 0, 128)
	reader := bufio.NewReaderSize(f, 1024*1024*8)
	for {
		line, _, err := reader.ReadLine()
		if err == io.EOF {
			break
		}

		d := make([]byte, 0, len(line)+1)
		//copy(d, line)
		d = append(d, line...)
		lines = append(lines, d)
		//fmt.Printf("size:%d==>%v\n\n\n", len(line), string(line))
	}

	return lines
}

type Trans struct {
	Id     int     `json:"id"`
	DataId int     `json:"data_id"`
	Uid    int     `json:"uid"`
	Amount string  `json:"amount"`
	AF     float64 `json:"af"`
}

func main() {
	ret := make(map[int][]Trans)
	stat := make(map[int]float64)

	lines := read("143.log")
	for _, line := range lines {
		var t Trans
		err := json.Unmarshal(line, &t)
		if err != nil {
			fmt.Printf("unmarshal [%v]  error: %v", string(line), err)
			break
		}

		v, err := strconv.ParseFloat(t.Amount, 64)
		if err != nil || v < 1{
			fmt.Printf("convert[%v] error: %v", t.Amount, err)
			break
		}
		t.AF = v

		ar, ok := ret[t.Uid]
		if !ok {
			ar = make([]Trans, 0, 128)
		}

		ar = append(ar, t)
		ret[t.Uid] = ar

		val, ok := stat[t.Uid]
		if !ok {
			val = 0
		}

		val += t.AF
		stat[t.Uid] = val
	}


	for k, v := range stat {
		count := len(ret[k])
		fmt.Printf("%v->%v,%v\n", k, count, v)
	}

	for k, v := range ret {
		fmt.Printf("user-->%v\n", k)
		dataIds := make([]string, 0, 128)
		for _, item := range v {
			dataIds = append(dataIds, fmt.Sprintf("%v", item.DataId))
		}

		fmt.Printf("ids:%v\n", strings.Join(dataIds, ","))
	}
}
