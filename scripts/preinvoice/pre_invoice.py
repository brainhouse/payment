#!/usr/bin/python3
# -*- coding: UTF-8 -*-

import pymysql
import openpyxl
import time
from os import path

def read_file(fname):
    if not path.exists(fname):
        return []

    fio = open(fname, 'r')
    lines = fio.readlines()
    body = []
    for line in lines:
        lin = line.strip()
        body.append(lin)

    fio.close()
    return body

def list_to_dict(lines):
    r_dict = {}
    for line in lines:
        values = line.split(',')
        order_id = values[0]
        cbNumber = values[1]
        cn = r_dict.get(order_id, -1)
        if cn == -1:
            r_dict[order_id] = cbNumber
            continue

        print("invalid order-->%s,%d" % (order_id, cbNumber))

    return r_dict


def connect_db(host, user, pwd, database):
    db = pymysql.connect(host=host,
                         user=user,
                         password=pwd,
                         database=database,
                         port=3306,
                         charset='utf8')

    return db


def get_payin(db, order_id):
    ret = []
    cursor = db.cursor()
    try:
        sql = "SELECT app_id, uid, pay_account_id, app_order_id, order_id, amount, created, user_name FROM pay_entry where order_id = '%s'" % order_id

        row_cnt = cursor.execute(sql)
        #print("sql[%s]--> %d" % (order_id, row_cnt))

        while row_cnt > 0:
            res = cursor.fetchone()
            ret.append({"app_id": res[0], "uid": res[1], "pay_account_id": res[2], "app_order_id": res[3], "order_id": res[4], "amount": res[5], "created": res[6], "user_name":res[7]})
            row_cnt -= 1

    except Exception as err:
        print("select pay entry error", err)

    cursor.close()
    return ret[0]

#def unixToBJ(unix):
#    time_tuple = time.localtime(unix + 3600*8)
#    return time.strftime("%Y/%m/%d %H:%M:%S", time_tuple)

def output_excel(excel, head, data):
    wb=openpyxl.Workbook()
    sheet=wb.active

    sheet.append(head)
    for row in data:
        sheet.append(row)

    wb.save(excel)

# connect
db = connect_db("payment-instance-1.cqynyhwicvd6.ap-south-1.rds.amazonaws.com", "payment", "$O1gRVu3vV#9qK*m", "payment")

# search
lines = read_file('order_list.txt')
orders = list_to_dict(lines)
print("total order:%d" % len(orders))


print('\ninvoice data---------------------------------------')
result_dict = {}
result_order_infos = {}
for order_id, number in orders.items():
    #print('kv---%s-->%s' % (order_id, number))
    order_info = get_payin(db, order_id)
    order_info["cbnumber"] = number
    result_order_infos[order_id] = order_info

    # airpay2,162,AIN16821079640416403656,315823779459,10000,2023-06-07
    uid = order_info["uid"]
    #print(order_info)
    invoice = "%s,%d,%s,%s,%s,%s,%d" % (order_info["pay_account_id"], order_info["uid"], order_info["order_id"], order_info["user_name"], number, order_info["amount"], order_info["created"])
    print(invoice)


    order_group = result_dict.get(uid, -1)
    if order_group == -1:
        result_dict[uid] = [order_id]
        continue

    order_group.append(order_id)
    result_dict[uid] = order_group


print('\nclient order data-----------------------------------')
for uid, group in result_dict.items():
    print("%s--->" % uid)
    for oid in group:
        print("  %s" % oid)


cmd = """curl --location --request POST 'http://127.0.0.1:6063/v1/internal/charge_rollback_bat' -H 'Content-Type: application/json' -d '{"order_ids":[]}'"""
print('\nreback order data-----------------------------------')
print(cmd)

order_ids = []
order_datas = []
date = time.strftime("%Y/%m/%d", time.localtime(time.time()))
for uid, group in result_dict.items():
    order_ids.extend(group)

    for oid in group:
        order_info = result_order_infos.get(oid, -1)
        if order_info == -1:
            print("result_order_infos not found %s" % oid)
            continue


        order_datas.append([date, oid, order_info["amount"], order_info["app_id"], order_info["uid"], "", "", "", order_info["pay_account_id"], order_info["cbnumber"]])

order_ids_str = '","'.join(order_ids)
print("{\"order_ids\":[\"%s\"]}" %  order_ids_str)


head = ["日期", "支付订单", "金额", "appid", "uid", "申诉状态", "fp后台", "", "总计"]
suffix = time.strftime("%Y-%m-%d-%H", time.localtime())
output_excel("customer_complaint_%s.xlsx" % suffix, head, order_datas)

#payin_list = get_payin(db, order_id)
#for i in payin_list:
#    id = i["id"]
#    order_id = i["order_id"]
#    if id == 0:
#        continue

