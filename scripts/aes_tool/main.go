package main

import (
	"flag"
	"fmt"
	"funzone_pay/utils"
	"os"
	"path"
)

var fileName string
var enc bool
var key string

func init() {
	flag.BoolVar(&enc, "enc", true, fmt.Sprintf("encrypt or decrypt, default true"))
	flag.StringVar(&fileName, "f", "./config.toml", fmt.Sprintf("config file "))
	flag.StringVar(&key, "key", "xxx", fmt.Sprintf("enc key"))
	flag.Parse()
}

func main() {
	data, err := os.ReadFile(fileName)
	if err != nil {
		fmt.Printf("\nread file error:%v\n", err)
		return
	}

	var result []byte
	var revert []byte
	if enc {
		result, err = utils.AesEncrypt(data, []byte(key))
		if err != nil {
			fmt.Printf("\nencrypt file error:%v\n", err)
			return
		}

		revert, err = utils.AesDecrypt(result, []byte(key))
		if err != nil {
			fmt.Printf("\ndeal file error:%v\n", err)
			return
		}
	} else {
		result, err = utils.AesDecrypt(data, []byte(key))
		if err != nil {
			fmt.Printf("\ndeal file error:%v\n", err)
			return
		}

		revert, err = utils.AesEncrypt(result, []byte(key))
		if err != nil {
			fmt.Printf("\nencrypt file error:%v\n", err)
			return
		}
	}

	for idx, b := range data {
		if b != revert[idx] {
			fmt.Printf("\n\n Encrypt Or Decrypt Fail....not match\n\n")
			return
		}
	}

	_, name := path.Split(fileName)
	err = os.MkdirAll("./out", os.ModePerm)
	if err != nil {
		fmt.Printf("\ncreate out put file error:%v\n", err)
		return
	}

	err = os.WriteFile("./out/"+name, result, os.ModePerm)
	if err != nil {
		fmt.Printf("\ncreate out put file error:%v\n", err)
	}

	return
}
