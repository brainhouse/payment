#!/usr/bin/python3
# -*- coding: UTF-8 -*-

import pymysql
import openpyxl
import time

# from os import path

def unixToBJ(unix):
    time_tuple = time.localtime(unix + 3600*8)
    return time.strftime("%Y/%m/%d %H:%M:%S", time_tuple)

def output_excel(excel, head, data):
    wb=openpyxl.Workbook()
    sheet=wb.active

    sheet.append(head)
    for row in data:
        sheet.append(row)

    wb.save(excel)

def connect_db(host, user, pwd, database):
    db = pymysql.connect(host=host,
                         user=user,
                         password=pwd,
                         database=database,
                         port=3306,
                         charset='utf8')

    return db

def get_all_payout(db, app_id, status, start, end):
    ret = []
    cursor = db.cursor()
    try:
        # sql = "SELECT a.uid, gold, statement_gold FROM account as a LEFT JOIN user as u ON a.uid = u.uid WHERE a.createtime >= %d AND a.createtime <=%d AND u.channel = '144'" % (start, end)
        where = "WHERE app_id = '%s' AND status = %d" % (app_id, status)
        if start > 0 or end > 0 :
            where = "WHERE created > %d AND created < %d AND app_id = '%s' AND status = %d" % (start, end, app_id, status)

        sql = "SELECT uid, app_id, order_id, app_order_id, amount, fee, status, user_name, phone, email, country_code, created, updated FROM pay_out " + where

        row_cnt = cursor.execute(sql)
        print("sql[%s]--> %d" % (sql, row_cnt))

        while row_cnt > 0:
            res = cursor.fetchone()

            ar = [x for x in res[:-2]]
            ar.append(res[-2])
            ar.append(unixToBJ(res[-2]))
            ar.append(res[-1])
            ar.append(unixToBJ(res[-1]))

            ret.append(ar)
            row_cnt -= 1

    except Exception as err:
        print("select pay out error", err)

    cursor.close()
    return ret


def get_all_payin(db, app_id, status, start, end):
    ret = []
    cursor = db.cursor()
    try:
        # sql = "SELECT a.uid, gold, statement_gold FROM account as a LEFT JOIN user as u ON a.uid = u.uid WHERE a.createtime >= %d AND a.createtime <=%d AND u.channel = '144'" % (start, end)
        where = "WHERE app_id = '%s' AND status = %d" % (app_id, status)
        if start > 0 or end > 0 :
            where = "WHERE created > %d AND created < %d AND app_id = '%s' AND status = %d" % (start, end, app_id, status)

        sql = "SELECT uid, app_id, order_id, app_order_id, amount, fee, status, user_name, phone, email, country_code, created, updated FROM pay_entry " + where

        row_cnt = cursor.execute(sql)
        print("sql[%s]--> %d" % (sql, row_cnt))

        while row_cnt > 0:
            res = cursor.fetchone()

            ar = [x for x in res[:-2]]
            ar.append(res[-2])
            ar.append(unixToBJ(res[-2]))
            ar.append(res[-1])
            ar.append(unixToBJ(res[-1]))

            ret.append(ar)
            row_cnt -= 1

    except Exception as err:
        print("select pay entry error", err)

    cursor.close()
    return ret

# connect
db = connect_db("payment-instance-1.cqynyhwicvd6.ap-south-1.rds.amazonaws.com", "payment", "$O1gRVu3vV#9qK*m", "payment")

# search
start = 1680278400
end = 1682870400
app_id = "RummyReality123"
payout_list = get_all_payout(db, app_id, 1, start, end)

payin_list = get_all_payin(db, app_id, 1, start, end)

head = ["ID", "app_id", "订单ID", "商户订单ID", "金额", "手续费", "状态", "用户名", "手机号", "邮箱", "国家", "创建时间", "北京时间", "最后更新时间", "北京时间"]
output_excel("%s_pay_in.xlsx" % app_id, head, payin_list)
output_excel("%s_pay_out.xlsx" % app_id,head, payout_list)
