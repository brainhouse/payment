
use payment;

ALTER TABLE `pay_entry` ADD COLUMN `discount` DECIMAL(20,4) NOT NULL DEFAULT '0.0000' COMMENT '一些渠道给的折扣信息：订单金额 - 实际付款金额' AFTER `fee`;