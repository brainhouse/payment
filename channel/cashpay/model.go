package cashpay

/*
{
    "amount": "200",
    "notifyUrl": "https://www.abc.com/notify/gupay",
    "merchantOrderId": "6c16fff6-9c53-4d59-bd55-f567d9207411"
}
*/

type PayInReq struct {
	Amount          string `json:"amount"` // 单位：分
	NotifyUrl       string `json:"notifyUrl"`
	MerchantOrderId string `json:"merchantOrderId"`
}

/*
{
    "code": 200,
    "msg": "success",
    "status": "00",
    "payUrl": "https://pix.cashpag.com/s3/checkout-page/v3.html?cid=expireAt%202023-02-23T18:49:03.824701&sid=S110202302231628859489161773056&amount=1.22&qrcode=00020101021226910014br.gov.bcb.pix2569api.developer.btgpactual.com%2Fv1%2Fp%2Fv2%2Ff846fb70503d4869afbd3806976cd8cb5204000053039865802BR5907CASHPAY6007Barueri61080120100562070503***6304FBC5",
    "qrcodeRaw": "00020101021226910014br.gov.bcb.pix2569api.developer.btgpactual.com/v1/p/v2/f846fb70503d4869afbd3806976cd8cb5204000053039865802BR5907CASHPAY6007Barueri61080120100562070503***6304FBC5",
    "amount": "122",
    "fee": "6.1",
    "createTimeL": 1677185339619,
    "orderId": "S110202302231628859489161773056",
    "merchantOrderId": "14c45be8-7fe5-4b13-a302-c7ee761ab9fd",
    "payType": "110",
    "sign": "0dae7bafad235516a186c14234855b0f",
    "traceId": "i160,i160223bd4f4ad58e2742d780a46800422891ad"
}
*/

type PayInResponse struct {
	Code            int    `json:"code"` // 响应码.正常:200,查询不到:404,失败:500,重复亲求:425
	Msg             string `json:"msg"`
	Status          string `json:"status"` // 订单状态.待提交:99,支付中:00,成功:01,失败:02,超时:03,退款:04
	PayUrl          string `json:"payUrl"`
	QrcodeRaw       string `json:"qrcodeRaw"`
	Amount          string `json:"amount"` // 订单金额(分)
	Fee             string `json:"fee"`
	RealPayAmount   string `json:"realPayAmount"`  // 实际付款金额(分)
	CreateTimeL     int64  `json:"createTimeL"`
	OrderId         string `json:"orderId"` // 平台订单id
	MerchantOrderId string `json:"merchantOrderId"` // 商户订单id
	PayType         string `json:"payType"`
	Sign            string `json:"sign"`
	TraceId         string `json:"traceId"`
}

/*
{
    "code": 200,
    "msg": "PAID",
    "status": "01",
    "payUrl": "https://pay2.mtbtop1.com/s3/checkout-page/v3.html?cid=expireAt%202023-03-11T23:02:28.707566&sid=S110202303111634721486436782080&amount=20&qrcode=00020101021226790014br.gov.bcb.pix2557brcode.starkinfra.com/v2/79f05d1cfba545e6be1352339026613f5204000053039865802BR5925Conect%20World%20Comercio,%20Im6009Sao%20Paulo62070503***630455FC",
    "endToEndId": "e08561701202303120103i5gsv8nm4xz",
    "amount": "2000",
    "fee": "40",
    "realPayAmount": "2000",
    "createTimeL": 1678582948680,
    "orderId": "S110202303111634721486436782080",
    "merchantOrderId": "S110202303111634721486239629312",
    "payType": "110",
    "sign": "931f31f3c6013d8ae01a8016ae595d1d",
    "traceId": "i210,i21031128d291b539b84025acedf30448d1dd72"
}
*/

type QueryInResponse struct {
	Code            int    `json:"code"` // 响应码.正常:200,查询不到:404,失败:500,重复亲求:425
	Msg             string `json:"msg"`
	Status          string `json:"status"` // 订单状态.待提交:99,支付中:00,成功:01,失败:02,超时:03,退款:04
	PayUrl          string `json:"payUrl"`
	EndToEndId      string `json:"endToEndId"`
	Amount          string `json:"amount"`
	Fee             string `json:"fee"`
	RealPayAmount   string `json:"realPayAmount"`
	CreateTimeL     int64  `json:"createTimeL"`
	OrderId         string `json:"orderId"`
	MerchantOrderId string `json:"merchantOrderId"`
	PayType         string `json:"payType"`
	Sign            string `json:"sign"`
	TraceId         string `json:"traceId"`
}

/*
{
    "amount": "150",
    "merchantOrderId": "d869e074-57d1-4e1b-8cd7-d46d763b6ac8",
    "notifyUrl": "https://pre.hzxb.pro/health_check/print",
    "accountType": "CPF",
    "accountNum": "23613276860",
    "customerName": "zhangsan",
    "customerCert": "23613276860"
}
*/

type PayOutReq struct {
	Amount          string `json:"amount"`
	MerchantOrderId string `json:"merchantOrderId"`
	NotifyUrl       string `json:"notifyUrl"`
	AccountType     string `json:"accountType"`
	AccountNum      string `json:"accountNum"`
	CustomerName    string `json:"customerName"`
	CustomerCert    string `json:"customerCert"`
}

/*
{
    "code": 200,
    "msg": "WAIT",
    "status": "99",
    "orderId": "S120202208051555527511771885568",
    "merchantOrderId": "c234235234234235234234",
    "payType": "120",
    "sign": "154cf9ea31deecef04f0394aee3e8594",
    "traceId": "1271bf2b484d62e4340358a72d00653ef5c3d"
}
*/

type PayOutResponse struct {
	Code            int    `json:"code"` // :200,查询不到:404,失败:500,重复亲求:425
	Msg             string `json:"msg"`
	Status          string `json:"status"` // :99,支付中:00,成功:01,失败:02,超时:03,退款:04
	OrderId         string `json:"orderId"`
	MerchantOrderId string `json:"merchantOrderId"`
	PayType         string `json:"payType"`
	Sign            string `json:"sign"`
	TraceId         string `json:"traceId"`
}

/*
{
    "amount": "5000",
    "code": 200,
    "createTimeL": 1680334544698,
    "endToEndId": "e00360305202304010736c2009ae8cdd",
    "fee": "150",
    "merchantOrderId": "164774677175285280-6-WVDVe",
    "msg": "PAID",
    "orderId": "S110202304011642068212621467648",
    "payType": "110",
    "payUrl": "https://pay2.mtbtop1.com/s3/checkout-page/v3.html?cid=expireAt%202023-04-01T05:35:44.725466&sid=S110202304011642068212621467648&amount=50&qrcode=00020101021226790014br.gov.bcb.pix2557brcode.starkinfra.com/v2/92cde06bbe9e4a15a349756d2d24a6325204000053039865802BR5925Wudi%20pay%20Correspondente%20d6009Sao%20Paulo62070503***63045AE8",
    "realPayAmount": "5000",
    "sign": "674e8a79f04939a44fda447d4b13ca8c",
    "status": "01",
    "traceId": "i200,i200401ec75e3c58a5a40cdb874d30bedabcb5c"
}
*/

type Notification struct {
	Amount          string `json:"amount"`
	Code            int    `json:"code"`
	CreateTimeL     int64  `json:"createTimeL"`
	EndToEndId      string `json:"endToEndId"`
	Fee             string `json:"fee"`
	MerchantOrderId string `json:"merchantOrderId"`
	Msg             string `json:"msg"`
	OrderId         string `json:"orderId"`
	PayType         string `json:"payType"`
	PayUrl          string `json:"payUrl"`
	RealPayAmount   string `json:"realPayAmount"`
	Sign            string `json:"sign"`
	Status          string `json:"status"`
	TraceId         string `json:"traceId"`
}