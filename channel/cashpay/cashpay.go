package cashpay

import (
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"funzone_pay/app/validator"
	"funzone_pay/centerlog"
	"funzone_pay/common"
	"funzone_pay/dao"
	"funzone_pay/model"
	"funzone_pay/utils"
	"github.com/wonderivan/logger"
	"net/http"
	"time"
)

const (
	StatusWaiting = "99"
	StatusPending = "00"
	StatusSuccess = "01"
	StatusFail    = "02"
	StatusExpired = "03"
	StatusRefund  = "04"
)

var (
	orderError = errors.New("create order error")
)

type CashPayService struct {
	PAY_ACCOUNT_ID string
	PAY_CHANNEL    string
	COUNTRY_CODE   int

	APP_ID   string
	SIGN_KEY string

	IN_NOTIFY_URL  string // 异步支付、提现结果通知
	OUT_NOTIFY_URL string
	HOST_URL       string // 支付、提现的请求地址
	COLLECT_HOST   string
}

func GetCashPayIns(conf map[string]interface{}) *CashPayService {
	return &CashPayService{
		PAY_ACCOUNT_ID: conf["PAY_ACCOUNT_ID"].(string),
		PAY_CHANNEL:    conf["PAY_CHANNEL"].(string),
		COUNTRY_CODE:   conf["COUNTRY_CODE"].(int),

		APP_ID:         conf["APP_ID"].(string),
		SIGN_KEY:       conf["SIGN_KEY"].(string),
		IN_NOTIFY_URL:  conf["IN_NOTIFY_URL"].(string),
		OUT_NOTIFY_URL: conf["OUT_NOTIFY_URL"].(string),
		HOST_URL:       conf["HOST_URL"].(string),
		COLLECT_HOST:   conf["COLLECT_HOST"].(string),
	}
}

func (u CashPayService) Header() http.Header {
	value := fmt.Sprintf("%v:%v", u.APP_ID, u.SIGN_KEY)
	authorization := fmt.Sprintf("Basic %v", base64.StdEncoding.EncodeToString([]byte(value)))
	return http.Header{
		"Content-Type":  {"application/json; charset=utf-8"},
		"Authorization": {authorization},
	}
}

func (u CashPayService) CreateOrder(args validator.PayEntryValidator) (*model.PayEntry, error) {
	var err error = nil
	isAlarm := true
	defer func() {
		if isAlarm {
			common.PushAlarmEvent(common.Warning, fmt.Sprintf("%v", args.Uid), "CashPayService/CreateOrder", "", fmt.Sprintf("%v-%v Create Order By CashPay: %v", args.AppId, args.Uid, err))
		}
	}()

	orderId := utils.GetOrderId("CIN")

	reqData := PayInReq{
		Amount:          fmt.Sprintf("%v", int(args.Amount*100)),
		MerchantOrderId: orderId,
		NotifyUrl:       u.IN_NOTIFY_URL,
	}

	header := u.Header()

	reqBts, err := json.Marshal(reqData)
	if err != nil {
		return nil, fmt.Errorf("package data error")
	}

	code, data, err := utils.Post(u.HOST_URL+"/open-api/pay/payment", reqBts, header)
	logger.Debug("CashPayService_CreateOrder_Info | reqData=%v | %v | code=%v | err=%v | response=%v", reqData, args.AppOrderId, code, err, string(data))
	if err != nil {
		return nil, fmt.Errorf("create order error-%v", args.AppOrderId)
	}

	response := &PayInResponse{}
	err = json.Unmarshal(data, response)
	if err != nil {
		logger.Error("CashPayService_CreateOrder_JsonUnmarshal_Err | %v | err=%v | data=%v", args.AppOrderId, err, string(data))
		centerlog.OrderError(centerlog.MsgThirdPlatformFail, u.PAY_ACCOUNT_ID, args.AppId, args.AppOrderId, orderId, "false", err.Error())
		return nil, err
	}

	//
	if response.Code != 200 || response.MerchantOrderId != orderId { //
		return nil, fmt.Errorf("service busy, invalid code %v", args.AppOrderId)
	}

	//请求成功写入流水
	pE := new(model.PayEntry)
	pE.AppId = args.AppId
	pE.Uid = args.Uid
	pE.PayAccountId = u.PAY_ACCOUNT_ID
	pE.PayChannel = model.CASHPAY
	pE.CountryCode = u.COUNTRY_CODE
	pE.AppOrderId = args.AppOrderId
	pE.OrderId = orderId
	pE.PaymentOrderId = response.OrderId
	pE.Amount = args.Amount
	pE.UserId = args.UserId
	pE.UserName = args.UserName
	pE.Email = args.Email
	pE.Phone = args.Phone
	pE.Status = model.PAYING
	pE.Created = time.Now().Unix()
	pE.PaymentId = response.TraceId

	//
	engine, err := dao.GetMysql()
	if engine == nil || err != nil {
		logger.Error("CashPayService_CreateOrder_GetDB_Err | err=%v", err)
		return nil, err
	}
	_, err = engine.Insert(pE)
	if err != nil {
		logger.Error("CashPayService_CreateOrder_InsertDB_Err | err=%v", err)
		return nil, err
	}

	isAlarm = false
	if u.PAY_ACCOUNT_ID == model.CashPayAccountIDCustomizeH5 {
		pE.PaymentLinkHost = u.COLLECT_HOST
		pE.SignatureNew = base64.URLEncoding.EncodeToString([]byte(response.QrcodeRaw))
	} else {
		pE.PaymentLinkHost = response.PayUrl
	}

	return pE, nil
}

func (u CashPayService) Inquiry(orderId string, platformOrderId string) (*model.PayEntry, error) {
	res, err := u.Query(orderId)
	if err != nil {
		return nil, err
	}

	engine, _ := dao.GetMysql()
	payIn := new(model.PayEntry)
	exist, err := engine.Where("order_id=?", orderId).Get(payIn)
	if err != nil {
		logger.Error("CashPayService_Inquiry_GetError | err=%v | data=%v", err, orderId)
		return nil, err
	}
	if !exist {
		return nil, errors.New("NotExist")
	}

	if payIn.Status != model.PAYING {
		return nil, fmt.Errorf("no need update, status = %v", payIn.Status)
	}

	status := res.Status
	if res.Code == 200 && status == StatusSuccess {
		payIn.Status = model.PAYSUCCESS
		payIn.PaymentId = res.TraceId
	} else if res.Code == 404 || status == StatusFail || status == StatusExpired || status == StatusRefund {
		payIn.Status = model.PAYFAILD
		payIn.ThirdCode = res.Status
		payIn.ThirdDesc = res.Msg
	}

	return payIn, nil
}

func (u CashPayService) InquiryPayout(orderId string, platformOrderId string) (*model.PayOut, error) {
	res, err := u.Query(orderId)
	if err != nil {
		return nil, err
	}

	engine, _ := dao.GetMysql()
	payOut := new(model.PayOut)
	exist, err := engine.Where("order_id=?", orderId).Get(payOut)
	if err != nil {
		logger.Error("CashPayService_PayOut_Inquiry_GetError | err=%v | data=%v", err, orderId)
		return nil, err
	}
	if !exist {
		return nil, errors.New("NotExist")
	}

	if payOut.Status != model.PAY_OUT_ING {
		return nil, fmt.Errorf("no need update, status = %v", payOut.Status)
	}

	status := res.Status
	if res.Code == 200 && status == StatusSuccess {
		payOut.Status = model.PAY_OUT_SUCCESS
		payOut.PaymentId = res.TraceId
	} else if res.Code == 404 || status == StatusFail || status == StatusExpired || status == StatusRefund {
		payOut.Status = model.PAY_OUT_FAILD
		payOut.ThirdCode = res.Status
		payOut.ThirdDesc = res.Msg
	}

	return payOut, nil
}

func (u CashPayService) PayOut(frozenId int64, args validator.PayOutValidator) (payOut *model.PayOut, err error) {
	isAlarm := true
	force := false
	defer func() {
		if isAlarm || force {
			common.PushAlarmEvent(common.Warning, fmt.Sprintf("%v", args.Uid), "CashPayService/PayOut", "", fmt.Sprintf("%v-%v PayOut By CashPay: %v-%v", args.AppId, args.Uid, args.AppOrderId, err))
		}
	}()

	if args.PayType != model.PT_PIX {
		err = fmt.Errorf("unsupported pay type[%v]", args.PayType)
		return nil, err
	}

	orderId := utils.GetOrderId("COUT")

	payOut = new(model.PayOut)
	payOut.Uid = args.Uid
	payOut.AppId = args.AppId
	payOut.PayAccountId = u.PAY_ACCOUNT_ID
	payOut.PayChannel = model.CASHPAY
	payOut.CountryCode = u.COUNTRY_CODE
	payOut.PayType = args.PayType
	payOut.AppOrderId = args.AppOrderId
	payOut.OrderId = orderId
	payOut.PaymentId = orderId
	payOut.FrozenId = frozenId
	payOut.Amount = args.Amount
	payOut.BankCard = args.BankCard
	payOut.UserId = args.UserId
	payOut.UserName = args.UserName
	payOut.Email = args.Email
	payOut.Paytm = args.PayTm
	payOut.IFSC = args.IFSC
	payOut.Phone = args.Phone
	payOut.Status = model.PAY_OUT_ING
	payOut.Created = time.Now().Unix()
	engine, err := dao.GetMysql()
	if engine == nil || err != nil {
		logger.Error("CashPayService_PayOut_GetDB_Err | err=%v", err)
		return nil, err
	}
	_, err = engine.Insert(payOut)
	if err != nil {
		logger.Error("CashPayService_PayOut_Insert_Error | data=%+v | err=%v", payOut, err)
		return nil, err
	}

	head := u.Header()

	accountType := string(args.CardType)
	if accountType == model.CardTypePixEVP {
		accountType = "RANDOM"
	}
	reqData := PayOutReq{
		Amount:          fmt.Sprintf("%v", int(args.Amount*100)),
		MerchantOrderId: orderId,
		NotifyUrl:       u.OUT_NOTIFY_URL,
		CustomerName:    args.UserName,
		CustomerCert:    args.DocumentValue,
		AccountType:     accountType,
		AccountNum:      args.BankCard,
	}

	reqBts, err := json.Marshal(reqData)

	// 发起
	code, respData, err := utils.Post(u.HOST_URL+"/open-api/pay/transfer", reqBts, head)
	logger.Debug("CashPayService_PayOut_Info | reqData=%v | %v | code=%v | err=%v | response=%v", string(reqBts), args.AppOrderId, code, err, string(respData))
	if err != nil {
		return nil, fmt.Errorf("payout order error")
	}

	response := &PayOutResponse{}
	err = json.Unmarshal(respData, response)
	if err != nil {
		logger.Error("CashPayService_PayOut_JsonUnmarshal_Err | %v | err=%v | data=%v", args.AppOrderId, err, string(respData))
		centerlog.OrderError(centerlog.MsgThirdPlatformFail, u.PAY_ACCOUNT_ID, args.AppId, args.AppOrderId, orderId, "false", err.Error())
		return nil, err
	}

	if response.OrderId == "" {
		logger.Error("CashPayService_PayOut_Status_Err | %v | %v-%v", args.AppOrderId, code, response.Code)
		centerlog.OrderError(centerlog.MsgThirdPlatformFail, u.PAY_ACCOUNT_ID, payOut.AppId, payOut.AppOrderId, payOut.OrderId, response.Msg, fmt.Sprintf("code:%v", response.Code))
		force = true
	}

	// success
	paymentId := response.OrderId
	_, err = engine.Where("id=?", payOut.Id).Update(&model.PayOut{
		PaymentId: paymentId,
	})
	if err != nil {
		logger.Error("CashPayService_PayOut_Update_Error | data=%v | err=%v", payOut, err)
		return nil, err
	}

	isAlarm = false
	payOut.PaymentId = paymentId

	return payOut, nil
}

func (u CashPayService) PayOutBatch(frozenId int64, outValidator validator.PayOutValidator) (*model.PayOut, error) {
	return u.PayOut(frozenId, outValidator)
}

func (u CashPayService) InSignature(cb interface{}) bool {
	return false
}

func (u CashPayService) OutSignature(cb interface{}) bool {
	return u.InSignature(cb)
}

func (u CashPayService) ReqTime() string {
	return fmt.Sprintf("%v", time.Now().UnixNano()/1000000)
}

func (u CashPayService) Query(orderId string) (respData QueryInResponse, err error) {
	header := u.Header()
	code, resp, err := utils.Get(fmt.Sprintf("%v/open-api/pay/query?merchantOrderId=%v", u.HOST_URL, orderId), header, nil)
	logger.Debug("CashPayService_Query_Info | requestData=%v |%v| response=%v", orderId, code, string(resp))
	if err != nil {
		logger.Error("CashPayService_Query_Request_Error | err=%v", err)
		return
	}

	err = json.Unmarshal(resp, &respData)
	if err != nil {
		logger.Error("CashPayService_Query_JsonUnmarshal_Error | err=%v | res=%v", err, string(resp))
		return
	}

	if respData.Code != 200 && respData.Code != 404 {
		err = fmt.Errorf("quiry[%v] error,%v-->%v", orderId, respData.Code, respData.Msg)
		logger.Error("CashPayService_Inquiry_GetError | err=%v | data=%v", err, orderId)
	}

	return
}

//func GetSign(data map[string]interface{}, key string) string {
//	str := sortKeys(data)
//	return utils.GetMd5Upper(str + fmt.Sprintf("&key=%v", key))
//}

//func sortKeys(data map[string]interface{}) string {
//	keys := make([]string, 0, len(data))
//	for k, _ := range data {
//		if k == FieldSign {
//			continue
//		}
//
//		keys = append(keys, k)
//	}
//
//	sort.Strings(keys)
//
//	keyVal := make([]string, 0, len(keys))
//	for _, k := range keys {
//		val := fmt.Sprintf("%v", data[k])
//		if len(val) < 1 {
//			continue
//		}
//
//		keyVal = append(keyVal, fmt.Sprintf("%v=%v", k, val))
//	}
//
//	return strings.Join(keyVal, "&")
//}
//
//func genValues(data map[string]interface{}) url.Values {
//	value := url.Values{}
//	for k, v := range data {
//		value.Add(k, fmt.Sprint(v))
//	}
//
//	return value
//}
