package xpay

import (
	"encoding/json"
	"errors"
	"fmt"
	"funzone_pay/app/validator"
	"funzone_pay/centerlog"
	"funzone_pay/dao"
	"funzone_pay/model"
	"funzone_pay/utils"
	"github.com/wonderivan/logger"
	"io/ioutil"
	"net/http"
	"net/url"
	"sort"
	"strings"
	"time"
)

const (
	FieldMchNo       = "mchNo"
	FieldMchOrderNo  = "mchOrderNo"
	FieldAppId       = "appId"
	FieldCurrency    = "currency"
	FieldNotifyUrl   = "notifyUrl"
	FieldOrderAmount = "orderAmount"
	FieldEmail       = "email" // not
	FieldName        = "name"
	FieldPhone       = "phone"   // not  电话不需要加+55国际区号
	FieldCert        = "cert"    // not 目标账户持有人的证件号CPF，11位数字
	FieldReqTime     = "reqTime" // 毫秒时间戳
	FieldSign        = "sign"

	FieldPayOrderId  = "payOrderId"
	FieldState       = "state"
	FieldErrCode     = "errCode"
	FieldErrMsg      = "errMsg"
	FieldCreatedAt   = "createdAt"
	FieldSuccessTime = "successTime"

	FieldBankCode        = "bankCode"
	FieldPixType         = "pixType"
	FieldAccountNo       = "accountNo"
	FieldAccountName     = "accountName"
	FieldAccountEmail    = "accountEmail"
	FieldAccountMobileNo = "accountMobileNo"
	FieldTransferDesc    = "transferDesc"
)

const (
	RespCodeSuccess = 0
	PayPending      = 1
	PayPendingStr   = "1"
	PaySuccess      = 2
	PaySuccessStr   = "2"
	PayFailed       = 3
	PayFailedStr    = "3"
)

var (
	orderError = errors.New("create order error")
)

type XPayService struct {
	PAY_ACCOUNT_ID string
	PAY_CHANNEL    string
	COUNTRY_CODE   int

	APP_ID string
	MCH_NO string
	KEY    string //

	IN_NOTIFY_URL  string // 异步支付、提现结果通知
	OUT_NOTIFY_URL string
	HOST_URL       string // 支付、提现的请求地址
}

func GetXPayIns(conf map[string]interface{}) *XPayService {
	return &XPayService{
		PAY_ACCOUNT_ID: conf["PAY_ACCOUNT_ID"].(string),
		PAY_CHANNEL:    conf["PAY_CHANNEL"].(string),
		COUNTRY_CODE:   conf["COUNTRY_CODE"].(int),
		MCH_NO:         conf["MCH_NO"].(string),
		KEY:            conf["KEY"].(string),

		IN_NOTIFY_URL:  conf["IN_NOTIFY_URL"].(string),
		OUT_NOTIFY_URL: conf["OUT_NOTIFY_URL"].(string),
		HOST_URL:       conf["HOST_URL"].(string),
	}
}

func (u XPayService) Header() http.Header {
	return http.Header{
		"Content-Type": {"application/x-www-form-urlencoded; charset=utf-8"},
	}
}

func (u XPayService) CreateOrder(args validator.PayEntryValidator) (*model.PayEntry, error) {
	orderId := utils.GetOrderId("XIN")
	reqData := u.genPayInReqData(orderId, args)
	header := u.Header()

	data, err := u.Request(u.HOST_URL+"/api/pay/unifiedOrder", "POST", reqData, header)
	logger.Debug("XPayService_CreateOrder_Info | reqData=%v | %v | err=%v | response=%v", reqData, args.AppOrderId, err, string(data))
	if err != nil {
		return nil, fmt.Errorf("create order error-%v", args.AppOrderId)
	}

	response := &PayInResponse{}
	err = json.Unmarshal(data, response)
	if err != nil {
		logger.Error("XPayService_CreateOrder_JsonUnmarshal_Err | %v | err=%v | data=%v", args.AppOrderId, err, string(data))
		centerlog.OrderError(centerlog.MsgThirdPlatformFail, u.PAY_ACCOUNT_ID, args.AppId, args.AppOrderId, orderId, "false", err.Error())
		return nil, err
	}

	//
	if response.Code != RespCodeSuccess { //
		return nil, fmt.Errorf("service busy, invalid code %v", args.AppOrderId)
	}

	// TODO verify data sign
	//err = VerifyJsonRawDataSign(data, utils.WrapPublicKey(u.PUBLIC_KEY))
	//if err != nil {
	//	logger.Error("XPayService_CreateOrder_VerifySign_Err | %v | err=%v ", args.AppOrderId, err)
	//	return nil, fmt.Errorf("create order error-%v", args.AppOrderId)
	//}

	//请求成功写入流水
	pE := new(model.PayEntry)
	pE.AppId = args.AppId
	pE.Uid = args.Uid
	pE.PayAccountId = u.PAY_ACCOUNT_ID
	pE.PayChannel = model.XPAY
	pE.CountryCode = u.COUNTRY_CODE
	pE.AppOrderId = args.AppOrderId
	pE.OrderId = orderId
	pE.PaymentOrderId = response.Data.PayOrderId
	pE.Amount = args.Amount
	pE.UserId = args.UserId
	pE.UserName = args.UserName
	pE.Email = args.Email
	pE.Phone = args.Phone
	pE.Status = model.PAYING

	//
	engine, err := dao.GetMysql()
	if engine == nil || err != nil {
		logger.Error("XPayService_CreateOrder_GetDB_Err | err=%v", err)
		return nil, err
	}
	_, err = engine.Insert(pE)
	if err != nil {
		logger.Error("XPayService_CreateOrder_InsertDB_Err | err=%v", err)
		return nil, err
	}

	pE.PaymentLinkHost = response.Data.PayData
	return pE, nil
}

func (u XPayService) Inquiry(orderId string, platformOrderId string) (*model.PayEntry, error) {
	header := u.Header()
	reqData := u.genQueryReqData(orderId)

	resp, err := u.Request(u.HOST_URL+"/api/pay/query", "POST", reqData, header)
	logger.Debug("XPayService_Inquiry_Info | requestData=%v | %v | response=%v", reqData, orderId, string(resp))
	if err != nil {
		logger.Error("XPayService_Inquiry_Request_Error | err=%v", err)
		return nil, err
	}

	res := &QueryInResponse{}
	err = json.Unmarshal(resp, res)
	if err != nil {
		logger.Error("XPayService_Inquiry_JsonUnmarshal_Error | err=%v | res=%v", err, res)
		return nil, err
	}

	if res.Code != RespCodeSuccess {
		logger.Error("XPayService_Inquiry_GetError | err=%v | data=%v", err, orderId)
		return nil, fmt.Errorf("quiry[%v] error", orderId)
	}

	engine, _ := dao.GetMysql()
	payIn := new(model.PayEntry)
	exist, err := engine.Where("order_id=?", orderId).Get(payIn)
	if err != nil {
		logger.Error("XPayService_Inquiry_GetError | err=%v | data=%v", err, orderId)
		return nil, err
	}
	if !exist {
		return nil, errors.New("NotExist")
	}

	status := res.Data.State
	if status == PaySuccess {
		payIn.Status = model.PAYSUCCESS
		payIn.PaymentId = res.Data.PayOrderId
	} else if status == PayFailed {
		payIn.Status = model.PAYFAILD
		payIn.ThirdCode = res.Data.ErrCode
		payIn.ThirdDesc = res.Data.ErrMsg
	}

	return payIn, nil
}

func (u XPayService) InquiryPayout(orderId string, platformOrderId string) (*model.PayOut, error) {
	header := u.Header()
	reqData := u.genQueryReqData(orderId)
	resp, err := u.Request(u.HOST_URL+"/api/transfer/query", "POST", reqData, header)
	logger.Debug("XPayService_Inquiry_out_Info | requestData=%v | %v | response=%v", reqData, orderId, string(resp))
	if err != nil {
		logger.Error("XPayService_Inquiry_out_Request_Error | err=%v", err)
		return nil, err
	}

	res := &QueryOutResponse{}
	err = json.Unmarshal(resp, res)
	if err != nil {
		logger.Error("XPayService_Inquiry_out_JsonUnmarshal_Error | err=%v | res=%v", err, res)
		return nil, err
	}

	if res.Code != RespCodeSuccess {
		logger.Error("XPayService_Inquiry_GetError | err=%v | data=%v", err, orderId)
		return nil, fmt.Errorf("quiry[%v] error", orderId)
	}

	engine, _ := dao.GetMysql()
	payOut := new(model.PayOut)
	exist, err := engine.Where("order_id=?", orderId).Get(payOut)
	if err != nil {
		logger.Error("XPayService_PayOut_Inquiry_GetError | err=%v | data=%v", err, orderId)
		return nil, err
	}
	if !exist {
		return nil, errors.New("NotExist")
	}

	status := res.Data.State
	if status == PaySuccess {
		payOut.Status = model.PAY_OUT_SUCCESS
		payOut.PaymentId = res.Data.TransferId
	}
	//else if status == Failed {
	//	payOut.Status = model.PAY_OUT_FAILD
	//	//payOut.ThirdCode = res.RespCode
	//	payOut.ThirdDesc = res.RespMsg
	//}

	return payOut, nil
}

func (u XPayService) PayOut(args validator.PayOutValidator) (payOut *model.PayOut, err error) {
	orderId := utils.GetOrderId("XOUT")

	payOut = new(model.PayOut)
	payOut.Uid = args.Uid
	payOut.AppId = args.AppId
	payOut.PayAccountId = u.PAY_ACCOUNT_ID
	payOut.PayChannel = model.XPAY
	payOut.CountryCode = u.COUNTRY_CODE
	payOut.PayType = args.PayType
	payOut.AppOrderId = args.AppOrderId
	payOut.OrderId = orderId
	payOut.PaymentId = orderId
	payOut.Amount = args.Amount
	payOut.BankCard = args.BankCard
	payOut.UserId = args.UserId
	payOut.UserName = args.UserName
	payOut.Email = args.Email
	payOut.Paytm = args.PayTm
	payOut.IFSC = args.IFSC
	payOut.Phone = args.Phone
	payOut.Status = model.PAY_OUT_ING
	engine, err := dao.GetMysql()
	if engine == nil || err != nil {
		logger.Error("XPayService_PayOut_GetDB_Err | err=%v", err)
		return nil, err
	}
	_, err = engine.Insert(payOut)
	if err != nil {
		logger.Error("XPayService_PayOut_Insert_Error | data=%+v | err=%v", payOut, err)
		return nil, err
	}

	head := u.Header()
	reqData := u.genPayOutReqData(orderId, args)

	// 发起
	data, err := u.Request(u.HOST_URL+"/api/transferOrder", "POST", reqData, head)
	logger.Debug("XPayService_PayOut_Info | reqData=%v | %v | err=%v | response=%v", reqData.Encode(), args.AppOrderId, err, string(data))
	if err != nil {
		return nil, fmt.Errorf("payout order error")
	}

	response := &PayOutResponse{}
	err = json.Unmarshal(data, response)
	if err != nil {
		logger.Error("XPayService_PayOut_JsonUnmarshal_Err | %v | err=%v | data=%v", args.AppOrderId, err, string(data))
		centerlog.OrderError(centerlog.MsgThirdPlatformFail, u.PAY_ACCOUNT_ID, args.AppId, args.AppOrderId, orderId, "false", err.Error())
		return nil, err
	}

	//err = VerifyJsonRawDataSign(data, utils.WrapPublicKey(u.PUBLIC_KEY))
	//if err != nil {
	//	logger.Error("XPayService_Payout_VerifySign_Err | %v | err=%v ", args.AppOrderId, err)
	//	return nil, fmt.Errorf("create order error-%v", args.AppOrderId)
	//}

	code := response.Code
	if code != RespCodeSuccess {
		logger.Error("XPayService_PayOut_Status_Err | %v | %v-%v", args.AppOrderId, code, response.Code)
		centerlog.OrderError(centerlog.MsgThirdPlatformFail, u.PAY_ACCOUNT_ID, payOut.AppId, payOut.AppOrderId, payOut.OrderId, response.Msg, fmt.Sprintf("code:%v", response.Code))

		return payOut, nil
	}

	// success
	paymentId := response.Data.TransferId
	_, err = engine.Where("id=?", payOut.Id).Update(&model.PayOut{
		PaymentId: paymentId,
	})
	if err != nil {
		logger.Error("XPayService_PayOut_Update_Error | data=%v | err=%v", payOut, err)
		return nil, err
	}

	payOut.PaymentId = paymentId

	return payOut, nil
}

func (u XPayService) PayOutBatch(outValidator validator.PayOutValidator) (*model.PayOut, error) {
	return u.PayOut(outValidator)
}

func (u XPayService) InSignature(cb interface{}) bool {
	data, ok := cb.(url.Values)
	if !ok {
		return false
	}

	sign := ""
	mData := make(map[string]interface{})
	for k, _ := range data {
		if k == FieldSign {
			sign = data.Get(k)
			continue
		}
		mData[k] = data.Get(k)
	}

	ret := GetSign(mData, u.KEY) == sign
	if !ret {
		logger.Error("XPayService_CheckSign_Error | data=%v", data.Encode())
	}

	return ret
}

func (u XPayService) OutSignature(cb interface{}) bool {
	return u.InSignature(cb)
}

func (u XPayService) genPayInReqData(orderId string, args validator.PayEntryValidator) url.Values {
	data := map[string]interface{}{
		FieldMchNo:       u.MCH_NO,
		FieldMchOrderNo:  orderId,
		FieldAppId:       u.APP_ID,
		FieldCurrency:    "BRL",
		FieldNotifyUrl:   u.IN_NOTIFY_URL,
		FieldOrderAmount: args.Amount,
		FieldName:        args.UserName,
		FieldReqTime:     u.ReqTime(),
	}

	data[FieldSign] = GetSign(data, u.KEY)

	return genValues(data)
}

func (u XPayService) genQueryReqData(orderId string) url.Values {
	data := map[string]interface{}{
		FieldMchNo:      u.MCH_NO,
		FieldAppId:      u.APP_ID,
		FieldMchOrderNo: orderId,
		FieldReqTime:    u.ReqTime(),
	}
	data[FieldSign] = GetSign(data, u.KEY)
	return genValues(data)
}

func (u XPayService) genPayOutReqData(orderId string, args validator.PayOutValidator) url.Values {
	bankCard := args.BankCard
	if bankCard[0:3] == "+55" {
		bankCard = bankCard[3:]
	}

	cert := ""
	if args.CardType == "CPF" || args.CardType == "CNPJ" {
		cert = args.BankCard
	}

	data := map[string]interface{}{
		FieldMchNo:       u.MCH_NO,
		FieldAppId:       u.APP_ID,
		FieldMchOrderNo:  orderId,
		FieldBankCode:    "PIX",
		FieldOrderAmount: args.Amount,
		FieldCurrency:    "BRL",
		FieldPixType:     args.CardType, // EMAIL/PHONE/CPF/RANDOM/CNPJ
		FieldAccountNo:   bankCard,      // 不需要 +55
		FieldAccountName: "",            // TODO 填入则验证姓名，请真实填写
		FieldCert:        cert,          // TODO 对应 CPF和 CNPJ 税号
		FieldReqTime:     u.ReqTime(),
		FieldNotifyUrl:   u.OUT_NOTIFY_URL,
	}

	data[FieldSign] = GetSign(data, u.KEY)
	return genValues(data)
}

func (u XPayService) ReqTime() string {
	return fmt.Sprintf("%v", time.Now().UnixNano()/1000000)
}

func (u XPayService) Request(url string, method string, sendData url.Values, header map[string][]string) (res []byte, err error) {
	client := &http.Client{}
	//处理返回结果
	response, err := client.PostForm(url, sendData)
	if err != nil {
		logger.Error("XPayService_request_err | url=%v | err=%+v | req=%+v", url, err, sendData)
		return
	}
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		logger.Error("XPayService_response_read_err | url=%v | err=%+v | req=%+v", url, err, sendData)
		return
	}
	defer func() {
		_ = response.Body.Close()
	}()
	return body, nil
}

func GetSign(data map[string]interface{}, key string) string {
	str := sortKeys(data)
	return utils.GetMd5Upper(str + fmt.Sprintf("&key=%v", key))
}

//func VerifyJsonRawDataSign(data []byte, sign string, publicKey []byte) (err error) {
//	keyVal := make(map[string]interface{})
//	err = json.Unmarshal(data, &keyVal)
//	if err != nil {
//		logger.Error("XPayService_JsonUnmarshal_Err | %v | err=%v ", string(data), err)
//		return err
//	}
//
//	if !VerifySign(keyVal, publicKey) {
//		logger.Error("XPayService_VerifySign_Err | %v ", string(data))
//		return fmt.Errorf("verify sign error")
//	}
//
//	return nil
//}

//func VerifySign(data map[string]interface{}, key string) bool {
//	sign, ok := data[FieldSign]
//	if !ok {
//		logger.Error("XPay verify sign not found sign field")
//		return false
//	}
//
//	return sign == nil
//}

func sortKeys(data map[string]interface{}) string {
	keys := make([]string, 0, len(data))
	for k, _ := range data {
		if k == FieldSign {
			continue
		}

		keys = append(keys, k)
	}

	sort.Strings(keys)

	keyVal := make([]string, 0, len(keys))
	for _, k := range keys {
		val := fmt.Sprintf("%v", data[k])
		if len(val) < 1 {
			continue
		}

		keyVal = append(keyVal, fmt.Sprintf("%v=%v", k, val))
	}

	return strings.Join(keyVal, "&")
}

func genValues(data map[string]interface{}) url.Values {
	value := url.Values{}
	for k, v := range data {
		value.Add(k, fmt.Sprint(v))
	}

	return value
}
