package xpay

/*
{
  "code": 0,
  "data": {
    "errCode": "500",
    "errMsg": "Business Failed",
    "mchOrderNo": "mho1624005752661",
    "orderState": 3,
    "payOrderId": "P202106181642329900002"
  },
  "msg": "SUCCESS",
  "sign": "F4DA202C516D1F33A12F1E547C5004FD"
}
*/
type PayInResponse struct {
	Code int    `json:"code"`
	Msg  string `json:"msg"`
	Sign string `json:"sign"`
	Data struct {
		PayOrderId  string `json:"payOrderId"`
		MchOrderNo  string `json:"mchOrderNo"`
		OrderState  int    `json:"orderState"` //1-支付中 2-支付成功 3-支付失败
		PayDataType string `json:"payDataType"`
		PayData     string `json:"payData"` // 支付链接
		ErrCode     string `json:"errCode"`
		ErrMsg      string `json:"errMsg"`
	} `json:"data"`
}

type PayInResponseForSign struct {
	Code int                    `json:"code"`
	Msg  string                 `json:"msg"`
	Sign string                 `json:"sign"`
	Data map[string]interface{} `json:"data"`
}

/*
{
  "code": 0,
  "data": {
    "amount": 58,
    "appId": "60cc09bce4b0f1c0b83761c9",
    "createdAt": 1623985457705,
    "currency": "cny",
    "mchNo": "M1623984572",
    "mchOrderNo": "mho1623985457320",
    "payOrderId": "P202106181104177050002",
    "state": 2,
    "successTime": 1623985459000,
  },
  "msg": "SUCCESS",
  "sign": "9548145EA12D0CD8C1628BCF44E19E0D"
}
*/
type QueryInResponse struct {
	Code int    `json:"code"`
	Msg  string `json:"msg"`
	Sign string `json:"sign"`
	Data struct {
		PayOrderId  string  `json:"payOrderId"`
		MchOrderNo  string  `json:"mchOrderNo"`
		OrderAmount float64 `json:"orderAmount"`
		State       int     `json:"state"` //1-支付中 2-支付成功 3-支付失败
		ErrCode     string  `json:"errCode"`
		ErrMsg      string  `json:"errMsg"`
	} `json:"data"`
}

type QueryInResponseForSign struct {
	Code int                    `json:"code"`
	Msg  string                 `json:"msg"`
	Sign string                 `json:"sign"`
	Data map[string]interface{} `json:"data"`
}

/*
{
    "code": 0,
    "data": {
        "accountNo": "13412312312",
 	 	"accountName": "zhangsan",
        "orderAmount": 100.00,
        "mchOrderNo": "1629106288",
        "state": 1,
        "transferId": "T202108161731281310004"
    },
    "msg": "SUCCESS",
	"sign": "195BF6F112386F7FC8EA2AA7EECA1D33"
}
*/
type PayOutResponse struct {
	Code int    `json:"code"`
	Msg  string `json:"msg"`
	Sign string `json:"sign"`
	Data struct {
		TransferId  string  `json:"transferId"`
		MchOrderNo  string  `json:"mchOrderNo"`
		State       int     `json:"state"`
		AccountNo   string  `json:"accountNo"`
		AccountName string  `json:"accountName"`
		OrderAmount float64 `json:"orderAmount"`
	} `json:"data"`
}

type PayOutResponseForSign struct {
	Code int                    `json:"code"`
	Msg  string                 `json:"msg"`
	Sign string                 `json:"sign"`
	Data map[string]interface{} `json:"data"`
}

type QueryOutResponse struct {
	Code int    `json:"code"`
	Msg  string `json:"msg"`
	Sign string `json:"sign"`
	Data struct {
		TransferId   string  `json:"transferId"` //
		MchNo        string  `json:"mchNo"`
		AppId        string  `json:"appId"`
		MchOrderNo   string  `json:"mchOrderNo"`
		OrderAmount  float64 `json:"orderAmount"`
		Currency     string  `json:"currency"`
		State        int     `json:"state"` //1-支付中 2-支付成功 3-支付失败
		ErrCode      string  `json:"errCode"`
		ErrMsg       string  `json:"errMsg"`
		CreatedAt    int64   `json:"createdAt"`
		SuccessTime  int64   `json:"successTime"`
		TransferDesc string  `json:"transferDesc"`
		AccountNo    string  `json:"accountNo"`
		AccountName  string  `json:"accountName"`
	} `json:"data"`
}

type QueryOutResponseForSign struct {
	Code int                    `json:"code"`
	Msg  string                 `json:"msg"`
	Sign string                 `json:"sign"`
	Data map[string]interface{} `json:"data"`
}
