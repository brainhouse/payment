package pagsmile

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"funzone_pay/app/validator"
	"funzone_pay/dao"
	"funzone_pay/model"
	"funzone_pay/utils"
	"github.com/spf13/viper"
	"github.com/wonderivan/logger"
	"io/ioutil"
	"net/http"
	"strings"
	"time"
)

type PagSmilePayService struct {
	HOST              string
	PAY_OUT_HOST      string
	PAY_ACCOUNT_ID    string
	PAY_CHANNEL       string
	APPID             string
	APPSK             string
	MERCHANT_ID       string
	MERCHANT_KEY      string
	COUNTRY_STR       string
	COUNTRY_CODE      int
	CURRENCY          string
	RETRUN_URL        string
	COLLECT_HOST      string
	NOTIFY_URL        string
	PAYOUT_NOTIFY_URL string
	PAY_OUT_URL       string
}

const countryCodeBRA = "BRA"
const countryCodeMEX = "MEX"

const payMethodWallet = "Wallet" // br / mx
const payMethodPIX = "PIX"       // br
const payMethodCash = "Cash"     // mx
const payMethodSPEI = "SPEI"     // mx

const walletChannelMercadopago = "Mercadopago" // br -
const walletChannelPicPay = "PicPay"           // br
const walletChannelAME = "AME"                 // br

var brChannel = map[string]bool{
	walletChannelMercadopago: true,
	walletChannelPicPay:      true,
	walletChannelAME:         true,
}

const walletChannelTodito = "Todito" // mx
var mxChannel = map[string]bool{
	walletChannelTodito: true,
}

func GetPagSmilePayIns(conf map[string]interface{}) *PagSmilePayService {
	return &PagSmilePayService{
		HOST:              conf["HOST"].(string),
		PAY_OUT_HOST:      conf["PAY_OUT_HOST"].(string),
		PAY_ACCOUNT_ID:    conf["PAY_ACCOUNT_ID"].(string),
		PAY_CHANNEL:       conf["PAY_CHANNEL"].(string),
		APPID:             conf["APPID"].(string),
		APPSK:             conf["APPSK"].(string),
		MERCHANT_ID:       conf["MERCHANT_ID"].(string),
		MERCHANT_KEY:      conf["MERCHANT_KEY"].(string),
		COUNTRY_CODE:      conf["COUNTRY_CODE"].(int),
		COUNTRY_STR:       conf["COUNTRY_STR"].(string),
		CURRENCY:          conf["CURRENCY"].(string),
		RETRUN_URL:        conf["RETURN_URL"].(string),
		NOTIFY_URL:        conf["NOTIFY_URL"].(string),
		PAYOUT_NOTIFY_URL: conf["PAYOUT_NOTIFY_URL"].(string),
		COLLECT_HOST:      conf["COLLECT_HOST"].(string),
		PAY_OUT_URL:       conf["PAY_OUT_URL"].(string),
	}
}

func (u *PagSmilePayService) IsDebug() bool {
	return viper.GetString("server.mode") != "release"
}

func (u *PagSmilePayService) CreateOrder(args validator.PayEntryValidator) (*model.PayEntry, error) {
	orderId := utils.GetOrderId("PGSIN")
	resp := tradeCreateResponse{}
	status := model.PAYING
	if !u.IsDebug() {
		requestUrl := fmt.Sprintf("%s", u.HOST)
		header := map[string][]string{
			"Content-Type":  {"application/json"},
			"Authorization": {"Basic " + base64.StdEncoding.EncodeToString([]byte(fmt.Sprintf("%s:%s", u.APPID, u.APPSK)))},
		}

		now := time.Now()
		req := tradeCreate{
			AppId:          u.APPID,
			Timestamp:      now.Format("2006-01-02 15:04:05"),
			OutTradeNO:     orderId,
			Method:         "",
			Channel:        "",
			OrderCurrency:  u.CURRENCY,
			OrderAmount:    int(args.Amount),
			Subject:        "PAY",
			Content:        "",
			TradeType:      "WEB",
			NotifyUrl:      u.NOTIFY_URL,
			ReturnUrl:      "",
			BuyerID:        args.UserId,
			TimeoutExpress: "",
		}

		data, err := u.Request(requestUrl, "POST", "JSON", req, header)
		logger.Debug("PagSmilePayService_CreateOrder_Info | orderReq=%+v | resp=%+v | err=%v", req, string(data), err)
		if err != nil {
			logger.Error("PagSmilePayService_CreateOrder_Err| orderReq=%+v | err=%v", req, err)
			return nil, err
		}
		err = json.Unmarshal(data, &resp)
		if err != nil {
			logger.Error("PagSmilePayService_CreateOrder_JsonUnmarshal_Err | data=%+v | err=%v", string(data), err)
			return nil, err
		}
	} else {
		status = model.PAYSUCCESS
		resp.Code = "10000"
		resp.TradeNO = fmt.Sprintf("%v", time.Now().UnixNano())
		resp.WebUrl = "https://payment.firstpayer.com"
	}

	//请求成功写入流水
	pE := new(model.PayEntry)
	pE.AppId = args.AppId
	pE.Uid = args.Uid
	pE.PayAccountId = u.PAY_ACCOUNT_ID
	pE.PayChannel = model.PAGSMILE
	pE.CountryCode = u.COUNTRY_CODE
	pE.AppOrderId = args.AppOrderId
	pE.OrderId = orderId
	pE.PaymentOrderId = resp.TradeNO
	pE.Amount = args.Amount
	pE.UserId = args.UserId
	pE.UserName = args.UserName
	pE.Email = args.Email
	pE.Phone = args.Phone
	pE.Status = status
	if u.IsDebug() {
		pE.SettleStatus = 1
	}
	pE.PayAppId = u.APPID
	pE.ThirdCode = resp.Code
	if resp.Code != "10000" {
		pE.ThirdDesc = fmt.Sprintf("%s->%v,%s->%v", resp.Code, resp.Msg, resp.SubCode, resp.SubMsg)
	}

	engine, err := dao.GetMysql()
	if engine == nil || err != nil {
		logger.Error("PagSmilePayService_CreateOrder_GetDB_Err | err=%v", err)
		return nil, err
	}
	_, err = engine.Insert(pE)
	if err != nil {
		logger.Error("PagSmilePayService_CreateOrder_InsertDB_Err | err=%v", err)
		return nil, err
	}

	if resp.Code != "10000" {
		err = errors.New(fmt.Sprintf(resp.Msg, resp.SubMsg))
		return nil, err
	}

	pE.PaymentLinkHost = resp.WebUrl
	return pE, nil
}

func (u *PagSmilePayService) Inquiry(s string, s2 string) (*model.PayEntry, error) {
	return nil, fmt.Errorf("unsupported")
}

func (u *PagSmilePayService) InquiryPayout(orderId string, ext string) (*model.PayOut, error) {
	return nil, fmt.Errorf("implement me")
}

func (u *PagSmilePayService) checkPayoutArgs(outValidator *validator.PayOutValidator) bool {
	switch u.COUNTRY_STR {
	case countryCodeBRA:
		//if outValidator.UserName == "" || outValidator.DocumentValue == "" || outValidator.DocumentType == "" ||
		//	outValidator.Account == "" || outValidator.AccountType == "" {
		//	logger.Error("PagSmilePayService_PayOut_checkPayoutArgs_Error | data=%+v ", outValidator)
		//	return false
		//}

		return true

	case countryCodeMEX:
		return false
	}

	return false
}

func (u *PagSmilePayService) PayOut(frozenId int64, outValidator validator.PayOutValidator) (payOut *model.PayOut, err error) {
	if !u.IsDebug() && !u.checkPayoutArgs(&outValidator) {
		return &model.PayOut{Status: model.PAY_OUT_FAILD}, errors.New("invalid args")
	}

	//
	transId := utils.GetOrderId("PGSOUT")
	payOut = new(model.PayOut)
	payOut.Uid = outValidator.Uid
	payOut.AppId = outValidator.AppId
	payOut.PayAccountId = u.PAY_ACCOUNT_ID
	payOut.PayChannel = model.PAGSMILE
	payOut.CountryCode = u.COUNTRY_CODE
	payOut.PayType = outValidator.PayType
	payOut.AppOrderId = outValidator.AppOrderId
	payOut.OrderId = transId
	payOut.PaymentId = transId
	payOut.FrozenId = frozenId
	payOut.Amount = outValidator.Amount
	payOut.BankCard = outValidator.BankCard
	payOut.UserId = outValidator.UserId
	payOut.UserName = outValidator.UserName
	payOut.Email = outValidator.Email
	payOut.Paytm = outValidator.PayTm
	payOut.IFSC = outValidator.IFSC
	payOut.Phone = outValidator.Phone
	if u.IsDebug() {
		payOut.Status = model.PAY_OUT_SUCCESS
	}
	engine, err := dao.GetMysql()
	if engine == nil || err != nil {
		logger.Error("PagSmilePayService_PayOut_GetDB_Err | err=%v", err)
		return nil, err
	}
	_, err = engine.Insert(payOut)
	if err != nil {
		logger.Error("PagSmilePayService_PayOut_Insert_Error | data=%+v | err=%v", payOut, err)
		return nil, err
	}

	if u.IsDebug() {
		logger.Warn("PagSmilePayService_PayOut_Debug | data=%+v", payOut)
		return payOut, nil
	}

	//发起支付
	requestUrl := fmt.Sprintf("%s", u.PAY_OUT_HOST)
	reqData := u.payoutRequestData(transId, outValidator)
	params := utils.SortedAndBuild(reqData)
	auth := utils.HmacSha256ToXString(params + u.MERCHANT_KEY)
	header := map[string][]string{
		"Content-Type":  {"application/json"},
		"MerchantId":    {u.MERCHANT_ID},
		"Authorization": {auth},
	}

	data, err := u.Request(requestUrl, "POST", "JSON", reqData, header)
	if err != nil {
		logger.Error("PagSmilePayService_PayOut_Request_Error | err=%v", err)
		return nil, err
	}

	logger.Debug("PagSmilePayService_PayOut_Info | requestUrl=%v | requestData=%v | response=%v | header=%v", requestUrl, params, string(data), header)

	res := &payoutResponse{}
	err = json.Unmarshal(data, res)
	if err != nil {
		logger.Error("PagSmilePayService_PayOut_JsonUnmarshal_Error | err=%v | res=%v", err, res)
		return nil, err
	}

	//
	resData := payoutSuccessResponse{}
	if res.Code == 200 {
		err = json.Unmarshal(data, &resData)
		if err != nil {
			logger.Error("PagSmilePayService_PayOut_JsonUnmarshal_Data_Error | err=%v | res=%v", err, string(data))
			return nil, err
		}
	}

	var (
		status      model.PayOutStatus
		thirdDesc   string
		referenceId string
	)
	payOut.ThirdCode = fmt.Sprint(res.Code)
	if res.Code != 200 {
		status = model.PAY_OUT_FAILD
		payOut.Status = status
		thirdDesc = res.Msg
	} else {
		status = model.PAY_OUT_ING
		referenceId = resData.Data.ID
	}
	_, err = engine.Where("id=?", payOut.Id).Update(&model.PayOut{
		Status:    status,
		ThirdCode: payOut.ThirdCode,
		PaymentId: referenceId,
		ThirdDesc: thirdDesc,
	})
	if err != nil {
		logger.Error("PagSmilePayService_PayOut_Insert_Error | data=%v | err=%v", payOut, err)
		return nil, err
	}
	return payOut, nil
}

func (u *PagSmilePayService) payoutRequestData(transId string, outValidator validator.PayOutValidator) map[string]interface{} {
	switch u.COUNTRY_STR {
	case countryCodeBRA:
		return u.braPayoutRequestData(transId, &outValidator)

	case countryCodeMEX:
		return u.mexPayoutRequestData(transId, &outValidator)
	}

	return map[string]interface{}{}
}

// pagsmile家 巴西提现只能是pix
func (u *PagSmilePayService) braPayoutRequestData(transId string, outValidator *validator.PayOutValidator) map[string]interface{} {
	return map[string]interface{}{
		"name":  outValidator.UserName,
		"phone": outValidator.Phone,
		"email": outValidator.Email,
		//"document_id":       outValidator.DocumentValue,
		//"document_type":     outValidator.DocumentType,
		"account":           outValidator.Account,
		"account_type":      outValidator.AccountType,
		"method":            "PIX",
		"custom_code":       transId,
		"fee_bear":          "merchant",
		"amount":            fmt.Sprint(float64(outValidator.Amount) / 100.0),
		"source_currency":   "USD",
		"arrival_currency":  "BRL",
		"notify_url":        u.PAYOUT_NOTIFY_URL,
		"additional_remark": "pay",
		"country":           "BRA",
	}
}

func (u *PagSmilePayService) mexPayoutRequestData(transId string, outValidator *validator.PayOutValidator) map[string]interface{} {
	return map[string]interface{}{
		"name":         outValidator.UserName,
		"phone":        outValidator.Phone,
		"email":        outValidator.Email,
		"bank_code":    "",
		"account":      outValidator.Account,
		"account_type": outValidator.AccountType,
		//"document_id":       outValidator.DocumentValue,
		//"document_type":     outValidator.DocumentType,
		"method":            "SPEI",
		"channel":           "SPEI",
		"custom_code":       transId,
		"fee_bear":          "merchant",
		"amount":            fmt.Sprint(float64(outValidator.Amount) / 100.0),
		"source_currency":   "USD",
		"arrival_currency":  "MXN",
		"notify_url":        u.PAYOUT_NOTIFY_URL,
		"additional_remark": "pay",
		"country":           "MEX",
	}
}

func (u *PagSmilePayService) PayOutBatch(frozenId int64, outValidator validator.PayOutValidator) (*model.PayOut, error) {
	return u.PayOut(frozenId, outValidator)
}

func (u *PagSmilePayService) InSignature(cb interface{}) bool {
	panic("implement me")
}

func (u *PagSmilePayService) OutSignature(cb interface{}) bool {
	panic("implement me")
}

type tradeCreate struct {
	AppId          string `json:"app_id"`
	Timestamp      string `json:"timestamp"`
	OutTradeNO     string `json:"out_trade_no"`
	Method         string `json:"method"`  // no
	Channel        string `json:"channel"` // no
	OrderAmount    int    `json:"order_amount"`
	OrderCurrency  string `json:"order_currency"`
	Subject        string `json:"subject"`
	Content        string `json:"content"` // no
	TradeType      string `json:"trade_type"`
	NotifyUrl      string `json:"notify_url"`
	ReturnUrl      string `json:"return_url"` // no
	BuyerID        string `json:"buyer_id"`
	TimeoutExpress string `json:"timeout_express"` // no
}

type tradeCreateResponse struct {
	Code       string `json:"code"`
	Msg        string `json:"msg"`
	SubCode    string `json:"sub_code"`
	SubMsg     string `json:"sub_msg"`
	OutTradeNO string `json:"out_trade_no"`
	TradeNO    string `json:"trade_no"`
	WebUrl     string `json:"web_url"`
}

type payoutResponse struct {
	Code int         `json:"code"`
	Msg  string      `json:"msg"`
	Time int64       `json:"time"`
	Data interface{} `json:"data"`
}

type payoutSuccessResponse struct {
	Code int    `json:"code"`
	Msg  string `json:"msg"`
	Time int64  `json:"time"`
	Data struct {
		ID              string `json:"id"`
		CustomCode      string `json:"custom_code"`
		SourceCurrency  string `json:"source_currency"`
		SourceAmount    string `json:"source_amount"`
		ArrivalCurrency string `json:"arrival_currency"`
		ArrivalAmount   string `json:"arrival_amount"`
		Status          string `json:"status"`
	} `json:"data"`
}

func (u *PagSmilePayService) Request(url string, method string, contentType string, sendData interface{}, header map[string][]string) (res []byte, err error) {
	client := &http.Client{}
	var req *http.Request
	if contentType == "JSON" {
		data, _ := json.Marshal(sendData)
		fmt.Println(string(data))
		reader := bytes.NewBuffer(data)
		req, err = http.NewRequest(method, url, reader)
	} else {
		s := sendData.(string)
		reader := strings.NewReader(s)
		req, err = http.NewRequest(method, url, reader)
	}
	//提交请求
	if err != nil {
		logger.Error("PagSmilePayService_Create_order | url=%v | err=%+v | req=%+v", url, err, sendData)
		return
	}
	req.Header = http.Header(header)
	//处理返回结果
	response, err := client.Do(req)
	if err != nil {
		logger.Error("PagSmilePayService_request_err | url=%v | err=%+v | req=%+v", url, err, sendData)
		return
	}
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		logger.Error("PagSmilePayService_response_read_err | url=%v | err=%+v | req=%+v", url, err, sendData)
		return
	}
	defer func() {
		_ = response.Body.Close()
	}()
	return body, nil
}
