package uppay

import "fmt"

// 注意字段的排序

type CollectOrder struct {
	Amount          int64  `json:"amount,omitempty"`
	ConsumerId      string `json:"consumerId,omitempty"`
	Country         string `json:"country,omitempty"`
	Currency        string `json:"currency,omitempty"`
	Ext             string `json:"ext,omitempty"`
	Feature         string `json:"feature,omitempty"`
	MerchantTradeId string `json:"merchantTradeId,omitempty"`
	NotifyUrl       string `json:"notifyUrl,omitempty"`
	ReturnUrl       string `json:"returnUrl,omitempty"`
	TradeType       string `json:"tradeType,omitempty"`
}

func (c CollectOrder) ForSign() CollectOrderForSign {
	return CollectOrderForSign{
		fmt.Sprintf("%v", c.Amount),
		c.ConsumerId,
		c.Country,
		c.Currency,
		c.Ext, c.Feature,
		c.MerchantTradeId,
		c.NotifyUrl,
		c.ReturnUrl,
		c.TradeType,
	}
}

type CollectOrderForSign struct {
	Amount          string `json:"amount,omitempty"`
	ConsumerId      string `json:"consumerId,omitempty"`
	Country         string `json:"country,omitempty"`
	Currency        string `json:"currency,omitempty"`
	Ext             string `json:"ext,omitempty"`
	Feature         string `json:"feature,omitempty"`
	MerchantTradeId string `json:"merchantTradeId,omitempty"`
	NotifyUrl       string `json:"notifyUrl,omitempty"`
	ReturnUrl       string `json:"returnUrl,omitempty"`
	TradeType       string `json:"tradeType,omitempty"`
}

type CreditCardExt struct {
	Address1      string `json:"address1"`
	Address2      string `json:"address2"`
	City          string `json:"city"`
	Country       string `json:"country"`
	DocumentType  string `json:"documentType"`
	DocumentValue string `json:"documentValue"`
	FirstName     string `json:"firstName"`
	LastName      string `json:"lastName"`
	State         string `json:"state"`
	UserEmail     string `json:"userEmail"`
	UserPhone     string `json:"userPhone"`
	ZipCode       string `json:"zipCode"`
}

type PixExt struct {
	FirstName string `json:"firstName"`
	LastName  string `json:"lastName"`
	UserEmail string `json:"userEmail"`

	DocumentType  string `json:"documentType"`
	DocumentValue string `json:"documentValue"`

	ContactsType  string `json:"contactsType"` // MOBILE, HOME, WORK, OTHER
	ContactsValue string `json:"contactsValue"`
	AutoQrCode    bool   `json:"autoQrCode"`
}

type CollectOrderResp struct {
	Code    string `json:"code"`
	Msg     string `json:"msg"`
	Success bool   `json:"success"`
	Data    struct {
		MerchantTradeId string `json:"merchantTradeId"`
		TradeId         string `json:"tradeId"`
		RedirectUrl     string `json:"redirectUrl"`
		ChannelField    string `json:"channelField"`
	} `json:"data"`
}

// 注意字段顺序

type QueryPayIn struct {
	Current         string `json:"current"`
	MerchantTradeId string `json:"merchantTradeId"`
	Size            string `json:"size"`
	TradeId         string `json:"tradeId"`
	//TradeType       string `json:"tradeType"`
}

/*
{
    "code":"200",
    "msg":"success",
    "data":{
        "records":[
            {
                "tradeId":"1636255425650102272",
                "merchantTradeId":"UIN1678948667045605080f",
                "consumerId":"",
                "notifyUrl":"http://test-pay-callback.rummyjosh.com/callback/uppay_callback",
                "returnUrl":"https://payment.rummybuffett.com/callback/success?ch=uppay",
                "amount":5300,
                "currency":"BRL",
                "status":"approved",
                "reverseStatus":null,
                "country":"",
                "captureStatus":null,
                "releaseAuthStatus":null,
                "cancelStatus":null,
                "chargebackStatus":null
            }
        ],
        "total":1,
        "size":16,
        "current":0
    },
    "success":true
}

{"code":"200","msg":"success","data":{"records":[],"total":1,"size":16,"current":1},"success":true}

*/

type PayInOrderRecord struct {
	TradeId         string `json:"tradeId"`
	MerchantTradeId string `json:"merchantTradeId"`
	ConsumerId      string `json:"consumerId"`
	Amount          int64  `json:"amount"`
	Currency        string `json:"currency"`
	Status          string `json:"status"` //
}

type QueryPayInResp struct {
	Code    string `json:"code"`
	Msg     string `json:"msg"`
	Success bool   `json:"success"`
	Data    struct {
		Records []PayInOrderRecord `json:"records"`
	} `json:"data"`
}

type PayInNotify struct {
	Amount           int64  `json:"amount"`
	AppId            string `json:"appId"`
	CaptureId        string `json:"captureId"`
	Currency         string `json:"currency"`
	MerchantId       string `json:"merchantId"`
	MerchantTradeId  string `json:"merchantTradeId"`
	Message          string `json:"message"`
	NotifyUrl        string `json:"notifyUrl"`
	RefundId         string `json:"refundId"`
	Sign             string `json:"sign"`
	Status           string `json:"status"`
	TechnicalMessage string `json:"technicalMessage"`
	TradeId          string `json:"tradeId"`
	TradeType        string `json:"tradeType"`
}

/*
{
	"bankDetail": {
		"accountNumber": "123456789",
		"account_type": "CHECKING", //CHECKING or Savings
		"bankCountryCode": "BR",
		"bankName": "NUBANK",
		"branchCode": "0001", // 支行码
		"currency": "BRL"
	},

	"countryCode":"BR",
	"currency":"BRL",
	"email":"8796852374@gmail.com",
	"entityType":"individual",
	"firstName":"Elianasia",
	"idNumber":"10030012345", // CPF
	"lastName":"Regina de Carvalho Recardo",
	"paymentType":"BankTransfer",
	"birthday":"1997-10-01"
}
*/

type BankDetail struct {
	AccountNumber   string `json:"accountNumber"`
	AccountType     string `json:"accountType"` //  //CHECKING or Savings
	BankCountryCode string `json:"bankCountryCode"`
	BankName        string `json:"bankName"`
	BranchCode      string `json:"branchCode"`
	Currency        string `json:"currency"`
}

type PayOutBrBankExt struct {
	BankDetail  BankDetail `json:"bankDetail"`
	Birthday    string     `json:"birthday"`     // 1997-10-01
	CountryCode string     `json:"country_code"` // BR
	Currency    string     `json:"currency"`     // BRL
	Email       string     `json:"email"`
	EntityType  string     `json:"entityType"` // "individual
	FirstName   string     `json:"firstName"`
	IdNumber    string     `json:"idNumber"` /* CPF */
	LastName    string     `json:"lastName"`
	PaymentType string     `json:"paymentType"`
}

type PixBankDetail struct {
	KeyType  string `json:"keyType"` // 可选择：MOBILE、EMAIL、CPF
	KeyValue string `json:"keyValue"`
}

type PayOutBrPixExt struct {
	FirstName  string        `json:"firstName"`
	LastName   string        `json:"lastName"`
	Birthday   string        `json:"birthday"`
	Email      string        `json:"email"`
	IdNumber   string        `json:"idNumber"` // 暂时只支持CPF
	BankDetail PixBankDetail `json:"bankDetail"`
}

/*
{
  "thirdUserId": "121231312",
  "tradeNumber": "2136451301",
  "accountCurrency": "USD",
  "beneficiaryId": 9,
  "amount": 100.00,
  "currency": "PHP",
  "callbackUrl": "https://dev-api.uppaynow.com/notify/trade",
  "terminal": "10041",
  "remark": "交易附言",
  "feePaidBy": "sender",
  "featureName": ""
}
*/

type PayOutOrder struct {
	Amount          string      `json:"amount"`     // 已币种最小精度为单位
	ConsumerId      string      `json:"consumerId"` // 第三方用户号
	Currency        string      `json:"currency"`
	EntityType      string      `json:"entityType"` // individual / corporate
	Ext             interface{} `json:"ext"`
	Feature         string      `json:"feature"`         // up pay 提供
	FeePaidBy       string      `json:"feePaidBy"`       // sender / receiver
	MerchantTradeId string      `json:"merchantTradeId"` // 三方交易号，防重
	NotifyUrl       string      `json:"notifyUrl"`
	PayerType       string      `json:"payerType"` // BankTransfer / eWallet
	Remark          string      `json:"remark"`
	ReturnUrl       string      `json:"returnUrl"`
	ShowCollectPage string      `json:"showCollectPage"` // 是否展示填写用户信息
	Terminal        string      `json:"terminal"`        // up pay 提供
}

/*
{
  "code": "200",
  "msg": "操作成功",
  "type": "success",
  "data": {
    "tradeId": "T*******",
    "merchantTradeId": "*********",
    "status": "Pending",
    "redirectUrl": null,
    "currency": "PHP",
    "amount": 10000,
    "createTime": "2023-03-20T14:00:00",
    "completeTime": null
  }
}

{
	"code":"200",
	"msg":"操作成功",
	"type":"success",
	"data":{
		"tradeId":"T2023032916375656606427",
		"merchantTradeId":"UOUT168007906807668205a2",
		"status":"pending",
		"redirectUrl":null,
		"currency":"BRL",
		"amount":"5000",
		"createTime":"2023-03-29T08:37:56",
		"completeTime":null
	}
}
*/

type PayOutResponse struct {
	Code string `json:"code"`
	Msg  string `json:"msg"`
	Type string `json:"type"`
	Data struct {
		TradeId         string `json:"tradeId"`
		MerchantTradeId string `json:"merchantTradeId"`
		Status          string `json:"status"`
		RedirectUrl     string `json:"redirectUrl"`
		Currency        string `json:"currency"`
		Amount          string `json:"amount"`
		CreateTime      string `json:"createTime"`
		CompleteTime    string `json:"completeTime"` // 0-ing, 1-success, 2-fail
	} `json:"data"`
}

type QueryPayout struct {
	ReferenceId string `json:"referenceId"` // 交易请求时的tradeNumber
	TradeNo     string `json:"tradeNo"`     // 交易时返回的paymentId
}

/*
{
	"code":"200",
	"msg":"操作成功",
	"type":"success",
	"data":{
		"tradeId":"T2023032916375656606427",
		"merchantTradeId":"UOUT168007906807668205a2",
		"status":"failure",
		"redirectUrl":null,
		"currency":"BRL",
		"amount":"5000",
		"createTime":"2023-03-29T08:37:56",
		"completeTime":null
	}
}
{"code":"200","msg":"操作成功","type":"success",
	"data":{
		"tradeId":"T2023040716022772504744",
		"merchantTradeId":"UOUT16808545390691211515",
		"status":"pending",
		"redirectUrl":null,
		"currency":"BRL",
		"amount":"5000",
		"createTime":"2023-04-07T08:02:28",
		"completeTime":null}}

*/

type QueryPayoutResp struct {
	Code string `json:"code"`
	Msg  string `json:"msg"`
	Type string `json:"type"`
	Data struct {
		TradeId         string `json:"tradeId"`
		MerchantTradeId string `json:"merchantTradeId"`
		Status          string `json:"status"`
		RedirectUrl     string `json:"redirectUrl"`
		Currency        string `json:"currency"`
		Amount          string `json:"amount"`
		CreateTime      string `json:"createTime"`
		CompleteTime    string `json:"completeTime"`
	} `json:"data"`
}

/*
{
  "code": "200",
  "msg": "操作成功",
  "type": "success",
  "data": {
    "tradeId": "T*******",
    "merchantTradeId": "*********",
    "status": "pending",
    "redirectUrl": null,
    "currency": "PHP",
    "amount": 10000,
    "createTime": "2023-03-20T14:00:00",
    "completeTime": null
  }
}

{
  "code": "200",
  "msg": "操作成功",
  "type": "success",
  "data": {
    "tradeId": "T*******",
    "merchantTradeId": "*********",
    "status": "error",
    "redirectUrl": null,
    "currency": "PHP",
    "amount": 10000,
    "createTime": "2023-03-20T14:00:00",
    "completeTime": null
  }
}
*/

type PayoutNotify struct {
	Code string `json:"code"`
	Msg  string `json:"msg"`
	Type string `json:"type"`
	Data struct {
		TradeId         string      `json:"tradeId"`
		MerchantTradeId string      `json:"merchantTradeId"`
		Status          string      `json:"status"` // 0-ing, 1-success, 2-fail
		RedirectUrl     interface{} `json:"redirectUrl"`
		Currency        string      `json:"currency"`
		Amount          int         `json:"amount"`
		CreateTime      string      `json:"createTime"`
		CompleteTime    interface{} `json:"completeTime"`
	} `json:"data"`
}

/*
{
	"bankDetail": {
		"accountNumber": "123456789",
		"accountType": "CHECKING", //CHECKING or Savings
		"bankCountryCode": "BR",
		"bankName": "NUBANK",
		"branchCode": "0001", // 支行码
		"currency": "BRL"
	},
	"birthday": "2003-01-01",
	"country_code": "BR",
	"currency": "BRL",
	"email": "***",
	"entityType": "individual",
	"firstName": "Test",
	"idNumber": "12345678901",  // CPF
	"lastName": "Abcd",
	"thirdUserId": "1234567890"
}

*/

// 创建收款人

type ReqCreateBeneficiary struct {
	BankDetail  BankDetail `json:"bankDetail"`
	birthday    string     `json:"birthday"`
	CountryCode string     `json:"country_code"`
	Currency    string     `json:"currency"`
	Email       string     `json:"email"`
	EntityType  string     `json:"entityType"`
	FirstName   string     `json:"firstName"`
	IDNumber    string     `json:"idNumber"`
	LastName    string     `json:"lastName"`
	ThirdUserId string     `json:"thirdUserId"`
}

type RespCreateBeneficiary struct {
	Code   int    `json:"code"`
	Msg    string `json:"msg"`
	Status int64  `json:"status"`
	Data   struct {
		AccountCurrency string `json:"accountCurrency"`
		BankDetail      struct {
			AccountName     string `json:"account_name"`
			AccountNumber   string `json:"account_number"`
			BankCountryCode string `json:"bank_country_code"`
			BankName        string `json:"bank_name"`
			PaymentMethod   string `json:"payment_method"`
			PurposeCode     string `json:"purpose_code"`
			RoutingNo       string `json:"routing_no"`
		} `json:"bankDetail"`

		BeneficiaryId string `json:"beneficiaryId"`
		Birthday      string `json:"birthday"`
		BusinessId    int    `json:"businessId"`
		CallbackUrl   string `json:"callbackUrl"`
		CountryCode   string `json:"country_code"`
		Currency      string `json:"currency"`
		Email         string `json:"email"`
		EntityType    string `json:"entityType"`
		FirstName     string `json:"firstName"`
		LastName      string `json:"lastName"`
		MerchantId    int    `json:"merchantId"`
		PaymentType   string `json:"payment_type"`
		Source        string `json:"source"`
		ThirdUserId   string `json:"thirdUserId"`
	} `json:"data"`
}
