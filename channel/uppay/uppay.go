package uppay

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"funzone_pay/app/validator"
	"funzone_pay/centerlog"
	"funzone_pay/dao"
	"funzone_pay/model"
	"funzone_pay/utils"
	"github.com/wonderivan/logger"
	"io/ioutil"
	"net/http"
	"net/url"
	"time"
)

const (
	RespCodeSuccess = "200"

	PayPending            = 1 //初始化
	PayPendingStr         = "pending"
	PayPendingAsync       = 2 // 本地异步处理
	PayPendingAsyncStr    = "pending_async"
	PayProcessing         = 3 // 处理中
	PayProcessingStr      = "processing"
	PayProcessingAsync    = 4
	PayProcessingAsyncStr = "processing_async"
	PayApproved           = 5 // 成功
	PayApprovedStr        = "approved"
	PayDeclined           = 6 // 拒绝
	PayDeclinedStr        = "declined"
	PayError              = 7 // 错误
	PayErrorStr           = "error"

	PayoutPending  = "pending"
	PayoutSuccess  = "approved"
	PayoutError    = "error"
	PayoutDeclined = "declined"
	PayoutFail     = "failure"
	PayoutReversed = "reversed"
)

var (
	orderError = errors.New("create order error")
)

type UpPayService struct {
	PAY_ACCOUNT_ID string
	PAY_CHANNEL    string
	COUNTRY_CODE   int

	ACCESS_KEY    string
	ACCESS_SECRET string
	TERMINAL      string
	FEATURE       string

	RETURN_URL     string
	IN_NOTIFY_URL  string // 异步支付、提现结果通知
	OUT_NOTIFY_URL string
	HOST_URL       string // 支付、提现的请求地址
	HOST_OUT_URL   string
}

func GetUpPayIns(conf map[string]interface{}) *UpPayService {
	return &UpPayService{
		PAY_ACCOUNT_ID: conf["PAY_ACCOUNT_ID"].(string),
		PAY_CHANNEL:    conf["PAY_CHANNEL"].(string),
		COUNTRY_CODE:   conf["COUNTRY_CODE"].(int),

		ACCESS_KEY:    conf["ACCESS_KEY"].(string),
		ACCESS_SECRET: conf["ACCESS_SECRET"].(string),
		TERMINAL:      conf["TERMINAL"].(string),
		FEATURE:       conf["FEATURE"].(string),

		RETURN_URL:     conf["RETURN_URL"].(string),
		IN_NOTIFY_URL:  conf["IN_NOTIFY_URL"].(string),
		OUT_NOTIFY_URL: conf["OUT_NOTIFY_URL"].(string),
		HOST_URL:       conf["HOST_URL"].(string),
		HOST_OUT_URL:   conf["HOST_OUT_URL"].(string),
	}
}

func (u UpPayService) Header(sign string, timestamp string) http.Header {
	return http.Header{
		"Content-Type": {"application/json; charset=utf-8"},
		"accessKey":    {u.ACCESS_KEY},
		"timestamp":    {timestamp},
		"sign":         {sign},
	}
}

func (u UpPayService) CreateOrder(args validator.PayEntryValidator) (*model.PayEntry, error) {
	orderId := utils.GetOrderId("UIN")
	timestamp := fmt.Sprintf("%v", time.Now().Unix())
	reqData, sign := u.genPayInReqData(orderId, timestamp, args)
	header := u.Header(sign, timestamp)

	data, err := u.Request(u.HOST_URL+"/payment/pay/prepay", reqData, header)
	logger.Debug("UpPayService_CreateOrder_Info | reqData=%v | head=%v | %v | err=%v | response=%v", string(reqData), header, args.AppOrderId, err, string(data))
	if err != nil {
		return nil, fmt.Errorf("create order error-%v", args.AppOrderId)
	}

	response := &CollectOrderResp{}
	err = json.Unmarshal(data, response)
	if err != nil {
		logger.Error("UpPayService_CreateOrder_JsonUnmarshal_Err | %v | err=%v | data=%v", args.AppOrderId, err, string(data))
		centerlog.OrderError(centerlog.MsgThirdPlatformFail, u.PAY_ACCOUNT_ID, args.AppId, args.AppOrderId, orderId, "false", err.Error())
		return nil, err
	}

	//
	if !response.Success { //
		return nil, fmt.Errorf("service busy, invalid code %v", args.AppOrderId)
	}

	//请求成功写入流水
	pE := new(model.PayEntry)
	pE.AppId = args.AppId
	pE.Uid = args.Uid
	pE.PayAccountId = u.PAY_ACCOUNT_ID
	pE.PayChannel = model.UPPAY
	pE.CountryCode = u.COUNTRY_CODE
	pE.AppOrderId = args.AppOrderId
	pE.OrderId = orderId
	pE.PaymentOrderId = response.Data.TradeId
	pE.Amount = args.Amount
	pE.UserId = args.UserId
	pE.UserName = args.UserName
	pE.Email = args.Email
	pE.Phone = args.Phone
	pE.Status = model.PAYING

	//
	engine, err := dao.GetMysql()
	if engine == nil || err != nil {
		logger.Error("UpPayService_CreateOrder_GetDB_Err | err=%v", err)
		return nil, err
	}
	_, err = engine.Insert(pE)
	if err != nil {
		logger.Error("UpPayService_CreateOrder_InsertDB_Err | err=%v", err)
		return nil, err
	}

	pE.PaymentLinkHost = response.Data.RedirectUrl
	return pE, nil
}

func (u UpPayService) Inquiry(orderId string, platformOrderId string) (*model.PayEntry, error) {
	timestamp := fmt.Sprintf("%v", time.Now().Unix())
	reqData, sign := u.genQueryPayInReqData(orderId, platformOrderId, timestamp)
	header := u.Header(sign, timestamp)

	resp, err := u.Request(u.HOST_URL+"/payment/pay/getOrderInfo", reqData, header)
	logger.Debug("UpPayService_Inquiry_Info | requestData=%v | %v | %v| response=%v", string(reqData), orderId, header, string(resp))
	if err != nil {
		logger.Error("UpPayService_Inquiry_Request_Error | err=%v", err)
		return nil, err
	}

	res := &QueryPayInResp{}
	err = json.Unmarshal(resp, res)
	if err != nil {
		logger.Error("UpPayService_Inquiry_JsonUnmarshal_Error | err=%v | res=%v", err, res)
		return nil, err
	}

	if !res.Success {
		logger.Error("UpPayService_Inquiry_GetError | err=%v | data=%v", err, orderId)
		return nil, fmt.Errorf("quiry[%v] error", orderId)
	}

	var order PayInOrderRecord
	for _, item := range res.Data.Records {
		if item.MerchantTradeId == orderId {
			order = item
			break
		}
	}

	if order.MerchantTradeId != orderId {
		logger.Error("UpPayService_Inquiry_GetError | order[%v] not committed", orderId)
		return nil, fmt.Errorf("no commited order")
	}

	engine, _ := dao.GetMysql()
	payIn := new(model.PayEntry)
	exist, err := engine.Where("order_id=?", orderId).Get(payIn)
	if err != nil {
		logger.Error("UpPayService_Inquiry_GetError | err=%v | data=%v", err, orderId)
		return nil, err
	}
	if !exist {
		return nil, errors.New("NotExist")
	}

	status := order.Status
	if status == PayApprovedStr {
		payIn.Status = model.PAYSUCCESS
		payIn.PaymentId = order.TradeId
	} else if status == PayDeclinedStr || status == PayErrorStr {
		payIn.Status = model.PAYFAILD
		payIn.ThirdCode = res.Code
		payIn.ThirdDesc = res.Msg
	}

	return payIn, nil
}

func (u UpPayService) InquiryPayout(orderId string, platformOrderId string) (*model.PayOut, error) {
	timestamp := time.Now().Format("20060102150405")
	reqData, sign := u.genQueryPayOutReqData(orderId, platformOrderId, timestamp)
	header := u.Header(sign, timestamp)
	resp, err := u.Request(u.HOST_OUT_URL+"/payout/V2/info", reqData, header)
	logger.Debug("UpPayService_Inquiry_out_Info | requestData=%v | %v | response=%v", string(reqData), orderId, string(resp))
	if err != nil {
		logger.Error("UpPayService_Inquiry_out_Request_Error | err=%v", err)
		return nil, err
	}

	res := &QueryPayoutResp{}
	err = json.Unmarshal(resp, res)
	if err != nil {
		logger.Error("UpPayService_Inquiry_out_JsonUnmarshal_Error | err=%v | res=%v", err, res)
		return nil, err
	}

	if res.Code != RespCodeSuccess {
		logger.Error("UpPayService_Inquiry_GetError | err=%v | data=%v", err, orderId)
		return nil, fmt.Errorf("quiry[%v] error", orderId)
	}

	engine, _ := dao.GetMysql()
	payOut := new(model.PayOut)
	exist, err := engine.Where("order_id=?", orderId).Get(payOut)
	if err != nil {
		logger.Error("UPayService_PayOut_Inquiry_GetError | err=%v | data=%v", err, orderId)
		return nil, err
	}
	if !exist {
		return nil, errors.New("NotExist")
	}

	status := res.Data.Status
	if status == PayoutSuccess {
		payOut.Status = model.PAY_OUT_SUCCESS
		payOut.PaymentId = res.Data.TradeId
	} else if status == PayoutFail || status == PayoutError || status == PayoutDeclined {
		payOut.Status = model.PAY_OUT_FAILD
		payOut.ThirdCode = res.Code
		//payOut.ThirdDesc = res.Msg
	}

	return payOut, nil
}

func (u UpPayService) PayOut(frozenId int64, args validator.PayOutValidator) (payOut *model.PayOut, err error) {
	orderId := utils.GetOrderId("UOUT")

	payOut = new(model.PayOut)
	payOut.Uid = args.Uid
	payOut.AppId = args.AppId
	payOut.PayAccountId = u.PAY_ACCOUNT_ID
	payOut.PayChannel = model.UPPAY
	payOut.CountryCode = u.COUNTRY_CODE
	payOut.PayType = args.PayType
	payOut.AppOrderId = args.AppOrderId
	payOut.OrderId = orderId
	payOut.PaymentId = orderId
	payOut.FrozenId = frozenId
	payOut.Amount = args.Amount
	payOut.BankCard = args.BankCard
	payOut.UserId = args.UserId
	payOut.UserName = args.UserName
	payOut.Email = args.Email
	payOut.Paytm = args.PayTm
	payOut.IFSC = args.IFSC
	payOut.Phone = args.Phone
	payOut.Status = model.PAY_OUT_ING
	engine, err := dao.GetMysql()
	if engine == nil || err != nil {
		logger.Error("UpPayService_PayOut_GetDB_Err | err=%v", err)
		return nil, err
	}
	_, err = engine.Insert(payOut)
	if err != nil {
		logger.Error("UpPayService_PayOut_Insert_Error | data=%+v | err=%v", payOut, err)
		return nil, err
	}

	timestamp := time.Now().Format("20060102150405") //yyyyMMddHHmmss
	reqData, sign := u.genPayOutReqData(orderId, timestamp, args)
	header := u.Header(sign, timestamp)

	// 发起
	urlEncData := url.QueryEscape(string(reqData))
	//data, err := u.Request(u.HOST_OUT_URL+"/openapi/payout/V2/payoutWithBeneficiary", []byte(reqData), header)
	data, err := u.Request(u.HOST_OUT_URL+"/payout/V2/payoutWithBeneficiary", []byte(urlEncData), header)

	logger.Debug("UpPayService_PayOut_Info | reqData=%v | %v | err=%v | response=%v", string(reqData), args.AppOrderId, err, string(data))
	if err != nil {
		return nil, fmt.Errorf("payout order error")
	}

	response := &PayOutResponse{}
	err = json.Unmarshal(data, response)
	if err != nil {
		logger.Error("UpPayService_PayOut_JsonUnmarshal_Err | %v | err=%v | data=%v", args.AppOrderId, err, string(data))
		centerlog.OrderError(centerlog.MsgThirdPlatformFail, u.PAY_ACCOUNT_ID, args.AppId, args.AppOrderId, orderId, "false", err.Error())
		return nil, err
	}

	code := response.Code
	if code != RespCodeSuccess {
		logger.Error("UpPayService_PayOut_Status_Err | %v | %v-%v", args.AppOrderId, code, response.Code)
		centerlog.OrderError(centerlog.MsgThirdPlatformFail, u.PAY_ACCOUNT_ID, payOut.AppId, payOut.AppOrderId, payOut.OrderId, response.Msg, fmt.Sprintf("code:%v", response.Code))

		return payOut, nil
	}

	// success
	paymentId := response.Data.TradeId
	_, err = engine.Where("id=?", payOut.Id).Update(&model.PayOut{
		PaymentId: paymentId,
	})
	if err != nil {
		logger.Error("UpPayService_PayOut_Update_Error | data=%v | err=%v", payOut, err)
		return nil, err
	}

	payOut.PaymentId = paymentId

	return payOut, nil
}

func (u UpPayService) PayOutBatch(frozenId int64, outValidator validator.PayOutValidator) (*model.PayOut, error) {
	return u.PayOut(frozenId, outValidator)
}

func (u UpPayService) InSignature(cb interface{}) bool {

	return true
}

func (u UpPayService) OutSignature(cb interface{}) bool {
	return u.InSignature(cb)
}

func (u UpPayService) genPayInReqData(orderId string, timestamp string, args validator.PayEntryValidator) ([]byte, string) {
	ext := PixExt{
		FirstName: args.UserName,
		LastName:  args.UserName,
		UserEmail: args.Email,
		//DocumentType:  args.DocumentType,
		//DocumentValue: args.DocumentValue,
		ContactsType:  "MOBILE",
		ContactsValue: args.Phone,
		AutoQrCode:    false,
	}

	extBytes, err := json.Marshal(ext)
	if err != nil {
		logger.Error("UpPayService_GenData_Error | err=%v | data=%v", err, ext)
		return []byte{}, ""
	}

	data := CollectOrder{
		MerchantTradeId: orderId,
		ConsumerId:      args.UserId,
		Currency:        "BRL",
		Amount:          int64(args.Amount * 100), // 分
		Feature:         "payin.pix.brl",
		TradeType:       "", // PIX情况下是非必须参数
		Country:         "", // feture = klarna 才需要填写
		NotifyUrl:       u.IN_NOTIFY_URL,
		ReturnUrl:       u.RETURN_URL,
		Ext:             string(extBytes),
	}

	bts, err := json.Marshal(data)
	if err != nil {
		logger.Error("UpPayService_GenData_Error_2 | err=%v | data=%v", err, ext)
		return []byte{}, ""
	}

	signData := data.ForSign()
	signBts, err := json.Marshal(signData)
	if err != nil {
		logger.Error("UpPayService_GenData_Error_3 | err=%v | data=%v", err, ext)
		return []byte{}, ""
	}

	sign := GetSign(signBts, timestamp, u.ACCESS_SECRET)

	return bts, sign
}

func (u UpPayService) genQueryPayInReqData(orderId string, tradeId string, timestamp string) ([]byte, string) {
	data := QueryPayIn{
		MerchantTradeId: orderId,
		TradeId:         tradeId,
		Current:         "0",
		Size:            "16",
	}

	bts, err := json.Marshal(data)
	if err != nil {
		logger.Error("UpPayService_GenQueryInData_Error_2 | err=%v | data=%v", err, data)
		return []byte{}, ""
	}

	sign := GetSign(bts, timestamp, u.ACCESS_SECRET)
	return bts, sign
}

func (u UpPayService) genQueryPayOutReqData(orderId string, tradeId string, timestamp string) ([]byte, string) {
	data := QueryPayout{
		TradeNo:     tradeId,
		ReferenceId: orderId,
	}

	bts, err := json.Marshal(data)
	if err != nil {
		logger.Error("UpPayService_GenQueryOutData_Error_2 | err=%v | data=%v", err, data)
		return []byte{}, ""
	}

	sign := GetSign(bts, timestamp, u.ACCESS_SECRET)
	return bts, sign
}

func (u UpPayService) genPayOutReqData(orderId string, timestamp string, args validator.PayOutValidator) ([]byte, string) {

	ext := PayOutBrPixExt{
		FirstName:  args.UserName,
		LastName:   args.UserName,
		Birthday:   "2000-11-06",
		Email:      args.Email,
		IdNumber:   args.DocumentValue,
		BankDetail: PixBankDetail{string(args.CardType), args.BankCard},
		//BankDetail: BankDetail{
		//	AccountNumber:   args.BankCard,
		//	AccountType:     "CHECKING",
		//	BankCountryCode: "BR",
		//	BankName:        args.BankName,
		//	BranchCode:      args.BankCode,
		//	Currency:        "BRL",
		//},
		//
		//Birthday:    args.Birthday, // 1997-10-01
		//CountryCode: "BR",
		//Currency:    "BRL",
		//Email:       args.Email,
		//EntityType:  "individual",
		//FirstName:   args.UserName,
		//IdNumber:    args.DocumentValue,
		//LastName:    args.UserName,
		//PaymentType: "BankTransfer",
	}

	extBytes, err := json.Marshal(ext)
	if err != nil {
		logger.Error("UpPayService_GenData_Error | err=%v | data=%v", err, ext)
		return []byte{}, ""
	}

	data := PayOutOrder{
		Terminal:        u.TERMINAL,
		Feature:         u.FEATURE,
		Currency:        "BRL",
		Amount:          fmt.Sprintf("%v", int64(args.Amount*100)),
		MerchantTradeId: orderId,
		ConsumerId:      args.UserId,
		ShowCollectPage: fmt.Sprintf("%v", false),
		PayerType:       "BankTransfer",
		EntityType:      "individual",
		NotifyUrl:       u.OUT_NOTIFY_URL,
		ReturnUrl:       u.OUT_NOTIFY_URL,
		FeePaidBy:       "sender",
		Remark:          "pix",
		//Ext:             ext,
		Ext: string(extBytes),
	}

	bts, err := json.Marshal(data)
	if err != nil {
		logger.Error("UpPayService_GenPayoutData_Error_2 | err=%v | data=%v", err, data)
		return []byte{}, ""
	}

	sign := GetSign(bts, timestamp, u.ACCESS_SECRET)

	return bts, sign
}

//func (u UpPayService) createBeneficiary(args validator.PayOutValidator) (beneficiaryId string, err error) {
//
//	req := ReqCreateBeneficiary{
//		birthday:    "2000-01-01",
//		CountryCode: "BR",
//		Currency:    "BRL",
//		Email:       args.Email,
//		EntityType:  "individual",
//		FirstName:   args.UserName,
//		IDNumber:    args.DocumentValue,
//		LastName:    args.UserName,
//		ThirdUserId: args.UserId,
//
//		BankDetail: BankDetail{
//			AccountNumber:   args.BankCard,
//			AccountType:     string(args.CardType),
//			BankCountryCode: "BR",
//			//BankName:        args.BankName,
//			BranchCode:      args.BankCode,
//			Currency:        "BRL",
//		},
//	}
//
//	reqData, err := json.Marshal(req)
//	if err != nil {
//		logger.Error("UpPayService_CreateBeneficiary_Error | err=%v | data=%v", err, req)
//		return "", fmt.Errorf("create beneficiary error")
//	}
//
//	timestamp := time.Now().Format("20060102150405")
//	sign := GetSign(reqData, timestamp, u.ACCESS_SECRET)
//
//	header := u.Header(sign, timestamp)
//
//	// 发起
//	urlEncData := url.QueryEscape(string(reqData))
//	data, err := u.Request(u.HOST_OUT_URL+"/openapi/payout/v2/beneficiary/createOrUpdate", []byte(urlEncData), header)
//
//	logger.Debug("UpPayService_CreateBeneficiary | reqData=%v | %v | err=%v | response=%v | header=%v", string(reqData), args.AppOrderId, err, string(data), header)
//	if err != nil {
//		return "", fmt.Errorf("req create beneficiary error")
//	}
//
//	var res RespCreateBeneficiary
//	err = json.Unmarshal(data, &res)
//	if err != nil {
//		logger.Debug("UpPayService_CreateBeneficiary | reqData=%v | %v | err=%v", string(reqData), args.AppOrderId, err)
//		return "", fmt.Errorf("unmarshal create beneficiary response error")
//	}
//
//	return res.Data.BeneficiaryId, nil
//}

func (u UpPayService) Request(url string, data []byte, header map[string][]string) (res []byte, err error) {
	reader := bytes.NewBuffer(data)
	req, err := http.NewRequest("POST", url, reader)
	if err != nil {
		logger.Error("UPPayService_Request_err | url=%v | err=%+v | req=%+v", url, err, string(data))
		return
	}
	req.Header = http.Header(header)

	//处理返回结果
	client := &http.Client{}
	response, err := client.Do(req)
	if err != nil {
		logger.Error("UPPayService_Request_err | url=%v | err=%+v | req=%+v", url, err, string(data))
		return
	}

	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		logger.Error("UPPayService_response_read_err | url=%v | err=%+v | req=%+v", url, err, string(data))
		return
	}
	defer func() {
		_ = response.Body.Close()
	}()
	return body, nil
}

func GetSign(data []byte, timestamp string, secret string) string {
	src := fmt.Sprintf("%v%v", string(data), timestamp)
	sign := utils.HmacSha256ToXStringWithKey(src, secret)
	fmt.Printf("\n\n%v\n\n%v\n\n%v\n\n", src, secret, sign)
	return sign
}
