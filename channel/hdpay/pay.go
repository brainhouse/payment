package hdpay

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"funzone_pay/app/validator"
	"funzone_pay/common"
	"funzone_pay/dao"
	"funzone_pay/model"
	"funzone_pay/utils"
	"github.com/wonderivan/logger"
	"io/ioutil"
	"net/http"
	"strings"
	"time"
)

const (
	RespCodeSuccess = "0000" // 请求成功
	RespCodeFail    = "9999"

	PayInStatusFail       = 0 // 失败
	PayIntStatusSuccess   = 1 // 成功
	PayInStatusPending    = 2 // 待支付
	PayInStatusProcessing = 3 // 支付中
	PayInStatusWaitSearch = 4 // 已退款
	PayInStatusRevert     = 5 // 待创建

	PayoutStatusCredited   = "Credited"   // Credited
	PayoutStatusProcessing = "Processing" // Processing
	PayoutStatusPending    = "Pending"    // Pending
	PayoutStatusRejected   = "REJECTED"
)

type HDPayService struct {
	HOST           string
	APPID          string
	PAY_ACCOUNT_ID string
	PAY_CHANNEL    string
	COUNTRY_CODE   int
	SK             string
	CLIENT_ID      string
}

func GetHDPayIns(conf map[string]interface{}) *HDPayService {
	return &HDPayService{
		HOST:           conf["HOST"].(string),
		PAY_ACCOUNT_ID: conf["PAY_ACCOUNT_ID"].(string),
		PAY_CHANNEL:    conf["PAY_CHANNEL"].(string),
		COUNTRY_CODE:   conf["COUNTRY_CODE"].(int),
		SK:             conf["SK"].(string),
		CLIENT_ID:      conf["CLIENT_ID"].(string),
	}
}

func (oe HDPayService) CreateOrder(args validator.PayEntryValidator) (*model.PayEntry, error) {
	return nil, fmt.Errorf("HPAY Unsupported CreateOrder")
}

func (HDPayService) Inquiry(string, string) (*model.PayEntry, error) {
	return nil, fmt.Errorf("HPAY Unsupported Inquiry")
}

func (hd HDPayService) InquiryPayout(orderId string, paymentId string) (*model.PayOut, error) {
	header := http.Header{}
	header.Add("Content-Type", "application/json")
	header.Add("x-client-id", hd.CLIENT_ID)
	header.Add("x-client-secret", hd.SK)
	reqData := &struct {
		PayoutId string `json:"payout_id"`
	}{
		PayoutId: paymentId,
	}

	// https://kepler.haodapayments.com/api/v1/payout/checkstatus
	requestUrl := fmt.Sprintf("%s/api/v1/payout/checkstatus", hd.HOST)
	data, err := hd.Request(requestUrl, "POST", "JSON", reqData, header)
	if err != nil {
		logger.Error("HDPayService_InquiryPayout_Request_Error | err=%v", err)
		return nil, err
	}
	logger.Debug("HDPayService_InquiryPayout_Info | data=%v | orderId=%v | req=%v", string(data), orderId, reqData)
	responseData := &struct {
		Code   string `json:"status_code"`
		Status string `json:"status"`
		Msg    string `json:"message"`
		Data   struct {
			Status string `json:"status"`
			Utr    string `json:"utr"`
		} `json:"data"`
	}{}

	err = json.Unmarshal(data, responseData)
	if err != nil {
		logger.Error("HDPayService_InquiryPayout_JsonError | err=%v | data=%v", err, string(data))
		return nil, err
	}

	// pending
	if responseData.Code == "200" && (responseData.Data.Status == PayoutStatusProcessing || responseData.Data.Status == PayoutStatusPending) {
		return nil, fmt.Errorf("pending")
	}

	//
	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("HDPayService_InquiryPayout_Engine_Error | data=%+v | err=%v", orderId, err)
		return nil, err
	}
	payOut := new(model.PayOut)
	exist, err := engine.Where("order_id=?", orderId).Get(payOut)
	if err != nil {
		logger.Error("HDPayService_InquiryPayout_GetError | err=%v | data=%v", err, orderId)
		return nil, err
	}
	if !exist {
		return nil, errors.New("NotExist")
	}

	if responseData.Code == "401" && responseData.Status == "failed" || responseData.Code == "200" && responseData.Data.Status == PayoutStatusRejected { // order id invalid
		payOut.Status = model.PAY_OUT_FAILD
		payOut.ThirdCode = responseData.Code
		payOut.ThirdDesc = responseData.Msg
	} else if responseData.Code == "200" && (responseData.Data.Status == "success" || responseData.Data.Status == PayoutStatusCredited) {
		payOut.Status = model.PAY_OUT_SUCCESS
		payOut.Utr = responseData.Data.Utr
	}
	return payOut, nil
}

func (hd *HDPayService) PayOut(frozenId int64, args validator.PayOutValidator) (payOut *model.PayOut, err error) {
	if args.PayType != model.PT_BANK {
		return nil, fmt.Errorf("unsupported type[%v] from %v", args.PayType, args.AppId)
	}

	orderId := utils.GetOrderId("HDO")

	isAlarm := true
	forceAlarm := false
	msg := ""
	defer func() {
		if isAlarm || forceAlarm {
			common.PushAlarmEvent(common.Warning, fmt.Sprintf("%v", args.Uid), "HDPayService/Payout", "", fmt.Sprintf("%v-%v-%v payout[%v] msg:%v, error:%v", args.AppId, args.Uid, args.AppOrderId, orderId, msg, err))
		}
	}()

	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("HDPayServicePayOut_Engine_Error | data=%+v | err=%v", payOut, err)
		return nil, err
	}

	payOut = new(model.PayOut)
	payOut.Uid = args.Uid
	payOut.AppId = args.AppId
	payOut.PayAccountId = hd.PAY_ACCOUNT_ID
	payOut.PayChannel = model.HDPAY
	payOut.CountryCode = hd.COUNTRY_CODE
	payOut.PayType = args.PayType
	payOut.AppOrderId = args.AppOrderId
	payOut.OrderId = orderId
	payOut.FrozenId = frozenId
	payOut.Amount = args.Amount
	payOut.BankCard = args.BankCard
	payOut.UserId = args.UserId
	payOut.UserName = args.UserName
	payOut.Email = args.Email
	payOut.Paytm = args.PayTm
	payOut.IFSC = args.IFSC
	payOut.Phone = args.Phone
	_, err = engine.Insert(payOut)

	if err != nil {
		logger.Error("HDPayServicePayOut_PayOut_Insert_Error | data=%+v | err=%v", payOut, err)
		return nil, err
	}
	var sendData interface{}
	/**
	{
	    "account_number": "919999999999",
	    "account_ifsc": "PYTM0TEST",
	    "bankname": "Paytm Payments Bank",
	    "confirm_acc_number" : "919999999999",
	    "requesttype" : "IMPS",
	    "beneficiary_name" : "TEST",

	    "amount" : "10",
	    "narration" : "Test bank transaction",
	    "reference" : "test123"
	  }
	*/
	sendData = &struct {
		AccountNumber    string `json:"account_number"`
		AccountIfsc      string `json:"account_ifsc"`
		Bankname         string `json:"bankname"`
		ConfirmAccNumber string `json:"confirm_acc_number"`
		Requesttype      string `json:"requesttype"`
		BeneficiaryName  string `json:"beneficiary_name"`
		Amount           string `json:"amount"`
		Narration        string `json:"narration"`
		Reference        string `json:"reference"`
	}{
		AccountNumber:    args.BankCard,
		AccountIfsc:      args.IFSC,
		Bankname:         "ptm bank",
		ConfirmAccNumber: args.BankCard,
		Requesttype:      "IMPS",
		BeneficiaryName:  args.UserName,
		Amount:           fmt.Sprint(args.Amount),
		Narration:        "payout",
		Reference:        orderId,
	}

	header := http.Header{}
	header.Add("Content-Type", "application/json")
	header.Add("x-client-id", hd.CLIENT_ID)
	header.Add("x-client-secret", hd.SK)

	// https://kepler.haodapayments.com/api/v1/payout/initiate
	host := fmt.Sprintf("%v/api/v1/payout/initiate", hd.HOST)
	bts, _ := json.Marshal(sendData)
	data, err := hd.Request(host, http.MethodPost, "JSON", sendData, header)
	logger.Debug("HDPayService_Payout | req=%v | res=%v | err=%v", string(bts), string(data), err)
	if err != nil {
		return nil, err
	}

	//{"status_code":"200","status":"Processing","message":"Kindly allow some time for the payout to process.","payout_id":"H11092304141147512131563","reference":"HOUT16813924860505002285","locked_funds":"0.00","balance":"500.00","channel":3}
	res := &struct {
		Code     string `json:"status_code"`
		Status   string `json:"status"`
		Message  string `json:"message"`
		PayoutId string `json:"payout_id"`
	}{}
	err = json.Unmarshal(data, res)
	if err != nil {
		logger.Error("HDPayService_PayOut_JsonUnmarshal_Error | err=%v | res=%v", err, string(data))
		return nil, err
	}

	if res.Code == "200" && res.PayoutId == "" {
		forceAlarm = true
	}

	thirdCode := ""
	thirdDesc := ""
	status := model.PAY_OUT_ING

	// 平台方错误
	if res.PayoutId == "" {
		status = model.PAY_OUT_FAILD
		thirdCode = res.Code
		thirdDesc = fmt.Sprintf("empty->%v", res.Message)
		msg = thirdDesc
		forceAlarm = true
	}
	_, err = engine.Where("id=?", payOut.Id).Update(&model.PayOut{
		Status:     status,
		ThirdCode:  thirdCode,
		PaymentId:  res.PayoutId,
		ThirdDesc:  thirdDesc,
		FinishTime: time.Now().Unix(),
	})
	if err != nil {
		logger.Error("HDPayService_PayOut_Insert_Error | data=%v | err=%v", payOut, err)
		return nil, err
	}

	isAlarm = false
	payOut.Status = status
	return payOut, nil

}

func (hd HDPayService) PayOutBatch(frozenId int64, args validator.PayOutValidator) (*model.PayOut, error) {
	return hd.PayOut(frozenId, args)
}

func (HDPayService) InSignature(cb interface{}) bool {
	logger.Error("un implement InSignature")
	return false
}

func (HDPayService) OutSignature(cb interface{}) bool {
	logger.Error("un implement OutSignature")
	return false
}

func (oe *HDPayService) Request(url string, method string, contentType string, sendData interface{}, header map[string][]string) (res []byte, err error) {
	client := &http.Client{}
	var req *http.Request
	if contentType == "JSON" {
		data, _ := json.Marshal(sendData)
		fmt.Println(string(data))
		reader := bytes.NewBuffer(data)
		req, err = http.NewRequest(method, url, reader)
	} else {
		s := sendData.(string)
		reader := strings.NewReader(s)
		req, err = http.NewRequest(method, url, reader)
	}
	fmt.Println(url)
	//提交请求
	if err != nil {
		logger.Error("HDPayService_Create_order | url=%v | err=%+v | req=%+v", url, err, sendData)
		return
	}
	req.Header = http.Header(header)
	//处理返回结果
	response, err := client.Do(req)
	if err != nil {
		logger.Error("HDPayService_request_err | url=%v | err=%+v | req=%+v", url, err, sendData)
		return
	}
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		logger.Error("HDPayService_response_read_err | url=%v | err=%+v | req=%+v", url, err, sendData)
		return
	}
	defer func() {
		_ = response.Body.Close()
	}()
	return body, nil
}
