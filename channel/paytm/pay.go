package paytm

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"funzone_pay/app/validator"
	"funzone_pay/dao"
	"funzone_pay/model"
	"funzone_pay/utils"
	"github.com/spf13/viper"
	"github.com/wonderivan/logger"
	"io/ioutil"
	"net/http"
	"net/url"
	"regexp"
	"strings"
	"time"
)

type PaytmService struct {
	PAY_ACCOUNT_ID string
	PAY_CHANNEL    string
	COUNTRY_CODE   int
	Host           string
	COLLECT_HOST   string
	MID            string
	SK             string
	SUBWALLET_ID   string
	CB_URL         string
	IN_HOST        string
	IN_MID         string
	IN_SK          string
	IN_CB_URL      string
}

func GetPayTm() *PaytmService {
	return &PaytmService{
		Host:         viper.GetString("paytm.HOST"),
		MID:          viper.GetString("paytm.MID"),
		SK:           viper.GetString("paytm.SK"),
		SUBWALLET_ID: viper.GetString("paytm.SUBWALLET_ID"),
		CB_URL:       viper.GetString("paytm.CB_URL"),
		IN_HOST:      viper.GetString("paytm.IN_HOST"),
		IN_MID:       viper.GetString("paytm.IN_MID"),
		IN_SK:        viper.GetString("paytm.IN_SK"),
		IN_CB_URL:    viper.GetString("paytm.IN_CB_URL"),
	}
}

func GetPayTmIns(conf map[string]interface{}) *PaytmService {
	return &PaytmService{
		PAY_ACCOUNT_ID: conf["PAY_ACCOUNT_ID"].(string),
		PAY_CHANNEL:    conf["PAY_CHANNEL"].(string),
		COUNTRY_CODE:   conf["COUNTRY_CODE"].(int),
		Host:           conf["HOST"].(string),
		MID:            conf["MID"].(string),
		SK:             conf["SK"].(string),
		SUBWALLET_ID:   conf["SUBWALLET_ID"].(string),
		COLLECT_HOST:   conf["COLLECT_HOST"].(string),
		CB_URL:         conf["CB_URL"].(string),
		IN_HOST:        conf["IN_HOST"].(string),
		IN_MID:         conf["IN_MID"].(string),
		IN_SK:          conf["IN_SK"].(string),
		IN_CB_URL:      conf["IN_CB_URL"].(string),
	}
}

func (ps *PaytmService) CreateOrder(args validator.PayEntryValidator) (*model.PayEntry, error) {
	orderId := "PTM" + utils.GetOrderId("IN")
	enableList := []*EnablePaymentMode{
		{Mode: "BALANCE", Channels: []string{}},
		{Mode: "PPBL", Channels: []string{}},
		{Mode: "DEBIT_CARD", Channels: []string{}},
		{Mode: "EMI", Channels: []string{}},
		{Mode: "PAYTM_DIGITAL_CREDIT", Channels: []string{}},
		{Mode: "CREDIT_CARD", Channels: []string{"VISA", "MASTER", "AMEX"}},
		{Mode: "UPI", Channels: []string{"UPI", "UPIPUSH", "UPIPUSHEXPRESS"}},
	}
	rD := &PaytmRequestData{
		CallbackUrl:       ps.IN_CB_URL,
		EnablePaymentMode: enableList,
		RequestType:       "Payment",
		Mid:               ps.IN_MID,
		WebsiteName:       "DEFAULT",
		OrderId:           orderId,
		TxnAmount: &PaytmRequestDataTxnAmount{
			Value:    fmt.Sprint(args.Amount),
			Currency: "INR",
		},
		UserInfo: &PaytmRequestDataUserInfo{
			CustId:    fmt.Sprintf("CUST_%v", args.Phone),
			Mobile:    args.Phone,
			Email:     args.Email,
			FirstName: args.UserName,
		},
	}
	singData, _ := json.Marshal(rD)
	paytmChecksum := GenerateSignatureByString(string(singData), ps.IN_SK)
	header := map[string][]string{
		"Content-Type": {"application/json"},
		"mid":          {ps.IN_MID},
	}
	var body = make(map[string]interface{})
	body["body"] = rD
	body["head"] = map[string]interface{}{
		"signature": paytmChecksum,
	}
	requestUrl := fmt.Sprintf("%s/%s?mid=%v&orderId=%v", ps.IN_HOST, "theia/api/v1/initiateTransaction", ps.IN_MID, orderId)
	data, err := ps.Request(requestUrl, "POST", "JSON", body, header)

	logger.Debug("PaytmService_CreateOrder_Info | orderReq=%v | err=%v | res=%v", body, err, string(data))
	paytmOrder := new(PaytmOrderResponse)
	err = json.Unmarshal(data, paytmOrder)
	if err != nil {
		logger.Error("PaytmService_CreateOrder_JsonUnmarshal_Err | err=%v", err)
		return nil, err
	}
	//请求成功写入流水
	pE := new(model.PayEntry)
	pE.Uid = args.Uid
	pE.AppId = args.AppId
	pE.Amount = args.Amount
	pE.UserId = args.UserId
	pE.UserName = args.UserName
	pE.Email = args.Email
	pE.Phone = args.Phone
	pE.CountryCode = ps.COUNTRY_CODE
	pE.PayChannel = model.PAYTM
	pE.PayAccountId = ps.PAY_ACCOUNT_ID
	pE.AppOrderId = args.AppOrderId
	pE.OrderId = orderId
	pE.PaymentOrderId = paytmOrder.Body.TxnToken
	pE.ThirdCode = paytmOrder.Body.ResultInfo.ResultCode
	pE.Status = model.PAYING
	pE.ThirdDesc = paytmOrder.Body.ResultInfo.ResultMsg
	pE.PayAppId = ps.IN_MID
	pE.FinishTime = time.Now().Unix()
	engine, err := dao.GetMysql()
	if engine == nil || err != nil {
		logger.Error("PaytmService_CreateOrder_GetDB_Err | err=%v", err)
		return nil, err
	}
	_, err = engine.Insert(pE)
	if err != nil {
		logger.Error("PaytmService_CreateOrder_InsertDB_Err | err=%v", err)
		return nil, err
	}
	pE.PaymentLinkHost = ps.COLLECT_HOST
	logger.Debug("PaytmService_CreateOrder_Request | req=%+v | res=%+v", body, paytmOrder)
	return pE, err
}

func (ps *PaytmService) CheckSign(dataStr string) map[string]interface{} {
	enEscapeUrl, _ := url.QueryUnescape(dataStr)

	reg := regexp.MustCompile("CHECKSUMHASH=(.*?)&")
	sign := reg.FindAllStringSubmatch(enEscapeUrl, -1)
	if len(sign) == 0 {
		reg1 := regexp.MustCompile("CHECKSUMHASH=(.*?)$")
		sign = reg1.FindAllStringSubmatch(enEscapeUrl, -1)
	}
	checkSign := sign[0][1]

	values, _ := url.ParseQuery(enEscapeUrl)
	var data = make(map[string]string)
	for k, v := range values {
		data[k] = v[0]
	}
	signData := make(map[string]interface{})
	signData["data"] = data
	signData["hash"] = checkSign
	return signData
}

func (ps *PaytmService) InSignature(data interface{}) bool {
	signData := data.(map[string]interface{})
	sData := signData["data"].(map[string]string)
	hash := signData["hash"].(string)
	return VerifySignature(sData, ps.IN_SK, hash)
}

func (ps *PaytmService) OutSignature(data interface{}) bool {
	return true
}

func (ps *PaytmService) Inquiry(paymentId string, orderId string) (*model.PayEntry, error) {
	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("PaytmService_Inquiry_Engine_Error | data=%+v | err=%v", orderId, err)
		return nil, err
	}
	payEntry := new(model.PayEntry)
	exist, err := engine.Where("order_id=? and status=0", orderId).Get(payEntry)
	if err != nil {
		logger.Error("PaytmService_Inquiry_GetError | err=%v | data=%v", err, orderId)
		return nil, err
	}
	if !exist {
		return nil, errors.New("NotExist")
	}
	header := map[string][]string{
		"Content-Type": {"application/json"},
		"mid":          {ps.IN_MID},
	}
	tokenUrl := fmt.Sprintf("%s/v3/order/status", ps.IN_HOST)
	tokenReq := &struct {
		Head struct {
			Signature string `json:"signature"`
		} `json:"head"`
		Body struct {
			Mid     string `json:"mid"`
			OrderId string `json:"orderId"`
		} `json:"body"`
	}{}
	tokenReq.Body.Mid = ps.IN_MID
	tokenReq.Body.OrderId = orderId
	singData, _ := json.Marshal(tokenReq.Body)
	paytmChecksum := GenerateSignatureByString(string(singData), ps.IN_SK)
	tokenReq.Head.Signature = paytmChecksum

	tokenJsonData, err := ps.Request(tokenUrl, "POST", "JSON", tokenReq, header)
	if err != nil {
		logger.Error("PaytmService_Inquiry_Request_TokenError | err=%v | orderId=%v", err, orderId)
		return nil, err
	}
	tokenResponse := &struct {
		Body struct {
			ResultInfo struct {
				ResultStatus string `json:"resultStatus"`
				ResultMsg    string `json:"resultMsg"`
			} `json:"resultInfo"`
		} `json:"body"`
	}{}
	err = json.Unmarshal(tokenJsonData, tokenResponse)

	logger.Debug("PaytmService_Inquiry_Info | orderId=%v | data=%v", err, orderId, string(tokenJsonData))

	if err != nil {
		logger.Error("PaytmService_Inquiry_JsonError | err=%v | data=%v", err, string(tokenJsonData))
		return nil, err
	}
	if tokenResponse.Body.ResultInfo.ResultStatus != "TXN_SUCCESS" && payEntry.Created < time.Now().Unix()-3*3600 {
		payEntry.Status = model.PAYFAILD
		payEntry.ThirdDesc = tokenResponse.Body.ResultInfo.ResultMsg
	} else if tokenResponse.Body.ResultInfo.ResultStatus == "TXN_SUCCESS" {
		payEntry.Status = model.PAYSUCCESS
	}
	return payEntry, nil
}

func (ps *PaytmService) InquiryPayout(orderId string, ext string) (*model.PayOut, error) {
	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("PaytmService_InquiryPayOut_Engine_Error | orderId=%+v | err=%v", orderId, err)
		return nil, err
	}
	var rData = make(map[string]string)
	rData["orderId"] = orderId
	singData, _ := json.Marshal(rData)
	paytmChecksum := GenerateSignatureByString(string(singData), ps.SK)
	header := map[string][]string{
		"Content-Type": {"application/json"},
		"x-mid":        {ps.MID},
		"x-checksum":   {paytmChecksum},
	}
	requestUrl := fmt.Sprintf("%s%s", ps.Host, "disburse/order/query")
	data, err := ps.Request(requestUrl, "POST", "JSON", rData, header)
	if err != nil {
		logger.Error("PaytmService_InquiryPayOut_Request_Error | err=%v", err)
		return nil, err
	}
	logger.Debug("PaytmService_InquiryPayOut_Request_Info | data=%+v | req=%+v", string(data), rData)
	res := &struct {
		StatusCode    string `json:"statusCode"`
		Status        string `json:"status"`
		StatusMessage string `json:"statusMessage"`
	}{}
	err = json.Unmarshal(data, res)
	if err != nil {
		logger.Error("PaytmService_InquiryPayOut_JsonUnmarshal_Error | err=%v | res=%v", err, res)
		return nil, err
	}
	var status model.PayOutStatus
	var thirdDesc string
	if res.Status == "FAILURE" {
		status = model.PAY_OUT_FAILD
		thirdDesc = res.StatusMessage
	} else if res.Status == "SUCCESS" {
		status = model.PAY_OUT_SUCCESS
	} else {
		return nil, nil
	}
	payOut := &model.PayOut{
		Status:    status,
		ThirdCode: res.Status,
		ThirdDesc: thirdDesc,
	}
	_, err = engine.Where("order_id=?", orderId).Update(payOut)
	if err != nil {
		logger.Error("PaytmService_Inquiry_Insert_Error | orderId=%+v | err=%v", orderId, err)
		return nil, err
	}
	return payOut, nil
}

func (ps *PaytmService) AddFunds(amount int64) error {
	var rData = make(map[string]string)
	rData["subwalletGuid"] = ps.SUBWALLET_ID
	rData["amount"] = fmt.Sprint(amount)
	singData, _ := json.Marshal(rData)
	paytmChecksum := GenerateSignatureByString(string(singData), ps.SK)
	fmt.Println(paytmChecksum)

	header := map[string][]string{
		"Content-Type": {"application/json"},
		"x-mid":        {ps.MID},
		"x-checksum":   {paytmChecksum},
	}
	requestUrl := fmt.Sprintf("%s%s", ps.Host, "account/credit")
	data, err := ps.Request(requestUrl, "POST", "JSON", rData, header)
	if err != nil {
		logger.Error("PaytmService_AddFunds_Request_Error | err=%v", err)
		return err
	}
	fmt.Println(string(data))
	logger.Debug("PaytmService_AddFunds_Request_Info | data=%+v | req=%+v", string(data), rData)
	res := &struct {
		StatusCode    string `json:"statusCode"`
		Status        string `json:"status"`
		StatusMessage string `json:"statusMessage"`
	}{}
	err = json.Unmarshal(data, res)
	if err != nil {
		logger.Error("PaytmService_AddFunds_JsonUnmarshal_Error | err=%v | res=%v", err, res)
		return err
	}
	fmt.Println(res)
	return nil
}

/**
 * 出金
 */
func (ps *PaytmService) PayOut(frozenId int64, args validator.PayOutValidator) (payOut *model.PayOut, err error) {
	transId := utils.GetOrderId("PTOUT")
	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("PaytmService_PayOut_Engine_Error | data=%+v | err=%v", payOut, err)
		return nil, err
	}
	payOut = new(model.PayOut)
	payOut.Uid = args.Uid
	payOut.AppId = args.AppId
	payOut.CountryCode = ps.COUNTRY_CODE
	payOut.PayChannel = model.PAYTM
	payOut.PayAccountId = ps.PAY_ACCOUNT_ID
	payOut.PayType = args.PayType
	payOut.AppOrderId = args.AppOrderId
	payOut.OrderId = transId
	payOut.PaymentId = transId
	payOut.FrozenId = frozenId
	payOut.Amount = args.Amount
	payOut.BankCard = args.BankCard
	payOut.UserId = args.UserId
	payOut.UserName = args.UserName
	payOut.Email = args.Email
	payOut.Paytm = args.PayTm
	payOut.IFSC = args.IFSC
	payOut.Phone = args.Phone
	_, err = engine.Insert(payOut)
	if err != nil {
		logger.Error("PaytmService_PayOut_Insert_Error | data=%+v | err=%v", payOut, err)
		return nil, err
	}
	var rData = make(map[string]string)
	if args.PayType == model.PT_BANK {
		rData = map[string]string{
			"orderId":            transId,
			"subwalletGuid":      ps.SUBWALLET_ID,
			"amount":             fmt.Sprint(args.Amount),
			"purpose":            "OTHERS",
			"date":               time.Now().Format("2006-01-02"),
			"callbackUrl":        ps.CB_URL,
			"beneficiaryAccount": args.BankCard,
			"beneficiaryIFSC":    args.IFSC,
			//"beneficiaryPhoneNo": args.Phone,
			//"beneficiaryVPA":     "",
		}
	} else {
		return nil, errors.New("PayType Not Support")
	}
	singData, _ := json.Marshal(rData)
	paytmChecksum := GenerateSignatureByString(string(singData), ps.SK)
	fmt.Println(paytmChecksum)
	header := map[string][]string{
		"Content-Type": {"application/json"},
		"x-mid":        {ps.MID},
		"x-checksum":   {paytmChecksum},
	}
	requestUrl := fmt.Sprintf("%s%s", ps.Host, "disburse/order/bank")
	data, err := ps.Request(requestUrl, "POST", "JSON", rData, header)
	if err != nil {
		logger.Error("PaytmService_Request_Error | err=%v", err)
		return nil, err
	}
	fmt.Println(string(data))
	logger.Debug("PaytmService_Request_Info | data=%+v | req=%+v", string(data), rData)
	res := &struct {
		StatusCode    string `json:"statusCode"`
		Status        string `json:"status"`
		StatusMessage string `json:"statusMessage"`
	}{}
	err = json.Unmarshal(data, res)
	if err != nil {
		logger.Error("PaytmService_PayOut_JsonUnmarshal_Error | err=%v | res=%v", err, res)
		return nil, err
	}
	var status model.PayOutStatus
	var thirdDesc string
	payOut.ThirdCode = res.Status
	if res.Status == "FAILURE" {
		status = model.PAY_OUT_FAILD
		thirdDesc = res.StatusMessage
		payOut.Status = status
	} else {
		status = model.PAY_OUT_ING
	}
	_, err = engine.Where("id=?", payOut.Id).Update(&model.PayOut{
		Status:     status,
		ThirdCode:  res.Status,
		ThirdDesc:  thirdDesc,
		FinishTime: time.Now().Unix(),
	})
	if err != nil {
		logger.Error("PaytmService_PayOut_Insert_Error | data=%+v | err=%v", payOut, err)
		return nil, err
	}
	if res.Status == "FAILURE" {
		err = errors.New(thirdDesc)
		logger.Error("PaytmService_PayOut_Response_Error | data=%+v", payOut)
		return payOut, err
	}
	return payOut, nil
}

func (ps *PaytmService) PayOutBatch(frozenId int64, args validator.PayOutValidator) (payOut *model.PayOut, err error) {
	return nil, fmt.Errorf("implement")
}

/*
*
统一请求
*/
func (ps *PaytmService) Request(url string, method string, contentType string, sendData interface{}, header map[string][]string) (res []byte, err error) {
	client := &http.Client{}
	var req *http.Request
	if contentType == "JSON" {
		data, _ := json.Marshal(sendData)
		reader := bytes.NewBuffer(data)
		req, err = http.NewRequest(method, url, reader)
	} else {
		s := sendData.(string)
		reader := strings.NewReader(s)
		req, err = http.NewRequest(method, url, reader)
	}
	//提交请求
	if err != nil {
		logger.Error("PaytmService_Request_Err | url=%v | err=%+v | req=%+v", url, err, sendData)
		return
	}
	req.Header = http.Header(header)
	//处理返回结果
	response, err := client.Do(req)
	if err != nil {
		logger.Error("PaytmService_Request_DoError | url=%v | err=%+v | req=%+v", url, err, sendData)
		return
	}
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		logger.Error("PaytmService_Request_ReadErr | url=%v | err=%+v | req=%+v", url, err, sendData)
		return
	}
	defer func() {
		_ = response.Body.Close()
	}()
	return body, nil
}
