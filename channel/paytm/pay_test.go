package paytm

import (
	"fmt"
	"funzone_pay/app/validator"
	"funzone_pay/boostrap"
	"funzone_pay/model"
	"testing"
	"time"
)

func TestCreateOrder(t *testing.T) {
	//str := "CURRENCY=INR&GATEWAYNAME=WALLET&RESPMSG=Txn+Success&BANKNAME=WALLET&PAYMENTMODE=PPI&MID=tvKODG10212030619225&RESPCODE=01&TXNID=20200830111212800110168665801861029&TXNAMOUNT=30.00&ORDERID=PTMrummy1598770749014726&STATUS=TXN_SUCCESS&BANKTXNID=63141980&TXNDATE=2020-08-30+12%3A29%3A11.0&CHECKSUMHASH=03B2rli2gOIAaqmOeYB782sk%2B63QXIjtp%2Bu2f7FXtsAmo78HJ0sRnJLaCymdgRUNw98ua0szW6gWs1y7W2QIYj8Oa11DpT1LCTnBJPS4hTs%3D"
	//boostrap.InitConf()
	//res := GetPayTm().CheckSign(str)
	//fmt.Println(res)
	//os.Exit(1)
	//data, _ := url.ParseQuery("CURRENCY=INR&GATEWAYNAME=WALLET&RESPMSG=Txn+Successful.&BANKNAME=WALLET&PAYMENTMODE=PPI&CUSTID=xxxxxxxxxx&MID=xxxxxxxxxxxxxxxxxxxx&MERC_UNQ_REF=LI_399&RESPCODE=01&TXNID=xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx&TXNAMOUNT=1.00&ORDERID=xxxxxxxxxxxxxxxxxx&STATUS=TXN_SUCCESS&BANKTXNID=xxxxxxx&TXNDATETIME=2019-05-14+16%3A48%3A36.0&TXNDATE=2019-05-14")
	//fmt.Println(data.Get("ORDERID"))
	//fmt.Println(data.Get("STATUS"))
	//return
	boostrap.InitConf()
	payEntry, err := GetPayTm().CreateOrder(validator.PayEntryValidator{
		Amount:     1000,
		PayChannel: model.PAYTM,
		Phone:      "9999999999",
	})
	fmt.Println(payEntry)
	fmt.Println(err)
}

func TestPaytmService_PayOut(t *testing.T) {
	boostrap.InitConf()
	paytm, err := GetPayTm().PayOut(validator.PayOutValidator{
		AppId:      "rummy",
		AppOrderId: fmt.Sprintf("%v%v", "test", time.Now().Unix()),
		PayType:    model.PT_BANK,
		PayChannel: model.PAYTM,
		Amount:     1,
		UserId:     "11",
		UserName:   "Thaneesh V",
		BankCard:   "1769155000016710",
		Phone:      "9999999999",
		Email:      "test@cashfree.com",
		IFSC:       "KVBL0001769",
		Address:    "ABC Street",
		PayTm:      "9999999999",
	})
	fmt.Println(paytm)
	fmt.Println(err)
}

func TestPaytmService_InquiryPayout(t *testing.T) {
	boostrap.InitConf()
	status, err := GetPayTm().InquiryPayout("PTOUT1597678570062517")
	fmt.Println(status)
	fmt.Println(err)
}

func TestPaytmService_AddFunds(t *testing.T) {
	boostrap.InitConf()
	err := GetPayTm().AddFunds(100000)
	fmt.Println(err)
}
