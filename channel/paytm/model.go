package paytm

type PaytmRequestData struct {
	CallbackUrl       string                     `json:"callbackUrl"`
	EnablePaymentMode []*EnablePaymentMode       `json:"enablePaymentMode"`
	RequestType       string                     `json:"requestType"`
	Mid               string                     `json:"mid"`
	WebsiteName       string                     `json:"websiteName"`
	OrderId           string                     `json:"orderId"`
	TxnAmount         *PaytmRequestDataTxnAmount `json:"txnAmount"`
	UserInfo          *PaytmRequestDataUserInfo  `json:"userInfo"`
}

type PaytmRequestDataTxnAmount struct {
	Value    string `json:"value"`
	Currency string `json:"currency"`
}

type PaytmRequestDataUserInfo struct {
	CustId    string `json:"custId"`
	Mobile    string `json:"mobile"`
	Email     string `json:"email"`
	FirstName string `json:"firstName"`
}

// PaytmOrderResponse is representing of paytm response structure
type PaytmOrderResponse struct {
	Body PaytmOrderResponseBody `json:"body"`
}

type PaytmOrderResponseBody struct {
	ResultInfo PORBResultInfo `json:"resultInfo"`
	TxnToken   string         `json:"txnToken"`
}

type PORBResultInfo struct {
	ResultCode string `json:"resultCode"`
	ResultMsg  string `json:"resultMsg"`
}

type TxnAmount struct {
	Value    string `json:"value"`
	Currency string `json:"currency"`
}

type UserInfo struct {
	CustId string `json:"custId"`
}

type EnablePaymentMode struct {
	Mode     string   `json:"mode"`
	Channels []string `json:"channels"`
}

type PaytmRequest struct {
	RequestType string    `json:"requestType"`
	Mid         string    `json:"mid"`
	OrderId     string    `json:"orderId"`
	WebsiteName string    `json:"websiteName"`
	TxnAmount   TxnAmount `json:"txnAmount"`
	UserInfo    UserInfo  `json:"userInfo"`
	CallbackUrl string    `json:"callbackUrl"`
}

/**
{
	"head": {
		"requestId": null,
		"responseTimestamp": "1596023582343",
		"version": "v2"
	},
	"body": {
		"resultInfo": {
			"resultStatus": "S",
			"resultCode": "0000",
			"resultMsg": "Success"
		}
	}
}
*/
type OrderInquiryResponse struct {
	Body OrderInquiryBody `json:"body"`
}

type OrderInquiryBody struct {
	ResultInfo ResultInfo `json:"resultInfo"`
}

type ResultInfo struct {
	ResultStatus string `json:"resultStatus"`
	ResultCode   string `json:"resultCode"`
	ResultMsg    string `json:"resultMsg"`
}
