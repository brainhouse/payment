package serpay

/**
{
   "success": true,
   "data": {
     "accessToken": "0512822dbcd8d2341d5e",
     "expireDate": 1601697258000
   },
   "code":null,
   "message":null
 }
*/
type TokenResponse struct {
	Data struct {
		AccessToken string `json:"accessToken"`
		ExpireDate  int64  `json:"expireDate"`
	} `json:"data"`
	Code    interface{} `json:"code"`
	Message interface{} `json:"message"`
	Success bool        `json:"success"`
}

/**
{
	"contentType": "01",
	"busContent": "https://pay.vengaladigital.co.in/payment/cashfreeVengala/payPage.webapp?formkey=20211012004557709638nnejrnh6j&payAmt=60&cy=INR&orderDesc=game&userName=ThaneeshV&userEmail=7845286022@cashfree.com&userPhone=7845286022&sign=a3W93+aP00wcc+9aSu54yAcVQGiCCzrQ9g+AAnyyYho=",
	"prdOrdNo": "20211012004557709638nnejrnh6j",
	"custOrderNo": "SPIN1633970756089234",
	"orgNo": "8211000787",
	"custId": "21100900002351",
	"code": "000000",
	"msg": "请求成功",
	"ordStatus": "04",
	"ordDesc": "处理中",
	"sign": "0EB65109EA44D6399C6FB89367E563B4"
}
*/
type OrderInResponse struct {
	ContentType string `json:"contentType"`
	BusContent  string `json:"busContent"`
	PrdOrdNo    string `json:"prdOrdNo"`
	CustOrderNo string `json:"custOrderNo"`
	OrgNo       string `json:"orgNo"`
	CustId      string `json:"custId"`
	Code        string `json:"code"`
	Msg         string `json:"msg"`
	OrdStatus   string `json:"ordStatus"`
	OrdDesc     string `json:"ordDesc"`
	Sign        string `json:"sign"`
}

type OrderOutResponse struct {
	CustOrdNo string `json:"custOrdNo"`
	CasOrdNo  string `json:"casOrdNo"`
	OrgNo     string `json:"orgNo"`
	CustId    string `json:"custId"`
	Code      string `json:"code"`
	Msg       string `json:"msg"`
	OrdStatus string `json:"ordStatus"`
	CasAmt    string `json:"casAmt"`
	CasTime   string `json:"casTime"`
	Sign      string `json:"sign"`
}
