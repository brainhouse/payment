package serpay

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"funzone_pay/app/validator"
	"funzone_pay/centerlog"
	"funzone_pay/dao"
	"funzone_pay/model"
	"funzone_pay/utils"
	"io/ioutil"
	"net/http"
	"net/url"
	"sort"
	"strings"
	"time"

	"github.com/wonderivan/logger"
)

type SerPayService struct {
	Host              string
	QueryHost         string
	APPID             string
	PAY_ACCOUNT_ID    string
	ORGNO             string
	CUSTID            string
	COUNTRY_CODE      int
	COUNTRY_STR       string
	CURRENCY          string
	SUB_ACCOUNT       string // 放款子账号
	CARD_TYPE         []string
	TRAN_TYPE         []string
	TEST_PREFIX       string
	PAY_CHANNEL       string
	APPSK             string
	RETRUN_URL        string
	COLLECT_HOST      string
	NOTIFY_URL        string
	PAYOUT_NOTIFY_URL string
	PAY_OUT_URL       string
}

const CountryCodeIndia = "IN"
const CountryCodeMX = "MX"
const CountryCodeBR = "BR"

func GetSerPayIns(conf map[string]interface{}) *SerPayService {
	cardType := make([]string, 0, 8)
	for _, item := range conf["CARD_TYPE"].([]interface{}) {
		cardType = append(cardType, item.(string))
	}

	tranType := make([]string, 0, 8)
	for _, item := range conf["TRAN_TYPE"].([]interface{}) {
		tranType = append(tranType, item.(string))
	}

	qhost := ""
	if v, ok := conf["QUERY_HOST"]; ok {
		qhost = v.(string)
	}
	return &SerPayService{
		PAY_ACCOUNT_ID:    conf["PAY_ACCOUNT_ID"].(string),
		PAY_CHANNEL:       conf["PAY_CHANNEL"].(string),
		Host:              conf["HOST"].(string),
		QueryHost:         qhost,
		APPID:             conf["APPID"].(string),
		APPSK:             conf["APPSK"].(string),
		ORGNO:             conf["ORGNO"].(string),
		COUNTRY_CODE:      conf["COUNTRY_CODE"].(int),
		COUNTRY_STR:       conf["COUNTRY_STR"].(string),
		CURRENCY:          conf["CURRENCY"].(string),
		SUB_ACCOUNT:       conf["SUB_ACCOUNT"].(string),
		CARD_TYPE:         cardType,
		TRAN_TYPE:         tranType,
		TEST_PREFIX:       conf["TEST_PREFIX"].(string),
		CUSTID:            conf["CUSTID"].(string),
		RETRUN_URL:        conf["RETURN_URL"].(string),
		NOTIFY_URL:        conf["NOTIFY_URL"].(string),
		PAYOUT_NOTIFY_URL: conf["PAYOUT_NOTIFY_URL"].(string),
		COLLECT_HOST:      conf["COLLECT_HOST"].(string),
		PAY_OUT_URL:       conf["PAY_OUT_URL"].(string),
	}
}

// 墨西哥
func (e *SerPayService) IsMX() bool {
	return e.COUNTRY_STR == CountryCodeMX
}

func (e *SerPayService) CreateOrder(args validator.PayEntryValidator) (*model.PayEntry, error) {
	orderId := utils.GetOrderId("SIN")
	if len(e.TEST_PREFIX) > 0 {
		orderId = e.TEST_PREFIX + orderId
	}
	//生成签名
	rData := fmt.Sprintf(`backUrl=%v&buyIp=13.200.29.120&clearType=01&countryCode=%v&currency=%v&custId=%v&custOrderNo=%v&frontUrl=%v&goodsName=game&orderDesc=game&orgNo=%v&payAmt=%d&tranType=%v&userEmail=%v&userName=%v&userPhone=%v&version=2.1`,
		e.NOTIFY_URL, e.COUNTRY_STR, e.CURRENCY, e.CUSTID, orderId, e.RETRUN_URL, e.ORGNO, int(args.Amount*100), e.TRAN_TYPE[0], args.Email, args.UserName, args.Phone)

	if e.IsMX() { // 墨西哥支付需要kyc，提供用户详细
		if args.UserDeviceId == "" || args.UserCitizenId == "" || args.City == "" || args.Street == "" || args.HouseNumber == "" {
			return nil, fmt.Errorf("invalid mx pay kyc")
		}

		rData = fmt.Sprintf(`backUrl=%v&buyIp=13.200.29.120&city=%v&clearType=01&countryCode=%v&currency=%v&custId=%v&custOrderNo=%v&frontUrl=%v&goodsName=game&houseNumber=%v&orderDesc=game&orgNo=%v&payAmt=%v&street=%v&tranType=%v&userCitizenId=%v&userDeviceId=%v&userEmail=%v&userName=%v&userPhone=%v&version=2.1`,
			e.NOTIFY_URL, "args.City", e.COUNTRY_STR, e.CURRENCY, e.CUSTID, orderId, e.RETRUN_URL, "args.HouseNumber", e.ORGNO, int64(args.Amount*100), "args.Street", e.TRAN_TYPE[0], "args.UserCitizenId", "args.UserDeviceId", args.Email, args.UserName, args.Phone)
	}

	sign := utils.GetMd5(fmt.Sprintf("%v&key=%v", rData, e.APPSK))

	rData = fmt.Sprintf("%v&sign=%v", rData, sign)
	requestUrl := fmt.Sprintf("%s%s", e.Host, "/cashier/pay.ac")
	header := map[string][]string{
		"Content-Type": {"application/x-www-form-urlencoded"},
	}

	data, err := e.Request(requestUrl, "POST", "", rData, header)
	logger.Debug("SerPayService_CreateOrder_Info | %v | %v | orderReq=%v | err=%v | data=%v", args.AppId, requestUrl, rData, err, string(data))

	egResponse := &OrderInResponse{}
	err = json.Unmarshal(data, egResponse)
	if err != nil {
		logger.Error("SerPayService_CreateOrder_JsonUnmarshal_Err | err=%v | data=%v", err, string(data))
		centerlog.OrderError(centerlog.MsgThirdPlatformFail, e.PAY_ACCOUNT_ID, args.AppId, args.AppOrderId, orderId, "false", err.Error())
		return nil, err
	}
	//请求成功写入流水
	pE := new(model.PayEntry)
	pE.AppId = args.AppId
	pE.Uid = args.Uid
	pE.PayAccountId = e.PAY_ACCOUNT_ID
	pE.PayChannel = model.SERPAY
	pE.AppOrderId = args.AppOrderId
	pE.CountryCode = e.COUNTRY_CODE
	pE.OrderId = orderId
	pE.PaymentOrderId = egResponse.BusContent
	pE.Amount = args.Amount
	pE.UserId = args.UserId
	pE.UserName = args.UserName
	pE.Email = args.Email
	pE.Phone = args.Phone
	pE.ThirdCode = fmt.Sprint(egResponse.Code)
	pE.Status = model.PAYING
	pE.PayAppId = e.APPID
	pE.FinishTime = time.Now().Unix()

	if egResponse.Code != "000000" {
		pE.ThirdDesc = fmt.Sprint(egResponse.Msg)
		centerlog.OrderError(centerlog.MsgThirdPlatformFail, e.PAY_ACCOUNT_ID, pE.AppId, pE.AppOrderId, pE.OrderId, egResponse.Code, egResponse.Msg)
	}
	engine, err := dao.GetMysql()
	if engine == nil || err != nil {
		logger.Error("SerPayService_CreateOrder_GetDB_Err | err=%v", err)
		return nil, err
	}
	_, err = engine.Insert(pE)
	if err != nil {
		logger.Error("SerPayService_CreateOrder_InsertDB_Err | err=%v", err)
		return nil, err
	}
	logger.Debug("SerPayService_CreateOrder_Request | req=%v | res=%v", rData, egResponse)
	if egResponse.Code != "000000" {
		err := errors.New(fmt.Sprint(egResponse.Msg))
		return nil, err
	}
	pE.PaymentLinkHost = egResponse.BusContent
	return pE, err
}

func (e *SerPayService) Inquiry(orderID string, s2 string) (*model.PayEntry, error) {
	requestUrl := fmt.Sprintf("%s%s", e.QueryHost, "/cashier/query.ac")
	header := map[string][]string{
		"Content-Type": {"application/x-www-form-urlencoded"},
	}
	params := url.Values{}
	params.Add("version", "2.1")
	params.Add("orgNo", e.ORGNO)
	params.Add("custId", e.CUSTID)
	params.Add("custOrderNo", orderID)
	sign := e.GetSign(params)
	params.Set("sign", sign)
	reqData, _ := url.QueryUnescape(params.Encode())

	data, err := e.Request(requestUrl, "POST", "", reqData, header)
	logger.Debug("SerPayService_InquiryPayIn_Info | req=%v | response=%v | err=%v", reqData, string(data), err)
	if err != nil {
		logger.Error("SerPayService_InquiryPayIn_Request_Error | err=%v", err)
		return nil, err
	}
	resData := &struct {
		Code        string `json:"code"`
		CustOrderNo string `json:"custOrderNo"`
		OrdStatus   string `json:"ordStatus"`
		OrdDesc     string `json:"ordDesc"`
		Msg         string `json:"msg"`
		OrdAmt      string `json:"ordAmt"`
		PayAmt      string `json:"payAmt"`
	}{}
	err = json.Unmarshal(data, resData)
	if err != nil {
		return nil, err
	}
	engine, _ := dao.GetMysql()
	payin := new(model.PayEntry)
	exist, err := engine.Where("order_id=?", orderID).Get(payin)
	if err != nil {
		logger.Error("SerPayService_PayIn_Inquiry_GetError | err=%v | data=%v", err, orderID)
		return nil, err
	}
	if !exist {
		return nil, errors.New("NotExist")
	}

	if payin.Status != model.PAYING {
		return nil, fmt.Errorf("order completed-[%v-%v-%v-%v]", payin.Status, resData.Code, resData.OrdStatus, resData.OrdDesc)
	}

	// normal
	if resData.Code == "000000" {
		if resData.OrdStatus == "01" { // success
			payin.Status = model.PAYSUCCESS
		} else if resData.OrdStatus == "00" || resData.OrdStatus == "04" { // 00-未交易, 04-处理中

		} else { // 02-失败，03-被拒绝，05-取消支付，06-未支付，07-已经退款，08-退款中
			payin.Status = model.PAYFAILD
			payin.ThirdCode = resData.Code
			payin.ThirdDesc = fmt.Sprintf("%v-%v-%v", resData.OrdStatus, resData.OrdDesc, resData.Msg)
		}
	} else {
		if resData.OrdStatus == "" {
			payin.Status = model.PAYFAILD
			payin.ThirdCode = resData.Code
			payin.ThirdDesc = fmt.Sprintf("%v-%v", resData.OrdDesc, resData.Msg)
		}
	}

	return payin, nil
}

func (e *SerPayService) InquiryPayout(orderId string, ext string) (*model.PayOut, error) {
	requestUrl := fmt.Sprintf("%s%s", e.QueryHost, "/cashier/TX0002.ac")
	header := map[string][]string{
		"Content-Type": {"application/x-www-form-urlencoded"},
	}
	params := url.Values{}
	params.Add("version", "2.1")
	params.Add("orgNo", e.ORGNO)
	params.Add("custId", e.CUSTID)
	params.Add("custOrdNo", orderId)
	sign := e.GetSign(params)
	params.Set("sign", sign)
	reqData, _ := url.QueryUnescape(params.Encode())

	data, err := e.Request(requestUrl, "POST", "", reqData, header)
	logger.Debug("SerPayService_InquiryPayout_Info | req=%v | response=%v | err=%v", reqData, string(data), err)
	if err != nil {
		logger.Error("SerPayService_InquiryPayout_Request_Error | err=%v", err)
		return nil, err
	}
	resData := &struct {
		Code      string `json:"code"`
		OrdStatus string `json:"ordStatus"`
		OrdDesc   string `json:"ordDesc"`
	}{}
	err = json.Unmarshal(data, resData)
	if err != nil {
		return nil, err
	}
	engine, _ := dao.GetMysql()
	payOut := new(model.PayOut)
	exist, err := engine.Where("order_id=?", orderId).Get(payOut)
	if err != nil {
		logger.Error("SerPayService_PayOut_Inquiry_GetError | err=%v | data=%v", err, orderId)
		return nil, err
	}
	if !exist {
		return nil, errors.New("NotExist")
	}
	if resData.Code == "000000" && resData.OrdStatus == "07" {
		payOut.Status = model.PAY_OUT_SUCCESS
	} else if resData.OrdStatus == "08" || resData.OrdStatus == "09" || (resData.Code == "900003" && resData.OrdStatus == "") {
		payOut.Status = model.PAY_OUT_FAILD
		payOut.ThirdCode = resData.OrdStatus
		payOut.ThirdDesc = fmt.Sprintf("%v-%v-%v", resData.OrdStatus, resData.Code, resData.OrdDesc)
	}
	return payOut, nil
}

func (e *SerPayService) PayOut(frozenId int64, outValidator validator.PayOutValidator) (payOut *model.PayOut, err error) {
	params := url.Values{}
	transId := utils.GetOrderId("SOUT")
	payOut = new(model.PayOut)
	payOut.Uid = outValidator.Uid
	payOut.AppId = outValidator.AppId
	payOut.PayAccountId = e.PAY_ACCOUNT_ID
	payOut.PayChannel = model.SERPAY
	payOut.CountryCode = e.COUNTRY_CODE
	payOut.PayType = outValidator.PayType
	payOut.AppOrderId = outValidator.AppOrderId
	payOut.OrderId = transId
	payOut.PaymentId = transId
	payOut.FrozenId = frozenId
	payOut.Amount = outValidator.Amount
	payOut.BankCard = outValidator.BankCard
	payOut.UserId = outValidator.UserId
	payOut.UserName = outValidator.UserName
	payOut.Email = outValidator.Email
	payOut.Paytm = outValidator.PayTm
	payOut.IFSC = outValidator.IFSC
	payOut.Phone = outValidator.Phone
	engine, err := dao.GetMysql()
	if engine == nil || err != nil {
		logger.Error("SerPayService_PayOut_GetDB_Err | err=%v", err)
		return nil, err
	}
	_, err = engine.Insert(payOut)
	if err != nil {
		logger.Error("SerPayService_PayOut_Insert_Error | data=%+v | err=%v", payOut, err)
		return nil, err
	}
	//发起支付
	requestUrl := fmt.Sprintf("%s%s", e.PAY_OUT_URL, "/cashier/TX0001.ac")
	header := map[string][]string{
		"Content-Type": {"application/x-www-form-urlencoded"},
	}
	params.Add("version", "2.1")
	params.Add("orgNo", e.ORGNO)
	params.Add("custId", e.CUSTID)
	params.Add("custOrdNo", transId)
	params.Add("casType", "00")
	params.Add("country", e.COUNTRY_STR)
	params.Add("currency", e.CURRENCY)
	params.Add("casAmt", fmt.Sprint(int64(outValidator.Amount*100)))
	params.Add("account", e.SUB_ACCOUNT)
	params.Add("deductWay", "02")
	params.Add("callBackUrl", e.PAYOUT_NOTIFY_URL)
	params.Add("accountName", outValidator.UserName)
	params.Add("phone", outValidator.Phone)
	params.Add("email", outValidator.Email)
	//transferMode := string(outValidator.PayType)
	if outValidator.PayType == model.PT_BANK {
		params.Add("payoutType", "Card") //做下转换 banktransfer, upi, paytm, amazonpay and card.
		switch e.COUNTRY_STR {
		case CountryCodeIndia:
			params.Add("payeeBankCode", outValidator.IFSC[:4])
			params.Add("cnapsCode", outValidator.IFSC)

		case CountryCodeMX, CountryCodeBR:
			if outValidator.BankCode == "" {
				return nil, fmt.Errorf("[%v-%v] bank code empty", outValidator.Uid, outValidator.UserId)
			}
			params.Add("payeeBankCode", outValidator.BankCode)
			params.Add("cnapsCode", "IFSC")
		}

		if len(e.CARD_TYPE) > 0 {
			params.Add("cardType", e.CARD_TYPE[0])
		}

		params.Add("cardNo", outValidator.BankCard)
	} else if outValidator.PayType == model.PT_UPI {
		params.Add("payoutType", "UPI")
		params.Add("upiId", outValidator.VPA)
	} else if outValidator.PayType == model.PT_CLABE {
		params.Add("payoutType", "Clabe")
		params.Add("payeeBankCode", outValidator.BankCode)
		params.Add("cardNo", outValidator.BankCard)
	} else if outValidator.PayType == model.PT_PIX {
		params.Add("payoutType", "PIX")
		params.Add("cardType", string(outValidator.CardType))
		params.Add("walletId", outValidator.BankCard)
	}
	sign := e.GetSign(params)
	params.Set("sign", sign)
	reqData := params.Encode()
	data, err := e.Request(requestUrl, "POST", "", reqData, header)
	if err != nil {
		logger.Error("SerPayService_PayOut_Request_Error | err=%v", err)
		return nil, err
	}

	logger.Debug("SerPayService_PayOut_Info | requestUrl=%v | requestData=%v | response=%v", requestUrl, reqData, string(data))
	/**
	{
	 "status": "SUCCESS",
	 "subCode": "200",
	 "message": "Transfer completed successfully",
	 "data": {
	   "referenceId": "10023",
	   "utr": "P16111765023806",
	   "acknowledged": 1
	 }
	}
	*/
	res := &OrderOutResponse{}
	err = json.Unmarshal(data, res)
	if err != nil {
		logger.Error("SerPayService_PayOut_JsonUnmarshal_Error | err=%v | res=%v", err, res)
		centerlog.OrderError(centerlog.MsgThirdPlatformFail, e.PAY_ACCOUNT_ID, payOut.AppId, payOut.AppOrderId, payOut.OrderId, "false", err.Error())
		return nil, err
	}
	var (
		status      model.PayOutStatus
		thirdDesc   string
		referenceId string
	)
	payOut.ThirdCode = res.Code
	if res.OrdStatus == "08" {
		status = model.PAY_OUT_FAILD
		thirdDesc = res.Msg
		payOut.Status = model.PAY_OUT_FAILD
	} else if res.Code != "000000" && res.OrdStatus == "" {
		status = model.PAY_OUT_FAILD
		thirdDesc = res.Msg
		payOut.Status = status
	} else {
		status = model.PAY_OUT_ING
		referenceId = res.CasOrdNo
	}
	if status == model.PAY_OUT_FAILD {
		centerlog.OrderError(centerlog.MsgThirdPlatformFail, e.PAY_ACCOUNT_ID, payOut.AppId, payOut.AppOrderId, payOut.OrderId, res.OrdStatus, res.Msg)
	}
	_, err = engine.Where("id=?", payOut.Id).Update(&model.PayOut{
		Status:     status,
		ThirdCode:  payOut.ThirdCode,
		PaymentId:  referenceId,
		ThirdDesc:  thirdDesc,
		FinishTime: time.Now().Unix(),
	})
	if err != nil {
		logger.Error("SerPayService_PayOut_Insert_Error | data=%v | err=%v", payOut, err)
		return nil, err
	}
	return payOut, nil
}

func (e *SerPayService) PayOutBatch(frozenId int64, outValidator validator.PayOutValidator) (*model.PayOut, error) {
	return e.PayOut(frozenId, outValidator)
}

/*
*
//orgNo=8211000787
//custId=21100900002351
//custOrderNo=SPIN1634047113052607
//prdOrdNo=20211012215835252172KqiVtchV7
//ordStatus=01
//ordAmt=6000
//payAmt=6000
//ordTime=20211012215835
//version=2.1
//sign=3631F5377AC387BC2AF0D41F05E2E3BE
*/
func (e *SerPayService) InSignature(cb interface{}) bool {
	ecb := cb.(*model.SerInCallback)
	params, _ := url.ParseQuery(ecb.OrgStr)
	sign := e.GetSign(params)
	if strings.ToUpper(sign) != ecb.Sign {
		logger.Error("InSignature | url=%v | uper=%+v | sign=%+v", ecb.OrgStr, strings.ToUpper(sign), ecb.Sign)
		return false
	}
	return true
}

func (e *SerPayService) GetSign(params url.Values) string {
	var sortSlice []string
	for k, _ := range params {
		if k == "sign" {
			continue
		}
		sortSlice = append(sortSlice, k)
	}
	sort.Strings(sortSlice)
	signs := ""
	for _, v := range sortSlice {
		if params[v][0] == "" {
			continue
		}
		signs = fmt.Sprintf("%v%v=%v&", signs, v, params[v][0])
	}
	signs += "key=" + e.APPSK
	return utils.GetMd5(signs)
}

func (e *SerPayService) OutSignature(cb interface{}) bool {
	return true
	ecb := cb.(*model.SerOutCallback)
	params, _ := url.ParseQuery(ecb.OrgStr)
	sign := e.GetSign(params)
	fmt.Println(sign)
	if strings.ToUpper(sign) != ecb.Sign {
		return false
	}
	return true
}

/*
*
统一请求
*/
func (e *SerPayService) Request(url string, method string, contentType string, sendData interface{}, header map[string][]string) (res []byte, err error) {
	client := &http.Client{}
	var req *http.Request
	if contentType == "JSON" {
		data, _ := json.Marshal(sendData)
		reader := bytes.NewBuffer(data)
		req, err = http.NewRequest(method, url, reader)
	} else {
		s := sendData.(string)
		reader := strings.NewReader(s)
		req, err = http.NewRequest(method, url, reader)
	}
	//提交请求
	if err != nil {
		logger.Error("EaglePayService_Create_order | url=%v | err=%+v | req=%+v", url, err, sendData)
		return
	}
	req.Header = http.Header(header)
	//处理返回结果
	response, err := client.Do(req)
	if err != nil {
		logger.Error("EaglePayService_request_err | url=%v | err=%+v | req=%+v", url, err, sendData)
		return
	}
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		logger.Error("EaglePayService_response_read_err | url=%v | err=%+v | req=%+v", url, err, sendData)
		return
	}
	defer func() {
		_ = response.Body.Close()
	}()
	return body, nil
}
