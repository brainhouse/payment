package fidypay

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"funzone_pay/app/validator"
	"funzone_pay/dao"
	"funzone_pay/model"
	"funzone_pay/utils"
	"github.com/wonderivan/logger"
	"io/ioutil"
	"net/http"
	"strings"
	"time"
)

type FidyPayService struct {
	HOST           string
	APPID          string
	PAY_ACCOUNT_ID string
	PAY_CHANNEL    string
	COUNTRY_CODE   int
	SK             string
	AUTH_USERNAME  string
	AUTH_PWD       string
	RETRUN_URL     string
	COLLECT_HOST   string
}

func GetFidyPayIns(conf map[string]interface{}) *FidyPayService {
	return &FidyPayService{
		HOST:           conf["HOST"].(string),
		APPID:          conf["APPID"].(string),
		PAY_ACCOUNT_ID: conf["PAY_ACCOUNT_ID"].(string),
		COUNTRY_CODE:   conf["COUNTRY_CODE"].(int),
		SK:             conf["APPSK"].(string),
		AUTH_USERNAME:  conf["AUTH_USERNAME"].(string),
		AUTH_PWD:       conf["AUTH_PWD"].(string),
		COLLECT_HOST:   conf["COLLECT_HOST"].(string),
	}
}

func (u *FidyPayService) CreateOrder(args validator.PayEntryValidator) (*model.PayEntry, error) {
	orderId := utils.GetOrderId("FDIN")
	//请求成功写入流水
	pE := new(model.PayEntry)
	pE.AppId = args.AppId
	pE.Uid = args.Uid
	pE.PayAccountId = u.PAY_ACCOUNT_ID
	pE.PayChannel = model.FIDYPAY
	pE.CountryCode = u.COUNTRY_CODE
	pE.AppOrderId = args.AppOrderId
	pE.OrderId = orderId
	pE.Amount = args.Amount
	pE.UserId = args.UserId
	pE.UserName = args.UserName
	pE.Email = args.Email
	pE.Phone = args.Phone
	pE.Status = model.PAYING
	pE.PayAppId = u.APPID
	pE.FinishTime = time.Now().Unix()
	engine, err := dao.GetMysql()
	if engine == nil || err != nil {
		logger.Error("FidyPayService_CreateOrder_GetDB_Err | err=%v", err)
		return nil, err
	}
	_, err = engine.Insert(pE)
	if err != nil {
		logger.Error("FidyPayService_CreateOrder_InsertDB_Err | err=%v", err)
		return nil, err
	}
	pE.PaymentLinkHost = u.COLLECT_HOST
	return pE, err
}

func (u *FidyPayService) Inquiry(orderId string, s2 string) (*model.PayEntry, error) {
	conf := map[string]interface{}{
		"HOST":          u.HOST,
		"APPID":         u.APPID,
		"SK":            u.SK,
		"AUTH_USERNAME": u.AUTH_USERNAME,
		"AUTH_PWD":      u.AUTH_PWD,
	}
	//通过UPI实例去查询，将当前账号配置初始化进去
	upiIns := GetFidyUPIPayIns(conf)
	tmpPayEntry := upiIns.GetPayStatus(orderId)
	if tmpPayEntry == nil {
		logger.Error("FidyPayService_Inquiry_GetPayStatusNil | orderId=%v", orderId)
		return nil, errors.New("QueryNil")
	}
	return tmpPayEntry, nil

}

func (u *FidyPayService) InquiryPayout(orderId string, ext string) (*model.PayOut, error) {
	panic("implement me")
}

func (u *FidyPayService) PayOut(frozenId int64, outValidator validator.PayOutValidator) (payOut *model.PayOut, err error) {
	if outValidator.PayType != model.PT_BANK {
		return &model.PayOut{Status: model.PAY_OUT_FAILD}, errors.New("not support")
	}
	transId := utils.GetOrderId("FDOUT")
	payOut = new(model.PayOut)
	payOut.Uid = outValidator.Uid
	payOut.AppId = outValidator.AppId
	payOut.PayAccountId = u.PAY_ACCOUNT_ID
	payOut.PayChannel = model.FIDYPAY
	payOut.CountryCode = u.COUNTRY_CODE
	payOut.PayType = outValidator.PayType
	payOut.AppOrderId = outValidator.AppOrderId
	payOut.OrderId = transId
	payOut.PaymentId = transId
	payOut.FrozenId = frozenId
	payOut.Amount = outValidator.Amount
	payOut.BankCard = outValidator.BankCard
	payOut.UserId = outValidator.UserId
	payOut.UserName = outValidator.UserName
	payOut.Email = outValidator.Email
	payOut.Paytm = outValidator.PayTm
	payOut.IFSC = outValidator.IFSC
	payOut.Phone = outValidator.Phone
	engine, err := dao.GetMysql()
	if engine == nil || err != nil {
		logger.Error("FidyPayService_PayOut_GetDB_Err | err=%v", err)
		return nil, err
	}
	_, err = engine.Insert(payOut)
	if err != nil {
		logger.Error("FidyPayService_PayOut_Insert_Error | data=%+v | err=%v", payOut, err)
		return nil, err
	}
	//发起支付
	requestUrl := fmt.Sprintf("%s%s", u.HOST, "/payout/domesticPayment")
	header := map[string][]string{
		"Content-Type":  {"application/json"},
		"Client-Id":     {u.APPID},
		"Client-Secret": {u.SK},
	}
	reqData := &PayOutRequest{
		Amount:              fmt.Sprint(outValidator.Amount),
		BeneficiaryName:     outValidator.UserName,
		BeneficiaryAccNo:    outValidator.BankCard,
		BeneficiaryIfscCode: outValidator.IFSC,
		EmailAddress:        outValidator.Email,
		TransferType:        "IMPS",
		MobileNumber:        outValidator.Phone,
		TrxnNote:            "pay",
		Address:             outValidator.Address,
		MerchantTrxnRefId:   transId,
		Optional:            "",
	}
	data, err := u.Request(requestUrl, "POST", "JSON", reqData, header)
	if err != nil {
		logger.Error("FidyPayService_PayOut_Request_Error | err=%v", err)
		return nil, err
	}
	fmt.Println(string(data))
	logger.Debug("FidyPayService_PayOut_Info | requestUrl=%v | requestData=%v | response=%v", requestUrl, reqData, string(data))

	res := &PayOutResponse{}
	err = json.Unmarshal(data, res)
	if err != nil {
		logger.Error("FidyPayService_PayOut_JsonUnmarshal_Error | err=%v | res=%v", err, res)
		return nil, err
	}
	var (
		status      model.PayOutStatus
		thirdDesc   string
		referenceId string
	)
	payOut.ThirdCode = res.Status
	if res.Status != "Received" {
		//先不设置成失败
		//status = model.PAY_OUT_FAILD
		thirdDesc = res.Status
	} else {
		status = model.PAY_OUT_ING
		referenceId = res.TrxnId
	}
	_, err = engine.Where("id=?", payOut.Id).Update(&model.PayOut{
		Status:    status,
		ThirdCode: payOut.ThirdCode,
		PaymentId: referenceId,
		ThirdDesc: thirdDesc,
	})
	if err != nil {
		logger.Error("FidyPayService_PayOut_Insert_Error | data=%v | err=%v", payOut, err)
		return nil, err
	}
	return payOut, nil
}

func (u *FidyPayService) PayOutBatch(frozenId int64, entryValidator validator.PayOutValidator) (*model.PayOut, error) {
	return nil, fmt.Errorf("unimplement")
}

func (u *FidyPayService) InSignature(cb interface{}) bool {
	return false
}

func (u *FidyPayService) OutSignature(cb interface{}) bool {
	return false
}

func (u *FidyPayService) Request(url string, method string, contentType string, sendData interface{}, header map[string][]string) (res []byte, err error) {
	client := &http.Client{}
	var req *http.Request
	if contentType == "JSON" {
		data, _ := json.Marshal(sendData)
		reader := bytes.NewBuffer(data)
		req, err = http.NewRequest(method, url, reader)
	} else {
		s := sendData.(string)
		reader := strings.NewReader(s)
		req, err = http.NewRequest(method, url, reader)
	}
	//提交请求
	if err != nil {
		logger.Error("FidyPayService_Create_order | url=%v | err=%+v | req=%+v", url, err, sendData)
		return
	}
	req.Header = http.Header(header)
	req.SetBasicAuth(u.AUTH_USERNAME, u.AUTH_PWD)
	//处理返回结果
	response, err := client.Do(req)
	if err != nil {
		logger.Error("FidyPayService_request_err | url=%v | err=%+v | req=%+v", url, err, sendData)
		return
	}
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		logger.Error("FidyPayService_response_read_err | url=%v | err=%+v | req=%+v", url, err, sendData)
		return
	}
	defer func() {
		_ = response.Body.Close()
	}()
	return body, nil
}
