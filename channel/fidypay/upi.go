package fidypay

import (
	"bytes"
	"encoding/json"
	"fmt"
	"funzone_pay/dao"
	"funzone_pay/model"
	"github.com/wonderivan/logger"
	"io/ioutil"
	"net/http"
	"strings"
)

type UPIService struct {
	APPID         string
	HOST          string
	SK            string
	AUTH_USERNAME string
	AUTH_PWD      string
}

func GetFidyUPIPayIns(conf map[string]interface{}) *UPIService {
	return &UPIService{
		HOST:          conf["HOST"].(string),
		APPID:         conf["APPID"].(string),
		SK:            conf["APPSK"].(string),
		AUTH_USERNAME: conf["AUTH_USERNAME"].(string),
		AUTH_PWD:      conf["AUTH_PWD"].(string),
	}
}

func (u *UPIService) DoPay(orderId string, amount float64, vpa string) error {
	/**
	{
	             "amount": "1",
	             "trxnNote": "Testing",
	             "VPA": "00000000000@upi",
	             "merchantTrxnRefId":"12131231231232"
	             "optional": ""
	}
	*/
	requestUrl := fmt.Sprintf("%v/payin/collect", u.HOST)
	header := map[string][]string{
		"Content-Type":  {"application/json"},
		"Client-Id":     {u.APPID},
		"Client-Secret": {u.SK},
	}
	payReq := struct {
		Amount            string `json:"amount"`
		TrxnNote          string `json:"trxnNote"`
		VPA               string `json:"vpa"`
		MerchantTrxnRefId string `json:"merchantTrxnRefId"`
		Optional          string `json:"optional"`
	}{
		Amount:            fmt.Sprint(amount),
		TrxnNote:          "charge",
		VPA:               vpa,
		MerchantTrxnRefId: orderId,
		Optional:          "",
	}
	data, err := u.Request(requestUrl, "POST", "JSON", payReq, header)
	logger.Debug("FidyPayUPIService_DoPay_Info | vpa=%v | res=%v | err=%v", vpa, string(data), err)
	/**
	{
	  "bankTrxnId": "1649779057",
	  "trxnNote": "Testing",
	  "amount": "1.00",
	  "code": "0x0200",
	  "originalOrderId": "FIDYPYUPI000000000000000000000",
	  "payorVPA": "00000000000@yesbank",
	  "description": "Transaction Collect request initiated successfully",
	  "PayeeVPA": "000000000@upi",
	  "customerRefId": "115513583556",
	  "uPITrxnId": "YESBC0EC0EAAEF000CF7E00000000FF0E00",
	  "status": "SUCCESS",
	  "trxn_id": "FPA1622792422198"
	}
	*/
	resData := new(UpiPayResponse)
	err = json.Unmarshal(data, resData)
	if err != nil {
		logger.Error("FidyPayUPIService_DoPay_DataJsonErr | vpa=%v | res=%v | err=%v", vpa, string(data), err)
		return nil
	}
	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("FidyPayUPIService_DoPay_GetDBError | vpa=%v | res=%v | err=%v", vpa, string(data), err)
		return nil
	}
	//更新trxid
	_, err = engine.Where("order_id=?", orderId).Update(&model.PayEntry{
		PaymentOrderId: resData.TrxnId,
	})
	return nil
}

func (u *UPIService) VerifyVPA(vpa string) *model.UPIInfo {
	requestUrl := fmt.Sprintf("%v/payin/checkVirtualAddress/%v", u.HOST, vpa)
	header := map[string][]string{
		"Content-Type":  {"application/json"},
		"Client-Id":     {u.APPID},
		"Client-Secret": {u.SK},
	}
	data, err := u.Request(requestUrl, "POST", "JSON", "", header)
	logger.Debug("FidyPayUPIService_VerifyVPA_Info | vpa=%v | res=%v | err=%v", vpa, string(data), err)
	/**
	{
	  "bankTrxnId": "3507836503",
	  "statusDescription": "Virtual Address exist for Transaction",
	  "code": "0x0200",
	  "maskName": "Account Holder Name",
	  "virtualAddress": "0000000000000@upi",
	  "status": "VE"
	}
	*/
	resData := &struct {
		MaskName string `json:"maskName"`
		Status   string `json:"status"`
	}{}
	err = json.Unmarshal(data, resData)
	if err != nil {
		logger.Error("FidyPayUPIService_VerifyVPA_DataJsonErr | vpa=%v | res=%v | err=%v", vpa, string(data), err)
		return nil
	}
	info := new(model.UPIInfo)
	if resData.Status == "VE" {
		info.Status = true
		info.UserName = resData.MaskName
	}
	return info
}

func (u *UPIService) GetPayStatus(orderId string) *model.PayEntry {
	requestUrl := fmt.Sprintf("%v/payin/status/%v", u.HOST, orderId)
	header := map[string][]string{
		"Content-Type":  {"application/json"},
		"Client-Id":     {u.APPID},
		"Client-Secret": {u.SK},
	}
	data, err := u.Request(requestUrl, "POST", "JSON", "", header)
	logger.Debug("FidyPayUPIService_GetPayStatus_Info | orderId=%v | res=%v | err=%v", orderId, string(data), err)
	/**
	{
		"trxn_id": "FPA1622785699059",
		"approvalNo": "UNI000",
		"trxnNote": "Testing",
		"amount": "1.0",
		"code": "0x0200",
		"payerIFSCCode": "BANK0000000",
		"description": "Transaction success",
		"nPCITrxnId": "YESB00000000000000000000000000000000",
		"customerRefId": "115513583556",
		"bankTrxnId": "3511567158",
		"payerAccNo": "XXXXXX3088",
		"originalOrderId": "FIDYPYUPI00000000000000000000",
		"trxnAuthDate": "2021:06:04 13:16:10",
		"payorVPA": "00000000000@upi",
		"payerName": "Account Holder Name",
		"status": "SUCCESS"
	}
	*/
	resData := &struct {
		TrxnId     string `json:"trxn_id"`
		ApprovalNo string `json:"approvalNo"`
		Status     string `json:"status"`
	}{}
	err = json.Unmarshal(data, resData)
	if err != nil {
		logger.Error("FidyPayUPIService_GetPayStatus_DataJsonErr | orderId=%v | res=%v | err=%v", orderId, string(data), err)
		return nil
	}
	payEntry := &model.PayEntry{}
	payEntry.OrderId = orderId
	if resData.Status == "SUCCESS" {
		payEntry.Status = model.PAYSUCCESS
	}
	return payEntry
}

func (u *UPIService) Request(url string, method string, contentType string, sendData interface{}, header map[string][]string) (res []byte, err error) {
	client := &http.Client{}
	var req *http.Request
	if contentType == "JSON" {
		data, _ := json.Marshal(sendData)
		reader := bytes.NewBuffer(data)
		req, err = http.NewRequest(method, url, reader)
	} else {
		s := sendData.(string)
		reader := strings.NewReader(s)
		req, err = http.NewRequest(method, url, reader)
	}
	//提交请求
	if err != nil {
		logger.Error("FidyUPIService_Create_order | url=%v | err=%+v | req=%+v", url, err, sendData)
		return
	}
	req.Header = http.Header(header)
	req.SetBasicAuth(u.AUTH_USERNAME, u.AUTH_PWD)
	//处理返回结果
	response, err := client.Do(req)
	if err != nil {
		logger.Error("FidyUPIService_request_err | url=%v | err=%+v | req=%+v", url, err, sendData)
		return
	}
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		logger.Error("FidyUPIService_response_read_err | url=%v | err=%+v | req=%+v", url, err, sendData)
		return
	}
	defer func() {
		_ = response.Body.Close()
	}()
	return body, nil
}
