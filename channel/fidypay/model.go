package fidypay

/**
{
	  "bankTrxnId": "1649779057",
	  "trxnNote": "Testing",
	  "amount": "1.00",
	  "code": "0x0200",
	  "originalOrderId": "FIDYPYUPI000000000000000000000",
	  "payorVPA": "00000000000@yesbank",
	  "description": "Transaction Collect request initiated successfully",
	  "PayeeVPA": "000000000@upi",
	  "customerRefId": "115513583556",
	  "uPITrxnId": "YESBC0EC0EAAEF000CF7E00000000FF0E00",
	  "status": "SUCCESS",
	  "trxn_id": "FPA1622792422198"
	}
*/
type UpiPayResponse struct {
	BankTrxnId      string `json:"bankTrxnId"`
	TrxnNote        string `json:"trxnNote"`
	Amount          string `json:"amount"`
	Code            string `json:"code"`
	OriginalOrderId string `json:"originalOrderId"`
	PayorVPA        string `json:"payorVPA"`
	Description     string `json:"description"`
	CustomerRefId   string `json:"customerRefId"`
	UPITrxnId       string `json:"uPITrxnId"`
	Status          string `json:"status"`
	TrxnId          string `json:"trxn_id"`
}

/**
{
	"amount": "1",
	"beneficiaryName": "Arpan Sethiya",
	"beneficiaryAccNo": "000890289871772",
	"beneficiaryIfscCode": "SCBL0036078",
	"emailAddress": "arpan.sethiya@fidypay.com",
	"transferType": "IMPS",
	"mobileNumber": "7869920537",
	"trxnNote": "testing",
    "address": "Indore",
    "merchantTrxnRefId":"21312312312312",
	"optional": ""
}
*/
type PayOutRequest struct {
	Amount              string `json:"amount"`
	BeneficiaryName     string `json:"beneficiaryName"`
	BeneficiaryAccNo    string `json:"beneficiaryAccNo"`
	BeneficiaryIfscCode string `json:"beneficiaryIfscCode"`
	EmailAddress        string `json:"emailAddress"`
	TransferType        string `json:"transferType"`
	MobileNumber        string `json:"mobileNumber"`
	TrxnNote            string `json:"trxnNote"`
	Address             string `json:"address"`
	MerchantTrxnRefId   string `json:"merchantTrxnRefId"`
	Optional            string `json:"optional"`
}

/**
{
  "amount": "1",
  "address": "Indore",
  "code": "0x0200",
  "mobileNumber": "7869920537",
  "beneficiaryIfscCode": "000890289871772",
  "trxn_id": "FPA1622785699059",
  "debitAccNo": "000190600017042",
  "consentId": "453733",
  "emailAddress": "arpan.sethiya@fidypay.com",
  "beneficiaryAccNo": "000890289871772",
  "trxnAuthDate": "2021-06-04T11:18:26.454+05:30",
  "beneficiaryName": "Arpan Sethiya",
  "instructionIdentification": "202106041106251825",
  "custId": "453733",
  "transferType": "IMPS",
  "transactionIdentification": "8d3e0930c4f811ebaca90a0028fc0000",
  "merchantTrxnRefId": "3123123213123",
  "status": "Received"
}
*/
type PayOutResponse struct {
	TrxnId string `json:"trxn_id"`
	Status string `json:"status"`
}
