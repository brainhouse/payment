package loogpay

import (
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"funzone_pay/app/validator"
	"funzone_pay/centerlog"
	"funzone_pay/common"
	"funzone_pay/dao"
	"funzone_pay/model"
	"funzone_pay/utils"
	"github.com/wonderivan/logger"
	"io/ioutil"
	"net/http"
	"net/url"
	"sort"
	"strings"
	"time"
)

const (
	FieldRequestNo      = "requestNo"
	FieldVersion        = "version"
	FieldProductId      = "productId"
	FieldTransType      = "transType"
	FieldSubTransType   = "subTransType"
	FieldBankCode       = "bankCode"
	FieldBankName       = "bankName"
	FieldAgentId        = "agentId"
	FieldMerNo          = "merNo"
	FieldOrderNo        = "orderNo"
	FieldOrderID        = "orderId"
	FieldCustIp         = "custIp"
	FieldPhoneNo        = "phoneNo"
	FieldCardHolderName = "cardHolderName"
	FieldEmail          = "email"
	FieldNotifyUrl      = "notifyUrl"
	FieldReturnUrl      = "returnUrl"
	FieldTransAmt       = "transAmt"
	FieldOrderDate      = "orderDate"
	FieldDesc           = "desc"
	FieldAcctNo         = "acctNo"
	FieldCurrencyCode   = "currencyCode"
	FieldSignature      = "signature"

	FieldRespCode   = "respCode"
	FieldRespDesc   = "respDesc"
	FieldBankMsg    = "bankMsg"
	FieldTradeState = "tradeState"
)

const (
	Success  = "PAIED"
	Failed   = "FAILED"
	Cancel   = "CANCEL"
	Pending1 = "ACCEPT"
	Pending2 = "READY"

	RespCodeSuccess  = "0000"
	RespCodePSuccess = "P000"
)

var (
	orderError = errors.New("create order error")
)

type LoogPayService struct {
	ID             string
	PAY_ACCOUNT_ID string
	PAY_CHANNEL    string
	COUNTRY_CODE   int

	MERCHANT_ID     string //
	MERCHANT_OUT_ID string
	AGENT_ID        string //
	PUBLIC_KEY      string //
	PRIVATE_KEY     string //

	IN_NOTIFY_URL  string // 异步支付、提现结果通知
	OUT_NOTIFY_URL string
	RETURN_URL     string
	HOST_URL       string // 支付、提现的请求地址
}

func GetLoogPayIns(conf map[string]interface{}) *LoogPayService {
	return &LoogPayService{
		ID:             conf["ID"].(string),
		PAY_ACCOUNT_ID: conf["PAY_ACCOUNT_ID"].(string),
		PAY_CHANNEL:    conf["PAY_CHANNEL"].(string),
		COUNTRY_CODE:   conf["COUNTRY_CODE"].(int),

		MERCHANT_ID:     conf["MERCHANT_ID"].(string),
		MERCHANT_OUT_ID: conf["MERCHANT_OUT_ID"].(string),
		AGENT_ID:        conf["AGENT_ID"].(string),
		PUBLIC_KEY:      conf["PUBLIC_KEY"].(string),
		PRIVATE_KEY:     conf["PRIVATE_KEY"].(string),

		IN_NOTIFY_URL:  conf["IN_NOTIFY_URL"].(string),
		OUT_NOTIFY_URL: conf["OUT_NOTIFY_URL"].(string),
		RETURN_URL:     conf["RETURN_URL"].(string),
		HOST_URL:       conf["HOST_URL"].(string),
	}
}

func (u LoogPayService) Header() http.Header {
	return http.Header{
		"Content-Type": {"application/x-www-form-urlencoded; charset=utf-8"},
	}
}

func (u LoogPayService) CreateOrder(args validator.PayEntryValidator) (*model.PayEntry, error) {
	var err error = nil
	isAlarm := true
	uninx := time.Now().Unix()
	defer func() {
		cost := time.Now().Unix() - uninx
		if isAlarm || cost > 15 {
			common.PushAlarmEvent(common.Warning, fmt.Sprintf("%v", args.Uid), "LoogPayService/CreateOrder", "", fmt.Sprintf("%v-%v Create Order By Loogpay: %v-%v,cost:%v", args.AppId, args.Uid, args.AppOrderId, err, cost))
		}
	}()

	orderId := utils.GetOrderId("LOIN"+u.ID)
	reqData := u.genPayInReqData(orderId, args)
	header := u.Header()

	data, err := u.Request(u.HOST_URL, "POST", reqData, header)
	logger.Debug("LoogPayService_CreateOrder_Info | pay_account_id=%v, reqData=%v | %v | err=%v | response=%v | dur=%v", u.PAY_ACCOUNT_ID, reqData, args.AppOrderId, err, string(data), time.Now().Unix()-uninx)
	if err != nil {
		return nil, fmt.Errorf("create order error-%v", args.AppOrderId)
	}

	response := &PayInResponse{}
	err = json.Unmarshal(data, response)
	if err != nil {
		logger.Error("LoogPayService_CreateOrder_JsonUnmarshal_Err | %v | err=%v | data=%v", args.AppOrderId, err, string(data))
		centerlog.OrderError(centerlog.MsgThirdPlatformFail, u.PAY_ACCOUNT_ID, args.AppId, args.AppOrderId, orderId, "false", err.Error())
		return nil, err
	}

	//
	if response.RespCode != RespCodeSuccess && response.RespCode != RespCodePSuccess { //
		err = fmt.Errorf("%v-%v", response.RespCode, response.RespMsg)
		return nil, fmt.Errorf("service busy, invalid code %v", args.AppOrderId)
	}

	err = VerifyJsonRawDataSign(data, utils.WrapPublicKey(u.PUBLIC_KEY))
	if err != nil {
		logger.Error("LoogPayService_CreateOrder_VerifySign_Err | %v | err=%v ", args.AppOrderId, err)
		return nil, fmt.Errorf("create order error-%v", args.AppOrderId)
	}

	//请求成功写入流水
	pE := new(model.PayEntry)
	pE.AppId = args.AppId
	pE.Uid = args.Uid
	pE.PayAccountId = u.PAY_ACCOUNT_ID
	pE.PayChannel = model.LOOGPAY
	pE.CountryCode = u.COUNTRY_CODE
	pE.AppOrderId = args.AppOrderId
	pE.OrderId = orderId
	pE.PaymentOrderId = fmt.Sprintf("%v", response.OrderId)
	pE.Amount = args.Amount
	pE.UserId = args.UserId
	pE.UserName = args.UserName
	pE.Email = args.Email
	pE.Phone = args.Phone
	pE.Status = model.PAYING
	pE.Created = time.Now().Unix()

	//
	engine, err := dao.GetMysql()
	if engine == nil || err != nil {
		logger.Error("LoogPayService_CreateOrder_GetDB_Err | err=%v", err)
		return nil, err
	}
	_, err = engine.Insert(pE)
	if err != nil {
		logger.Error("LoogPayService_CreateOrder_InsertDB_Err | err=%v", err)
		return nil, err
	}

	isAlarm = false
	pE.PaymentLinkHost = response.PayUrl
	return pE, nil
}

func (u LoogPayService) Inquiry(orderId string, platformOrderId string) (*model.PayEntry, error) {
	header := u.Header()
	reqData := u.genQueryReqData(u.AGENT_ID, u.MERCHANT_ID, orderId, "TRANS_QUERY", platformOrderId)

	resp, err := u.Request(u.HOST_URL, "POST", reqData, header)
	logger.Debug("LoogPayService_Inquiry_Info | requestData=%v | %v | response=%v", reqData, orderId, string(resp))
	if err != nil {
		logger.Error("LoogPayService_Inquiry_Request_Error | err=%v", err)
		return nil, err
	}

	res := &QueryResponse{}
	err = json.Unmarshal(resp, res)
	if err != nil {
		logger.Error("LoogPayService_Inquiry_JsonUnmarshal_Error | err=%v | res=%v", err, res)
		return nil, err
	}

	if res.RespCode != RespCodeSuccess {
		logger.Error("LoogPayService_Inquiry_GetError | err=%v | data=%v", err, orderId)
		return nil, fmt.Errorf("quiry[%v] error", orderId)
	}

	engine, _ := dao.GetMysql()
	payIn := new(model.PayEntry)
	exist, err := engine.Where("order_id=?", orderId).Get(payIn)
	if err != nil {
		logger.Error("LoogPayService_Inquiry_GetError | err=%v | data=%v", err, orderId)
		return nil, err
	}
	if !exist {
		return nil, errors.New("NotExist")
	}

	status := res.TradeState
	if status == Success {
		payIn.Status = model.PAYSUCCESS
		payIn.PaymentId = res.OrderId
	} else if status == Failed {
		payIn.Status = model.PAYFAILD
		//payIn.ThirdCode = res.RespCode
		payIn.ThirdDesc = res.RespMsg
	}

	return payIn, nil
}

func (u LoogPayService) InquiryPayout(orderId string, platformOrderId string) (*model.PayOut, error) {
	header := u.Header()
	reqData := u.genQueryReqData(u.AGENT_ID, u.MERCHANT_OUT_ID, orderId, "TRANS_QUERY", platformOrderId)
	resp, err := u.Request(u.HOST_URL, "POST", reqData, header)
	logger.Debug("LoogPayService_Inquiry_out_Info | requestData=%v | %v | response=%v", reqData, orderId, string(resp))
	if err != nil {
		logger.Error("LoogPayService_Inquiry_out_Request_Error | err=%v", err)
		return nil, err
	}

	res := &QueryResponse{}
	err = json.Unmarshal(resp, res)
	if err != nil {
		logger.Error("LoogPayService_Inquiry_out_JsonUnmarshal_Error | err=%v | res=%v", err, res)
		return nil, err
	}

	if res.RespCode != RespCodeSuccess {
		logger.Error("LoogPayService_Inquiry_GetError | err=%v | data=%v", err, orderId)
		return nil, fmt.Errorf("quiry[%v] error", orderId)
	}

	engine, _ := dao.GetMysql()
	payOut := new(model.PayOut)
	exist, err := engine.Where("order_id=?", orderId).Get(payOut)
	if err != nil {
		logger.Error("LoogPayService_PayOut_Inquiry_GetError | err=%v | data=%v", err, orderId)
		return nil, err
	}
	if !exist {
		return nil, errors.New("NotExist")
	}

	status := res.TradeState
	if status == Success {
		payOut.Status = model.PAY_OUT_SUCCESS
		payOut.PaymentId = res.OrderId
	}
	//else if status == Failed {
	//	payOut.Status = model.PAY_OUT_FAILD
	//	//payOut.ThirdCode = res.RespCode
	//	payOut.ThirdDesc = res.RespMsg
	//}

	return payOut, nil
}

func (u LoogPayService) PayOut(frozenId int64, args validator.PayOutValidator) (payOut *model.PayOut, err error) {
	orderId := utils.GetOrderId("LOUT"+u.ID)
	isAlarm := true
	isForce := false
	msg := ""
	defer func() {
		if isAlarm || isForce {
			common.PushAlarmEvent(common.Warning, fmt.Sprintf("%v", args.Uid), "LoogPayService/Payout", "", fmt.Sprintf("%v-%v Create PayoutOrder By Loogpay: %v-%v", args.AppId, args.Uid, msg, err))
		}
	}()

	payOut = new(model.PayOut)
	payOut.Uid = args.Uid
	payOut.AppId = args.AppId
	payOut.PayAccountId = u.PAY_ACCOUNT_ID
	payOut.PayChannel = model.LOOGPAY
	payOut.CountryCode = u.COUNTRY_CODE
	payOut.PayType = args.PayType
	payOut.AppOrderId = args.AppOrderId
	payOut.OrderId = orderId
	payOut.PaymentId = orderId
	payOut.FrozenId = frozenId
	payOut.Amount = args.Amount
	payOut.BankCard = args.BankCard
	payOut.UserId = args.UserId
	payOut.UserName = args.UserName
	payOut.Email = args.Email
	payOut.Paytm = args.PayTm
	payOut.IFSC = args.IFSC
	payOut.Phone = args.Phone
	engine, err := dao.GetMysql()
	if engine == nil || err != nil {
		logger.Error("LoogPayService_PayOut_GetDB_Err | err=%v", err)
		return nil, err
	}
	_, err = engine.Insert(payOut)
	if err != nil {
		logger.Error("LoogPayService_PayOut_Insert_Error | data=%+v | err=%v", payOut, err)
		return nil, err
	}

	head := u.Header()
	reqData := u.genPayOutReqData(orderId, args)

	// 发起
	data, err := u.Request(u.HOST_URL, "POST", reqData, head)
	logger.Debug("LoogPayService_PayOut_Info | reqData=%v | %v | err=%v | response=%v", reqData.Encode(), args.AppOrderId, err, string(data))
	if err != nil {
		return nil, fmt.Errorf("payout order error")
	}

	response := &PayOutResponse{}
	err = json.Unmarshal(data, response)
	if err != nil {
		logger.Error("LoogPayService_PayOut_JsonUnmarshal_Err | %v | err=%v | data=%v", args.AppOrderId, err, string(data))
		centerlog.OrderError(centerlog.MsgThirdPlatformFail, u.PAY_ACCOUNT_ID, args.AppId, args.AppOrderId, orderId, "false", err.Error())
		return nil, err
	}

	err = VerifyJsonRawDataSign(data, utils.WrapPublicKey(u.PUBLIC_KEY))
	if err != nil {
		logger.Error("LoogPayService_Payout_VerifySign_Err | %v | err=%v ", args.AppOrderId, err)
		return nil, fmt.Errorf("create order error-%v", args.AppOrderId)
	}

	code := response.RespCode
	status := model.PAY_OUT_ING
	thirdCode := payOut.ThirdCode
	thirdDesc := payOut.ThirdDesc
	if response.RespCode != RespCodeSuccess && response.RespCode != RespCodePSuccess {
		isForce = true
		msg = fmt.Sprintf("%v,%v-%v", orderId, response.RespCode, response.RespMsg)
		status = model.PAY_OUT_FAILD
		thirdCode = code
		thirdDesc = response.RespCode
		logger.Error("LoogPayService_PayOut_Status_Err | %v | %v-%v", args.AppOrderId, code, response.RespCode)
		centerlog.OrderError(centerlog.MsgThirdPlatformFail, u.PAY_ACCOUNT_ID, payOut.AppId, payOut.AppOrderId, payOut.OrderId, response.RespMsg, fmt.Sprintf("code:%v", response.RespCode))
	}

	paymentId := response.OrderId
	_, err = engine.Where("id=?", payOut.Id).Update(&model.PayOut{
		Status:    status,
		PaymentId: paymentId,
		ThirdCode: thirdCode,
		ThirdDesc: thirdDesc,
	})
	if err != nil {
		logger.Error("LoogPayService_PayOut_Update_Error | data=%v | err=%v", payOut, err)
		return nil, err
	}

	isAlarm = false

	payOut.Status = status
	payOut.PaymentId = paymentId
	payOut.ThirdDesc = thirdDesc
	payOut.ThirdCode = thirdCode

	return payOut, nil
}

func (u LoogPayService) PayOutBatch(frozenId int64, outValidator validator.PayOutValidator) (*model.PayOut, error) {
	return u.PayOut(frozenId, outValidator)
}

func (u LoogPayService) InSignature(cb interface{}) bool {
	data, ok := cb.(url.Values)
	if !ok {
		return false
	}

	mData := make(map[string]interface{})
	for k, _ := range data {
		mData[k] = data.Get(k)
	}

	return VerifySign(mData, utils.WrapPublicKey(u.PUBLIC_KEY))
}

func (u LoogPayService) OutSignature(cb interface{}) bool {
	return u.InSignature(cb)
}

func (u LoogPayService) genPayInReqData(orderId string, args validator.PayEntryValidator) url.Values {
	data := map[string]interface{}{
		FieldRequestNo:      orderId,
		FieldVersion:        "1.0",
		FieldProductId:      "0105",
		FieldTransType:      "SALES",
		FieldSubTransType:   "01",
		FieldBankCode:       "Paytm",
		FieldAgentId:        u.AGENT_ID,
		FieldMerNo:          u.MERCHANT_ID,
		FieldOrderNo:        orderId,
		FieldCustIp:         args.IP,
		FieldPhoneNo:        args.Phone,
		FieldCardHolderName: args.UserName,
		FieldEmail:          args.Email,
		FieldNotifyUrl:      u.IN_NOTIFY_URL,
		FieldReturnUrl:      u.RETURN_URL,
		FieldTransAmt:       fmt.Sprintf("%d", int(args.Amount*100)),
		FieldOrderDate:      time.Now().Format("20060102"),
		FieldDesc:           "rummy pay",
		FieldCurrencyCode:   "356",
	}

	data[FieldSignature] = GetSign(data, utils.WrapPrivateKey(u.PRIVATE_KEY))

	return genValues(data)
}

func (u LoogPayService) genQueryReqData(agentId string, merchantId string, orderId string, transType string, platformOrderId string) url.Values {
	data := map[string]interface{}{
		FieldRequestNo: orderId,
		FieldVersion:   "1.0",
		FieldProductId: "0401",
		FieldTransType: transType,
		FieldMerNo:     merchantId,
		FieldAgentId:   agentId,
		FieldOrderNo:   orderId,
		FieldOrderID:   platformOrderId,
	}
	data[FieldSignature] = GetSign(data, utils.WrapPrivateKey(u.PRIVATE_KEY))
	return genValues(data)
}

func (u LoogPayService) genPayOutReqData(orderId string, args validator.PayOutValidator) url.Values {
	bankCode := "100" //"IMPS"
	accNo := args.BankCard
	if args.PayType == model.PT_UPI {
		bankCode = "103" //"UPI"
		accNo = args.VPA
	}

	data := map[string]interface{}{
		FieldRequestNo:      orderId,
		FieldVersion:        "1.0",
		FieldProductId:      "0201",
		FieldTransType:      "PROXY_PAY",
		FieldAgentId:        u.AGENT_ID,
		FieldMerNo:          u.MERCHANT_OUT_ID,
		FieldBankCode:       bankCode,  //（100：IMPS、101:NEFT、102:RTGS、103:UPI
		FieldBankName:       args.IFSC, // 收款方账号IFSC，交易类型为IMPS、NEFT、RTGS时
		FieldOrderNo:        orderId,
		FieldOrderDate:      time.Now().Format("20060102"),
		FieldNotifyUrl:      u.OUT_NOTIFY_URL,
		FieldTransAmt:       int64(args.Amount * 100),
		FieldAcctNo:         accNo, //交易类型为UPI时，此项内容为VPS/VPA/UPI ID
		FieldEmail:          args.Email,
		FieldPhoneNo:        args.Phone,
		FieldCurrencyCode:   356,
		FieldCardHolderName: args.UserName,
	}

	data[FieldSignature] = GetSign(data, utils.WrapPrivateKey(u.PRIVATE_KEY))
	return genValues(data)
}

func (u LoogPayService) Request(url string, method string, sendData url.Values, header map[string][]string) (res []byte, err error) {
	client := &http.Client{}
	//处理返回结果
	response, err := client.PostForm(url, sendData)
	if err != nil {
		logger.Error("LoogPayService_request_err | url=%v | err=%+v | req=%+v", url, err, sendData)
		return
	}
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		logger.Error("LoogPayService_response_read_err | url=%v | err=%+v | req=%+v", url, err, sendData)
		return
	}
	defer func() {
		_ = response.Body.Close()
	}()
	return body, nil
}

func GetSign(data map[string]interface{}, privateKey []byte) string {
	str := sortKeys(data)
	sign, err := utils.RsaSignWithSha1Base64([]byte(str), privateKey)
	if err != nil {
		logger.Error("LoogPay build sign error [%v]:%v", err, str)
	}

	return sign
}

func VerifyJsonRawDataSign(data []byte, publicKey []byte) (err error) {
	keyVal := make(map[string]interface{})
	err = json.Unmarshal(data, &keyVal)
	if err != nil {
		logger.Error("LoogPayService_JsonUnmarshal_Err | %v | err=%v ", string(data), err)
		return err
	}

	if !VerifySign(keyVal, publicKey) {
		logger.Error("LoogPayService_VerifySign_Err | %v ", string(data))
		return fmt.Errorf("verify sign error")
	}

	return nil
}

func VerifySign(data map[string]interface{}, publicKey []byte) bool {
	sign, ok := data[FieldSignature]
	if !ok {
		logger.Error("LoogPay verify sign not found sign field")
		return false
	}

	origin, err := base64.StdEncoding.DecodeString(fmt.Sprintf("%v", sign))
	if err != nil {
		logger.Error("LoogPay verify sign decode base64 error: %v:%v", err, sign)
		return false
	}

	str := sortKeys(data)
	b, err := utils.RsaVerySignWithSha1([]byte(str), origin, publicKey)
	if err != nil {
		logger.Error("LoogPay verify sign error %v:%v", err, str)
	}

	return b
}

func sortKeys(data map[string]interface{}) string {
	keys := make([]string, 0, len(data))
	for k, _ := range data {
		if k == FieldSignature {
			continue
		}

		keys = append(keys, k)
	}

	sort.Strings(keys)

	keyVal := make([]string, 0, len(keys))
	for _, k := range keys {
		val := fmt.Sprintf("%v", data[k])
		if len(val) < 1 {
			continue
		}

		keyVal = append(keyVal, fmt.Sprintf("%v=%v", k, val))
	}

	return strings.Join(keyVal, "&")
}

func genValues(data map[string]interface{}) url.Values {
	value := url.Values{}
	for k, v := range data {
		value.Add(k, fmt.Sprint(v))
	}

	return value
}
