package loogpay

/*
"agentId":"100472","orderDate":"2023208","orderId":"1600848247447113728","orderNo":"LOIN16705069360011251e21",
"payUrl":"https://payment.indipayment.com/KJ1912194497245","productId":"0105","requestNo":"LOIN16705069360011251e21","respCode":"P000","respMsg":"Pending",
"signature":"A5mqGfykd9sAyIGlwO5cIDZ+9tfmCbHcnpec7ANLUc9fosMFv7e5Efq8yTeG5mp5GbFQRjbryX9jy6wn7lnq7I8GbkLKmlWrR460+3Z5dH3MGJ+ogIFoGfhJYrlSr/aKSJB4K+useavy5tt/WkpTacXi22tYEuyFbwH602RiywavyAO2gou34XqvBkS9w5fH8YObRTRKcglxOKYhIZ0AaRAgiuBRtBzyp9YD9I/oi+0UGAhm0OgAtkI4nVVYAHgz/eH/Awk94Q7+ufnd/xSeqMJDNEj7NMH/EEIqeiNrlNqNwML2IYrkKGpopuKwNFkAQkS649NWD+xjn+yEHsKhCw==",
"tradeSatus":"ACCEPT","transAmt":"9000","transType":"SALES","version":"1.0"}

*/
type PayInResponse struct {
	RequestNo  string `json:"requestNo"`
	PayUrl     string `json:"payUrl"` // 如果该值不为空，并且返回码为：P000,待支付
	PayData    string `json:"payData"`
	OrderId    string `json:"orderId"`
	OrderDate  string `json:"orderDate"`
	TradeSatus string `json:"tradeSatus"`
	Acquirer   string `json:"acquirer"`
	OrderNo    string `json:"orderNo"`
	RespCode   string `json:"respCode"`
	RespMsg    string `json:"respMsg"`
	Signature  string `json:"signature"`
}

/*
{"agentId":"100472","bankMsg":"Pending","orderDate":"2023208","orderId":"1600848247447113728","orderNo":"LOIN16705069360011251e21","orderProductId":"0105",
"orderTransType":"SALES","productId":"0401","requestNo":"LOIN16705069360011251e21","respCode":"0000","respMsg":"approve",
"signature":"Lrf/XiHnxmMRUGiTnukZBdQsHnrJGL9tP/7oVE6fF8MYiWzkqb7CQXu9NoqMgYrCEBq/6rb+WSQd9hnEglBwfNo0Ov3cuTv6xxfHdWGrbFIUgBOofwl268Cz1cnfnigFbm1dDYDPIdicqyMEIwfGmguuJbbhq1yjxLWyqOg49NV+yEBKN5Wp0TbN525XbUS49c4hCSWaez3ohCqJ40gLkQWYLQKxHwlpdpsr3DfiK0pyFIlCl3iyzgmB2KoAQhdgYrR/QebRL/yHSZTvU0IVqi8n7qiCwukedJRMvvbQzwJcU6y9KJJo1aEMmTEy8l8K5NQbRu+1kqzy7v0VGZ4H3A==",
"tradeState":"ACCEPT","transAmt":"9000","transType":"TRANS_QUERY","version":"1.0"}
*/
type QueryResponse struct {
	OrderDate      string `json:"orderDate"`
	TradeState     string `json:"tradeState"` //PAIED.成功FAILED.失败ACCEPT处理中，READY待支付
	OrderProductId string `json:"orderProductId"`
	OrderTransType string `json:"orderTransType"`
	BankMsg        string `json:"bankMsg"`
	OrderNo        string `json:"orderNo"`
	OrderId        string `json:"orderId"`
	RespCode       string `json:"respCode"`
	RespMsg        string `json:"respMsg"`
	Signature      string `json:"signature"`
}

type PayNotify struct {
	OrderDate  string `json:"orderDate"`
	OrderNo    string `json:"orderNo"`
	OrderId    string `json:"orderId"`
	TradeState string `json:"tradeState"` // PAIED.成功FAILED.失败，CANCEl 撤销
	BankMsg    string `json:"bankMsg"`    // tradeState为PAIED，是返回银行URT信息,tradeState为FAILED时，返回具体的失败原因
	TransAmt   string `json:"transAmt"`
	RespCode   string `json:"respCode"`
	RespDesc   string `json:"respDesc"`
}

type PayOutResponse struct {
	OrderId  string `json:"orderId"`
	OrderNo  string `json:"orderNo"`
	RespCode string `json:"respCode"`
	RespMsg  string `json:"respMsg"`
}
