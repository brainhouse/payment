package globpay

import (
	"encoding/base64"
	"encoding/json"
)

type GlobExtraData struct {
	PayAccountId string `json:"pay_account_id"`
}

func (g*GlobExtraData) ToString() string {
	data, err := json.Marshal(g)
	if err != nil {
		return ""
	}

	return base64.StdEncoding.EncodeToString(data)
}

func (g *GlobExtraData) ParseFromString(data string) error {
	bts, err := base64.StdEncoding.DecodeString(data)
	if err != nil {
		return err
	}

	return json.Unmarshal(bts, g)
}

/**
{
	“code”: 200,
	“message”,”place an order successfully”,
	“data”:{
		“payOrderId”: “ordeNo123456”,—平台单号
		“orderAmount”:1000, —订单金额
		“payUrl”:”http://xxx/pay?orderId=xxx“ —支付链接
	}
}
*/
type OrderInResponse struct {
	Data struct {
		PayOrderId  string `json:"payOrderId"`
		OrderAmount int64  `json:"orderAmount"`
		PayUrl      string `json:"payUrl"`
	} `json:"data"`

	Code int64  `json:"code"`
	Msg  string `json:"message"`
}



/**
{
    “code”: 200,
     “message”,”place an order successfully”,
     “data”:{
             “payOrderId”: “ordeNo123456”,—平台单号
            “orderAmount”:1000, —订单金额
     }
}
*/

type PayOutResponse struct {
	Data struct {
		OrderAmount int64  `json:"orderAmount"`
		PayOrderId  string `json:"payOrderId"`
	} `json:"data"`
	Code int64  `json:"code"`
	Msg  string `json:"message"`
}


type InquiryResponse struct {
	Data struct {
		PayOrderId  string `json:"payOrderId"`
		MchId       int64  `json:"mchId"`
		Status      string `json:"status"`
		OrderAmount int64  `json:"orderAmount"`
		MchOrderNo  string `json:"mchOrderNo"`
		ProductId   int64  `json:"productId"`
		CreateTime  string `json:"createTime"`
		PaySuccTime int64  `json:"paySuccTime"`
	} `json:"data"`
	Code int64  `json:"code"`
	Msg  string `json:"message"`
}