package globpay

import (
	"encoding/json"
	"errors"
	"fmt"
	"funzone_pay/app/validator"
	"funzone_pay/dao"
	"funzone_pay/errdef"
	"funzone_pay/model"
	"funzone_pay/utils"
	"github.com/wonderivan/logger"
	"io/ioutil"
	"net/http"
	"net/url"
	"sort"
	"strconv"
	"strings"
	"time"
)

type GlobPayService struct {
	PAY_ACCOUNT_ID string
	PAY_CHANNEL    string
	COUNTRY_CODE   int

	MERCHANT_ID    string
	MERCHANT_KEY   string
	PRODUCT_ID_IN  string
	PRODUCT_ID_OUT string

	HOST         string // 三方支付收款
	COLLECT_HOST string // 我们的落地页

	NOTIFY_URL        string
	RETRUN_URL        string
	PAYOUT_NOTIFY_URL string
	PAYOUT_RETURN_URL string

	// upi维护时间
	MAINTAIN_START int
	MAINTAIN_END   int
}

func GetGlobPayIns(conf map[string]interface{}) *GlobPayService {
	start, _ := strconv.ParseInt(conf["MAINTAIN_START"].(string), 10, 64)
	end, _ := strconv.ParseInt(conf["MAINTAIN_END"].(string), 10, 64)

	return &GlobPayService{
		PAY_ACCOUNT_ID: conf["PAY_ACCOUNT_ID"].(string),
		PAY_CHANNEL:    conf["PAY_CHANNEL"].(string),
		COUNTRY_CODE:   conf["COUNTRY_CODE"].(int),

		MERCHANT_ID:    conf["MERCHANT_ID"].(string),
		MERCHANT_KEY:   conf["MERCHANT_KEY"].(string),
		PRODUCT_ID_IN:  conf["PRODUCT_ID_IN"].(string),
		PRODUCT_ID_OUT: conf["PRODUCT_ID_OUT"].(string),

		HOST:         conf["HOST"].(string),
		COLLECT_HOST: conf["COLLECT_HOST"].(string),

		RETRUN_URL: conf["RETURN_URL"].(string),
		NOTIFY_URL: conf["NOTIFY_URL"].(string),

		PAYOUT_NOTIFY_URL: conf["PAYOUT_NOTIFY_URL"].(string),
		PAYOUT_RETURN_URL: conf["PAYOUT_RETURN_URL"].(string),

		MAINTAIN_START: int(start),
		MAINTAIN_END:   int(end),
	}
}

func (e *GlobPayService) CreateOrder(args validator.PayEntryValidator) (*model.PayEntry, error) {
	hour := (time.Now().Hour() + 8) % 24
	logger.Debug("GlobPayService_CreateOrder_Time %v|%v|%v", hour, e.MAINTAIN_START, e.MAINTAIN_END)
	if hour >= e.MAINTAIN_START && hour < e.MAINTAIN_END {
		return nil, errdef.ErrMaintainTime
	}

	orderId := utils.GetOrderId("GPIN")
	requestUrl := fmt.Sprintf("%s%s", e.HOST, "/api/pay/create_order")

	params := url.Values{}
	params.Add("type", "0")
	params.Add("mchId", e.MERCHANT_ID)
	params.Add("mchOrderNo", orderId)
	params.Add("productId", e.PRODUCT_ID_IN)
	params.Add("orderAmount", fmt.Sprintf("%d", int64(args.Amount*100)))
	params.Add("notifyUrl", e.NOTIFY_URL)
	if len(args.ReturnUrl) > 0 {
		params.Add("returnUrl", args.ReturnUrl)
	} else if len(e.RETRUN_URL) > 0 {
		params.Add("returnUrl", e.RETRUN_URL)
	}
	params.Add("clientIp", "0.0.0.0")
	params.Add("device", "pc")
	params.Add("uid", fmt.Sprintf("%v", args.Uid))
	params.Add("customerName", args.UserName)
	params.Add("tel", args.Phone)
	params.Add("email", args.Email)
	params.Add("returnType", "json")

	extra := GlobExtraData{
		PayAccountId: e.PAY_ACCOUNT_ID,
	}
	extraStr := extra.ToString()
	if extraStr == "" {
		logger.Error("GlobPayService_CreateOrder_Pack_extra_Err | pay_account_id=%v | uid=%v | params=%v", e.PAY_ACCOUNT_ID, args.Uid, params.Encode())
		return nil, fmt.Errorf("inner error")
	}
	params.Add("extra", extraStr)

	sign := e.GetSign(params, e.MERCHANT_KEY)
	params.Add("sign", sign)

	header := map[string][]string{
		"Content-type": {"application/x-www-form-urlencoded"},
	}
	data, err := e.PostRequestForm(requestUrl, params, header)
	logger.Debug("GlobPayService_CreateOrder_Info | orderReq=%+v | data = %+v", params.Encode(), string(data))
	if err != nil {
		logger.Error("GlobPayService_CreateOrder_PostRequestForm_Err | err=%v", err)
		return nil, fmt.Errorf("create order error")
	}

	egResponse := &OrderInResponse{}
	err = json.Unmarshal(data, egResponse)
	if err != nil {
		logger.Error("GlobPayService_CreateOrder_JsonUnmarshal_Err | err=%v ", err)
		return nil, err
	}
	//请求成功写入流水
	pE := new(model.PayEntry)
	pE.AppId = args.AppId
	pE.Uid = args.Uid
	pE.PayAccountId = e.PAY_ACCOUNT_ID
	pE.PayChannel = model.GLOBPAY
	pE.CountryCode = e.COUNTRY_CODE
	pE.AppOrderId = args.AppOrderId
	pE.OrderId = orderId
	pE.PaymentOrderId = egResponse.Data.PayOrderId
	pE.Amount = args.Amount
	pE.UserId = args.UserId
	pE.UserName = args.UserName
	pE.Email = args.Email
	pE.Phone = args.Phone
	pE.ThirdCode = fmt.Sprint(egResponse.Code)
	pE.Status = model.PAYING
	pE.PayAppId = e.MERCHANT_ID
	pE.FinishTime = time.Now().Unix()
	if egResponse.Code != 200 {
		pE.ThirdDesc = fmt.Sprintf("%d:%v", egResponse.Code, egResponse.Msg)
	}
	engine, err := dao.GetMysql()
	if engine == nil || err != nil {
		logger.Error("GlobPayService_CreateOrder_GetDB_Err | err=%v", err)
		return nil, err
	}
	_, err = engine.Insert(pE)
	if err != nil {
		logger.Error("GlobPayService_CreateOrder_InsertDB_Err | err=%v", err)
		return nil, err
	}
	if egResponse.Code != 200 {
		err := errors.New(fmt.Sprintf("%d:%v", egResponse.Code, egResponse.Msg))
		return nil, err
	}
	pE.PaymentLinkHost = egResponse.Data.PayUrl
	return pE, err
}

func (e *GlobPayService) Inquiry(s string, s2 string) (*model.PayEntry, error) {
	return nil, fmt.Errorf("no impletion")
}

func (e *GlobPayService) InquiryPayout(orderId string, ext string) (*model.PayOut, error) {
	inquiry := url.Values{}
	inquiry.Add("mchId", e.MERCHANT_ID)
	inquiry.Add("payOrderId", orderId)

	sign := e.GetSign(inquiry, e.MERCHANT_KEY)
	inquiry.Add("sign", sign)

	requestUrl := fmt.Sprintf("%s%s", e.HOST, "/api/pay/query_order")
	header := map[string][]string{
		"Content-Type": {"application/x-www-form-urlencoded"},
	}
	data, err := e.PostRequestForm(requestUrl, inquiry, header)
	logger.Debug("GlobPayService_Inquiry_Info | requestUrl=%v | requestData=%v | response=%v", requestUrl, inquiry.Encode(), string(data))
	if err != nil {
		logger.Error("GlobPayService_Inquiry_Request_Error | err=%v", err)
		return nil, fmt.Errorf("Inquiry Order Error")
	}

	res := &InquiryResponse{}
	err = json.Unmarshal(data, res)
	if err != nil {
		logger.Error("GlobPayService_Inquiry_JsonUnmarshal_Error | err=%v", err)
		return nil, err
	}

	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("GlobPayService_InquiryPayOut_Engine_Error | orderId=%+v | err=%v", orderId, err)
		return nil, err
	}

	payOut := new(model.PayOut)
	exist, err := engine.Where("order_id=?", orderId).Get(payOut)
	if err != nil {
		logger.Error("GlobPayService_InquiryPayout_GetError | err=%v | data=%v", err, orderId)
		return nil, err
	}

	logger.Debug("GlobPayService_InquiryPayout | exist=%v | data=%v", exist, payOut)
	return payOut, nil
}

func (e *GlobPayService) PayOut(frozenId int64, outValidator validator.PayOutValidator) (payOut *model.PayOut, err error) {
	transId := utils.GetOrderId("GPOUT")
	payOut = new(model.PayOut)
	payOut.Uid = outValidator.Uid
	payOut.AppId = outValidator.AppId
	payOut.PayAccountId = e.PAY_ACCOUNT_ID
	payOut.PayChannel = model.GLOBPAY
	payOut.CountryCode = e.COUNTRY_CODE
	payOut.PayType = outValidator.PayType
	payOut.AppOrderId = outValidator.AppOrderId
	payOut.OrderId = transId
	payOut.PaymentId = transId
	payOut.FrozenId = frozenId
	payOut.Amount = outValidator.Amount
	payOut.BankCard = outValidator.BankCard
	payOut.UserId = outValidator.UserId
	payOut.UserName = outValidator.UserName
	payOut.Email = outValidator.Email
	payOut.Paytm = outValidator.PayTm
	payOut.IFSC = outValidator.IFSC
	payOut.Phone = outValidator.Phone
	engine, err := dao.GetMysql()
	if engine == nil || err != nil {
		logger.Error("GlobPayService_PayOut_GetDB_Err | err=%v", err)
		return nil, err
	}
	_, err = engine.Insert(payOut)
	if err != nil {
		logger.Error("GlobPayService_PayOut_Insert_Error | data=%+v | err=%v", payOut, err)
		return nil, err
	}
	//发起支付
	requestUrl := fmt.Sprintf("%s%s", e.HOST, "/api/pay/create_order")
	header := map[string][]string{
		"Content-Type": {"application/x-www-form-urlencoded"},
	}

	params := url.Values{}
	params.Add("type", "1")
	params.Add("mchId", e.MERCHANT_ID)
	params.Add("accountname", outValidator.UserName)
	params.Add("cardnumber", outValidator.BankCard)
	params.Add("mchOrderNo", transId)
	params.Add("productId", e.PRODUCT_ID_OUT)
	params.Add("orderAmount", fmt.Sprintf("%d", int64(outValidator.Amount*100)))
	params.Add("notifyUrl", e.PAYOUT_NOTIFY_URL)
	if len(e.PAYOUT_RETURN_URL) > 0 {
		params.Add("returnUrl", e.PAYOUT_RETURN_URL)
	}
	params.Add("clientIp", "0.0.0.0")
	params.Add("device", "pc")
	params.Add("uid", fmt.Sprintf("%v", outValidator.Uid))
	params.Add("customerName", outValidator.UserName)
	params.Add("tel", outValidator.Phone)
	params.Add("mode", "IMPS")
	params.Add("ifsc", outValidator.IFSC)
	params.Add("tel", outValidator.Phone)
	params.Add("returnType", "json")
	params.Add("bankname", "default")
	params.Add("email", outValidator.Email)
	extra := GlobExtraData{
		PayAccountId: e.PAY_ACCOUNT_ID,
	}
	extraStr := extra.ToString()
	if extraStr == "" {
		logger.Error("GlobPayService_PayOut_Pack_extra_Err | pay_account_id=%v | uid=%v | params=%v", e.PAY_ACCOUNT_ID, outValidator.Uid, params)
		return nil, fmt.Errorf("inner error")
	}
	params.Add("extra", extraStr)

	sign := e.GetSign(params, e.MERCHANT_KEY)
	params.Add("sign", sign)

	data, err := e.PostRequestForm(requestUrl, params, header)
	logger.Debug("GlobPayService_PayOut_Info | requestUrl=%v | requestData=%v | response=%v", requestUrl, params.Encode(), string(data))
	if err != nil {
		logger.Error("GlobPayService_PayOut_Request_Error | err=%v", err)
		return nil, fmt.Errorf("PayOut Order Error")
	}

	res := &PayOutResponse{}
	err = json.Unmarshal(data, res)
	if err != nil {
		logger.Error("GlobPayService_PayOut_JsonUnmarshal_Error | err=%v", err)
		return nil, err
	}
	var (
		status      model.PayOutStatus
		thirdDesc   string
		referenceId string
	)
	payOut.ThirdCode = fmt.Sprintf("%v", res.Code)
	if res.Code != 200 {
		//先不设置成失败
		//status = model.PAY_OUT_FAILD
		thirdDesc = res.Msg
	} else {
		status = model.PAY_OUT_ING
		referenceId = res.Data.PayOrderId
	}
	_, err = engine.Where("id=?", payOut.Id).Update(&model.PayOut{
		Status:     status,
		ThirdCode:  payOut.ThirdCode,
		PaymentId:  referenceId,
		ThirdDesc:  thirdDesc,
		FinishTime: time.Now().Unix(),
	})
	if err != nil {
		logger.Error("GlobPayService_PayOut_Insert_Error | data=%v | err=%v", payOut, err)
		return nil, err
	}
	return payOut, nil
}

func (e *GlobPayService) GetSign(params url.Values, sk string) string {
	var sortSlice []string
	for k, v := range params {
		if k == "sign" || k == "message" || v[0] == "" {
			continue
		}
		sortSlice = append(sortSlice, k)
	}
	sort.Strings(sortSlice)
	signs := ""
	for _, v := range sortSlice {
		signs = fmt.Sprintf("%v%v=%v&", signs, v, params[v][0])
	}
	signs += "key=" + sk
	return utils.GetMd5(signs)
}

func (e *GlobPayService) PayOutBatch(frozenId int64, outValidator validator.PayOutValidator) (*model.PayOut, error) {
	return e.PayOut(frozenId, outValidator)
}

func (e *GlobPayService) InSignature(cb interface{}) bool {
	params, ok := cb.(url.Values)
	if !ok {
		return false
	}

	sign := params.Get("sign")
	calc := e.GetSign(params, e.MERCHANT_KEY)
	if sign == calc {
		return true
	}

	logger.Error("sign invalid %v-->%v", sign, calc)
	return false
}

func (e *GlobPayService) OutSignature(cb interface{}) bool {
	return e.InSignature(cb)
}

/*
*
统一请求
*/
func (e *GlobPayService) PostRequestForm(url string, sendData url.Values, header map[string][]string) (res []byte, err error) {
	client := &http.Client{}
	reader := strings.NewReader(sendData.Encode())
	req, err := http.NewRequest("POST", url, reader)
	if err != nil {
		logger.Error("GlobPayService_Create_order | url=%v | err=%+v | req=%+v", url, err, sendData)
		return
	}
	req.Header = http.Header(header)
	//处理返回结果
	response, err := client.Do(req)
	if err != nil {
		logger.Error("GlobPayService_request_err | url=%v | err=%+v | req=%+v", url, err, sendData)
		return
	}
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		logger.Error("GlobPayService_response_read_err | url=%v | err=%+v | req=%+v", url, err, sendData)
		return
	}
	defer func() {
		_ = response.Body.Close()
	}()
	return body, nil
}
