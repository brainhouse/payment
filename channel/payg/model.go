package payg

type CollectRequest struct {
	Merchantkeyid   string `json:"Merchantkeyid"`
	UniqueRequestID string `json:"UniqueRequestId"`
	ProductData     string `json:"ProductData"`
	RequestDateTime string `json:"RequestDateTime"`
	RedirectURL     string `json:"RedirectUrl"`
	TransactionData struct {
		AcceptedPaymentTypes string `json:"AcceptedPaymentTypes"`
		PaymentType          string `json:"PaymentType"`
		WalletType           string `json:"WalletType"`
	} `json:"TransactionData"`
	OrderAmount     string `json:"OrderAmount"`
	OrderType       string `json:"OrderType"`
	OrderAmountData struct {
		AmountTypeDesc string `json:"AmountTypeDesc"`
		Amount         string `json:"Amount"`
	} `json:"OrderAmountData"`
	CustomerData struct {
		CustomerID    string `json:"CustomerId"`
		CustomerNotes string `json:"CustomerNotes"`
		FirstName     string `json:"FirstName"`
		LastName      string `json:"LastName"`
		MobileNo      string `json:"MobileNo"`
		Email         string `json:"Email"`
	} `json:"CustomerData"`
}

/*
{
    "OrderKeyId": "25393230118M8792Uf3f5b5c13p_1121",
    "MerchantKeyId": 8792,
    "UniqueRequestId": "f3f5b5c13p_1121",
    "OrderType": "PAYMENT",
    "OrderAmount": 100.0,
    "OrderId": null,
    "OrderStatus": null,
    "OrderPaymentStatus": 0,
    "OrderPaymentStatusText": null,
    "PaymentStatus": 0,
    "PaymentTransactionId": null,
    "PaymentResponseCode": 0,
    "PaymentApprovalCode": null,
    "PaymentTransactionRefNo": null,
    "PaymentResponseText": null,
    "PaymentMethod": null,
    "PaymentAccount": null,
    "OrderNotes": null,
    "PaymentDateTime": null,
    "UpdatedDateTime": null,
    "PaymentProcessUrl": "https://uat.payg.in/payment/payment?orderid=25393230118M8792Uf3f5b5c13p_1121",
    "CustomerData": {
        "CustomerId": "152433",
        "CustomerNotes": "Mens clothing",
        "FirstName": "ravi",
        "LastName": "sharma",
        "MobileNo": "7337327109",
        "Email": "test@gmail.com",
        "EmailReceipt": true,
        "BillingAddress": "44 bhagvan nagar",
        "BillingCity": "orissa",
        "BillingState": "orissa",
        "BillingCountry": "India",
        "BillingZipCode": "30202020",
        "ShippingFirstName": "ravi",
        "ShippingLastName": "sharma",
        "ShippingAddress": "44 bhagvan nagar",
        "ShippingCity": "orissa",
        "ShippingState": "orissa",
        "ShippingCountry": "India",
        "ShippingZipCode": "30202020",
        "ShippingMobileNo": "7337327109"
    },
    "ProductData": "{}",
    "OrderPaymentCustomerData": {
        "FirstName": "ravi",
        "LastName": null,
        "Address": null,
        "City": null,
        "State": null,
        "ZipCode": null,
        "Country": null,
        "MobileNo": "7337327109",
        "Email": "test@gmail.com",
        "UserId": null,
        "IpAddress": null
    },
    "UpiLink": null,
    "OrderPaymentTransactionDetail": null,
    "UserDefinedData": {
        "UserDefined1": "",
        "UserDefined2": null,
        "UserDefined3": null,
        "UserDefined4": null,
        "UserDefined5": null,
        "UserDefined6": null,
        "UserDefined7": null,
        "UserDefined8": null,
        "UserDefined9": null,
        "UserDefined10": null,
        "UserDefined11": null,
        "UserDefined12": null,
        "UserDefined13": null,
        "UserDefined14": null,
        "UserDefined15": null,
        "UserDefined16": null,
        "UserDefined17": null,
        "UserDefined18": null,
        "UserDefined19": null,
        "UserDefined20": null
    },
    "Id": 25393
}
*/
type OrderInResponse struct {
	OrderKeyID                    string      `json:"OrderKeyId"`
	MerchantKeyID                 int         `json:"MerchantKeyId"`
	UniqueRequestID               string      `json:"UniqueRequestId"`
	OrderType                     string      `json:"OrderType"`
	OrderAmount                   float64     `json:"OrderAmount"`
	OrderID                       interface{} `json:"OrderId"`
	OrderStatus                   interface{} `json:"OrderStatus"`
	OrderPaymentStatus            int         `json:"OrderPaymentStatus"`
	OrderPaymentStatusText        interface{} `json:"OrderPaymentStatusText"`
	PaymentStatus                 int         `json:"PaymentStatus"`
	PaymentTransactionID          interface{} `json:"PaymentTransactionId"`
	PaymentResponseCode           int         `json:"PaymentResponseCode"`
	PaymentApprovalCode           interface{} `json:"PaymentApprovalCode"`
	PaymentTransactionRefNo       interface{} `json:"PaymentTransactionRefNo"`
	PaymentResponseText           interface{} `json:"PaymentResponseText"`
	PaymentMethod                 interface{} `json:"PaymentMethod"`
	PaymentAccount                interface{} `json:"PaymentAccount"`
	OrderNotes                    interface{} `json:"OrderNotes"`
	PaymentDateTime               interface{} `json:"PaymentDateTime"`
	UpdatedDateTime               interface{} `json:"UpdatedDateTime"`
	PaymentProcessURL             string      `json:"PaymentProcessUrl"`
	ProductData                   string      `json:"ProductData"`
	UpiLink                       interface{} `json:"UpiLink"`
	OrderPaymentTransactionDetail interface{} `json:"OrderPaymentTransactionDetail"`
	ID                            int         `json:"Id"`
}
