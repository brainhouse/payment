package payg

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"funzone_pay/app/validator"
	"funzone_pay/centerlog"
	"funzone_pay/dao"
	"funzone_pay/model"
	"funzone_pay/utils"
	"github.com/wonderivan/logger"
	"io/ioutil"
	"net/http"
	"strings"
	"time"
)

type PayGService struct {
	PAY_ACCOUNT_ID string
	PAY_CHANNEL    string
	HOST           string
	MID            string
	AK             string
	TOKEN          string
	SHK            string
	NOTIFY_URL     string
	COLLECT_HOST   string
	COUNTRY_CODE   int
}

func GetPayGIns(conf map[string]interface{}) *PayGService {
	return &PayGService{
		PAY_ACCOUNT_ID: conf["PAY_ACCOUNT_ID"].(string),
		PAY_CHANNEL:    conf["PAY_CHANNEL"].(string),
		HOST:           conf["HOST"].(string),
		MID:            conf["MID"].(string),
		AK:             conf["AK"].(string),
		TOKEN:          conf["TOKEN"].(string),
		SHK:            conf["SHK"].(string),
		NOTIFY_URL:     conf["NOTIFY_URL"].(string),
		COUNTRY_CODE:   conf["COUNTRY_CODE"].(int),
		COLLECT_HOST:   conf["COLLECT_HOST"].(string),
	}
}

func (a *PayGService) CreateOrder(args validator.PayEntryValidator) (*model.PayEntry, error) {
	orderId := utils.GetOrderId("P1")

	requestUrl := fmt.Sprintf("%s%s", a.HOST, "/payment/api/order/create")

	/**
	{
	    "Merchantkeyid": "29900",
	    "UniqueRequestId": "f3f5b5c13p_1123",
	    "ProductData": "{}",
	    "RequestDateTime": "06232021",
	    "RedirectUrl": "https://payg.in",
	    "TransactionData": {
	        "AcceptedPaymentTypes": "",
	        "PaymentType": "Wallet",
	        "WalletType": "PayTm"
	    },
	    "OrderAmount": "100",
	    "OrderType": "",
	    "CustomerData": {
	        "CustomerId": "152433",
	        "CustomerNotes": "Mens clothing",
	        "FirstName": "ravi",
	        "LastName": "sharma",
	        "MobileNo": "7337327109",
	        "Email": "john@gmail.com"
	    }
	}
	*/
	auth := base64.StdEncoding.EncodeToString([]byte(fmt.Sprintf("%v:%v:M:%v", a.AK, a.TOKEN, a.MID)))
	header := map[string][]string{
		"Content-Type":  {"application/json"},
		"Authorization": {fmt.Sprintf("basic %v", auth)},
	}
	reqData := &CollectRequest{
		Merchantkeyid:   a.MID,
		UniqueRequestID: orderId,
		ProductData:     "{}",
		RequestDateTime: time.Now().Format("01022006"),
		RedirectURL:     "https://payg.in",
		OrderAmount:     fmt.Sprint(args.Amount),
	}
	reqData.TransactionData.PaymentType = "Wallet"
	reqData.TransactionData.WalletType = "PayTm"
	reqData.CustomerData.FirstName = args.UserName
	reqData.CustomerData.LastName = args.UserName
	reqData.CustomerData.MobileNo = args.Phone
	reqData.CustomerData.Email = args.Email

	data, err := a.Request(requestUrl, "POST", "JSON", reqData, header)
	logger.Debug("PayGService_CreateOrder_Info | orderReq=%v | err=%v | data=%v", reqData, err, string(data))

	egResponse := &OrderInResponse{}
	err = json.Unmarshal(data, egResponse)
	if err != nil {
		logger.Error("PayGService_CreateOrder_JsonUnmarshal_Err | err=%v", err)
		centerlog.OrderError(centerlog.MsgThirdPlatformFail, a.PAY_ACCOUNT_ID, args.AppId, args.AppOrderId, orderId, "false", err.Error())
		return nil, err
	}
	//请求成功写入流水
	pE := new(model.PayEntry)
	pE.AppId = args.AppId
	pE.Uid = args.Uid
	pE.PayAccountId = a.PAY_ACCOUNT_ID
	pE.PayChannel = model.PAYG
	pE.CountryCode = a.COUNTRY_CODE
	pE.AppOrderId = args.AppOrderId
	pE.OrderId = orderId
	pE.Amount = args.Amount
	pE.UserId = args.UserId
	pE.UserName = args.UserName
	pE.Email = args.Email
	pE.Phone = args.Phone
	pE.Status = model.PAYING
	pE.FinishTime = time.Now().Unix()

	engine, err := dao.GetMysql()
	if engine == nil || err != nil {
		logger.Error("PayGService_CreateOrder_GetDB_Err | err=%v", err)
		return nil, err
	}
	_, err = engine.Insert(pE)
	if err != nil {
		logger.Error("PayGService_CreateOrder_InsertDB_Err | err=%v", err)
		return nil, err
	}
	pE.PaymentLinkHost = egResponse.PaymentProcessURL
	return pE, err
}

func (a *PayGService) Inquiry(orderId string, s2 string) (*model.PayEntry, error) {
	return nil, nil
}

func (a *PayGService) InquiryPayout(orderId string, ext string) (*model.PayOut, error) {

	return nil, nil
}

func (a *PayGService) PayOut(frozenId int64, args validator.PayOutValidator) (*model.PayOut, error) {
	return nil, fmt.Errorf("implement")
}

func (a *PayGService) PayOutBatch(frozenId int64, args validator.PayOutValidator) (*model.PayOut, error) {
	return a.PayOut(frozenId, args)
}

func (a *PayGService) InSignature(cb interface{}) bool {
	panic("implement me")
}

func (a *PayGService) OutSignature(cb interface{}) bool {
	panic("implement me")
}

func (a *PayGService) Request(url string, method string, contentType string, sendData interface{}, header map[string][]string) (res []byte, err error) {
	client := &http.Client{}
	var req *http.Request
	if contentType == "JSON" {
		data, _ := json.Marshal(sendData)
		fmt.Println(string(data))
		reader := bytes.NewBuffer(data)
		req, err = http.NewRequest(method, url, reader)
	} else {
		s := sendData.(string)
		reader := strings.NewReader(s)
		req, err = http.NewRequest(method, url, reader)
	}
	//提交请求
	if err != nil {
		logger.Error("PayGService_Create_order | url=%v | err=%+v | req=%+v", url, err, sendData)
		return
	}
	req.Header = header
	//处理返回结果
	response, err := client.Do(req)
	if err != nil {
		logger.Error("PayGService_request_err | url=%v | err=%+v | req=%+v", url, err, sendData)
		return
	}
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		logger.Error("PayGService_response_read_err | url=%v | err=%+v | req=%+v", url, err, sendData)
		return
	}
	defer func() {
		_ = response.Body.Close()
	}()
	return body, nil
}
