package phonepay

import (
	"crypto/sha256"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"funzone_pay/app/validator"
	"funzone_pay/common"
	"funzone_pay/dao"
	"funzone_pay/model"
	"funzone_pay/utils"
	"github.com/wonderivan/logger"
	"math/rand"
	"net/http"
	"net/url"
	"time"
)

func IsSuccess(status string) bool {
	return status == PaymentSuccess
}

func IsFail(status string) bool {
	return status == PaymentError || status == TransactionNotFound || status == PaymentDeclined || status == Timeout
}

type PhonePayService struct {
	PAY_ACCOUNT_ID string
	PAY_CHANNEL    string
	COUNTRY_CODE   int

	MerchantCode string
	SALT         string
	SALT_INDEX   string
	API_HOST     string

	InCallBack  string
	InReturnUrl string
}

func GetPhonePayIns(conf map[string]interface{}) *PhonePayService {
	return &PhonePayService{
		PAY_ACCOUNT_ID: conf["PAY_ACCOUNT_ID"].(string),
		PAY_CHANNEL:    conf["PAY_CHANNEL"].(string),
		COUNTRY_CODE:   conf["COUNTRY_CODE"].(int),

		MerchantCode: conf["MERCHANT_CODE"].(string),
		SALT_INDEX:   conf["SALT_INDEX"].(string),
		SALT:         conf["SALT"].(string),
		API_HOST:     conf["API_HOST"].(string),

		InCallBack:  conf["IN_CALL_BACK"].(string),
		InReturnUrl: conf["IN_RETURN_URL"].(string),
	}
}

func (u PhonePayService) CreateOrder(args validator.PayEntryValidator) (*model.PayEntry, error) {
	orderId := utils.GetOrderId("PIN")

	unix := time.Now().Unix()
	var err error = nil
	isAlarm := true
	defer func() {
		cost := time.Now().Unix() - unix
		if isAlarm || cost > 15 {
			common.PushAlarmEvent(common.Warning, fmt.Sprintf("%v", args.Uid), "PhonePayService/CreateOrder", "", fmt.Sprintf("%v-%v-%v Create Order By PhonePay Error %v, cost:%v", args.AppId, args.Uid, orderId, err, cost))
		}
	}()

	payload := PayPageRequest{
		MerchantId:            u.MerchantCode,
		MerchantTransactionId: orderId,
		MerchantUserId:        fmt.Sprintf("%v00%v", args.Uid, rand.Int31n(100000)+1),
		Amount:                int64(args.Amount * 100),
		RedirectUrl:           u.InReturnUrl,
		RedirectMode:          "POST",
		CallbackUrl:           u.InCallBack,
		PaymentInstrument: PaymentInstrument{
			Type: "PAY_PAGE",
		},
		MobileNumber: args.Phone,
	}

	//json.MarshalIndent()
	payloadRaw, err := json.Marshal(payload)
	if err != nil {
		logger.Error("PhonePayService_CreateOrder_Request_Err | order=%v | err=%v", orderId, err)
		return nil, err
	}

	payloadRawBase64 := base64.StdEncoding.EncodeToString(payloadRaw)
	reqBts, _ := json.Marshal(ReqPhonePay{Request: payloadRawBase64})

	token := XVerify(payloadRawBase64, "/pg/v1/pay", u.SALT, u.SALT_INDEX)
	header := http.Header{
		"Content-Type": {"application/json"},
		"X-Verify":     {token},
	}

	host := fmt.Sprintf("%v/pg/v1/pay", u.API_HOST)
	_, res, err := utils.Post(host,  reqBts, header)
	logger.Debug("PhonePayService_CreateOrder | order=%v||%v|err=%v", orderId, string(res), err)
	if err != nil {
		logger.Error("PhonePayService_CreateOrder_Request_Err | req=%v | err=%v", string(payloadRaw), err)
		return nil, err
	}

	resp := PayPageResponse{}
	err = json.Unmarshal(res, &resp)
	if err != nil {
		logger.Error("PhonePayService_CreateOrder_Unmarshal_Err | req=%v | err=%v", string(payloadRaw), err)
		return nil, err
	}

	if !resp.Success || resp.Data.InstrumentResponse.RedirectInfo.Url == "" {
		logger.Error("PhonePayService_CreateOrder_Fail | req=%v", string(payloadRaw))
		return nil, fmt.Errorf("channel busy, no pay link")
	}

	PaymentOrderId := ""
	//URL, err := url.Parse(resp.Data.InstrumentResponse.RedirectInfo.Url)
	//if err == nil {
	//	PaymentOrderId = URL.Query().Get("token")
	//}

	//请求成功写入流水
	pE := new(model.PayEntry)
	pE.AppId = args.AppId
	pE.Uid = args.Uid
	pE.PayAccountId = u.PAY_ACCOUNT_ID
	pE.PayChannel = model.PhonePay
	pE.CountryCode = u.COUNTRY_CODE
	pE.AppOrderId = args.AppOrderId
	pE.OrderId = orderId
	pE.PaymentOrderId = PaymentOrderId
	pE.Amount = args.Amount
	pE.UserId = args.UserId
	pE.UserName = args.UserName
	pE.Email = args.Email
	pE.Phone = args.Phone
	pE.Status = model.PAYING
	pE.Created = time.Now().Unix()
	pE.PaymentId = ""

	//
	engine, err := dao.GetMysql()
	if engine == nil || err != nil {
		logger.Error("PhonePayService_CreateOrder_GetDB_Err | err=%v", err)
		return nil, err
	}
	_, err = engine.Insert(pE)
	if err != nil {
		logger.Error("PhonePayService_CreateOrder_InsertDB_Err | err=%v", err)
		return nil, err
	}

	isAlarm = false
	pE.PaymentLinkHost = resp.Data.InstrumentResponse.RedirectInfo.Url
	return pE, nil
}

func (u PhonePayService) Inquiry(orderId string, date string) (*model.PayEntry, error) {
	// https://api-preprod.phonepe.com/apis/pg-sandbox/pg/v1/status/{merchantId}/{merchantTransactionId}
	token := XVerify("", fmt.Sprintf("/pg/v1/status/%v/%v", u.MerchantCode, orderId), u.SALT, u.SALT_INDEX)
	header := http.Header{
		"Content-Type":  {"application/json; charset=utf-8"},
		"accept":        {"application/json"},
		"X-VERIFY":      {token},
		"X-MERCHANT-ID": {u.MerchantCode},
	}

	//host := fmt.Sprintf("%v/pg/v1/pay", u.API_HOST)
	host := fmt.Sprintf("%v/pg/v1/status/%v/%v", u.API_HOST, u.MerchantCode, orderId)
	_, res, err := utils.Get(host, header, nil)
	logger.Debug("PhonePayService_Inquiry | order=%v |%v|err=%v", orderId, string(res), err)

	if err != nil {
		logger.Error("PhonePayService_Inquiry_Request_Err | order=%v | err=%v", orderId, err)
		return nil, err
	}

	resp := InquiryResponse{}
	err = json.Unmarshal(res, &resp)
	if err != nil {
		logger.Error("PhonePayService_Inquiry_Unmarshal_Err | order=%v | err=%v", orderId, err)
		return nil, err
	}

	if !resp.Success || (!IsSuccess(resp.Code) && !IsFail(resp.Code)) {
		return nil, fmt.Errorf("no explicity status, maybe %v", resp.Code)
	}

	engine, _ := dao.GetMysql()
	payIn := new(model.PayEntry)
	exist, err := engine.Where("order_id=?", orderId).Get(payIn)
	if err != nil {
		logger.Error("PhonePayService_Inquiry_GetError | err=%v | data=%v", err, orderId)
		return nil, err
	}
	if !exist {
		return nil, errors.New("NotExist")
	}

	if payIn.Status != model.PAYING {
		return nil, fmt.Errorf("no need update, status = %v", payIn.Status)
	}

	if IsSuccess(resp.Code) {
		payIn.Status = model.PAYSUCCESS
		payIn.PaymentId = resp.Data.PaymentInstrument.Utr
	} else if IsFail(resp.Code) {
		payIn.Status = model.PAYFAILD
		payIn.ThirdCode = resp.Code
		payIn.ThirdDesc = resp.Message
	}

	return payIn, nil
}

func CheckReturnResponse(rawBody []byte, params url.Values) (success int, order string, message string) {
	// Form
	// code=PAYMENT_SUCCESS&merchantId=MERCHANTUAT&transactionId=caa0e0c1-250c-4963-8e63-2f74fe7fb446
	// &amount=1000&providerReferenceId=T2306011442566832817587&param1=na&param2=na&param3=na
	// &param4=na&param5=na&param6=na&param7=na&param8=na&param9=na&param10=na&param11=na
	// &param12=na&param13=na&param14=na&param15=na&param16=na&param17=na&param18=na&param19=na
	// &param20=na&checksum=2ec420101ce340d500564d384adb5625accf5354bedcfa4588a9a4d915c2dbd0%23%23%231


	// data=code=PAYMENT_PENDING&merchantId=SOFTUAT&transactionId=PIN1690202211010949f1fc&amount=200000&providerReferenceId=T2307241806516070477647&param1=na&param2=na&param3=na&param4=na&param5=na&param6=na&param7=na&param8=na&param9=na&param10=na&param11=na&param12=na&param13=na&param14=na&param15=na&param16=na&param17=na&param18=na&param19=na&param20=na&checksum=2ec420101ce340d500564d384adb5625accf5354bedcfa4588a9a4d915c2dbd0%23%23%231
	// data=code=PAYMENT_ERROR&merchantId=SOFTUAT&transactionId=PIN1690195324034139ede9&amount=10000&providerReferenceId=T2307241612049370477730&param1=na&param2=na&param3=na&param4=na&param5=na&param6=na&param7=na&param8=na&param9=na&param10=na&param11=na&param12=na&param13=na&param14=na&param15=na&param16=na&param17=na&param18=na&param19=na&param20=na&checksum=2ec420101ce340d500564d384adb5625accf5354bedcfa4588a9a4d915c2dbd0%23%23%231
	// data=code=PAYMENT_PENDING&merchantId=RUMMYSHINEONLINE&transactionId=OM2309050900359205538221&amount=50000&merchantOrderId=PIN16938846353325951269&param1=na&param2=na&param3=na&param4=na&param5=na&param6=na&param7=na&param8=na&param9=na&param10=na&param11=na&param12=na&param13=na&param14=na&param15=na&param16=na&param17=na&param18=na&param19=na&param20=na&checksum=45522a7bb031f6f44511b11a60d985f8ac84407b00ec23c8bd4a44fe0fa936af%23%23%231
	resp, err := url.ParseQuery(string(rawBody))
	if err != nil {
		return -1, "", err.Error()
	}

	order = resp.Get("transactionId")
	merchantOrderId := resp.Get("merchantOrderId")
	if len(merchantOrderId) > 0 {
		order = merchantOrderId
	}
	code := resp.Get("code")
	if IsSuccess(code) {
		return 1,  order,""
	}

	if IsFail(code) {
		return -1, order, ""
	}

	return 0, order, code
}

func (u PhonePayService) InquiryPayout(orderId string, ext string) (*model.PayOut, error) {
	return nil, fmt.Errorf("unsupported")
}

func (u PhonePayService) PayOut(frozenId int64, entryValidator validator.PayOutValidator) (*model.PayOut, error) {
	return nil, fmt.Errorf("unsupported")
}

func (u PhonePayService) PayOutBatch(frozenId int64, entryValidator validator.PayOutValidator) (*model.PayOut, error) {
	return nil, fmt.Errorf("unsupported")
}

func (u PhonePayService) InSignature(cb interface{}) bool {
	return false
}

func (u PhonePayService) OutSignature(cb interface{}) bool {
	return false
}

func XVerify(payload string, path string, saltKey string, saltIndex string) string {
	data := ""
	if payload != "" {
		data = payload + path + saltKey
	} else {
		data = path + saltKey
	}

	return fmt.Sprintf("%x###%s", sha256.Sum256([]byte(data)), saltIndex)
}
