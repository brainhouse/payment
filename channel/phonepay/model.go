package phonepay

type ReqPhonePay struct {
	Request string `json:"request"`
}

type RespPhonePay struct {
	Response string `json:"response"`
}

type PaymentInstrument struct {
	Type string `json:"type"`
}

type PayPageRequest struct {
	MerchantId            string            `json:"merchantId"`
	MerchantTransactionId string            `json:"merchantTransactionId"`
	MerchantUserId        string            `json:"merchantUserId"`
	Amount                int64             `json:"amount"`
	RedirectUrl           string            `json:"redirectUrl"`
	RedirectMode          string            `json:"redirectMode"`
	CallbackUrl           string            `json:"callbackUrl"`
	MobileNumber          string            `json:"mobileNumber"`
	PaymentInstrument     PaymentInstrument `json:"paymentInstrument"`
}

const (
	// PaymentSuccess success
	PaymentSuccess = "PAYMENT_SUCCESS"

	// PaymentPending not sure
	PaymentPending      = "PAYMENT_PENDING"       // Payment is pending. It does not indicate success/failed payment. The merchant needs to call Check Status API to verify the transaction status.
	InternalServerError = "INTERNAL_SERVER_ERROR" // Something went wrong. It does not indicate failed payment. The merchant needs to call Check Status API to verify the transaction status.

	PaymentInitiated    = "PAYMENT_INITIATED"
	BadRequest          = "BAD_REQUEST"           // Invalid request
	AuthorizationFailed = "AUTHORIZATION_FAILED"  // X-VERIFY header is incorrect

	// PaymentError fail
	PaymentError        = "PAYMENT_ERROR"         // Payment failed
	TransactionNotFound = "TRANSACTION_NOT_FOUND" // The transaction id is incorrect
	PaymentDeclined     = "PAYMENT_DECLINED"      // Payment declined by user
	Timeout             = "TIMED_OUT"             // The payment failed due to the timeout.
)

type PayPageResponse struct {
	Success bool   `json:"success"`
	Code    string `json:"code"`
	Message string `json:"message"`
	Data    struct {
		MerchantId            string `json:"merchantId"`
		MerchantTransactionId string `json:"merchantTransactionId"`
		TransactionId         string `json:"transactionId"`
		InstrumentResponse    struct {
			Type         string `json:"type"`
			RedirectInfo struct {
				Url    string `json:"url"`
				Method string `json:"method"`
			} `json:"redirectInfo"`
		} `json:"instrumentResponse"`
	} `json:"data"`
}


type InquiryResponse struct {
	Success bool   `json:"success"` // A boolean to indicate the success/failure of the request.
	Code    string `json:"code"`    // success: PAYMENT_SUCCESS, fail: BAD_REQUEST, AUTHORIZATION_FAILED, INTERNAL_SERVER_ERROR, TRANSACTION_NOT_FOUND, PAYMENT_ERROR, PAYMENT_PENDING, PAYMENT_DECLINED, TIMED_OUT
	Message string `json:"message"`

	Data struct {
		MerchantId            string `json:"merchantId"`
		MerchantTransactionId string `json:"merchantTransactionId"`
		TransactionId         string `json:"transactionId"`
		Amount                int    `json:"amount"`
		State                 string `json:"state"`
		ResponseCode          string `json:"responseCode"`
		PaymentInstrument     struct {
			Type                string `json:"type"`
			Utr                 string `json:"utr"`
			MaskedAccountNumber string `json:"maskedAccountNumber"`
			UpiTransactionId    string `json:"upiTransactionId"`
			VPA                 string `json:"vpa"`
			IFSC                string `json:"ifsc"`
		} `json:"paymentInstrument"`
	} `json:"data"`
}
