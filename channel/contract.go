package channel

import (
	"funzone_pay/app/validator"
	"funzone_pay/model"
)

type PaymentContract interface {
	CreateOrder(validator.PayEntryValidator) (*model.PayEntry, error)
	Inquiry(string, string) (*model.PayEntry, error)
	InquiryPayout(orderId string, ext string) (*model.PayOut, error)
	PayOut(frozenId int64, entryValidator validator.PayOutValidator) (*model.PayOut, error)
	PayOutBatch(frozenId int64, entryValidator validator.PayOutValidator) (*model.PayOut, error)
	InSignature(cb interface{}) bool
	OutSignature(cb interface{}) bool
}

type UPIContract interface {
	DoPay(orderId string, amount float64, vpa string) error
	GetPayStatus(orderId string) *model.PayEntry
	VerifyVPA(vpa string) *model.UPIInfo
}
