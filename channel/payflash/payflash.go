package payflash

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"funzone_pay/app/validator"
	"funzone_pay/dao"
	"funzone_pay/model"
	"funzone_pay/utils"
	"github.com/wonderivan/logger"
	"io/ioutil"
	"net/http"
	"net/url"
	"sort"
	"strconv"
	"strings"
)

const (
	FieldPayHas = "hash"

	FieldAddressLine1    = "address_line_1"
	FieldAddressLine2    = "address_line_2"
	FieldAmount          = "amount"
	FieldApiKey          = "api_key"
	FieldCity            = "city"
	FieldCountry         = "country"
	FieldCurrency        = "currency"
	FieldDescription     = "description"
	FieldEmail           = "email"
	FieldMode            = "mode"
	FieldName            = "name"
	FieldOrderId         = "order_id"
	FieldPaymentOptions  = "payment_options"
	FieldPhone           = "phone"
	FieldReturnURL       = "return_url"
	FieldReturnCancelURL = "return_url_cancel"
	FieldReturnFailURL   = "return_url_failure"
	FieldState           = "state"
	FieldUDF1            = "udf1"
	FieldUDF2            = "udf2"
	FieldUDF3            = "udf3"
	FieldUDF4            = "udf4"
	FieldUDF5            = "udf5"
	FieldZipCode         = "zip_code"

	FieldMerchantReferenceNumber = "merchant_reference_number"
	FieldTransferType            = "transfer_type"
	FieldAccountName             = "account_name"
	FieldAccountNumber           = "account_number"
	FieldIfscCode                = "ifsc_code"
	FieldBankName                = "bank_name"
	FieldBankBranch              = "bank_branch"
	FieldUPIID                   = "upi_id"

	FieldTransactionId   = "transaction_id"
	FieldPaymentMode     = "payment_mode"
	FieldPaymentChannel  = "payment_channel"
	FieldPaymentDateTime = "payment_datetime"
	FieldResponseCode    = "response_code"
	FieldResponseMessage = "response_message"
	FieldErrorDesc       = "error_desc"
	FieldStatus          = "status"
	FieldErrorMessage    = "error_message"
)

const (
	DisbursementStatusProcessing = "PROCESSING"
	DisbursementStatusIncomplete = "INCOMPLETE"
	DisbursementStatusPending    = "PENDING"
	DisbursementStatusHalfSend   = "SENT_TO_BENEFICIARY"
	DisbursementStatusFailure    = "FAILURE"
	DisbursementStatusFailed     = "FAILED"
	DisbursementStatusSuccess    = "SUCCESS"
	DisbursementStatusReject     = "RETURNED_FROM_BENEFICIARY"
)

var formParams = []string{
	FieldAddressLine1,
	FieldAddressLine2,
	FieldAmount,
	FieldApiKey,
	FieldCity,
	FieldCountry,
	FieldCurrency,
	FieldDescription,
	FieldEmail,
	FieldMode,
	FieldName,
	FieldOrderId,
	FieldPaymentOptions,
	FieldPhone,
	FieldReturnURL,
	FieldReturnCancelURL,
	FieldReturnFailURL,
	FieldState,
	FieldUDF1,
	FieldUDF2,
	FieldUDF3,
	FieldUDF4,
	FieldUDF5,
	FieldZipCode,
}

type PayFlashService struct {
	PAY_ACCOUNT_ID string
	PAY_CHANNEL    string
	COUNTRY_CODE   int

	MERCHANT_ID      string
	MERCHANT_API_KEY string
	MERCHANT_SALT    string

	RETURN_URL  string
	COLLECT_URL string
	PAY_OUT_URL string
}

func GetPayFlashIns(conf map[string]interface{}) *PayFlashService {
	return &PayFlashService{
		PAY_ACCOUNT_ID: conf["PAY_ACCOUNT_ID"].(string),
		PAY_CHANNEL:    conf["PAY_CHANNEL"].(string),
		COUNTRY_CODE:   conf["COUNTRY_CODE"].(int),

		MERCHANT_ID:      conf["MERCHANT_ID"].(string),
		MERCHANT_API_KEY: conf["MERCHANT_API_KEY"].(string),
		MERCHANT_SALT:    conf["MERCHANT_SALT"].(string),

		RETURN_URL:  conf["RETURN_URL"].(string),
		COLLECT_URL: conf["COLLECT_URL"].(string),
		PAY_OUT_URL: conf["PAY_OUT_URL"].(string),
	}
}

func CheckReturnResponse(rawBody []byte, params url.Values) (success int, order string, message string) {
	// POST
	// data=
	// order_id=PFIN16626405980244531442&amount=500.00&currency=INR
	// &description=no+description&name=SATISH+KADIMISE&email=8729092354%40gmail.com&phone=8729092354
	// &address_line_1=&address_line_2=&city=Bangalore&state=&country=India&zip_code=560001
	// &udf1=payflash1&udf2=&udf3=&udf4=&udf5=&transaction_id=IOUPII68967949574&payment_mode=UPI
	// &payment_channel=Unified+Payments+Intent+Interface&payment_datetime=2022-09-08+18%3A06%3A46
	// &response_code=1030
	// &response_message=Transaction+incomplete
	// &error_desc=Transaction+incomplete
	// &cardmasked=&hash=B27855304569B6102BC39033ED957DE3A7791D0F4022995C1F2EFD6E38C3D2B9B7E689062BA97DBB7FA8D9C1E4FE927B2927C07AFDAC739E3FADF32458319A6B
	resp, err := url.ParseQuery(string(rawBody))
	if err != nil {
		return -1, "",  err.Error()
	}

	order = resp.Get("order_id")
	code := resp.Get("response_code")
	if code == "0" {
		return 1, order,""
	}

	return -1, order,""
}

func (u *PayFlashService) CreateOrder(args validator.PayEntryValidator) (*model.PayEntry, error) {
	orderId := utils.GetOrderId("PFIN")
	formData := u.genReqData(orderId, args)

	//请求成功写入流水
	pE := new(model.PayEntry)
	pE.AppId = args.AppId
	pE.Uid = args.Uid
	pE.PayAccountId = u.PAY_ACCOUNT_ID
	pE.PayChannel = model.PAYFLASH
	pE.CountryCode = u.COUNTRY_CODE
	pE.AppOrderId = args.AppOrderId
	pE.OrderId = orderId
	pE.PaymentOrderId = ""
	pE.Amount = args.Amount
	pE.UserId = args.UserId
	pE.UserName = args.UserName
	pE.Email = args.Email
	pE.Phone = args.Phone
	pE.Status = model.PAYING
	pE.PayAppId = u.MERCHANT_ID

	engine, err := dao.GetMysql()
	if engine == nil || err != nil {
		logger.Error("PayFlashService_CreateOrder_GetDB_Err | err=%v", err)
		return nil, err
	}
	_, err = engine.Insert(pE)
	if err != nil {
		logger.Error("PayFlashService_CreateOrder_InsertDB_Err | err=%v", err)
		return nil, err
	}

	logger.Debug("PayFlashService_CreateOrder_Info | orderReq=%+v | err=%v", formData, err)

	data, err := json.Marshal(formData)
	if err != nil {
		logger.Error("PayFlashService_CreateOrder_Marshal_Err | err=%v", err)
		return nil, err
	}

	params := url.Values{}
	params.Set("token", base64.URLEncoding.EncodeToString(data))
	pE.PaymentLinkHost = fmt.Sprintf("%v?%v", u.COLLECT_URL, params.Encode())
	return pE, nil
}

func (u *PayFlashService) genReqData(orderId string, args validator.PayEntryValidator) (data map[string]string) {
	amount := fmt.Sprintf("%.2f", args.Amount) // 两位小数

	data = map[string]string{
		FieldApiKey:         u.MERCHANT_API_KEY,
		FieldReturnURL:      u.RETURN_URL,
		FieldMode:           "LIVE", // TEST or LIVE
		FieldOrderId:        orderId,
		FieldAmount:         amount,
		FieldCurrency:       "INR",
		FieldDescription:    "no description",
		FieldName:           args.UserName,
		FieldEmail:          args.Email,
		FieldPaymentOptions: "upi,nb,w,atm,cc,dp",
		FieldPhone:          args.Phone,
		//FieldAddressLine1:   "",
		//FieldAddressLine2:   "",
		FieldCity: "Bangalore",
		//FieldState:          "",
		FieldCountry: "India",
		FieldZipCode: "560001",
		FieldUDF1:    u.PAY_ACCOUNT_ID,
		//FieldUDF2:           "",
		//FieldUDF3:           "",
		//FieldUDF4:           "",
		//FieldUDF5:           "",
	}

	data[FieldPayHas] = u.GetSign(data, u.MERCHANT_SALT)
	return
}

type inquiryResponse struct {
	Data []struct {
		TransactionId   string `json:"transaction_id"`
		BankCode        string `json:"bank_code"`
		PaymentMode     string `json:"payment_mode"`
		PaymentChannel  string `json:"payment_channel"`
		PaymentDatetime string `json:"payment_datetime"`
		ResponseCode    int    `json:"response_code"`
		ResponseMessage string `json:"response_message"`
		OrderId         string `json:"order_id"`
		Amount          string `json:"amount"`
		AmountOrig      string `json:"amount_orig"`
		ErrorDesc       string `json:"error_desc"`
		CustomerPhone   string `json:"customer_phone"`
		CustomerName    string `json:"customer_name"`
		CustomerEmail   string `json:"customer_email"`
		Currency        string `json:"currency"`
		UDF1            string `json:"udf1"`
		UDF2            string `json:"udf2"`
		UDF3            string `json:"udf3"`
		UDF4            string `json:"udf4"`
		UDF5            string `json:"udf5"`
	} `json:"data"`

	Error struct {
		Code    int    `json:"code"`
		Message string `json:"message"`
	} `json:"error"`
}

func (u *PayFlashService) Inquiry(orderId string, s2 string) (*model.PayEntry, error) {
	reqData := map[string]string{
		FieldApiKey:  u.MERCHANT_API_KEY,
		FieldOrderId: orderId,
	}

	reqData[FieldPayHas] = u.GetSign(reqData, u.MERCHANT_SALT)

	//
	reqStatusData := make(url.Values)
	for k, v := range reqData {
		reqStatusData.Add(k, v)
	}

	reqStr := reqStatusData.Encode()
	header := map[string][]string{
		"Content-Type":   {"application/x-www-form-urlencoded"},
		"Content-Length": {strconv.Itoa(len(reqStr))},
	}

	resp, err := u.Request("https://biz.payflash.in/v2/paymentstatus", "POST", "", reqStr, header)
	if err != nil {
		logger.Error("PayFlashService_Inquiry_Request_Error | err=%v", err)
		return nil, err
	}

	logger.Debug("PayFlashService_Inquiry_Info | requestData=%v | response=%v | header=%v", reqStr, string(resp), header)

	res := &inquiryResponse{}
	err = json.Unmarshal(resp, res)
	if err != nil {
		logger.Error("PayFlashService_Inquiry_JsonUnmarshal_Error | err=%v | res=%v", err, res)
		return nil, err
	}

	engine, _ := dao.GetMysql()
	payIn := new(model.PayEntry)
	exist, err := engine.Where("order_id=?", orderId).Get(payIn)
	if err != nil {
		logger.Error("PayFlashService_Inquiry_GetError | err=%v | data=%v", err, orderId)
		return nil, err
	}
	if !exist {
		return nil, errors.New("NotExist")
	}

	// 查询订单错误--订单不存在
	if res.Error.Code != 0 { // Transaction not found
		payIn.Status = model.PAYFAILD
		payIn.ThirdCode = fmt.Sprintf("%v", res.Error.Code)
		payIn.ThirdDesc = fmt.Sprintf("%v-%v", res.Error.Code, res.Error.Message)
		return payIn, nil
	}

	//
	if len(res.Data) < 1 {
		return nil, fmt.Errorf("invalid orderID[%v]", orderId)
	}

	code := res.Data[0].ResponseCode
	if code == 0 {
		payIn.Status = model.PAYSUCCESS
		payIn.PaymentId = res.Data[0].TransactionId
	} else if code == 1006 || code == 1088 || code == 1030 {
		// 1006 WAITING-BANK-RESPONSE Waiting for the response from bank
		// 1088 TRANSACTION-IN-PROCESS We are processing your transaction
		// 1030 TRANSACTION-INCOMPLETE Transaction incomplete
		// 不做处理
	} else {
		payIn.Status = model.PAYFAILD
		payIn.ThirdCode = fmt.Sprintf("%v", res.Data[0].ResponseCode)
		payIn.ThirdDesc = fmt.Sprintf("%v-%v-%v", res.Data[0].ResponseCode, res.Data[0].ResponseMessage, res.Data[0].ErrorDesc)
	}

	return payIn, nil
}

func (u *PayFlashService) InquiryPayout(orderId string, ext string) (*model.PayOut, error) {
	reqData := map[string]string{
		FieldApiKey:                  u.MERCHANT_API_KEY,
		FieldMerchantReferenceNumber: orderId,
	}

	reqData[FieldPayHas] = u.GetSign(reqData, u.MERCHANT_SALT)

	params := url.Values{}
	for k, v := range reqData {
		params.Add(k, v)
	}

	reqStr := params.Encode()
	header := map[string][]string{
		"Content-Type":   {"application/x-www-form-urlencoded"},
		"Content-Length": {strconv.Itoa(len(reqStr))},
	}

	resp, err := u.Request("https://biz.payflash.in/v3/fundtransferstatus", "POST", "", reqStr, header)
	if err != nil {
		logger.Error("PayFlashService_PayoutInquiry_Request_Error | err=%v", err)
		return nil, err
	}

	logger.Debug("PayFlashService_PayoutInquiry_Info | requestData=%v | response=%v | header=%v", reqStr, string(resp), header)

	statusData := &struct {
		Data struct {
			Status                  string `json:"status"`
			MerchantReferenceNumber string `json:"merchant_reference_number"`
			TransactionId           string `json:"transaction_id"`
			ErrorMessage            string `json:"error_message"`
		} `json:"data"`
		Error struct {
			Code    int    `json:"code"`
			Message string `json:"message"`
		} `json:"error"`
	}{}
	err = json.Unmarshal(resp, statusData)
	if err != nil {
		logger.Error("PayFlashService_PayOut_Inquiry_JsonError | err=%v | data=%v", err, string(resp))
		return nil, err
	}

	if statusData.Error.Code != 0 {
		return nil, fmt.Errorf("%v-%v", statusData.Error.Code, statusData.Error.Message)
	}

	engine, _ := dao.GetMysql()
	payOut := new(model.PayOut)
	exist, err := engine.Where("order_id=?", orderId).Get(payOut)
	if err != nil {
		logger.Error("PayFlashService_PayOut_Inquiry_GetError | err=%v | data=%v", err, orderId)
		return nil, err
	}
	if !exist {
		return nil, errors.New("NotExist")
	}

	status := statusData.Data.Status
	if status == "SUCCESS" {
		payOut.Status = model.PAY_OUT_SUCCESS
		payOut.PaymentId = statusData.Data.TransactionId
	} else if status == "FAILED" || status == "FAILURE" || status == "RETURNED_FROM_BENEFICIARY" {
		payOut.Status = model.PAY_OUT_FAILD
		payOut.ThirdDesc = fmt.Sprintf("%v-%v", status, statusData.Data.ErrorMessage)
	} else {
		//if status =="PROCESSING" || status =="INCOMPLETE" || status =="PENDING" || status == "SENT_TO_BENEFICIARY" {
		//	return nil, fmt.Errorf("payflash platform pending -->%v", status)
		//}
		// 不做处理
	}

	return payOut, nil
}

type payoutResponse struct {
	Data struct {
		Status                  string `json:"status"`
		MerchantReferenceNumber string `json:"merchant_reference_number"`
		TransactionId           string `json:"transaction_id"`
	}
	Error struct {
		Code    int    `json:"code"`
		Message string `json:"message"`
	}
}

func (u *PayFlashService) PayOut(frozenId int64, outValidator validator.PayOutValidator) (payOut *model.PayOut, err error) {
	transId := utils.GetOrderId("PFOUT")

	payOut = new(model.PayOut)
	payOut.Uid = outValidator.Uid
	payOut.AppId = outValidator.AppId
	payOut.PayAccountId = u.PAY_ACCOUNT_ID
	payOut.PayChannel = model.PAYFLASH
	payOut.CountryCode = u.COUNTRY_CODE
	payOut.PayType = outValidator.PayType
	payOut.AppOrderId = outValidator.AppOrderId
	payOut.OrderId = transId
	payOut.PaymentId = transId
	payOut.FrozenId = frozenId
	payOut.Amount = outValidator.Amount
	payOut.BankCard = outValidator.BankCard
	payOut.UserId = outValidator.UserId
	payOut.UserName = outValidator.UserName
	payOut.Email = outValidator.Email
	payOut.Paytm = outValidator.PayTm
	payOut.IFSC = outValidator.IFSC
	payOut.Phone = outValidator.Phone
	engine, err := dao.GetMysql()
	if engine == nil || err != nil {
		logger.Error("PayFlashService_PayOut_GetDB_Err | err=%v", err)
		return nil, err
	}
	_, err = engine.Insert(payOut)
	if err != nil {
		logger.Error("PayFlashService_PayOut_Insert_Error | data=%+v | err=%v", payOut, err)
		return nil, err
	}

	//发起支付
	requestUrl := fmt.Sprintf("%s", u.PAY_OUT_URL)

	//
	reqPayoutData := make(url.Values)
	reqRawData := u.genPayoutReqData(transId, outValidator)
	for k, v := range reqRawData {
		reqPayoutData.Add(k, v)
	}

	reqStr := reqPayoutData.Encode()
	header := map[string][]string{
		"Content-Type":   {"application/x-www-form-urlencoded"},
		"Content-Length": {strconv.Itoa(len(reqStr))},
	}

	data, err := u.Request(requestUrl, "POST", "", reqStr, header)
	if err != nil {
		logger.Error("PayFlashService_PayOut_Request_Error | err=%v | reqStr=%v", err, reqStr)
		return nil, err
	}

	logger.Debug("PayFlashService_PayOut_Info | requestUrl=%v | requestData=%v | response=%v | header=%v", requestUrl, reqStr, string(data), header)

	res := &payoutResponse{}
	err = json.Unmarshal(data, res)
	if err != nil {
		logger.Error("PayFlashService_PayOut_JsonUnmarshal_Error | err=%v | req = %v | res=%v", err, reqStr, res)
		return nil, err
	}

	var (
		status      model.PayOutStatus
		referenceId string
		thirdDesc   string
		thirdCode   string
	)

	if res.Error.Code != 0 || res.Data.Status == DisbursementStatusFailed ||
		res.Data.Status == DisbursementStatusFailure || res.Data.Status == DisbursementStatusReject {
		status = model.PAY_OUT_FAILD
		referenceId = payOut.PaymentId
		thirdDesc = res.Error.Message
		thirdCode = fmt.Sprint(res.Error.Code)
	} else {
		status = model.PAY_OUT_ING
		referenceId = res.Data.TransactionId
	}

	_, err = engine.Where("id=?", payOut.Id).Update(&model.PayOut{
		Status:    status,
		PaymentId: referenceId,
		ThirdCode: thirdCode,
		ThirdDesc: thirdDesc,
	})
	if err != nil {
		logger.Error("PayFlashService_PayOut_Insert_Error | data=%v | err=%v", payOut, err)
		return nil, err
	}

	payOut.Status = status
	payOut.PaymentId = referenceId
	payOut.ThirdDesc = thirdDesc
	payOut.ThirdCode = thirdCode

	return payOut, nil
}

func (u *PayFlashService) PayOutBatch(frozenId int64, outValidator validator.PayOutValidator) (*model.PayOut, error) {
	return u.PayOut(frozenId, outValidator)
}

func (u *PayFlashService) genPayoutReqData(orderId string, args validator.PayOutValidator) (data map[string]string) {
	amount := fmt.Sprintf("%.2f", args.Amount) // 两位小数

	data = map[string]string{
		FieldApiKey:                  u.MERCHANT_API_KEY,
		FieldMerchantReferenceNumber: orderId,
		FieldAmount:                  amount,
	}

	if args.PayType == model.PT_UPI {
		data[FieldUPIID] = args.VPA
	} else {
		data[FieldAccountName] = args.UserName
		data[FieldAccountNumber] = args.BankCard
		data[FieldIfscCode] = args.IFSC
	}

	data[FieldPayHas] = u.GetSign(data, u.MERCHANT_SALT)
	return
}

func (u *PayFlashService) InSignature(cb interface{}) bool {
	data, ok := cb.(map[string]string)
	if !ok {
		return false
	}

	sign := data[FieldPayHas]
	hash := u.GetSign(data, u.MERCHANT_SALT)
	if hash != sign {
		logger.Error("PayFlashService_Signature_Error | data=%v | hash=%v ", data, hash)
		return false
	}

	return true
}

func (u *PayFlashService) OutSignature(cb interface{}) bool {
	return u.InSignature(cb)
}

func (u *PayFlashService) Request(url string, method string, contentType string, sendData interface{}, header map[string][]string) (res []byte, err error) {
	client := &http.Client{}
	var req *http.Request
	if contentType == "JSON" {
		data, _ := json.Marshal(sendData)
		fmt.Println(string(data))
		reader := bytes.NewBuffer(data)
		req, err = http.NewRequest(method, url, reader)
	} else {
		s := sendData.(string)
		reader := strings.NewReader(s)
		req, err = http.NewRequest(method, url, reader)
	}
	//提交请求
	if err != nil {
		logger.Error("PayFlashService_Create_order | url=%v | err=%+v | req=%+v", url, err, sendData)
		return
	}
	req.Header = http.Header(header)
	//处理返回结果
	response, err := client.Do(req)
	if err != nil {
		logger.Error("PayFlashService_request_err | url=%v | err=%+v | req=%+v", url, err, sendData)
		return
	}
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		logger.Error("PayFlashService_response_read_err | url=%v | err=%+v | req=%+v", url, err, sendData)
		return
	}
	defer func() {
		_ = response.Body.Close()
	}()
	return body, nil
}

func (u *PayFlashService) GetSign(data map[string]string, salt string) string {
	param := make([]string, 0, len(data))
	for k, _ := range data {
		if k == FieldPayHas {
			continue
		}

		param = append(param, k)
	}

	sort.Strings(param)

	str := salt
	for _, v := range param {
		d := strings.TrimSpace(data[v])
		if len(d) < 1 {
			continue
		}

		str = str + fmt.Sprintf("|%v", d)
	}

	hash := strings.ToUpper(utils.GetHashSha512(str))
	return hash
}
