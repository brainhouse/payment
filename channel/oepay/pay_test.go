package oepay

import (
	"fmt"
	"funzone_pay/app/validator"
	"testing"
)

func TestGetOEPayService(t *testing.T) {
	conf := make(map[string]interface{})
	conf["PAY_ACCOUNT_ID"] = "oepay1"
	conf["PAY_CHANNEL"] = "oepay"
	conf["COUNTRY_CODE"] = 366
	conf["HOST"] = "https://openapi.oepay.co.in"
	conf["SK"] = "MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBANzBlmAYnv30W4UPNGcQivwjXPTYa0musTnf9YswNPH4F+QKMLuG4nbFD9TDpZwLw4JWwsMNThM5AAmSi5dN/2A8uqDlDsvkZoiZnxWhzOgfmHry6T4WPRHrdpA+3umJ7wW37vpqdOBSA8fW9ROxPQYuVUjh+GE5NGyOJAxkXZqzAgMBAAECgYAp+i3MDCJ/i+2zOZbvWjfzhg3xCQ4trqhIpwub8gi/UjvIwx9NKXL6vrIp9SISsDH3IORcDrWf9iDJsTIWDWDX42UYil+A2fmka71japS6Q6C8fi1FtJcQeZ3ljsdBjSm/GsysX44muyf2t/R9nm/pF08T1pGI0zqpRj152WGSsQJBAPvUBQ05put67qnYcEjVgS4go/8HIIxx8UsTtt1GWC4v4Wz/m/X7t5UUdfG3btw96wa53Xuy/m10Z8Tt80YVjn8CQQDgactOm669n+owKPA0cGE5ojyeePtGuiMza90bKORsZoQh79aAFutUkJ8thn8DzUef5nVEY44AY+/ldlpidAHNAkANDZdZcSSSziNy0FI5pD926aA2huMMBJfyQUpYO940Pq0bnkAyRuLkKlRWvCBV8CuvhJDTaZrAh6kdTwwuFM33AkB9cIV1Pi6RzkagfvGtzjd6eMY7qtKCOV+dyahBGz17nl4zfhW/aZBHp8NiNjDfViq4JaDdBm0EqNgawmpJaCDtAkEA8DArdkqZXy+wfhUKIy3q1kFHil90dGDzZAI1D1PmN4mo9L8y06Ib3Slb6hOpV8VZAzHUxKGOt8K6lK5BBbTNNg=="
	conf["CODE"] = "63d5ffd1e4b0632463fbda4d"

	args := validator.PayEntryValidator{
		UserName: "NAMEABDURRAHAMAN",
		Email:    "john@gmail.com",
		Phone:    "7203964072",
		Amount:   50,
	}
	instance := GetOEPayIns(conf)
	//_, beneId, _ := instance.AddBeneficiary(args)

	payOut, err := instance.CreateOrder(args)
	fmt.Println(payOut)
	fmt.Println(err)

	//payEntry, err := instance.Inquiry("45674280", "")
	//fmt.Println(payEntry)
	//fmt.Println(err)
}

func TestGetOEPayOutService(t *testing.T) {
	conf := make(map[string]interface{})
	conf["PAY_ACCOUNT_ID"] = "oepay1"
	conf["PAY_CHANNEL"] = "oepay"
	conf["COUNTRY_CODE"] = 366
	conf["HOST"] = "https://openapi.oepay.co.in"
	conf["SK"] = "MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBAIFKE1PluacsrwXBhSO4b+ahJe/Nqa4jOsIxEWVXAmk6jL1Xtw0kC1FWj6NxaauswHJoBSauWA3G0Q9HETgpGbcKJh7tnLaQwEVNus3OtyXDgfTpYp1iOYzNtVtuyNOmwtMkq0g1VDkSBA1eFrCMZgT2ZwUvFK9jIaeA/kJcmbzJAgMBAAECgYAswxrB9005pezKz19rKJYFgVGDHu9aTPQw+5AMR/2btRu5Cj9kG1CXzqJYchGl0F+ovH7cD4yI6IbFBVPe38v45im3/dhKmyJYG99z3ZkBfkaD0C3AG4qaqYQVLWTePku6D352hx8okYCIor4+FvEHrszzYOgfV0CRPlolOwk5qQJBANDs9I8yf19ygQFwhXDIhB4+645SL8QshD0WoRKfiCj+qesLio+fJt02lTv9gQp8aX7UhbAZQKRSiX2fY9WSMRcCQQCea55+RgQoMwkp+Ub6hgH70jXkJslxQFgPYb/fLFhipLlVgihoMtoaKdlOZ/kikA/zFgE/FWy4t7BtV75QWW0fAkBGPTOnJNH/s849PqgOF8ubI4or6eOXL2Qx6XazfSDYr21O0S1/RYFaY6xeY8bLULJi6m4bmxHJbNo+eqSku6Z3AkAiqUNFN4aDUR9JKrxH5fgbI9QctgvPFpftzaJQbwBrnMTcG51BpxanMM7V49PTpxeGRSNCrSpbg/06/z6pj9dbAkEAuaq+0WOeVBVTZfpOsl78cXKssuET0jvzrbTh9dNmyZgsFiSDtInCSLN20gdwxNUMd0yAgNhGfnsbUoHEwFpzBg=="
	conf["CODE"] = "63d5ffd1e4b0632463fbda4d"

	//args := validator.PayOutValidator{
	//	UserName: "NAMEABDURRAHAMAN",
	//	BankCard: "19110100017165",
	//	IFSC:     "BARB0KASHIA",
	//	Phone:    "7203964072",
	//	Email:    "NAMEABDURRAHAMAN@gmail.com",
	//	Amount:   100,
	//	PayType:  model.PT_BANK,
	//}
	instance := GetOEPayIns(conf)
	//_, beneId, _ := instance.AddBeneficiary(args)

	payout, err := instance.InquiryPayout("OEOUT16788840840600081a1c", "")

	//payOut, err := instance.PayOut(args)
	fmt.Println(payout)
	fmt.Println(err)

	//payEntry, err := instance.Inquiry("45674280", "")
	//fmt.Println(payEntry)
	//fmt.Println(err)
}
