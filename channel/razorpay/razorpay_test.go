package razorpay

import (
	"fmt"
	"funzone_pay/app/validator"
	"funzone_pay/boostrap"
	"funzone_pay/model"
	"testing"
	"time"
)

func TestRzPayService_Payment(t *testing.T) {
	boostrap.InitConf()
	payEntry, err := GetRzPay().CreateOrder(validator.PayEntryValidator{
		Amount:     50000,
		PayChannel: model.RAZORPAY,
	})
	fmt.Println(payEntry)
	fmt.Println(err)
}

func TestRzPayService_PayOut(t *testing.T) {
	boostrap.InitConf()
	pO, err := GetRzPay().PayOut(validator.PayOutValidator{
		AppId:      "rummy",
		AppOrderId: fmt.Sprintf("%v%v", "test", time.Now().Unix()),
		PayType:    model.PT_BANK,
		PayChannel: model.PAYTM,
		Amount:     1,
		UserId:     "11",
		UserName:   "Thaneesh V",
		BankCard:   "1769155000016710",
		Phone:      "9999999999",
		Email:      "test@cashfree.com",
		IFSC:       "KVBL0001769",
		Address:    "ABC Street",
		PayTm:      "9999999999",
	})
	fmt.Println(pO)
	fmt.Println(err)
}
