package razorpay

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"funzone_pay/app/validator"
	"funzone_pay/dao"
	"funzone_pay/model"
	"funzone_pay/utils"
	"github.com/spf13/viper"
	"github.com/wonderivan/logger"
	"io/ioutil"
	"net/http"
	"time"
)

type RzPayService struct {
	PAY_ACCOUNT_ID string
	PAY_CHANNEL    string
	COUNTRY_CODE   int
	COLLECT_HOST   string
	Host           string
	SKID           string
	SK             string
	ACCOUNT        string
}

func GetRzPay() (rz *RzPayService) {
	return &RzPayService{
		Host:    viper.GetString("razorpay.HOST"),
		SKID:    viper.GetString("razorpay.SKID"),
		SK:      viper.GetString("razorpay.SK"),
		ACCOUNT: viper.GetString("razorpay.PAYOUTS_ACCOUNT"),
	}
}

func GetRzIns(conf map[string]interface{}) *RzPayService {
	return &RzPayService{
		PAY_ACCOUNT_ID: conf["PAY_ACCOUNT_ID"].(string),
		PAY_CHANNEL:    conf["PAY_CHANNEL"].(string),
		COUNTRY_CODE:   conf["COUNTRY_CODE"].(int),
		COLLECT_HOST:   conf["COLLECT_HOST"].(string),
		Host:           conf["HOST"].(string),
		SKID:           conf["SKID"].(string),
		SK:             conf["SK"].(string),
		ACCOUNT:        conf["PAYOUTS_ACCOUNT"].(string),
	}
}

/*
*
创建收款支付单
*/
func (rz *RzPayService) CreateOrder(args validator.PayEntryValidator) (*model.PayEntry, error) {
	orderId := "RZ" + utils.GetOrderId("IN")
	orderReq := OrderRequest{
		Amount:         args.Amount * 100, //razorpay需要乘以100
		Currency:       "INR",
		Receipt:        orderId,
		PaymentCapture: 1, //1自动收钱
	}
	requestUrl := fmt.Sprintf("%s%s", rz.Host, "orders")
	data, err := rz.Request(requestUrl, "POST", "JSON", orderReq)
	logger.Debug("RzPayService_CreateOrder_Info | orderReq=%+v | err=%v", orderReq, err)
	razor := new(RazorCreateOrderResponse)
	err = json.Unmarshal(data, razor)
	if err != nil {
		logger.Error("RzPayService_CreateOrder_JsonUnmarshal_Err | err=%v", err)
		return nil, err
	}
	//请求成功写入流水
	pE := new(model.PayEntry)
	pE.AppId = args.AppId
	pE.Uid = args.Uid
	pE.PayAccountId = rz.PAY_ACCOUNT_ID
	pE.CountryCode = rz.COUNTRY_CODE
	pE.Amount = args.Amount
	pE.UserId = args.UserId
	pE.UserName = args.UserName
	pE.Email = args.Email
	pE.Phone = args.Phone
	pE.PayChannel = model.RAZORPAY
	pE.AppOrderId = args.AppOrderId
	pE.OrderId = orderReq.Receipt
	pE.PaymentOrderId = razor.Id
	pE.ThirdCode = razor.Status
	pE.Status = model.PAYING
	pE.ThirdDesc = razor.Status
	pE.PayAppId = rz.SKID
	pE.FinishTime = time.Now().Unix()
	engine, err := dao.GetMysql()
	if engine == nil || err != nil {
		logger.Error("RzPayService_CreateOrder_GetDB_Err | err=%v", err)
		return nil, err
	}
	_, err = engine.Insert(pE)
	if err != nil {
		logger.Error("RzPayService_CreateOrder_InsertDB_Err | err=%v", err)
		return nil, err
	}
	pE.PaymentLinkHost = rz.COLLECT_HOST
	logger.Debug("RzPayService_CreateOrder_Request | req=%+v | res=%+v", orderReq, razor)
	return pE, err
}

/*
*

  - 查询订单状态
    *

    {
    "id": "order_EPhIvAjVwIqqpL",
    "entity": "order",
    "amount": 50000,
    "amount_paid": 0,
    "amount_due": 50000,
    "currency": "INR",
    "receipt": "order_test1123213123",
    "offer_id": null,
    "status": "attempted",
    "attempts": 1,
    "notes": [],
    "created_at": 1583664618
    }

  - param: string orderId

  - return: interface{}

  - return: error
*/
func (rz *RzPayService) Inquiry(paymentId string, orderId string) (payEntry *model.PayEntry, err error) {
	requestUrl := fmt.Sprintf("%s%s/%s", rz.Host, "payments", paymentId)
	data, err := rz.Request(requestUrl, "GET", "", nil)
	logger.Debug("RzPayService_Inquiry_Info | orderReq=%+v | err=%v | data=%v", paymentId, err, string(data))
	if err != nil {
		return nil, err
	}
	paymentEntity := new(model.RazorPaymentEntity)
	err = json.Unmarshal(data, paymentEntity)
	if err != nil {
		logger.Error("PayEntryService_Inquiry_JsonUnmarshal | err=%v | paymentId=%+v", err, paymentId)
		return nil, err
	}
	if paymentEntity.Status == model.Captured {
		return nil, nil
	}
	if paymentEntity.Status == model.Failed {
		return nil, nil
	}
	return nil, err
}

func (ps *RzPayService) InquiryPayout(orderId string, ext string) (*model.PayOut, error) {
	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("PayUService_InquiryPayout_Engine_Error | data=%+v | err=%v", orderId, err)
		return nil, err
	}
	payOut := new(model.PayOut)
	exist, err := engine.Where("order_id=?", orderId).Get(payOut)
	if err != nil {
		logger.Error("RzPayService_InquiryPayout_GetError | err=%v | data=%v", err, orderId)
		return nil, err
	}
	if !exist {
		return nil, errors.New("NotExist")
	}
	requestUrl := fmt.Sprintf("%spayouts/%v", ps.Host, payOut.PaymentId)
	//header := map[string]string{}
	data, err := ps.Request(requestUrl, "GET", "", nil)
	if err != nil {
		logger.Error("RzPayService_InquiryPayout_Request_Error | err=%v", err)
		return nil, err
	}
	logger.Debug("RzPayService_InquiryPayout_Info | data=%v | orderId=%v", string(data), orderId)
	responseData := new(RzInquiryPayout)
	err = json.Unmarshal(data, responseData)
	if err != nil {
		logger.Error("PRzPayServiceInquiryPayout_JsonError | err=%v | data=%v", err, string(data))
		return nil, err
	}
	if responseData.Error.Code == "BAD_REQUEST_ERROR" && responseData.Error.Description == "The id provided does not exist" {
		payOut.Status = model.PAY_OUT_FAILD
	} else if responseData.Status == "rejected" || responseData.Status == "cancelled" || responseData.Status == "reversed" {
		payOut.Status = model.PAY_OUT_FAILD
		payOut.ThirdDesc = responseData.StatusDetails.Description
	} else if responseData.Status == "processed" {
		payOut.Status = model.PAY_OUT_SUCCESS
	}
	return payOut, nil

}

/*
*

  - Create a Contact
    *

    {
    "name":"Gaurav Kumar",
    "email":"gaurav.kumar@example.com",
    "contact":"9123456789",
    "type":"employee",
    "reference_id":"Acme Contact ID 12345",
    "notes":{
    "notes_key_1":"Tea, Earl Grey, Hot",
    "notes_key_2":"Tea, Earl Grey… decaf."
    }
    }
*/
func (rz *RzPayService) createContacts(outValidator validator.PayOutValidator) (*RazorCreateContactsResponse, error) {
	requestUrl := fmt.Sprintf("%s%s", rz.Host, "contacts")
	//utils.GetOrderId("RZ")
	rData := struct {
		Name    string `json:"name"`
		Email   string `json:"email"`
		Contact string `json:"contact"`
		Type    string `json:"type"`
	}{
		Name:    outValidator.UserName,
		Email:   outValidator.Email,
		Contact: outValidator.Phone,
		Type:    "customer",
	}

	if outValidator.PayType == model.PT_PayTm {
		rData.Contact = outValidator.PayTm
	}

	data, err := rz.Request(requestUrl, "POST", "JSON", rData)
	logger.Debug("RzPayService_CreateContacts_Info | data=%+v | req=%+v", string(data), rData)
	if err != nil {
		logger.Error("RzPayService_Authorize_Request_Error | err=%v", err)
		return nil, err
	}

	razor := new(RazorCreateContactsResponse)
	err = json.Unmarshal(data, razor)
	if err != nil {
		logger.Error("RzPayService_CreateContacts_JsonUnmarshal_Err | err=%v", err)
		return nil, err
	}
	if razor.Id == "" {
		logger.Error("RzPayService_ContactId_IsNil | contact_id=%v", razor.Id)
		return nil, fmt.Errorf("invalid empty id")
	}
	return razor, nil
}

/*
*

	{
	  "contact_id":"cont_00000000000001",
	  "account_type":"bank_account",
	  "bank_account":{
	    "name":"Gaurav Kumar",
	    "ifsc":"HDFC0000053",
	    "account_number":"765432123456789"
	  }
	}
*/
func (rz *RzPayService) fundAccounts(outValidator validator.PayOutValidator, contactId string) (*RazorFundAccountsResponse, error) {
	requestUrl := fmt.Sprintf("%s%s", rz.Host, "fund_accounts")

	rData := RZFundsAccount{
		ContactId: contactId,
	}
	var (
		data []byte
		err  error
	)
	if outValidator.PayType == model.PT_BANK {
		rData.AccountType = "bank_account"
		rData.BankAccount = RazorFundAccountsBankAccount{
			Name:          outValidator.UserName,
			IFSC:          outValidator.IFSC,
			AccountNumber: outValidator.BankCard,
		}
		//account_type=vpa  vpa.address = sss@exmpleupi
		data, err = rz.Request(requestUrl, "POST", "JSON", rData)
		logger.Debug("RzPayService_BankCardFundAccounts_Info | data=%v | req=%v", string(data), rData)
	} else if outValidator.PayType == model.PT_UPI {
		vpaData := &RZVPAFundsAccount{
			ContactId:   contactId,
			AccountType: "vpa",
			VPA: RazorFundAccountsVPA{
				Address: outValidator.VPA,
			},
		}
		data, err = rz.Request(requestUrl, "POST", "JSON", vpaData)
		logger.Debug("RzPayService_VPAFundAccounts_Info | data=%v | req=%v", string(data), vpaData)
	}
	if err != nil {
		logger.Error("RzPayService_Authorize_Request_Error | err=%v", err)
		return nil, err
	}

	razor := new(RazorFundAccountsResponse)
	err = json.Unmarshal(data, razor)
	if err != nil {
		logger.Error("RzPayService_FundAccounts_JsonUnmarshal_Err | err=%v", err)
		return nil, err
	}

	return razor, nil
}

func (ps *RzPayService) InSignature(data interface{}) bool {
	return true
}

func (ps *RzPayService) OutSignature(data interface{}) bool {
	return true
}

/*
*
付钱
*/
func (rz *RzPayService) PayOut(frozenId int64, outValidator validator.PayOutValidator) (payOut *model.PayOut, err error) {
	// created in contacts
	var beneId string
	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("RzPayService_PayOut_Engine_Error | data=%+v | err=%v", payOut, err)
		return nil, err
	}
	var exist bool
	payHis := new(model.PayOut)
	benSession := engine.Where("pay_channel='razorpay'")
	if outValidator.PayType == model.PT_BANK {
		// check bank number are register
		exist, err = benSession.Where("bank_card=?", outValidator.BankCard).OrderBy("id desc").Get(payHis)
		if err != nil {
			logger.Error("RzPayService_PayOut_BankCard_Engine_Error | data=%+v | err=%v", payOut, err)
			return nil, err
		}

	} else if outValidator.PayType == model.PT_UPI {
		exist, err = benSession.Where("vpa=?", outValidator.VPA).OrderBy("id desc").Get(payHis)
		if err != nil {
			logger.Error("RzPayService_PayOut_VPA_Engine_Error | data=%+v | err=%v", payOut, err)
			return nil, err
		}
	}
	if exist && payHis.BeneId != "" {
		beneId = payHis.BeneId
	}

	if beneId == "" {
		// reate a Contact
		contact, err := rz.createContacts(outValidator)
		if err != nil {
			err = errors.New("CreateContactsError")
			logger.Error("RzPayService_PayOut_CreateContactsError | data=%+v | err=%v", outValidator, err)
			return nil, err
		}
		// Create a Fund Account for a Contact's Card
		// fund constacts
		far, err := rz.fundAccounts(outValidator, contact.Id)
		if err != nil {
			err = errors.New("FundAccountsError")
			logger.Error("RzPayService_PayOut_FundAccountsError | data=%+v | err=%v", outValidator, err)
			return nil, err
		}
		if far != nil && far.Id != "" {
			beneId = far.Id
		}
	}
	// insert db records
	transId := utils.GetOrderId("RZOUT")
	payOut = new(model.PayOut)
	payOut.Uid = outValidator.Uid
	payOut.AppId = outValidator.AppId
	payOut.PayAccountId = rz.PAY_ACCOUNT_ID
	payOut.PayChannel = model.RAZORPAY
	payOut.CountryCode = rz.COUNTRY_CODE
	payOut.PayType = outValidator.PayType
	payOut.AppOrderId = outValidator.AppOrderId
	payOut.FrozenId = frozenId
	payOut.OrderId = transId
	payOut.PaymentId = transId
	payOut.Amount = outValidator.Amount
	payOut.BankCard = outValidator.BankCard
	payOut.UserId = outValidator.UserId
	payOut.UserName = outValidator.UserName
	payOut.Email = outValidator.Email
	payOut.BeneId = beneId
	payOut.Paytm = outValidator.PayTm
	payOut.IFSC = outValidator.IFSC
	payOut.VPA = outValidator.VPA
	payOut.Phone = outValidator.Phone
	_, err = engine.Insert(payOut)
	if err != nil {
		logger.Error("RzPayService_PayOut_Insert_Error | data=%+v | err=%v", payOut, err)
		return nil, err
	}
	/*
		transferMode := string(outValidator.PayType)
		if transferMode == "bank" {
			transferMode = "banktransfer" //做下转换 banktransfer, upi, paytm, amazonpay and card.
		}
	*/
	mode := "IMPS"
	if outValidator.PayType == model.PT_UPI {
		mode = "UPI"
	}
	reqData := struct {
		AccountNumber     string        `json:"account_number"`
		FundAccountId     string        `json:"fund_account_id"`
		Amount            float64       `json:"amount"`
		Currency          string        `json:"currency"`
		Mode              string        `json:"mode"`
		Purpose           string        `json:"purpose"`
		QueueIfLowBalance bool          `json:"queue_if_low_balance"`
		ReferenceId       string        `json:"reference_id"`
		Narration         string        `json:"narration"`
		Notes             []interface{} `json:"notes"`
	}{
		AccountNumber:     rz.ACCOUNT,
		FundAccountId:     beneId,
		Amount:            outValidator.Amount * 100,
		Currency:          "INR",
		Mode:              mode,
		Purpose:           "payout",
		QueueIfLowBalance: true,
		ReferenceId:       transId,
		Narration:         "",
		Notes:             []interface{}{},
	}
	// payouts
	requestUrl := fmt.Sprintf("%s%s", rz.Host, "payouts")
	data, err := rz.Request(requestUrl, "POST", "JSON", reqData)
	logger.Debug("RzPayService_PayOut_Info | data=%v | reqData=%v", string(data), reqData)
	if err != nil {
		logger.Error("RzPayService_PayOut_Request_Error | err=%v", err)
		return nil, err
	}

	res := &RazorTransfersResponse{}
	err = json.Unmarshal(data, res)
	if err != nil {
		logger.Error("RzPayService_PayOut_JsonUnmarshal_Error | err=%v", err)
		return nil, err
	}
	var (
		status      model.PayOutStatus
		thirdDesc   string
		referenceId string
	)

	payOut.ThirdCode = res.Status
	if res.Status == "rejected" || res.Status == "cancelled" || res.Status == "reversed" {
		status = model.PAY_OUT_FAILD
	} else {
		status = model.PAY_OUT_ING
		referenceId = res.Id
	}
	_, err = engine.Where("id=?", payOut.Id).Update(&model.PayOut{
		Status:     status,
		ThirdCode:  res.Status,
		PaymentId:  referenceId,
		FinishTime: time.Now().Unix(),
		ThirdDesc:  thirdDesc,
	})
	if err != nil {
		logger.Error("RzPayService_PayOut_Update_Error | data=%+v | err=%v", payOut, err)
		return nil, err
	}
	if res.FailureReason != "" {
		err = errors.New(res.FailureReason)
		logger.Error("RzPayService_PayOut_Response_Error | data=%+v", payOut)
		return nil, err
	}
	return payOut, nil
}

func (rz *RzPayService) PayOutBatch(frozenId int64, outValidator validator.PayOutValidator) (payOut *model.PayOut, err error) {
	return rz.PayOut(frozenId, outValidator)
}

/*
*
统一请求
*/
func (rz *RzPayService) Request(url string, method string, contentType string, sendData interface{}) (res []byte, err error) {
	client := &http.Client{}
	var data []byte
	if contentType == "JSON" {
		data, _ = json.Marshal(sendData)
	} else if sendData != nil {
		data = sendData.([]byte)
	}
	//提交请求
	req, err := http.NewRequest(method, url, bytes.NewBuffer(data))
	if err != nil {
		logger.Error("RzPayService_create_order | err=%+v | req=%+v", err, sendData)
		return
	}
	input := fmt.Sprintf("%s:%s", rz.SKID, rz.SK)
	authKey := base64.StdEncoding.EncodeToString([]byte(input))
	header := map[string][]string{
		"Content-Type":  {"application/json;charset=utf-8"},
		"Authorization": {"Basic " + authKey},
	}
	req.Header = http.Header(header)

	//处理返回结果
	response, err := client.Do(req)
	if err != nil {
		logger.Error("RzPayService_request_err | err=%+v | req=%+v", err, sendData)
		return
	}
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		logger.Error("RzPayService_response_read_err | err=%+v | req=%+v", err, sendData)
		return
	}
	defer func() {
		_ = response.Body.Close()
	}()
	return body, nil
}
