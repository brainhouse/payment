package razorpay

type OrderRequest struct {
	Amount         float64 `json:"amount"`
	Currency       string  `json:"currency"`
	Receipt        string  `json:"receipt"`
	PaymentCapture int     `json:"payment_capture"`
}

/*
{
	"id": "order_EPkS6s6eERgWOR",
	"entity": "order",
	"amount": 50000,
	"amount_paid": 0,
	"amount_due": 50000,
	"currency": "INR",
	"receipt": "RZ1583675701242909000570434",
	"offer_id": null,
	"status": "created",
	"attempts": 0,
	"notes": [],
	"created_at": 1583675705
}
*/
type RazorCreateOrderResponse struct {
	Id         string `json:"id"`
	Entity     string `json:"entity"`
	Amount     int64  `json:"amount"`
	AmountPaid int64  `json:"amount_paid"`
	AmountDue  int64  `json:"amount_due"`
	Currency   string `json:"currency"`
	Receipt    string `json:"receipt"`
	Status     string `json:"status"`
	Attempts   int    `json:"attempts"`
	CreatedAt  int    `json:"created_at"`
}

/**
{
  "id": "cont_00000000000001",
  "entity": "contact",
  "name": "Gaurav Kumar",
  "contact": "9123456789",
  "email": "gaurav.kumar@example.com",
  "type": "employee",
  "reference_id": "Acme Contact ID 12345",
  "batch_id": null,
  "active": true,
  "notes": {
    "notes_key_1": "Tea, Earl Grey, Hot",
    "notes_key_2": "Tea, Earl Grey… decaf."
  },
  "created_at": 1545320320
}
*/
type RazorCreateContactsResponse struct {
	Id          string `json:"id"`
	Entity      string `json:"entity"`
	Name        string `json:"name"`
	Contact     string `json:"contact"`
	Email       string `json:"email"`
	Type        string `json:"type"`
	ReferenceId string `json:"reference_id"`
	BatchId     string `json:"batch_id"`
	Active      bool   `json:"active"`
	CreatedAt   int    `json:"created_at"`
}

/**
{
  "id" : "fa_00000000000001",
  "entity": "fund_account",
  "contact_id" : "cont_00000000000001",
  "account_type": "bank_account",
  "bank_account": {
    "ifsc": "HDFC0000053",
    "bank_name": "HDFC Bank",
    "name": "Gaurav Kumar",
    "account_number": "765432123456789",
    "notes": []
  },
  "active": true,
  "batch_id": null,
  "created_at": 1543650891
}
*/
type RazorFundAccountsResponse struct {
	Id          string                       `json:"id"`
	Entity      string                       `json:"entity"`
	ContactId   string                       `json:"contact_id"`
	AccountType string                       `json:"account_type"`
	BankAccount RazorFundAccountsBankAccount `json:"bank_account"`
	VPA         RazorFundAccountsVPA         `json:"vpa"`
	Active      bool                         `json:"active"`
	BatchId     string                       `json:"batch_id"`
	CreatedAt   int                          `json:"created_at"`
}

type RazorFundAccountsBankAccount struct {
	IFSC string `json:"ifsc"`
	//BankName      string `json:"bank_name"`
	Name          string `json:"name"`
	AccountNumber string `json:"account_number"`
}

type RazorFundAccountsVPA struct {
	Address string `json:"address"`
}

type RZFundsAccount struct {
	ContactId   string                       `json:"contact_id"`
	AccountType string                       `json:"account_type"`
	BankAccount RazorFundAccountsBankAccount `json:"bank_account,omitempty"`
}

type RZVPAFundsAccount struct {
	ContactId   string               `json:"contact_id"`
	AccountType string               `json:"account_type"`
	VPA         RazorFundAccountsVPA `json:"vpa,omitempty"`
}

/**
{
  "id": "pout_00000000000001",
  "entity": "payout",
  "fund_account_id": "fa_00000000000001",
  "amount": 1000000,
  "currency": "INR",
  "notes": {
    "notes_key_1":"Tea, Earl Grey, Hot",
    "notes_key_2":"Tea, Earl Grey… decaf."
  },
  "fees": 0,
  "tax": 0,
  "status": "queued",
  "utr": null,
  "mode": "IMPS",
  "purpose": "refund",
  "reference_id": "Acme Transaction ID 12345",
  "narration": "Acme Corp Fund Transfer",
  "batch_id": null,
  "failure_reason": null,
  "created_at": 1545383037
}
*/
type RazorTransfersResponse struct {
	Id            string `json:"id"`
	Entity        string `json:"entity"`
	FundAccountId string `json:"fund_account_id"`
	Amount        int64  `json:"amount"`
	Fees          int64  `json:"fees"`
	Tax           int64  `json:"tax"`
	Status        string `json:"status"`
	Mode          string `json:"mode"`
	FailureReason string `json:"failure_reason"`
}

/**
{
  "error": {
    "code": "BAD_REQUEST_ERROR",
    "description": "Authentication failed due to incorrect otp",
    "field": null,
    "source": "customer",
    "step": "payment_authentication",
    "reason": "invalid_otp",
    "metadata": {
      "payment_id": "pay_EDNBKIP31Y4jl8",
      "order_id": "order_DBJKIP31Y4jl8"
    }
  }
}

*/

type RzInquiryPayout struct {
	ID            string `json:"id"`
	Entity        string `json:"entity"`
	FundAccountID string `json:"fund_account_id"`
	Amount        int    `json:"amount"`
	Currency      string `json:"currency"`
	Notes         struct {
		NoteKey string `json:"note_key"`
	} `json:"notes"`
	Fees          int         `json:"fees"`
	Tax           int         `json:"tax"`
	Status        string      `json:"status"`
	Purpose       string      `json:"purpose"`
	Utr           interface{} `json:"utr"`
	Mode          string      `json:"mode"`
	ReferenceID   string      `json:"reference_id"`
	Narration     string      `json:"narration"`
	BatchID       interface{} `json:"batch_id"`
	StatusDetails struct {
		Description string `json:"description"`
		Source      string `json:"source"`
		Reason      string `json:"reason"`
	} `json:"status_details"`
	CreatedAt int    `json:"created_at"`
	FeeType   string `json:"fee_type"`
	Error     struct {
		Code        string `json:"code"`
		Description string `json:"description"`
	} `json:"error"`
}

type RazorPaymentCallback struct {
}
