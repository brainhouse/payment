package pdkpay

import "fmt"

type PayInResponse struct {
	Status int    `json:"status"`
	Msg    string `json:"msg"`
	Data   struct {
		MerSn       string  `json:"mer_sn"`
		SourceSn    string  `json:"source_sn"`
		TransAmt    float64 `json:"transAmt"`
		FeeAmount   float64 `json:"fee_amount"`
		OrderSn     string  `json:"order_sn"`
		SerialSn    string  `json:"serial_sn"`
		TradeStatus int     `json:"tradeStatus"`
		PaymentUrl  string  `json:"payment_url"` // 支付结果带有
	} `json:"data"`

	//status	返回状态 0 失败 1 成功
	//msg	描述：success/fail
	//data	JSON格式数据
	//mer_sn	商户号
	//source_sn	渠道号
	//transAmt	交易金额
	//fee_amount	手续费
	//order_sn	商户订单号
	//serial_sn	流水号
	//tradeStatus	交易状态 0 待交易 1 交易成功 -1 交易失败
	//payment_url	支付路径
}

type PayNotify struct {
	Status int    `json:"status"`
	Msg    string `json:"msg"`
	Data   struct {
		MerSn       string  `json:"mer_sn"`
		SourceSn    string  `json:"source_sn"`
		TransAmt    float64 `json:"transAmt"`
		FeeAmount   float64 `json:"fee_amount"`
		OrderSn     string  `json:"order_sn"`
		SerialSn    string  `json:"serial_sn"`
		TradeStatus int     `json:"tradeStatus"`
		Sign        string  `json:"sign"` // 异步通知带有
	} `json:"data"`

	//status	返回状态 0 失败 1 成功
	//msg	描述：success/fail
	//data	JSON格式数据
	//mer_sn	商户号
	//source_sn	渠道号
	//transAmt	交易金额
	//fee_amount	手续费
	//order_sn	商户订单号
	//serial_sn	流水号
	//tradeStatus	交易状态 0 待交易 1 交易成功 -1 交易失败
	//"sign":"",       //使用公钥 进行解签 验签
}

func (p PayNotify) DataMap()map[string]string {
	return map[string]string{
		FieldMerSn:       p.Data.MerSn,
		FieldSourceSn:    p.Data.SourceSn,
		FieldTransAmt:    fmt.Sprintf("%.2f", p.Data.TransAmt),
		FieldFeeAmount:   fmt.Sprintf("%.2f", p.Data.FeeAmount),
		FieldOrderSn:     p.Data.OrderSn,
		FieldSerialSn:    p.Data.SerialSn,
		FieldTradeStatus: fmt.Sprintf("%d", p.Data.TradeStatus),
		FieldSign:        p.Data.Sign,
	}
}

type PayQueryResponse struct {
	Status int    `json:"status"`
	Msg    string `json:"msg"`
	Data   struct {
		MerSn       string  `json:"mer_sn"`
		SourceSn    string  `json:"source_sn"`
		TransAmt    float64 `json:"transAmt"`
		FeeAmount   float64 `json:"fee_amount"`
		OrderSn     string  `json:"order_sn"`
		SerialSn    string  `json:"serial_sn"`
		TradeStatus int     `json:"tradeStatus"`
	} `json:"data"`
	//"status":1,
	//"msg":"success",
	//"data":{
	//"mer_sn":"M16624580326",
	//"source_sn":"s166256615310",
	//"transAmt":0,
	//"fee_amount":0,
	//"order_sn:"",
	//"serial_sn:"",
	//"tradeStatus":1
}


type PayOutResponse struct {
	Status int    `json:"status"`
	Msg    string `json:"msg"`
	Data   struct {
		MerSn       string  `json:"mer_sn"`
		SourceSn    string  `json:"source_sn"`
		TransAmt    float64 `json:"transAmt"`
		FeeAmount   float64 `json:"fee_amount"`
		OrderSn     string  `json:"order_sn"`
		SerialSn    string  `json:"serial_sn"`
		TradeStatus int     `json:"tradeStatus"`
		Sign        string  `json:"sign"`
	} `json:"data"`

	//"mer_sn":"M16624580326",
	//"source_sn":"s166256615310",
	//"transAmt":0,
	//"fee_amount":0,
	//"order_sn:"",
	//"serial_sn:"",
	//"tradeStatus":1
	//"sign":"",//使用公钥 进行解签 验签
}