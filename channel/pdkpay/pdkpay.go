package pdkpay

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"funzone_pay/app/validator"
	"funzone_pay/centerlog"
	"funzone_pay/dao"
	"funzone_pay/model"
	"funzone_pay/utils"
	"github.com/wonderivan/logger"
	"io/ioutil"
	"net/http"
	"sort"
	"strings"
)

const (
	FieldSign = "sign"

	FieldAction      = "action"
	FieldOrderSn     = "order_sn"
	FieldMerSn       = "mer_sn"
	FieldSourceSn    = "source_sn"
	FieldCustName    = "custName"
	FieldPhoneNo     = "phone_no"
	FieldEmail       = "email"
	FieldCustIp      = "custIp"
	FieldTitle       = "title"
	FieldTransAmt    = "transAmt"
	FieldNotifyUrl   = "notify_url"
	FieldReturnUrl   = "return_url"
	FieldDescription = "description"
	FieldBankCode    = "bankCode"
	FieldBankName    = "bankName"
	FieldAcctNo      = "acctNo"

	FieldStatus      = "status"
	FieldMsg         = "msg"
	FieldData        = "data"
	FieldFeeAmount   = "fee_amount"
	FieldSerialSn    = "serial_sn"
	FieldTradeStatus = "tradeStatus"
	FieldPaymentUrl  = "payment_url"
)

const (
	ReqStatusSuccess = 1

	PayInStatusPending = 0
	PayInStatusSuccess = 1
	PayInStatusCancel  = -1

	PayOutStatusPending = 0
	PayOutStatusSuccess = 1
	PayOutStatusFail    = -1
)

type PPayService struct {
	PAY_ACCOUNT_ID string
	PAY_CHANNEL    string
	COUNTRY_CODE   int

	MERCHANT_ID string // 商户id
	SOURCE_ID   string // 渠道id
	PUBLIC_KEY  string // 公钥私钥
	PRIVATE_KEY string //

	IN_NOTIFY_URL  string // 异步支付、提现结果通知
	OUT_NOTIFY_URL string
	HOST_URL       string // 支付、提现的请求地址
}

func GetPPayIns(conf map[string]interface{}) *PPayService {
	return &PPayService{
		PAY_ACCOUNT_ID: conf["PAY_ACCOUNT_ID"].(string),
		PAY_CHANNEL:    conf["PAY_CHANNEL"].(string),
		COUNTRY_CODE:   conf["COUNTRY_CODE"].(int),

		MERCHANT_ID: conf["MERCHANT_ID"].(string),
		SOURCE_ID:   conf["SOURCE_ID"].(string),
		PUBLIC_KEY:  conf["PUBLIC_KEY"].(string),
		PRIVATE_KEY: conf["PRIVATE_KEY"].(string),

		IN_NOTIFY_URL:  conf["IN_NOTIFY_URL"].(string),
		OUT_NOTIFY_URL: conf["OUT_NOTIFY_URL"].(string),
		HOST_URL:       conf["HOST_URL"].(string),
	}
}

func (u *PPayService) CreateOrder(args validator.PayEntryValidator) (*model.PayEntry, error) {
	orderId := utils.GetOrderId("PDKIN")
	reqData := u.genPayInReqData(orderId, args)

	data, err := u.Request(u.HOST_URL, "POST", "JSON", reqData, nil)
	logger.Debug("PDKPayService_CreateOrder_Info | reqData=%v | %v| err=%v | response=%v", reqData, args.AppOrderId, err, string(data))
	if err != nil {
		return nil, fmt.Errorf("create order error")
	}

	response := &PayInResponse{}
	err = json.Unmarshal(data, response)
	if err != nil {
		logger.Error("PDKPayService_CreateOrder_JsonUnmarshal_Err | %v | err=%v | data=%v", args.AppOrderId, err, string(data))
		centerlog.OrderError(centerlog.MsgThirdPlatformFail, u.PAY_ACCOUNT_ID, args.AppId, args.AppOrderId, orderId, "false", err.Error())
		return nil, err
	}

	//请求成功写入流水
	pE := new(model.PayEntry)
	pE.AppId = args.AppId
	pE.Uid = args.Uid
	pE.PayAccountId = u.PAY_ACCOUNT_ID
	pE.PayChannel = model.PDKPAY
	pE.CountryCode = u.COUNTRY_CODE
	pE.AppOrderId = args.AppOrderId
	pE.OrderId = orderId
	pE.PaymentOrderId = ""
	pE.Amount = args.Amount
	pE.UserId = args.UserId
	pE.UserName = args.UserName
	pE.Email = args.Email
	pE.Phone = args.Phone
	pE.Status = model.PAYING
	pE.PayAppId = u.MERCHANT_ID

	//
	if response.Status != ReqStatusSuccess { // 可能失败,不做订单失败判定，等异步检查的时候再进行更新状态
		pE.ThirdCode = fmt.Sprintf("%v", response.Status)
		pE.ThirdDesc = fmt.Sprintf("%v", response.Msg)
	}

	//
	engine, err := dao.GetMysql()
	if engine == nil || err != nil {
		logger.Error("PDKPayService_CreateOrder_GetDB_Err | err=%v", err)
		return nil, err
	}
	_, err = engine.Insert(pE)
	if err != nil {
		logger.Error("PDKPayService_CreateOrder_InsertDB_Err | err=%v", err)
		return nil, err
	}

	pE.PaymentLinkHost = response.Data.PaymentUrl
	return pE, nil
}

func (u *PPayService) Inquiry(orderId string, s2 string) (*model.PayEntry, error) {
	res, err := u.query(orderId)
	if err != nil {
		return nil, err
	}

	if res.Status != ReqStatusSuccess {
		return nil, fmt.Errorf("PDK quiry error %v-%v", res.Status, res.Msg)
	}

	engine, _ := dao.GetMysql()
	payIn := new(model.PayEntry)
	exist, err := engine.Where("order_id=?", orderId).Get(payIn)
	if err != nil {
		logger.Error("PDKPayService_Inquiry_GetError | err=%v | data=%v", err, orderId)
		return nil, err
	}
	if !exist {
		return nil, errors.New("NotExist")
	}

	code := res.Data.TradeStatus
	if code == PayInStatusSuccess {
		payIn.Status = model.PAYSUCCESS
		payIn.PaymentId = res.Data.SerialSn
	} else if code == PayInStatusCancel {
		payIn.Status = model.PAYFAILD
		payIn.ThirdCode = fmt.Sprintf("%v", code)
	}

	return payIn, nil
}

func (u *PPayService) InquiryPayout(orderId string, ext string) (*model.PayOut, error) {
	res, err := u.query(orderId)
	if err != nil {
		return nil, err
	}

	if res.Status != ReqStatusSuccess {
		return nil, fmt.Errorf("PDK quiry error %v-%v", res.Status, res.Msg)
	}

	engine, _ := dao.GetMysql()
	payOut := new(model.PayOut)
	exist, err := engine.Where("order_id=?", orderId).Get(payOut)
	if err != nil {
		logger.Error("PDKPayService_PayOut_Inquiry_GetError | err=%v | data=%v", err, orderId)
		return nil, err
	}
	if !exist {
		return nil, errors.New("NotExist")
	}

	status := res.Data.TradeStatus
	if status == PayInStatusSuccess {
		payOut.Status = model.PAY_OUT_SUCCESS
		payOut.PaymentId = res.Data.SerialSn
	} else if status == PayInStatusCancel {
		payOut.Status = model.PAY_OUT_FAILD
		payOut.ThirdCode = fmt.Sprintf("%v", status)
	} else {
		// 不做处理
	}

	return payOut, nil
}

type payoutResponse struct {
	Data struct {
		Status                  string `json:"status"`
		MerchantReferenceNumber string `json:"merchant_reference_number"`
		TransactionId           string `json:"transaction_id"`
	}
	Error struct {
		Code    int    `json:"code"`
		Message string `json:"message"`
	}
}

func (u *PPayService) PayOut(frozenId int64, args validator.PayOutValidator) (payOut *model.PayOut, err error) {
	orderId := utils.GetOrderId("PDKOUT")

	payOut = new(model.PayOut)
	payOut.Uid = args.Uid
	payOut.AppId = args.AppId
	payOut.PayAccountId = u.PAY_ACCOUNT_ID
	payOut.PayChannel = model.PDKPAY
	payOut.CountryCode = u.COUNTRY_CODE
	payOut.PayType = args.PayType
	payOut.AppOrderId = args.AppOrderId
	payOut.OrderId = orderId
	payOut.PaymentId = orderId
	payOut.FrozenId = frozenId
	payOut.Amount = args.Amount
	payOut.BankCard = args.BankCard
	payOut.UserId = args.UserId
	payOut.UserName = args.UserName
	payOut.Email = args.Email
	payOut.Paytm = args.PayTm
	payOut.IFSC = args.IFSC
	payOut.Phone = args.Phone
	engine, err := dao.GetMysql()
	if engine == nil || err != nil {
		logger.Error("PDKPayService_PayOut_GetDB_Err | err=%v", err)
		return nil, err
	}
	_, err = engine.Insert(payOut)
	if err != nil {
		logger.Error("PDKPayService_PayOut_Insert_Error | data=%+v | err=%v", payOut, err)
		return nil, err
	}

	reqData := u.genPayOutReqData(orderId, args)

	// 发起
	data, err := u.Request(u.HOST_URL, "POST", "JSON", reqData, nil)
	logger.Debug("PDKPayService_PayOut_Info | reqData=%v | %v| err=%v | response=%v", reqData, args.AppOrderId, err, string(data))
	if err != nil {
		return nil, fmt.Errorf("payout order error")
	}

	response := &PayOutResponse{}
	err = json.Unmarshal(data, response)
	if err != nil {
		logger.Error("PDKPayService_PayOut_JsonUnmarshal_Err | %v | err=%v | data=%v", args.AppOrderId, err, string(data))
		centerlog.OrderError(centerlog.MsgThirdPlatformFail, u.PAY_ACCOUNT_ID, args.AppId, args.AppOrderId, orderId, "false", err.Error())
		return nil, err
	}

	if response.Status != ReqStatusSuccess {
		logger.Error("PDKPayService_PayOut_Status_Err | %v | %v", args.AppOrderId, response.Status)
		return nil, fmt.Errorf("payout order error")
	}

	code := response.Data.TradeStatus
	paymentId := response.Data.SerialSn
	status := model.PAY_OUT_ING
	thirdCode := payOut.ThirdCode
	thirdDesc := payOut.ThirdDesc
	if code == PayOutStatusFail {
		status = model.PAY_OUT_FAILD
		thirdCode = fmt.Sprint(response.Data.TradeStatus)
		thirdDesc = thirdCode
	}

	if status == model.PAY_OUT_FAILD {
		centerlog.OrderError(centerlog.MsgThirdPlatformFail, u.PAY_ACCOUNT_ID, payOut.AppId, payOut.AppOrderId, payOut.OrderId, fmt.Sprint(response.Data.TradeStatus), "")
	}

	_, err = engine.Where("id=?", payOut.Id).Update(&model.PayOut{
		Status:    status,
		PaymentId: paymentId,
		ThirdCode: thirdCode,
		ThirdDesc: thirdDesc,
	})
	if err != nil {
		logger.Error("PDKPayService_PayOut_Update_Error | data=%v | err=%v", payOut, err)
		return nil, err
	}

	payOut.Status = status
	payOut.PaymentId = paymentId
	payOut.ThirdDesc = thirdDesc
	payOut.ThirdCode = thirdCode

	return payOut, nil
}

func (u *PPayService) PayOutBatch(frozenId int64, outValidator validator.PayOutValidator) (*model.PayOut, error) {
	return u.PayOut(frozenId, outValidator)
}

func (u *PPayService) InSignature(cb interface{}) bool {
	data, ok := cb.(map[string]string)
	if !ok {
		return false
	}

	return VerifySign(data, u.PUBLIC_KEY)
}

func (u *PPayService) OutSignature(cb interface{}) bool {
	return u.InSignature(cb)
}

func (u *PPayService) genPayInReqData(orderId string, args validator.PayEntryValidator) (data map[string]string) {
	amount := fmt.Sprintf("%.2f", args.Amount) // 两位小数

	data = map[string]string{
		FieldAction:      "PAYIN",
		FieldOrderSn:     orderId,
		FieldMerSn:       u.MERCHANT_ID,
		FieldSourceSn:    u.SOURCE_ID,
		FieldCustName:    args.UserName,
		FieldPhoneNo:     args.Phone,
		FieldEmail:       args.Email,
		FieldCustIp:      "192.168.1.201",
		FieldTitle:       "PayIn",
		FieldTransAmt:    amount,
		FieldNotifyUrl:   u.IN_NOTIFY_URL,
		FieldDescription: "description",
	}

	data[FieldSign] = GetSign(data, u.PRIVATE_KEY)
	return
}

func (u *PPayService) genPayOutReqData(orderId string, args validator.PayOutValidator) (data map[string]string) {
	amount := fmt.Sprintf("%.2f", args.Amount) // 两位小数

	data = map[string]string{
		FieldAction:   "PAYIN",
		FieldOrderSn:  orderId,
		FieldMerSn:    u.MERCHANT_ID,
		FieldSourceSn: u.SOURCE_ID,
		FieldCustName: args.UserName,

		FieldPhoneNo:     "8456792323",
		FieldEmail:       "8456792323@gmail.com",
		FieldCustIp:      "customer-ip",
		FieldTitle:       "PayIn",
		FieldTransAmt:    amount,
		FieldNotifyUrl:   u.OUT_NOTIFY_URL,
		FieldDescription: "description",
	}

	data[FieldSign] = GetSign(data, u.PRIVATE_KEY)
	return
}

func (u *PPayService) query(orderId string) (*PayQueryResponse, error) {
	reqData := map[string]string{
		FieldAction:   "QUERY_TRADE_STATUS",
		FieldMerSn:    u.MERCHANT_ID,
		FieldSourceSn: u.SOURCE_ID,
		FieldOrderSn:  orderId,
	}

	reqData[FieldSign] = GetSign(reqData, u.PRIVATE_KEY)

	resp, err := u.Request(u.HOST_URL, "POST", "JSON", reqData, nil)
	if err != nil {
		logger.Error("PDKPayService_Inquiry_Request_Error | err=%v", err)
		return nil, err
	}

	logger.Debug("PDKPayService_Inquiry_Info | requestData=%v | response=%v", orderId, string(resp))

	res := &PayQueryResponse{}
	err = json.Unmarshal(resp, res)
	if err != nil {
		logger.Error("PDKPayService_Inquiry_JsonUnmarshal_Error | err=%v | res=%v", err, res)
		return nil, err
	}

	if res.Status != ReqStatusSuccess {
		return nil, fmt.Errorf("PDKPayService_InQuiry_Error %v-%v", res.Status, res.Msg)
	}

	return res, nil
}

func (u *PPayService) Request(url string, method string, contentType string, sendData interface{}, header map[string][]string) (res []byte, err error) {
	client := &http.Client{}
	var req *http.Request
	if contentType == "JSON" {
		data, _ := json.Marshal(sendData)
		fmt.Println(string(data))
		reader := bytes.NewBuffer(data)
		req, err = http.NewRequest(method, url, reader)
	} else {
		s := sendData.(string)
		reader := strings.NewReader(s)
		req, err = http.NewRequest(method, url, reader)
	}
	//提交请求
	if err != nil {
		logger.Error("PDKPayService_Create_order | url=%v | err=%+v | req=%+v", url, err, sendData)
		return
	}
	if header == nil {
		req.Header = http.Header{
			"Content-Type": {"application/json; charset=utf-8"},
		}
	} else {
		req.Header = http.Header(header)
	}

	//处理返回结果
	response, err := client.Do(req)
	if err != nil {
		logger.Error("PDKPayService_request_err | url=%v | err=%+v | req=%+v", url, err, sendData)
		return
	}
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		logger.Error("PDKPayService_response_read_err | url=%v | err=%+v | req=%+v", url, err, sendData)
		return
	}
	defer func() {
		_ = response.Body.Close()
	}()
	return body, nil
}

func GetSign(data map[string]string, privateKey string) string {
	keys := make([]string, 0, len(data))
	for k, _ := range data {
		if k == FieldSign {
			continue
		}

		keys = append(keys, k)
	}

	sort.Strings(keys)

	str := ""
	for _, k := range keys {
		d := strings.TrimSpace(data[k])
		if len(d) < 1 {
			continue
		}

		str = str + fmt.Sprintf("%v=%v&", k, d)
	}

	fmt.Printf("%v\n", str)
	sign, err := utils.RsaSignWithSha1Base64([]byte(str), utils.WrapPrivateKey(privateKey))
	if err != nil {
		logger.Error("PDKPay build sign error [%v]:%v", err, str)
	}

	return sign
}

func VerifySign(data map[string]string, publicKey string) bool {
	sign := ""
	keys := make([]string, 0, len(data))
	for k, v := range data {
		if k == FieldSign {
			sign = v
			continue
		}

		keys = append(keys, k)
	}

	if len(sign) < 1 {
		logger.Error("PDKPay verify sign error: no field sign")
		return false
	}

	sort.Strings(keys)

	str := ""
	for _, k := range keys {
		d := strings.TrimSpace(data[k])
		if len(d) < 1 {
			continue
		}

		str = str + fmt.Sprintf("%v=%v&", k, d)
	}

	origin, err := base64.StdEncoding.DecodeString(sign)
	if err != nil {
		logger.Error("PDKPay verify sign decode base64 error: %v:%v", err, sign)
		return false
	}

	b, err := utils.RsaVerySignWithSha1([]byte(str), origin, utils.WrapPublicKey(publicKey))
	if err != nil {
		logger.Error("PDKPay verify sign error %v:%v", err, str)
	}

	return b
}
