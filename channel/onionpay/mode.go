package onionpay

/**
{
  "amount": "10",
  "appId": "4189b620f93b48c5904210ff47bb8936",
  "country": "IN",
  "currency": "INR",
  "extInfo": {
    "paymentTypes": "credit,debit,ewallet,upi"
  },
  "merTransNo": "demo-1576735973586",
  "notifyUrl": "https://example.com/notifyurl",
  "prodName": "southeast.asia",
  "returnUrl": "https://example.com/returnurl",
  "sign": "ee2406f9713f9cc234a4e2bb176b1968c500455866e0509a86896264c38dbd20",
  "version": "1.1"
}
*/
type CollectInRequest struct {
	Amount     string    `json:"amount"`
	AppId      string    `json:"appId"`
	Country    string    `json:"country"`
	Currency   string    `json:"currency"`
	ExtInfo    InExtInfo `json:"extInfo"`
	MerTransNo string    `json:"merTransNo"`
	NotifyUrl  string    `json:"notifyUrl"`
	ReturnUrl  string    `json:"returnUrl"`
	Sign       string    `json:"sign"`
	Version    string    `json:"version"`
}

type InExtInfo struct {
	PaymentTypes string `json:"paymentTypes"`
}

/**
{
  "data": {
    "amount": "10",
    "currency": "INR",
    "doing": "redirect",
    "merTransNo": "demo-1576735973586",
    "message": "success",
    "resultCode": "0000",
    "sign": "e28a04a3bcb0dba92d8e7e8d9cfa0c4b6069430f313ae166cbe8b447c65e8f9a",
    "tradeNo": "20191219141556118010479493",
    "url": "https://onion-pay.com/checkout/b2fda22f77d94373b6123b21a144272ed"
  },
  "code": "200",
  "msg": "OK",
  "timestamp": "1576736156349"
}
*/
type OrderInResponse struct {
	Data struct {
		Amount     string `json:"amount"`
		Doing      string `json:"doing"`
		Currency   string `json:"currency"`
		MerTransNo string `json:"merTransNo"`
		Message    string `json:"message"`
		ResultCode string `json:"resultCode"`
		Sign       string `json:"sign"`
		TradeNo    string `json:"tradeNo"`
		Url        string `json:"url"`
	} `json:"data"`
	Code      string `json:"code"`
	Msg       string `json:"msg"`
	Timestamp string `json:"timestamp"`
}

/**
{
  "amount": "100.00",
  "appId": "c6bc0348acc447a9d55fc4eb5100e3c49a",
  "currency": "INR",
  "merTransNo": "demo-1576742093462",
  "notifyUrl": "https://example.com/payout/notifyurl",
  "version": "1.1",
  "sign": "c6bc0348acc447a9d55fc4eb5100e3c49a998ec958c11b062e4040d85958cff5",
  "extInfo":
  {
    "bankCode": "SBIN0007440",
    "accountNumber": "12345678912",
    "accountHolderName": "ABC lastName"
    "payeeLastName": "lastName"
    "payeePhone": "911234567890",
    "payeeEmail": "abc@gmail.com",
    "payeeAddress": "accountHolder's address",
  }
}
*/
type PayOutRequest struct {
	Amount     string     `json:"amount"`
	AppId      string     `json:"appId"`
	Currency   string     `json:"currency"`
	MerTransNo string     `json:"merTransNo"`
	NotifyUrl  string     `json:"notifyUrl"`
	ExtInfo    OutExtInfo `json:"extInfo"`
	Sign       string     `json:"sign"`
	Version    string     `json:"version"`
}

type OutExtInfo struct {
	BankCode          string `json:"bankCode"`
	AccountNumber     string `json:"accountNumber"`
	AccountHolderName string `json:"accountHolderName"`
	PayeePhone        string `json:"payeePhone"`
	PayeeEmail        string `json:"payeeEmail"`
	PayeeAddress      string `json:"payeeAddress"`
}

/**
{
	  "data": {
	    "amount": "10000",
	    "currency": "INR",
	    "merTransNo": "demo-1576742093462",
	    "message": "success",
	    "resultCode": "0000",
	    "sign": "e1618758a8eeccefa001e8694ba07355546f73b78a740a0c146c9d8ef4881487",
	    "tradeNo": "20191219155755942010474021"
	  },
	  "code": "200",
	  "msg": "OK",
	  "timestamp": "1576742276065"
	}
*/

type PayOutResponse struct {
	Data struct {
		Amount     string `json:"amount"`
		Currency   string `json:"currency"`
		MerTransNo string `json:"merTransNo"`
		Message    string `json:"message"`
		ResultCode string `json:"resultCode"`
		Sign       string `json:"sign"`
		TradeNo    string `json:"tradeNo"`
	} `json:"data"`
	Code      string `json:"code"`
	Msg       string `json:"msg"`
	Timestamp string `json:"timestamp"`
}
