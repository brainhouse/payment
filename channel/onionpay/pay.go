package onionpay

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"funzone_pay/app/validator"
	"funzone_pay/centerlog"
	"funzone_pay/dao"
	"funzone_pay/model"
	"funzone_pay/utils"
	"github.com/wonderivan/logger"
	"io/ioutil"
	"net/http"
	"net/url"
	"sort"
	"strings"
	"time"
)

type OnionPayService struct {
	Host              string
	APPID             string
	PAY_ACCOUNT_ID    string
	PAY_CHANNEL       string
	COUNTRY_CODE      int
	APPSK             string
	RETRUN_URL        string
	COLLECT_HOST      string
	NOTIFY_URL        string
	PAY_OUT_URL       string
	PAY_OUT_CLIENTID  string
	PAY_OUT_CLIENTSK  string
	PAYOUT_NOTIFY_URL string
}

func GetOnionPayIns(conf map[string]interface{}) *OnionPayService {
	return &OnionPayService{
		PAY_ACCOUNT_ID:    conf["PAY_ACCOUNT_ID"].(string),
		PAY_CHANNEL:       conf["PAY_CHANNEL"].(string),
		COUNTRY_CODE:      conf["COUNTRY_CODE"].(int),
		Host:              conf["HOST"].(string),
		APPID:             conf["APPID"].(string),
		APPSK:             conf["APPSK"].(string),
		RETRUN_URL:        conf["RETURN_URL"].(string),
		NOTIFY_URL:        conf["NOTIFY_URL"].(string),
		COLLECT_HOST:      conf["COLLECT_HOST"].(string),
		PAY_OUT_URL:       conf["PAY_OUT_URL"].(string),
		PAY_OUT_CLIENTID:  conf["PAY_OUT_CLIENTID"].(string),
		PAY_OUT_CLIENTSK:  conf["PAY_OUT_CLIENTSK"].(string),
		PAYOUT_NOTIFY_URL: conf["PAYOUT_NOTIFY_URL"].(string),
	}
}

func CheckReturnResponse(rawBody []byte, params url.Values) (success int, order string, message string) {
	// GET
	// query=
	// processCurrency=INR&amount=215.00&merTransNo=OPIN1662640544026292f030
	// &sign=583c184361c66a0f8672914486acb9ab62f8fccd65e9bcadb5d31a438b2f88e7&updateTime=2022-09-08+18%3A06%3A02
	// &transNo=20220908180544303559578198834394&utr=225191159607&createTime=2022-09-08+18%3A05%3A44
	// &processAmount=215.00&currency=INR
	// &merCustomize=&transStatus=success
	order = params.Get("merTransNo")
	status := params.Get("transStatus")
	if status == "success" {
		return 1, order, ""
	}

	return -1, order, ""
}

func (e *OnionPayService) CreateOrder(args validator.PayEntryValidator) (*model.PayEntry, error) {
	orderId := utils.GetOrderId("OPIN")
	requestUrl := fmt.Sprintf("%s%s", e.Host, "/pg/dk/order/create")
	/**
	{
	  "amount": "10",
	  "appId": "4189b620f93b48c5904210ff47bb8936",
	  "country": "IN",
	  "currency": "INR",
	  "extInfo": {
	    "paymentTypes": "credit,debit,ewallet,upi"
	  },
	  "merTransNo": "demo-1576735973586",
	  "notifyUrl": "https://example.com/notifyurl",
	  "prodName": "southeast.asia",
	  "returnUrl": "https://example.com/returnurl",
	  "sign": "ee2406f9713f9cc234a4e2bb176b1968c500455866e0509a86896264c38dbd20",
	  "version": "1.1"
	}
	*/
	params := url.Values{}
	params.Add("amount", fmt.Sprint(args.Amount))
	params.Add("appId", e.APPID)
	params.Add("country", "IN")
	params.Add("currency", "INR")
	params.Add("extInfo", "paymentTypes=credit,debit,ewallet,upi")
	params.Add("merTransNo", orderId)
	params.Add("notifyUrl", e.NOTIFY_URL)
	//params.Add("prodName", "rummy")
	params.Add("returnUrl", e.RETRUN_URL)
	params.Add("version", "1.1")

	sign := e.GetSign(params, e.APPSK)

	var rData = &CollectInRequest{
		Amount:   fmt.Sprint(args.Amount),
		AppId:    e.APPID,
		Country:  "IN",
		Currency: "INR",
		ExtInfo: InExtInfo{
			PaymentTypes: "credit,debit,ewallet,upi",
		},
		MerTransNo: orderId,
		NotifyUrl:  e.NOTIFY_URL,
		ReturnUrl:  e.RETRUN_URL,
		Version:    "1.1",
		Sign:       sign,
	}
	header := map[string][]string{
		"Content-type": {"application/json"},
	}
	data, err := e.Request(requestUrl, "POST", "JSON", rData, header)
	logger.Debug("OnionPayService_CreateOrder_Info | orderReq=%+v | err=%v | data=%v", rData, err, string(data))

	fmt.Println(string(data))
	egResponse := &OrderInResponse{}
	err = json.Unmarshal(data, egResponse)
	if err != nil {
		logger.Error("OnionPayService_CreateOrder_JsonUnmarshal_Err | err=%v", err)
		centerlog.OrderError(centerlog.MsgThirdPlatformFail, e.PAY_ACCOUNT_ID, args.AppId, args.AppOrderId, orderId, "false", err.Error())
		return nil, err
	}
	//请求成功写入流水
	pE := new(model.PayEntry)
	pE.AppId = args.AppId
	pE.Uid = args.Uid
	pE.PayAccountId = e.PAY_ACCOUNT_ID
	pE.PayChannel = model.ONIONPAY
	pE.CountryCode = e.COUNTRY_CODE
	pE.AppOrderId = args.AppOrderId
	pE.PaymentOrderId = egResponse.Data.TradeNo
	pE.OrderId = orderId
	pE.Amount = args.Amount
	pE.UserId = args.UserId
	pE.UserName = args.UserName
	pE.Email = args.Email
	pE.Phone = args.Phone
	pE.ThirdCode = egResponse.Code
	pE.Status = model.PAYING
	pE.PayAppId = e.APPID
	pE.FinishTime = time.Now().Unix()

	fail := false
	if egResponse.Code != "200" {
		fail = true
		pE.Status = model.PAYFAILD
		pE.ThirdCode = egResponse.Code
		pE.ThirdDesc = fmt.Sprintf("%v-%v", egResponse.Code, egResponse.Msg)
		centerlog.OrderError(centerlog.MsgThirdPlatformFail, e.PAY_ACCOUNT_ID, args.AppId, args.AppOrderId, orderId, egResponse.Code, egResponse.Msg)
	} else if egResponse.Data.ResultCode != "0000" && egResponse.Data.Message != "success" {
		fail = true
		pE.Status = model.PAYFAILD
		pE.ThirdCode = egResponse.Data.ResultCode
		pE.ThirdDesc = fmt.Sprintf("%v-%v", egResponse.Data.ResultCode, egResponse.Data.Message)
		centerlog.OrderError(centerlog.MsgThirdPlatformFail, e.PAY_ACCOUNT_ID, args.AppId, args.AppOrderId, orderId, egResponse.Data.ResultCode, egResponse.Data.Message)
	}
	engine, err := dao.GetMysql()
	if engine == nil || err != nil {
		logger.Error("OnionPayService_CreateOrder_GetDB_Err | err=%v", err)
		return nil, err
	}
	_, err = engine.Insert(pE)
	if err != nil {
		logger.Error("OnionPayService_CreateOrder_InsertDB_Err | err=%v", err)
		return nil, err
	}
	logger.Debug("OnionPayService_CreateOrder_Request | req=%v | res=%v", rData, egResponse)
	if fail {
		return nil, errors.New(pE.ThirdDesc)
	}
	pE.PaymentLinkHost = egResponse.Data.Url
	return pE, err
}

func (e *OnionPayService) Inquiry(orderId string, s2 string) (*model.PayEntry, error) {
	params := url.Values{}
	params.Add("merchantId", e.APPID)
	params.Add("merTransNo", orderId)
	params.Add("version", "1.1")

	sign := e.GetSign(params, e.APPSK)

	reqData := &struct {
		MerchantId string `json:"merchantId"`
		MerTransNo string `json:"merTransNo"`
		Version    string `json:"version"`
		Sign       string `json:"sign"`
	}{
		MerchantId: e.APPID,
		MerTransNo: orderId,
		Version:    "1.1",
		Sign:       sign,
	}

	requestUrl := fmt.Sprintf("%s%s", e.PAY_OUT_URL, "/pg/dk/order/query")
	header := map[string][]string{
		"Content-Type": {"application/json"},
	}
	data, err := e.Request(requestUrl, "POST", "JSON", reqData, header)
	if err != nil {
		logger.Error("OnionPayService_Collect_Inquiry_Error | err=%v", err)
		return nil, err
	}
	logger.Debug("OnionPayService_Collect_Inquiry_Data | orderId=%v | res=%v", orderId, string(data))
	statusData := &struct {
		Data struct {
			TradeStatus string `json:"tradeStatus"`
			Message     string `json:"message"`
		} `json:"data"`
		Msg string `json:"msg"`
	}{}
	err = json.Unmarshal(data, statusData)
	if err != nil {
		logger.Error("OnionPayService_CollectInquiry_JsonError | err=%v | data=%v", err, string(data))
		return nil, err
	}

	engine, _ := dao.GetMysql()
	payEntry := new(model.PayEntry)
	exist, err := engine.Where("order_id=?", orderId).Get(payEntry)
	if err != nil {
		logger.Error("OnionPayService_Collect_Inquiry_GetError | err=%v | data=%v", err, orderId)
		return nil, err
	}
	if !exist {
		return nil, errors.New("NotExist")
	}
	if statusData.Data.TradeStatus == "failure" {
		payEntry.Status = model.PAYFAILD
		payEntry.ThirdDesc = statusData.Data.Message
	} else if statusData.Data.TradeStatus == "success" {
		payEntry.Status = model.PAYSUCCESS
	} else if (statusData.Data.TradeStatus == "pending" || strings.Contains(statusData.Msg, "not found")) && payEntry.Created < time.Now().Unix()-24*3600 {
		payEntry.Status = model.PAYFAILD
	}
	return payEntry, nil
}

func (e *OnionPayService) InquiryPayout(orderId string, ext string) (*model.PayOut, error) {
	params := url.Values{}
	params.Add("merchantId", e.PAY_OUT_CLIENTID)
	params.Add("merTransNo", orderId)
	params.Add("version", "1.1")

	sign := e.GetSign(params, e.PAY_OUT_CLIENTSK)

	reqData := &struct {
		MerchantId string `json:"merchantId"`
		MerTransNo string `json:"merTransNo"`
		Version    string `json:"version"`
		Sign       string `json:"sign"`
	}{
		MerchantId: e.PAY_OUT_CLIENTID,
		MerTransNo: orderId,
		Version:    "1.1",
		Sign:       sign,
	}

	requestUrl := fmt.Sprintf("%s%s", e.PAY_OUT_URL, "/pg/dk/payout/query")
	header := map[string][]string{
		"Content-Type": {"application/json"},
	}
	data, err := e.Request(requestUrl, "POST", "JSON", reqData, header)
	if err != nil {
		logger.Error("OnionPayService_PayOut_Inquiry_Error | err=%v", err)
		return nil, err
	}
	logger.Debug("OnionPayService_PayOut_Inquiry_Data | orderId=%v | res=%v", orderId, string(data))
	statusData := &struct {
		Data struct {
			TradeStatus string `json:"tradeStatus"`
			Message     string `json:"message"`
			Utr         string `json:"utr"`
		} `json:"data"`
		Code string `json:"code"`
	}{}
	err = json.Unmarshal(data, statusData)
	if err != nil {
		logger.Error("OnionPayService_PayOut_Inquiry_JsonError | err=%v | data=%v", err, string(data))
		return nil, err
	}

	engine, _ := dao.GetMysql()
	payOut := new(model.PayOut)
	exist, err := engine.Where("order_id=?", orderId).Get(payOut)
	if err != nil {
		logger.Error("OnionPayService_PayOut_Inquiry_GetError | err=%v | data=%v", err, orderId)
		return nil, err
	}
	if !exist {
		return nil, errors.New("NotExist")
	}
	if statusData.Data.TradeStatus == "failure" || statusData.Code == "0500" {
		payOut.Status = model.PAY_OUT_FAILD
		payOut.ThirdDesc = statusData.Data.Message
	} else if statusData.Data.TradeStatus == "success" {
		payOut.Status = model.PAY_OUT_SUCCESS
		payOut.PaymentId = statusData.Data.Utr
	}
	return payOut, nil

}

func (e *OnionPayService) PayOut(frozenId int64, outValidator validator.PayOutValidator) (payOut *model.PayOut, err error) {
	transId := utils.GetOrderId("OPOUT")
	payOut = new(model.PayOut)
	payOut.Uid = outValidator.Uid
	payOut.AppId = outValidator.AppId
	payOut.PayAccountId = e.PAY_ACCOUNT_ID
	payOut.PayChannel = model.ONIONPAY
	payOut.CountryCode = e.COUNTRY_CODE
	payOut.PayType = outValidator.PayType
	payOut.AppOrderId = outValidator.AppOrderId
	payOut.OrderId = transId
	payOut.PaymentId = transId
	payOut.FrozenId = frozenId
	payOut.Amount = outValidator.Amount
	payOut.BankCard = outValidator.BankCard
	payOut.UserId = outValidator.UserId
	payOut.UserName = outValidator.UserName
	payOut.Email = outValidator.Email
	payOut.Paytm = outValidator.PayTm
	payOut.IFSC = outValidator.IFSC
	payOut.Phone = outValidator.Phone
	engine, err := dao.GetMysql()
	if engine == nil || err != nil {
		logger.Error("OnionPayService_PayOut_GetDB_Err | err=%v", err)
		return nil, err
	}
	_, err = engine.Insert(payOut)
	if err != nil {
		logger.Error("OnionPayService_PayOut_Insert_Error | data=%+v | err=%v", payOut, err)
		return nil, err
	}
	//发起支付
	requestUrl := fmt.Sprintf("%s%s", e.PAY_OUT_URL, "/pg/dk/payout/create")
	header := map[string][]string{
		"Content-Type": {"application/json"},
	}
	params := url.Values{}
	params.Add("amount", fmt.Sprint(outValidator.Amount))
	params.Add("appId", e.PAY_OUT_CLIENTID)
	params.Add("currency", "INR")
	params.Add("notifyUrl", e.PAYOUT_NOTIFY_URL)
	params.Add("merTransNo", transId)
	params.Add("version", "1.1")

	reqData := &PayOutRequest{
		Amount:     fmt.Sprint(outValidator.Amount),
		AppId:      e.PAY_OUT_CLIENTID,
		Currency:   "INR",
		MerTransNo: transId,
		NotifyUrl:  e.PAYOUT_NOTIFY_URL,
		Version:    "1.1",
		ExtInfo: OutExtInfo{
			AccountHolderName: outValidator.UserName,
			PayeePhone:        outValidator.Phone,
			PayeeEmail:        outValidator.Email,
			PayeeAddress:      outValidator.Address,
		},
	}
	bankCode := outValidator.IFSC
	accountNumber := outValidator.BankCard

	if outValidator.PayType == model.PT_BANK {
		reqData.ExtInfo.BankCode = outValidator.IFSC
		reqData.ExtInfo.AccountNumber = accountNumber
	} else if outValidator.PayType == "upi" {
		bankCode = "upi"
		accountNumber = outValidator.VPA

		reqData.ExtInfo.BankCode = "upi"
		reqData.ExtInfo.AccountNumber = outValidator.VPA
	}
	extInfo := fmt.Sprintf("accountHolderName=%v&accountNumber=%v&bankCode=%v&payeeAddress=%v&payeeEmail=%v&payeePhone=%v", outValidator.UserName, accountNumber, bankCode, outValidator.Address, outValidator.Email, outValidator.Phone)
	params.Add("extInfo", extInfo)

	sign := e.GetSign(params, e.PAY_OUT_CLIENTSK)
	reqData.Sign = sign

	data, err := e.Request(requestUrl, "POST", "JSON", reqData, header)
	if err != nil {
		logger.Error("OnionPayService_PayOut_Request_Error | err=%v", err)
		return nil, err
	}

	logger.Debug("OnionPayService_PayOut_Info | requestUrl=%v | requestData=%v | response=%v", requestUrl, reqData, string(data))

	res := &PayOutResponse{}
	err = json.Unmarshal(data, res)
	if err != nil {
		logger.Error("OnionPayService_PayOut_JsonUnmarshal_Error | err=%v | res=%v", err, res)
		centerlog.OrderError(centerlog.MsgThirdPlatformFail, e.PAY_ACCOUNT_ID, payOut.AppId, payOut.AppOrderId, payOut.OrderId, "false", err.Error())
		return nil, err
	}
	var (
		status      model.PayOutStatus = model.PAY_OUT_ING
		thirdDesc   string
		referenceId string
	)
	payOut.ThirdCode = res.Code
	if res.Code != "200" {
		//先不设置成失败，避免接口失败但后续出款成功了亏钱
		//status = model.PAY_OUT_FAILD
		thirdDesc = fmt.Sprintf("%v-%v", res.Code, res.Msg)
		centerlog.OrderError(centerlog.MsgThirdPlatformFail, e.PAY_ACCOUNT_ID, payOut.AppId, payOut.AppOrderId, payOut.OrderId, res.Code, thirdDesc)
	} else if res.Data.ResultCode != "0000" && res.Data.Message != "success" {
		//先不设置成失败，避免接口失败但后续出款成功了亏钱
		//status = model.PAY_OUT_FAILD
		thirdDesc = fmt.Sprintf("%v-%v", res.Data.ResultCode, res.Data.Message)
		centerlog.OrderError(centerlog.MsgThirdPlatformFail, e.PAY_ACCOUNT_ID, payOut.AppId, payOut.AppOrderId, payOut.OrderId, res.Data.ResultCode, thirdDesc)
	} else {
		status = model.PAY_OUT_ING
		referenceId = res.Data.TradeNo
	}
	_, err = engine.Where("id=?", payOut.Id).Update(&model.PayOut{
		Status:     status,
		ThirdCode:  payOut.ThirdCode,
		PaymentId:  referenceId,
		ThirdDesc:  thirdDesc,
		FinishTime: time.Now().Unix(),
	})
	if err != nil {
		logger.Error("OnionPayService_PayOut_Insert_Error | data=%v | err=%v", payOut, err)
		return nil, err
	}
	return payOut, nil
}

func (e *OnionPayService) GetSign(params url.Values, sk string) string {
	var sortSlice []string
	for k, v := range params {
		if k == "sign" || v[0] == "" {
			continue
		}
		sortSlice = append(sortSlice, k)
	}
	sort.Strings(sortSlice)
	signs := ""
	for _, v := range sortSlice {
		signs = fmt.Sprintf("%v%v=%v&", signs, v, params[v][0])
	}
	signs += "key=" + sk

	return utils.HSha256(signs)
}

func (e *OnionPayService) PayOutBatch(frozenId int64, outValidator validator.PayOutValidator) (*model.PayOut, error) {
	return e.PayOut(frozenId, outValidator)
}

func (e *OnionPayService) InSignature(cb interface{}) bool {
	ecb := cb.(*model.OnionInCallback)
	params, _ := url.ParseQuery(ecb.OrgStr)
	sign := e.GetSign(params, e.APPSK)
	if sign != ecb.Sign {
		return false
	}
	return true
}

func (e *OnionPayService) OutSignature(cb interface{}) bool {
	ecb := cb.(*model.OnionOutCallback)
	params, _ := url.ParseQuery(ecb.OrgStr)
	sign := e.GetSign(params, e.PAY_OUT_CLIENTSK)
	if sign != ecb.Sign {
		return false
	}
	return true
}

/*
*
统一请求
*/
func (e *OnionPayService) Request(url string, method string, contentType string, sendData interface{}, header map[string][]string) (res []byte, err error) {
	client := &http.Client{}
	var req *http.Request
	if contentType == "JSON" {
		data, _ := json.Marshal(sendData)
		fmt.Println(string(data))
		reader := bytes.NewBuffer(data)
		req, err = http.NewRequest(method, url, reader)
	} else {
		s := sendData.(string)
		reader := strings.NewReader(s)
		req, err = http.NewRequest(method, url, reader)
	}
	//提交请求
	if err != nil {
		logger.Error("OnionPayService_Create_order | url=%v | err=%+v | req=%+v", url, err, sendData)
		return
	}
	req.Header = http.Header(header)
	//处理返回结果
	response, err := client.Do(req)
	if err != nil {
		logger.Error("OnionPayService_request_err | url=%v | err=%+v | req=%+v", url, err, sendData)
		return
	}
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		logger.Error("OnionPayService_response_read_err | url=%v | err=%+v | req=%+v", url, err, sendData)
		return
	}
	defer func() {
		_ = response.Body.Close()
	}()
	return body, nil
}
