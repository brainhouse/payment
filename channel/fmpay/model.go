package fmpay

type PayInResponse struct {
	Success   bool   `json:"success"`
	Msg       string `json:"msg"`
	Code      int    `json:"code"`
	Timestamp int64  `json:"timestamp"`
	Data      struct {
		ID             string `json:"id"`
		PayUrl         string `json:"PayUrl"`
		ExpirationTime string `json:"expirationTime"`
	} `json:"data"`
}

//{
//"success":true,"msg":"success","code":200,"timestamp":1668062159529,"data":
//{
//"id":"1590181790528569344",
//"orderNo":"1590181790528569344",
//"gatheringChannelCode":"bankCard","gatheringChannelName":"Bank","gatheringAmount":8936.0,"orderAmount":9000.0,"orderState":"3","orderStateName":"已接单","submitTime":"2022-11-09 11:17:38",
//"usefulTime":"2022-11-09 12:15:39","gatheringCodeStorageId":null,"gatheringCodeUrl":null,"alipayUserid":"","openAccountBank":"Habib metro bank","accountHolder":"HuiDa PVT","bankCardAccount":"6990229301714134459","codeContent":null,"returnUrl":null,"timestamp":"2022-11-10 14:35:59"}}
type PayInOrderInfoResponse struct {
	Success   bool   `json:"success"`
	Msg       string `json:"msg"`
	Code      int    `json:"code"`
	Timestamp int64  `json:"timestamp"`
	Data      struct {
		ID                   string  `json:"id"`
		OrderNo              string  `json:"orderNo"`
		GatheringChannelCode string  `json:"gatheringChannelCode"`
		GatheringChannelName string  `json:"gatheringChannelName"`
		GatheringAmount      float64 `json:"gatheringAmount"`
		OrderAmount          float64 `json:"orderAmount"`
		OrderState           string  `json:"orderState"`
		OrderStateName       string  `json:"orderStateName"`
		SubmitTime           string  `json:"submitTime"`
		Timestamp            string  `json:"timestamp"`
		UsefulTime           string  `json:"usefulTime"`
		OpenAccountBank      string  `json:"openAccountBank"`
		AccountHolder        string  `json:"accountHolder"`
		BankCardAccount      string  `json:"bankCardAccount"`
	} `json:"data"`
}

type PayInNotify struct {
	MerchantNum     string `json:"merchantNum"`
	OrderNo         string `json:"orderNo"`
	PlatformOrderNo string `json:"platformOrderNo"`
	Amount          string `json:"amount"`
	ActualPayAmount string `json:"actualPayAmount"`
	Attch           string `json:"attch"`
	State           string `json:"state"` // success -> 1
	PayTime         string `json:"payTime"`
	Sign            string `json:"sign"`
}

type AccountNotify struct {
	MerchantNum        string `json:"merchantNum"`
	InformationSources string `json:"informationSources"`
	PayerAccount       string `json:"payerAccount"`
	CollectionAccount  string `json:"collectionAccount"`
	TransTime          string `json:"transTime"`
	Amount             string `json:"amount"`
	RcvTime            string `json:"rcvTime"`
	BankBalance        string `json:"bankBalance"`
	Sign               string `json:"sign"`
}

type PayOutResponse struct {
	Success   bool   `json:"success"`
	Msg       string `json:"msg"`
	Code      int    `json:"code"`
	Timestamp int64  `json:"timestamp"`
	Data      struct {
		PfOrderId        string `json:"pfOrderId"`
		PfOrderTm        string `json:"pfOrderTm"`
		MerchantNo       string `json:"merchantNo"`
		MerchantOrderId  string `json:"merchantOrderId"`
		MerchantOrderAmt string `json:"merchantOrderAmt"`
		ActualOrderAmt   string `json:"actualOrderAmt"`
		OrderState       string `json:"orderState"`
		OrderMsg         string `json:"orderMsg"`
		SignType         string `json:"signType"`
		Sign             string `json:"sign"`
	} `json:"data"`
}

type PayOutNotify struct {
	PfOrderId        string `json:"pfOrderId"`
	PfOrderTm        string `json:"pfOrderTm"`
	MerchantNo       string `json:"merchantNo"`
	MerchantOrderId  string `json:"merchantOrderId"`
	MerchantOrderAmt string `json:"merchantOrderAmt"`
	ActualOrderAmt   string `json:"actualOrderAmt"`
	PfServiceFee     string `json:"pfServiceFee"`
	OrderState       string `json:"orderState"`
	OrderMsg         string `json:"orderMsg"`
	SignType         string `json:"signType"`
	Sign             string `json:"sign"`
}

type PayOutQueryResponse struct {
	Success   bool   `json:"success"`
	Msg       string `json:"msg"`
	Code      int    `json:"code"`
	Timestamp int64  `json:"timestamp"`
	Data      struct {
		PfOrderId        string `json:"pfOrderId"`
		PfOrderTm        string `json:"pfOrderTm"`
		MerchantNo       string `json:"merchantNo"`
		MerchantOrderId  string `json:"merchantOrderId"`
		MerchantOrderAmt string `json:"merchantOrderAmt"`
		ActualOrderAmt   string `json:"actualOrderAmt"`
		OrderState       string `json:"orderState"`
		OrderMsg         string `json:"orderMsg"`
		SignType         string `json:"signType"`
		Sign             string `json:"sign"`
	} `json:"data"`
}
