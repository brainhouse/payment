package fmpay

import (
	"bytes"
	"crypto/md5"
	"encoding/json"
	"errors"
	"fmt"
	"funzone_pay/app/validator"
	"funzone_pay/centerlog"
	"funzone_pay/dao"
	"funzone_pay/model"
	"funzone_pay/utils"
	"github.com/wonderivan/logger"
	"io/ioutil"
	"math/rand"
	"net/http"
	"net/url"
	"sort"
	"strings"
	"sync/atomic"
	"time"
)

const (
	FieldSign = "sign"

	FieldMerchantNum = "merchantNum"
	FieldAppId       = "appId"
	FieldOrderNo     = "orderNo"
	FieldPayType     = "payType"
	FieldAmount      = "amount"
	FieldNotifyUrl   = "notifyUrl"
	FieldBuyerId     = "buyerId"
	FieldPublishTime = "publishTime"

	FieldMerchantNo       = "merchantNo"
	FieldMerchantOrderId  = "merchantOrderId"
	FieldMerchantOrderAmt = "merchantOrderAmt"
	FieldServiceFeeType   = "serviceFeeType"
	FieldPayeeName        = "payeeName"
	FieldBankAcc          = "bankAcc"
	FieldBankCode         = "bankCode"
	FieldSignType         = "signType"
)

const (
	ReqStatusSuccess = true

	PayInSuccess = "1"

	PayOutStatusSuccess = "00"
	PayOutStatusFailure = "02"
)

type FMPayService struct {
	PAY_ACCOUNT_ID string
	PAY_CHANNEL    string
	COUNTRY_CODE   int

	MERCHANT_NUM string // 商户id
	MERCHANT_KEY string
	APP_ID       string //

	IN_NOTIFY_URL   string // 异步支付、提现结果通知
	OUT_NOTIFY_URL  string
	HOST_URL        string // 支付、提现的请求地址
	ORDER_HOST      string // 获取订单信息
	COLLECT_URL_FMT string
}

func GetFMPayIns(conf map[string]interface{}) *FMPayService {
	return &FMPayService{
		PAY_ACCOUNT_ID: conf["PAY_ACCOUNT_ID"].(string),
		PAY_CHANNEL:    conf["PAY_CHANNEL"].(string),
		COUNTRY_CODE:   conf["COUNTRY_CODE"].(int),

		MERCHANT_NUM: conf["MERCHANT_NUM"].(string),
		MERCHANT_KEY: conf["MERCHANT_KEY"].(string),
		APP_ID:       conf["APP_ID"].(string),

		IN_NOTIFY_URL:   conf["IN_NOTIFY_URL"].(string),
		OUT_NOTIFY_URL:  conf["OUT_NOTIFY_URL"].(string),
		HOST_URL:        conf["HOST_URL"].(string),
		ORDER_HOST:      conf["ORDER_HOST"].(string),
		COLLECT_URL_FMT: conf["COLLECT_URL_FMT"].(string),
	}
}

func (u *FMPayService) CreateOrder(args validator.PayEntryValidator) (*model.PayEntry, error) {
	orderId := utils.GetOrderId("FMIN")
	reqData := u.genPayInReqData(orderId, args)

	data, err := u.Request(u.HOST_URL+"/api/payOrder", "POST", "JSON", reqData, nil)
	logger.Debug("FMPayService_CreateOrder_Info | reqData=%v | %v| err=%v | response=%v", reqData, args.AppOrderId, err, string(data))
	if err != nil {
		return nil, fmt.Errorf("create order error")
	}

	response := &PayInResponse{}
	err = json.Unmarshal(data, response)
	if err != nil {
		logger.Error("FMPayService_CreateOrder_JsonUnmarshal_Err | %v | err=%v | data=%v", args.AppOrderId, err, string(data))
		centerlog.OrderError(centerlog.MsgThirdPlatformFail, u.PAY_ACCOUNT_ID, args.AppId, args.AppOrderId, orderId, "false", err.Error())
		return nil, fmt.Errorf("unmarshal error")
	}

	if response.Success != ReqStatusSuccess { // 失败
		return nil, fmt.Errorf("create order error")
	}

	//请求成功写入流水
	pE := new(model.PayEntry)
	pE.AppId = args.AppId
	pE.Uid = args.Uid
	pE.PayAccountId = u.PAY_ACCOUNT_ID
	pE.PayChannel = model.FMPAY
	pE.AppOrderId = args.AppOrderId
	pE.CountryCode = u.COUNTRY_CODE
	pE.OrderId = orderId
	pE.PaymentOrderId = ""
	pE.Amount = args.Amount
	pE.UserId = args.UserId
	pE.UserName = args.UserName
	pE.Email = args.Email
	pE.Phone = args.Phone
	pE.Status = model.PAYING
	pE.PayAppId = u.MERCHANT_NUM

	//
	engine, err := dao.GetMysql()
	if engine == nil || err != nil {
		logger.Error("FMPayService_CreateOrder_GetDB_Err | err=%v", err)
		return nil, err
	}

	pE.PaymentOrderId = response.Data.ID
	_, err = engine.Insert(pE)
	if err != nil {
		logger.Error("FMPayService_CreateOrder_InsertDB_Err | err=%v", err)
		return nil, err
	}

	pE.PaymentLinkHost = fmt.Sprintf(u.COLLECT_URL_FMT, url.QueryEscape(u.ORDER_HOST), response.Data.ID)
	return pE, nil
}

func (u *FMPayService) Inquiry(orderId string, s2 string) (*model.PayEntry, error) {
	return nil, fmt.Errorf("no order[%v] data", orderId)
}

func (u *FMPayService) InquiryPayout(orderId string, ext string) (*model.PayOut, error) {
	reqData := map[string]string{
		FieldMerchantNo:      u.MERCHANT_NUM,
		FieldMerchantOrderId: orderId,
		FieldSignType:        "MD5",
	}

	reqData[FieldSign] = GetSign(reqData, u.MERCHANT_KEY)

	resp, err := u.Request(u.HOST_URL+"/pakistan/payoutQuery", "POST", "JSON", reqData, nil)
	if err != nil {
		logger.Error("FMPayService_Inquiry_Request_Error | err=%v", err)
		return nil, fmt.Errorf("inquiry payout error")
	}

	logger.Debug("FMPayService_Inquiry_Info | requestData=%v | response=%v", orderId, string(resp))

	res := &PayOutQueryResponse{}
	err = json.Unmarshal(resp, res)
	if err != nil {
		logger.Error("FMPayService_Inquiry_JsonUnmarshal_Error | err=%v | res=%v", err, res)
		return nil, fmt.Errorf("unmarshal response error")
	}

	// 提现流水不存在,请核实
	// false
	if res.Success != ReqStatusSuccess {
		return nil, fmt.Errorf("FMPayService_InQuiry_Error %v-%v", res.Code, res.Msg)
	}

	engine, _ := dao.GetMysql()
	payOut := new(model.PayOut)
	exist, err := engine.Where("order_id=?", orderId).Get(payOut)
	if err != nil {
		logger.Error("FMPayService_PayOut_Inquiry_GetError | err=%v | data=%v", err, orderId)
		return nil, err
	}
	if !exist {
		return nil, errors.New("NotExist")
	}

	status := res.Data.OrderState
	if status == PayOutStatusSuccess {
		payOut.Status = model.PAY_OUT_SUCCESS
		payOut.PaymentId = res.Data.PfOrderId
	} else if status == PayOutStatusFailure {
		payOut.Status = model.PAY_OUT_FAILD
		payOut.ThirdCode = status
		payOut.ThirdDesc = res.Data.OrderMsg
	} else {
		// 不做处理
	}

	return payOut, nil
}

func (u *FMPayService) PayOut(frozenId int64, args validator.PayOutValidator) (payOut *model.PayOut, err error) {
	orderId := utils.GetOrderId("FMOUT")

	payOut = new(model.PayOut)
	payOut.Uid = args.Uid
	payOut.AppId = args.AppId
	payOut.PayAccountId = u.PAY_ACCOUNT_ID
	payOut.PayChannel = model.FMPAY
	payOut.CountryCode = u.COUNTRY_CODE
	payOut.PayType = args.PayType
	payOut.AppOrderId = args.AppOrderId
	payOut.OrderId = orderId
	payOut.PaymentId = orderId
	payOut.FrozenId = frozenId
	payOut.Amount = args.Amount
	payOut.BankCard = args.BankCard
	payOut.UserId = args.UserId
	payOut.UserName = args.UserName
	payOut.Email = args.Email
	payOut.Paytm = args.PayTm
	payOut.IFSC = args.IFSC + "<-##->" + args.BankCode // 临时处理存储bank_code
	payOut.Phone = args.Phone
	engine, err := dao.GetMysql()
	if engine == nil || err != nil {
		logger.Error("FMPayService_PayOut_GetDB_Err | err=%v", err)
		return nil, err
	}
	_, err = engine.Insert(payOut)
	if err != nil {
		logger.Error("FMPayService_PayOut_Insert_Error | data=%+v | err=%v", payOut, err)
		return nil, err
	}

	/*
		reqData := u.genPayOutReqData(orderId, args)

		// 发起
		data, err := u.Request(u.HOST_URL+"/pakistan/payout", "POST", "JSON", reqData, nil)
		logger.Debug("FMPayService_PayOut_Info | reqData=%v | %v| err=%v | response=%v", reqData, args.AppOrderId, err, string(data))
		if err != nil {
			return nil, fmt.Errorf("payout order error")
		}

		response := &PayOutResponse{}
		err = json.Unmarshal(data, response)
		if err != nil {
			logger.Error("FMPayService_PayOut_JsonUnmarshal_Err | %v | err=%v | data=%v", args.AppOrderId, err, string(data))
			centerlog.OrderError(centerlog.MsgThirdPlatformFail, u.PAY_ACCOUNT_ID, args.AppId, args.AppOrderId, orderId, "false", err.Error())
			return nil, fmt.Errorf("unmarshal data error")
		}

		status := model.PAY_OUT_ING
		paymentId := response.Data.PfOrderId
		thirdCode := payOut.ThirdCode
		thirdDesc := payOut.ThirdDesc
		if response.Success != ReqStatusSuccess {
			logger.Error("FMPayService_PayOut_Status_Err | %v | %v-%v", args.AppOrderId, response.Code, response.Msg)
			status = model.PAY_OUT_FAILD
			thirdCode = fmt.Sprintf("%v", response.Code)
			//thirdDesc = response.Msg
			centerlog.OrderError(centerlog.MsgThirdPlatformFail, u.PAY_ACCOUNT_ID, payOut.AppId, payOut.AppOrderId, payOut.OrderId, response.Msg, "")
		}

		_, err = engine.Where("id=?", payOut.Id).Update(&model.PayOut{
			Status:    status,
			PaymentId: paymentId,
			ThirdCode: thirdCode,
			ThirdDesc: thirdDesc,
		})
		if err != nil {
			logger.Error("FMPayService_PayOut_Update_Error | data=%v | err=%v", payOut, err)
			return nil, err
		}

		payOut.Status = status
		payOut.PaymentId = paymentId
		payOut.ThirdDesc = thirdDesc
		payOut.ThirdCode = thirdCode
	*/
	return payOut, nil
}

func (u *FMPayService) PayOutBatch(frozenId int64, outValidator validator.PayOutValidator) (*model.PayOut, error) {
	return u.PayOut(frozenId, outValidator)
}

func (u *FMPayService) InSignature(cb interface{}) bool {
	data, ok := cb.(map[string]string)
	if !ok {
		return false
	}

	return VerifySign(data, u.MERCHANT_KEY)
}

func (u *FMPayService) OutSignature(cb interface{}) bool {
	return u.InSignature(cb)
}

var counter = uint64(rand.Int63n(8192))

func (u *FMPayService) genPayInReqData(orderId string, args validator.PayEntryValidator) (data map[string]string) {
	amount := fmt.Sprintf("%.2f", args.Amount) // 两位小数

	nVal := atomic.AddUint64(&counter, 1)
	buyerId := (time.Now().Unix()%8192)<<13 | int64(nVal%8192)

	//
	valPhone := "0000"
	sz := len(args.Phone)
	if sz > 4 {
		valPhone = args.Phone[sz-4:]
	}
	data = map[string]string{
		FieldMerchantNum: u.MERCHANT_NUM,
		FieldAppId:       u.APP_ID,
		FieldOrderNo:     orderId,
		FieldPayType:     "bankcard",
		FieldAmount:      amount,
		FieldNotifyUrl:   u.IN_NOTIFY_URL,
		FieldBuyerId:     fmt.Sprintf("%04d%s%08d", args.Uid, valPhone, buyerId), // 尽量不要重复就行，统一用户的相同金额的订单，如果短时间内没支付的话（1小时以内），再次唤醒支付会返回老的订单号
		FieldPublishTime: time.Now().Format("2006-01-02 15:04:05"),
	}

	data[FieldSign] = GetSign(data, u.MERCHANT_KEY)
	return
}

func (u *FMPayService) genPayOutReqData(orderId string, args validator.PayOutValidator) (data map[string]string) {
	amount := fmt.Sprintf("%d", int64(args.Amount)) // 两位小数

	data = map[string]string{
		FieldMerchantNo:       u.MERCHANT_NUM,
		FieldMerchantOrderId:  orderId,
		FieldMerchantOrderAmt: amount,
		FieldPayType:          "03",
		FieldNotifyUrl:        u.OUT_NOTIFY_URL,
		FieldServiceFeeType:   "01", // 00-手续费从提现金额种扣除，比如提现1000，扣除10%手续费，用户导致900;; 01-手续费从余额中扣除，还是前面的例子，客户到账1000，从账户余额扣除手续费
		FieldPayeeName:        args.UserName,
		FieldBankAcc:          args.BankCard,
		FieldBankCode:         args.BankCode,
		FieldSignType:         "MD5",
	}

	data[FieldSign] = GetSign(data, u.MERCHANT_KEY)
	return
}

func (u *FMPayService) Request(url string, method string, contentType string, sendData interface{}, header map[string][]string) (res []byte, err error) {
	var client = &http.Client{}
	var req *http.Request
	if contentType == "JSON" {
		data, _ := json.Marshal(sendData)
		fmt.Println(string(data))
		reader := bytes.NewBuffer(data)
		req, err = http.NewRequest(method, url, reader)
	} else {
		s := sendData.(string)
		reader := strings.NewReader(s)
		req, err = http.NewRequest(method, url, reader)
	}
	//提交请求
	if err != nil {
		logger.Error("FMPayService_Create_order | url=%v | err=%+v | req=%+v", url, err, sendData)
		return
	}
	if header == nil {
		req.Header = http.Header{
			"Content-Type": {"application/json; charset=utf-8"},
		}
	} else {
		req.Header = http.Header(header)
	}

	//处理返回结果
	response, err := client.Do(req)
	if err != nil {
		logger.Error("FMPayService_request_err | url=%v | err=%+v | req=%+v", url, err, sendData)
		return
	}
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		logger.Error("FMPayService_response_read_err | url=%v | err=%+v | req=%+v", url, err, sendData)
		return
	}
	defer func() {
		_ = response.Body.Close()
	}()
	return body, nil
}

func OrderInfo(orderId string) ([]byte, error) {
	key := fmt.Sprintf("fmpay:%v", orderId)
	data := dao.RedisGetBytes(key)
	if len(data) > 10 {
		return data, nil
	}

	fmOrderUrl := fmt.Sprintf("http://fundmxpay.com:8080/api/getOrderGatheringCode?orderNo=%v", orderId)
	data, err := Get(fmOrderUrl)
	logger.Debug("FMPayService_GetOrder_Info | order=%v | err=%v | response=%v", fmOrderUrl, err, string(data))
	if err != nil {
		return nil, fmt.Errorf("fm get order error")
	}

	orderResponse := &PayInOrderInfoResponse{}
	err = json.Unmarshal(data, orderResponse)
	if err != nil {
		logger.Debug("FMPayService_GetOrder_UnmarshalOrder | order=%v | err=%v | response=%v", fmOrderUrl, err, string(data))
		return nil, fmt.Errorf("fm unmarshal order error")
	}

	resMap := map[string]interface{}{
		"orderPayAmount": orderResponse.Data.GatheringAmount,
		"orderAmount":    orderResponse.Data.OrderAmount,
		"orderState":     orderResponse.Data.OrderState,
		"submitTime":     orderResponse.Data.SubmitTime,
		"endTime":        orderResponse.Data.UsefulTime,
		"channelCode":    orderResponse.Data.GatheringChannelCode,
		"channelName":    orderResponse.Data.GatheringChannelName,
		"accountBank":    orderResponse.Data.OpenAccountBank,
		"accountHolder":  orderResponse.Data.AccountHolder,
		"accountCard":    orderResponse.Data.BankCardAccount,
	}

	resData, err := json.Marshal(resMap)
	if err != nil {
		logger.Error("FMPayService_CreateOrder_Marshal_Err | err=%v", err)
		return nil, err
	}

	dao.RedisSetBytes(key, resData, 3600)
	return resData, nil
}

func Get(url string) (res []byte, err error) {
	response, err := http.Get(url)
	if err != nil {
		logger.Error("FMPayService_get_err | url=%v | err=%+v ", url, err)
		return
	}

	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		logger.Error("FMPayService_get_read_err | url=%v | err=%+v", url, err)
		return
	}
	defer func() {
		_ = response.Body.Close()
	}()
	return body, nil
}

func GetSign(data map[string]string, key string) string {
	keys := make([]string, 0, len(data))
	for k, _ := range data {
		if k == FieldSign {
			continue
		}

		keys = append(keys, k)
	}

	sort.Strings(keys)

	str := ""
	for _, k := range keys {
		d := strings.TrimSpace(data[k])
		if len(d) < 1 {
			continue
		}

		str = str + fmt.Sprintf("%v=%v&", k, d)
	}

	str += key

	return fmt.Sprintf("%x", md5.Sum([]byte(str)))
}

func VerifySign(data map[string]string, key string) bool {
	sign := data[FieldSign]

	return sign == GetSign(data, key)
}
