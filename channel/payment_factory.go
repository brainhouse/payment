package channel

import (
	"funzone_pay/channel/airpay"
	"funzone_pay/channel/bhtpay"
	"funzone_pay/channel/cashfree"
	"funzone_pay/channel/cashpay"
	"funzone_pay/channel/clickncash"
	"funzone_pay/channel/eaglepay"
	"funzone_pay/channel/easebuzz"
	"funzone_pay/channel/fidypay"
	"funzone_pay/channel/fmpay"
	"funzone_pay/channel/globpay"
	"funzone_pay/channel/hopepay"
	"funzone_pay/channel/hdpay"
	"funzone_pay/channel/loogpay"
	"funzone_pay/channel/oepay"
	"funzone_pay/channel/onionpay"
	"funzone_pay/channel/opay"
	"funzone_pay/channel/pagsmile"
	"funzone_pay/channel/payflash"
	"funzone_pay/channel/payg"
	"funzone_pay/channel/paytm"
	"funzone_pay/channel/payu"
	"funzone_pay/channel/pdkpay"
	"funzone_pay/channel/phonepay"
	"funzone_pay/channel/razorpay"
	"funzone_pay/channel/serpay"
	"funzone_pay/channel/uppay"
	"funzone_pay/channel/worldline"
	"funzone_pay/channel/ybpay"
	"funzone_pay/channel/ydgpay"
	"funzone_pay/channel/zwpay"
	"funzone_pay/model"
)

func GetPayInstance(payChannel model.PayChannel) (payIns PaymentContract) {
	switch payChannel {
	case model.RAZORPAY:
		payIns = razorpay.GetRzPay()
	case model.CASHFREE:
		payIns = cashfree.GetCashFree()
	case model.PAYTM:
		payIns = paytm.GetPayTm()
	case model.PAYU:
		payIns = payu.GetPayU()
	case model.PAYU2:
		payIns = payu.GetPayU2()
	case model.PAYU3:
		payIns = payu.GetPayU3()
	}
	return
}

func GetInstance(payChannel model.PayChannel, inConf map[string]interface{}) interface{} {
	switch payChannel {
	case model.CASHPAY:
		return cashpay.GetCashPayIns(inConf)
	}

	return nil
}

func GetPayObj(payChannel model.PayChannel, inConf map[string]interface{}) (payIns PaymentContract, err error) {
	switch payChannel {
	case model.RAZORPAY:
		payIns = razorpay.GetRzIns(inConf)
	case model.CASHFREE:
		payIns = cashfree.GetCashFreeIns(inConf)
	case model.PAYTM:
		payIns = paytm.GetPayTmIns(inConf)
	case model.PAYU:
		payIns = payu.GetPayUIns(inConf)
	case model.EASEBUZZ:
		payIns = easebuzz.GetPayEBIns(inConf)
	case model.EAGLEPAY:
		payIns = eaglepay.GetEaglePayIns(inConf)
	case model.ONIONPAY:
		payIns = onionpay.GetOnionPayIns(inConf)
	case model.SERPAY:
		payIns = serpay.GetSerPayIns(inConf)
	case model.FIDYPAY:
		payIns = fidypay.GetFidyPayIns(inConf)
	case model.PAGSMILE:
		payIns = pagsmile.GetPagSmilePayIns(inConf)
	case model.GLOBPAY:
		payIns = globpay.GetGlobPayIns(inConf)
	case model.PAYFLASH:
		payIns = payflash.GetPayFlashIns(inConf)
	case model.PDKPAY:
		payIns = pdkpay.GetPPayIns(inConf)
	case model.ZWPAY:
		payIns = zwpay.GetZWPayIns(inConf)
	case model.FMPAY:
		payIns = fmpay.GetFMPayIns(inConf)
	case model.YBPAY:
		payIns = ybpay.GetYBIns(inConf)
	case model.LOOGPAY:
		payIns = loogpay.GetLoogPayIns(inConf)
	case model.YDGPAY:
		payIns = ydgpay.GetYDGIns(inConf)
	case model.AIRPAY:
		payIns = airpay.GetAirPayIns(inConf)
	case model.CLICKNCASH:
		payIns = clickncash.GetClickNCashPayIns(inConf)
	case model.PAYG:
		payIns = payg.GetPayGIns(inConf)
	case model.BHTPAY:
		payIns = bhtpay.GetBHTPayService(inConf)
	case model.OEPAY:
		payIns = oepay.GetOEPayIns(inConf)
	case model.UPPAY:
		payIns = uppay.GetUpPayIns(inConf)
	case model.OPAY: //和OEPay一样的换个名字
		payIns = opay.GetOEPayIns(inConf)
	case model.HOPEPAY:
		payIns = hopepay.GetHopePayIns(inConf)
	case model.HDPAY:
		payIns = hdpay.GetHDPayIns(inConf)
	case model.CASHPAY:
		payIns = cashpay.GetCashPayIns(inConf)
	case model.WDLPAY:
		payIns = worldline.GetWDLIns(inConf)
	case model.PhonePay:
		payIns = phonepay.GetPhonePayIns(inConf)
	}
	return
}

func GetUPIObj(payChannel model.PayChannel, inConf map[string]interface{}) (payIns UPIContract, err error) {
	switch payChannel {
	case model.FIDYPAY:
		payIns = fidypay.GetFidyUPIPayIns(inConf)
	}
	return
}
