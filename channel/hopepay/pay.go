package hopepay

import (
	"crypto/md5"
	"encoding/json"
	"errors"
	"fmt"
	"funzone_pay/app/validator"
	"funzone_pay/common"
	"funzone_pay/dao"
	"funzone_pay/model"
	"funzone_pay/utils"
	"github.com/wonderivan/logger"
	"net/http"
	"sort"
	"strings"
	"time"
)

const (
//400	参数错误或商户未配置
//401	签名验证失败
//404	商户或订单不存在
//10001	订单已存在
//10002	渠道暂不可用
//10003	商户余额不足

	HopeStatusFail = 0
	HopeStatusSuccess = 1

	PayStatusPending   = "0" // pending
	PayStatusSuccess   = "1" // success
	PayStatusFail      = "2" // fail

	PayoutStatusCredited   = "Credited"   // Credited
	PayoutStatusProcessing = "Processing" // Processing
	PayoutStatusPending    = "Pending"    // Pending
	PayoutStatusRejected   = "REJECTED"
)

const (
	HPFieldMerchantNo = "merchantNo"
	HPFieldMerTradeNo = "merTradeNo"
	HPFieldAmount = "amount"
	HPFieldName = "name"
	HPFieldTel = "tel"
	HPFieldEmail = "email"
	HPFieldType = "type"
	HPFieldCallBackUrl = "callbackUrl"
	HPFieldExtra = "extra"
	HPFieldSign = "sign"
	HPFieldSignKey = "signKey"

	HPFieldAccCode = "accCode"
	HPFieldAccount = "account"
	)

type HopePayService struct {
	HOST           string
	APPID          string
	PAY_ACCOUNT_ID string
	PAY_CHANNEL    string
	COUNTRY_CODE   int
	IN_SK          string
	OUT_SK         string
	IN_CB          string
	OUT_CB         string
	CLIENT_ID      string
}

func GetHopePayIns(conf map[string]interface{}) *HopePayService {
	return &HopePayService{
		HOST:           conf["HOST"].(string),
		PAY_ACCOUNT_ID: conf["PAY_ACCOUNT_ID"].(string),
		PAY_CHANNEL:    conf["PAY_CHANNEL"].(string),
		COUNTRY_CODE:   conf["COUNTRY_CODE"].(int),

		IN_SK:     conf["IN_SK"].(string),
		OUT_SK:    conf["OUT_SK"].(string),
		IN_CB:     conf["IN_CB"].(string),
		OUT_CB:    conf["OUT_CB"].(string),
		CLIENT_ID: conf["CLIENT_ID"].(string),
	}
}

func (hd HopePayService) CreateOrder(args validator.PayEntryValidator) (*model.PayEntry, error) {
	orderId := utils.GetOrderId("HI")

	var err error = nil
	isAlarm := true
	defer func() {
		if isAlarm {
			common.PushAlarmEvent(common.Warning, fmt.Sprintf("%v", args.Uid), "HopePayService/CreateOrder", "", fmt.Sprintf("%v-%v Create Order By Hopepay: %v-%v", args.AppId, args.Uid, orderId, err))
		}
	}()


	amount := fmt.Sprintf("%.2f", args.Amount)
	req := map[string]string {
		HPFieldMerchantNo: hd.CLIENT_ID,
		HPFieldMerTradeNo: orderId,
		HPFieldAmount:  amount,
		HPFieldName: args.UserName,
		HPFieldEmail: fmt.Sprintf("%xH%x@gm.com", args.Phone, time.Now().Unix()), // Hopepay return same pay link, when same user try multi pay in 10 min
		HPFieldTel: args.Phone,
		HPFieldType: "upi",
		HPFieldCallBackUrl: hd.IN_CB,
		HPFieldExtra: "hopepay1",
	}

	req[HPFieldSign] = Sign(req, hd.IN_SK)

	reqData, err := json.Marshal(req)
	if err != nil {
		logger.Error("HopePayService_Create_order | %v | req=%+v", err, req)
		return nil, err
	}

	head := http.Header{
		"Content-Type": {"application/json"},
	}
	code, res, err := utils.Post(hd.HOST + "/ext_api/v1/payment/add", reqData, head)
	logger.Debug("HopePayService_Create| %v | %v | res=%+v", orderId, args.AppOrderId, string(res))
	if code != http.StatusOK || err != nil {
		logger.Error("HopePayService_Create_order | %v | req=%+v", err, string(reqData))
		err = fmt.Errorf("h channel busy %v", err)
		return nil, fmt.Errorf("h channel busy")
	}

	resData := HPInResponse{}
	err = Unmarshal(res, hd.IN_SK, &resData)
	if err != nil {
		logger.Error("HopePayService_Create_unmarshal error | %v | %v | req=%+v", orderId, args.AppOrderId, err)
		return nil, err
	}

	if resData.Status != HopeStatusSuccess || resData.Data.PayLink == "" {
		logger.Error("HopePayService_Create error | %v | %v |channel busy", orderId, args.AppOrderId )
		err = fmt.Errorf("h channel resp busy %v", resData.ErrorCode)
		return nil, fmt.Errorf("h channel busy")
	}

	//请求成功写入流水
	pE := new(model.PayEntry)
	pE.AppId = args.AppId
	pE.Uid = args.Uid
	pE.PayAccountId = hd.PAY_ACCOUNT_ID
	pE.PayChannel = model.HOPEPAY
	pE.CountryCode = hd.COUNTRY_CODE
	pE.AppOrderId = args.AppOrderId
	pE.OrderId = orderId
	pE.PaymentOrderId = resData.Data.OrderNo
	pE.Amount = args.Amount
	pE.UserId = args.UserId
	pE.UserName = args.UserName
	pE.Email = args.Email
	pE.Phone = args.Phone
	pE.Status = model.PAYING
	pE.Created = time.Now().Unix()

	//
	engine, err := dao.GetMysql()
	if engine == nil || err != nil {
		logger.Error("HopePayService_CreateOrder_GetDB_Err | err=%v", err)
		return nil, err
	}
	_, err = engine.Insert(pE)
	if err != nil {
		logger.Error("HopePayService_CreateOrder_InsertDB_Err | err=%v", err)
		return nil, err
	}

	isAlarm = false
	pE.PaymentLinkHost =  resData.Data.PayLink
	return pE, nil
}

func (hd HopePayService) Inquiry(orderId string, n string) (*model.PayEntry, error) {
	// order
	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("HopePayService_Inquiry_Engine_Error | data=%+v | err=%v", orderId, err)
		return nil, err
	}
	payEntry := new(model.PayEntry)
	exist, err := engine.Where("order_id=?", orderId).Get(payEntry)
	if err != nil {
		logger.Error("HopePayService_Inquiry_GetError | err=%v | data=%v", err, orderId)
		return nil, err
	}
	if !exist {
		return nil, errors.New("NotExist")
	}

	if payEntry.Status != model.PAYING {
		return payEntry, nil
	}


	req := map[string]string {
		HPFieldMerchantNo: hd.CLIENT_ID,
		HPFieldMerTradeNo: orderId,
	}

	req[HPFieldSign] = Sign(req, hd.IN_SK)

	reqData, err := json.Marshal(req)
	if err != nil {
		logger.Error("HopePayService_Inquiry | %v | req=%+v", err, req)
		return nil, err
	}

	head := http.Header{
		"Content-Type": {"application/json"},
	}
	code, res, err := utils.Post(hd.HOST + "/ext_api/v1/payment/query", reqData, head)
	logger.Debug("HopePayService_Inquiry | %v | req=%+v | %v", err, req, string(res))
	if code != http.StatusOK || err != nil {
		return nil, fmt.Errorf("hd query channel error")
	}

	resData := HPInQueryResponse{}
	err = Unmarshal(res, hd.IN_SK, &resData)
	if err != nil {
		logger.Error("HopePayService_Inquiry_unmarshal error | %v | req=%+v", orderId, err)
		return nil, err
	}

	if resData.Status != HopeStatusSuccess || resData.Data.MerTradeNo != orderId {
		logger.Error("HopePayService_Inquiry Error: %v | %v | %v", orderId, resData.ErrorCode, resData.Message)
		return nil, err
	}

	if resData.Data.OrderStatus == PayStatusPending {
		return payEntry, nil
	}

	if resData.Data.OrderStatus == PayStatusSuccess {
		payEntry.Status = model.PAYSUCCESS
	} else {
		payEntry.Status = model.PAYFAILD
		payEntry.ThirdCode = resData.ErrorCode
		payEntry.ThirdDesc = resData.Message
	}

	return payEntry, nil
}

func (hd HopePayService) InquiryPayout(orderId string, paymentId string) (*model.PayOut, error) {
	// order
	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("HopePayService_Inquiry_Engine_Error | data=%+v | err=%v", orderId, err)
		return nil, err
	}
	payOut := new(model.PayOut)
	exist, err := engine.Where("order_id=?", orderId).Get(payOut)
	if err != nil {
		logger.Error("HopePayService_Inquiry_GetError | err=%v | data=%v", err, orderId)
		return nil, err
	}
	if !exist {
		return nil, errors.New("NotExist")
	}

	//
	if payOut.Status != model.PAY_OUT_ING {
		return payOut, nil
	}

	req := map[string]string {
		HPFieldMerchantNo: hd.CLIENT_ID,
		HPFieldMerTradeNo: orderId,
	}

	req[HPFieldSign] = Sign(req, hd.OUT_SK)

	reqData, err := json.Marshal(req)
	if err != nil {
		logger.Error("HopePayService_Inquiry | %v | req=%+v", err, req)
		return nil, err
	}

	head := http.Header{
		"Content-Type": {"application/json"},
	}
	code, res, err := utils.Post(hd.HOST + "/ext_api/v1/payout/query", reqData, head)
	logger.Debug("HopePayService_Inquiry | %v | req=%+v | %v", err, req, string(res))
	if code != http.StatusOK || err != nil {
		return nil, fmt.Errorf("hd query channel error")
	}

	// for alarm
	resAlarm := HPOutQueryResponse{}
	json.Unmarshal(res, &resAlarm)
	// //{"status":0,"errorCode":"404","message":"Not found the order"}
	if  resAlarm.Status == 0 && resAlarm.ErrorCode == "404" && resAlarm.Message == "Not found the order" {
		// Alarm
		logger.Debug("HopePayService_Inquiry_Alarm Not found the order | %v", orderId)
		common.PushAlarmEvent(common.Warning, fmt.Sprintf("%v", payOut.Uid), "HopePayService/QueryOut", "",
			fmt.Sprintf("%v-%v-%v hopepay->payout fail [%v] msg:%v:%v", payOut.AppId, payOut.Uid, payOut.AppOrderId, orderId, resAlarm.ErrorCode, resAlarm.Message))
	}

	resData := HPOutQueryResponse{}
	err = Unmarshal(res, hd.OUT_SK, &resData)

	if err != nil {
		logger.Error("HopePayService_Inquiry_unmarshal error | %v | req=%+v", orderId, err)
		return nil, err
	}

	if resData.Status != HopeStatusSuccess || resData.Data.MerTradeNo != orderId {
		logger.Error("HopePayService_Inquiry Error: %v | %v | %v", orderId, resData.ErrorCode, resData.Message)
		return nil, err
	}

	if resData.Data.OrderStatus == PayStatusPending {
		return payOut, nil
	}

	if resData.Data.OrderStatus == PayStatusSuccess {
		payOut.Status = model.PAY_OUT_SUCCESS
	} else {
		payOut.Status = model.PAY_OUT_FAILD
		payOut.ThirdCode = resData.ErrorCode
		payOut.ThirdDesc = resData.Message
	}

	return payOut, nil
}

func (hd *HopePayService) PayOut(frozenId int64, args validator.PayOutValidator) (payOut *model.PayOut, err error) {
	if args.PayType != model.PT_BANK && args.PayType != model.PT_UPI {
		return nil, fmt.Errorf("unsupported type[%v] from %v", args.PayType, args.AppId)
	}

	orderId := utils.GetOrderId("HO")

	isAlarm := true
	forceAlarm := false
	msg := ""
	defer func() {
		if isAlarm || forceAlarm {
			logger.Debug("HopePayService_PayOut_Alarm:%v|%v", orderId, msg)
			common.PushAlarmEvent(common.Warning, fmt.Sprintf("%v", args.Uid), "HopePayService/Payout", "", fmt.Sprintf("%v-%v-%v payout[%v] msg:%v, error:%v", args.AppId, args.Uid, args.AppOrderId, orderId, msg, err))
		}
	}()

	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("HopePayServicePayOut_Engine_Error | data=%+v | err=%v", payOut, err)
		return nil, err
	}

	payOut = new(model.PayOut)
	payOut.Uid = args.Uid
	payOut.AppId = args.AppId
	payOut.PayAccountId = hd.PAY_ACCOUNT_ID
	payOut.PayChannel = model.HOPEPAY
	payOut.CountryCode = hd.COUNTRY_CODE
	payOut.PayType = args.PayType
	payOut.AppOrderId = args.AppOrderId
	payOut.OrderId = orderId
	payOut.FrozenId = frozenId
	payOut.Amount = args.Amount
	payOut.BankCard = args.BankCard
	payOut.UserId = args.UserId
	payOut.UserName = args.UserName
	payOut.Email = args.Email
	payOut.Paytm = args.PayTm
	payOut.IFSC = args.IFSC
	payOut.Phone = args.Phone
	payOut.Status = model.PAY_OUT_ING
	payOut.Created = time.Now().Unix()
	_, err = engine.Insert(payOut)

	if err != nil {
		logger.Error("HopePayServicePayOut_PayOut_Insert_Error | data=%+v | err=%v", payOut, err)
		return nil, err
	}

	oType := "upi"
	if args.PayType == model.PT_BANK {
		oType = "ifsc"
	}

	amount := fmt.Sprintf("%.2f", args.Amount)
	req := map[string]string {
		HPFieldMerchantNo: hd.CLIENT_ID,
		HPFieldMerTradeNo: orderId,
		HPFieldAmount:  amount,
		HPFieldType: oType,
		HPFieldName: args.UserName,
		HPFieldTel: args.Phone,
		HPFieldEmail: args.Email,
		//HPFieldAccount: args.VPA,
		HPFieldCallBackUrl: hd.OUT_CB,
		HPFieldExtra: "hopepay1",
	}

	if args.PayType == model.PT_BANK {
		req[HPFieldAccount] = args.BankCard
		req[HPFieldAccCode] = args.IFSC
	} else {
		req[HPFieldAccount] = args.VPA
	}

	req[HPFieldSign] = Sign(req, hd.OUT_SK)

	reqData, err := json.Marshal(req)
	if err != nil {
		logger.Error("HopePayService_Payout | %v | req=%+v", err, req)
		return nil, err
	}

	head := http.Header{
		"Content-Type": {"application/json"},
	}
	code, res, err := utils.Post(hd.HOST + "/ext_api/v1/payout/add", reqData, head)
	logger.Debug("HopePayService_Payout| %v | %v | %v | res=%+v", orderId, args.AppOrderId, err, string(res))
	if code != http.StatusOK || err != nil {
		logger.Error("HopePayService_Payout | %v | req=%+v", err, string(reqData))
		err = fmt.Errorf("hd out channel busy %v", err)
		return payOut, fmt.Errorf("hd channel busy")
	}

	resData := HPOutResponse{}
	err = Unmarshal(res, hd.OUT_SK, &resData)
	if err != nil {
		msg = fmt.Sprintf("resp:%v", string(res))
		logger.Error("HopePayService_Payout_unmarshal error | %v | %v | req=%+v", orderId, args.AppOrderId, err)
		return payOut, err
	}

	//
	if resData.Status != HopeStatusSuccess {
		forceAlarm = true
		// update payout order status, when checkout order or callback
		_, err = engine.Where("id=?", payOut.Id).Update(&model.PayOut{
			PaymentId: resData.Data.OrderNo,
			ThirdCode:  resData.ErrorCode,
			ThirdDesc:  resData.Message,
		})
		if err != nil {
			logger.Error("HopePayService_PayOut_Update_Error | data=%v | err=%v", payOut, err)
			return payOut, err
		}
	}

	isAlarm = false
	return payOut, nil
}

func (hd HopePayService) PayOutBatch(frozenId int64, args validator.PayOutValidator) (*model.PayOut, error) {
	return hd.PayOut(frozenId, args)
}

func QueryBalance(host, merchantNo, key string) (b BalanceResponse, err error) {
	req := map[string]string{
		HPFieldMerchantNo: merchantNo,
	}
	req[HPFieldSign] = Sign(req, key)

	reqData, err := json.Marshal(req)
	if err != nil {
		logger.Error("HopePayService_Payout | %v | req=%+v", err, req)
		return
	}

	head := http.Header{
		"Content-Type": {"application/json"},
	}
	code, res, err := utils.Post(host + "/ext_api/v1/payout/query_balance", reqData, head)
	logger.Debug("HopePayService_Balance | %v", string(res))
	if code != http.StatusOK || err != nil {
		err = fmt.Errorf("hd channel busy")
		return
	}

	err = json.Unmarshal(res, &b)
	return
}

func Unmarshal(raw []byte, key string, v any) error {
	ret := HPResponse{}
	err := json.Unmarshal(raw, &ret)
	if err != nil {
		return err
	}

	sign := Sign(ret.Data, key)
	if sign != ret.Sign {
		logger.Error("HopePayService check sign error: %v != %v", sign, ret.Sign)
		return fmt.Errorf("check sign error")
	}

	return json.Unmarshal(raw, v)
}

func Sign(params map[string]string, key string) string {
	sortedKeys := make([]string, 0, len(params))
	for k := range params {
		sortedKeys = append(sortedKeys, k)
	}
	sort.Strings(sortedKeys)

	sortedValues := make([]string, 0, len(params))
	for _, element := range sortedKeys {
		param := params[element]
		if element == "" || param == "" || element == HPFieldSign {
			continue
		}
		sortedValues = append(sortedValues, fmt.Sprintf("%v=%v", element, param))
	}

	sortedValues = append(sortedValues, fmt.Sprintf("%v=%v", HPFieldSignKey, key))

	raw := strings.Join(sortedValues, "&")

	return fmt.Sprintf("%X", md5.Sum([]byte(raw)))
}

func (hd HopePayService) InSignature(cb interface{}) bool {
	data, ok := cb.(map[string]string)
	if !ok {
		return false
	}

	sign := Sign(data, hd.IN_SK)
	return sign == data[HPFieldSign]
}

func (hd HopePayService) OutSignature(cb interface{}) bool {
	data, ok := cb.(map[string]string)
	if !ok {
		return false
	}

	sign := Sign(data, hd.OUT_SK)
	return sign == data[HPFieldSign]
}