package hopepay

type HPResponse struct {
	Status int               `json:"status"`
	Sign   string            `json:"sign"`
	Data   map[string]string `json:"data"`
}

type HPInResponse struct {
	Status int    `json:"status"`
	Sign   string `json:"sign"`
	Data   struct {
		PayLink    string `json:"payLink"`
		OrderNo    string `json:"orderNo"`
		MerTradeNo string `json:"merTradeNo"`
	} `json:"data"`

	ErrorCode string `json:"errorCode"`
	Message   string `json:"message"`
}

type HPInQueryResponse struct {
	Status int    `json:"status"`
	Sign   string `json:"sign"`
	Data   struct {
		OrderNo      string `json:"orderNo"`
		MerchantNo   string `json:"merchantNo"`
		MerTradeNo   string `json:"merTradeNo"`
		Amount       string `json:"amount"`
		ActualAmount string `json:"actualAmount"`
		Poundage     string `json:"poundage"`
		OrderStatus  string `json:"orderStatus"`
		CreateTime   string `json:"createTime"`
	} `json:"data"`

	ErrorCode string `json:"errorCode"`
	Message   string `json:"message"`
}

type HPOutResponse struct {
	Status int    `json:"status"`
	Sign   string `json:"sign"`
	Data   struct {
		OrderNo    string `json:"orderNo"`
		MerTradeNo string `json:"merTradeNo"`
	} `json:"data"`

	ErrorCode string `json:"errorCode"`
	Message   string `json:"message"`
}

type HPOutQueryResponse struct {
	Status int    `json:"status"`
	Sign   string `json:"sign"`
	Data   struct {
		OrderNo     string `json:"orderNo"`
		MerchantNo  string `json:"merchantNo"`
		MerTradeNo  string `json:"merTradeNo"`
		Amount      string `json:"amount"`
		Poundage    string `json:"poundage"`
		OrderStatus string `json:"orderStatus"`
		CreateTime  string `json:"createTime"`
		Type        string `json:"type"`
	} `json:"data"`
	ErrorCode string `json:"errorCode"`
	Message   string `json:"message"`
}

type BalanceResponse struct {
	Status int    `json:"status"`
	Sign   string `json:"sign"`
	Data   struct {
		MerchantNo      string `json:"merchantNo"`
		Currency        string `json:"currency"`
		AccountBalance  string `json:"accountBalance"`
		UnsettledAmount string `json:"unsettledAmount"`
		FrozenAmount    string `json:"frozenAmount"`
	} `json:"data"`
	ErrorCode string `json:"errorCode"`
	Message   string `json:"message"`
}

type HPCallBack struct {
	OrderNo      string `json:"orderNo"`
	MerchantNo   string `json:"merchantNo"`
	MerTradeNo   string `json:"merTradeNo"`
	Amount       string `json:"amount"`
	ActualAmount string `json:"actualAmount"`
	Poundage     string `json:"poundage"`
	OrderStatus  string `json:"orderStatus"`
	CreateTime   string `json:"createTime"`
	Sign         string `json:"sign"`
}


func (c *HPCallBack) Map() map[string]string {
	return map[string]string{
		"orderNo":      c.OrderNo,
		"merchantNo":   c.MerchantNo,
		"merTradeNo":   c.MerTradeNo,
		"amount":       c.Amount,
		"actualAmount": c.ActualAmount,
		"poundage":     c.Poundage,
		"orderStatus":  c.OrderStatus,
		"createTime":   c.CreateTime,
		"sign":         c.Sign,
	}
}

type HPOutCallBack struct {
	OrderNo     string `json:"orderNo"`
	MerchantNo  string `json:"merchantNo"`
	MerTradeNo  string `json:"merTradeNo"`
	Amount      string `json:"amount"`
	Poundage    string `json:"poundage"`
	OrderStatus string `json:"orderStatus"`
	CreateTime  string `json:"createTime"`
	Type        string `json:"type"`
	Sign        string `json:"sign"`
}

func (c *HPOutCallBack) Map() map[string]string {
	return map[string]string{
		"orderNo":     c.OrderNo,
		"merchantNo":  c.MerchantNo,
		"merTradeNo":  c.MerTradeNo,
		"amount":      c.Amount,
		"poundage":    c.Poundage,
		"orderStatus": c.OrderStatus,
		"createTime":  c.CreateTime,
		"type":        c.Type,
		"sign":        c.Sign,
	}
}