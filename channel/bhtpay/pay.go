package bhtpay

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"funzone_pay/app/validator"
	"funzone_pay/centerlog"
	"funzone_pay/common"
	"funzone_pay/dao"
	"funzone_pay/model"
	"funzone_pay/utils"
	"github.com/wonderivan/logger"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
	"time"
)

type BHTPayService struct {
	Host           string
	APPID          string
	PAY_ACCOUNT_ID string
	PAY_CHANNEL    string
	COUNTRY_CODE   int
	TOKEN          string
}

func GetBHTPayService(conf map[string]interface{}) *BHTPayService {
	return &BHTPayService{
		PAY_ACCOUNT_ID: conf["PAY_ACCOUNT_ID"].(string),
		PAY_CHANNEL:    conf["PAY_CHANNEL"].(string),
		COUNTRY_CODE:   conf["COUNTRY_CODE"].(int),
		Host:           conf["HOST"].(string),
		TOKEN:          conf["TOKEN"].(string),
	}
}

func (bh *BHTPayService) CreateOrder(args validator.PayEntryValidator) (*model.PayEntry, error) {
	orderId := utils.GetOrderId("BHT")
	/**
	customer_name:parikshit
	customer_email:parikshitvaghasiya343@gmail.com
	customer_mobile:7203964072
	redirect_url:https://google.com
	p_info:Product Name
	amount:1
	//udf1:
	//udf2:
	//udf3:
	upi_txn_id:asdsdfffdffs1123
	//customer_vpa:testabc@paytm
	*/
	sendData := url.Values{}
	sendData.Add("customer_email", args.Email)
	sendData.Add("customer_mobile", args.Phone)
	sendData.Add("customer_name", args.UserName)
	sendData.Add("redirect_url", "https://pay.rummybuffett.com/#/callback_?type=1")
	sendData.Add("p_info", "payforcharge")
	sendData.Add("amount", fmt.Sprint(args.Amount))
	sendData.Add("upi_txn_id", orderId)

	isAlarm := false
	msg := ""
	defer func() {
		if isAlarm {
			common.PushAlarmEvent(common.Warning, fmt.Sprintf("%v", args.Uid), "BHTPayService/CreateOrder", "", fmt.Sprintf("%v-%v Create Order By BHTPay Error->%v", args.AppId, args.Uid, msg))
		}
	}()

	header := map[string][]string{
		"Authorization": {fmt.Sprintf("Bearer %v", bh.TOKEN)},
		"Content-Type":  {"application/x-www-form-urlencoded"},
	}
	res, err := bh.Request(fmt.Sprintf("%v%v", bh.Host, "/api/upi_gateway/create_order"), http.MethodPost, "", sendData.Encode(), header)
	if err != nil {
		logger.Error("BHTPayService_CreateOrder_Err req=%v | err=%v", sendData.Encode(), err)
		isAlarm = true
		msg = err.Error()
		return nil, err
	}
	logger.Debug("BHTPayService_CreateOrder_Info | req=%v | response=%v", sendData.Encode(), string(res))
	data := &struct {
		Success int    `json:"success"`
		Message string `json:"message"`
		Data    struct {
			OrderId json.Number `json:"order_id"`
			UPILink string      `json:"payment_url"`
		} `json:"data"`
	}{}
	err = json.Unmarshal(res, data)
	if err != nil {
		isAlarm = true
		msg = err.Error()
		logger.Error("BHTPayService_JsonUnmarshal_Error | err=%v | res=%+v", err, string(res))
		return nil, err
	}

	if len(data.Data.UPILink) < 1 {
		isAlarm = true
		msg = fmt.Sprintf("%v", data.Message)
		return nil, fmt.Errorf("create in[%v] error", args.AppOrderId)
	}

	oId, _ := data.Data.OrderId.Int64()
	if oId != 0 {
		orderId = fmt.Sprintf("BHT%v", oId)
	}
	//请求成功写入流水
	pE := new(model.PayEntry)
	pE.AppId = args.AppId
	pE.Uid = args.Uid
	pE.PayAccountId = bh.PAY_ACCOUNT_ID
	pE.PayChannel = model.BHTPAY
	pE.CountryCode = bh.COUNTRY_CODE
	pE.AppOrderId = args.AppOrderId
	pE.OrderId = orderId
	pE.Amount = args.Amount
	pE.UserId = args.UserId
	pE.UserName = args.UserName
	pE.Email = args.Email
	pE.Phone = args.Phone
	pE.Status = model.PAYING
	pE.FinishTime = time.Now().Unix()

	engine, err := dao.GetMysql()
	if engine == nil || err != nil {
		logger.Error("BHTPayService_CreateOrder_GetDB_Err | err=%v", err)
		return nil, err
	}
	_, err = engine.Insert(pE)
	if err != nil {
		logger.Error("BHTPayService_CreateOrder_InsertDB_Err | err=%v", err)
		return nil, err
	}
	pE.PaymentLinkHost = data.Data.UPILink
	return pE, err
}

func (bh *BHTPayService) Inquiry(orderId string, t string) (*model.PayEntry, error) {
	//替换掉BHT查询
	orderId = strings.Replace(orderId, "BHT", "", -1)
	params := url.Values{}
	params.Add("order_id", orderId)

	requestUrl := fmt.Sprintf("%s%s", bh.Host, "/api/upi_gateway/status_check")
	header := map[string][]string{
		"Authorization": {fmt.Sprintf("Bearer %v", bh.TOKEN)},
		"Content-Type":  {"application/x-www-form-urlencoded"},
	}
	data, err := bh.Request(requestUrl, "POST", "", params.Encode(), header)
	if err != nil {
		logger.Error("BHTPayService_Collect_Inquiry_Error | err=%v", err)
		return nil, err
	}
	logger.Debug("BHTPayService_Collect_Inquiry_Data | orderId=%v | res=%v", orderId, string(data))
	resData := &struct {
		Success int `json:"success"`
		Data    struct {
			Status string `json:"status"`
		} `json:"data"`
	}{}
	err = json.Unmarshal(data, &resData)
	if err != nil {
		logger.Error("BHTPayService_CollectInquiry_JsonError | err=%v | data=%v", err, string(data))
		return nil, err
	}

	engine, _ := dao.GetMysql()
	payEntry := new(model.PayEntry)
	exist, err := engine.Where("order_id=?", orderId).Get(payEntry)
	if err != nil {
		logger.Error("BHTPayService_Collect_Inquiry_GetError | err=%v | data=%v", err, orderId)
		return nil, err
	}
	if !exist {
		return nil, errors.New("NotExist")
	}
	if resData.Data.Status == "FAILED" {
		payEntry.Status = model.PAYFAILD
	} else if resData.Data.Status == "SUCCESS" {
		payEntry.Status = model.PAYSUCCESS
	}
	return payEntry, nil
}

func (bh *BHTPayService) InquiryPayout(orderId string, ext string) (*model.PayOut, error) {
	return nil, fmt.Errorf("implement it")
}

func (bh *BHTPayService) PayOut(frozenId int64, args validator.PayOutValidator) (payOut *model.PayOut, err error) {
	//创建受益人
	var beneId string
	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("BHTPayService_PayOut_Engine_Error | data=%+v | err=%v", payOut, err)
		return nil, err
	}

	transId := utils.GetOrderId("BH")
	payOut = new(model.PayOut)
	payOut.Uid = args.Uid
	payOut.AppId = args.AppId
	payOut.PayAccountId = bh.PAY_ACCOUNT_ID
	payOut.PayChannel = model.BHTPAY
	payOut.CountryCode = bh.COUNTRY_CODE
	payOut.PayType = args.PayType
	payOut.AppOrderId = args.AppOrderId
	payOut.OrderId = transId
	payOut.FrozenId = frozenId
	payOut.Amount = args.Amount
	payOut.BankCard = args.BankCard
	payOut.UserId = args.UserId
	payOut.UserName = args.UserName
	payOut.Email = args.Email
	payOut.Paytm = args.PayTm
	payOut.IFSC = args.IFSC
	payOut.Phone = args.Phone
	_, err = engine.Insert(payOut)
	if err != nil {
		logger.Error("BHTPayService_PayOut_Insert_Error | data=%+v | err=%v", payOut, err)
		return nil, err
	}
	//payOut.Id = 10441223320
	if args.PayType == model.PT_BANK {
		//检查银行卡账号是否已经注册了
		session := engine.Where("bank_card=? AND pay_channel=? and bene_id!=''", args.BankCard, model.BHTPAY)
		if bh.PAY_ACCOUNT_ID != "" {
			session.Where("pay_account_id=?", bh.PAY_ACCOUNT_ID)
		}
		payHis := new(model.PayOut)
		exist, err := session.OrderBy("id desc").Get(payHis)
		if err != nil {
			logger.Error("BHTPayService_PayOut_BankCard_Engine_Error | data=%+v | err=%v", payOut, err)
			return nil, err
		}
		if exist && payHis.BeneId != "" {
			beneId = payHis.BeneId
		}
	} else {
		payOut.Status = model.PAY_OUT_FAILD
		return payOut, errors.New("NOT_SUPPORT")
	}
	if beneId == "" {
		var (
			benRes bool
			msg    string
		)
		benRes, beneId, msg = bh.AddBeneficiary(args)
		if !benRes {
			err = errors.New("AddBeneficiaryError")
			logger.Error("BHTPayService_PayOut_AddBeneficiaryError | data=%+v", args)
			_, _ = engine.Where("order_id=?", transId).Update(&model.PayOut{
				ThirdDesc: msg,
				Status:    model.PAY_OUT_FAILD,
			})
			payOut.Status = model.PAY_OUT_FAILD
			return payOut, err
		}
	}
	//发起支付
	requestUrl := fmt.Sprintf("%v/api/va/create_payout?token=%v&amount=%v&purpose=%v&ref_id=%v&txn_type=IMPS&bank_account_id=%v",
		bh.Host, bh.TOKEN, args.Amount, "payout", payOut.Id, beneId)
	header := map[string][]string{
		"Authorization": {fmt.Sprintf("Bearer %v", bh.TOKEN)},
	}

	data, err := bh.Request(requestUrl, "GET", "", "", header)
	if err != nil {
		logger.Error("BHTPayService_PayOut_Request_Error | err=%v", err)
		return nil, err
	}
	logger.Debug("BHTPayService_PayOut_Info | requestUrl=%v | response=%v", requestUrl, string(data))

	/**
	{
	    "data": {
	        "order_id": "8377589591",
	        "bank_account_id": "896",
	        "txn_type": "IMPS",
	        "amount": "1.00",
	        "status": "SUCCESS",
	        "purpose": "test",
	        "bank_remark": null,
	        "ref_id": "2147483647",
	        "created_at": "2023-02-11 18:50:52"
	    },
	    "message": "Payout Request Submitted Successfully.",
	    "success": 1
	}
	*/
	res := &struct {
		Success int    `json:"success"`
		Message string `json:"message"`
		Data    struct {
			RefId   string `json:"ref_id"`
			OrderId string `json:"order_id"`
		} `json:"data"`
	}{}
	err = json.Unmarshal(data, res)
	if err != nil {
		logger.Error("BHTPayService_PayOut_JsonUnmarshal_Error | err=%v | res=%v", err, res)
		centerlog.OrderError(centerlog.MsgThirdPlatformFail, payOut.PayAccountId, payOut.AppOrderId, payOut.AppId, payOut.OrderId, "false", err.Error())
		return nil, err
	}
	var (
		status      model.PayOutStatus
		thirdDesc   string
		referenceId string
	)
	if res.Success != 1 {
		//先不设置成失败
		status = model.PAY_OUT_FAILD
		payOut.Status = model.PAY_OUT_FAILD
		thirdDesc = res.Message
		centerlog.OrderError(centerlog.MsgThirdPlatformFail, payOut.PayAccountId, payOut.AppOrderId, payOut.AppId, payOut.OrderId, fmt.Sprint(res.Success), res.Message)
	} else {
		status = model.PAY_OUT_ING
		referenceId = res.Data.OrderId
	}
	_, err = engine.Where("id=?", payOut.Id).Update(&model.PayOut{
		Status:     status,
		BeneId:     beneId,
		OrderId:    fmt.Sprintf("%v%v", transId, payOut.Id),
		ThirdCode:  res.Message,
		PaymentId:  referenceId,
		ThirdDesc:  thirdDesc,
		FinishTime: time.Now().Unix(),
	})
	if err != nil {
		logger.Error("BHTPayService_PayOut_Insert_Error | data=%v | err=%v", payOut, err)
		return nil, err
	}
	return payOut, nil
}

func (bh *BHTPayService) AddBeneficiary(outValidator validator.PayOutValidator) (bool, string, string) {
	userName := strings.Replace(outValidator.UserName, " ", "", -1)
	email := strings.Replace(outValidator.Email, " ", "", -1)
	requestUrl := fmt.Sprintf("%v/api/va/create_payout_account?token=%v&bene_account_number=%v&ifsc_code=%v&recepient_name=%v&email_id=%v&mobile_number=%v",
		bh.Host, bh.TOKEN, outValidator.BankCard, outValidator.IFSC, userName, email, outValidator.Phone)
	header := map[string][]string{
		"Authorization": {fmt.Sprintf("Bearer %v", bh.TOKEN)},
	}

	data, err := bh.Request(requestUrl, "GET", "", "", header)
	if err != nil {
		logger.Error("BHTPayService_Authorize_Request_Error | err=%v | req=%v", err, outValidator)
		return false, "", ""
	}
	fmt.Println(string(data))
	logger.Debug("BHTPayService_AddBeneficiary_Info | data=%v | req=%v", string(data), requestUrl)
	/**
	{
	    "success": 1,
	    "message": "Payout Bank Account Created Successfully",
	    "data": {
	        "bank_account_id": "907",
	        "bene_account_number": "19110100017165",
	        "ifsc_code": "BARB0KASHIA",
	        "recepient_name": "NAME ABDUR RAHAMAN",
	        "email_id": "NAMEABDURRAHAMAN@gmail.com",
	        "mobile_number": "7203964072",
	        "status": "PENDING",
	        "remark": null
	    }
	}
	*/
	res := &struct {
		Success int    `json:"success"`
		Message string `json:"message"`
		Data    struct {
			BankAccountId string `json:"bank_account_id"`
		} `json:"data"`
	}{}
	err = json.Unmarshal(data, res)
	if err != nil {
		logger.Error("BHTPayService_AddBeneficiary_JsonUnmarshal_Error | err=%v | res=%+v", err, string(data))
		return false, "", ""
	}
	if res.Success != 1 {
		return false, "", res.Message
	}
	return true, res.Data.BankAccountId, ""
}

func (bh *BHTPayService) PayOutBatch(frozenId int64, entryValidator validator.PayOutValidator) (*model.PayOut, error) {
	return bh.PayOut(frozenId, entryValidator)
}

func (bh *BHTPayService) InSignature(cb interface{}) bool {
	panic("implement me")
}

func (bh *BHTPayService) OutSignature(cb interface{}) bool {
	panic("implement me")
}

func (bh *BHTPayService) Request(url string, method string, contentType string, sendData interface{}, header map[string][]string) (res []byte, err error) {
	client := &http.Client{}
	var req *http.Request
	if contentType == "JSON" {
		data, _ := json.Marshal(sendData)
		fmt.Println(string(data))
		reader := bytes.NewBuffer(data)
		req, err = http.NewRequest(method, url, reader)
	} else {
		s := sendData.(string)
		reader := strings.NewReader(s)
		req, err = http.NewRequest(method, url, reader)
	}
	fmt.Println(url)
	//提交请求
	if err != nil {
		logger.Error("BHTPayService_Create_order | url=%v | err=%+v | req=%+v", url, err, sendData)
		return
	}
	req.Header = http.Header(header)
	//处理返回结果
	response, err := client.Do(req)
	if err != nil {
		logger.Error("BHTPayService_request_err | url=%v | err=%+v | req=%+v", url, err, sendData)
		return
	}
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		logger.Error("BHTPayService_response_read_err | url=%v | err=%+v | req=%+v", url, err, sendData)
		return
	}
	defer func() {
		_ = response.Body.Close()
	}()
	return body, nil
}
