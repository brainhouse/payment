package bhtpay

import (
	"fmt"
	"funzone_pay/app/validator"
	"testing"
)

func TestGetBHTPayService(t *testing.T) {
	conf := make(map[string]interface{})
	conf["PAY_ACCOUNT_ID"] = "bhtpay1"
	conf["PAY_CHANNEL"] = "bhtpay"
	conf["COUNTRY_CODE"] = 366
	conf["HOST"] = "https://api.bharatpays.in"
	conf["TOKEN"] = "a2475fa54c6575862b2387c7c8ca6721"

	args := validator.PayOutValidator{
		UserName: "NAMEABDURRAHAMAN",
		BankCard: "19110100017165",
		IFSC:     "BARB0KASHIA",
		Phone:    "7203964072",
		Email:    "NAMEABDURRAHAMAN@gmail.com",
	}
	instance := GetBHTPayService(conf)
	//_, beneId, _ := instance.AddBeneficiary(args)

	payOut, err := instance.PayOut(args)
	fmt.Println(payOut)
	fmt.Println(err)
}

func TestCollect(t *testing.T) {
	conf := make(map[string]interface{})
	conf["PAY_ACCOUNT_ID"] = "bhtpay1"
	conf["PAY_CHANNEL"] = "bhtpay"
	conf["COUNTRY_CODE"] = 366
	conf["HOST"] = "https://api.bharatpays.in"
	conf["TOKEN"] = "a2475fa54c6575862b2387c7c8ca6721"

	args := validator.PayEntryValidator{
		UserName: "NAMEABDURRAHAMAN",
		Email:    "john@gmail.com",
		Phone:    "7203964072",
		Amount:   1,
	}
	instance := GetBHTPayService(conf)
	//_, beneId, _ := instance.AddBeneficiary(args)

	payOut, err := instance.CreateOrder(args)
	fmt.Println(payOut)
	fmt.Println(err)

	//payEntry, err := instance.Inquiry("45674280", "")
	//fmt.Println(payEntry)
	//fmt.Println(err)
}
