package cashfree

/*
{
	"id": "order_EPkS6s6eERgWOR",
	"entity": "order",
	"amount": 50000,
	"amount_paid": 0,
	"amount_due": 50000,
	"currency": "INR",
	"receipt": "RZ1583675701242909000570434",
	"offer_id": null,
	"status": "created",
	"attempts": 0,
	"notes": [],
	"created_at": 1583675705
}
*/
type AuthorizeData struct {
	Token  string `json:"token"`
	Expiry int64  `json:"expiry"`
}

type AuthorizeResponse struct {
	Status  string         `json:"status"`
	Message string         `json:"message"`
	SubCode string         `json:"subCode"`
	Data    *AuthorizeData `json:"data"`
}

/**
{
	"status": "SUCCESS",
	"subCode": "200",
	"message": "Details of transfer with transferId 159381033b123",
	"data": {
		"transfer": {
			"referenceId": 17073,
			"bankAccount": "026291800001191",
			"beneId": "ABCD_123",
			"amount": "20.00",
			"status": "SUCCESS",
			"utr": "1387420170430008800069857",
			"addedOn": "2017­-01­-07 20:09:59",
			"processedOn": "2017­-01­-07 20:10:05",
			"acknowledged": 1
		}
	}
}
*/

type InquiryOrderResponse struct {
	OrderStatus     string `json:"orderStatus"`
	TxStatus        string `json:"txStatus"`
	TxTime          string `json:"txTime"`
	TxMsg           string `json:"txMsg"`
	ReferenceID     string `json:"referenceId"`
	PaymentMode     string `json:"paymentMode"`
	OrderCurrency   string `json:"orderCurrency"`
	OrderExpiryTime string `json:"orderExpiryTime"`
	Status          string `json:"status"`
}

type InquiryPayoutResponse struct {
	Message string   `json:"message"`
	Data    IPR_Data `json:"data"`
	Status  string   `json:"status"`
	SubCode string   `json:"subCode"`
}

type IPR_Data struct {
	Transfer IPR_Data_Transfer `json:"transfer"`
}

type IPR_Data_Transfer struct {
	Status        string `json:"status"`
	ReferenceId   int64  `json:"referenceId"`
	FailureReason string `json:"failureReason"`
}
