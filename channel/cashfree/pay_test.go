package cashfree

import (
	"fmt"
	"funzone_pay/app/validator"
	"funzone_pay/model"
	"testing"
	"time"
)

func TestCashFreeService_CreateOrder(t *testing.T) {
	//boostrap.InitConf()
	payEntry, err := GetCashFree().CreateOrder(validator.PayEntryValidator{
		AppId:      "pubg",
		AppOrderId: "test",
		PayChannel: model.CASHFREE,
		Amount:     10,
		UserId:     "1",
		UserName:   "test",
		Email:      "test@cashfree.com",
		Phone:      "9000012345",
	})
	fmt.Println(payEntry)
	fmt.Println(err)
}

func TestCashFreeService_InSignature(t *testing.T) {
	//boostrap.InitConf()
	//orderId=CF1585415673013449&orderAmount=5000.00&referenceId=289347&txStatus=SUCCESS&paymentMode=CREDIT_CARD&txMsg=&txTime=&signature=
	res := GetCashFree().InSignature(&model.CFOrderCallBack{
		OrderId:     "CF1585415673013449",
		OrderAmount: "5000.00",
		ReferenceId: "289347",
		TxStatus:    "SUCCESS",
		PaymentMode: "CREDIT_CARD",
		TxMsg:       "Transaction Successful",
		TxTime:      "2020-03-28 22:46:15",
		Signature:   "ae+5AtrbHFVcz5Lje5t/DH/pDUC219/Dk/wty1nHi9g=",
	})
	fmt.Println(res)
}

func TestSendEntryCallback(t *testing.T) {

}

func TestCashFreeService_Authorize(t *testing.T) {
	//boostrap.InitConf()
	fmt.Println(GetCashFree().Authorize())
}

func TestCashFreeService_PayOut(t *testing.T) {
	//boostrap.InitConf()
	pO, err := GetCashFree().PayOut(validator.PayOutValidator{
		AppId:      "rummy",
		AppOrderId: fmt.Sprintf("testout%v", time.Now().Unix()),
		PayType:    model.PT_BANK,
		PayChannel: model.CASHFREE,
		Amount:     10,
		UserId:     "10050",
		UserName:   "Thaneesh V",
		BankCard:   "1769155000016710",
		Phone:      "7845286022",
		Email:      "7845286022@cashfree.com",
		IFSC:       "KVBL0001769",
		Address:    "ABC Street",
		PayTm:      "9999999999",
	})
	fmt.Println(pO)
	fmt.Println(err)
}

func TestCashFreeService_AddBeneficiary(t *testing.T) {
	GetCashFree().AddBeneficiary(validator.PayOutValidator{
		AppId:      "pubg",
		AppOrderId: "123123123",
		PayChannel: model.CASHFREE,
		Amount:     5000,
		UserId:     "11",
		UserName:   "john doe",
		Phone:      "9876543210",
		Email:      "johndoe@cashfree.com",
		Address:    "ABC Street",
		PayTm:      "",
	}, "")
}
