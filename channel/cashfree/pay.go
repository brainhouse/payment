package cashfree

import (
	"bytes"
	"crypto/hmac"
	"crypto/sha256"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"funzone_pay/app/validator"
	"funzone_pay/centerlog"
	"funzone_pay/dao"
	"funzone_pay/model"
	"funzone_pay/utils"
	"github.com/spf13/viper"
	"github.com/wonderivan/logger"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
	"time"
)

type CashFreeService struct {
	Host             string
	APPID            string
	PAY_ACCOUNT_ID   string
	PAY_CHANNEL      string
	COUNTRY_CODE     int
	APPSK            string
	RETRUN_URL       string
	COLLECT_HOST     string
	NOTIFY_URL       string
	PAY_OUT_URL      string
	PAY_OUT_CLIENTID string
	PAY_OUT_CLIENTSK string
}

func GetCashFree() *CashFreeService {
	return &CashFreeService{
		Host:             viper.GetString("cashfree.HOST"),
		APPID:            viper.GetString("cashfree.APPID"),
		APPSK:            viper.GetString("cashfree.APPSK"),
		RETRUN_URL:       viper.GetString("cashfree.RETURN_URL"),
		PAY_OUT_URL:      viper.GetString("cashfree.PAY_OUT_URL"),
		PAY_OUT_CLIENTID: viper.GetString("cashfree.PAY_OUT_CLIENTID"),
		PAY_OUT_CLIENTSK: viper.GetString("cashfree.PAY_OUT_CLIENTSK"),
	}
}

func GetCashFreeIns(conf map[string]interface{}) *CashFreeService {
	return &CashFreeService{
		PAY_ACCOUNT_ID:   conf["PAY_ACCOUNT_ID"].(string),
		PAY_CHANNEL:      conf["PAY_CHANNEL"].(string),
		COUNTRY_CODE:     conf["COUNTRY_CODE"].(int),
		Host:             conf["HOST"].(string),
		APPID:            conf["APPID"].(string),
		APPSK:            conf["APPSK"].(string),
		RETRUN_URL:       conf["RETURN_URL"].(string),
		NOTIFY_URL:       conf["NOTIFY_URL"].(string),
		COLLECT_HOST:     conf["COLLECT_HOST"].(string),
		PAY_OUT_URL:      conf["PAY_OUT_URL"].(string),
		PAY_OUT_CLIENTID: conf["PAY_OUT_CLIENTID"].(string),
		PAY_OUT_CLIENTSK: conf["PAY_OUT_CLIENTSK"].(string),
	}
}

func CheckReturn(rawBody []byte, param url.Values) (int, string, string) {
	vs, err := url.ParseQuery(string(rawBody))
	if err != nil {
		return -1, "", err.Error()
	}

	ok := vs.Get("txStatus") == "SUCCESS"
	if ok {
		return 1, "", ""
	} else {
		return -1, "", ""
	}
}

/*
*
创建收款支付单
*/
func (cf *CashFreeService) CreateOrder(args validator.PayEntryValidator) (*model.PayEntry, error) {
	orderId := utils.GetOrderId("CFIN")
	requestUrl := fmt.Sprintf("%s%s", cf.Host, "api/v2/cftoken/order")

	rData := struct {
		OrderId       string  `json:"orderId"`
		Amount        float64 `json:"orderAmount"`
		OrderCurrency string  `json:"orderCurrency"`
		//ReturnUrl     string `json:"returnUrl"`
		//NotifyUrl     string `json:"notifyUrl"`
	}{
		OrderId:       orderId,
		Amount:        args.Amount,
		OrderCurrency: "INR",
		//ReturnUrl:     cf.RETRUN_URL,
		//		//NotifyUrl:     cf.NOTIFY_URL,
	}
	header := map[string][]string{
		"x-client-id":     {cf.APPID},
		"x-client-secret": {cf.APPSK},
		"Content-Type":    {"application/json"},
	}
	data, err := cf.Request(requestUrl, "POST", "JSON", rData, header)
	logger.Debug("CashFreeService_CreateOrder_Info | orderReq=%+v | err=%v", rData, err)

	cfResponse := &struct {
		Status  string `json:"status"`
		Message string `json:"message"`
		CfToken string `json:"cftoken"`
	}{}
	err = json.Unmarshal(data, cfResponse)
	if err != nil {
		logger.Error("CashFreeService_CreateOrder_JsonUnmarshal_Err | err=%v", err)
		centerlog.OrderError(centerlog.MsgThirdPlatformFail, cf.PAY_ACCOUNT_ID, args.AppId, args.AppOrderId, orderId, "false", err.Error())
		return nil, err
	}
	//请求成功写入流水
	pE := new(model.PayEntry)
	pE.AppId = args.AppId
	pE.Uid = args.Uid
	pE.PayAccountId = cf.PAY_ACCOUNT_ID
	pE.PayChannel = model.CASHFREE
	pE.CountryCode = cf.COUNTRY_CODE
	pE.AppOrderId = args.AppOrderId
	pE.OrderId = orderId
	pE.PaymentOrderId = cfResponse.CfToken
	pE.Amount = args.Amount
	pE.UserId = args.UserId
	pE.UserName = args.UserName
	pE.Email = args.Email
	pE.Phone = args.Phone
	pE.ThirdCode = cfResponse.Status
	pE.Status = model.PAYING
	pE.PayAppId = cf.APPID
	//生成签名连接
	signStr := fmt.Sprintf("appId%vcustomerEmail%vcustomerName%vcustomerPhone%vnotifyUrl%vorderAmount%vorderCurrency%vorderId%vreturnUrl%v",
		cf.APPID, args.Email, args.UserName, args.Phone, cf.NOTIFY_URL, args.Amount, "INR", pE.OrderId, cf.RETRUN_URL)

	pE.Signature = utils.HmacSha256(signStr, cf.APPSK)

	signStr1 := fmt.Sprintf("appId%vcustomerEmail%vcustomerName%vcustomerPhone%vnotifyUrl%vorderAmount%vorderCurrency%vorderId%vpaymentModes%vreturnUrl%v",
		cf.APPID, args.Email, args.UserName, args.Phone, cf.NOTIFY_URL, args.Amount, "INR", pE.OrderId, "dc,nb,upi,paypal,wallet", cf.RETRUN_URL)
	sign1 := utils.HmacSha256(signStr1, cf.APPSK)
	pE.SignatureNew = sign1

	if cfResponse.Status != "OK" {
		pE.ThirdDesc = cfResponse.Message
		centerlog.OrderError(centerlog.MsgThirdPlatformFail, pE.PayAccountId, pE.AppId, pE.AppOrderId, pE.OrderId, cfResponse.Status, cfResponse.Message)
	}
	engine, err := dao.GetMysql()
	if engine == nil || err != nil {
		logger.Error("CashFreeService_CreateOrder_GetDB_Err | err=%v", err)
		return nil, err
	}
	_, err = engine.Insert(pE)
	if err != nil {
		logger.Error("CashFreeService_CreateOrder_InsertDB_Err | err=%v", err)
		return nil, err
	}
	logger.Debug("CashFreeService_CreateOrder_Request | req=%v | res=%v | newSign=%v", rData, cfResponse, sign1)
	if cfResponse.Status != "OK" {
		err := errors.New(cfResponse.Message)
		return nil, err
	}
	pE.PaymentLinkHost = cf.COLLECT_HOST
	return pE, err
}

/**
 * 充值签名校验
 * param: *model.CFOrderCallBack cb
 * return: bool
 */
func (cf *CashFreeService) InSignature(data interface{}) bool {
	cb := data.(*model.CFOrderCallBack)
	h := hmac.New(sha256.New, []byte(cf.APPSK))
	str := fmt.Sprintf("%s%s%s%s%s%s%s", cb.OrderId, cb.OrderAmount, cb.ReferenceId, cb.TxStatus, cb.PaymentMode, cb.TxMsg, cb.TxTime)
	_, _ = io.WriteString(h, str)
	validateS := base64.StdEncoding.EncodeToString(h.Sum(nil))
	if validateS != cb.Signature {
		return false
	}
	return true
}

func (cf *CashFreeService) Inquiry(orderId string, ext string) (*model.PayEntry, error) {

	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("CashFreeService_Inquiry_Engine_Error | data=%+v | err=%v", orderId, err)
		return nil, err
	}
	requestUrl := fmt.Sprintf("%sapi/v1/order/info/status", cf.Host)
	header := map[string][]string{
		"Content-Type": {"application/x-www-form-urlencoded"},
	}
	rdata := url.Values{}
	rdata.Set("appId", cf.APPID)
	rdata.Set("secretKey", cf.APPSK)
	rdata.Set("orderId", orderId)
	data, err := cf.Request(requestUrl, "POST", "", rdata.Encode(), header)
	if err != nil {
		logger.Error("CashFreeService_Inquiry_Request_Error | err=%v", err)
		return nil, err
	}
	logger.Debug("CashFreeService_Inquiry | data=%v | orderId=%v", string(data), orderId)
	responseData := new(InquiryOrderResponse)
	err = json.Unmarshal(data, responseData)
	if err != nil {
		logger.Error("CashFreeService_Inquiry_JsonError | err=%v | data=%v", err, string(data))
		return nil, err
	}
	payEntry := new(model.PayEntry)
	exist, err := engine.Where("order_id=? and status=0", orderId).ForUpdate().Get(payEntry)
	if err != nil {
		logger.Error("CashFreeService_Inquiry_GetError | err=%v | data=%v", err, orderId)
		return nil, err
	}
	if !exist {
		return nil, errors.New("NotExist")
	}
	if responseData.TxStatus == "SUCCESS" {
		payEntry.Status = model.PAYSUCCESS
	} else if responseData.TxStatus == "FAILED" || responseData.TxStatus == "CANCELLED" || responseData.Status == "ERROR" || responseData.Status == "USER_DROPPED" {
		payEntry.Status = model.PAYFAILD
		payEntry.ThirdDesc = responseData.TxMsg
	} else {
		if responseData.TxMsg != "" {
			payEntry.ThirdDesc = responseData.TxMsg
		}
	}
	/* else if responseData.OrderStatus == "ACTIVE" && (time.Now().Unix()-payEntry.Created) > 3600 {
		payEntry.Status = model.PAYFAILD
		payEntry.ThirdDesc = "orderExpiryTime:" + responseData.OrderExpiryTime
	}*/
	return payEntry, nil
}

func (cf *CashFreeService) InquiryPayout(orderId string, ext string) (*model.PayOut, error) {
	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("CashFreeService_InquiryPayout_Engine_Error | data=%+v | err=%v", orderId, err)
		return nil, err
	}
	payOut := new(model.PayOut)
	exist, err := engine.Where("order_id=?", orderId).Get(payOut)
	if err != nil {
		logger.Error("CashFreeService_InquiryPayout_GetError | err=%v | data=%v", err, orderId)
		return nil, err
	}
	if !exist {
		return nil, errors.New("NotExist")
	}
	//获取token
	Authorization := cf.Authorize()
	if Authorization == "" {
		err = errors.New("AuthorizationError")
		logger.Error("CashFreeService_InquiryPayout_Authorization_Empty | data=%+v", orderId)
		return nil, err
	}
	if strings.Contains(orderId, "CFOUTB") == true {
		return cf.InquiryPayoutBatch(payOut, Authorization, orderId)
	}
	requestUrl := fmt.Sprintf("%s/payout/v1/getTransferStatus?transferId=%v", cf.PAY_OUT_URL, orderId)
	header := map[string][]string{
		"Authorization": {fmt.Sprintf("Bearer %v", Authorization)},
	}
	data, err := cf.Request(requestUrl, "GET", "application/x-www-form-urlencoded", "", header)
	if err != nil {
		logger.Error("CashFreeService_InquiryPayout_Request_Error | err=%v", err)
		return nil, err
	}
	logger.Debug("CashFreeService_InquiryPayout_Info | data=%v | orderId=%v", string(data), orderId)
	responseData := new(InquiryPayoutResponse)
	err = json.Unmarshal(data, responseData)
	if err != nil {
		logger.Error("CashFreeService_InquiryPayout_JsonError | err=%v | data=%v", err, string(data))
		return nil, err
	}
	if responseData.Status == "ERROR" && responseData.SubCode == "404" {
		payOut.Status = model.PAY_OUT_FAILD
	} else if responseData.Data.Transfer.Status == "FAILED" {
		payOut.Status = model.PAY_OUT_FAILD
		payOut.ThirdDesc = responseData.Message
	} else if responseData.Data.Transfer.Status == "SUCCESS" {
		payOut.Status = model.PAY_OUT_SUCCESS
		payOut.PaymentId = fmt.Sprint(responseData.Data.Transfer.ReferenceId)
	}
	return payOut, nil
}

func (cf *CashFreeService) InquiryPayoutBatch(payOut *model.PayOut, auth string, orderId string) (*model.PayOut, error) {
	requestUrl := fmt.Sprintf("%s/payout/v1/getBatchTransferStatus?batchTransferId=%v", cf.PAY_OUT_URL, orderId)
	header := map[string][]string{
		"Authorization": {fmt.Sprintf("Bearer %v", auth)},
	}
	data, err := cf.Request(requestUrl, "GET", "application/x-www-form-urlencoded", "", header)
	if err != nil {
		logger.Error("CashFreeService_InquiryPayoutBatch_Request_Error | err=%v", err)
		return nil, err
	}
	logger.Debug("CashFreeService_InquiryPayoutBatch_Info | data=%v | orderId=%v", string(data), orderId)
	responseData := new(InquiryPayoutResponse)
	err = json.Unmarshal(data, responseData)
	if err != nil {
		logger.Error("CashFreeService_InquiryPayoutBatch_JsonError | err=%v | data=%v", err, string(data))
		return nil, err
	}
	if responseData.Status == "ERROR" && responseData.SubCode == "404" {
		payOut.Status = model.PAY_OUT_FAILD
	} else if responseData.Data.Transfer.Status == "FAILED" || (responseData.Data.Transfer.FailureReason != "" && responseData.Data.Transfer.ReferenceId == 0) {
		payOut.Status = model.PAY_OUT_FAILD
		payOut.ThirdDesc = responseData.Message
	} else if responseData.Data.Transfer.Status == "SUCCESS" {
		payOut.Status = model.PAY_OUT_SUCCESS
		payOut.PaymentId = fmt.Sprint(responseData.Data.Transfer.ReferenceId)
	}
	return payOut, nil
}

/**
 * 提现签名校验
 * param: *model.CFOrderCallBack cb
 * return: bool
 */
func (cf *CashFreeService) OutSignature(data interface{}) bool {
	cb := data.(*model.CFPayOutCallBack)
	h := hmac.New(sha256.New, []byte(cf.PAY_OUT_CLIENTSK))
	var str string
	if cb.Event == "TRANSFER_FAILED" {
		str = fmt.Sprintf("%s%s%s%s", cb.Event, cb.Reason, cb.ReferenceId, cb.TransferId)
	} else if cb.Event == "TRANSFER_REVERSED" {
		str = fmt.Sprintf("%s%s%s%s%s", cb.Event, cb.EventTime, cb.Reason, cb.ReferenceId, cb.TransferId)
	} else if cb.Event == "TRANSFER_SUCCESS" {
		str = fmt.Sprintf("%s%s%s%s%s%s", cb.Acknowledged, cb.Event, cb.EventTime, cb.ReferenceId, cb.TransferId, cb.Utr)
	}
	_, _ = io.WriteString(h, str)
	validateS := base64.StdEncoding.EncodeToString(h.Sum(nil))
	if validateS != cb.Signature {
		return false
	}
	return true
}

/**
 * 发起放款
 * param: validator.PayOutValidator entryValidator
 * return: *model.PayOut
 * return: error
 */
func (cf *CashFreeService) PayOut(frozenId int64, outValidator validator.PayOutValidator) (payOut *model.PayOut, err error) {
	//if outValidator.BankCard == "1769155000016710" {
	//	cf.PAY_OUT_CLIENTID = "CF55181BIWSQOF89KMQQYY"
	//	//cf.PAY_OUT_CLIENTSK = "1db89210046da0ec8613139a2369ead18774e8b5"
	//}
	//创建auth
	Authorization := cf.Authorize()
	if Authorization == "" {
		err = errors.New("AuthorizationError")
		logger.Error("CashFreeService_PayOut_Authorization_Empty | data=%+v", outValidator)
		return nil, err
	}
	//创建受益人
	var beneId string
	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("CashFreeService_PayOut_Engine_Error | data=%+v | err=%v", payOut, err)
		return nil, err
	}
	if outValidator.PayType == model.PT_BANK {
		//检查银行卡账号是否已经注册了
		session := engine.Where("bank_card=? AND pay_channel=? and status=1", outValidator.BankCard, model.CASHFREE)
		if cf.PAY_ACCOUNT_ID != "" {
			session.Where("pay_account_id=?", cf.PAY_ACCOUNT_ID)
		}
		payHis := new(model.PayOut)
		exist, err := session.OrderBy("id desc").Get(payHis)
		if err != nil {
			logger.Error("CashFreeService_PayOut_BankCard_Engine_Error | data=%+v | err=%v", payOut, err)
			return nil, err
		}
		if exist && payHis.BeneId != "" {
			beneId = payHis.BeneId
		}
	} else if outValidator.PayType == model.PT_UPI {
		session := engine.Where("vpa=? AND pay_channel=? and status=1", outValidator.VPA, model.CASHFREE)
		if cf.PAY_ACCOUNT_ID != "" {
			session.Where("pay_account_id=?", cf.PAY_ACCOUNT_ID)
		}
		payHis := new(model.PayOut)
		exist, err := session.OrderBy("id desc").Get(payHis)
		if err != nil {
			logger.Error("CashFreeService_PayOut_VPA_Engine_Error | data=%+v | err=%v", payOut, err)
			return nil, err
		}
		if exist && payHis.BeneId != "" {
			beneId = payHis.BeneId
		}
	}
	transId := utils.GetOrderId("CFOUT")
	payOut = new(model.PayOut)
	payOut.Uid = outValidator.Uid
	payOut.AppId = outValidator.AppId
	payOut.PayAccountId = cf.PAY_ACCOUNT_ID
	payOut.PayChannel = model.CASHFREE
	payOut.CountryCode = cf.COUNTRY_CODE
	payOut.PayType = outValidator.PayType
	payOut.AppOrderId = outValidator.AppOrderId
	payOut.OrderId = transId
	payOut.PaymentId = transId
	payOut.FrozenId = frozenId
	payOut.Amount = outValidator.Amount
	payOut.BankCard = outValidator.BankCard
	payOut.UserId = outValidator.UserId
	payOut.UserName = outValidator.UserName
	payOut.Email = outValidator.Email
	payOut.BeneId = beneId
	payOut.Paytm = outValidator.PayTm
	payOut.IFSC = outValidator.IFSC
	payOut.Phone = outValidator.Phone
	_, err = engine.Insert(payOut)
	if err != nil {
		logger.Error("CashFreeService_PayOut_Insert_Error | data=%+v | err=%v", payOut, err)
		return nil, err
	}
	if beneId == "" {
		var (
			benRes bool
			msg    string
		)
		benRes, beneId, msg = cf.AddBeneficiary(outValidator, Authorization)
		if !benRes {
			err = errors.New("AddBeneficiaryError")
			logger.Error("CashFreeService_PayOut_AddBeneficiaryError | data=%+v", outValidator)
			_, _ = engine.Where("order_id=?", transId).Update(&model.PayOut{
				ThirdDesc: msg,
				Status:    model.PAY_OUT_FAILD,
			})
			payOut.Status = model.PAY_OUT_FAILD
			return payOut, err
		}
	}
	//发起支付
	requestUrl := fmt.Sprintf("%s%s", cf.PAY_OUT_URL, "/payout/v1/requestTransfer")
	header := map[string][]string{
		"Authorization": {fmt.Sprintf("Bearer %v", Authorization)},
	}
	/**
	{
	  "beneId": "JOHN18011",
	  "amount": "100.00",
	  "transferId": "DEC2016"
	}
	*/
	transferMode := string(outValidator.PayType)
	if transferMode == "bank" {
		transferMode = "banktransfer" //做下转换 banktransfer, upi, paytm, amazonpay and card.
	} else if transferMode == "upi" {
		transferMode = "upi"
	}
	reqData := &struct {
		BeneId       string `json:"beneId"`
		Amount       string `json:"amount"`
		TransferId   string `json:"transferId"`
		TransferMode string `json:"transferMode"`
	}{
		BeneId:       beneId,
		Amount:       fmt.Sprint(outValidator.Amount),
		TransferId:   transId,
		TransferMode: transferMode,
	}

	data, err := cf.Request(requestUrl, "POST", "JSON", reqData, header)
	if err != nil {
		logger.Error("CashFreeService_PayOut_Request_Error | err=%v", err)
		return nil, err
	}
	rdata, _ := json.Marshal(reqData)
	logger.Debug("CashFreeService_PayOut_Info | requestUrl=%v | requestData=%v | response=%v", requestUrl, string(rdata), string(data))
	/**
	{
	  "status": "SUCCESS",
	  "subCode": "200",
	  "message": "Transfer completed successfully",
	  "data": {
	    "referenceId": "10023",
	    "utr": "P16111765023806",
	    "acknowledged": 1
	  }
	}
	*/
	res := &struct {
		Status  string `json:"status"`
		SubCode string `json:"subCode"`
		Message string `json:"message"`
		Data    struct {
			ReferenceId  string `json:"referenceId"`
			Utr          string `json:"utr"`
			Acknowledged int    `json:"acknowledged"`
		} `json:"data"`
	}{}
	err = json.Unmarshal(data, res)
	if err != nil {
		logger.Error("CashFreeService_PayOut_JsonUnmarshal_Error | err=%v | res=%v", err, res)
		centerlog.OrderError(centerlog.MsgThirdPlatformFail, payOut.PayAccountId, payOut.AppOrderId, payOut.AppId, payOut.OrderId, "false", err.Error())
		return nil, err
	}
	var (
		status      model.PayOutStatus
		thirdDesc   string
		referenceId string
		utr         string
	)
	payOut.ThirdCode = res.SubCode
	if res.Status == "ERROR" {
		//先不设置成失败
		//status = model.PAY_OUT_FAILD
		thirdDesc = res.Message
		centerlog.OrderError(centerlog.MsgThirdPlatformFail, payOut.PayAccountId, payOut.AppOrderId, payOut.AppId, payOut.OrderId, res.Status, res.Message)
	} else {
		status = model.PAY_OUT_ING
		referenceId = res.Data.ReferenceId
		utr = res.Data.Utr
	}
	_, err = engine.Where("id=?", payOut.Id).Update(&model.PayOut{
		Status:     status,
		BeneId:     beneId,
		ThirdCode:  res.SubCode,
		PaymentId:  referenceId,
		Utr:        utr,
		ThirdDesc:  thirdDesc,
		FinishTime: time.Now().Unix(),
	})
	if err != nil {
		logger.Error("CashFreeService_PayOut_Insert_Error | data=%v | err=%v", payOut, err)
		return nil, err
	}
	return payOut, nil
}

/*
*
 */
func (cf *CashFreeService) PayOutBatch(frozenId int64, outValidator validator.PayOutValidator) (payOut *model.PayOut, err error) {
	//创建auth
	Authorization := cf.Authorize()
	if Authorization == "" {
		err = errors.New("AuthorizationError")
		logger.Error("CashFreeService_PayOutBatch_Authorization_Empty | data=%+v", outValidator)
		return nil, err
	}
	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("CashFreeService_PayOutBatch_Engine_Error | data=%+v | err=%v", payOut, err)
		return nil, err
	}

	transId := utils.GetOrderId("CFOUTB")
	payOut = new(model.PayOut)
	payOut.Uid = outValidator.Uid
	payOut.AppId = outValidator.AppId
	payOut.PayAccountId = cf.PAY_ACCOUNT_ID
	payOut.PayChannel = model.CASHFREE
	payOut.PayType = outValidator.PayType
	payOut.AppOrderId = outValidator.AppOrderId
	payOut.OrderId = transId
	payOut.PaymentId = transId
	payOut.FrozenId = frozenId
	payOut.Amount = outValidator.Amount
	payOut.BankCard = outValidator.BankCard
	payOut.UserId = outValidator.UserId
	payOut.UserName = outValidator.UserName
	payOut.Email = outValidator.Email
	payOut.Paytm = outValidator.PayTm
	payOut.IFSC = outValidator.IFSC
	payOut.Phone = outValidator.Phone
	_, err = engine.Insert(payOut)
	if err != nil {
		logger.Error("CashFreeService_PayOutBatch_Insert_Error | data=%+v | err=%v", payOut, err)
		return nil, err
	}

	//发起支付
	requestUrl := fmt.Sprintf("%s%s", cf.PAY_OUT_URL, "/payout/v1/requestBatchTransfer")
	header := map[string][]string{
		"Authorization": {fmt.Sprintf("Bearer %v", Authorization)},
	}
	transferMode := string(outValidator.PayType)
	if transferMode == "bank" {
		transferMode = "BANK_ACCOUNT" //做下转换 banktransfer, upi, paytm, amazonpay and card.
	}
	/**
	{
	    "batchTransferId": "abc-.12356",
	    "batchFormat": "BANK_ACCOUNT",
	    "batch": [
	        {
	            "amount": "1",
	            "transferId": "1234",
	            "remarks": "Transfer with Id 12356",
	            "name": "john doe",
	            "email": "johndoe@cashfree.com",
	            "phone": 9876543210,
	            "bankAccount": "00111122233",
	            "ifsc": "HDFC0000001"
	        }
	    ]
	}
	*/
	type BatchBody struct {
		Amount      string `json:"amount"`
		TransferId  string `json:"transferId"`
		Remarks     string `json:"remarks"`
		Name        string `json:"name"`
		Email       string `json:"email"`
		Phone       string `json:"phone"`
		BankAccount string `json:"bankAccount"`
		Ifsc        string `json:"ifsc"`
	}
	reqData := &struct {
		BatchTransferId string      `json:"batchTransferId"`
		BatchFormat     string      `json:"batchFormat"`
		Batch           []BatchBody `json:"batch"`
	}{
		BatchTransferId: payOut.OrderId,
		BatchFormat:     transferMode,
		Batch: []BatchBody{
			{
				Amount:      fmt.Sprint(payOut.Amount),
				TransferId:  payOut.OrderId,
				Remarks:     "pay to player",
				Name:        payOut.UserName,
				Email:       payOut.Email,
				Phone:       payOut.Phone,
				BankAccount: payOut.BankCard,
				Ifsc:        payOut.IFSC,
			},
		},
	}
	data, err := cf.Request(requestUrl, "POST", "JSON", reqData, header)
	if err != nil {
		logger.Error("CashFreeService_PayOutBatch_Request_Error | err=%v", err)
		return nil, err
	}
	rdata, _ := json.Marshal(reqData)
	logger.Debug("CashFreeService_PayOutBatch_Info | requestUrl=%v | requestData=%v | response=%v", requestUrl, string(rdata), string(data))
	/**
	{
	  "status": SUCCESS,
	  "subCode": "200",
	  "message": "Request accepted",
	  "data":
	  {
	    "referenceId": 1594
	  }
	}
	*/
	res := &struct {
		Status  string `json:"status"`
		SubCode string `json:"subCode"`
		Message string `json:"message"`
		Data    struct {
			ReferenceId json.Number `json:"referenceId"`
		} `json:"data"`
	}{}
	err = json.Unmarshal(data, res)
	if err != nil {
		logger.Error("CashFreeService_PayOutBatch_JsonUnmarshal_Error | err=%v | res=%v", err, res)
		return nil, err
	}
	var status model.PayOutStatus
	var thirdDesc string
	var referenceId string
	payOut.ThirdCode = res.SubCode
	if res.Status == "ERROR" {
		//先不设置成失败
		//status = model.PAY_OUT_FAILD
		thirdDesc = res.Message
	} else {
		status = model.PAY_OUT_ING
		referenceId = res.Data.ReferenceId.String()
	}
	_, err = engine.Where("id=?", payOut.Id).Update(&model.PayOut{
		Status:     status,
		ThirdCode:  res.SubCode,
		PaymentId:  referenceId,
		ThirdDesc:  thirdDesc,
		FinishTime: time.Now().Unix(),
	})
	if err != nil {
		logger.Error("CashFreeService_PayOutBatch_Insert_Error | data=%v | err=%v", payOut, err)
		return nil, err
	}
	return payOut, nil
}

/**
 * 获得token
 * return: string
 */
func (cf *CashFreeService) Authorize() string {
	requestUrl := fmt.Sprintf("%s%s", cf.PAY_OUT_URL, "/payout/v1/authorize")
	header := map[string][]string{
		"X-Client-Id":     {cf.PAY_OUT_CLIENTID},
		"X-Client-Secret": {cf.PAY_OUT_CLIENTSK},
	}
	data, err := cf.Request(requestUrl, "POST", "", "", header)
	if err != nil {
		logger.Error("CashFreeService_Authorize_Request_Error | err=%v", err)
		return ""
	}
	logger.Debug("CashFreeService_Authorize_Info | err=%v | res=%v | req=%v", err, string(data), header)
	res := new(AuthorizeResponse)
	err = json.Unmarshal(data, res)
	if err != nil {
		logger.Error("CashFreeService_Authorize_JsonUnmarshal_Error | err=%v | res=%v", err, res)
		return ""
	}
	if res.Data == nil {
		return ""
	}
	return res.Data.Token
}

/**
 * 添加放款人
 */
func (cf *CashFreeService) AddBeneficiary(outValidator validator.PayOutValidator, auth string) (bool, string, string) {
	requestUrl := fmt.Sprintf("%s%s", cf.PAY_OUT_URL, "/payout/v1/addBeneficiary")
	header := map[string][]string{
		"Authorization": {fmt.Sprintf("Bearer %v", auth)},
	}

	/**
	{
		"beneId": "JOHN18011",
		"name": "john doe",
		"email": "johndoe@cashfree.com",
		"phone": "9876543210",
		"bankAccount": "00001111222233",
		"ifsc": "HDFC0000001",
		"address1": "ABC Street",
		"city": "Bangalore",
		"state": "Karnataka",
		"pincode": "560001"
	}
	*/
	beneId := utils.GetOrderId("CFBID")
	rData := struct {
		BeneId      string `json:"beneId"`
		Name        string `json:"name"`
		Phone       string `json:"phone"`
		Email       string `json:"email"`
		BankAccount string `json:"bankAccount"`
		IFSC        string `json:"ifsc"`
		VPA         string `json:"vpa"`
		Address1    string `json:"address1"`
	}{
		BeneId:   beneId,
		Name:     outValidator.UserName,
		Phone:    outValidator.Phone,
		Email:    outValidator.Email,
		Address1: outValidator.Address,
	}
	if outValidator.PayType == model.PT_BANK {
		rData.BankAccount = outValidator.BankCard
		rData.IFSC = outValidator.IFSC
	} else if outValidator.PayType == model.PT_PayTm {
		rData.Phone = outValidator.PayTm
	} else if outValidator.PayType == model.PT_UPI {
		rData.VPA = outValidator.VPA
	}
	data, err := cf.Request(requestUrl, "POST", "JSON", rData, header)
	if err != nil {
		logger.Error("CashFreeService_Authorize_Request_Error | err=%v | req=%v", err, outValidator)
		return false, "", ""
	}
	logger.Debug("CashFreeService_AddBeneficiary_Info | data=%v | req=%v", string(data), rData)
	/**
	{"status":"SUCCESS","subCode":"200","message":"Beneficiary added successfully"}
	*/
	res := &struct {
		Status  string `json:"status"`
		Message string `json:"message"`
		SubCode string `json:"subCode"`
	}{}
	err = json.Unmarshal(data, res)
	if err != nil {
		logger.Error("CashFreeService_AddBeneficiary_JsonUnmarshal_Error | err=%v | res=%+v", err, string(data))
		return false, "", ""
	}
	//存在重新获取
	if res.SubCode == "409" && outValidator.PayType == model.PT_BANK {
		geUrl := fmt.Sprintf("%s%s?bankAccount=%v&ifsc=%v", cf.PAY_OUT_URL, "/payout/v1/getBeneId", outValidator.BankCard, outValidator.IFSC)
		beData, err := cf.Request(geUrl, "GET", "", "", header)
		if err != nil {
			logger.Error("CashFreeService_AddBeneficiary_GetBeneficiary_Error | err=%v | res=%v | req=%v", err, string(beData), outValidator)
			return false, "", ""
		}
		resData := &struct {
			Status string `json:"status"`
			Data   struct {
				BeneId string `json:"beneId"`
			} `json:"data"`
		}{}
		logger.Debug("CashFreeService_AddBeneficiary_GetBeneficiary_Info | data=%v | req=%v", string(beData), outValidator)
		err = json.Unmarshal(beData, resData)
		if err != nil {
			logger.Error("CashFreeService_AddBeneficiary_GetBeneficiary_JsonError | err=%v | res=%v | req=%v", err, string(beData), outValidator)
			return false, "", ""
		}
		if resData.Data.BeneId != "" {
			return true, resData.Data.BeneId, ""
		}
		return false, "", ""
	}
	if res.Status != "SUCCESS" {
		return false, "", res.Message
	}
	return true, beneId, ""
}

/*
*
统一请求
*/
func (cf *CashFreeService) Request(url string, method string, contentType string, sendData interface{}, header map[string][]string) (res []byte, err error) {
	client := &http.Client{}
	var req *http.Request
	if contentType == "JSON" {
		data, _ := json.Marshal(sendData)
		reader := bytes.NewBuffer(data)
		req, err = http.NewRequest(method, url, reader)
	} else {
		s := sendData.(string)
		reader := strings.NewReader(s)
		req, err = http.NewRequest(method, url, reader)
	}
	//提交请求
	if err != nil {
		logger.Error("CashFreeService_Create_order | url=%v | err=%+v | req=%+v", url, err, sendData)
		return
	}
	req.Header = http.Header(header)
	//处理返回结果
	response, err := client.Do(req)
	if err != nil {
		logger.Error("CashFreeService_request_err | url=%v | err=%+v | req=%+v", url, err, sendData)
		return
	}
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		logger.Error("CashFreeService_response_read_err | url=%v | err=%+v | req=%+v", url, err, sendData)
		return
	}
	defer func() {
		_ = response.Body.Close()
	}()
	return body, nil
}
