package ybpay

import (
	"crypto"
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"encoding/base64"
	"encoding/hex"
	"errors"
)

//测试公钥
var publicKey = []byte(`MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCyrhl29B41bba9pmfjjHdEbPLAa0+oo1mBZLsTs/IjgLoSiACqWHSH55oEPUjKEGaX+hyAokAOtHbBmuazFbhje9mGkPWESNClSKfJOJOwrtXgA9RfWc+ZdFiTz3rJTLTmoVU+n/f6/iRJnKOKsCRq1LBo1FepvtkUKDTdgH6qrQIDAQAB`)

var privateKey = []byte(`MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBAOzrQMnLcd6DhIEsShhH34Y5ewtHzKsp6JL025Q5tZDJHDU/uDa4pvci/v4ub+jexf7oHTjXpwvyPxPrNCGdnsS0p7K0sw0VnGL9GESbq1R4j+LQbY9aA6f6myBjQEEilxbcK1+tMlHZilPPRaX3NHes3DldSODjmswKfNFBWJm9AgMBAAECgYBAkXjtxjF5aDSDk9N5vJTRmvbKwz2nZFuLOYpiYjv2gqjaZkxJNZfjgLkoAvXi7FvftdjI9hUDr3i7b/2cL73l7eVgxqoGQhPH063TUgiZvGBFKhBKwHtIHVN8rHHVrH7hhaMW7XhuhBm8xtT9TtdGfNXyr33HrLYI7kTThfIYLQJBAPuiYUyr/25EmBoCi84MgpzCrJ5mvITGW31epF52Yp+1LdWVYUYF5MPjZbXaFs+kAWorv3a+ITjGsjRO3449kfMCQQDxB4QAO0Trfs7dcVLP8YYftfhmx4+OvEPz0W6W+hVYHpTIqLRWPcE5tcQyMNXVIJ4auRUw91FtB3ml0uA3fWGPAkBone8UAGJXBt+x2KMlTGA1Kl64EICKNymsat+tgkyol5vxs4RYp3Sf2Hfd7kkTlN0dtgiJJq0LPNi0FKKvMXvvAkEAu6HIJo39Jv5+d3CisPIqIUF9cDVmEg2JMKMW3j0spH6aAGMkkAZfB1fytcfCjrlnmxH95zKAlsm871Pr7AaVDQJASDVlp4ZDQawYWc8OvjYUsw5J2U70JIXqLHagvBKVxfDysKS5CY0LVJQybdM/OoImCRfB2ZXFCyQ4M2GHP+/2Gw==`)

func RsaEncrypt(publicKey string, origData []byte) ([]byte, error) {
	block, _ := hex.DecodeString(publicKey)
	if block == nil {
		return nil, errors.New("public key error")
	}
	pubInterface, err := x509.ParsePKIXPublicKey(block)
	if err != nil {
		return nil, err
	}
	pub := pubInterface.(*rsa.PublicKey)
	return rsa.EncryptPKCS1v15(rand.Reader, pub, origData)
}

func RsaEncrypt1(publicKey string, origData []byte) (string, error) {
	block, err := base64.StdEncoding.DecodeString(publicKey)
	if err != nil {
		return "", errors.New("public key error")
	}
	pubInterface, err := x509.ParsePKIXPublicKey(block)
	if err != nil {
		return "", err
	}
	pub := pubInterface.(*rsa.PublicKey)

	msgLen := len(origData)

	step := 117
	//要分段加密
	var encryptedBytes []byte
	for start := 0; start < msgLen; start += step {
		finish := start + step
		if finish > msgLen {
			finish = msgLen
		}
		encryptedBlockBytes, err := rsa.EncryptPKCS1v15(rand.Reader, pub, origData[start:finish])
		if err != nil {
			return "", err

		}
		encryptedBytes = append(encryptedBytes, encryptedBlockBytes...)
	}
	return base64.StdEncoding.EncodeToString(encryptedBytes), nil
}

func RsaSignEncrypt(privateKey string, origData []byte, hash crypto.Hash) (string, error) {
	block, err := base64.StdEncoding.DecodeString(privateKey)
	if err != nil {
		return "", errors.New("public key error")
	}
	priInterface, err := x509.ParsePKCS8PrivateKey(block)
	if err != nil {
		return "", err
	}
	pri := priInterface.(*rsa.PrivateKey)
	shaNew := hash.New()
	shaNew.Write(origData)
	hashed := shaNew.Sum(nil)
	signature, err := rsa.SignPKCS1v15(rand.Reader, pri, hash, hashed)
	if err != nil {
		return "", err
	}
	return base64.StdEncoding.EncodeToString(signature), nil
}

func RsaDecrypt(privateKey string, ciphertext []byte) ([]byte, error) {
	block, err := base64.StdEncoding.DecodeString(privateKey)
	if err != nil {
		return nil, errors.New("public key error")
	}
	prvKey, err := x509.ParsePKCS8PrivateKey(block)
	if err != nil {
		return nil, err
	}
	priv := prvKey.(*rsa.PrivateKey)
	return rsa.DecryptPKCS1v15(rand.Reader, priv, ciphertext)
}
