package ybpay

import (
	"bytes"
	"crypto"
	"encoding/json"
	"errors"
	"fmt"
	"funzone_pay/app/validator"
	"funzone_pay/centerlog"
	"funzone_pay/dao"
	"funzone_pay/model"
	"funzone_pay/utils"
	"github.com/wonderivan/logger"
	"io/ioutil"
	"net/http"
	"strings"
	"time"
)

type YBPayService struct {
	PAY_ACCOUNT_ID string
	PAY_CHANNEL    string
	MID            string
	SK             string
	HOST           string
	NOTIFY_URL     string
	RETURN_URL     string
	PAYOUT_URL     string
	COUNTRY_CODE   int
	RSA_PRI_KEY    string
	RSA_PUB_KEY    string
}

func GetYBIns(conf map[string]interface{}) *YBPayService {
	return &YBPayService{
		PAY_ACCOUNT_ID: conf["PAY_ACCOUNT_ID"].(string),
		PAY_CHANNEL:    conf["PAY_CHANNEL"].(string),
		MID:            conf["MID"].(string),
		SK:             conf["SK"].(string),
		HOST:           conf["HOST"].(string),
		NOTIFY_URL:     conf["NOTIFY_URL"].(string),
		RETURN_URL:     conf["RETURN_URL"].(string),
		PAYOUT_URL:     conf["PAYOUT_URL"].(string),
		COUNTRY_CODE:   conf["COUNTRY_CODE"].(int),
		RSA_PRI_KEY:    conf["RSA_PRI_KEY"].(string),
		RSA_PUB_KEY:    conf["RSA_PUB_KEY"].(string),
	}
}

func (Y YBPayService) CreateOrder(args validator.PayEntryValidator) (*model.PayEntry, error) {
	orderId := utils.GetOrderId("YBIN")
	requestUrl := fmt.Sprintf("%s%s", Y.HOST, "/poi/pay/index/PayOrderCreate")
	sign := utils.Md5Str(fmt.Sprintf("%v%v%v", Y.MID, orderId, Y.SK))

	var rData = &CollectRequest{
		MerNo:       Y.MID,
		MerOrderNo:  orderId,
		Pname:       args.UserName,
		Pemail:      args.Email,
		Phone:       args.Phone,
		OrderAmount: int64(args.Amount),
		CountryCode: "IND",
		CyyNo:       "INR",
		PayType:     "UPI",
		NotifyUrl:   Y.NOTIFY_URL,
		CallbackUrl: Y.RETURN_URL,
		Sign:        sign,
	}
	header := map[string][]string{
		"Content-type": {"application/json"},
	}
	data, err := Y.Request(requestUrl, "POST", "JSON", rData, header)
	logger.Debug("YBPayService_CreateOrder_Info | orderReq=%v | err=%v | data=%v", rData, err, string(data))

	fmt.Println(string(data))
	egResponse := &CollectResponse{}
	err = json.Unmarshal(data, egResponse)
	if err != nil {
		logger.Error("YBPayService_CreateOrder_JsonUnmarshal_Err | err=%v", err)
		centerlog.OrderError(centerlog.MsgThirdPlatformFail, Y.PAY_ACCOUNT_ID, args.AppId, args.AppOrderId, orderId, "false", err.Error())
		return nil, err
	}
	//请求成功写入流水
	pE := new(model.PayEntry)
	pE.AppId = args.AppId
	pE.Uid = args.Uid
	pE.PayAccountId = Y.PAY_ACCOUNT_ID
	pE.PayChannel = model.YBPAY
	pE.CountryCode = Y.COUNTRY_CODE
	pE.AppOrderId = args.AppOrderId
	pE.PaymentOrderId = egResponse.OrderNumber
	pE.OrderId = orderId
	pE.Amount = args.Amount
	pE.UserId = args.UserId
	pE.UserName = args.UserName
	pE.Email = args.Email
	pE.Phone = args.Phone
	pE.ThirdCode = egResponse.Msg
	pE.Status = model.PAYING
	pE.Created = time.Now().Unix()

	fail := false
	if egResponse.Code == 0 {
		fail = true
		pE.Status = model.PAYFAILD
		pE.ThirdCode = fmt.Sprint(egResponse.Code)
		pE.ThirdDesc = fmt.Sprintf("%v-%v", egResponse.Code, egResponse.Msg)
		centerlog.OrderError(centerlog.MsgThirdPlatformFail, Y.PAY_ACCOUNT_ID, args.AppId, args.AppOrderId, orderId, fmt.Sprint(egResponse.Code), egResponse.Msg)
	}
	engine, err := dao.GetMysql()
	if engine == nil || err != nil {
		logger.Error("YBPayService_CreateOrder_GetDB_Err | err=%v", err)
		return nil, err
	}
	_, err = engine.Insert(pE)
	if err != nil {
		logger.Error("YBPayService_CreateOrder_InsertDB_Err | err=%v", err)
		return nil, err
	}
	logger.Debug("YBPayService_CreateOrder_Request | req=%v | res=%v", rData, egResponse)
	if fail {
		return nil, errors.New(pE.ThirdDesc)
	}
	pE.PaymentLinkHost = egResponse.PayURL
	return pE, err
}

func (Y YBPayService) Inquiry(orderId string, s2 string) (*model.PayEntry, error) {
	sign := utils.Md5Str(fmt.Sprintf("%v%v%v", Y.MID, orderId, Y.SK))
	reqData := &struct {
		MerNo      string `json:"mer_no"`
		MerOrderNo string `json:"mer_order_no"`
		Sign       string `json:"sign"`
	}{
		MerNo:      Y.MID,
		MerOrderNo: orderId,
		Sign:       sign,
	}
	requestUrl := fmt.Sprintf("%s%s", Y.HOST, "/poi/pay/Orderquery")
	header := map[string][]string{
		"Content-Type": {"application/json"},
	}
	data, err := Y.Request(requestUrl, "POST", "JSON", reqData, header)
	if err != nil {
		logger.Error("YBPayService_Collect_Inquiry_Error | err=%v", err)
		return nil, err
	}
	logger.Debug("YBPayService_Collect_Inquiry_Data | orderId=%v | res=%v", orderId, string(data))
	statusData := &struct {
		OrderStatus string `json:"order_status"`
		OrderNumber string `json:"order_number"`
		Code        int64  `json:"code"`
		Msg         string `json:"msg"`
	}{}
	err = json.Unmarshal(data, statusData)
	if err != nil {
		logger.Error("YBPayService_CollectInquiry_JsonError | err=%v | data=%v", err, string(data))
		return nil, err
	}

	engine, _ := dao.GetMysql()
	payEntry := new(model.PayEntry)
	exist, err := engine.Where("order_id=?", orderId).Get(payEntry)
	if err != nil {
		logger.Error("YBPayService_Collect_Inquiry_GetError | err=%v | data=%v", err, orderId)
		return nil, err
	}
	if !exist {
		return nil, errors.New("NotExist")
	}
	if statusData.OrderStatus == "3" {
		payEntry.Status = model.PAYFAILD
		payEntry.ThirdDesc = statusData.Msg
	} else if statusData.OrderStatus == "4" {
		payEntry.Status = model.PAYSUCCESS
	}
	return payEntry, nil
}

func (Y YBPayService) InquiryPayout(orderId string, ext string) (*model.PayOut, error) {
	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("YBPayService_InquiryPayout_Engine_Error | data=%+v | err=%v", orderId, err)
		return nil, err
	}
	payOut := new(model.PayOut)
	exist, err := engine.Where("order_id=?", orderId).Get(payOut)
	if err != nil {
		logger.Error("YBPayService_InquiryPayout_GetError | err=%v | data=%v", err, orderId)
		return nil, err
	}
	if !exist {
		return nil, errors.New("NotExist")
	}
	requestUrl := fmt.Sprintf("%s/poi/dai/Orderquery", Y.HOST)

	header := map[string][]string{
		"Content-Type": {"application/json"},
	}

	sign := utils.Md5Str(fmt.Sprintf("%v%v%v", Y.MID, orderId, Y.SK))
	reqData := struct {
		MerNo      string `json:"mer_no"`
		MerOrderNo string `json:"mer_order_no"`
		Sign       string `json:"sign"`
	}{
		MerNo:      Y.MID,
		MerOrderNo: orderId,
		Sign:       sign,
	}

	data, err := Y.Request(requestUrl, "POST", "JSON", reqData, header)
	if err != nil {
		logger.Error("YBPayService_InquiryPayout_Request_Error | err=%v", err)
		return nil, err
	}
	logger.Debug("YBPayService_InquiryPayout_Info | data=%v | orderId=%v | req=%v", string(data), orderId, reqData)
	responseData := new(PayoutQueryResponse)
	err = json.Unmarshal(data, responseData)
	if err != nil {
		logger.Error("YBPayService_InquiryPayout_JsonError | err=%v | data=%v", err, string(data))
		return nil, err
	}
	if responseData.OrderStatus == "2" || responseData.OrderStatus == "3" {
		payOut.Status = model.PAY_OUT_FAILD
		payOut.ThirdDesc = responseData.Msg
	} else if responseData.OrderStatus == "4" {
		payOut.Status = model.PAY_OUT_SUCCESS
	}
	return payOut, nil
}

func (Y YBPayService) PayOut(frozenId int64, outValidator validator.PayOutValidator) (payOut *model.PayOut, err error) {
	transId := utils.GetOrderId("YBOUT")
	payOut = new(model.PayOut)
	payOut.Uid = outValidator.Uid
	payOut.AppId = outValidator.AppId
	payOut.PayAccountId = Y.PAY_ACCOUNT_ID
	payOut.PayChannel = model.YBPAY
	payOut.CountryCode = Y.COUNTRY_CODE
	payOut.PayType = outValidator.PayType
	payOut.AppOrderId = outValidator.AppOrderId
	payOut.OrderId = transId
	payOut.PaymentId = transId
	payOut.FrozenId = frozenId
	payOut.Amount = outValidator.Amount
	payOut.BankCard = outValidator.BankCard
	payOut.UserId = outValidator.UserId
	payOut.UserName = outValidator.UserName
	payOut.Email = outValidator.Email
	payOut.Paytm = outValidator.PayTm
	payOut.IFSC = outValidator.IFSC
	payOut.Phone = outValidator.Phone
	engine, err := dao.GetMysql()
	if engine == nil || err != nil {
		logger.Error("YBPayService_PayOut_GetDB_Err | err=%v", err)
		return nil, err
	}
	_, err = engine.Insert(payOut)
	if err != nil {
		logger.Error("YBPayService_PayOut_Insert_Error | data=%+v | err=%v", payOut, err)
		return nil, err
	}
	//发起支付
	requestUrl := fmt.Sprintf("%s%s", Y.HOST, "/poi/dai/index/DaiOrderCreate")
	header := map[string][]string{
		"Content-Type": {"application/json"},
	}

	reqData := PayOutRequest{
		MerNo:       Y.MID,
		MerOrderNo:  transId,
		Email:       outValidator.Email,
		Phone:       outValidator.Phone,
		OrderAmount: int64(outValidator.Amount),
		CyyNo:       "INR",
		AccName:     outValidator.UserName,
		NotifyUrl:   Y.PAYOUT_URL,
	}

	if outValidator.PayType == model.PT_BANK {
		reqData.PayType = "BANK"
		reqData.Province = outValidator.IFSC
		reqData.AccNo = outValidator.BankCard
	} else if outValidator.PayType == "upi" {
		reqData.PayType = "UPI"
		reqData.VPA = outValidator.VPA
	} else {
		return nil, err
	}
	jsonD, _ := json.Marshal(reqData)
	signByte, err := RsaSignEncrypt(Y.RSA_PRI_KEY, jsonD, crypto.SHA1)
	if err != nil {
		logger.Error("YBPayService_PayOut_RSA_Error | err=%v | json=%v", err, string(jsonD))
		return nil, err
	}
	var requestData = SignPayOutRequest{
		PayOutRequest: reqData,
		Sign:          signByte,
	}
	data, err := Y.Request(requestUrl, "POST", "JSON", requestData, header)
	if err != nil {
		logger.Error("YBPayService_PayOut_Request_Error | err=%v", err)
		return nil, err
	}

	logger.Debug("YBPayService_PayOut_Info | requestUrl=%v | requestData=%v | response=%v", requestUrl, requestData, string(data))

	res := &PayoutResponse{}
	err = json.Unmarshal(data, res)
	if err != nil {
		logger.Error("YBPayService_PayOut_JsonUnmarshal_Error | err=%v | res=%v", err, res)
		centerlog.OrderError(centerlog.MsgThirdPlatformFail, Y.PAY_ACCOUNT_ID, payOut.AppId, payOut.AppOrderId, payOut.OrderId, "false", err.Error())
		return nil, err
	}
	var (
		status = model.PAY_OUT_ING
	)
	_, err = engine.Where("id=?", payOut.Id).Update(&model.PayOut{
		Status:     status,
		ThirdCode:  payOut.ThirdCode,
		PaymentId:  res.OrderNumber,
		ThirdDesc:  res.Msg,
		FinishTime: time.Now().Unix(),
	})
	if err != nil {
		logger.Error("YBPayService_PayOut_Insert_Error | data=%v | err=%v", payOut, err)
		return nil, err
	}
	return payOut, nil
}

func (Y YBPayService) PayOutBatch(frozenId int64, args validator.PayOutValidator) (*model.PayOut, error) {
	return Y.PayOut(frozenId, args)
}

func (Y YBPayService) InSignature(cb interface{}) bool {
	panic("implement me")
}

func (Y YBPayService) OutSignature(cb interface{}) bool {
	panic("implement me")
}

func (Y YBPayService) Request(url string, method string, contentType string, sendData interface{}, header map[string][]string) (res []byte, err error) {
	client := &http.Client{}
	var req *http.Request
	if contentType == "JSON" {
		data, _ := json.Marshal(sendData)
		fmt.Println(string(data))
		reader := bytes.NewBuffer(data)
		req, err = http.NewRequest(method, url, reader)
	} else {
		s := sendData.(string)
		reader := strings.NewReader(s)
		req, err = http.NewRequest(method, url, reader)
	}
	fmt.Println(url)
	//提交请求
	if err != nil {
		logger.Error("YBPayService_Create_order | url=%v | err=%+v | req=%+v", url, err, sendData)
		return
	}
	req.Header = http.Header(header)
	//处理返回结果
	response, err := client.Do(req)
	if err != nil {
		logger.Error("YBPayService_request_err | url=%v | err=%+v | req=%+v", url, err, sendData)
		return
	}
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		logger.Error("YBPayService_response_read_err | url=%v | err=%+v | req=%+v", url, err, sendData)
		return
	}
	defer func() {
		_ = response.Body.Close()
	}()
	return body, nil
}
