package ybpay

import (
	"crypto"
	"encoding/json"
	"fmt"
	"testing"
)

func TestYBPayService_PayOut(t *testing.T) {
	reqData := PayOutRequest{
		MerNo:       "",
		MerOrderNo:  "",
		Email:       "",
		Phone:       "",
		OrderAmount: int64(100),
		CyyNo:       "INR",
		AccName:     "",
		NotifyUrl:   "",
	}

	reqData.PayType = "BANK"
	reqData.Province = "ifsc"
	reqData.AccNo = ""

	jsonD, _ := json.Marshal(reqData)
	fmt.Println(string(jsonD))

	signByte, err := RsaEncrypt1(`MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBAOzrQMnLcd6DhIEsShhH34Y5ewtHzKsp6JL025Q5tZDJHDU/uDa4pvci/v4ub+jexf7oHTjXpwvyPxPrNCGdnsS0p7K0sw0VnGL9GESbq1R4j+LQbY9aA6f6myBjQEEilxbcK1+tMlHZilPPRaX3NHes3DldSODjmswKfNFBWJm9AgMBAAECgYBAkXjtxjF5aDSDk9N5vJTRmvbKwz2nZFuLOYpiYjv2gqjaZkxJNZfjgLkoAvXi7FvftdjI9hUDr3i7b/2cL73l7eVgxqoGQhPH063TUgiZvGBFKhBKwHtIHVN8rHHVrH7hhaMW7XhuhBm8xtT9TtdGfNXyr33HrLYI7kTThfIYLQJBAPuiYUyr/25EmBoCi84MgpzCrJ5mvITGW31epF52Yp+1LdWVYUYF5MPjZbXaFs+kAWorv3a+ITjGsjRO3449kfMCQQDxB4QAO0Trfs7dcVLP8YYftfhmx4+OvEPz0W6W+hVYHpTIqLRWPcE5tcQyMNXVIJ4auRUw91FtB3ml0uA3fWGPAkBone8UAGJXBt+x2KMlTGA1Kl64EICKNymsat+tgkyol5vxs4RYp3Sf2Hfd7kkTlN0dtgiJJq0LPNi0FKKvMXvvAkEAu6HIJo39Jv5+d3CisPIqIUF9cDVmEg2JMKMW3j0spH6aAGMkkAZfB1fytcfCjrlnmxH95zKAlsm871Pr7AaVDQJASDVlp4ZDQawYWc8OvjYUsw5J2U70JIXqLHagvBKVxfDysKS5CY0LVJQybdM/OoImCRfB2ZXFCyQ4M2GHP+/2Gw==`, jsonD)

	fmt.Println(string(signByte))
	fmt.Println(err)

	var requestData = SignPayOutRequest{
		PayOutRequest: reqData,
		Sign:          signByte,
	}
	d, _ := json.Marshal(requestData)
	fmt.Println(string(d))

	//data, err := RsaDecrypt(`MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBAJ+wWN05Yu8lndLwfHoZDPAp8os6iSAMe8y/IyGH5y83PjUu1Ibp15k9noACEGxUZtskzCx+AdxgFx6OX6EkKul/SSaQ2bXUBf1AG2HmSdKM9WuS6kIvT7rduwcndh4aa21+XzeN4b7UQHwwxOMaKuv1Aq/BguPvV13YWOWlGLBNAgMBAAECgYEAgUDFzNpEP/mckz5gIpvgIwxw60zH7Q7trb4P+3X7I/Z8/aMYFtNz1O9ROZ9yF50BW+Oe92xOqUeFrw/iS5XwvKEsIMVnbxluGwNLqWQMYh1TYmPUWf3qBIeXZDAV/ko5K+HHX6dokLptY+vOoan3GIHRy3E1Z8m6ruIOieal2kECQQDhiNxeQ0NVpHdRK7sAQHrXDjEUOmTN86++AQeTqjt+kMwdQmFVPPJ/XYUsJWBz9G6M1pbpTQtYxpKHyqFJ8+O5AkEAtUJ+3u7Mmb+Kik0W8zZqqyJPTHlDptC29dtHjgiG4oJfOsdLOotMA1sGQy9HiZw9LZfCTz6OlybwEN/nYdpjNQJBALHxxc0pzOq5hNqqYjTkqwcaz2ZiXD6S0kpbjZrYaXCG1kUPprQWDk6nfvdB1kQmy0Kb3R3eB+HXaq1xFqi+oSECQHDWamChzCDSSo56GxVhBNpwS2lR/Dl2qM84qLzUthOAEYRn2H8jhDh6i9V7M0+JQdSGKuJmESIrjTsUB9i0uRkCQFFrwz/x0TypG3yAsmkSSVt6YgytB5AMeWcTXv4JdmNG08e+4vfLxEBeGUUcASZgBWq9XDSsZXjnl0Uj8PXiaTI=`, []byte(signByte))
	//fmt.Println(string(data))
	//fmt.Println(err)
}

func TestSign(t *testing.T) {
	data, err := RsaSignEncrypt(`MIICeAIBADANBgkqhkiG9w0BAQEFAASCAmIwggJeAgEAAoGBAN2+LTiRas44jagA5JkKHcjWU0CrrzKRHL8rTTiogNvf5POV4un8NSPw6eFglXr3/OU0wRJkwk5am8/ghbs20lQG2YPsll/XKBVblvYBJlqGzNHgr4kWNoFsWKCNwD77jx62qQxnba+0OeWSe4RcFbfCredB4REJmEpePlDfgqdLAgMBAAECgYBWccZVUDNwE5H91eNIWi5EVnF8O7IAJ5wqVqozFVWu4fjykRM2HrFkwo2a5DlCCB1aCZffINKzExW9E41u769MgpK/CSatuAQKEFhhL16jxwTAbZiWfE/eM49E5WKex1nqOIxDNBGDUat+ipaVI7kYgJhfa/66ReToTP7PpmblgQJBAPwcEN7PMP53nd1GPcSBBSlIw3w1O9uSNsDqeWN2IiTUeorkj0RzzhjdiLqZUALb+pQDWrtTlnm/W/qMUv2Q0sECQQDhKiZfQNOeJSX/Sa4rWmcYEnxJttn7AF63Hm36z0vc3UZKECRbQESDGCUIAhxbkV8O0GJWftOu6E9VYro0pNkLAkEAsf0ql/OVdoKCzJ7zqtEgELaTdRSsTueaina//s1/a5srbPXShBRyVDHLf1oQGASwk2EO5KQt34SPboM1cFmXwQJBANIbJMdJlK5Dd9tqA44Mw5qg1T1r1Kf1RmyVhoa9nMqx5/8AZQyJQeUaSKPaZF/FnPAT309a9WiG+lBw2dR55tUCQQCDP+0mWMRPlanvZ+qizo+Xx4GI9mVVd9ltMllE+SMXJFSN/qi8aQW8hy+1Gh4wYAGKo/PrqiL3ORyW3PuG50Zm`, []byte(`1`), crypto.SHA1)
	fmt.Println(string(data))
	fmt.Println(err)
}
