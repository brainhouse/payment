package ybpay

/**
mer_no	是	string	商户编号
mer_order_no	是	string	商户订单号；保证唯一性
pname	是	string	姓名
pemail	是	string	客户邮箱
phone	是	string	手机号
order_amount	是	int	交易金额 整型,单位卢比
country_code	是	string	国家填：IND
cyy_no	是	string	币种印度填：INR
pay_type	是	string	支付类型填：UPI
notify_url	是	string	异步通知地址；支付成功后，平台主动通知商家系统，商家系统必须指定接收通知的地址
callback_url	否	string	支付提交之后跳转链接
sign	是	签名	签名算法
*/
type CollectRequest struct {
	MerNo       string `json:"mer_no"`
	MerOrderNo  string `json:"mer_order_no"`
	Pname       string `json:"pname"`
	Pemail      string `json:"pemail"`
	Phone       string `json:"phone"`
	OrderAmount int64  `json:"order_amount"`
	CountryCode string `json:"country_code"`
	CyyNo       string `json:"cyy_no"`
	PayType     string `json:"pay_type"`
	NotifyUrl   string `json:"notify_url"`
	CallbackUrl string `json:"callback_url"`
	Sign        string `json:"sign"` //(商户号+商户订单号+密钥
}

/**
{
    "code":"1",
    "msg":"success",
    "mer_no":"21040760685101",
    "mer_order_no":"TEST_2021042207190337313",
    "pay_type":"UPI",
    "pay_url":"http://xxxxx.com/poi/show_code/transferaccounts?order_no=20210422071",
    "order_number":"2021042207190xxx",
    "order_amount":"40000.00",
     "pay_amount":"40000.23",
    "sign":"73582b63d26cfe0ea3f51682e7772fdb"
}
*/
type CollectResponse struct {
	Code        int    `json:"code"`
	Msg         string `json:"msg"`
	MerNo       string `json:"mer_no"`
	MerOrderNo  string `json:"mer_order_no"`
	PayType     string `json:"pay_type"`
	PayURL      string `json:"pay_url"`
	OrderNumber string `json:"order_number"`
	OrderAmount string `json:"order_amount"`
	PayAmount   string `json:"pay_amount"`
	Sign        string `json:"sign"`
}

/**
mer_no	是	string	商户编号
mer_order_no	是	string	商户订单号；保证唯一性
order_amount	是	int	交易金额 整数 ,单位卢比
pay_type	是	string	支付类型填： BANK/UPI（任选其一）
cyy_no	是	string	币种 INR
acc_name	是	string	填姓名
acc_no	否	string	填银行卡号 ；pay_type=BANK的时候必填
province	否	string	用户的IFSC；pay_type=BANK的时候必填
vpa	否	string	用户的UPI账号；pay_type=UPI的时候必填
phone	是	string	用户手机号
email	是	string	用户邮箱
summary	否	string	备注
notifyurl	是	string	异步通知地址；支付成功后，平台主动通知商家系统，商家系统必须指定接收通知的地址
sign	是	签名	除了sign字段，其他字段组成数组然后 json_encode ， 把json串 进行RSA 加密（用商户私钥加密）
*/
type PayOutRequest struct {
	MerNo       string `json:"mer_no"`
	MerOrderNo  string `json:"mer_order_no"`
	Email       string `json:"email"`
	Phone       string `json:"phone"`
	OrderAmount int64  `json:"order_amount"`
	CyyNo       string `json:"cyy_no"`
	AccName     string `json:"acc_name"`
	AccNo       string `json:"acc_no"`
	Province    string `json:"province"`
	VPA         string `json:"vpa"`
	PayType     string `json:"pay_type"`
	NotifyUrl   string `json:"notifyurl"`
}

type SignPayOutRequest struct {
	PayOutRequest
	Sign string `json:"sign"`
}

type PayoutResponse struct {
	Code        int    `json:"code"`
	Msg         string `json:"msg"`
	MerNo       string `json:"mer_no"`
	MerOrderNo  string `json:"mer_order_no"`
	OrderNumber string `json:"order_number"`
	OrderAmount string `json:"order_amount"`
	PayType     string `json:"pay_type"`
	CyyNo       string `json:"cyy_no"`
	AccNo       string `json:"acc_no"`
	AccName     string `json:"acc_name"`
	Province    string `json:"province"`
	Sign        string `json:"sign"`
}

/**
code	int	接口状态：1获取成功 0 失败 ；不可作为订单状态判断
msg	string	返回错误信息
mer_no	string	商户号
mer_order_no	string	商户订单号
order_number	string	平台订单号
order_status	string	订单状态:-1=订单异常,0=待处理,1=转账处理中,2=转账拒绝,3=转账失败,4=转账成功
sign	string	签名
*/
type PayoutQueryResponse struct {
	Code        int    `json:"code"`
	Msg         string `json:"msg"`
	MerNo       string `json:"mer_no"`
	MerOrderNo  string `json:"mer_order_no"`
	OrderNumber string `json:"order_number"`
	OrderStatus string `json:"order_amount"`
	Sign        string `json:"sign"`
}
