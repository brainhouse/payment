package ydgpay

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"funzone_pay/app/validator"
	"funzone_pay/centerlog"
	"funzone_pay/dao"
	"funzone_pay/model"
	"funzone_pay/utils"
	"github.com/wonderivan/logger"
	"io/ioutil"
	"net/http"
	"net/url"
	"sort"
	"strings"
	"time"
)

type YDGPayService struct {
	PAY_ACCOUNT_ID string
	PAY_CHANNEL    string
	MID            string
	SK             string
	HOST           string
	NOTIFY_URL     string
	OUT_NOTIFY_URL string
	COUNTRY_CODE   int
	PAYOUT_SK      string
}

func GetYDGIns(conf map[string]interface{}) *YDGPayService {
	return &YDGPayService{
		PAY_ACCOUNT_ID: conf["PAY_ACCOUNT_ID"].(string),
		PAY_CHANNEL:    conf["PAY_CHANNEL"].(string),
		MID:            conf["MID"].(string),
		SK:             conf["SK"].(string),
		HOST:           conf["HOST"].(string),
		NOTIFY_URL:     conf["IN_NOTIFY_URL"].(string),
		OUT_NOTIFY_URL: conf["OUT_NOTIFY_URL"].(string),
		COUNTRY_CODE:   conf["COUNTRY_CODE"].(int),
		PAYOUT_SK:      conf["PAYOUT_SK"].(string),
	}
}

func (Y YDGPayService) CreateOrder(args validator.PayEntryValidator) (*model.PayEntry, error) {
	orderId := utils.GetOrderId("YDGIN")
	requestUrl := fmt.Sprintf("%s%s", Y.HOST, "/ext_api/v1/payment/add")

	params := url.Values{}
	params.Add("merchantNo", Y.MID)
	params.Add("merTradeNo", orderId)
	params.Add("amount", fmt.Sprint(args.Amount))
	params.Add("currency", "INR")
	params.Add("name", args.UserName)
	params.Add("tel", args.Phone)
	params.Add("type", "upi")
	params.Add("email", args.Email)
	params.Add("callbackUrl", Y.NOTIFY_URL)
	sign := Y.GetSign(params, Y.SK)

	var rData = &CollectRequest{
		MerchantNo:  Y.MID,
		MerTradeNo:  orderId,
		Amount:      fmt.Sprint(args.Amount),
		Currency:    "INR",
		Name:        args.UserName,
		Tel:         args.Phone,
		Type:        "upi",
		Email:       args.Email,
		CallbackURL: Y.NOTIFY_URL,
		Sign:        sign,
	}
	header := map[string][]string{
		"Content-type": {"application/json"},
	}

	data, err := Y.Request(requestUrl, "POST", "JSON", rData, header)
	logger.Debug("YDGPayService_CreateOrder_Info | orderReq=%v | err=%v | data=%v", rData, err, string(data))

	fmt.Println(string(data))
	egResponse := &CollectResponse{}
	err = json.Unmarshal(data, egResponse)
	if err != nil {
		logger.Error("YDGPayService_CreateOrder_JsonUnmarshal_Err | err=%v", err)
		centerlog.OrderError(centerlog.MsgThirdPlatformFail, Y.PAY_ACCOUNT_ID, args.AppId, args.AppOrderId, orderId, "false", err.Error())
		return nil, err
	}
	//请求成功写入流水
	pE := new(model.PayEntry)
	pE.AppId = args.AppId
	pE.Uid = args.Uid
	pE.PayAccountId = Y.PAY_ACCOUNT_ID
	pE.PayChannel = model.YDGPAY
	pE.CountryCode = Y.COUNTRY_CODE
	pE.AppOrderId = args.AppOrderId
	pE.PaymentOrderId = egResponse.Data.OrderNo
	pE.OrderId = orderId
	pE.Amount = args.Amount
	pE.UserId = args.UserId
	pE.UserName = args.UserName
	pE.Email = args.Email
	pE.Phone = args.Phone
	pE.ThirdCode = egResponse.ErrorCode
	pE.Status = model.PAYING
	pE.FinishTime = time.Now().Unix()

	fail := false
	if egResponse.Status == 0 {
		fail = true
		pE.Status = model.PAYFAILD
		pE.ThirdCode = fmt.Sprint(egResponse.ErrorCode)
		pE.ThirdDesc = fmt.Sprintf("%v-%v", egResponse.ErrorCode, egResponse.Message)
		centerlog.OrderError(centerlog.MsgThirdPlatformFail, Y.PAY_ACCOUNT_ID, args.AppId, args.AppOrderId, orderId, fmt.Sprint(egResponse.ErrorCode), egResponse.Message)
	}
	engine, err := dao.GetMysql()
	if engine == nil || err != nil {
		logger.Error("YDGPayService_CreateOrder_GetDB_Err | err=%v", err)
		return nil, err
	}
	_, err = engine.Insert(pE)
	if err != nil {
		logger.Error("YDGPayService_CreateOrder_InsertDB_Err | err=%v", err)
		return nil, err
	}
	logger.Debug("YDGPayService_CreateOrder_Request | req=%v | res=%v", rData, egResponse)
	if fail {
		return nil, errors.New(pE.ThirdDesc)
	}
	pE.PaymentLinkHost = egResponse.Data.PayLink
	return pE, err
}

func (Y YDGPayService) GetSign(params url.Values, sk string) string {
	var sortSlice []string
	for k, v := range params {
		if k == "sign" || v[0] == "" {
			continue
		}
		sortSlice = append(sortSlice, k)
	}
	sort.Strings(sortSlice)
	signs := ""
	for _, v := range sortSlice {
		signs = fmt.Sprintf("%v%v=%v&", signs, v, params[v][0])
	}
	signs += "signKey=" + sk
	fmt.Println(signs)
	return strings.ToUpper(utils.Md5Str(signs))
}

func (Y YDGPayService) Inquiry(orderId string, s2 string) (*model.PayEntry, error) {
	params := url.Values{}
	params.Add("merchantNo", Y.MID)
	params.Add("merTradeNo", orderId)
	sign := Y.GetSign(params, Y.SK)

	reqData := &struct {
		MerNo      string `json:"merchantNo"`
		MerOrderNo string `json:"merTradeNo"`
		Sign       string `json:"sign"`
	}{
		MerNo:      Y.MID,
		MerOrderNo: orderId,
		Sign:       sign,
	}
	requestUrl := fmt.Sprintf("%s%s", Y.HOST, "/ext_api/v1/payment/query")
	header := map[string][]string{
		"Content-Type": {"application/json"},
	}
	data, err := Y.Request(requestUrl, "POST", "JSON", reqData, header)
	if err != nil {
		logger.Error("YDGPayService_Collect_Inquiry_Error | err=%v", err)
		return nil, err
	}
	logger.Debug("YDGPayService_Collect_Inquiry_Data | orderId=%v | res=%v", orderId, string(data))
	statusData := &struct {
		Status int    `json:"status"`
		Msg    string `json:"message"`
		Data   struct {
			OrderStatus string `json:"orderStatus"`
		} `json:"data"`
	}{}
	err = json.Unmarshal(data, statusData)
	if err != nil {
		logger.Error("YDGPayService_CollectInquiry_JsonError | err=%v | data=%v", err, string(data))
		return nil, err
	}

	engine, _ := dao.GetMysql()
	payEntry := new(model.PayEntry)
	exist, err := engine.Where("order_id=?", orderId).Get(payEntry)
	if err != nil {
		logger.Error("YDGPayService_Collect_Inquiry_GetError | err=%v | data=%v", err, orderId)
		return nil, err
	}
	if !exist {
		return nil, errors.New("NotExist")
	}
	if statusData.Data.OrderStatus == "2" {
		payEntry.Status = model.PAYFAILD
		payEntry.ThirdDesc = statusData.Msg
	} else if statusData.Data.OrderStatus == "1" {
		payEntry.Status = model.PAYSUCCESS
	}
	return payEntry, nil
}

func (Y YDGPayService) InquiryPayout(orderId string, ext string) (*model.PayOut, error) {
	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("YDGPayService_InquiryPayout_Engine_Error | data=%+v | err=%v", orderId, err)
		return nil, err
	}
	payOut := new(model.PayOut)
	exist, err := engine.Where("order_id=?", orderId).Get(payOut)
	if err != nil {
		logger.Error("YDGPayService_InquiryPayout_GetError | err=%v | data=%v", err, orderId)
		return nil, err
	}
	if !exist {
		return nil, errors.New("NotExist")
	}
	requestUrl := fmt.Sprintf("%s/ext_api/v1/payout/query", Y.HOST)

	header := map[string][]string{
		"Content-Type": {"application/json"},
	}

	params := url.Values{}
	params.Set("merchantNo", Y.MID)
	params.Set("merTradeNo", orderId)
	sign := Y.GetSign(params, Y.PAYOUT_SK)

	reqData := struct {
		MerchantNo string `json:"merchantNo"`
		MerTradeNo string `json:"merTradeNo"`
		Sign       string `json:"sign"`
	}{
		MerchantNo: Y.MID,
		MerTradeNo: orderId,
		Sign:       sign,
	}

	data, err := Y.Request(requestUrl, "POST", "JSON", reqData, header)
	if err != nil {
		logger.Error("YDGPayService_InquiryPayout_Request_Error | err=%v", err)
		return nil, err
	}
	logger.Debug("YDGPayService_InquiryPayout_Info | data=%v | orderId=%v | req=%v", string(data), orderId, reqData)
	responseData := new(PayoutQueryResponse)
	err = json.Unmarshal(data, responseData)
	if err != nil {
		logger.Error("YDGPayService_InquiryPayout_JsonError | err=%v | data=%v", err, string(data))
		return nil, err
	}
	if responseData.Data.OrderStatus == "2" {
		payOut.Status = model.PAY_OUT_FAILD
		payOut.ThirdDesc = responseData.Message
	} else if responseData.Data.OrderStatus == "1" {
		payOut.Status = model.PAY_OUT_SUCCESS
	}
	return payOut, nil
}

func (Y YDGPayService) PayOut(frozenId int64, outValidator validator.PayOutValidator) (payOut *model.PayOut, err error) {
	transId := utils.GetOrderId("YDGOUT")
	payOut = new(model.PayOut)
	payOut.Uid = outValidator.Uid
	payOut.AppId = outValidator.AppId
	payOut.PayAccountId = Y.PAY_ACCOUNT_ID
	payOut.PayChannel = model.YDGPAY
	payOut.CountryCode = Y.COUNTRY_CODE
	payOut.PayType = outValidator.PayType
	payOut.AppOrderId = outValidator.AppOrderId
	payOut.OrderId = transId
	payOut.PaymentId = transId
	payOut.FrozenId = frozenId
	payOut.Amount = outValidator.Amount
	payOut.BankCard = outValidator.BankCard
	payOut.UserId = outValidator.UserId
	payOut.UserName = outValidator.UserName
	payOut.Email = outValidator.Email
	payOut.Paytm = outValidator.PayTm
	payOut.IFSC = outValidator.IFSC
	payOut.Phone = outValidator.Phone
	engine, err := dao.GetMysql()
	if engine == nil || err != nil {
		logger.Error("YDGPayService_PayOut_GetDB_Err | err=%v", err)
		return nil, err
	}
	_, err = engine.Insert(payOut)
	if err != nil {
		logger.Error("YDGPayService_PayOut_Insert_Error | data=%+v | err=%v", payOut, err)
		return nil, err
	}
	//发起支付
	requestUrl := fmt.Sprintf("%s%s", Y.HOST, "/ext_api/v1/payout/add")
	header := map[string][]string{
		"Content-Type": {"application/json"},
	}

	reqData := PayOutRequest{
		MerchantNo:  Y.MID,
		MerTradeNo:  transId,
		Amount:      fmt.Sprint(outValidator.Amount),
		Currency:    "INR",
		Name:        outValidator.UserName,
		Tel:         outValidator.Phone,
		Email:       outValidator.Email,
		CallbackURL: Y.OUT_NOTIFY_URL,
	}
	params := url.Values{}
	params.Add("merchantNo", Y.MID)
	params.Add("merTradeNo", transId)
	params.Add("amount", fmt.Sprint(outValidator.Amount))
	params.Add("currency", "INR")
	params.Add("name", outValidator.UserName)
	params.Add("tel", outValidator.Phone)
	params.Add("email", outValidator.Email)
	params.Add("callbackUrl", Y.OUT_NOTIFY_URL)

	if outValidator.PayType == model.PT_BANK {
		reqData.Type = "ifsc"
		reqData.Account = outValidator.BankCard
		reqData.AccCode = outValidator.IFSC

		params.Add("type", "ifsc")
		params.Add("accCode", outValidator.IFSC)
		params.Add("account", outValidator.BankCard)
	} else if outValidator.PayType == "upi" {
		reqData.Type = "upi"
		reqData.Account = outValidator.VPA

		params.Add("type", "upi")
		params.Add("account", outValidator.VPA)
	} else {
		return nil, err
	}
	reqData.Sign = Y.GetSign(params, Y.PAYOUT_SK)
	data, err := Y.Request(requestUrl, "POST", "JSON", reqData, header)
	if err != nil {
		logger.Error("YDGPayService_PayOut_Request_Error | err=%v", err)
		return nil, err
	}

	logger.Debug("YDGPayService_PayOut_Info | requestUrl=%v | requestData=%v | response=%v", requestUrl, reqData, string(data))

	res := &PayoutResponse{}
	err = json.Unmarshal(data, res)
	if err != nil {
		logger.Error("YDGPayService_PayOut_JsonUnmarshal_Error | err=%v | res=%v", err, res)
		centerlog.OrderError(centerlog.MsgThirdPlatformFail, Y.PAY_ACCOUNT_ID, payOut.AppId, payOut.AppOrderId, payOut.OrderId, "false", err.Error())
		return nil, err
	}
	var (
		status = model.PAY_OUT_ING
	)
	_, err = engine.Where("id=?", payOut.Id).Update(&model.PayOut{
		Status:     status,
		ThirdCode:  payOut.ThirdCode,
		PaymentId:  res.Data.OrderNo,
		ThirdDesc:  res.Message,
		FinishTime: time.Now().Unix(),
	})
	if err != nil {
		logger.Error("YDGPayService_PayOut_Insert_Error | data=%v | err=%v", payOut, err)
		return nil, err
	}
	return payOut, nil
}

func (Y YDGPayService) PayOutBatch(frozenId int64, args validator.PayOutValidator) (*model.PayOut, error) {
	return Y.PayOut(frozenId, args)
}

func (Y YDGPayService) InSignature(cb interface{}) bool {
	panic("implement me")
}

func (Y YDGPayService) OutSignature(cb interface{}) bool {
	panic("implement me")
}

func (Y YDGPayService) Request(url string, method string, contentType string, sendData interface{}, header map[string][]string) (res []byte, err error) {
	client := &http.Client{}
	var req *http.Request
	if contentType == "JSON" {
		data, _ := json.Marshal(sendData)
		fmt.Println(string(data))
		reader := bytes.NewBuffer(data)
		req, err = http.NewRequest(method, url, reader)
	} else {
		s := sendData.(string)
		reader := strings.NewReader(s)
		req, err = http.NewRequest(method, url, reader)
	}
	fmt.Println(url)
	//提交请求
	if err != nil {
		logger.Error("YDGPayService_Create_order | url=%v | err=%+v | req=%+v", url, err, sendData)
		return
	}
	req.Header = http.Header(header)
	//处理返回结果
	response, err := client.Do(req)
	if err != nil {
		logger.Error("YDGPayService_request_err | url=%v | err=%+v | req=%+v", url, err, sendData)
		return
	}
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		logger.Error("YDGPayService_response_read_err | url=%v | err=%+v | req=%+v", url, err, sendData)
		return
	}
	defer func() {
		_ = response.Body.Close()
	}()
	return body, nil
}
