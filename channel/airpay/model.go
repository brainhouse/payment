package airpay

type CollectRequest struct {
	Amount      string `json:"amount"`
	CallbackURL string `json:"callbackUrl"`
	Currency    string `json:"currency"`
	Email       string `json:"email"`
	MerTradeNo  string `json:"merTradeNo"`
	MerchantNo  string `json:"merchantNo"`
	Name        string `json:"name"`
	Sign        string `json:"sign"`
	Tel         string `json:"tel"`
	Type        string `json:"type"`
}

// BscApiResponsePaymentResData
type CollectResponse struct {
	Data      PaymentResData `json:"data,omitempty"`
	ErrorCode string         `json:"errorCode,omitempty"` // 错误码
	Message   string         `json:"message,omitempty"`   // 错误信息
	Sign      string         `json:"sign,omitempty"`      // 签名
	Status    int64          `json:"status,omitempty"`    // 接口状态值，1 成功， 0失败
}

// PaymentResData
type PaymentResData struct {
	MerTradeNo string `json:"merTradeNo,omitempty"` // 商户订单号
	OrderNo    string `json:"orderNo,omitempty"`    // 平台订单号
	PayLink    string `json:"payLink,omitempty"`    // 支付链接
}

type PayOutRequest struct {
	AccCode     string `json:"accCode"`
	Account     string `json:"account"`
	Amount      string `json:"amount"`
	CallbackURL string `json:"callbackUrl"`
	Currency    string `json:"currency"`
	Email       string `json:"email"`
	MerTradeNo  string `json:"merTradeNo"`
	MerchantNo  string `json:"merchantNo"`
	Name        string `json:"name"`
	Sign        string `json:"sign"`
	Tel         string `json:"tel"`
	Type        string `json:"type"`
}

type PayoutResponse struct {
	Data      PayoutResData `json:"data,omitempty"`
	ErrorCode string        `json:"errorCode,omitempty"` // 错误码
	Message   string        `json:"message,omitempty"`   // 错误信息
	Sign      string        `json:"sign,omitempty"`      // 签名
	Status    int64         `json:"status,omitempty"`    // 接口状态值，1 成功， 0失败
}

// PayoutResData
type PayoutResData struct {
	MerTradeNo string `json:"merTradeNo,omitempty"` // 商户订单号
	OrderNo    string `json:"orderNo,omitempty"`    // 系统订单号
}

// BscApiResponsePayoutOrderData
type PayoutQueryResponse struct {
	Data      PayoutOrderData `json:"data,omitempty"`
	ErrorCode string          `json:"errorCode,omitempty"` // 错误码
	Message   string          `json:"message,omitempty"`   // 错误信息
	Sign      string          `json:"sign,omitempty"`      // 签名
	Status    int             `json:"status,omitempty"`    // 接口状态值，1 成功， 0失败
}

// PayoutOrderData
type PayoutOrderData struct {
	Amount      string `json:"amount,omitempty"`      // 订单金额
	CreateTime  string `json:"createTime,omitempty"`  // 订单创建时间
	MerchantNo  string `json:"merchantNo,omitempty"`  // 商户号
	MerTradeNo  string `json:"merTradeNo,omitempty"`  // 商户订单号
	OrderNo     string `json:"orderNo,omitempty"`     // 平台订单号
	OrderStatus string `json:"orderStatus,omitempty"` // 订单状态，见数据字典
	Poundage    string `json:"poundage,omitempty"`    // 手续费
	Type        string `json:"type,omitempty"`        // 支付类型（ifsc/upi)
}
