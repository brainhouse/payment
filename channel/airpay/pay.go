package airpay

import (
	"bytes"
	"crypto/md5"
	"crypto/rand"
	"encoding/base64"
	"encoding/json"
	"encoding/xml"
	"errors"
	"fmt"
	"funzone_pay/app/validator"
	"funzone_pay/common"
	"funzone_pay/dao"
	"funzone_pay/model"
	"funzone_pay/utils"
	"hash/crc32"
	"io/ioutil"
	"net/http"
	"net/url"
	"regexp"
	"strconv"
	"strings"
	"time"

	"github.com/wonderivan/logger"
)

const AirPaySDKAccount = "rummybank123"
const airCallType = "upiqr"

var gaTxnIdRegexp, _ = regexp.Compile(`ga_txn_id\s{0,}=\s{0,}\d+`)

// var airPayUpiFmt = "upi://pay?pa=apsl.mindgeek271754@timecosmos&pn=Mindgeek&cu=INR&tn=Pay to Mindgeek&am=%s&mc=5816&mode=04&tr=APSL%s"
var airPayUpiFmt = []string{
	"pa=apsl.mindgeek271754@timecosmos&pn=Mindgeek&cu=INR&tn=Pay to Mindgeek&am=%s&mc=5816&mode=04&tr=APSL%s",
	"pa=apsl.softquidnet279111@timecosmos&pn=Softquid Network&cu=INR&tn=Pay to Softquid Network&am=%s&mc=5816&mode=04&tr=APSL%s",
}

func GenAirPayUPILink(id string, amount, code string) string {
	airFmt := airPayUpiFmt[0]
	if id == "2" {
		airFmt = airPayUpiFmt[1]
	}
	return fmt.Sprintf(airFmt, amount, code)
}

type AirPayService struct {
	ID             string
	PAY_ACCOUNT_ID string
	PAY_CHANNEL    string
	MID            string
	CALL_TYPE      string
	SK             string
	USERNAME       string
	PWD            string
	HOST           string
	COLLECT_HOST   string
	REFERER        string
	COUNTRY_CODE   int
}

func GetAirPayIns(conf map[string]interface{}) *AirPayService {
	callType := airCallType
	v, ok := conf["CALL_TYPE"]
	if ok {
		callType = v.(string)
	}
	return &AirPayService{
		ID:             conf["ID"].(string),
		PAY_ACCOUNT_ID: conf["PAY_ACCOUNT_ID"].(string),
		PAY_CHANNEL:    conf["PAY_CHANNEL"].(string),
		MID:            conf["MID"].(string),
		CALL_TYPE:      callType,
		SK:             conf["SK"].(string),
		USERNAME:       conf["USERNAME"].(string),
		PWD:            conf["PWD"].(string),
		HOST:           conf["HOST"].(string),
		COUNTRY_CODE:   conf["COUNTRY_CODE"].(int),
		COLLECT_HOST:   conf["COLLECT_HOST"].(string),
		REFERER:        conf["REFERER"].(string),
	}
}

func (a AirPayService) CreateOrderAndroid(pk, orderToken, orderId string, args validator.PayEntryValidator) (*model.PayEntry, error) {
	//请求成功写入流水
	pE := new(model.PayEntry)
	pE.AppId = args.AppId
	pE.Uid = args.Uid
	pE.PayAccountId = a.PAY_ACCOUNT_ID
	pE.PayChannel = model.AIRPAY
	pE.CountryCode = a.COUNTRY_CODE
	pE.AppOrderId = args.AppOrderId
	pE.PaymentOrderId = orderToken
	pE.OrderId = orderId
	pE.Amount = args.Amount
	pE.UserId = args.UserId
	pE.UserName = args.UserName
	pE.Email = args.Email
	pE.Phone = args.Phone
	pE.Status = model.PAYING
	pE.FinishTime = time.Now().Unix()

	engine, err := dao.GetMysql()
	if engine == nil || err != nil {
		logger.Error("AirPayService_CreateOrder_GetDB_Err | err=%v", err)
		return nil, err
	}
	_, err = engine.Insert(pE)
	if err != nil {
		logger.Error("AirPayService_CreateOrder_InsertDB_Err | err=%v", err)
		return nil, err
	}
	pE.PaymentLinkHost = "https://gle.com/#/"
	pE.PK = pk
	pE.Mid = a.MID
	return pE, err
}

func (a AirPayService) CreateOrderQr(args validator.PayEntryValidator) (*model.PayEntry, error) {
	orderId := utils.GetOrderId("AIN" + a.ID)

	amount := fmt.Sprintf("%.2f", args.Amount)
	dom := base64.StdEncoding.EncodeToString([]byte(a.REFERER))
	//$alldata = $mercid.$orderid.$amt.$buyerPhone.$buyerEmail.$mer_dom.$call_type;
	allData := fmt.Sprintf("%v%v%v%v%v%v%v", a.MID, orderId, amount, args.Phone, args.Email, dom, airCallType)

	keySha256 := utils.HSha256(fmt.Sprintf("%v~:~%v", a.USERNAME, a.PWD))

	nTime := time.Now().Format("2006-01-02")
	checksum := utils.HSha256(fmt.Sprintf("%v@%v%v", keySha256, allData, nTime))

	unix := time.Now().Unix()
	isAlarm := false
	defer func() {
		cost := time.Now().Unix() - unix
		if isAlarm || cost > 15 {
			common.PushAlarmEvent(common.Warning, fmt.Sprintf("%v", args.Uid), "AirPayService/CreateOrder", "", fmt.Sprintf("%v-%v-%v Create Order By AriPay QR Error, cost:%v", args.AppId, args.Uid, orderId, cost))
		}
	}()

	//fields := map[string]string{
	//	"mercid":     a.MID,
	//	"orderid":    orderId,
	//	"amount":     amount,
	//	"buyerPhone": args.Phone,
	//	"buyerEmail": args.Email,
	//	"mer_dom":    dom,
	//	"call_type":  airCallType,
	//}

	fields := struct {
		Mid     string `json:"mercid"`
		OrderId string `json:"orderid"`
		Amount  string `json:"amount"`
		Phone   string `json:"buyerPhone"`
		Email   string `json:"buyerEmail"`
		Dom     string `json:"mer_dom"`
		T       string `json:"call_type"`
	}{
		Mid:     a.MID,
		OrderId: orderId,
		Amount:  amount,
		Phone:   args.Phone,
		Email:   args.Email,
		Dom:     dom,
		T:       a.CALL_TYPE,
	}

	rawData, err := json.Marshal(fields)
	if err != nil {
		logger.Error("AirPayService_CreateOrder_Marshal req=%v|%v | err=%v", args.AppOrderId, orderId, err)
		isAlarm = true
		return nil, err
	}

	randIv := make([]byte, 8)
	_, err = rand.Read(randIv)
	if err != nil {
		logger.Error("AirPayService_CreateOrder_RandIV req=%v|%v | err=%v", args.AppOrderId, orderId, err)
		isAlarm = true
		return nil, err
	}

	encKey := fmt.Sprintf("%x", md5.Sum([]byte(a.SK)))
	iv := fmt.Sprintf("%x", randIv)
	encData, err := utils.AESEncryptWithBase64(rawData, []byte(encKey), []byte(iv))
	if err != nil {
		logger.Error("AirPayService_CreateOrder_AESEnc req=%v|%v | err=%v", args.AppOrderId, string(rawData), err)
		isAlarm = true
		return nil, err
	}

	reqData := map[string]string{
		"encData":  fmt.Sprintf("%v%v", iv, encData),
		"checksum": checksum,
		"mercid":   a.MID,
	}

	header := http.Header{}
	header.Add("Content-Type", "application/json")
	res, err := a.Request("https://kraken.airpay.co.in/airpay/api/generateOrder.php", http.MethodPost, "JSON", reqData, header)
	if err != nil {
		logger.Error("AirPayService_CreateOrder_QR_Err req=%v|%v:%v | err=%v", args.AppOrderId, orderId, reqData, err)
		isAlarm = true
		return nil, err
	}

	responsRaw := struct {
		Data string `json:"data"`
	}{}

	err = json.Unmarshal(res, &responsRaw)
	if err != nil {
		logger.Error("AirPayService_CreateOrder_Unmarshal_Err res=%v|%v|%v | err=%v", args.AppOrderId, orderId, string(res), err)
		isAlarm = true
		return nil, err
	}

	div := []byte(responsRaw.Data[:16])
	dRaw, err := base64.StdEncoding.DecodeString(responsRaw.Data[16:])
	if err != nil {
		logger.Error("AirPayService_CreateOrder_base64Decode_Err res=%v|%v|%v | err=%v", args.AppOrderId, orderId, responsRaw.Data, err)
		isAlarm = true
		return nil, err
	}

	result, err := utils.AESDecrypt(dRaw, []byte(encKey), div)
	if err != nil {
		logger.Error("AirPayService_CreateOrder_AESDec_Err res=%v|%v|%v | err=%v", args.AppOrderId, orderId, responsRaw.Data, err)
		isAlarm = true
		return nil, err
	}

	logger.Debug("AirPayService_CreateOrder_Qr reqData=%v,orderId=%v,appOrder=%v,res=%v", string(rawData), orderId, args.AppOrderId, string(result))

	ret := struct {
		QRCODE string `json:"QRCODE_STRING"`
		MID    int    `json:"MID"`
		RID    string `json:"RID"`
		Status int    `json:"status"`
		//"QRCODE_STRING": "upi://pay?pa=example@icici&pn=Adam%20Innovations%20Test&cu=INR&tn=Pay to Adam%20Innovations%20Test&am=1.00&mc=5045&mode=04&tr=APS17722152&td=APS17722152",
		//"MID": 18999,
		//"RID": 17722152,
		//"status": 200
	}{}

	err = json.Unmarshal(result, &ret)
	if err != nil {
		logger.Error("AirPayService_CreateOrder_Unmarshal_raw_Err res=%v|%v|%v | err=%v", args.AppOrderId, orderId, string(result), err)
		isAlarm = true
		return nil, err
	}

	values := strings.Split(ret.QRCODE, "?")
	if len(values) < 2 {
		isAlarm = true
		return nil, fmt.Errorf("invalid qrcode[%v]", ret.QRCODE)
	}

	qr := strings.ReplaceAll(values[1], "+", "")
	qr = strings.ReplaceAll(qr, "\t", "")
	qr = strings.ReplaceAll(qr, " ", "")

	logger.Debug("AirPayService_CreateOrder_Qr respData=%v,orderId=%v,appOrder=%v,qr=%v,dur:%v", string(result), orderId, args.AppOrderId, qr, time.Now().Unix()-unix)

	//请求成功写入流水
	pE := new(model.PayEntry)
	pE.AppId = args.AppId
	pE.Uid = args.Uid
	pE.PayAccountId = a.PAY_ACCOUNT_ID
	pE.PayChannel = model.AIRPAY
	pE.CountryCode = a.COUNTRY_CODE
	pE.AppOrderId = args.AppOrderId
	pE.PaymentOrderId = ""
	pE.OrderId = orderId
	pE.Amount = args.Amount
	pE.UserId = args.UserId
	pE.UserName = args.UserName
	pE.Email = args.Email
	pE.Phone = args.Phone
	pE.Status = model.PAYING
	pE.Created = time.Now().Unix()

	engine, err := dao.GetMysql()
	if engine == nil || err != nil {
		logger.Error("AirPayService_CreateOrder_GetDB_Err | err=%v", err)
		return nil, err
	}
	_, err = engine.Insert(pE)
	if err != nil {
		logger.Error("AirPayService_CreateOrder_InsertDB_Err | err=%v", err)
		return nil, err
	}
	pE.PaymentLinkHost = a.COLLECT_HOST
	pE.SignatureNew = base64.URLEncoding.EncodeToString([]byte(qr))
	//pE.PK = pk
	//pE.Mid = a.MID
	return pE, err
}

func (a AirPayService) CreateOrder(args validator.PayEntryValidator) (*model.PayEntry, error) {
	if a.CALL_TYPE == airCallType {
		return a.CreateOrderQr(args)
	}

	orderId := utils.GetOrderId("AIN" + a.ID)
	pk := utils.HSha256(fmt.Sprintf("%v@%v:|:%v", a.SK, a.USERNAME, a.PWD))

	keySha256 := utils.HSha256(fmt.Sprintf("%v~:~%v", a.USERNAME, a.PWD))

	nTime := time.Now().Format("2006-01-02")
	amount := fmt.Sprintf("%.2f", args.Amount)
	//$buyerEmail.$buyerFirstName.$buyerLastName.$buyerAddress.$buyerCity.$buyerState.$buyerCountry.$amount.$orderid
	allData := fmt.Sprintf("%v%v%v%v%v%v", args.Email, args.UserName, args.UserName, amount, orderId, nTime)
	orderToken := utils.HSha256(fmt.Sprintf("%v@%v", keySha256, allData))

	//if args.AppId == AirPaySDKAccount {
	//	return a.CreateOrderAndroid(pk, orderToken, orderId, args)
	//}

	sendData := url.Values{}
	sendData.Add("privatekey", pk)
	sendData.Add("mercid", a.MID)
	sendData.Add("orderid", orderId)
	sendData.Add("kittype", "iframe")
	sendData.Add("chmod", "")
	sendData.Add("buyerEmail", args.Email)
	sendData.Add("buyerPhone", args.Phone)
	sendData.Add("buyerFirstName", args.UserName)
	sendData.Add("buyerLastName", args.UserName)
	sendData.Add("buyerAddress", "")
	sendData.Add("buyerCity", "")
	sendData.Add("buyerState", "")
	sendData.Add("buyerCountry", "")
	sendData.Add("buyerPinCode", "")
	sendData.Add("amount", amount)
	sendData.Add("customvar", "")
	sendData.Add("currency", "356")
	sendData.Add("isocurrency", "INR")
	sendData.Add("txnsubtype", "")
	sendData.Add("checksum", orderToken)

	unix := time.Now().Unix()
	isAlarm := false
	defer func() {
		cost := time.Now().Unix() - unix
		if isAlarm || cost > 15 {
			common.PushAlarmEvent(common.Warning, fmt.Sprintf("%v", args.Uid), "AirPayService/CreateOrder", "", fmt.Sprintf("%v-%v-%v Create Order By AriPay Error, cost:%v", args.AppId, args.Uid, orderId, cost))
		}
	}()

	header := http.Header{}
	header.Add("Content-Type", "application/x-www-form-urlencoded")
	header.Add("Referer", a.REFERER)
	reqData := sendData.Encode()
	res, err := a.Request("https://payments.airpay.co.in/pay/index.php", http.MethodPost, "Form", reqData, header)
	if err != nil {
		logger.Error("AirPayService_CreateOrder_Err req=%v | err=%v", reqData, err)
		isAlarm = true
		return nil, err
	}

	gaTxnId := gaTxnIdRegexp.FindString(string(res))
	values := strings.Split(gaTxnId, "=")
	if len(values) < 2 {
		logger.Error("AirPayService_CreateOrder_Err req=%v | res=%v", reqData, string(res))
		isAlarm = true
		return nil, fmt.Errorf("channel busy A")
	}

	code := strings.TrimSpace(values[1])
	if len(code) < 1 {
		logger.Error("AirPayService_CreateOrder_Err ga=%v | res=%v", gaTxnId, string(res))
		isAlarm = true
		return nil, fmt.Errorf("channel busy A")
	}

	upiUrl := GenAirPayUPILink(a.ID, amount, code)
	logger.Debug("AirPayService_CreateOrder reqData=%v,orderId=%v,appOrder=%v,gat_txn_id=%v,values=%v,upiUrl=%v,dur:%v", reqData, orderId, args.AppOrderId, gaTxnId, values, upiUrl, time.Now().Unix()-unix)

	//请求成功写入流水
	pE := new(model.PayEntry)
	pE.AppId = args.AppId
	pE.Uid = args.Uid
	pE.PayAccountId = a.PAY_ACCOUNT_ID
	pE.PayChannel = model.AIRPAY
	pE.CountryCode = a.COUNTRY_CODE
	pE.AppOrderId = args.AppOrderId
	pE.PaymentOrderId = code
	pE.OrderId = orderId
	pE.Amount = args.Amount
	pE.UserId = args.UserId
	pE.UserName = args.UserName
	pE.Email = args.Email
	pE.Phone = args.Phone
	pE.Status = model.PAYING
	pE.Created = time.Now().Unix()

	engine, err := dao.GetMysql()
	if engine == nil || err != nil {
		logger.Error("AirPayService_CreateOrder_GetDB_Err | err=%v", err)
		return nil, err
	}
	_, err = engine.Insert(pE)
	if err != nil {
		logger.Error("AirPayService_CreateOrder_InsertDB_Err | err=%v", err)
		return nil, err
	}
	pE.PaymentLinkHost = a.COLLECT_HOST
	pE.SignatureNew = base64.URLEncoding.EncodeToString([]byte(upiUrl))
	pE.PK = pk
	pE.Mid = a.MID
	return pE, err
}

func (a AirPayService) Inquiry(orderId string, s2 string) (*model.PayEntry, error) {
	/**
	date = YYYY-MM-DD
	alldata = merchant_id.merchant_txn_id.processor_id.terminal_id .txn_type.date
	Key generated by : key = hash('SHA256',username."~:~".password)
	Hash generated by : hash('SHA256', key.'@'.alldata)
	*/
	pk := utils.HSha256(fmt.Sprintf("%v@%v:|:%v", a.SK, a.USERNAME, a.PWD))
	nTime := time.Now().Format("2006-01-02")
	keySha256 := utils.HSha256(fmt.Sprintf("%v~:~%v", a.USERNAME, a.PWD))
	allData := fmt.Sprintf("%v%v%v", a.MID, orderId, nTime)
	sign := utils.HSha256(fmt.Sprintf("%v@%v", keySha256, allData))
	params := url.Values{}
	params.Add("merchant_id", a.MID)
	params.Add("merchant_txn_id", orderId)
	params.Add("private_key", pk)
	params.Add("checksum", sign)

	requestUrl := fmt.Sprintf("%s%s", a.HOST, "/airpay/order/verify.php")
	header := map[string][]string{
		"Content-Type": {"application/x-www-form-urlencoded"},
	}
	data, err := a.Request(requestUrl, "POST", "", params.Encode(), header)
	if err != nil {
		logger.Error("AirPayService_Collect_Inquiry_Error | err=%v", err)
		return nil, err
	}
	logger.Debug("AirPayService_Collect_Inquiry_Data | orderId=%v | res=%v", orderId, string(data))
	type StatusData struct {
		XMLName           xml.Name `xml:"TRANSACTION"`
		TRANSACTIONSTATUS string   `xml:"TRANSACTIONSTATUS"`
		APTRANSACTIONID   string   `xml:"APTRANSACTIONID"`
		AMOUNT            string   `xml:"AMOUNT"`
		MESSAGE           string   `xml:"MESSAGE"`
		TRANSACTIONTYPE   string   `xml:"TRANSACTIONTYPE"`
	}

	type Response struct {
		XMLName xml.Name `xml:"RESPONSE"`
		Data    StatusData
	}
	xmlData := Response{}
	err = xml.Unmarshal(data, &xmlData)
	if err != nil {
		logger.Error("AirPayService_CollectInquiry_JsonError | err=%v | data=%v", err, string(data))
		return nil, err
	}

	engine, _ := dao.GetMysql()
	payEntry := new(model.PayEntry)
	exist, err := engine.Where("order_id=?", orderId).Get(payEntry)
	if err != nil {
		logger.Error("AirPayService_Collect_Inquiry_GetError | err=%v | data=%v", err, orderId)
		return nil, err
	}
	if !exist {
		return nil, errors.New("NotExist")
	}
	if xmlData.Data.TRANSACTIONSTATUS == "400" || xmlData.Data.TRANSACTIONSTATUS == "401" || xmlData.Data.TRANSACTIONSTATUS == "402" {
		if xmlData.Data.MESSAGE != "Wrong checksum" { //
			payEntry.Status = model.PAYFAILD
			payEntry.ThirdDesc = xmlData.Data.MESSAGE
		}
	} else if xmlData.Data.TRANSACTIONSTATUS == "200" {
		amount, _ := strconv.ParseFloat(xmlData.Data.AMOUNT, 64)
		if int64(payEntry.Amount*100) != int64(amount*100) {
			raw := string(data)
			logger.Error("AirPayService_Collect_Inquiry_OrderAmountError | order=%v|%v | data=%v", orderId, payEntry.Amount, raw)
			msg := fmt.Sprintf("air inquiry order[%v] amount[%v] != %v", payEntry.OrderId, payEntry.Amount, xmlData.Data.AMOUNT)
			common.PushAlarmEvent(common.Warning, "air", "air_inquiry", raw, msg)
			return nil, fmt.Errorf("order[%v] amount[%v] != %v", payEntry.OrderId, payEntry.Amount, xmlData.Data.AMOUNT)
		}

		if xmlData.Data.TRANSACTIONTYPE == "320" { // Sale - 320
			payEntry.Status = model.PAYSUCCESS
		} else if xmlData.Data.TRANSACTIONTYPE == "340" { // Refund - 340
			payEntry.Status = model.PAYFAILD
			payEntry.ThirdDesc = "refunded"
			msg := fmt.Sprintf("air inquiry order[%v]%v refunded", payEntry.OrderId, payEntry.Uid)
			common.PushAlarmEvent(common.Warning, "air", "air_inquiry", msg, msg)
		}
	}
	return payEntry, nil
}

func (a AirPayService) InquiryPayout(orderId string, ext string) (*model.PayOut, error) {
	return nil, fmt.Errorf("no supported")
}

func (a AirPayService) PayOut(frozenId int64, args validator.PayOutValidator) (*model.PayOut, error) {
	return nil, fmt.Errorf("no supported")
}

func (a AirPayService) PayOutBatch(frozenId int64, args validator.PayOutValidator) (*model.PayOut, error) {
	return a.PayOut(frozenId, args)
}

func (a AirPayService) InSignature(cb interface{}) bool {
	hook, ok := cb.(url.Values)
	if !ok {
		return false
	}

	/*
		Secure hash generated by airpay

		If Channel is upi,
		Hash generated by : crc32(TRANSACTIONID. : .APTRANSACTIONID. : .AMOUNT. : .TRANSACTIONSTATUS. : .MESSAGE. : .MID. : .USERNAME. : . CUSTOMERVPA);

		Otherwise,
		Hash generated by : crc32(TRANSACTIONID. : .APTRANSACTIONID. : .AMOUNT. : .TRANSACTIONSTATUS. : .MESSAGE. : .MID. : .USERNAME);
	*/
	source := fmt.Sprintf("%v:%v:%v:%v:%v:%v:%v:%v", hook.Get("TRANSACTIONID"), hook.Get("APTRANSACTIONID"),
		hook.Get("AMOUNT"), hook.Get("TRANSACTIONSTATUS"),
		hook.Get("MESSAGE"), a.MID, a.USERNAME,
		hook.Get("CUSTOMERVPA"))

	hash := hook.Get("ap_SecureHash")
	crcV := crc32.ChecksumIEEE([]byte(source))
	if fmt.Sprintf("%v", crcV) != hash {
		logger.Error("AirPayService_InSignature err %v!=%v", crcV, hash)
		return false
	}

	return true
}

func (a AirPayService) OutSignature(cb interface{}) bool {
	return false
}

func (a AirPayService) Request(url string, method string, contentType string, sendData interface{}, header map[string][]string) (res []byte, err error) {
	client := &http.Client{}
	var req *http.Request
	if contentType == "JSON" {
		data, _ := json.Marshal(sendData)
		logger.Debug("AirPayService_Request %v", string(data))
		reader := bytes.NewBuffer(data)
		req, err = http.NewRequest(method, url, reader)
	} else {
		s := sendData.(string)
		reader := strings.NewReader(s)
		req, err = http.NewRequest(method, url, reader)
	}
	//fmt.Println(url)
	//提交请求
	if err != nil {
		logger.Error("AirPayService_Create_order | url=%v | err=%+v | req=%+v", url, err, sendData)
		return
	}
	req.Header = http.Header(header)
	//处理返回结果
	response, err := client.Do(req)
	if err != nil {
		logger.Error("AirPayService_request_err | url=%v | err=%+v | req=%+v", url, err, sendData)
		return
	}
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		logger.Error("AirPayService_response_read_err | url=%v | err=%+v | req=%+v", url, err, sendData)
		return
	}
	defer func() {
		_ = response.Body.Close()
	}()
	return body, nil
}
