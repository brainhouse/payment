package payu

/**
{
"access_token":
"c6827e119a0bef253b2a94bde346396c038f6b9a8f5b870eee2d0bb8f9fe6536",
"token_type": "Bearer",
"expires_in": 7199,
"refresh_token":
"319e0d809d80d325e8987f4f85cec840b236f6d5fb8cf21aabe14d7d80f42c35",
"scope": "create_payout_transactions",
"created_at": 1557916897,
"user_uuid": "11e8-28fd-15f3852a-8bbf-028edbaa01be"
}
*/
type TokenResponse struct {
	AccessToken string `json:"access_token"`
}

/**
{
"beneficiaryName": "Payu",
"beneficiaryEmail": "payu@payu.in",
"beneficiaryMobile": "9876473627",
"purpose": "transfer request",
"amount": 1234,
"batchId": "1",
"merchantRefId": "654ytv65v",
"paymentType" : "UPI",
"vpa":"komal@yesb"
}
*/
type PaymentRequest struct {
	BeneficiaryName          string  `json:"beneficiaryName"`
	BeneficiaryEmail         string  `json:"beneficiaryEmail"`
	BeneficiaryAccountNumber string  `json:"beneficiaryAccountNumber"`
	BeneficiaryIfscCode      string  `json:"beneficiaryIfscCode"`
	BeneficiaryMobile        string  `json:"beneficiaryMobile"`
	Amount                   float64 `json:"amount"`
	BatchId                  string  `json:"batchId"`
	MerchantRefId            string  `json:"merchantRefId"`
	PaymentType              string  `json:"paymentType"`
	Purpose                  string  `json:"purpose"`
}

type InquiryResponse struct {
	Result []InquiryResult `json:"result"`
}

type InquiryResult struct {
	PostBackParam InquiryPostBackParam `json:"postBackParam"`
}

type InquiryPostBackParam struct {
	Status         string `json:"status"`
	PaymentId      int64  `json:"paymentId"`
	PayuMoneyId    string `json:"payuMoneyId"`
	Field9         string `json:"field9"`
	UnmappedStatus string `json:"unmappedstatus"`
	ErrorMessage   string `json:"error_Message"`
}

type PayoutInquiry struct {
	Data PayoutInquiryData `json:"data"`
}

type PayoutInquiryData struct {
	TransactionDetails []PayoutInquiryDataDetail `json:"transactionDetails"`
}

type PayoutInquiryDataDetail struct {
	TxnId     int64  `json:"txnId"`
	TxnStatus string `json:"txnStatus"`
}
