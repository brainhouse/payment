package payu

import (
	"fmt"
	"funzone_pay/app/validator"
	"funzone_pay/boostrap"
	"funzone_pay/model"
	"regexp"
	"testing"
	"time"
)

func TestGetPayU(t *testing.T) {
	boostrap.InitConf()
	//payEntry, err := GetPayU().CreateOrder(validator.PayEntryValidator{
	//	AppId:      "pubg",
	//	AppOrderId: "test",
	//	PayChannel: model.PAYU,
	//	Amount:     50000,
	//	UserId:     "1",
	//	UserName:   "test",
	//	Email:      "test@cashfree.com",
	//	Phone:      "9000012345",
	//})
	//fmt.Println(payEntry)
	//fmt.Println(err)

	pattern := "^(\\d+)$" //反斜杠要转义
	result, _ := regexp.MatchString(pattern, "")
	fmt.Println(result)
}

func TestPayUService_PayOut(t *testing.T) {
	boostrap.InitConf()

	data, err := GetPayU().PayOut(validator.PayOutValidator{
		AppId:      "rummy",
		AppOrderId: fmt.Sprintf("%v%v", "test", time.Now().Unix()),
		PayType:    model.PT_BANK,
		PayChannel: model.PAYU,
		Amount:     1,
		UserId:     "11",
		UserName:   "Thaneesh V",
		BankCard:   "1769155000016710",
		Phone:      "9999999999",
		Email:      "test@cashfree.com",
		IFSC:       "KVBL0001769",
		Address:    "ABC Street",
		PayTm:      "9999999999",
	})
	fmt.Println(data)
	fmt.Println(err)
}

func TestPayUService_PayOut2(t *testing.T) {
	GetPayU4().SetCallback()
}
