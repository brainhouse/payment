package payu

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"funzone_pay/app/validator"
	"funzone_pay/dao"
	"funzone_pay/model"
	"funzone_pay/utils"
	"github.com/spf13/viper"
	"github.com/wonderivan/logger"
	"io/ioutil"
	"net/http"
	"net/url"
	"time"
)

type PayUService struct {
	PAY_ACCOUNT_ID  string
	PAY_CHANNEL     string
	COUNTRY_CODE    int
	MK              string
	HOST            string
	AUTH_HEADER     string
	MSALT           string
	MTOKEN          string
	PAYOUT_HOST     string
	COLLECT_HOST    string
	PAYOUT_MID      string
	PAYOUT_MK       string
	PAYOUT_ACCOUNT  string
	PAYOUT_USERNAME string
	PAYOUT_PWD      string
}

func GetPayU() *PayUService {
	return &PayUService{
		MK:          viper.GetString("payu.MK"),
		MSALT:       viper.GetString("payu.MSALT"),
		MTOKEN:      viper.GetString("payu.MTOKEN"),
		HOST:        viper.GetString("payu.HOST"),
		AUTH_HEADER: viper.GetString("payu.AUTH_HEADER"),

		PAYOUT_HOST:     viper.GetString("payu.PAYOUT_HOST"),
		PAYOUT_MK:       viper.GetString("payu.PAYOUT_MK"),
		PAYOUT_MID:      viper.GetString("payu.PAYOUT_MID"),
		PAYOUT_ACCOUNT:  viper.GetString("payu.PAYOUT_ACCOUNT"),
		PAYOUT_USERNAME: viper.GetString("payu.PAYOUT_USERNAME"),
		PAYOUT_PWD:      viper.GetString("payu.PAYOUT_PWD"),
	}
}

func GetPayU2() *PayUService {
	return &PayUService{
		MK:     viper.GetString("payu2.MK"),
		MSALT:  viper.GetString("payu2.MSALT"),
		MTOKEN: viper.GetString("payu2.MTOKEN"),
	}
}

func GetPayU3() *PayUService {
	return &PayUService{
		MK:     viper.GetString("payu3.MK"),
		MSALT:  viper.GetString("payu3.MSALT"),
		MTOKEN: viper.GetString("payu3.MTOKEN"),
	}
}

func GetPayU4() *PayUService {
	return &PayUService{
		/**
		PAYOUT_MID = "1111504"
		    PAYOUT_ACCOUNT = "https://accounts.payu.in"
		    PAYOUT_HOST = "https://www.payumoney.com/payout"
		    PAYOUT_MK = "ccbb70745faad9c06092bb5c79bfd919b6f45fd454f34619d83920893e90ae6b"
		    PAYOUT_USERNAME = "danieljoseph1121@gmail.com"
		    PAYOUT_PWD = "Rummybank666%"
		*/
		PAYOUT_HOST:     "https://www.payumoney.com/payout",
		PAYOUT_MK:       "ccbb70745faad9c06092bb5c79bfd919b6f45fd454f34619d83920893e90ae6b",
		PAYOUT_MID:      "1111504",
		PAYOUT_ACCOUNT:  "https://accounts.payu.in",
		PAYOUT_USERNAME: "danieljoseph1121@gmail.com",
		PAYOUT_PWD:      "Rummybank666%",
	}
}

func GetPayUIns(conf map[string]interface{}) *PayUService {
	return &PayUService{
		PAY_ACCOUNT_ID: conf["PAY_ACCOUNT_ID"].(string),
		PAY_CHANNEL:    conf["PAY_CHANNEL"].(string),
		COUNTRY_CODE:   conf["COUNTRY_CODE"].(int),
		MK:             conf["MK"].(string),
		MSALT:          conf["MSALT"].(string),
		MTOKEN:         conf["MTOKEN"].(string),
		HOST:           conf["HOST"].(string),
		AUTH_HEADER:    conf["AUTH_HEADER"].(string),
		COLLECT_HOST:   conf["COLLECT_HOST"].(string),

		PAYOUT_HOST:     conf["PAYOUT_HOST"].(string),
		PAYOUT_MK:       conf["PAYOUT_MK"].(string),
		PAYOUT_MID:      conf["PAYOUT_MID"].(string),
		PAYOUT_ACCOUNT:  conf["PAYOUT_ACCOUNT"].(string),
		PAYOUT_USERNAME: conf["PAYOUT_USERNAME"].(string),
		PAYOUT_PWD:      conf["PAYOUT_PWD"].(string),
	}
}

func (pu *PayUService) CreateOrder(args validator.PayEntryValidator) (*model.PayEntry, error) {
	orderId := "PU" + utils.GetOrderId("IN")
	//key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5||||||salt;
	hashStr := fmt.Sprintf("%s|%s|%v|%v|%v|%v|||||||||||%v", pu.MK, orderId, args.Amount, "CASH_1", "rummy", args.Email, pu.MSALT)

	//请求成功写入流水
	pE := new(model.PayEntry)
	pE.AppId = args.AppId
	pE.Uid = args.Uid
	pE.PayAccountId = pu.PAY_ACCOUNT_ID
	pE.CountryCode = pu.COUNTRY_CODE
	pE.Amount = args.Amount
	pE.UserId = args.UserId
	pE.UserName = args.UserName
	pE.Email = args.Email
	pE.Phone = args.Phone
	pE.PayChannel = model.PAYU
	pE.AppOrderId = args.AppOrderId
	pE.OrderId = orderId
	pE.PaymentOrderId = utils.GetHashSha512(hashStr)
	pE.Status = model.PAYING
	pE.PayAppId = pu.MK
	pE.FinishTime = time.Now().Unix()
	engine, err := dao.GetMysql()
	if engine == nil || err != nil {
		logger.Error("PayUService_CreateOrder_GetDB_Err | err=%v", err)
		return nil, err
	}
	_, err = engine.Insert(pE)
	if err != nil {
		logger.Error("PayUService_CreateOrder_InsertDB_Err | err=%v", err)
		return nil, err
	}
	pE.PaymentLinkHost = pu.COLLECT_HOST
	logger.Debug("PayUService_CreateOrder_Info | data=%+v | hashStr=%v", pE, hashStr)
	return pE, err
}

func (ps *PayUService) CheckSign(dataStr string) bool {
	return ps.MTOKEN == dataStr
}

func (ps *PayUService) InSignature(data interface{}) bool {
	return ps.MTOKEN == data.(string)
}

func (ps *PayUService) OutSignature(data interface{}) bool {
	return true
}

func (pu *PayUService) Inquiry(orderId string, ext string) (*model.PayEntry, error) {
	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("PayUService_Inquiry_Engine_Error | data=%+v | err=%v", orderId, err)
		return nil, err
	}
	requestUrl := fmt.Sprintf("%s/payment/op/getPaymentResponse?merchantKey=%v&merchantTransactionIds=%v", pu.HOST, pu.MK, orderId)
	header := map[string]string{
		"authorization": pu.AUTH_HEADER,
	}
	data, err := pu.Request(requestUrl, "POST", "", nil, header)
	if err != nil {
		logger.Error("PayUService_Inquiry_Request_Error | err=%v", err)
		return nil, err
	}
	logger.Debug("PayUService_Inquiry_Info | orderId=%v | data=%v", err, orderId, string(data))
	responseData := new(InquiryResponse)
	err = json.Unmarshal(data, responseData)
	if err != nil {
		logger.Error("PayUService_Inquiry_JsonError | err=%v | data=%v", err, string(data))
		return nil, err
	}
	payEntry := new(model.PayEntry)
	exist, err := engine.Where("order_id=? and status=0", orderId).ForUpdate().Get(payEntry)
	if err != nil {
		logger.Error("PayUService_Inquiry_GetError | err=%v | data=%v", err, orderId)
		return nil, err
	}
	if !exist {
		return nil, errors.New("NotExist")
	}
	if len(responseData.Result) == 0 && payEntry.Created < time.Now().Unix()-3*3600 {
		payEntry.Status = model.PAYFAILD
	} else if len(responseData.Result) > 0 && responseData.Result[0].PostBackParam.Status == "success" {
		payEntry.Status = model.PAYSUCCESS
		payEntry.PaymentId = responseData.Result[0].PostBackParam.PayuMoneyId
	} else if len(responseData.Result) > 0 && responseData.Result[0].PostBackParam.Status == "failure" {
		payEntry.Status = model.PAYFAILD
		desc := responseData.Result[0].PostBackParam.ErrorMessage
		if desc == "" {
			desc = responseData.Result[0].PostBackParam.Field9
		}
		payEntry.ThirdDesc = desc
	}
	return payEntry, nil
}

func (pu *PayUService) InquiryPayout(orderId string, ext string) (*model.PayOut, error) {
	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("PayUService_InquiryPayout_Engine_Error | data=%+v | err=%v", orderId, err)
		return nil, err
	}
	//获取token
	token, err := pu.AuthToken()
	if token == "" {
		return nil, errors.New("PayUService_InquiryPayout_GetTokenErr | err=%v")
	}
	requestUrl := fmt.Sprintf("%s/payout/payment/listTransactions", pu.HOST)
	header := map[string]string{
		"payoutMerchantId": pu.PAYOUT_MID,
		"Authorization":    "Bearer " + token,
	}
	req := url.Values{}
	req.Set("merchantRefId", orderId)

	data, err := pu.Request(requestUrl, "POST", "application/x-www-form-urlencoded", []byte(req.Encode()), header)
	if err != nil {
		logger.Error("PayUService_InquiryPayout_Request_Error | err=%v", err)
		return nil, err
	}
	logger.Debug("PayUService_InquiryPayout_Info | data=%v | orderId=%v", string(data), orderId)
	responseData := new(PayoutInquiry)
	err = json.Unmarshal(data, responseData)
	if err != nil {
		logger.Error("PayUService_InquiryPayout_JsonError | err=%v | data=%v", err, string(data))
		return nil, err
	}
	payOut := new(model.PayOut)
	exist, err := engine.Where("order_id=?", orderId).Get(payOut)
	if err != nil {
		logger.Error("PayUService_InquiryPayout_GetError | err=%v | data=%v", err, orderId)
		return nil, err
	}
	if !exist {
		return nil, errors.New("NotExist")
	}
	transLen := len(responseData.Data.TransactionDetails)
	if transLen == 0 && payOut.Created < time.Now().Unix()-3600 {
		payOut.Status = model.PAY_OUT_FAILD
	} else if transLen > 0 && responseData.Data.TransactionDetails[0].TxnStatus == "FAILED" {
		payOut.Status = model.PAY_OUT_FAILD
	} else if transLen > 0 && responseData.Data.TransactionDetails[0].TxnStatus == "SUCCESS" {
		payOut.Status = model.PAY_OUT_SUCCESS
		payOut.PaymentId = fmt.Sprint(responseData.Data.TransactionDetails[0].TxnId)
	}
	return payOut, nil
}
func (pu *PayUService) PayOut(frozenId int64, outValidator validator.PayOutValidator) (*model.PayOut, error) {
	// created in contacts
	var beneId string
	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("PayUService_PayOut_Engine_Error | data=%+v | err=%v", outValidator, err)
		return nil, err
	}
	//获取token
	token, err := pu.AuthToken()
	if token == "" {
		return nil, errors.New("PayUService_PayOut_GetTokenErr | err=%v")
	}
	// insert db records
	transId := utils.GetOrderId("PUOUT")
	payOut := new(model.PayOut)
	payOut.Uid = outValidator.Uid
	payOut.AppId = outValidator.AppId
	payOut.PayAccountId = pu.PAY_ACCOUNT_ID
	payOut.PayChannel = model.PAYU
	payOut.CountryCode = pu.COUNTRY_CODE
	payOut.PayType = outValidator.PayType
	payOut.AppOrderId = outValidator.AppOrderId
	payOut.OrderId = transId
	payOut.PaymentId = transId
	payOut.FrozenId = frozenId
	payOut.Amount = outValidator.Amount
	payOut.BankCard = outValidator.BankCard
	payOut.UserId = outValidator.UserId
	payOut.UserName = outValidator.UserName
	payOut.Email = outValidator.Email
	payOut.BeneId = beneId
	payOut.Paytm = outValidator.PayTm
	payOut.IFSC = outValidator.IFSC
	payOut.Phone = outValidator.Phone
	_, err = engine.Insert(payOut)
	if err != nil {
		logger.Error("PayUService_PayOut_Insert_Error | data=%+v | err=%v", payOut, err)
		return nil, err
	}
	reqData := []PaymentRequest{
		{
			BeneficiaryAccountNumber: outValidator.BankCard,
			BeneficiaryName:          outValidator.UserName,
			BeneficiaryIfscCode:      outValidator.IFSC,
			BeneficiaryEmail:         outValidator.Email,
			BeneficiaryMobile:        "",
			Amount:                   outValidator.Amount,
			BatchId:                  "1",
			MerchantRefId:            transId,
			PaymentType:              "IMPS",
			Purpose:                  "withdraw",
		},
	}
	// payouts
	requestUrl := fmt.Sprintf("%s/payment", pu.PAYOUT_HOST)
	header := map[string]string{
		"payoutMerchantId": pu.PAYOUT_MID,
		"Authorization":    "Bearer " + token,
	}
	data, err := pu.Request(requestUrl, "POST", "application/json", reqData, header)
	if err != nil {
		logger.Error("PayUService_PayOut_Request_Error | err=%v", err)
		return payOut, err
	}
	logger.Debug("PayUService_PayOut_Info | err=%v | data=%v | reqData=%v | header=%v", err, string(data), reqData, header)
	res := &struct {
		Status int    `json:"status"`
		Msg    string `json:"msg"`
		Error  string `json:"error"`
	}{}
	err = json.Unmarshal(data, res)
	if err != nil {
		logger.Error("PayUService_PayOut_JsonUnmarshal_Error | err=%v | res=%v", err, res)
		return payOut, err
	}
	var msg string
	if res.Msg == "" {
		msg = res.Msg
	} else {
		msg = res.Error
	}
	_, err = engine.Where("id=?", payOut.Id).Update(&model.PayOut{
		Status:     model.PAY_OUT_ING,
		ThirdCode:  fmt.Sprint(res.Status),
		ThirdDesc:  msg,
		FinishTime: time.Now().Unix(),
	})
	if err != nil {
		logger.Error("PayUService_PayOut_Update_Error | data=%+v | err=%v", payOut, err)
		return payOut, err
	}
	return payOut, nil
}

func (pu *PayUService) PayOutBatch(frozenId int64, outValidator validator.PayOutValidator) (payout *model.PayOut, err error) {
	payout, err = pu.PayOut(frozenId, outValidator)
	return
}

func (pu *PayUService) AuthToken() (token string, err error) {
	reqUrl := fmt.Sprintf("%s/oauth/token", pu.PAYOUT_ACCOUNT)
	uri := url.Values{}
	uri.Set("client_id", pu.PAYOUT_MK)
	uri.Set("grant_type", "password")
	uri.Set("username", pu.PAYOUT_USERNAME)
	uri.Set("password", pu.PAYOUT_PWD)
	uri.Set("scope", "create_payout_transactions")
	resData, err := pu.Request(reqUrl, "POST", "application/x-www-form-urlencoded", []byte(uri.Encode()), map[string]string{})
	if err != nil {
		logger.Error("PayUService_AuthToken_Err | err=%v", err)
		return "", err
	}
	tokenData := new(TokenResponse)
	err = json.Unmarshal(resData, tokenData)
	if err != nil {
		return "", err
	}
	if tokenData.AccessToken == "" {
		logger.Error("PayUService_AuthToken_Error | data=%v", string(resData))
	} else {
		logger.Debug("PayUService_AuthToken_Info | err=%v | data=%v", err, string(resData))
	}
	return tokenData.AccessToken, nil
}

func (pu *PayUService) SetCallback() {
	reqUrl := fmt.Sprintf("%s/saveWebhook", pu.PAYOUT_HOST)
	req := &struct {
		Default string `json:"default"`
	}{
		Default: "https://payment.rummybank.com/callback/payu_payout",
	}
	//获取token
	token, err := pu.AuthToken()
	if token == "" {
		return
	}
	header := map[string]string{
		"payoutMerchantId": pu.PAYOUT_MID,
		"Authorization":    "Bearer " + token,
	}
	fmt.Println(reqUrl)
	fmt.Println(pu.PAYOUT_MID)
	fmt.Println(token)
	fmt.Println(reqUrl)
	resData, err := pu.Request(reqUrl, "POST", "application/json", req, header)
	if err != nil {
		fmt.Println(err)
		logger.Error("PayUService_AuthToken_Err | err=%v", err)
		return
	}
	fmt.Println(string(resData))
}

/*
*
统一请求
*/
func (pu *PayUService) Request(url string, method string, contentType string, sendData interface{}, hds map[string]string) (res []byte, err error) {
	client := &http.Client{}
	var data []byte
	if contentType == "application/json" {
		data, _ = json.Marshal(sendData)
	} else if sendData != nil {
		data = sendData.([]byte)
	}
	//提交请求
	req, err := http.NewRequest(method, url, bytes.NewBuffer(data))
	if err != nil {
		logger.Error("PayUService_create_order | err=%+v | req=%+v", err, sendData)
		return
	}
	header := map[string][]string{
		"Content-Type": {contentType},
	}
	req.Header = http.Header(header)
	for k, v := range hds {
		req.Header.Set(k, v)
	}
	//处理返回结果
	response, err := client.Do(req)
	if err != nil {
		logger.Error("PayUService_request_err | err=%+v | req=%+v", err, sendData)
		return
	}
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		logger.Error("PayUService_response_read_err | err=%+v | req=%+v", err, sendData)
		return
	}
	defer func() {
		_ = response.Body.Close()
	}()
	return body, nil
}
