package eaglepay

/**
{
   "success": true,
   "data": {
     "accessToken": "0512822dbcd8d2341d5e",
     "expireDate": 1601697258000
   },
   "code":null,
   "message":null
 }
*/
type TokenResponse struct {
	Data struct {
		AccessToken string `json:"accessToken"`
		ExpireDate  int64  `json:"expireDate"`
	} `json:"data"`
	Code    interface{} `json:"code"`
	Message interface{} `json:"message"`
	Success bool        `json:"success"`
}

/**
{
    "success": true,
    "data": {
      "paySerialNumber": "0b3c7ee88c0e4db488cd6d7c60df4a37",
      "amount":918.0,
      "orderId": "order_FXEHiIZy6Wc0hl",
      "paymentLink":"https://rzp.io/i/o9GegfdB",
      "payChannel":"razorpay"
    },
    "message":null,
    "code":null
  }
*/
type OrderInResponse struct {
	Data struct {
		PaySerialNumber string  `json:"paySerialNumber"`
		Amount          float64 `json:"amount"`
		OrderId         string  `json:"orderId"`
		PaymentLink     string  `json:"paymentLink"`
		PayChannel      string  `json:"payChannel"`
	} `json:"data"`
	Code    interface{} `json:"code"`
	Message interface{} `json:"message"`
	Success bool        `json:"success"`
}
