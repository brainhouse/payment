package eaglepay

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"funzone_pay/app/validator"
	"funzone_pay/dao"
	"funzone_pay/model"
	"funzone_pay/utils"
	"github.com/wonderivan/logger"
	"io/ioutil"
	"net/http"
	"strings"
	"time"
)

type EaglePayService struct {
	Host             string
	APPID            string
	APPKEY           string
	PAY_ACCOUNT_ID   string
	PAY_CHANNEL      string
	COUNTRY_CODE     int
	APPSK            string
	RETRUN_URL       string
	COLLECT_HOST     string
	NOTIFY_URL       string
	PAY_OUT_URL      string
	PAY_OUT_CLIENTID string
	PAY_OUT_CLIENTSK string
}

func GetEaglePayIns(conf map[string]interface{}) *EaglePayService {
	return &EaglePayService{
		PAY_ACCOUNT_ID:   conf["PAY_ACCOUNT_ID"].(string),
		PAY_CHANNEL:      conf["PAY_CHANNEL"].(string),
		COUNTRY_CODE:     conf["COUNTRY_CODE"].(int),
		Host:             conf["HOST"].(string),
		APPID:            conf["APPID"].(string),
		APPKEY:           conf["APPKEY"].(string),
		APPSK:            conf["APPSK"].(string),
		RETRUN_URL:       conf["RETURN_URL"].(string),
		NOTIFY_URL:       conf["NOTIFY_URL"].(string),
		COLLECT_HOST:     conf["COLLECT_HOST"].(string),
		PAY_OUT_URL:      conf["PAY_OUT_URL"].(string),
		PAY_OUT_CLIENTID: conf["PAY_OUT_CLIENTID"].(string),
		PAY_OUT_CLIENTSK: conf["PAY_OUT_CLIENTSK"].(string),
	}
}

func (e *EaglePayService) GetToken() string {
	requestUrl := fmt.Sprintf("%s%s", e.Host, "/auth/token/apply")
	header := map[string][]string{
		"ContentType": {"application/x-www-form-urlencoded"},
	}
	reqData := fmt.Sprintf("corpId=%v&appKey=%v&secretKey=%v", e.APPID, e.APPKEY, e.APPSK)
	data, err := e.Request(requestUrl, "POST", "", reqData, header)
	if err != nil {
		logger.Error("EaglePayService_GetToken_Request_Error | err=%v", err)
		return ""
	}
	logger.Debug("CashFreeService_GetToken_Info | err=%v | res=%v | req=%v", err, string(data), header)
	res := new(TokenResponse)
	err = json.Unmarshal(data, res)
	if err != nil {
		logger.Error("EaglePayService_GetToken_JsonUnmarshal_Error | err=%v | res=%v", err, res)
		return ""
	}
	return res.Data.AccessToken
}

func (e *EaglePayService) CreateOrder(args validator.PayEntryValidator) (*model.PayEntry, error) {
	accToken := e.GetToken()
	if accToken == "" {
		return nil, errors.New("INTERNAL_TOKEN_ERROR")
	}
	orderId := utils.GetOrderId("EGIN")
	requestUrl := fmt.Sprintf("%s%s", e.Host, "/payment/rest/order/apply")

	rData := fmt.Sprintf("paySerialNumber=%v&amount=%v&productName=GameChips&returnUrl=%v", orderId, args.Amount, e.RETRUN_URL)
	header := map[string][]string{
		"accessToken": {accToken},
	}
	data, err := e.Request(requestUrl, "POST", "", rData, header)
	logger.Debug("EaglePayService_CreateOrder_Info | orderReq=%+v | err=%v", rData, err)

	egResponse := &OrderInResponse{}
	err = json.Unmarshal(data, egResponse)
	if err != nil {
		logger.Error("EaglePayService_CreateOrder_JsonUnmarshal_Err | err=%v", err)
		return nil, err
	}
	//请求成功写入流水
	pE := new(model.PayEntry)
	pE.AppId = args.AppId
	pE.Uid = args.Uid
	pE.PayAccountId = e.PAY_ACCOUNT_ID
	pE.PayChannel = model.CASHFREE
	pE.CountryCode = e.COUNTRY_CODE
	pE.AppOrderId = args.AppOrderId
	pE.OrderId = orderId
	pE.Amount = args.Amount
	pE.UserId = args.UserId
	pE.UserName = args.UserName
	pE.Email = args.Email
	pE.Phone = args.Phone
	pE.ThirdCode = fmt.Sprint(egResponse.Code)
	pE.Status = model.PAYING
	pE.PayAppId = e.APPID
	pE.FinishTime = time.Now().Unix()
	if !egResponse.Success {
		pE.ThirdDesc = fmt.Sprint(egResponse.Message)
	}
	engine, err := dao.GetMysql()
	if engine == nil || err != nil {
		logger.Error("CashFreeService_CreateOrder_GetDB_Err | err=%v", err)
		return nil, err
	}
	_, err = engine.Insert(pE)
	if err != nil {
		logger.Error("CashFreeService_CreateOrder_InsertDB_Err | err=%v", err)
		return nil, err
	}
	logger.Debug("CashFreeService_CreateOrder_Request | req=%v | res=%v", rData, egResponse)
	if !egResponse.Success {
		err := errors.New(fmt.Sprint(egResponse.Message))
		return nil, err
	}
	pE.PaymentLinkHost = egResponse.Data.PaymentLink
	return pE, err
}

func (e *EaglePayService) Inquiry(s string, s2 string) (*model.PayEntry, error) {
	panic("implement me")
}

func (e *EaglePayService) InquiryPayout(orderId string, ext string) (*model.PayOut, error) {
	panic("implement me")
}

func (e *EaglePayService) PayOut(frozenId int64, outValidator validator.PayOutValidator) (payOut *model.PayOut, err error) {
	//Authorization := e.GetToken()
	//if Authorization == "" {
	//	err = errors.New("GetTokenError")
	//	logger.Error("EaglePayService_PayOut_GetToken_Empty | data=%+v", outValidator)
	//	return nil, err
	//}
	////创建受益人
	//var beneId string
	//engine, err := dao.GetMysql()
	//if err != nil {
	//	logger.Error("CashFreeService_PayOut_Engine_Error | data=%+v | err=%v", payOut, err)
	//	return nil, err
	//}
	//if outValidator.PayType == model.PT_BANK {
	//	//检查银行卡账号是否已经注册了
	//	session := engine.Where("bank_card=? AND pay_channel=? and status=1", outValidator.BankCard, model.CASHFREE)
	//	if cf.PAY_ACCOUNT_ID != "" {
	//		session.Where("pay_account_id=?", cf.PAY_ACCOUNT_ID)
	//	}
	//	payHis := new(model.PayOut)
	//	exist, err := session.OrderBy("id desc").Get(payHis)
	//	if err != nil {
	//		logger.Error("CashFreeService_PayOut_BankCard_Engine_Error | data=%+v | err=%v", payOut, err)
	//		return nil, err
	//	}
	//	if exist && payHis.BeneId != "" {
	//		beneId = payHis.BeneId
	//	}
	//} else if outValidator.PayType == model.PT_UPI {
	//	session := engine.Where("vpa=? AND pay_channel=? and status=1", outValidator.VPA, model.CASHFREE)
	//	if cf.PAY_ACCOUNT_ID != "" {
	//		session.Where("pay_account_id=?", cf.PAY_ACCOUNT_ID)
	//	}
	//	payHis := new(model.PayOut)
	//	exist, err := session.OrderBy("id desc").Get(payHis)
	//	if err != nil {
	//		logger.Error("CashFreeService_PayOut_VPA_Engine_Error | data=%+v | err=%v", payOut, err)
	//		return nil, err
	//	}
	//	if exist && payHis.BeneId != "" {
	//		beneId = payHis.BeneId
	//	}
	//}
	//transId := utils.GetOrderId("EGOUT")
	//payOut = new(model.PayOut)
	//payOut.Uid = outValidator.Uid
	//payOut.AppId = outValidator.AppId
	//payOut.PayAccountId = cf.PAY_ACCOUNT_ID
	//payOut.PayChannel = model.CASHFREE
	//payOut.PayType = outValidator.PayType
	//payOut.AppOrderId = outValidator.AppOrderId
	//payOut.OrderId = transId
	//payOut.PaymentId = transId
	//payOut.Amount = outValidator.Amount
	//payOut.BankCard = outValidator.BankCard
	//payOut.UserId = outValidator.UserId
	//payOut.UserName = outValidator.UserName
	//payOut.Email = outValidator.Email
	//payOut.BeneId = beneId
	//payOut.Paytm = outValidator.PayTm
	//payOut.IFSC = outValidator.IFSC
	//payOut.Phone = outValidator.Phone
	//_, err = engine.Insert(payOut)
	//if err != nil {
	//	logger.Error("CashFreeService_PayOut_Insert_Error | data=%+v | err=%v", payOut, err)
	//	return nil, err
	//}
	//if beneId == "" {
	//	var (
	//		benRes bool
	//		msg    string
	//	)
	//	benRes, beneId, msg = cf.AddBeneficiary(outValidator, Authorization)
	//	if !benRes {
	//		err = errors.New("AddBeneficiaryError")
	//		logger.Error("CashFreeService_PayOut_AddBeneficiaryError | data=%+v", outValidator)
	//		_, _ = engine.Where("order_id=?", transId).Update(&model.PayOut{
	//			ThirdDesc: msg,
	//			Status:    model.PAY_OUT_FAILD,
	//		})
	//		payOut.Status = model.PAY_OUT_FAILD
	//		return payOut, err
	//	}
	//}
	////发起支付
	//requestUrl := fmt.Sprintf("%s%s", cf.PAY_OUT_URL, "/payout/v1/requestTransfer")
	//header := map[string][]string{
	//	"Authorization": {fmt.Sprintf("Bearer %v", Authorization)},
	//}
	///**
	//{
	//  "beneId": "JOHN18011",
	//  "amount": "100.00",
	//  "transferId": "DEC2016"
	//}
	//*/
	//transferMode := string(outValidator.PayType)
	//if transferMode == "bank" {
	//	transferMode = "banktransfer" //做下转换 banktransfer, upi, paytm, amazonpay and card.
	//} else if transferMode == "upi" {
	//	transferMode = "upi"
	//}
	//reqData := &struct {
	//	BeneId       string `json:"beneId"`
	//	Amount       string `json:"amount"`
	//	TransferId   string `json:"transferId"`
	//	TransferMode string `json:"transferMode"`
	//}{
	//	BeneId:       beneId,
	//	Amount:       fmt.Sprint(outValidator.Amount),
	//	TransferId:   transId,
	//	TransferMode: transferMode,
	//}
	//
	//data, err := cf.Request(requestUrl, "POST", "JSON", reqData, header)
	//if err != nil {
	//	logger.Error("CashFreeService_PayOut_Request_Error | err=%v", err)
	//	return nil, err
	//}
	//rdata, _ := json.Marshal(reqData)
	//logger.Debug("CashFreeService_PayOut_Info | requestUrl=%v | requestData=%v | response=%v", requestUrl, string(rdata), string(data))
	///**
	//{
	//  "status": "SUCCESS",
	//  "subCode": "200",
	//  "message": "Transfer completed successfully",
	//  "data": {
	//    "referenceId": "10023",
	//    "utr": "P16111765023806",
	//    "acknowledged": 1
	//  }
	//}
	//*/
	//res := &struct {
	//	Status  string `json:"status"`
	//	SubCode string `json:"subCode"`
	//	Message string `json:"message"`
	//	Data    struct {
	//		ReferenceId  string `json:"referenceId"`
	//		Utr          string `json:"utr"`
	//		Acknowledged int    `json:"acknowledged"`
	//	} `json:"data"`
	//}{}
	//err = json.Unmarshal(data, res)
	//if err != nil {
	//	logger.Error("CashFreeService_PayOut_JsonUnmarshal_Error | err=%v | res=%v", err, res)
	//	return nil, err
	//}
	//var (
	//	status      model.PayOutStatus
	//	thirdDesc   string
	//	referenceId string
	//	utr         string
	//)
	//payOut.ThirdCode = res.SubCode
	//if res.Status == "ERROR" {
	//	//先不设置成失败
	//	//status = model.PAY_OUT_FAILD
	//	thirdDesc = res.Message
	//} else {
	//	status = model.PAY_OUT_ING
	//	referenceId = res.Data.ReferenceId
	//	utr = res.Data.Utr
	//}
	//_, err = engine.Where("id=?", payOut.Id).Update(&model.PayOut{
	//	Status:    status,
	//	BeneId:    beneId,
	//	ThirdCode: res.SubCode,
	//	PaymentId: referenceId,
	//	Utr:       utr,
	//	ThirdDesc: thirdDesc,
	//})
	//if err != nil {
	//	logger.Error("CashFreeService_PayOut_Insert_Error | data=%v | err=%v", payOut, err)
	//	return nil, err
	//}
	return payOut, nil
}

func (e *EaglePayService) PayOutBatch(frozenId int64, entryValidator validator.PayOutValidator) (*model.PayOut, error) {
	return nil, fmt.Errorf("not implement")
}

func (e *EaglePayService) InSignature(cb interface{}) bool {
	return false
}

func (e *EaglePayService) OutSignature(cb interface{}) bool {
	return false
}

/*
*
统一请求
*/
func (e *EaglePayService) Request(url string, method string, contentType string, sendData interface{}, header map[string][]string) (res []byte, err error) {
	client := &http.Client{}
	var req *http.Request
	if contentType == "JSON" {
		data, _ := json.Marshal(sendData)
		reader := bytes.NewBuffer(data)
		req, err = http.NewRequest(method, url, reader)
	} else {
		s := sendData.(string)
		reader := strings.NewReader(s)
		req, err = http.NewRequest(method, url, reader)
	}
	//提交请求
	if err != nil {
		logger.Error("EaglePayService_Create_order | url=%v | err=%+v | req=%+v", url, err, sendData)
		return
	}
	req.Header = http.Header(header)
	//处理返回结果
	response, err := client.Do(req)
	if err != nil {
		logger.Error("EaglePayService_request_err | url=%v | err=%+v | req=%+v", url, err, sendData)
		return
	}
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		logger.Error("EaglePayService_response_read_err | url=%v | err=%+v | req=%+v", url, err, sendData)
		return
	}
	defer func() {
		_ = response.Body.Close()
	}()
	return body, nil
}
