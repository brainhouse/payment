package opay

import (
	"bytes"
	"crypto"
	"encoding/json"
	"errors"
	"fmt"
	"funzone_pay/app/validator"
	"funzone_pay/centerlog"
	"funzone_pay/common"
	"funzone_pay/dao"
	"funzone_pay/model"
	"funzone_pay/utils"
	"github.com/wonderivan/logger"
	"io/ioutil"
	"net/http"
	"net/url"
	"regexp"
	"strings"
	"time"
)

const (
	RespCodeSuccess = "0000" // 请求成功
	RespCodeFail    = "9999"

	PayInStatusFail       = 0 // 失败
	PayIntStatusSuccess   = 1 // 成功
	PayInStatusPending    = 2 // 待支付
	PayInStatusProcessing = 3 // 支付中
	PayInStatusWaitSearch = 4 // 已退款
	PayInStatusRevert     = 5 // 待创建

	PayoutStatusFail       = 0 // 失败
	PayoutStatusSuccess    = 1 // 成功
	PayoutStatusPending    = 2 // 待支付
	PayoutStatusProcessing = 3 // 支付中
	PayoutStatusWaitSearch = 4 // 已经支付，等待查询
	PayoutStatusRevert     = 5 // 资金反转
)

type OEPayService struct {
	HOST           string
	APPID          string
	PAY_ACCOUNT_ID string
	PAY_CHANNEL    string
	COUNTRY_CODE   int
	SK             string
	CODE           string
}

func GetOEPayIns(conf map[string]interface{}) *OEPayService {
	return &OEPayService{
		HOST:           conf["HOST"].(string),
		PAY_ACCOUNT_ID: conf["PAY_ACCOUNT_ID"].(string),
		PAY_CHANNEL:    conf["PAY_CHANNEL"].(string),
		COUNTRY_CODE:   conf["COUNTRY_CODE"].(int),
		SK:             conf["SK"].(string),
		CODE:           conf["CODE"].(string),
	}
}

func (oe OEPayService) CreateOrder(args validator.PayEntryValidator) (*model.PayEntry, error) {
	orderId := utils.GetOrderId("OIN")

	//amount=221.00&email=abc%40gmail.com&firstname=abc&furl=https%3A%2F%2www.google.com&mobile=9999999999&orderNo=AAA123456&remark=abc&surl=https%3A%2F%2www.google.com

	amount := fmt.Sprintf("%.2f", args.Amount)

	email := url.QueryEscape(args.Email)
	surl := url.QueryEscape("https://pay.rummybuffett.com/#/callback_?type=1")
	furl := url.QueryEscape("https://pay.rummybuffett.com/#/callback_?type=2")

	signStr := fmt.Sprintf("amount=%v&email=%v&firstname=rummy&furl=%v&mobile=%v&orderNo=%v&remark=charge&surl=%v", amount, email, furl, args.Phone, orderId, surl)
	sign, _ := utils.RsaSignEncrypt(oe.SK, []byte(signStr), crypto.SHA512)
	header := http.Header{}
	header.Add("Content-Type", "application/json")
	header.Add("X-SIGN", sign)
	header.Add("X-SERVICE-CODE", oe.CODE)

	sendData := &struct {
		OrderNo   string `json:"orderNo"`
		Amount    string `json:"amount"`
		Firstname string `json:"firstname"`
		Mobile    string `json:"mobile"`
		Email     string `json:"email"`
		Surl      string `json:"surl"`
		Furl      string `json:"furl"`
		Remark    string `json:"remark"`
	}{
		OrderNo:   orderId,
		Amount:    amount,
		Firstname: "rummy",
		Mobile:    args.Phone,
		Email:     args.Email,
		Surl:      "https://pay.rummybuffett.com/#/callback_?type=1",
		Furl:      "https://pay.rummybuffett.com/#/callback_?type=2",
		Remark:    "charge",
	}
	res, err := oe.Request(fmt.Sprintf("%v/gold-pay/portal/createH5PayLink", oe.HOST), http.MethodPost, "JSON", sendData, header)
	if err != nil {
		logger.Error("OEPayService_CreateOrder_Err req=%v | err=%v", sendData, err)
		return nil, err
	}
	logger.Debug("OEPayService_CreateOrder | req=%v | res=%v ", sendData, string(res))
	/**
	{
	    "code": "0000 成功",
	    "msg": "",
	    "data": {
	        "link Url": "",
	        "status": "1",
	        "orderNo": "",
	        "platformOrderNo": ""
	    }
	}
	*/
	resData := &struct {
		Code string `json:"code"`
		Msg  string `json:"msg"`
		Data struct {
			LinkURL         string `json:"linkUrl"`
			Status          int    `json:"status"`
			OrderNo         string `json:"orderNo"`
			PlatformOrderNo string `json:"platformOrderNo"`
		} `json:"data"`
	}{}
	err = json.Unmarshal(res, resData)
	if err != nil {
		logger.Error("OEPayService_CreateOrder_JsonUnmarshal_Error | err=%v | res=%v", err, res)
		return nil, err
	}
	if resData.Code != "0000" {
		logger.Error("OEPayService_CreateOrder_resp | signStr=%v,sign:%v", signStr, sign)
		return nil, errors.New(resData.Msg)
	}

	//请求成功写入流水
	pE := new(model.PayEntry)
	pE.AppId = args.AppId
	pE.Uid = args.Uid
	pE.PayAccountId = oe.PAY_ACCOUNT_ID
	pE.PayChannel = model.OPAY
	pE.CountryCode = oe.COUNTRY_CODE
	pE.AppOrderId = args.AppOrderId
	pE.OrderId = orderId
	pE.Amount = args.Amount
	pE.UserId = args.UserId
	pE.UserName = args.UserName
	pE.Email = args.Email
	pE.Phone = args.Phone
	pE.Status = model.PAYING
	pE.FinishTime = time.Now().Unix()

	engine, err := dao.GetMysql()
	if engine == nil || err != nil {
		logger.Error("OEPayService_CreateOrder_GetDB_Err | err=%v", err)
		return nil, err
	}
	_, err = engine.Insert(pE)
	if err != nil {
		logger.Error("OEPayService_CreateOrder_InsertDB_Err | err=%v", err)
		return nil, err
	}
	pE.PaymentLinkHost = resData.Data.LinkURL
	return pE, err
}

func (OEPayService) Inquiry(string, string) (*model.PayEntry, error) {
	return nil, fmt.Errorf("implement me")
}

func (oe OEPayService) InquiryPayout(orderId string, ext string) (*model.PayOut, error) {
	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("OEPayService_InquiryPayout_Engine_Error | data=%+v | err=%v", orderId, err)
		return nil, err
	}
	payOut := new(model.PayOut)
	exist, err := engine.Where("order_id=?", orderId).Get(payOut)
	if err != nil {
		logger.Error("OEPayService_InquiryPayout_GetError | err=%v | data=%v", err, orderId)
		return nil, err
	}
	if !exist {
		return nil, errors.New("NotExist")
	}
	requestUrl := fmt.Sprintf("%s/gold-pay/portal/queryTransferOrderStatus", oe.HOST)

	signStr := fmt.Sprintf(`orderNo=%v`, orderId)
	sign, _ := utils.RsaSignEncrypt(oe.SK, []byte(signStr), crypto.SHA512)

	header := http.Header{}
	header.Add("Content-Type", "application/json")
	header.Add("X-SIGN", sign)
	header.Add("X-SERVICE-CODE", oe.CODE)
	reqData := &struct {
		OrderNo string `json:"orderNo"`
	}{
		OrderNo: orderId,
	}
	data, err := oe.Request(requestUrl, "POST", "JSON", reqData, header)
	if err != nil {
		logger.Error("OEPayService_InquiryPayout_Request_Error | err=%v", err)
		return nil, err
	}
	logger.Debug("OEPayService_InquiryPayout_Info | data=%v | orderId=%v | req=%v", string(data), orderId, reqData)
	responseData := &struct {
		Code string `json:"code"`
		Msg  string `json:"msg"`
		Data struct {
			Status json.Number `json:"status"`
		} `json:"data"`
	}{}

	err = json.Unmarshal(data, responseData)
	if err != nil {
		logger.Error("OEPayService_InquiryPayout_JsonError | err=%v | data=%v", err, string(data))
		return nil, err
	}
	if responseData.Code == "30005" || (responseData.Code != "0000" && responseData.Data.Status.String() == "0") {
		payOut.Status = model.PAY_OUT_FAILD
		payOut.ThirdDesc = responseData.Msg
	} else if responseData.Data.Status.String() == "1" {
		payOut.Status = model.PAY_OUT_SUCCESS
	}
	return payOut, nil
}

func (oe *OEPayService) PayOut(frozenId int64, args validator.PayOutValidator) (payOut *model.PayOut, err error) {
	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("OEPayServicePayOut_Engine_Error | data=%+v | err=%v", payOut, err)
		return nil, err
	}

	transId := utils.GetOrderId("0T")
	payOut = new(model.PayOut)
	payOut.Uid = args.Uid
	payOut.AppId = args.AppId
	payOut.PayAccountId = oe.PAY_ACCOUNT_ID
	payOut.PayChannel = model.OPAY
	payOut.CountryCode = oe.COUNTRY_CODE
	payOut.PayType = args.PayType
	payOut.AppOrderId = args.AppOrderId
	payOut.OrderId = transId
	payOut.FrozenId = frozenId
	payOut.Amount = args.Amount
	payOut.BankCard = args.BankCard
	payOut.UserId = args.UserId
	payOut.UserName = args.UserName
	payOut.Email = args.Email
	payOut.Paytm = args.PayTm
	payOut.IFSC = args.IFSC
	payOut.Phone = args.Phone
	_, err = engine.Insert(payOut)

	if err != nil {
		logger.Error("OEPayServicePayOut_PayOut_Insert_Error | data=%+v | err=%v", payOut, err)
		return nil, err
	}
	//amount=221.00&bankAccount=abc%40gmail.com&beneficiaryEmail=&beneficiaryName=abc&beneficiaryMobile=https%3A%2F%2www.google.com&mobile=9999999999&orderNo=AAA123456&remark=abc&surl=https%3A%2F%2www.google.com
	amount := fmt.Sprintf("%.2f", args.Amount)

	userName := args.UserName
	reg, err := regexp.Compile("[^a-zA-Z0-9]+")
	if err == nil {
		userName = reg.ReplaceAllString(userName, "")
	}
	email := url.QueryEscape(args.Email)
	timeMark := time.Now().Unix()
	mark := fmt.Sprintf("%v:%v", timeMark, common.PublicIP)
	var signStr string
	var sendData interface{}
	if args.PayType == model.PT_BANK {
		signStr = fmt.Sprintf("amount=%v&bankAccount=%v&beneficiaryEmail=%v&beneficiaryMobile=%v&beneficiaryName=%v&ifsc=%v&orderNo=%v&paymentType=0&remark=withdraw", amount, args.BankCard, email, args.Phone, userName, args.IFSC, transId)
	} else {
		return nil, err
	}
	sendData = &struct {
		OrderNo           string `json:"orderNo"`
		Amount            string `json:"amount"`
		PaymentType       string `json:"paymentType"`
		BeneficiaryName   string `json:"beneficiaryName"`
		BeneficiaryMobile string `json:"beneficiaryMobile"`
		BankAccount       string `json:"bankAccount"`
		Ifsc              string `json:"ifsc"`
		BeneficiaryEmail  string `json:"beneficiaryEmail"`
		Remark            string `json:"remark"`
	}{
		OrderNo:           transId,
		Amount:            amount,
		PaymentType:       "0",
		BeneficiaryName:   userName,
		BeneficiaryMobile: args.Phone,
		BankAccount:       args.BankCard,
		Ifsc:              args.IFSC,
		BeneficiaryEmail:  args.Email,
		Remark:            fmt.Sprintf("w:%v", utils.XXTeaEnc([]byte(mark))),
	}
	sign, _ := utils.RsaSignEncrypt(oe.SK, []byte(signStr), crypto.SHA512)

	header := http.Header{}
	header.Add("Content-Type", "application/json")
	header.Add("X-SIGN", sign)
	header.Add("X-SERVICE-CODE", oe.CODE)

	data, err := oe.Request(fmt.Sprintf("%v/gold-pay/portal/transfer", oe.HOST), http.MethodPost, "JSON", sendData, header)
	logger.Debug("OEPayService_Payout | req=%v | res=%v | err=%v", sendData, string(data), err)
	if err != nil {
		return nil, err
	}

	res := &struct {
		Code    string `json:"code"`
		Message string `json:"msg"`
		Data    struct {
			PlatformOrderNo string `json:"platformOrderNo"`
			Status          int    `json:"status"`
		} `json:"data"`
	}{}
	res.Data.Status = 1 // 展位
	err = json.Unmarshal(data, res)
	if err != nil {
		logger.Error("OEPayService_PayOut_JsonUnmarshal_Error | err=%v | res=%v", err, string(data))
		return nil, err
	}
	var (
		status      model.PayOutStatus
		thirdDesc   string
		referenceId string
	)
	if res.Data.Status == PayoutStatusFail {
		status = model.PAY_OUT_FAILD
		payOut.Status = model.PAY_OUT_FAILD
		thirdDesc = res.Message
		centerlog.OrderError(centerlog.MsgThirdPlatformFail, payOut.PayAccountId, payOut.AppOrderId, payOut.AppId, payOut.OrderId, fmt.Sprint(res.Message), res.Message)
	} else {
		status = model.PAY_OUT_ING
		referenceId = res.Data.PlatformOrderNo
	}
	_, err = engine.Where("id=?", payOut.Id).Update(&model.PayOut{
		Status:     status,
		OrderId:    transId,
		ThirdCode:  res.Message,
		PaymentId:  referenceId,
		ThirdDesc:  thirdDesc,
		FinishTime: time.Now().Unix(),
	})
	if err != nil {
		logger.Error("OEPayService_PayOut_Insert_Error | data=%v | err=%v", payOut, err)
		return nil, err
	}
	return payOut, nil

}

func (oe OEPayService) PayOutBatch(frozenId int64, args validator.PayOutValidator) (*model.PayOut, error) {
	return oe.PayOut(frozenId, args)
}

func (OEPayService) InSignature(cb interface{}) bool {
	panic("implement me")
}

func (OEPayService) OutSignature(cb interface{}) bool {
	panic("implement me")
}

func (oe *OEPayService) Request(url string, method string, contentType string, sendData interface{}, header map[string][]string) (res []byte, err error) {
	client := &http.Client{}
	var req *http.Request
	if contentType == "JSON" {
		data, _ := json.Marshal(sendData)
		fmt.Println(string(data))
		reader := bytes.NewBuffer(data)
		req, err = http.NewRequest(method, url, reader)
	} else {
		s := sendData.(string)
		reader := strings.NewReader(s)
		req, err = http.NewRequest(method, url, reader)
	}
	fmt.Println(url)
	//提交请求
	if err != nil {
		logger.Error("BHTPayService_Create_order | url=%v | err=%+v | req=%+v", url, err, sendData)
		return
	}
	req.Header = http.Header(header)
	//处理返回结果
	response, err := client.Do(req)
	if err != nil {
		logger.Error("BHTPayService_request_err | url=%v | err=%+v | req=%+v", url, err, sendData)
		return
	}
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		logger.Error("BHTPayService_response_read_err | url=%v | err=%+v | req=%+v", url, err, sendData)
		return
	}
	defer func() {
		_ = response.Body.Close()
	}()
	return body, nil
}
