package clickncash

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"funzone_pay/app/validator"
	"funzone_pay/dao"
	"funzone_pay/model"
	"funzone_pay/utils"
	"github.com/wonderivan/logger"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
	"time"
)

type ClicknCashService struct {
	PAY_ACCOUNT_ID string
	PAY_CHANNEL    string
	HOST           string
	KEY            string
	TOKEN          string
	NOTIFY_URL     string
	COLLECT_HOST   string
	COUNTRY_CODE   int
}

func GetClickNCashPayIns(conf map[string]interface{}) *ClicknCashService {
	return &ClicknCashService{
		PAY_ACCOUNT_ID: conf["PAY_ACCOUNT_ID"].(string),
		PAY_CHANNEL:    conf["PAY_CHANNEL"].(string),
		HOST:           conf["HOST"].(string),
		KEY:            conf["KEY"].(string),
		TOKEN:          conf["TOKEN"].(string),
		NOTIFY_URL:     conf["NOTIFY_URL"].(string),
		COUNTRY_CODE:   conf["COUNTRY_CODE"].(int),
		COLLECT_HOST:   conf["COLLECT_HOST"].(string),
	}
}

func (a ClicknCashService) CreateOrder(args validator.PayEntryValidator) (*model.PayEntry, error) {
	orderId := utils.GetOrderId("CCIN")
	requestUrl := fmt.Sprintf("%s%s", a.HOST, "/app/api/hitPgRequest")

	/**
	UserInfo:2190 (DEFAULT PASS 2190)
	Amount : amount
	Mode:UPI
	EmailId:Email ID
	Mobile:Mobile Number
	Callback: Your Callback Url
	OrderId:Unique Txn ID
	user_id:API USER ID
	token: API USER TOKEN

	user_id: Your api user_id
	token: Your api user token
	txnid: Your txn_id
	email: Your email
	firstname: Your First Name
	lastname: Your Last Name
	callbackurl: callback_url
	mobile: your mobile no.
	amount:10
	mode:upi
	*/
	params := url.Values{}
	params.Add("user_id", a.KEY)
	params.Add("token", a.TOKEN)
	params.Add("OrderId", orderId)
	params.Add("EmailId", args.Email)
	params.Add("Callback", a.NOTIFY_URL)
	params.Add("Mobile", args.Phone)
	params.Add("UserInfo", "2190")
	params.Add("Amount", fmt.Sprintf("%.2f", args.Amount))
	params.Add("Mode", "upi")

	header := map[string][]string{
		"Content-Type": {"application/x-www-form-urlencoded"},
	}
	data, err := a.Request(requestUrl, "POST", "", params.Encode(), header)
	logger.Debug("ClicknCashService_CreateOrder_Info | orderReq=%v | err=%v | data=%v", params.Encode(), err, string(data))
	//{"status":"false","msg":"error","error":["The Mobile field must be at least 10 characters in length."]}
	res := struct {
		Status string   `json:"status"`
		Error  []string `json:"error"`
	}{}
	err = json.Unmarshal(data, &res)
	logger.Debug("ClicknCashService_CreateOrder_Info_Unmarshall | err=%v | data=%v", err, string(data))
	if err == nil {
		return nil, errors.New(res.Error[0])
	}
	//将html收银台代码 base64返回
	resData := base64.StdEncoding.EncodeToString(data)
	//请求成功写入流水
	pE := new(model.PayEntry)
	pE.AppId = args.AppId
	pE.Uid = args.Uid
	pE.PayAccountId = a.PAY_ACCOUNT_ID
	pE.PayChannel = model.CLICKNCASH
	pE.CountryCode = a.COUNTRY_CODE
	pE.AppOrderId = args.AppOrderId
	pE.OrderId = orderId
	pE.Amount = args.Amount
	pE.UserId = args.UserId
	pE.UserName = args.UserName
	pE.Email = args.Email
	pE.Phone = args.Phone
	pE.Status = model.PAYING
	pE.FinishTime = time.Now().Unix()

	engine, err := dao.GetMysql()
	if engine == nil || err != nil {
		logger.Error("ClicknCashService_CreateOrder_GetDB_Err | err=%v", err)
		return nil, err
	}
	_, err = engine.Insert(pE)
	if err != nil {
		logger.Error("ClicknCashService_CreateOrder_InsertDB_Err | err=%v", err)
		return nil, err
	}
	pE.PaymentLinkHost = a.COLLECT_HOST
	pE.SignatureNew = resData
	return pE, err
}

func (a ClicknCashService) Inquiry(orderId string, s2 string) (*model.PayEntry, error) {
	return nil, nil
}

func (a ClicknCashService) InquiryPayout(orderId string, ext string) (*model.PayOut, error) {

	return nil, nil
}

func (a ClicknCashService) PayOut(frozenId int64, args validator.PayOutValidator) (*model.PayOut, error) {
	return nil, fmt.Errorf("implement")
}

func (a ClicknCashService) PayOutBatch(frozenId int64, args validator.PayOutValidator) (*model.PayOut, error) {
	return a.PayOut(frozenId, args)
}

func (Y ClicknCashService) InSignature(cb interface{}) bool {
	panic("implement me")
}

func (Y ClicknCashService) OutSignature(cb interface{}) bool {
	panic("implement me")
}

func (a ClicknCashService) Request(url string, method string, contentType string, sendData interface{}, header map[string][]string) (res []byte, err error) {
	client := &http.Client{}
	var req *http.Request
	if contentType == "JSON" {
		data, _ := json.Marshal(sendData)
		fmt.Println(string(data))
		reader := bytes.NewBuffer(data)
		req, err = http.NewRequest(method, url, reader)
	} else {
		s := sendData.(string)
		reader := strings.NewReader(s)
		req, err = http.NewRequest(method, url, reader)
	}
	//提交请求
	if err != nil {
		logger.Error("ClicknCashService_Create_order | url=%v | err=%+v | req=%+v", url, err, sendData)
		return
	}
	req.Header = header
	//处理返回结果
	response, err := client.Do(req)
	if err != nil {
		logger.Error("ClicknCashService_request_err | url=%v | err=%+v | req=%+v", url, err, sendData)
		return
	}
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		logger.Error("ClicknCashService_response_read_err | url=%v | err=%+v | req=%+v", url, err, sendData)
		return
	}
	defer func() {
		_ = response.Body.Close()
	}()
	return body, nil
}
