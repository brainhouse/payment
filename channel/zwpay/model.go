package zwpay

type PayInResponse struct {
	Code int    `json:"code"`
	Msg  string `json:"msg"`
	Data struct {
		OrderNo        string `json:"orderNo"`
		Status         string `json:"status"`
		PaymentLinkUrl string `json:"paymentLinkUrl"`
		Message        string `json:"message"`
	} `json:"data"`
}

type PayInQueryResponse struct {
	Code int    `json:"code"`
	Msg  string `json:"msg"`
	Data struct {
		OrderNo         string  `json:"orderNo"`
		MerchantOrderNo string  `json:"merchantOrderNo"`
		Status          string  `json:"status"`
		Amount          float64 `json:"amount"`
		TransferMode    string  `json:"transferMode"`
		Message         string  `json:"message"`
		ExtraInfo       string  `json:"extraInfo"`
	} `json:"data"`
}

type PayInNotify struct {
	Code       int    `json:"code"`
	Msg        string `json:"msg"`
	Signature  string `json:"signature"`
	SignatureN string `json:"signature_n"`
	Data       struct {
		OrderNo         string `json:"orderNo"`
		MerchantOrderNo string `json:"merchantOrderNo"`
		Status          string `json:"status"`
		Amount          string `json:"amount"`
		TransferMode    string `json:"transferMode"`
		ProcessedTime   string `json:"processedTime"`
		Message         string `json:"message"`
		ExtraInfo       string `json:"extraInfo"`
		Type            string `json:"type"`
	} `json:"data"`
}

type PayOutResponse struct {
	Code int    `json:"code"`
	Msg  string `json:"msg"`
	Data struct {
		OrderNo string  `json:"orderNo"` // 流水号
		Status  string  `json:"status"`
		Amount  float64 `json:"amount"`
		Message string  `json:"message"` // 失败原因
	} `json:"data"`
}

type PayOutQueryResponse struct {
	Code int    `json:"code"`
	Msg  string `json:"msg"`
	Data struct {
		OrderNo         string `json:"orderNo"`
		MerchantOrderNo string `json:"merchantOrderNo"`
		ReferenceId     string `json:"referenceId"`
		Status          string `json:"status"`
		Amount          string `json:"amount"`
		ProcessAmount   string `json:"processAmount"`
		Message         string `json:"message"`
		Utr             string `json:"utr"`
		ExtraInfo       string `json:"extraInfo"`
	} `json:"data"`
}

type PayOutNotify struct {
	Code       int    `json:"code"`
	Msg        string `json:"msg"`
	Signature  string `json:"signature"`
	SignatureN string `json:"signature_n"`
	Data       struct {
		OrderNo         string `json:"orderNo"`
		MerchantOrderNo string `json:"merchantOrderNo"`
		Status          string `json:"status"`
		Amount          string `json:"amount"`
		ProcessAmount   string `json:"processAmount"`
		ProcessedTime   string `json:"processedTime"`
		Message         string `json:"message"`
		ExtraInfo       string `json:"extraInfo"`
		Type            string `json:"type"`
	} `json:"data"`
}
