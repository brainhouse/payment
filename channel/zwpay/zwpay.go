package zwpay

import (
	"bytes"
	"crypto/md5"
	"encoding/json"
	"errors"
	"fmt"
	"funzone_pay/app/validator"
	"funzone_pay/centerlog"
	"funzone_pay/common"
	"funzone_pay/dao"
	"funzone_pay/model"
	"funzone_pay/utils"
	"github.com/wonderivan/logger"
	"io/ioutil"
	"net/http"
	"strings"
	"time"
)

const (
	FieldAmount          = "amount"
	FieldMerchantOrderNo = "merchantOrderNo"
	FieldCustomerName    = "customerName"
	FieldCustomerEmail   = "customerEmail"
	FieldCustomerPhone   = "customerPhone"
	FieldNotifyUrl       = "notifyUrl"
	FieldChannel         = "channel"
	FieldExtraInfo       = "extraInfo"

	FieldTransferType       = "transferType"
	FieldTransferMode       = "transferMode"
	FieldBeneficiaryName    = "beneficiaryName"
	FieldBeneficiaryEmail   = "beneficiaryEmail"
	FieldBeneficiaryPhoneNo = "beneficiaryPhoneNo"
	FieldBeneficiaryAccount = "beneficiaryAccount"
	FieldBeneficiaryIFSC    = "beneficiaryIFSC"
	FieldBeneficiaryVPA     = "beneficiaryVPA"
)

const (
	ZWCodeSuccess = 0

	ZWPayOutAccept  = "ACCEPT"   // 接受交易
	ZWPayOutSuccess = "SUCCESS"  // 交易成功
	ZWPayOutPending = "PENDING"  //
	ZWPayOutFailure = "FAILURE"  //
	ZWPayOutReverse = "REVERSED" // 交易回退

	ZWPayInSuccess = "SUCCESS"
	ZWPayInPending = "PENDING"
	ZWPayInFailure = "FAILURE"
	ZWPayInRefund  = "REFUND" // 退款
)

type ZWPayService struct {
	PAY_ACCOUNT_ID string
	PAY_CHANNEL    string
	COUNTRY_CODE   int

	MERCHANT_CH  string // zw的支付方式:upi,paytm,payu,cashfree,razorpay
	MERCHANT_KEY string //
	AES_KEY      []byte //
	AES_IV       []byte //

	IN_NOTIFY_URL  string // 异步支付、提现结果通知
	OUT_NOTIFY_URL string
	HOST_URL       string // 支付、提现的请求地址
}

func GetZWPayIns(conf map[string]interface{}) *ZWPayService {
	mch, ok := conf["MERCHANT_CH"].(string)
	if !ok {
		msg := fmt.Sprintf("zwpay mch[%v] value type error", conf["MERCHANT_CH"])
		raw, _ := json.Marshal(conf)
		common.PushAlarmEvent(common.Warning, "zwpay", "GetZWPayIns", string(raw), msg)
		mch = ""
	}
	return &ZWPayService{
		PAY_ACCOUNT_ID: conf["PAY_ACCOUNT_ID"].(string),
		PAY_CHANNEL:    conf["PAY_CHANNEL"].(string),
		COUNTRY_CODE:   conf["COUNTRY_CODE"].(int),

		MERCHANT_CH:  mch,
		MERCHANT_KEY: conf["MERCHANT_KEY"].(string),
		AES_KEY:      []byte(conf["AES_KEY"].(string)),
		AES_IV:       []byte(conf["AES_IV"].(string)),

		IN_NOTIFY_URL:  conf["IN_NOTIFY_URL"].(string),
		OUT_NOTIFY_URL: conf["OUT_NOTIFY_URL"].(string),
		HOST_URL:       conf["HOST_URL"].(string),
	}
}

func (u *ZWPayService) Header() http.Header {
	return http.Header{
		"Content-Type": {"application/json; charset=utf-8"},
		"merchant_key": {u.MERCHANT_KEY},
	}
}

func (u *ZWPayService) CreateOrder(args validator.PayEntryValidator) (*model.PayEntry, error) {
	var err error = nil
	isAlarm := true
	defer func() {
		if isAlarm {
			common.PushAlarmEvent(common.Warning, fmt.Sprintf("%v", args.Uid), "ZWPayService/CreateOrder", "", fmt.Sprintf("%v-%v Create Order By zwpay: %v", args.AppId, args.Uid, err))
		}
	}()

	orderId := utils.GetOrderId("ZIN")
	reqData, err := u.genPayInReqData(orderId, args)
	if err != nil {
		logger.Error("ZWPayService_CreateOrder_Info | reqData=%v | %v| err=%v", string(reqData), args.AppOrderId, err)
		return nil, err
	}

	header := u.Header()
	encData, err := utils.AESEncryptWithBase64(reqData, u.AES_KEY, u.AES_IV)
	if err != nil {
		logger.Error("ZWPayService_CreateOrder_Info | reqData=%v | %v| err=%v ", string(reqData), args.AppOrderId, err)
		return nil, fmt.Errorf("enc error")
	}

	data, err := u.Request(u.HOST_URL+"/payment/init", "POST", "JSON", map[string]string{"data": encData}, header)
	logger.Debug("ZWPayService_CreateOrder_Info | reqData=%v | %v | %v | %v | err=%v | response=%v", string(reqData), header, encData, args.AppOrderId, err, string(data))
	if err != nil {
		return nil, fmt.Errorf("create order error")
	}

	response := &PayInResponse{}
	err = json.Unmarshal(data, response)
	if err != nil {
		logger.Error("ZWPayService_CreateOrder_JsonUnmarshal_Err | %v | err=%v | data=%v", args.AppOrderId, err, string(data))
		centerlog.OrderError(centerlog.MsgThirdPlatformFail, u.PAY_ACCOUNT_ID, args.AppId, args.AppOrderId, orderId, "false", err.Error())
		return nil, err
	}

	//
	if response.Code != ZWCodeSuccess { //
		err = fmt.Errorf("%v-%v", response.Code, response.Msg)
		return nil, fmt.Errorf("service busy, invalid code %v", args.AppOrderId)
	}

	//请求成功写入流水
	pE := new(model.PayEntry)
	pE.AppId = args.AppId
	pE.Uid = args.Uid
	pE.PayAccountId = u.PAY_ACCOUNT_ID
	pE.PayChannel = model.ZWPAY
	pE.CountryCode = u.COUNTRY_CODE
	pE.AppOrderId = args.AppOrderId
	pE.OrderId = orderId
	pE.PaymentOrderId = ""
	pE.Amount = args.Amount
	pE.UserId = args.UserId
	pE.UserName = args.UserName
	pE.Email = args.Email
	pE.Phone = args.Phone
	pE.Status = model.PAYING
	pE.Created = time.Now().Unix()

	//
	engine, err := dao.GetMysql()
	if engine == nil || err != nil {
		logger.Error("ZWPayService_CreateOrder_GetDB_Err | err=%v", err)
		return nil, err
	}
	_, err = engine.Insert(pE)
	if err != nil {
		logger.Error("ZWPayService_CreateOrder_InsertDB_Err | err=%v", err)
		return nil, err
	}

	isAlarm = false
	pE.PaymentLinkHost = response.Data.PaymentLinkUrl
	return pE, nil
}

func (u *ZWPayService) Inquiry(orderId string, s2 string) (*model.PayEntry, error) {
	header := u.Header()
	reqData, err := json.Marshal(map[string]string{FieldMerchantOrderNo: orderId})
	if err != nil {
		return nil, err
	}

	encData, err := utils.AESEncryptWithBase64(reqData, u.AES_KEY, u.AES_IV)
	if err != nil {
		logger.Error("ZWPayService_Inquiry_Error | reqData=%v | err=%v ", string(reqData), err)
		return nil, fmt.Errorf("enc error")
	}

	resp, err := u.Request(u.HOST_URL+"/payment/orderQuery", "POST", "JSON", map[string]string{"data": encData}, header)
	logger.Debug("ZWPayService_Inquiry_Info | requestData=%v | response=%v", orderId, string(resp))
	if err != nil {
		logger.Error("ZWPayService_Inquiry_Request_Error | err=%v", err)
		return nil, err
	}

	res := &PayInQueryResponse{}
	err = json.Unmarshal(resp, res)
	if err != nil {
		logger.Error("ZWPayService_Inquiry_JsonUnmarshal_Error | err=%v | res=%v", err, res)
		return nil, err
	}

	if res.Code != ZWCodeSuccess && res.Code != 2002 { // 2002 订单不存在,需要修改订单为失败{
		logger.Error("ZWPayService_Inquiry_GetError | err=%v | data=%v", err, orderId)
		return nil, fmt.Errorf("quiry[%v] error", orderId)
	}

	engine, _ := dao.GetMysql()
	payIn := new(model.PayEntry)
	exist, err := engine.Where("order_id=?", orderId).Get(payIn)
	if err != nil {
		logger.Error("ZWPayService_Inquiry_GetError | err=%v | data=%v", err, orderId)
		return nil, err
	}
	if !exist {
		return nil, errors.New("NotExist")
	}

	status := res.Data.Status
	if status == ZWPayInSuccess {
		payIn.Status = model.PAYSUCCESS
		payIn.PaymentId = res.Data.OrderNo
	} else if status == ZWPayInFailure || status == ZWPayInRefund {
		payIn.Status = model.PAYFAILD
		payIn.ThirdCode = res.Data.Status
		payIn.ThirdDesc = res.Data.Message
	} else if res.Code == 2002 {
		payIn.Status = model.PAYFAILD
		payIn.ThirdCode = fmt.Sprintf("%d", res.Code)
		payIn.ThirdDesc = res.Msg
	}

	return payIn, nil
}

func (u *ZWPayService) InquiryPayout(orderId string, ext string) (*model.PayOut, error) {
	header := u.Header()
	reqData, err := json.Marshal(map[string]string{FieldMerchantOrderNo: orderId})
	if err != nil {
		return nil, err
	}

	encData, err := utils.AESEncryptWithBase64(reqData, u.AES_KEY, u.AES_IV)
	if err != nil {
		logger.Error("ZWPayService_InquiryPayout_Error | reqData=%v | err=%v ", string(reqData), err)
		return nil, fmt.Errorf("enc error")
	}

	resp, err := u.Request(u.HOST_URL+"/payout/orderQuery", "POST", "JSON", map[string]string{"data": encData}, header)
	logger.Debug("ZWPayService_InquiryPayout_Info | requestData=%v | response=%v", orderId, string(resp))
	if err != nil {
		logger.Error("ZWPayService_InquiryPayout_Request_Error | err=%v", err)
		return nil, err
	}

	res := &PayOutQueryResponse{}
	err = json.Unmarshal(resp, res)
	if err != nil {
		logger.Error("ZWPayService_InquiryPayout_JsonUnmarshal_Error | err=%v | res=%v", err, res)
		return nil, err
	}

	if res.Code != ZWCodeSuccess && res.Code != 2002 { // 2002 订单不存在,需要修改订单为失败
		logger.Error("ZWPayService_InquiryPayout_GetError | err=%v | data=%v", err, orderId)
		return nil, fmt.Errorf("quiry[%v] error", orderId)
	}

	engine, _ := dao.GetMysql()
	payOut := new(model.PayOut)
	exist, err := engine.Where("order_id=?", orderId).Get(payOut)
	if err != nil {
		logger.Error("ZWPayService_PayOut_Inquiry_GetError | err=%v | data=%v", err, orderId)
		return nil, err
	}
	if !exist {
		return nil, errors.New("NotExist")
	}

	status := res.Data.Status
	if status == ZWPayOutSuccess {
		payOut.Status = model.PAY_OUT_SUCCESS
		payOut.PaymentId = res.Data.OrderNo
	} else if status == ZWPayOutFailure || status == ZWPayOutReverse {
		payOut.Status = model.PAY_OUT_FAILD
		payOut.ThirdCode = res.Data.Status
		payOut.ThirdDesc = res.Data.Message
	} else if res.Code == 2002 {
		payOut.Status = model.PAY_OUT_FAILD
		payOut.ThirdCode = fmt.Sprintf("%d", res.Code)
		payOut.ThirdDesc = res.Msg
	}

	return payOut, nil
}

func (u *ZWPayService) PayOut(frozenId int64, args validator.PayOutValidator) (payOut *model.PayOut, err error) {
	orderId := utils.GetOrderId("ZOUT")

	payOut = new(model.PayOut)
	payOut.Uid = args.Uid
	payOut.AppId = args.AppId
	payOut.PayAccountId = u.PAY_ACCOUNT_ID
	payOut.PayChannel = model.ZWPAY
	payOut.CountryCode = u.COUNTRY_CODE
	payOut.PayType = args.PayType
	payOut.AppOrderId = args.AppOrderId
	payOut.OrderId = orderId
	payOut.PaymentId = orderId
	payOut.FrozenId = frozenId
	payOut.Amount = args.Amount
	payOut.BankCard = args.BankCard
	payOut.UserId = args.UserId
	payOut.UserName = args.UserName
	payOut.Email = args.Email
	payOut.Paytm = args.PayTm
	payOut.IFSC = args.IFSC
	payOut.Phone = args.Phone
	engine, err := dao.GetMysql()
	if engine == nil || err != nil {
		logger.Error("ZWPayService_PayOut_GetDB_Err | err=%v", err)
		return nil, err
	}
	_, err = engine.Insert(payOut)
	if err != nil {
		logger.Error("ZWPayService_PayOut_Insert_Error | data=%+v | err=%v", payOut, err)
		return nil, err
	}

	reqData, err := u.genPayOutReqData(orderId, args)
	if err != nil {
		return nil, err
	}

	encReqData, err := utils.AESEncryptWithBase64(reqData, u.AES_KEY, u.AES_IV)
	if err != nil {
		logger.Debug("ZWPayService_PayOut_AES_Error | reqData=%v | %v| err=%v ", string(reqData), args.AppOrderId, err)
		return nil, fmt.Errorf("")
	}

	// 发起
	head := u.Header()
	data, err := u.Request(u.HOST_URL+"/payout/init", "POST", "JSON", map[string]string{"data": encReqData}, head)
	logger.Debug("ZWPayService_PayOut_Info | reqData=%v | %v | %v | %v | err=%v | response=%v", string(reqData), head, encReqData, args.AppOrderId, err, string(data))
	if err != nil {
		return nil, fmt.Errorf("payout order error")
	}

	response := &PayOutResponse{}
	err = json.Unmarshal(data, response)
	if err != nil {
		logger.Error("ZWPayService_PayOut_JsonUnmarshal_Err | %v | err=%v | data=%v", args.AppOrderId, err, string(data))
		centerlog.OrderError(centerlog.MsgThirdPlatformFail, u.PAY_ACCOUNT_ID, args.AppId, args.AppOrderId, orderId, "false", err.Error())
		return nil, err
	}

	code := response.Data.Status
	status := model.PAY_OUT_ING
	thirdCode := payOut.ThirdCode
	thirdDesc := payOut.ThirdDesc
	if response.Code != ZWCodeSuccess || code == ZWPayOutFailure || code == ZWPayOutReverse {
		status = model.PAY_OUT_FAILD
		thirdCode = code
		thirdDesc = response.Data.Message
		logger.Error("ZWPayService_PayOut_Status_Err | %v | %v-%v", args.AppOrderId, response.Code, response.Msg)
		centerlog.OrderError(centerlog.MsgThirdPlatformFail, u.PAY_ACCOUNT_ID, payOut.AppId, payOut.AppOrderId, payOut.OrderId, response.Data.Status, fmt.Sprintf("code:%v", response.Code))
	}

	paymentId := response.Data.OrderNo
	_, err = engine.Where("id=?", payOut.Id).Update(&model.PayOut{
		Status:    status,
		PaymentId: paymentId,
		ThirdCode: thirdCode,
		ThirdDesc: thirdDesc,
	})
	if err != nil {
		logger.Error("ZWPayService_PayOut_Update_Error | data=%v | err=%v", payOut, err)
		return nil, err
	}

	payOut.Status = status
	payOut.PaymentId = paymentId
	payOut.ThirdDesc = thirdDesc
	payOut.ThirdCode = thirdCode

	return payOut, nil
}

func (u *ZWPayService) PayOutBatch(frozenId int64, outValidator validator.PayOutValidator) (*model.PayOut, error) {
	return u.PayOut(frozenId, outValidator)
}

func (u *ZWPayService) InSignature(cb interface{}) bool {
	data, ok := cb.(PayInNotify)
	if !ok {
		return false
	}

	return verify(u.MERCHANT_KEY, data.Data.Message, data.Data.Amount, data.Data.Status, data.Data.MerchantOrderNo, data.Data.OrderNo, string(u.AES_KEY), data.SignatureN)
}

func (u *ZWPayService) OutSignature(cb interface{}) bool {
	data, ok := cb.(PayOutNotify)
	if !ok {
		return false
	}

	return verify(u.MERCHANT_KEY, data.Data.Message, data.Data.Amount, data.Data.Status, data.Data.MerchantOrderNo, data.Data.OrderNo, string(u.AES_KEY), data.SignatureN)
}

func verify(merchantKey string, message string, amount string, status string, merchantOrderNo string, orderNo string, aesKey string, sign string) bool {
	val := fmt.Sprintf("%v%v%v%v%v%v%v", merchantKey, message, amount, status, merchantOrderNo, orderNo, aesKey)
	str := fmt.Sprintf("%x", md5.Sum([]byte(val)))

	return str == sign
}

func (u *ZWPayService) genPayInReqData(orderId string, args validator.PayEntryValidator) ([]byte, error) {
	amount := fmt.Sprintf("%.2f", args.Amount) // 两位小数

	data := map[string]string{
		FieldAmount:          amount,
		FieldMerchantOrderNo: orderId,
		FieldCustomerName:    orderId,
		FieldCustomerEmail:   args.Email,
		FieldCustomerPhone:   args.Phone,
		FieldNotifyUrl:       u.IN_NOTIFY_URL,
		FieldChannel:         u.MERCHANT_CH,
		FieldExtraInfo:       u.PAY_ACCOUNT_ID,
	}

	return json.Marshal(data)
}

func (u *ZWPayService) genPayOutReqData(orderId string, args validator.PayOutValidator) ([]byte, error) {
	amount := fmt.Sprintf("%.2f", args.Amount) // 两位小数

	transferType := "BANK_TRANSFER"
	if args.PayType == model.PT_UPI {
		transferType = "UPI"
	}

	data := map[string]string{
		FieldAmount:          amount,
		FieldMerchantOrderNo: orderId,
		FieldTransferType:    transferType,
		//FieldTransferMode: "", //支付类型为 BANK_TRANSFER 时: //IMPS、NEFT、RTGS,默认为 IMPS
		FieldBeneficiaryName:    args.UserName,
		FieldBeneficiaryEmail:   args.Email,
		FieldBeneficiaryPhoneNo: args.Phone,
		FieldBeneficiaryAccount: args.BankCard,
		FieldBeneficiaryIFSC:    args.IFSC,
		FieldBeneficiaryVPA:     args.VPA,
		FieldNotifyUrl:          u.OUT_NOTIFY_URL,
		FieldExtraInfo:          u.PAY_ACCOUNT_ID,
	}

	return json.Marshal(data)
}

func (u *ZWPayService) Request(url string, method string, contentType string, sendData interface{}, header map[string][]string) (res []byte, err error) {
	client := &http.Client{}
	var req *http.Request
	if contentType == "JSON" {
		data, _ := json.Marshal(sendData)
		fmt.Println(string(data))
		reader := bytes.NewBuffer(data)
		req, err = http.NewRequest(method, url, reader)
	} else {
		s := sendData.(string)
		reader := strings.NewReader(s)
		req, err = http.NewRequest(method, url, reader)
	}
	//提交请求
	if err != nil {
		logger.Error("ZWPayService_Create_order | url=%v | err=%+v | req=%+v", url, err, sendData)
		return
	}

	req.Header = http.Header(header)

	//处理返回结果
	response, err := client.Do(req)
	if err != nil {
		logger.Error("ZWPayService_request_err | url=%v | err=%+v | req=%+v", url, err, sendData)
		return
	}
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		logger.Error("ZWPayService_response_read_err | url=%v | err=%+v | req=%+v", url, err, sendData)
		return
	}
	defer func() {
		_ = response.Body.Close()
	}()
	return body, nil
}
