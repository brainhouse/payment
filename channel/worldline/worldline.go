package worldline

import (
	"crypto/sha512"
	"encoding/json"
	"fmt"
	"funzone_pay/app/validator"
	"funzone_pay/common"
	"funzone_pay/dao"
	"funzone_pay/model"
	"funzone_pay/utils"
	"github.com/wonderivan/logger"
	"net/http"
	"net/url"
	"strings"
	"time"
)

const CurrencyCode = "INR"
const RequestType = "T"
const BankCode = "470"
const RqstKitVrsn = "1.0.1"

const Success = "0300"
const Failure = "0399"
const Pending = "0398" // Corporate Transaction Initiated
const Awaited = "0396"
const Aborted = "0392"

func IsSuccess(status string) bool {
	return status == Success
}

func IsFailure(status string) bool {
	return status == Failure || status == Aborted
}

func IsPending(status string) bool {
	return status == Pending || status == Awaited
}

type WDLineService struct {
	PAY_ACCOUNT_ID string
	PAY_CHANNEL    string
	COUNTRY_CODE   int

	MerchantCode    string
	KEY             string
	IV              string
	SALT            string
	//SchemeCode      string
	//ShowAllResponse string

	InHost      string
	InReturnUrl string
}

func GetWDLIns(conf map[string]interface{}) *WDLineService {
	return &WDLineService{
		PAY_ACCOUNT_ID: conf["PAY_ACCOUNT_ID"].(string),
		PAY_CHANNEL:    conf["PAY_CHANNEL"].(string),
		COUNTRY_CODE:   conf["COUNTRY_CODE"].(int),

		MerchantCode:    conf["MERCHANT_CODE"].(string),
		KEY:             conf["KEY"].(string),
		IV:              conf["IV"].(string),
		SALT:            conf["SALT"].(string),
		//SchemeCode:      conf["SCHEME_CODE"].(string),
		//ShowAllResponse: conf["SHOW_ALL_RESPONSE"].(string),

		InHost:      conf["IN_HOST"].(string),
		InReturnUrl: conf["IN_RETURN_URL"].(string),
	}
}

func (u WDLineService) CreateOrder(args validator.PayEntryValidator) (*model.PayEntry, error) {
	orderId := utils.GetOrderId("WIN")

	unix := time.Now().Unix()
	isAlarm := false
	defer func() {
		cost :=  time.Now().Unix() - unix
		if isAlarm || cost > 15 {
			common.PushAlarmEvent(common.Warning, fmt.Sprintf("%v", args.Uid), "WDLPayService/CreateOrder", "", fmt.Sprintf("%v-%v-%v Create Order By AriPay Error, cost:%v", args.AppId, args.Uid, orderId, cost))
		}
	}()

	amount := fmt.Sprintf("%.2f", args.Amount)
	userId := fmt.Sprintf("%v", args.UserId)

	token := genWebToken(u.MerchantCode, orderId, amount, userId, args.Phone, args.Email, u.SALT)

	//merchantId=T874610&amount=1.5&consumerId=1001&consumerMobileNo=8506234781&consumerEmailId=8506234781@gmail.com&txnId=1687771760611
	//&token=f3d7798b981afa511a18d5fdfdbffb58b50c0ca8a12eff382585b1f86c69e8a4bfc1898a7e6d90d220bed105c3fbbb8cf96f2b7fa9a7e2d96f852a045a1ee2a6
	//&returnUrl=https://test-pay-callback.rummyjosh.com/callback/cashpay_callback
	params := url.Values{}
	params.Add("mid", u.MerchantCode) // merchantId
	params.Add("am", amount) // amount
	params.Add("cid", userId) // consumerId
	params.Add("mo", args.Phone) // consumerMobileNo
	params.Add("em", args.Email) // consumerEmailId
	params.Add("txn", orderId) // txnId
	params.Add("token", token)
	params.Add("returnUrl", u.InReturnUrl)

	//请求成功写入流水
	pE := new(model.PayEntry)
	pE.AppId = args.AppId
	pE.Uid = args.Uid
	pE.PayAccountId = u.PAY_ACCOUNT_ID
	pE.PayChannel = model.WDLPAY
	pE.CountryCode = u.COUNTRY_CODE
	pE.AppOrderId = args.AppOrderId
	pE.OrderId = orderId
	pE.PaymentOrderId = ""
	pE.Amount = args.Amount
	pE.UserId = args.UserId
	pE.UserName = args.UserName
	pE.Email = args.Email
	pE.Phone = args.Phone
	pE.Status = model.PAYING
	pE.Created = time.Now().Unix()
	pE.PaymentId = ""

	//
	engine, err := dao.GetMysql()
	if engine == nil || err != nil {
		logger.Error("WDLPayService_CreateOrder_GetDB_Err | err=%v", err)
		isAlarm = true
		return nil, err
	}
	_, err = engine.Insert(pE)
	if err != nil {
		logger.Error("WDLPayService_CreateOrder_InsertDB_Err | err=%v", err)
		isAlarm = true
		return nil, err
	}

	pE.PaymentLinkHost = fmt.Sprintf("%v?%v", u.InHost, params.Encode())
	logger.Debug("WDLPayService_CreateOrder orderId=%v,appOrder=%v,link=%v", orderId, args.AppOrderId, pE.PaymentLinkHost)

	return pE, nil
}

func genWebToken(merchantId, orderId, amount, userId, userMobile, userEmail, saltKey string) string {
	//consumerData.merchantId|consumerData.txnId|totalamount|consumerData.accountNo|consumerData.consumerId|consumerData.consumerMobileNo|consumerData.consumerEmailId|
	//consumerData.debitStartDate|consumerData.debitEndDate|consumerData.maxAmount|consumerData.amountType|consumerData.frequency|consumerData.cardNumber|consumerData.expMonth|
	//consumerData.expYear|consumerData.cvvCode|SALT [Salt will be given by Worldline]

	source := fmt.Sprintf("%v|%v|%v|%v|%v|%v|%v|%v|%v|%v|%v|%v|%v|%v|%v|%v|%v", merchantId, orderId, amount, "", userId, userMobile, userEmail, "", "", "", "", "", "", "", "", "", saltKey)
	return websh2([]byte(source))
}

func getTransactionToken(orderId, amount, transactionDate string) {
	/*
		{
		    "reqType":"T",
		    "mrctCode":"T874610",
		    "mrctTxtID":"916215",
		    "currencyType":"INR",
		    "amount":"1.00",
		    "itc":"email:demo@demo.com",
		    "reqDetail":"FIRST_1.0_0.0",
		    "txnDate":"2023-06-24",
		    "bankCode":"470",
		    "locatorURL":"https:\/\/www.tpsl-india.in\/PaymentGateway\/TransactionDetailsNew.wsdl",
		    "tpsl_txn_id":"",
		    "custID":"19872627",
		    "custname":"test",
		    "mobile":"",
		    "accNo":"",
		    "returnURL":"http:\/\/test-pay-callback.rummyjosh.com\/callback\/cashpay_callback",
		    "submit":"Submit"
		}


		Dear Team,

		Greetings from Ingenico ePayments India Private Limited.!!!

		Details of Test ID issued to Mindgeek Digital OPC Private Limited for Test environment are as below.

		Merchant Code	Scheme Code	Encryption Key	Encryption IV	Integration type	Platform
		T874610	FIRST	3848844415GNQYHB	8166905130LKGDIY	Mobile App	.Net


		Test Bank login credentials:

		Login id : test
		Password : test

		For all transactions lower amount limit is defined as Rs.1/- and upper amount limit is defined as Rs.10/-. Please make sure your transaction requests are within these limits.

		Please inform us after you have completed Integration activities at your end.

		You can download appropriate INTEGRATION KIT from following link : https://www.techprocesssolutions.co.in/PaynimoKit/index.html

		Please contact us at Prod.Provisioning.ind@ingenico.com for any queries.

		Thanks
	*/
}

func (u WDLineService) DualVerification(orderId string, date string) (string, error) {
	req := DualVerificationReq{
		Merchant: MerchantType{
			Identifier: u.MerchantCode,
		},

		Transaction: TransactionType{
			DeviceIdentifier: "S",
			Currency:         "INR",
			Token:            orderId,
			DateTime:         date, // "17-01-2018",
			RequestType:      "S",
		},
	}

	reqData, err := json.Marshal(req)
	if err != nil {
		logger.Error("WDLPayService_dual_Marshal_Err | err=%v", err)
		return "", err
	}

	header := http.Header{
		"Content-Type": {"application/json; charset=utf-8"},
	}

	_, res, err := utils.Post("https://www.paynimo.com/api/paynimoV2.req", reqData, header)
	if err != nil {
		logger.Error("WDLPayService_dual_Request_Err | order=%v | err=%v", orderId, err)
		return "", err
	}

	logger.Debug("WDLPayService_dual | order=%v |%v|err=%v", orderId, string(res), err)

	resp := DualVerificationResponse{}
	err = json.Unmarshal(res, &resp)
	if err != nil {
		logger.Error("WDLPayService_dual_Unmarshal_Err | order=%v | err=%v", orderId, err)
		return "", err
	}

	return resp.PaymentMethod.PaymentTransaction.StatusCode, nil
}

func ParseCallBack(data string) {

	values := strings.Split(data, "|")
	if len(values) < 10 {

	}

	//txnStatus := values[0]
	//txnMsg := values[1]
}

func (u WDLineService) Inquiry(orderId string, date string) (*model.PayEntry, error) {
	req := OfflineVerificationReq{
		Merchant: MerchantType{
			Identifier: u.MerchantCode,
		},

		Transaction: TransactionType{
			DeviceIdentifier: "S",
			Currency:         "INR",
			Identifier:       orderId,
			DateTime:         date, // "17-01-2018",
			RequestType:      "O",
		},
	}

	reqData, err := json.Marshal(req)
	if err != nil {
		logger.Error("WDLPayService_Inquiry_Marshal_Err | err=%v", err)
		return nil, err
	}

	header := http.Header{
		"Content-Type": {"application/json; charset=utf-8"},
	}

	_, res, err := utils.Post("https://www.paynimo.com/api/paynimoV2.req", reqData, header)
	if err != nil {
		logger.Error("WDLPayService_Inquiry_Request_Err | order=%v | err=%v", orderId, err)
		return nil, err
	}

	logger.Debug("WDLPayService_Inquiry | order=%v |%v|err=%v", orderId, string(res), err)

	resp := OfflineVerificationResponse{}
	err = json.Unmarshal(res, &resp)
	if err != nil {
		logger.Error("WDLPayService_Inquiry_Unmarshal_Err | order=%v | err=%v", orderId, err)
		return nil, err
	}

	if resp.PaymentMethod.Error.Code != "" {
		return nil, err
	}
	return nil, nil

	//engine, _ := dao.GetMysql()
	//payIn := new(model.PayEntry)
	//exist, err := engine.Where("order_id=?", orderId).Get(payIn)
	//if err != nil {
	//	logger.Error("WDLPayService_Inquiry_GetError | err=%v | data=%v", err, orderId)
	//	return nil, err
	//}
	//if !exist {
	//	return nil, errors.New("NotExist")
	//}
	//
	//if payIn.Status != model.PAYING {
	//	return nil, fmt.Errorf("no need update, status = %v", payIn.Status)
	//}
	//
	//status := resp.PaymentMethod.PaymentTransaction.StatusCode
	////msg := resp.PaymentMethod.PaymentTransaction.StatusMessage
	//errMsg := resp.PaymentMethod.PaymentTransaction.ErrorMessage
	//
	//if status == Success {
	//	payIn.Status = model.PAYSUCCESS
	//	payIn.PaymentId = resp.PaymentMethod.PaymentTransaction.Identifier
	//} else if status == Failure {
	//	payIn.Status = model.PAYFAILD
	//	payIn.ThirdCode = status
	//	payIn.ThirdDesc = errMsg
	//}
	//
	//return payIn, nil
}

func (u WDLineService) InquiryPayout(orderId string, ext string) (*model.PayOut, error) {
	return nil, fmt.Errorf("unsupported")
}

func (u WDLineService) PayOut(frozenId int64, entryValidator validator.PayOutValidator) (*model.PayOut, error) {
	return nil, fmt.Errorf("unsupported")
}

func (u WDLineService) PayOutBatch(frozenId int64, entryValidator validator.PayOutValidator) (*model.PayOut, error) {
	return nil, fmt.Errorf("unsupported")
}

func (u WDLineService) InSignature(cb interface{}) bool {
	valueStr, ok := cb.(string)
	if !ok {
		return false
	}

	values := strings.Split(valueStr, "|")
	if len(values) < 16 {
		return false
	}

	hash := values[16]
	values[16] = u.SALT

	newStr := strings.Join(values, "|")
	return hash == websh2([]byte(newStr))
}

func (u WDLineService) OutSignature(cb interface{}) bool {
	return false
}


func websh2(data []byte) string {
	sha_512 := sha512.New()
	sha_512.Write(data)
	token := sha_512.Sum(nil)

	return fmt.Sprintf("%x", token)
}