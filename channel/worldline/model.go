package worldline

import "strings"

//https://www.paynimo.com/api/paynimoV2.req

type MerchantType struct {
	Identifier string `json:"identifier"`
}

type TransactionType struct {
	DeviceIdentifier string `json:"deviceIdentifier"`
	Currency         string `json:"currency"`
	Identifier       string `json:"identifier"`
	DateTime         string `json:"dateTime"` // "17-01-2018",
	Token            string `json:"token"`
	RequestType      string `json:"requestType"`
}

type DualVerificationReq struct {
	Merchant    MerchantType    `json:"merchant"`
	Transaction TransactionType `json:"transaction"`
}

type DualVerificationResponse struct {
	MerchantCode                   string      `json:"merchantCode"`
	MerchantTransactionIdentifier  string      `json:"merchantTransactionIdentifier"`
	MerchantTransactionRequestType string      `json:"merchantTransactionRequestType"`
	ResponseType                   string      `json:"responseType"`
	TransactionState               string      `json:"transactionState"`
	MerchantAdditionalDetails      interface{} `json:"merchantAdditionalDetails"`
	PaymentMethod                  struct {
		Token               string `json:"token"`
		InstrumentAliasName string `json:"instrumentAliasName"`
		InstrumentToken     string `json:"instrumentToken"`
		BankSelectionCode   string `json:"bankSelectionCode"`
		ACS                 struct {
			BankAcsFormName   string        `json:"bankAcsFormName"`
			BankAcsHttpMethod string        `json:"bankAcsHttpMethod"`
			BankAcsParams     []interface{} `json:"bankAcsParams"`
			BankAcsUrl        string        `json:"bankAcsUrl"`
		} `json:"aCS"`
		OTP struct {
			Initiator     string `json:"initiator"`
			Message       string `json:"message"`
			NumberOfDigit string `json:"numberOfDigit"`
			Target        string `json:"target"`
			Type          string `json:"type"`
		} `json:"oTP"`
		PaymentTransaction struct {
			Amount                  string `json:"amount"`
			BalanceAmount           string `json:"balanceAmount"`
			BankReferenceIdentifier string `json:"bankReferenceIdentifier"`
			DateTime                string `json:"dateTime"`
			ErrorMessage            string `json:"errorMessage"`
			Identifier              string `json:"identifier"`
			RefundIdentifier        string `json:"refundIdentifier"`
			StatusCode              string `json:"statusCode"`
			StatusMessage           string `json:"statusMessage"`
			Instruction             struct {
				Id         string `json:"id"`
				StatusCode string `json:"statusCode"`
				Errorcode  string `json:"errorcode"`
				Errordesc  string `json:"errordesc"`
			} `json:"instruction"`
			Reference interface{} `json:"reference"`
			AccountNo interface{} `json:"accountNo"`
		} `json:"paymentTransaction"`
		Authentication struct {
			Type    string `json:"type"`
			SubType string `json:"subType"`
		} `json:"authentication"`
		Error struct {
			Code string `json:"code"`
			Desc string `json:"desc"`
		} `json:"error"`
	} `json:"paymentMethod"`
	Error interface{} `json:"error"`
}


// https://www.paynimo.com/api/paynimoV2.req

type OfflineVerificationReq struct {
	Merchant    MerchantType    `json:"merchant"`
	Transaction TransactionType `json:"transaction"`
}

type OfflineVerificationResponse struct {
	MerchantCode                   string      `json:"merchantCode"`
	MerchantTransactionIdentifier  string      `json:"merchantTransactionIdentifier"`
	MerchantTransactionRequestType string      `json:"merchantTransactionRequestType"`
	ResponseType                   string      `json:"responseType"`
	TransactionState               string      `json:"transactionState"`
	MerchantAdditionalDetails      interface{} `json:"merchantAdditionalDetails"`
	PaymentMethod                  struct {
		Token               string `json:"token"`
		InstrumentAliasName string `json:"instrumentAliasName"`
		InstrumentToken     string `json:"instrumentToken"`
		BankSelectionCode   string `json:"bankSelectionCode"`
		ACS                 struct {
			BankAcsFormName   string        `json:"bankAcsFormName"`
			BankAcsHttpMethod string        `json:"bankAcsHttpMethod"`
			BankAcsParams     []interface{} `json:"bankAcsParams"`
			BankAcsUrl        string        `json:"bankAcsUrl"`
		} `json:"aCS"`
		OTP struct {
			Initiator     string `json:"initiator"`
			Message       string `json:"message"`
			NumberOfDigit string `json:"numberOfDigit"`
			Target        string `json:"target"`
			Type          string `json:"type"`
		} `json:"oTP"`
		PaymentTransaction struct {
			Amount                  string `json:"amount"`
			BalanceAmount           string `json:"balanceAmount"`
			BankReferenceIdentifier string `json:"bankReferenceIdentifier"`
			DateTime                string `json:"dateTime"`
			ErrorMessage            string `json:"errorMessage"`
			Identifier              string `json:"identifier"`
			RefundIdentifier        string `json:"refundIdentifier"`
			StatusCode              string `json:"statusCode"`
			StatusMessage           string `json:"statusMessage"`
			Instruction             struct {
				Id         string `json:"id"`
				StatusCode string `json:"statusCode"`
				Errorcode  string `json:"errorcode"`
				Errordesc  string `json:"errordesc"`
			} `json:"instruction"`
			Reference interface{} `json:"reference"`
			AccountNo interface{} `json:"accountNo"`
		} `json:"paymentTransaction"`
		Authentication struct {
			Type    string `json:"type"`
			SubType string `json:"subType"`
		} `json:"authentication"`
		Error struct {
			Code string `json:"code"`
			Desc string `json:"desc"`
		} `json:"error"`
	} `json:"paymentMethod"`
	Error interface{} `json:"error"`
}

type WDLCallBack struct {
	TxnStatus         string `json:"txn_status"`
	TxnMsg            string `json:"txn_msg"`
	TxnErrMsg         string `json:"txn_err_msg"`
	ClntTxnRef        string `json:"clnt_txn_ref"`
	TpslBankCd        string `json:"tpsl_bank_cd"`
	TpslTxnId         string `json:"tpsl_txn_id"` // world line transaction id used for query / dual check
	TxnAmt            string `json:"txn_amt"`
	ClntRqstMeta      string `json:"clnt_rqst_meta"`
	TpslTxnTime       string `json:"tpsl_txn_time"` // 28-06-2023+16:33:22
	BalAmt            string `json:"bal_amt"`
	CardId            string `json:"card_id"`
	AliasName         string `json:"alias_name"`
	BankTransactionID string `json:"bank_transaction_id"`
	MandateRegNo      string `json:"mandate_reg_no"`
	Token             string `json:"token"`
	Hash              string `json:"hash"`
}

func (wdl *WDLCallBack) Parse(data string) bool {
	//	txn_status|txn_msg|txn_err_msg|clnt_txn_ref|tpsl_bank_cd|tpsl_txn_id|txn_amt|clnt_rqst_meta|tpsl_txn_time|bal_amt|card_id|alias_name|BankTransactionID|mandate_reg_no|token|hash
	//	"0392|
	//	User+Aborted|
	//	Aborted,+as+no+activity+performed+by+User|
	//	1687775714264|
	//	|
	//	E31196086|
	//	10|
	//	{itc:}{email:test@test.com}{mob:9876543210}|
	//	28-06-2023+16:33:22|
	//	NA|
	//	|
	//	|
	//	|
	//	|
	//	|
	//	80b2c033f59881f1dfafc4dfd80c02ee9edc0e422be7da37f89029bb84e67bfc06aa8b60ce4a4cb2d63cb386d4b150a6c8c38c9e3d03fc33e4bf5f165a2aaa18"

	// txn_status value will be 0300 in case of Success, 0398 in case of Initiated, 0399 in case of failure, 0396 in case of Awaited and 0392 in case of Aborted.
	// On the basis of response code you can mark your transaction either Success/Initiated/Failed/Awaited/Aborted.

	values := strings.Split(data, "|")
	if len(values) < 16 {
		return false
	}

	wdl.TxnStatus = values[0]
	wdl.TxnMsg = values[1]
	wdl.TxnErrMsg = values[2]
	wdl.ClntTxnRef = values[3]
	wdl.TpslBankCd = values[4]
	wdl.TpslTxnId = values[5]
	wdl.TxnAmt = values[6]
	wdl.ClntRqstMeta = values[7]
	wdl.TpslTxnTime = values[8]
	wdl.BalAmt = values[9]
	wdl.CardId = values[10]
	wdl.AliasName = values[11]
	wdl.BankTransactionID = values[12]
	wdl.MandateRegNo = values[13]
	wdl.Token = values[14]
	wdl.Hash = values[15]

	return true
}