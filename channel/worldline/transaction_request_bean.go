package worldline

import (
	"crypto/sha1"
	"fmt"
	"funzone_pay/utils"
)

type TransactionMsg struct {

}

type TransactionRequestBean struct {
	MerchantCode         string
	ITC                  string
	CustomerName         string
	RequestType          string
	MerchantTxnRefNumber string
	Amount               string
	CurrencyCode         string
	ReturnURL            string
	ShoppingCartDetails  string
	TPSLTxnID            string
	MobileNumber         string
	TxnDate              string
	BankCode             string
	CustId               string
	Key                  string
	IV                   string
	AccountNo            string
	WebServiceLocator    string
	Timeout              int
}

func (t TransactionRequestBean) validateRequestParam() bool {
	if t.RequestType == "" || t.MerchantCode == "" || t.Key == "" || t.IV == "" {
		return false
	}

	return true
}

func (t TransactionRequestBean) getEncryptedData() (string, error) {
	clientMetaData := ""
	if t.ITC != "" {
		clientMetaData += fmt.Sprintf("{itc:%v}", t.ITC)
	}
	//if t.Email != "" {
	//	clientMetaData += fmt.Sprintf("{email:%v}", t.Email)
	//}
	if t.MobileNumber != "" {
		clientMetaData += fmt.Sprintf("{mob:%v}", t.MobileNumber)
	}
	//if (!$this->isBlankOrNull($this->uniqueCustomerId)) {
	//$clientMetaData .= "{custid:" . $this->uniqueCustomerId . "}";
	//}
	if t.CustomerName != "" {
		clientMetaData += fmt.Sprintf("{custname:%v}", t.CustomerName)
	}

	strReqst := ""
	strReqst += fmt.Sprintf("rqst_type=%v", t.RequestType)
	strReqst += fmt.Sprintf("|rqst_kit_vrsn=%v", RqstKitVrsn)
	strReqst += fmt.Sprintf("|tpsl_clnt_cd=%v", t.MerchantCode)
	if t.AccountNo != "" {
		strReqst += fmt.Sprintf("|accountNo=%v", t.AccountNo)
	}

	strReqst += fmt.Sprintf("|clnt_txn_ref=%v", t.MerchantTxnRefNumber)

	if clientMetaData != "" {
		strReqst += fmt.Sprintf("|clnt_rqst_meta=%v", clientMetaData)
	}

	strReqst += fmt.Sprintf("|rqst_amnt=%v", t.Amount)
	strReqst += fmt.Sprintf("|rqst_crncy=%v", t.CurrencyCode)
	strReqst += fmt.Sprintf("|rtrn_url=%v", t.ReturnURL)
	//if (!$this->isBlankOrNull($this->s2SReturnURL)) {
	//	$this->strReqst .= "|s2s_url=" . $this->s2SReturnURL;
	//}
	if t.ShoppingCartDetails != "" {
		strReqst += fmt.Sprintf("|rqst_rqst_dtls=%v", t.ShoppingCartDetails)
	}
	if t.TxnDate != "" {
		strReqst += fmt.Sprintf("|clnt_dt_tm=%v", t.TxnDate)
	}
	strReqst += fmt.Sprintf("|tpsl_bank_cd=%v", t.BankCode)
	if t.TPSLTxnID != "" {
		strReqst += fmt.Sprintf("|tpsl_txn_id=%v", t.TPSLTxnID)
	}
	if t.CustId != "" {
		strReqst += fmt.Sprintf("|cust_id=%v", t.CustId)
	}
	//if (!$this->isBlankOrNull($this->cardId)) {
	//	$this->strReqst .= "|card_id=" . $this->cardId;
	//}
	if t.MobileNumber != "" {
		strReqst += fmt.Sprintf("|mob=%v", t.MobileNumber)
	}
	//if (!$this->isBlankOrNull($this->txn_type)) {
	//	$this->strReqst .= "|TxnType=" . $this->txn_type;
	//}
	//if (!$this->isBlankOrNull($this->txnSubType)) {
	//	$this->strReqst .= "|TxnSubType=" . $this->txnSubType;
	//}

	hash := sha1.Sum([]byte(strReqst))
	strReqst += fmt.Sprintf("|hash=%v", string(hash[:]))

	encData, err := utils.AESEncrypt([]byte(strReqst), []byte(t.Key), []byte(t.IV))
	if err != nil {
		return "", err
	}

	soapData := fmt.Sprintf("%v|%v~", string(encData), t.MerchantCode)
	return soapData, nil
}





























