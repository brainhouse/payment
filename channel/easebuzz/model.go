package easebuzz

type PayoutResponse struct {
	Success bool               `json:"success"`
	Message string             `json:"message"`
	Data    PayoutResponseData `json:"data"`
}

type PayoutResponseData struct {
	TransferRequest TransferRequest `json:"transfer_request"`
}

type TransferRequest struct {
	Id                         string `json:"id"`
	Status                     string `json:"status"`
	UniqueTransactionReference string `json:"unique_transaction_reference"`
}
