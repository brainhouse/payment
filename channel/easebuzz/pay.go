package easebuzz

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"funzone_pay/app/validator"
	"funzone_pay/centerlog"
	"funzone_pay/dao"
	"funzone_pay/model"
	"funzone_pay/utils"
	"github.com/wonderivan/logger"
	"io/ioutil"
	"net/http"
	"net/url"
	"regexp"
	"strings"
	"time"
)

type EaseBuzzService struct {
	PAY_ACCOUNT_ID string
	PAY_CHANNEL    string
	COUNTRY_CODE   int
	HOST           string
	COLLECT_HOST   string
	SK             string
	KEY            string
	SALT           string
	PAY_OUT_HOST   string
}

func GetPayEBIns(conf map[string]interface{}) *EaseBuzzService {
	return &EaseBuzzService{
		PAY_ACCOUNT_ID: conf["PAY_ACCOUNT_ID"].(string),
		PAY_CHANNEL:    conf["PAY_CHANNEL"].(string),
		COUNTRY_CODE:   conf["COUNTRY_CODE"].(int),
		HOST:           conf["HOST"].(string),
		COLLECT_HOST:   conf["COLLECT_HOST"].(string),
		SK:             conf["SK"].(string),
		KEY:            conf["KEY"].(string),
		SALT:           conf["SALT"].(string),
		PAY_OUT_HOST:   conf["PAY_OUT_HOST"].(string),
	}
}

func (ps *EaseBuzzService) CreateOrder(args validator.PayEntryValidator) (*model.PayEntry, error) {
	/*orderId := "PTM" + utils.GetOrderId("IN")
	enableList := []*EnablePaymentMode{
		{Mode: "BALANCE", Channels: []string{}},
		{Mode: "PPBL", Channels: []string{}},
		{Mode: "DEBIT_CARD", Channels: []string{}},
		{Mode: "EMI", Channels: []string{}},
		{Mode: "PAYTM_DIGITAL_CREDIT", Channels: []string{}},
		{Mode: "CREDIT_CARD", Channels: []string{"VISA", "MASTER", "AMEX"}},
		{Mode: "UPI", Channels: []string{"UPI", "UPIPUSH", "UPIPUSHEXPRESS"}},
	}
	rD := &PaytmRequestData{
		CallbackUrl:       ps.IN_CB_URL,
		EnablePaymentMode: enableList,
		RequestType:       "Payment",
		Mid:               ps.IN_MID,
		WebsiteName:       "DEFAULT",
		OrderId:           orderId,
		TxnAmount: &PaytmRequestDataTxnAmount{
			Value:    fmt.Sprint(args.Amount),
			Currency: "INR",
		},
		UserInfo: &PaytmRequestDataUserInfo{
			CustId:    fmt.Sprintf("CUST_%v", args.Phone),
			Mobile:    args.Phone,
			Email:     args.Email,
			FirstName: args.UserName,
		},
	}
	singData, _ := json.Marshal(rD)
	paytmChecksum := GenerateSignatureByString(string(singData), ps.IN_SK)
	header := map[string][]string{
		"Content-Type": {"application/json"},
		"mid":          {ps.IN_MID},
	}
	var body = make(map[string]interface{})
	body["body"] = rD
	body["head"] = map[string]interface{}{
		"signature": paytmChecksum,
	}
	requestUrl := fmt.Sprintf("%s/%s?mid=%v&orderId=%v", ps.IN_HOST, "theia/api/v1/initiateTransaction", ps.IN_MID, orderId)
	data, err := ps.Request(requestUrl, "POST", "JSON", body, header)

	logger.Debug("EaseBuzzService_CreateOrder_Info | orderReq=%+v | err=%v", body, err)
	paytmOrder := new(PaytmOrderResponse)
	err = json.Unmarshal(data, paytmOrder)
	if err != nil {
		logger.Error("EaseBuzzService_CreateOrder_JsonUnmarshal_Err | err=%v", err)
		return nil, err
	}
	//请求成功写入流水
	pE := new(model.PayEntry)
	pE.Uid = args.Uid
	pE.AppId = args.AppId
	pE.Amount = args.Amount
	pE.UserId = args.UserId
	pE.UserName = args.UserName
	pE.Email = args.Email
	pE.Phone = args.Phone
	pE.PayChannel = model.PAYTM
	pE.PayAccountId = ps.PAY_ACCOUNT_ID
	pE.AppOrderId = args.AppOrderId
	pE.OrderId = orderId
	pE.PaymentOrderId = paytmOrder.Body.TxnToken
	pE.ThirdCode = paytmOrder.Body.ResultInfo.ResultCode
	pE.Status = model.PAYING
	pE.ThirdDesc = paytmOrder.Body.ResultInfo.ResultMsg
	pE.PayAppId = ps.IN_MID
	engine, err := dao.GetMysql()
	if engine == nil || err != nil {
		logger.Error("EaseBuzzService_CreateOrder_GetDB_Err | err=%v", err)
		return nil, err
	}
	_, err = engine.Insert(pE)
	if err != nil {
		logger.Error("EaseBuzzService_CreateOrder_InsertDB_Err | err=%v", err)
		return nil, err
	}
	logger.Debug("EaseBuzzService_CreateOrder_Request | req=%+v | res=%+v", body, paytmOrder)
	return pE, err*/
	return nil, nil
}

func (ps *EaseBuzzService) CheckSign(dataStr string) map[string]interface{} {
	enEscapeUrl, _ := url.QueryUnescape(dataStr)

	reg := regexp.MustCompile("CHECKSUMHASH=(.*?)&")
	sign := reg.FindAllStringSubmatch(enEscapeUrl, -1)
	checkSign := sign[0][1]

	values, _ := url.ParseQuery(enEscapeUrl)
	var data = make(map[string]string)
	for k, v := range values {
		data[k] = v[0]
	}
	signData := make(map[string]interface{})
	signData["data"] = data
	signData["hash"] = checkSign
	return signData
}

func (ps *EaseBuzzService) InSignature(data interface{}) bool {
	return true
}

func (ps *EaseBuzzService) OutSignature(data interface{}) bool {
	return true
}

func (ps *EaseBuzzService) Inquiry(paymentId string, orderId string) (*model.PayEntry, error) {
	return nil, nil
}

func (ps *EaseBuzzService) InquiryPayout(orderId string, ext string) (*model.PayOut, error) {
	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("EaseBuzzService_InquiryPayOut_Engine_Error | orderId=%+v | err=%v", orderId, err)
		return nil, err
	}
	auth := utils.HSha512(fmt.Sprintf("%v|%v|%v", ps.KEY, orderId, ps.SALT))
	header := map[string][]string{
		"Content-Type":  {"application/json"},
		"Authorization": {auth},
	}
	requestUrl := fmt.Sprintf("%sapi/v1/transfers/%s/", ps.PAY_OUT_HOST, orderId)
	reqData := make(map[string]string)
	reqData["key"] = ps.KEY
	reqByte, _ := json.Marshal(reqData)
	data, err := ps.Request(requestUrl, "GET", "", string(reqByte), header)
	if err != nil {
		logger.Error("EaseBuzzService_InquiryPayOut_Request_Error | err=%v", err)
		return nil, err
	}
	logger.Debug("EaseBuzzService_InquiryPayOut_Request_Info | data=%+v | orderId=%+v", string(data), orderId)
	res := new(PayoutResponse)
	err = json.Unmarshal(data, res)
	if err != nil {
		logger.Error("EaseBuzzService_InquiryPayOut_JsonUnmarshal_Error | err=%v | res=%v", err, res)
		return nil, err
	}
	payOut := new(model.PayOut)
	exist, err := engine.Where("order_id=?", orderId).Get(payOut)
	if err != nil {
		logger.Error("EaseBuzzService_InquiryPayout_GetError | err=%v | data=%v", err, orderId)
		return nil, err
	}
	if !exist {
		return nil, errors.New("NotExist")
	}
	if res.Data.TransferRequest.Status == "failure" || res.Data.TransferRequest.Status == "rejected" {
		payOut.Status = model.PAY_OUT_FAILD
		payOut.ThirdDesc = res.Message
	} else if res.Data.TransferRequest.Status == "success" {
		payOut.Status = model.PAY_OUT_SUCCESS
		payOut.PaymentId = res.Data.TransferRequest.UniqueTransactionReference
	} else if !res.Success && payOut.Created < time.Now().Unix()-3600 {
		payOut.Status = model.PAY_OUT_FAILD
	}
	return payOut, nil
}

/**
 * 出金
 */
func (ps *EaseBuzzService) PayOut(frozenId int64, args validator.PayOutValidator) (payOut *model.PayOut, err error) {
	transId := utils.GetOrderId("EBOUT")
	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("EaseBuzzService_PayOut_Engine_Error | data=%+v | err=%v", payOut, err)
		return nil, err
	}
	payOut = new(model.PayOut)
	payOut.Uid = args.Uid
	payOut.AppId = args.AppId
	payOut.PayChannel = model.EASEBUZZ
	payOut.PayAccountId = ps.PAY_ACCOUNT_ID
	payOut.CountryCode = ps.COUNTRY_CODE
	payOut.PayType = args.PayType
	payOut.AppOrderId = args.AppOrderId
	payOut.OrderId = transId
	payOut.PaymentId = transId
	payOut.FrozenId = frozenId
	payOut.Amount = args.Amount
	payOut.BankCard = args.BankCard
	payOut.UserId = args.UserId
	payOut.UserName = args.UserName
	payOut.Email = args.Email
	payOut.Paytm = args.PayTm
	payOut.IFSC = args.IFSC
	payOut.Phone = args.Phone
	payOut.VPA = args.VPA
	_, err = engine.Insert(payOut)
	if err != nil {
		logger.Error("EaseBuzzService_PayOut_Insert_Error | data=%+v | err=%v", payOut, err)
		return nil, err
	}
	var reqData interface{}
	if args.PayType == model.PT_BANK {
		reqData = &struct {
			Key                 string  `json:"key"`
			BeneficiaryType     string  `json:"beneficiary_type"`
			BeneficiaryName     string  `json:"beneficiary_name"`
			AccountNumber       string  `json:"account_number"`
			PaymentMode         string  `json:"payment_mode"`
			Amount              float64 `json:"amount"`
			UniqueRequestNumber string  `json:"unique_request_number"`
			IFSC                string  `json:"ifsc"`
		}{
			Key:                 ps.KEY,
			BeneficiaryType:     "bank_account",
			BeneficiaryName:     args.UserName,
			AccountNumber:       args.BankCard,
			PaymentMode:         "IMPS",
			Amount:              args.Amount,
			UniqueRequestNumber: transId,
			IFSC:                args.IFSC,
		}

	} else if args.PayType == "upi" {
		reqData = &struct {
			Key                 string  `json:"key"`
			BeneficiaryType     string  `json:"beneficiary_type"`
			BeneficiaryName     string  `json:"beneficiary_name"`
			UpiHandle           string  `json:"upi_handle"`
			PaymentMode         string  `json:"payment_mode"`
			Amount              float64 `json:"amount"`
			UniqueRequestNumber string  `json:"unique_request_number"`
		}{
			Key:                 ps.KEY,
			BeneficiaryType:     "upi",
			BeneficiaryName:     args.UserName,
			UpiHandle:           args.VPA,
			PaymentMode:         "UPI",
			Amount:              args.Amount,
			UniqueRequestNumber: transId,
		}
	} else {
		return nil, errors.New("PayType Not Support")
	}
	//“<key>|<account_number>|<ifsc>|<upi_handle>|<unique_request_number>|<amount>|<salt>
	auth := utils.HSha512(fmt.Sprintf("%v|%v|%v|%v|%v|%v|%v", ps.KEY, args.BankCard, args.IFSC, args.VPA, transId, args.Amount, ps.SALT))
	header := map[string][]string{
		"Content-Type":  {"application/json"},
		"Authorization": {auth},
	}
	requestUrl := fmt.Sprintf("%s%s", ps.PAY_OUT_HOST, "api/v1/quick_transfers/initiate/")
	data, err := ps.Request(requestUrl, "POST", "JSON", reqData, header)
	if err != nil {
		logger.Error("EaseBuzzService_Request_Error | err=%v", err)
		return nil, err
	}
	logger.Debug("EaseBuzzService_Request_Info | data=%v | req=%v", string(data), reqData)
	res := new(PayoutResponse)
	err = json.Unmarshal(data, res)
	if err != nil {
		centerlog.OrderError(centerlog.MsgThirdPlatformFail, payOut.PayAccountId, payOut.AppId, payOut.AppOrderId, payOut.OrderId, "false", err.Error())
		logger.Error("EaseBuzzService_PayOut_JsonUnmarshal_Error | err=%v | res=%v | reqData=%v", err, string(data), reqData)
		return nil, err
	}
	var status model.PayOutStatus
	var thirdDesc string
	if !res.Success && res.Message != "" {
		status = model.PAY_OUT_FAILD
		thirdDesc = res.Message
		centerlog.OrderError(centerlog.MsgThirdPlatformFail, payOut.PayAccountId, payOut.AppId, payOut.AppOrderId, payOut.OrderId, "false", res.Message)
	} else {
		status = model.PAY_OUT_ING
	}
	payOut.Status = status
	_, err = engine.Where("id=?", payOut.Id).Update(&model.PayOut{
		Status:     status,
		ThirdCode:  fmt.Sprint(res.Success),
		ThirdDesc:  thirdDesc,
		FinishTime: time.Now().Unix(),
	})
	if err != nil {
		logger.Error("EaseBuzzService_PayOut_Insert_Error | data=%+v | err=%v", payOut, err)
		return nil, err
	}
	if !res.Success && res.Message != "" {
		err = errors.New(thirdDesc)
		logger.Error("EaseBuzzService_PayOut_Response_Error | data=%+v", payOut)
		return payOut, err
	}
	return payOut, nil
}

func (ps *EaseBuzzService) PayOutBatch(frozenId int64, args validator.PayOutValidator) (payOut *model.PayOut, err error) {
	payOut, err = ps.PayOut(frozenId, args)
	return
}

/*
*
统一请求
*/
func (ps *EaseBuzzService) Request(url string, method string, contentType string, sendData interface{}, header map[string][]string) (res []byte, err error) {
	client := &http.Client{}
	var req *http.Request
	if contentType == "JSON" {
		data, _ := json.Marshal(sendData)
		reader := bytes.NewBuffer(data)
		req, err = http.NewRequest(method, url, reader)
	} else {
		s := sendData.(string)
		reader := strings.NewReader(s)
		req, err = http.NewRequest(method, url, reader)
	}
	//提交请求
	if err != nil {
		logger.Error("EaseBuzzService_Request_Err | url=%v | err=%+v | req=%+v", url, err, sendData)
		return
	}
	req.Header = http.Header(header)
	//处理返回结果
	response, err := client.Do(req)
	if err != nil {
		logger.Error("EaseBuzzService_Request_DoError | url=%v | err=%+v | req=%+v", url, err, sendData)
		return
	}
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		logger.Error("EaseBuzzService_Request_ReadErr | url=%v | err=%+v | req=%+v", url, err, sendData)
		return
	}
	defer func() {
		_ = response.Body.Close()
	}()
	return body, nil
}
