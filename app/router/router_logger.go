package router

import (
	"bytes"
	"fmt"
	"funzone_pay/centerlog"
	"funzone_pay/common"
	"github.com/gin-gonic/gin"
	"github.com/wonderivan/logger"
	"io/ioutil"
	"math"
	"math/rand"
	"strings"
	"time"
)

type LoggerConfig struct {
	SkipPaths []string
	IsDebug   bool
}

type responseWriter struct {
	gin.ResponseWriter
	b *bytes.Buffer
}

func (w responseWriter) Write(b []byte) (int, error) {
	w.b.Write(b)
	return w.ResponseWriter.Write(b)
}

const uuuid = "#uuuid"
const uuuidTime = "#uuuidTime"

func LoggerWithConfig(conf LoggerConfig) gin.HandlerFunc {
	var skip map[string]struct{}

	if length := len(conf.SkipPaths); length > 0 {
		skip = make(map[string]struct{}, length)

		for _, path := range conf.SkipPaths {
			skip[path] = struct{}{}
		}
	}

	isDebug := conf.IsDebug
	return func(c *gin.Context) {
		// Start timer
		start := time.Now()
		path := c.Request.URL.Path
		raw := c.Request.URL.RawQuery
		c.Request.Header.Set(uuuid, fmt.Sprintf("%x", start.Unix()<<8|rand.Int63n(math.MaxInt8)))
		c.Set(uuuidTime, int64(start.UnixNano()))

		// Log only when path is not being skipped
		_, ok := skip[path]
		if ok {
			c.Next()
			return
		}

		//
		bodyStr := ""
		if c.Request.Method == "POST" {
			body, err := c.GetRawData()
			if err != nil {
				logger.Error("uuuid:%s,GetRawData error[%v]", c.GetHeader(uuuid), err)
			}

			// 重新赋值request body
			c.Request.Body = ioutil.NopCloser(bytes.NewBuffer(body))
			bodyStr = string(body)
		}

		// 重新赋值response writer
		buff := bytes.NewBuffer([]byte{})
		c.Writer = responseWriter{
			c.Writer,
			buff,
		}
		// Process request
		c.Next()

		if raw != "" {
			path = path + "?" + raw
		}

		status := c.Writer.Status()
		dur := float64(time.Now().Sub(start)) / 1000000
		appId := c.GetHeader("AppId")
		uuuID := c.GetHeader(uuuid)
		clientIP := c.ClientIP()
		resp := buff.String()
		code := status / 100
		needResp := c.Request.Method == "POST" || c.Request.Method == "GET"
		if status != 404 && (code == 5 || len(resp) < 1 && code != 3) && needResp && !isDebug {
			raw := ""
			msg := ""
			level := common.Info
			if len(resp) > 1 {
				raw = strings.ReplaceAll(resp, "\"", "")
				raw = strings.ReplaceAll(raw, "\n", "")
				msg = fmt.Sprintf("支付有接口错误:%d,%v,uuid:%v,app_id:%v,raw:%v", status, path, uuuID, appId, raw)
			} else {
				msg = fmt.Sprintf("支付有接口可能panic:%v,uuid:%v,app_id:%v", path, uuuID, appId)
				level = common.Warning
			}

			common.PushAlarmEvent(level, appId, path, raw, msg)

			centerlog.Error(centerlog.MsgErrorResp, map[string]interface{}{
				centerlog.FieldUUUID:    uuuID,
				centerlog.FieldStatus:   status,
				centerlog.FieldDur:      dur,
				centerlog.FieldClientIP: clientIP,
				centerlog.FieldPath:     path,
				centerlog.FieldAppId:    appId,
				centerlog.FieldReqBody:  bodyStr,
				centerlog.FieldRaw:      raw,
			})
		}

		if dur > 20000 { // 大于20s
			msg := fmt.Sprintf("接口处理时间过长,可能负载过高:dur:%v,%v,uuid:%v,app_id:%v",
				dur, path, uuuID, appId)
			common.PushAlarmEvent(common.Warning, appId, path, "", msg)
		}

		centerlog.Info(centerlog.MsgAPI, map[string]interface{}{
			centerlog.FieldUUUID:    uuuID,
			centerlog.FieldStatus:   status,
			centerlog.FieldDur:      dur,
			centerlog.FieldClientIP: clientIP,
			centerlog.FieldPath:     path,
			centerlog.FieldAppId:    appId,
			centerlog.FieldReqBody:  bodyStr,
		})

		logger.Info("speed-uuuid:%s,code:%d,dur:%.3f,ip:%s,path:%s,m:%v,app_id:%v,body:%s,resp:%s", uuuID, code, dur, clientIP, path, c.Request.Method, appId, bodyStr, resp)
	}
}
