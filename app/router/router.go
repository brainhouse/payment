package router

import (
	"funzone_pay/app/controller"
	"funzone_pay/app/middlewares"
	"funzone_pay/app/service"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/spf13/viper"
	"log"
)

func GetRouter() *gin.Engine {
	// Engin
	router := gin.New()
	isDebug := viper.GetString("server.mode") != "release"
	router.Use(LoggerWithConfig(LoggerConfig{SkipPaths: []string{}, IsDebug: isDebug}), gin.Recovery())
	//router := gin.New()c
	v1 := router.Group("/v1/")
	{
		v1.Use(cors.Default())
		//创建入金订单-已经废弃
		v1.POST("create_in_order", controller.CreateInOrder)
		//同步订单状态-已经废弃
		v1.POST("get_order_status", controller.GetOrderStatus)
		v1.GET("order_status", controller.GetOrderStatus2)
		//创建提现订单-已经废弃
		v1.POST("create_out_order", controller.CreatePayOutOrder)

		v1.POST("test", controller.TestIndex)
		v1.POST("test_cf", controller.TestCf)
		v1.POST("test_out", controller.TestOut)
		v1.POST("web_log", controller.WebLog)
		v1.OPTIONS("web_log", controller.WebLogOption)
		v1.POST("order_click", controller.ClickOrderLink)
		v1.OPTIONS("order_click", controller.ClickOrderLinkOption)
		v1.GET("order", controller.FMOrder)
		internal := v1.Group("internal/")
		{
			internal.Use(middlewares.InternalAuth)
			internal.POST("reload", func(c *gin.Context) {
				service.Reload()
				c.JSON(200, map[string]interface{}{"status": "success"})
			})
			//手动结算
			internal.POST("settlement", controller.CollectSettlement)
			internal.POST("user_settlement", controller.UserCollectSettlement)
			internal.GET("query_settlement", controller.QuerySettlement)
			//支付用户提现
			internal.POST("pay_withdraw", controller.PayWithdraw)
			//充值回滚
			internal.POST("charge_rollback", controller.ChargeRollback)
			internal.POST("charge_rollback_bat", controller.ChargeRollbackBat)
			internal.POST("payout_rollback", controller.PayoutRollback)
			internal.POST("payout_m_to_fail", controller.PayoutToFail)
			//手动成功回调
			internal.POST("charge_manual_callback", controller.ManualCheckCallback)
			//结果触发
			internal.POST("retry_callback", controller.InternalRetryCallback)
			internal.POST("release_transaction", controller.InternalReleaseTransaction)
		}
		//支付平台接口
		platform := v1.Group("platform/")
		{
			// 客户限速的接口
			limit := map[string]int64{
				"retry_collect_callback": 5,
				"retry_payout_callback":  5,
				"collect_order":          64,
			}
			platform.Use(middlewares.SpeedLimit(limit), middlewares.PlatFormAuth)
			platform.POST("collect_order", controller.CollectOrder)
			platform.POST("payout", controller.PayOutOrder)
			platform.GET("inquiry_payout_status", controller.InquiryPayOutStatus)
			platform.GET("inquiry_account", controller.InquiryAccountDetail)
			platform.POST("retry_collect_callback", controller.RetryCollectCallback)
			platform.POST("retry_payout_callback", controller.RetryPayoutCallback)
		}
	}
	cb := router.Group("/callback/")
	{
		cb.POST("paytm_order", controller.PayTmOrderCallback)
		//cb.GET("paytm_order", controller.PayTmOrderCallback)
		cb.POST("paytm_payout", controller.PayTmPayOutCallback)
		//payu支付回调
		cb.POST("payu_order", controller.PayUOrderCallback)
		//payu提现回调
		cb.POST("payu_payout", controller.PUPayOutCallback)
		//easebuzz充值回调
		cb.POST("easebuzz_order", controller.EBPayOutCallback)
		//easebuzz提现回调
		cb.POST("easebuzz_payout", controller.EBPayOutCallback)
		//eaglepay充值回调
		cb.POST("eaglepay_collect", controller.EaglePayCollectCallback)
		//eaglepay提现回调
		cb.POST("eaglepay_payout", controller.EaglePayPayOutCallback)
		//serpay充值回调
		cb.POST("serpay_collect", controller.SerPayCollectCallback)
		//serpay提现回调
		cb.POST("serpay_payout", controller.SerPayPayOutCallback)
		//onion充值回调
		cb.POST("onionpay_collect", controller.OnionPayCollectCallback)
		//oniony提现回调
		cb.POST("onionpay_payout", controller.OnionPayPayOutCallback)
		//onion充值回调
		cb.POST("fidypay_collect", controller.FidyPayCollectCallback)
		//oniony提现回调
		cb.POST("fidypay_payout", controller.FidyPayPayOutCallback)
		//pagsmile充值回调
		cb.POST("pagsmilepay_collect", controller.PagSmilePayCollectCallback)
		//pagsmile提现回调
		cb.POST("pagsmilepay_payout", controller.PagSmilePayPayOutCallback)
		//globpay充值回调
		cb.POST("globpay_collect", controller.GlobPayCollectCallback)
		//globpay提现回调
		cb.POST("globpay_payout", controller.GlobPayPayOutCallback)
		// pay-flash
		cb.POST("payflash_collect", controller.PayFlashCollectCallback)
		cb.POST("payflash_payout", controller.PayFlashPayOutCallback)
		//cb.POST("pdk_collect", controller.PDKPayCollectCallback)
		//cb.POST("pdk_payout", controller.PDKPayOutCallback)
		cb.POST("zw_collect", controller.ZWPayCollectCallback)
		cb.POST("zw_payout", controller.ZWPayOutCallback)
		cb.POST("fm_collect", controller.FMPayCollectCallback)
		cb.POST("fm_payout", controller.FMPayOutCallback)

		cb.POST("success", controller.SuccessCallback)
		cb.POST("failed", controller.FailedCallback)
		cb.GET("success", controller.SuccessCallback)
		cb.GET("failed", controller.FailedCallback)

		cb.POST("rz_callback", controller.OrderCallback)
		cb.POST("rz_payout_callback", controller.RZPayOutCallback)

		cb.POST("yb_callback", controller.YBCallback)
		cb.POST("yb_payout_callback", controller.YBPayOutCallback)

		cb.POST("loog_collect", controller.LoogPayCollectCallback)
		cb.POST("loog_payout", controller.LoogPayOutCallback)

		cb.POST("x_collect", controller.XPayCollectCallback)
		cb.POST("x_payout", controller.XPayOutCallback)

		cb.POST("ydg_callback", controller.YDGCallback)
		cb.POST("ydg_payout_callback", controller.YDGPayOutCallback)

		cb.POST("airpay_callback", controller.AirPayCallback)
		cb.POST("clickncash_callback", controller.ClicknCashPayCallback)
		cb.POST("pgpay_callback", controller.PgPayCallback)

		cb.POST("bht_upi_callback", controller.BHTPayCallback)
		cb.POST("bht_payout", controller.BHTPayOutCallback)

		cb.POST("oepay_callback", controller.OEPayCallback)
		cb.POST("oepay_payout", controller.OEPayOutCallback)

		cb.POST("uppay_callback", controller.UpPayCollectCallback)
		cb.POST("uppay_payout_callback", controller.UpPayPayOutCallback)

		cb.POST("hopepay_in_callback", controller.HopePayInCallback)
		cb.POST("hopepay_out_callback", controller.HopePayOutCallback)
		cb.POST("hdpay_payout_callback", controller.HDPayOutCallback)
		cb.POST("cashpay_callback", controller.CashPayOutCallback)
		cb.POST("wdp_callback", controller.WorldLineCallback)
		cb.POST("phonepay_callback", controller.PhonePayCallback)
	}
	rz := router.Group("/razorpay/")
	{
		rz.POST("callback", controller.OrderCallback)
		rz.POST("payout_callback", controller.RZPayOutCallback)
	}
	cf := router.Group("/cashfree/")
	{
		//cf.GET("test", controller.CashfreeTest)
		cf.POST("order_callback", controller.CFOrderCallback)
		cf.POST("pay_out_callback", controller.CFPayOutCallback)
	}
	upi := router.Group("/upi/")
	{
		upi.Use(middlewares.Cors())
		upi.GET("verify_vpa", controller.VerifyVPA)
		upi.POST("collect", controller.UpiCollect)
		upi.GET("pay_status", controller.UpiPayInStatus)
	}
	router.POST("/center-log", controller.CollectLog)
	router.GET("/health", controller.Health)
	router.GET("/", func(context *gin.Context) {
		log.Println(">>>> hello gin start <<<<")
		context.JSON(200, gin.H{
			"code":    200,
			"success": true,
		})
	})
	router.GET("/hello", func(context *gin.Context) {
		log.Println(">>>> hello gin start <<<<")
		context.JSON(200, gin.H{
			"code":    200,
			"success": true,
		})
	})

	return router
}
