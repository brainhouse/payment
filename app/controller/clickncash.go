package controller

import (
	"encoding/json"
	"funzone_pay/app/service"
	"funzone_pay/model"
	"github.com/gin-gonic/gin"
	"github.com/wonderivan/logger"
	"net/url"
)

func ClicknCashPayCallback(c *gin.Context) {
	rawData, _ := c.GetRawData()
	/**
	YOURCALLBACKURL?OrderId=202301040008&txn_id=RPAYXXXXXXXXXXb6b99&Message=success&Statu
	s=success&Amount=10.00&Mode=UPI&ProviderTxnId=16525509955&bank_ref_num=300XXXXXXX898
	&PG_TYPE=UPI-PG&bankcode=UPI&payeeUpi=maXXXXXXXXXX11-
	2@okicici&payeeName=MXXXXXXXXXXai
	*/
	logger.Debug("ClicknCashPayCallback_Data data=%v", string(rawData))
	params, err := url.ParseQuery(string(rawData))
	if err != nil {
		logger.Error("ClicknCashPayCallback_Data_Error | err=%v | data=%v", err, rawData)
		c.JSON(400, &map[string]string{"msg": "parse data error"})
		return
	}
	service.GetPayEntryService().ClickNCashCallback(params)
	c.String(200, "success")
}

func BHTPayCallback(c *gin.Context) {
	rawData, _ := c.GetRawData()
	/**
	YOURCALLBACKURL?OrderId=202301040008&txn_id=RPAYXXXXXXXXXXb6b99&Message=success&Statu
	s=success&Amount=10.00&Mode=UPI&ProviderTxnId=16525509955&bank_ref_num=300XXXXXXX898
	&PG_TYPE=UPI-PG&bankcode=UPI&payeeUpi=maXXXXXXXXXX11-
	2@okicici&payeeName=MXXXXXXXXXXai
	*/
	logger.Debug("BHTPayCallback_Data data=%v", string(rawData))

	data := new(model.BHTUPICollect)
	err := json.Unmarshal(rawData, data)
	if err != nil {
		logger.Error("BHTPayCallback_Data_Error | err=%v | data=%v", err, rawData)
		c.JSON(400, &map[string]string{"msg": "parse data error"})
		return
	}
	service.GetPayEntryService().BHTUpiPayCallback(data)
	c.String(200, "success")
}

func OEPayCallback(c *gin.Context) {
	rawData, _ := c.GetRawData()
	/**
	YOURCALLBACKURL?OrderId=202301040008&txn_id=RPAYXXXXXXXXXXb6b99&Message=success&Statu
	s=success&Amount=10.00&Mode=UPI&ProviderTxnId=16525509955&bank_ref_num=300XXXXXXX898
	&PG_TYPE=UPI-PG&bankcode=UPI&payeeUpi=maXXXXXXXXXX11-
	2@okicici&payeeName=MXXXXXXXXXXai
	*/
	logger.Debug("OEPayCallback_Data data=%v", string(rawData))

	data := new(model.OECollect)
	err := json.Unmarshal(rawData, data)
	if err != nil {
		logger.Error("OEPayCallback_Data_Error | err=%v | data=%v", err, rawData)
		c.JSON(400, &map[string]string{"msg": "parse data error"})
		return
	}
	service.GetPayEntryService().OEPayCallback(data)
	c.String(200, "success")
}
