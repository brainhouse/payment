package controller

import (
	"github.com/gin-gonic/gin"
	"github.com/wonderivan/logger"
)

func EaglePayCollectCallback(c *gin.Context) {
	rawData, _ := c.GetRawData()
	logger.Debug("EaglePayCollectCallback_Data | json=%v | header=%v", string(rawData), c.Request.Header)
}

func EaglePayPayOutCallback(c *gin.Context) {
	rawData, _ := c.GetRawData()
	logger.Debug("EaglePayPayOutCallback_Data | json=%v | header=%v", string(rawData), c.Request.Header)
}
