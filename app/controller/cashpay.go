package controller

import (
	"encoding/json"
	"fmt"
	"funzone_pay/app/service"
	"funzone_pay/channel/cashpay"
	"funzone_pay/common"
	"github.com/gin-gonic/gin"
	"github.com/wonderivan/logger"
)

func CashPayOutCallback(c *gin.Context) {
	rawData, _ := c.GetRawData()

	logger.Debug("CashPayCallback_Data data=%v", string(rawData))
	notify := new(cashpay.Notification)
	success := false
	ret := ""
	defer func() {
		if !success && notify.PayType == "120" {
			orderId := notify.OrderId
			common.PushAlarmEvent(common.Warning, "no",
				fmt.Sprintf("CashPayPay/CashPayOutCallback/%v", orderId), "",
				fmt.Sprintf("Payout[%v] Order Error %v , Please check...", orderId, ret))
		}
	}()

	err := json.Unmarshal(rawData, notify)
	if err != nil {
		logger.Error("CashPayCallback_Data_Error | err=%v | data=%v", err, string(rawData))
		c.JSON(400, &map[string]string{"msg": "parse data error"})
		return
	}

	if notify.Code != 200 {
		common.PushAlarmEvent(common.Warning, fmt.Sprintf("cashpay"), "CashPayOutCallback", "", fmt.Sprintf("Invalid CashPay Callback %v-%v, order:%v", notify.Code, notify.Status, notify.MerchantOrderId))
		c.String(200, "success")
	}

	if notify.Status == cashpay.StatusWaiting || notify.Status == cashpay.StatusPending {
		c.String(200, "success")
		return
	}

	//ret := ""

	// 代收110,代付120
	switch notify.PayType {
	case "110":
		ret = service.GetPayEntryService().CashPayInCallback(notify)

	case "120":
		ret = service.GetPayOutService().CashPayoutCallback(notify)

	default:
		logger.Error("UnSupported CashPayCallback_Data data=%v", string(rawData))
	}

	if ret != "" {
		logger.Error("CashPayCallback_result|%v|%v", ret, notify.MerchantOrderId)
		return
	}

	success = true
	c.String(200, "success")
}
