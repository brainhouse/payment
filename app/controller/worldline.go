package controller

import (
	"encoding/json"
	"funzone_pay/app/service"
	"funzone_pay/channel/worldline"
	"github.com/gin-gonic/gin"
	"github.com/wonderivan/logger"
	"net/http"
)

type WDLCb struct {
	Msg string `json:"msg"`
}

func WorldLineCallback(c*gin.Context) {
	defer func() {
		c.JSON(http.StatusOK, map[string]interface{}{"message":"order is processing, please back to app"})
	}()

	rawData, _ := c.GetRawData()
	logger.Debug("WDLPayCallback_Data data=%v", string(rawData))

	var data WDLCb
	err := json.Unmarshal(rawData, &data)
	if err != nil {
		logger.Debug("WDLPayCallback_Data Unmarshal[%v] error %v", string(rawData), err)
		return
	}

	cb := worldline.WDLCallBack{}
	b := cb.Parse(data.Msg)
	if !b {
		logger.Debug("WDLPayCallback_Data Parse cb data[%v] error", data.Msg)
		return
	}

	// pending
	if worldline.IsPending(cb.TxnStatus) {
		return
	}

	ret := service.GetPayEntryService().WDLPayInCallback(cb, data.Msg)
	if ret != "" {
		logger.Debug("WDLPayCallback_Data InCallback Msg[%v]", ret)
	}

	/*
		msg=0399%7Cfailure%7CApologies.+We+are+experiencing+some+internal+issues.+Please+try+again+later.%7C1687775714264%7C810%7C65942903%7C10.00%7C%7Bemail%3Atest%40test.com%7D%7Bmob%3A9876543210%7D%7C26-06-2023+16%3A55%3A19%7CNA%7C%7C%7C%7C%7C4cd260d1-42f6-4c73-8ff3-a420d78d788c%7C3637868f644e69de52cde91b00c14d05d3f5e1c4833746bb2590a6fdd5f882a7fcb741e18204392df13b1c5b03aefd3dec8ab1a31a00e17c7b3dddd4f1b79f0

		"0392|User+Aborted|Aborted,+as+no+activity+performed+by+User|1687775714264||E31196086|10|{itc:}{email:test@test.com}{mob:9876543210}|28-06-2023+16:33:22|NA||||||80b2c033f59881f1dfafc4dfd80c02ee9edc0e422be7da37f89029bb84e67bfc06aa8b60ce4a4cb2d63cb386d4b150a6c8c38c9e3d03fc33e4bf5f165a2aaa18"
	*/

	// txn_status|txn_msg|txn_err_msg|clnt_txn_ref|tpsl_bank_cd|tpsl_txn_id|txn_amt|clnt_rqst_meta|tpsl_txn_time|bal_amt|card_id|alias_name|BankTransactionID|mandate_reg_no|token|hash

	// txn_status|txn_msg|txn_err_msg|clnt_txn_ref|tpsl_bank_cd|tpsl_txn_id|txn_amt|clnt_rqst_meta|tpsl_txn_time|bal_amt|card_id|alias_name|BankTransactionID|mandate_reg_no|token|SALT (Same key used during sending request)

	//c.JSON(http.StatusOK, map[string]interface{}{"message":"ok"})
}
