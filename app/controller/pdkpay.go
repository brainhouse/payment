package controller

import (
	"encoding/json"
	"funzone_pay/app/service"
	"funzone_pay/channel/pdkpay"
	"github.com/gin-gonic/gin"
	"github.com/wonderivan/logger"
	"net/http"
)

func PDKPayCollectCallback(c *gin.Context) {
	rawData, err := c.GetRawData()
	if err != nil {
		logger.Error("PDKPayCollect get raw data error %v", err)
	}
	logger.Debug("PDKPayCollectCallback_Data | data=%v | size %v| header=%v", string(rawData), len(rawData), c.Request.Header)

	data := pdkpay.PayNotify{}
	err = json.Unmarshal(rawData, &data)
	if err != nil {
		logger.Error("PDKPayCollectCallback_Invalid raw=%v | error=%v ", string(rawData), err)
		c.JSON(http.StatusBadRequest, "fail")
		return
	}

	if data.Status != pdkpay.ReqStatusSuccess {
		logger.Error("PDKPayCollectCallbackInvalid status =%v | data=%v ", data.Status, string(rawData))
		c.JSON(http.StatusBadRequest, "fail")
		return
	}

	ret := service.GetPayEntryService().PDKPayInCallback(data)
	if len(ret) < 1 {
		c.JSON(200, "success")
		return
	}

	c.JSON(http.StatusInternalServerError, ret)
}

func PDKPayOutCallback(c *gin.Context) {
	rawData, err := c.GetRawData()
	if err != nil {
		logger.Error("PDKPayPayout get raw data error %v", err)
	}
	logger.Debug("PDKPayOutCallback_Data | data=%v | size %v| header=%v", string(rawData), len(rawData), c.Request.Header)

	data := pdkpay.PayNotify{}
	err = json.Unmarshal(rawData, &data)
	if err != nil {
		logger.Error("PDKPayoutCallback_Invalid raw=%v | error=%v ", string(rawData), err)
		c.JSON(http.StatusBadRequest, "fail")
		return
	}

	if data.Status != pdkpay.ReqStatusSuccess {
		logger.Error("PDKPayoutCallback_Invalid status =%v | data=%v ", data.Status, string(rawData))
		c.JSON(http.StatusBadRequest, "fail")
		return
	}

	ret := service.GetPayOutService().PDKPayOutCallback(data)
	if len(ret) < 1 {
		c.JSON(200, "success")
		return
	}

	c.JSON(http.StatusInternalServerError, ret)
}
