package controller

import (
	"encoding/json"
	"fmt"
	"funzone_pay/app/service"
	"funzone_pay/channel/hopepay"
	"funzone_pay/common"
	"github.com/gin-gonic/gin"
	"github.com/wonderivan/logger"
)

/*
{
	"event":"TRANSFER_STATUS_UPDATE",
	"status":"SUCCESS",
	"data":{
		"payout_id":"H11092304141240208848912",
		"amount":"100.00",
		"remarks":"payout","created_at":"2023-04-14T12:40:20",
		"payment_mode":"IMPS","transfer_date":"Null","beneficiary_bank_name":"ptm bank",
		"beneficiary_account_ifsc":"SBIN0040241","beneficiary_account_name":"DHANUSH B B",
		"beneficiary_account_number":"64186818245","beneficiary_upi_handle":null,
		"UTR":"310418502377",
		"reference":"HOUT168147602003674131b0",
		"message":"Credited to beneficiary Mr  DHANUSH  B B on 14-04-2023 18:10:52",
		"checksum":"6a97e5306042adddc90e53c4fafba2a68067ca946fcc3e1682d13ce352566824"
	}
}
*/

func HopePayOutCallback(c *gin.Context) {
	rawData, _ := c.GetRawData()

	logger.Debug("HopePayOutCallback_Data data=%v", string(rawData))
	collectData := new(hopepay.HPOutCallBack)
	err := json.Unmarshal(rawData, collectData)
	if err != nil {
		logger.Error("HopePayOutCallback_Data_Error | err=%v | data=%v", err, rawData)
		c.JSON(400, &map[string]string{"msg": "parse data error"})
		return
	}

	success := false
	ret := ""
	defer func() {
		if !success {
			common.PushAlarmEvent(common.Warning, "no",
				fmt.Sprintf("HopePay/HopePayOutCallback/%v", collectData.OrderNo), "",
				fmt.Sprintf("Payout[%v] Callback Order Error %v, Please check...", ret, collectData.OrderNo))
		}
	}()

	ret = service.GetPayOutService().HopePayOutCallbackNew(collectData)
	if ret != "" {
		logger.Error("PayOutService_HopePayOutCallback_result|%v|%v", ret, string(rawData))
		c.Data(500, "application/json", []byte("Fail"))
		return
	}

	success = true
	c.Data(200, "application/json", []byte("SUCCESS"))
}

func HopePayInCallback(c *gin.Context) {
	rawData, _ := c.GetRawData()

	logger.Debug("HopePayInCallback_Data data=%v", string(rawData))
	collectData := new(hopepay.HPCallBack)
	err := json.Unmarshal(rawData, collectData)
	if err != nil {
		logger.Error("HopePayInCallback_Data_Error | err=%v | data=%v", err, rawData)
		c.JSON(400, &map[string]string{"msg": "parse data error"})
		return
	}

	ret := service.GetPayEntryService().HopePayEntryCallback(collectData)
	if ret != "" {
		logger.Error("PayInService_HopePayInCallback_result|%v|%v", ret, collectData.MerTradeNo)
		c.Data(500, "application/json", []byte("Fail"))
		return
	}

	c.Data(200, "application/json", []byte("SUCCESS"))
}