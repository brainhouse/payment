
package controller

import (
	"encoding/json"
	"funzone_pay/app/service"
	"funzone_pay/channel/uppay"
	"github.com/gin-gonic/gin"
	"github.com/wonderivan/logger"
)

/*
data={
	"amount":5300,
	"sign":"26ef7c7d99e3b8563e0fca0070bdbfab1716c59e9a9a434f7e667316559647f9",
	"merchantTradeId":"UIN1678948667045605080f",
	"merchantId":"quzhongshidai",
	"appId":"10007",
	"notifyUrl":"http://test-pay-callback.rummyjosh.com/callback/uppay_callback",
	"currency":"BRL","tradeId":"1636255425650102272",
	"tradeType":"pix",
	"status":"approved"

} | header=map[#uuuid:[6412ba5147] Accept:[application/json] Accept-Encoding:[gzip,deflate] Charset:[UTF-8] Connection:[close] Content-Length:[342] Content-Type:[application/json] User-Agent:[Apache-HttpClient/4.5.12 (Java/1.8.0_241)]]
*/

func UpPayCollectCallback(c *gin.Context) {
	rawData, _ := c.GetRawData()
	logger.Debug("UpPayCollectCallback_Data | data=%v | header=%v", string(rawData), c.Request.Header)

	cb := uppay.PayInNotify{}
	err := json.Unmarshal(rawData, &cb)
	if err != nil {
		logger.Error("UpPayCollectCallback_Data | Unmarshal error %v | json=%v | header=%v", err, string(rawData), c.Request.Header)
		c.JSON(400, map[string]string{"code": "400", "msg": "fail"})
		return
	}

	ret := service.GetPayEntryService().UpPayInCallback(cb)
	if len(ret) < 1 {
		c.JSON(200, map[string]string{"code": "200", "msg": "success"})
		return
	}

	c.JSON(500, &map[string]string{"code": "500", "msg": "fail"})
}

/*
{"code":"200","data":{"amount":5000,"completeTime":null,"createTime":"2023-04-07T09:06:03","currency":"BRL","merchantTradeId":"UOUT16808583560857311c65","status":"reversed","tradeId":"T2023040717060224206930"},"msg":"操作成功","type":"success"}
{"code":"200","data":{"amount":5000,"completeTime":"2023-04-07T11:17:34","createTime":"2023-04-07T09:06:03","currency":"BRL","merchantTradeId":"UOUT16808583560857311c65","status":"approved","tradeId":"T2023040717060224206930"},"msg":"操作成功","type":"success"}
*/

func UpPayPayOutCallback(c *gin.Context) {
	rawData, _ := c.GetRawData()
	logger.Debug("UpPayPayOutCallback_Data | json=%v | header=%v", string(rawData), c.Request.Header)

	cb := uppay.PayoutNotify{}
	err := json.Unmarshal(rawData, &cb)
	if err != nil {
		logger.Error("UpPayPayOutCallback_Data | Unmarshal error %v | json=%v | header=%v", err, string(rawData), c.Request.Header)
		c.JSON(400, map[string]string{"code": "400", "msg": "fail"})
		return
	}

	ret := service.GetPayOutService().UpPayOutCallback(cb)
	if len(ret) < 1 {
		c.JSON(200, map[string]string{"code": "200", "msg": "success"})
		return
	}

	c.JSON(200, &map[string]string{"code":"200"})
}
