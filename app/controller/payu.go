package controller

import (
	"encoding/json"
	"funzone_pay/app/service"
	"funzone_pay/model"
	"github.com/gin-gonic/gin"
	"github.com/wonderivan/logger"
)

func PayUOrderCallback(c *gin.Context) {
	rawData, _ := c.GetRawData()
	header := c.Request.Header
	logger.Debug("PayUOrderCallback_Data | data=%v | header=%v", string(rawData), header)

	payoutCallback := new(model.PayUOrderData)
	err := json.Unmarshal(rawData, payoutCallback)
	if err != nil {
		logger.Error("PayUOrderCallback_Err | err=%v | data=%v", err, string(rawData))
		c.JSON(200, &map[string]string{"msg": "json error"})
		return
	}
	service.GetPayEntryService().PayUOrderCallback(payoutCallback, header)
	c.JSON(200, &map[string]string{"msg": "success"})
}

func PUPayOutCallback(c *gin.Context) {
	rawData, _ := c.GetRawData()
	logger.Debug("PUPayOutCallback_Data json=%v", string(rawData))
	webHook := new(model.PayUOutCallback)
	err := json.Unmarshal(rawData, webHook)
	if err != nil {
		logger.Error("PUPayOutCallback_JsonUnmarshal_Error | err=%v", err)
		c.JSON(200, &map[string]string{"msg": "json error"})
		return
	}
	service.GetPayOutService().PayuPayOutCallback(webHook)
	c.JSON(200, &map[string]string{"msg": "success"})
}
