package controller

import (
	"fmt"
	"funzone_pay/app/service"
	"funzone_pay/model"
	"github.com/gin-gonic/gin"
	"github.com/wonderivan/logger"
)

func PayTmOrderCallback(c *gin.Context) {
	rawData, _ := c.GetRawData()
	logger.Debug("Paytm_OrderCallback_Data | data=%v", string(rawData))
	status, host := service.GetPayEntryService().PaytmOrderCallback(string(rawData))
	//c.Redirect(http.StatusMovedPermanently, "https://www.rummybank.com/m.html")
	//c.JSON(200, &map[string]string{"msg": "success"})
	if host == "" {
		host = "pay.rummybank.com"
	}
	if status == "FAILED" {
		c.Redirect(301, fmt.Sprintf("http://%v/#/callback_?type=2", host))
	} else {
		c.Redirect(301, fmt.Sprintf("http://%v/#/callback_?type=1", host))
	}
	return
	c.Header("Content-Type", "text/html; charset=utf-8")
	c.String(200, "<!DOCTYPE html>"+
		"<html lang=\"en\">"+
		"<head>"+
		"<meta charset=\"UTF-8\">"+
		"<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">"+
		"<title>RummyBank Payment Status</title>"+
		"<style>"+
		"*{"+
		"margin: 0;"+
		"padding: 0;"+
		"}"+
		"html ,body{width: 100%;height: 100%;display: flex;align-items: center;justify-content: center;"+
		"}"+
		".main{text-align: center;width: 50%;}"+
		".main img{width: 50px;height: 50px;}"+
		".main .succeed{"+
		"font-size: 18px;color: #2FCE74;margin-top: 20px;}"+
		".main .failed{font-size: 18px;color: #CD2F2F;margin-top: 20px;}"+
		".main .msg{font-size: 15px;margin-top: 20px;}"+
		"</style>"+
		"</head>"+
		"<body>"+
		"<div class=\"main\">"+
		"<p class=\"succeed\">ADD CASH succeeded</p >"+
		"<input id=\"response\" value=1 style=\"display:none;\"></input>"+
		"</div>"+
		"</body>"+
		"</html>")
	return
}

func PayTmPayOutCallback(c *gin.Context) {
	rawData, _ := c.GetRawData()
	logger.Debug("PayTmPayOutCallback | data=%v", string(rawData))
	c.JSON(200, &map[string]string{"msg": "success"})
	return
	event := c.Request.PostFormValue("event")
	transferId := c.Request.PostFormValue("transferId")
	referenceId := c.Request.PostFormValue("referenceId")
	reason := c.Request.PostFormValue("reason")
	acknowledged := c.Request.PostFormValue("acknowledged")
	eventTime := c.Request.PostFormValue("eventTime")
	utr := c.Request.PostFormValue("utr")
	signature := c.Request.PostFormValue("signature")
	webHook := &model.CFPayOutCallBack{
		Event:        event,
		TransferId:   transferId,
		ReferenceId:  referenceId,
		Reason:       reason,
		Acknowledged: acknowledged,
		EventTime:    eventTime,
		Utr:          utr,
		Signature:    signature,
	}

	service.GetPayOutService().PayTmPayOutCallback(webHook)
	c.JSON(200, &map[string]string{"msg": "success"})
}
