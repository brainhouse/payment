package controller

import (
	"funzone_pay/app/service"
	"funzone_pay/channel/xpay"
	"github.com/gin-gonic/gin"
	"github.com/wonderivan/logger"
	"net/http"
	"net/url"
)

/*
agentId=100472&respDesc=approve&orderNo=LOIN167056614500830526de&productId=0105&orderId=1601096581322092544&
signature=F2WpAWnu0EqvKxQ0KU84ro0zwl5lxtqt22dXbg7pgUUOnrtnjUOVBWa7NXvqEGYKtJUEzUpaQVBlpi8XpzaNR93cy8M3pRYGCicgmfeYMMUkWuWVvcuwIIt7BOdUDNCMEKe5F%2F6lzJt8BQyy%2Bz3EOiMM9KPl6KQ3%2BXwhNsqk5iRQPQHzwbhgISvktQZaIRZTYOZ%2Fr5Sp1PaBOeOx3CMeZD834o7JmVdBB3tL9kJ2jFK0h7bm653AoaSLS8m1ebv9fCg3g6X203MHni0EEoH5SDM0AmC8VQuH1VQrLUc3j5TXw%2FxoBpOkfN7ZMyQTPZoF3O7cBB79MFq682cdz5JVuw%3D%3D
&bankMsg=KT1139061361406&version=V1.0&tradeState=PAIED&merNo=2024201&transType=SALES&transAmt=9000&orderDate=20221209&respCode=0000
*/
func XPayCollectCallback(c *gin.Context) {
	rawData, err := c.GetRawData()
	if err != nil {
		logger.Error("XPayCollect get raw data error %v", err)
	}
	logger.Debug("XPayCollectCallback_Data | data=%v | size %v| header=%v", string(rawData), len(rawData), c.Request.Header)

	vs, err := url.ParseQuery(string(rawData))
	if err != nil {
		logger.Error("XPayCollectCallback get raw data error %v", err)
	}

	state := vs.Get(xpay.FieldState)
	if state == xpay.PayPendingStr { // 处理ing
		c.JSON(200, "success")
		return
	}

	ret := service.GetPayEntryService().XPayInCallback(vs)
	if len(ret) < 1 {
		c.JSON(200, "success")
		return
	}

	c.JSON(http.StatusInternalServerError, ret)
}

func XPayOutCallback(c *gin.Context) {
	rawData, err := c.GetRawData()
	if err != nil {
		logger.Error("XPayPayout get raw data error %v", err)
	}
	logger.Debug("XPayOutCallback_Data | data=%v | size %v| header=%v", string(rawData), len(rawData), c.Request.Header)

	vs, err := url.ParseQuery(string(rawData))
	if err != nil {
		logger.Error("XPayCollectCallback get raw data error %v", err)
	}

	state := vs.Get(xpay.FieldState)
	if state == xpay.PayPendingStr { // 处理ing
		c.JSON(200, "success")
		return
	}

	ret := service.GetPayOutService().XPayOutCallback(vs)
	if len(ret) < 1 {
		c.JSON(200, "success")
		return
	}

	c.JSON(http.StatusInternalServerError, ret)
}
