package controller

import (
	"encoding/json"
	"funzone_pay/app/service"
	"funzone_pay/channel/fmpay"
	"funzone_pay/model"
	"github.com/gin-gonic/gin"
	"github.com/wonderivan/logger"
	"net/http"
)

var fmPaySuccess = "success"

func FMOrder(c *gin.Context) {
	order, ok := c.GetQuery("orderNo")
	if !ok {
		c.JSON(http.StatusBadRequest, map[string]string{"msg": "invalid order id"})
		return
	}

	orderData, err := fmpay.OrderInfo(order)
	if err != nil {
		c.JSON(http.StatusInternalServerError, map[string]string{"msg": "invalid order id"})
		return
	}

	c.Data(http.StatusOK, "application/json; charset=utf-8", orderData)
}

func FMPayCollectCallback(c *gin.Context) {
	// {"amount":"9000.00","orderNo":"FMIN166752818309545113a6","actualPayAmount":"8974.0","payTime":"2022-11-04 07:22:00",
	// "platformOrderNo":"1588354445572308992","merchantNum":"1000000001","sign":"f72e5e3aa2e2b0fd981cb7535a65df7a","state":"1"}
	rawData, err := c.GetRawData()
	if err != nil {
		logger.Error("FMPayCollect get raw data error %v", err)
	}
	logger.Debug("FMPayCollectCallback_Data | data=%v | size %v| header=%v", string(rawData), len(rawData), c.Request.Header)

	cbData := fmpay.PayInNotify{}
	err = json.Unmarshal(rawData, &cbData)
	if err != nil {
		logger.Error("ZWPayCollectCallback_Invalid raw=%v | error=%v ", string(rawData), err)
		c.JSON(http.StatusBadRequest, "fail")
		return
	}

	payAccountId := cbData.Attch
	if len(payAccountId) < 1{
		payAccountId = "fmpay1"
	}

	ins, err := service.PayAccountConfServe.GetSpecificPayInstance(model.FMPAY, payAccountId)
	if err != nil {
		logger.Error("FMPayCollectCallback_Invalid PayAccountId=%v | error=%v ", payAccountId, err)
		c.JSON(http.StatusBadRequest, "fail")
		return
	}

	//
	checkData := make(map[string]string)
	err = json.Unmarshal(rawData, &checkData)
	if err != nil {
		logger.Error("FMPayCollectCallback_Unmarshal  PayAccountId=%v | error=%v ", payAccountId, err)
		c.JSON(http.StatusBadRequest, "fail")
	}

	if !ins.InSignature(checkData) {
		logger.Error("FMPayCollectCallback_Signature Error")
		c.JSON(http.StatusBadRequest, map[string]interface{}{"msg": "invalid sign"})
		return
	}

	ret := service.GetPayEntryService().FMPayInCallback(cbData)
	if len(ret) < 1 {
		c.JSON(http.StatusOK, fmPaySuccess)
		return
	}

	c.JSON(http.StatusInternalServerError, "fail")
}

func FMPayOutCallback(c *gin.Context) {
	// {"actualOrderAmt":"501.0","orderMsg":"TEST-FAILED","sign":"693de15648d017bd0d4711f05677aa6f","merchantOrderAmt":"501.0","signType":"MD5",
	// "merchantOrderId":"FMOUT1667549077063607248c","pfOrderTm":"20221104130440","pfServiceFee":"5.0","pfOrderId":"1588442087919976448","merchantNo":"1000000001","orderState":"02"}
	rawData, err := c.GetRawData()
	if err != nil {
		logger.Error("FMPayPayout get raw data error %v", err)
	}
	logger.Debug("FMPayOutCallback_Data | data=%v | size %v| header=%v", string(rawData), len(rawData), c.Request.Header)

	cbData := fmpay.PayOutNotify{}
	err = json.Unmarshal(rawData, &cbData)
	if err != nil {
		logger.Error("FMPayoutCallback_Invalid raw=%v | error=%v ", string(rawData), err)
		c.JSON(http.StatusBadRequest, "fail")
		return
	}

	ins, err := service.PayAccountConfServe.GetSpecificPayInstance(model.FMPAY, "fmpay1")
	if err != nil {
		logger.Error("FMPayoutCallback_Invalid | error=%v ", err)
		c.JSON(http.StatusBadRequest, "fail")
		return
	}

	checkData := make(map[string]string)
	err = json.Unmarshal(rawData, &checkData)
	if err != nil {
		logger.Error("FMPayoutCallback_Unmarshal | error=%v ", err)
		c.JSON(http.StatusBadRequest, "fail")
	}
	if !ins.OutSignature(checkData) {
		logger.Error("FMPayoutCallback_Signature Error")
		c.JSON(http.StatusBadRequest, map[string]interface{}{"msg": "invalid sign"})
		return
	}

	ret := service.GetPayOutService().FMPayOutCallback(cbData)
	if len(ret) < 1 {
		c.JSON(http.StatusOK, success)
		return
	}

	c.JSON(http.StatusOK, fmPaySuccess)
}
