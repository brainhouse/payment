package controller

import (
	"encoding/json"
	"funzone_pay/app/service"
	"funzone_pay/model"
	"github.com/gin-gonic/gin"
	"github.com/wonderivan/logger"
)

func EBPayOutCallback(c *gin.Context) {
	rawData, _ := c.GetRawData()
	logger.Debug("EBPayOutCallback_Data | json=%v | header=%v", string(rawData), c.Request.Header)
	payoutData := new(model.EaseBuzzPayoutCallback)
	err := json.Unmarshal(rawData, payoutData)
	if err != nil {
		logger.Error("EBPayOutCallback_JSON_ERROR | err=%v | data=%v", err, string(rawData))
		c.JSON(400, &map[string]string{"msg": "json error"})
		return
	}
	service.GetPayOutService().EaseBuzzPayOutCallback(payoutData)
	c.JSON(200, &map[string]string{"msg": "success"})
}
