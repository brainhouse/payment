package controller

import (
	"funzone_pay/app/service"
	"funzone_pay/model"
	"github.com/gin-gonic/gin"
	"strconv"
)

func WebLogOption(c *gin.Context) {
	c.JSON(200, gin.H{"msg": "success"})
}

func WebLog(c *gin.Context) {
	//var weblogArg = new(validator.WebLogValidator)
	//jsonData, _ := c.GetRawData()
	//err := json.Unmarshal(jsonData, weblogArg)
	//if err != nil {
	//	logger.Error("WebLog_Json_Error | err=%v | data=%v", err, string(jsonData))
	//	c.JSON(http.StatusBadRequest, gin.H{"code": "400", "error": "json Unmarshal Error"})
	//	return
	//}
	//if vs, msg := weblogArg.Valid(); !vs {
	//	c.JSON(http.StatusBadRequest, gin.H{"code": "400", "error": msg})
	//	return
	//}
	//weblogArg.IP = c.ClientIP()
	//service.WebLogServ.Record(weblogArg)
	c.JSON(200, gin.H{"msg": "success"})
	return
}

func TestCf(c *gin.Context) {
	amountStr := c.Query("amount")
	destAccunt := c.Query("dest_account")
	amount, _ := strconv.ParseFloat(amountStr, 64)
	if amount <= 0 {
		c.JSON(200, gin.H{"msg": "error"})
		return
	}
	payChannel := model.PayChannel(c.Query("pay_channel"))
	payAccountId := c.Query("pay_account_id")
	payInstance, _ := service.PayAccountConfServe.GetSpecificPayInstance(payChannel, payAccountId)
	if payInstance == nil {
		c.JSON(400, gin.H{"msg": "pay channel error"})
		return
	}

	var payAccount = make(map[string]string)
	switch destAccunt {
	case "payu4":
		payAccount["user_name"] = "PAYU"
		payAccount["bank_card"] = "PAYUNA1111504"
		payAccount["ifsc"] = "ICIC0000106"
	case "payu3":
		payAccount["user_name"] = "nodal"
		payAccount["bank_card"] = "PAYUNA1111370"
		payAccount["ifsc"] = "ICIC0000106"
	case "easebuzz1":
		payAccount["user_name"] = "EASEBUZZ"
		payAccount["bank_card"] = "EASEBZ000000043765"
		payAccount["ifsc"] = "YESB0CMSNOC"
	default:
		c.JSON(400, gin.H{"msg": "destination account error"})
		return
	}
	//payEntry, err := payInstance.PayOut(validator.PayOutValidator{
	//	AppId:      "rummy",
	//	AppOrderId: fmt.Sprintf("withdraw_%v", time.Now().Unix()),
	//	PayType:    model.PT_BANK,
	//	PayChannel: model.CASHFREE,
	//	Amount:     amount,
	//	UserId:     "10050",
	//	UserName:   payAccount["user_name"],
	//	BankCard:   payAccount["bank_card"],
	//	Phone:      "9999999999",
	//	Email:      "9999999999@cashfree.com",
	//	IFSC:       payAccount["ifsc"],
	//	Address:    "ABC Street",
	//	PayTm:      "9999999999",
	//})
	//fmt.Println(payEntry)
	//fmt.Println(err)
	c.JSON(200, gin.H{"msg": "success"})
	return
}

func TestOut(c *gin.Context) {
	//payInstance, _ := service.PayAccountConfServe.GetSpecificPayInstance(model.CASHFREE, "cashfree1")
	//payEntry, err := payInstance.PayOut(validator.PayOutValidator{
	//	AppId:      "rummy",
	//	AppOrderId: fmt.Sprintf("testout%v", time.Now().Unix()),
	//	PayType:    model.PT_UPI,
	//	PayChannel: model.CASHFREE,
	//	Amount:     10,
	//	UserId:     "10050",
	//	UserName:   "Thaneesh V",
	//	BankCard:   "1769155000016710",
	//	VPA:        "7845286022@paytm",
	//	Phone:      "7845286022",
	//	Email:      "7845286022@cashfree.com",
	//	IFSC:       "KVBL0001769",
	//	Address:    "ABC Street",
	//	PayTm:      "9999999999",
	//	Uid:        0,
	//})
	//fmt.Println(err)
	//c.JSON(200, payEntry)
}
