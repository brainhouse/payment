package controller

import (
	"funzone_pay/app/service"
	"funzone_pay/model"
	"github.com/gin-gonic/gin"
	"github.com/wonderivan/logger"
	"net/url"
)

/**
serpay充值回调
**/
func OnionPayCollectCallback(c *gin.Context) {
	rawData, _ := c.GetRawData()
	logger.Debug("OnionPayCollectCallback_Data | data=%v | header=%v", string(rawData), c.Request.Header)

	values, _ := url.ParseQuery(string(rawData))
	transNo := values.Get("transNo")
	merTransNo := values.Get("merTransNo")
	transStatus := values.Get("transStatus")
	sign := values.Get("sign")
	webHook := &model.OnionInCallback{
		TransNo:     transNo,
		MerTransNo:  merTransNo,
		TransStatus: transStatus,
		Sign:        sign,
		OrgStr:      string(rawData),
	}
	logger.Debug("OnionPayCollectCallback  | cb=%+v", webHook)
	//处理回调
	service.GetPayEntryService().OnionPayInCallback(webHook)
	c.JSON(200, &map[string]string{"msg": "success"})
}

func OnionPayPayOutCallback(c *gin.Context) {
	rawData, _ := c.GetRawData()
	logger.Debug("OnionPayPayOutCallback_Data | json=%v | header=%v", string(rawData), c.Request.Header)

	values, _ := url.ParseQuery(string(rawData))
	transNo := values.Get("transNo")
	message := values.Get("message")
	merTransNo := values.Get("merTransNo")
	transStatus := values.Get("tradeStatus")
	utr := values.Get("utr")
	sign := values.Get("sign")
	webHook := &model.OnionOutCallback{
		TransNo:     transNo,
		Message:     message,
		MerTransNo:  merTransNo,
		TransStatus: transStatus,
		Utr:         utr,
		Sign:        sign,
		OrgStr:      string(rawData),
	}
	service.GetPayOutService().OnionPayOutCallback(webHook)
	c.JSON(200, &map[string]string{"msg": "success"})
}
