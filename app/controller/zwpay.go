package controller

import (
	"encoding/json"
	"funzone_pay/app/service"
	"funzone_pay/channel/zwpay"
	"funzone_pay/model"
	"github.com/gin-gonic/gin"
	"github.com/wonderivan/logger"
	"net/http"
)

var success = map[string]interface{}{"code": 200}

func ZWPayCollectCallback(c *gin.Context) {
	rawData, err := c.GetRawData()
	if err != nil {
		logger.Error("ZWPayCollect get raw data error %v", err)
	}
	logger.Debug("ZWPayCollectCallback_Data | data=%v | size %v| header=%v", string(rawData), len(rawData), c.Request.Header)

	data := zwpay.PayInNotify{}
	err = json.Unmarshal(rawData, &data)
	if err != nil {
		logger.Error("ZWPayCollectCallback_Invalid raw=%v | error=%v ", string(rawData), err)
		c.JSON(http.StatusBadRequest, "fail")
		return
	}

	if data.Data.Status == zwpay.ZWPayInPending {
		logger.Debug("ZWPayCollectCallbackPending data=%v ", string(rawData))
		c.JSON(http.StatusOK, success)
		return
	}

	payAccountId := data.Data.ExtraInfo
	if len(payAccountId) < 1{
		payAccountId = "zwpay1"
	}

	ins, err := service.PayAccountConfServe.GetSpecificPayInstance(model.ZWPAY, payAccountId)
	if err != nil {
		logger.Error("ZWPayCollectCallback_Invalid PayAccountId=%v | error=%v ", payAccountId, err)
		c.JSON(http.StatusBadRequest, "fail")
		return
	}

	if !ins.InSignature(data) {
		logger.Error("ZWPayCollectCallback_Signature Error")
		c.JSON(http.StatusBadRequest, map[string]interface{}{"msg": "invalid sign"})
		return
	}

	ret := service.GetPayEntryService().ZWPayInCallback(data)
	if len(ret) < 1 {
		c.JSON(http.StatusOK, success)
		return
	}

	c.JSON(http.StatusInternalServerError, ret)
}

func ZWPayOutCallback(c *gin.Context) {
	rawData, err := c.GetRawData()
	if err != nil {
		logger.Error("ZWPayPayout get raw data error %v", err)
	}
	logger.Debug("ZWPayOutCallback_Data | data=%v | size %v| header=%v", string(rawData), len(rawData), c.Request.Header)

	data := zwpay.PayOutNotify{}
	err = json.Unmarshal(rawData, &data)
	if err != nil {
		logger.Error("ZWPayoutCallback_Invalid raw=%v | error=%v ", string(rawData), err)
		c.JSON(http.StatusBadRequest, "fail")
		return
	}

	if data.Data.Status == zwpay.ZWPayOutPending {
		logger.Debug("ZWPayoutCallbackPending data=%v ", string(rawData))
		c.JSON(http.StatusOK, success)
		return
	}

	payAccountId := data.Data.ExtraInfo
	if len(payAccountId) < 1{
		payAccountId = "zwpay1"
	}
	
	ins, err := service.PayAccountConfServe.GetSpecificPayInstance(model.ZWPAY, payAccountId)
	if err != nil {
		logger.Error("ZWPayoutCallback_Invalid PayAccountId=%v | error=%v ", payAccountId, err)
		c.JSON(http.StatusBadRequest, "fail")
		return
	}

	if !ins.OutSignature(data) {
		logger.Error("ZWPayoutCallback_Signature Error")
		c.JSON(http.StatusBadRequest, map[string]interface{}{"msg": "invalid sign"})
		return
	}

	ret := service.GetPayOutService().ZWPayOutCallback(data)
	if len(ret) < 1 {
		c.JSON(http.StatusOK, success)
		return
	}

	c.JSON(http.StatusInternalServerError, ret)
}
