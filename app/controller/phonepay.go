package controller

import (
	"encoding/base64"
	"encoding/json"
	"funzone_pay/app/service"
	"funzone_pay/channel/phonepay"
	"github.com/gin-gonic/gin"
	"github.com/wonderivan/logger"
	"net/http"
)

func PhonePayCallback(c*gin.Context) {
	httpCode := http.StatusOK
	msg := "success"
	defer func() {
		c.JSON(httpCode, map[string]interface{}{"message": msg})
	}()

	rawData, _ := c.GetRawData()
	logger.Debug("PhonePayCallback_Data data=%v", string(rawData))
	resp := phonepay.RespPhonePay{}
	err := json.Unmarshal(rawData, &resp)
	if err != nil || resp.Response == "" {
		logger.Error("PhonePayCallback_Data data=%v|err=%v", string(rawData), err)
		msg = "invalid response data"
		return
	}

	raw, err := base64.StdEncoding.DecodeString(resp.Response)
	if err != nil {
		logger.Error("PhonePayCallback_Data data=%v|err=%v", string(rawData), err)
		msg = "invalid response data base64"
		return
	}

	logger.Debug("PhonePayCallback_Data data=%v", string(raw))

	callback := phonepay.InquiryResponse{}
	err = json.Unmarshal(raw, &callback)
	if err != nil {
		logger.Error("PhonePayCallback_Data Unmarshal error=%v|err=%v", string(rawData), err)
		msg = "invalid response data json"
		return
	}

	if !phonepay.IsSuccess(callback.Code) && !phonepay.IsFail(callback.Code) {
		logger.Debug("PhonePayCallback_Data pending, %v|%v", callback.Code, callback.Message)
		return
	}

	xVerify := c.GetHeader("X-VERIFY")
	if xVerify == "" {
		msg = "no x-verify"
		httpCode = http.StatusBadRequest
		return
	}

	ret := service.GetPayEntryService().PhonePayInCallback(callback, resp.Response, xVerify)
	if ret != "" {
		logger.Debug("PhonePayCallback_Data InCallback Msg[%v-%v]", callback.Data.MerchantTransactionId, ret)
		msg = "invalid status or amount"
		httpCode = http.StatusBadRequest
	}
}
