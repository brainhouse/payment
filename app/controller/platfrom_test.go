package controller

import (
	"crypto/hmac"
	"crypto/sha256"
	"crypto/sha512"
	"encoding/base64"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"funzone_pay/app/service"
	"funzone_pay/app/validator"
	"funzone_pay/boostrap"
	"funzone_pay/channel"
	"funzone_pay/channel/hopepay"
	"funzone_pay/channel/payu"
	"funzone_pay/channel/phonepay"
	"funzone_pay/channel/uppay"
	"funzone_pay/channel/worldline"
	"funzone_pay/common"
	"funzone_pay/crontab"
	"funzone_pay/dao"
	"funzone_pay/model"
	"funzone_pay/utils"
	"github.com/wonderivan/logger"
	"math/rand"
	"net/url"
	"testing"
	"time"
)

func TestPayEntryService_CreateCollectOrder(t *testing.T) {
	boostrap.InitConf()
	payArg := validator.PayEntryValidator{
		Uid:    12,
		AppId:  "Bgrummy1",
		Amount: 10,
	}
	fmt.Println(111)
	data, err, _ := service.GetPayEntryService().CreateCollectOrder(12, payArg)
	fmt.Println(data)
	fmt.Println(err)
}

func TestPlatFromOrder(t *testing.T) {
	boostrap.InitConf()
	//payArg := validator.PayOutValidator{
	//	Uid:        3,
	//	AppOrderId: "test1233311",
	//	AppId:      "rummy-test",
	//	Amount:     10,
	//	UserId:     "11",
	//	UserName:   "Thaneesh V",
	//	BankCard:   "0537301000121914",
	//	Phone:      "9999999999",
	//	Email:      "test@cashfree.com",
	//	IFSC:       "LAVB0000537",
	//	Address:    "ABC Street",
	//	PayTm:      "9999999999",
	//}
	//data, err := service.GetPayOutService().PlatFromOrder(3, payArg)
	//fmt.Println(data)
	//fmt.Println(err)
}

// 墨西哥
// 巴西充值测试
func TestPayEntrySerpayMXService_CreateCollectOrder(t *testing.T) {
	boostrap.InitConf()
	payArg := validator.PayEntryValidator{
		Uid:      51,
		AppId:    "serpay1",
		Amount:   52085,
		Email:    "add-cash-serpay@gmail.com",
		Phone:    "12312312131",
		UserName: "fz-add-cash-552085",

		UserCitizenId: "cityzen-id",
		UserDeviceId:  "user-device-id",
		City:          "city",
		Street:        "street",
		HouseNumber:   "house num",
	}
	fmt.Println(111)
	data, err, _ := service.GetPayEntryService().CreateCollectOrder(51, payArg)
	fmt.Println(data)
	fmt.Println(err)
}

func TestSerpayMXPayOutPlatFromOrder(t *testing.T) {
	boostrap.InitConf()
	payArg := validator.PayOutValidator{
		Uid:        46,
		AppOrderId: "t-mx-appid-02012",
		AppId:      "t-mx-appid",
		Amount:     100,
		UserId:     "10859",
		UserName:   "Than",
		BankCard:   "50100233157710",
		Phone:      "9999999999",
		Email:      "9999999999@mxay.com",
		IFSC:       "HDFC0004050",
		Address:    "ABC Street",
		PayTm:      "9999999999",
		PayType:    model.PT_BANK,
		BankCode:   "846",
	}
	data, err := service.GetPayOutService().PlatFromOrder(46, payArg)
	fmt.Println(data)
	fmt.Println(err)
}

func TestPayEntrySerpayBrService_CreateCollectOrder(t *testing.T) {
	boostrap.InitConf()
	payArg := validator.PayEntryValidator{
		Uid:      51,
		AppId:    "test-serpay-mx-appid",
		Amount:   100,
		Email:    "test-serpay@gmail.com",
		Phone:    "12312312131",
		UserName: "test-br",
	}
	fmt.Println(111)
	data, err, _ := service.GetPayEntryService().CreateCollectOrder(51, payArg)
	fmt.Println(data)
	fmt.Println(err)
}

func TestPayEntryPDKPayCreateCollectOrder(t *testing.T) {
	boostrap.InitConf()
	payArg := validator.PayEntryValidator{
		Uid:      45,
		AppId:    "test-pay-appid",
		Amount:   120.00,
		Email:    "test-pay@gmail.com",
		Phone:    "12312312131",
		UserName: "test-pay",
	}
	fmt.Println(111)
	data, err, _ := service.GetPayEntryService().CreateCollectOrder(45, payArg)
	fmt.Println(data)
	fmt.Println(err)
}

// 巴西
func TestSerpayBRPayOutPlatFromOrder(t *testing.T) {
	boostrap.InitConf()
	payArg := validator.PayOutValidator{
		Uid:        51,
		AppOrderId: "test-serpay-br-appid-031",
		AppId:      "test-serpay-br-appid",
		Amount:     1,
		UserId:     "10859",
		UserName:   "Than",
		//WalletId:   "846180000400000001",
		BankCard: "846180000400000001",
		Phone:    "9999999999",
		Email:    "test@mxpay.com",
		IFSC:     "LAVB00",
		Address:  "ABC Street",
		PayTm:    "9999999999",
		PayType:  model.PT_PIX,
		BankCode: "846",
	}
	data, err := service.GetPayOutService().PlatFromOrder(51, payArg)
	fmt.Println(data)
	fmt.Println(err)
}

// pagsmile br
func TestPagsmileBRPayInPlatFromOrder(t *testing.T) {
	boostrap.InitConf()
	payArg := validator.PayEntryValidator{
		Uid:    51,
		AppId:  "pay-serpay-appid",
		Amount: 100,
		UserId: "108519",
	}
	fmt.Println(111)
	data, err, _ := service.GetPayEntryService().CreateCollectOrder(51, payArg)
	fmt.Println(data)
	fmt.Println(err)
}

func TestPagsmileBRPayOutPlatFromOrder(t *testing.T) {
	boostrap.InitConf()
	payArg := validator.PayOutValidator{
		Uid:           52,
		AppOrderId:    "test-pagsmile-mx-appid-035",
		AppId:         "pagsmile_br-appid",
		Amount:        100,
		UserName:      "Test User Name",
		Account:       "50284414727",
		AccountType:   "CPF",
		DocumentValue: "50284414727",
		//DocumentType:  "CPF",
	}
	data, err := service.GetPayOutService().PlatFromOrder(52, payArg)
	fmt.Println(data)
	fmt.Println(err)
}

// pagsmile mx
func TestPagsmileMXPayInPlatFromOrder(t *testing.T) {
	boostrap.InitConf()
	payArg := validator.PayEntryValidator{
		Uid:    53,
		AppId:  "pagsmile_mx-appid",
		Amount: 1,
		UserId: "10859",
	}
	fmt.Println(111)
	data, err, _ := service.GetPayEntryService().CreateCollectOrder(53, payArg)
	fmt.Println(data)
	fmt.Println(err)
}

func TestPagsmileMXPayOutPlatFromOrder(t *testing.T) {
	boostrap.InitConf()
	payArg := validator.PayOutValidator{
		Uid:           53,
		AppOrderId:    "test-pagsmile-mx-appid-035",
		AppId:         "pagsmile_mx-appid",
		Amount:        100,
		UserName:      "Test User Name",
		Account:       "50284414727",
		AccountType:   "CPF",
		DocumentValue: "50284414727",
		//DocumentType:  "CPF",
	}
	data, err := service.GetPayOutService().PlatFromOrder(53, payArg)
	fmt.Println(data)
	fmt.Println(err)
}

func TestPayFlashPlatFromOrder(t *testing.T) {
	boostrap.InitConf()
	payArg := validator.PayEntryValidator{
		Uid:        54,
		AppOrderId: "apporder_id-" + time.Now().Format("2006-01-02T15:04:05Z07:00"),
		AppId:      "jazzcash_appid",
		Amount:     11.25,
		UserId:     "10859",
		Phone:      "778983212",
		UserName:   "Test",
	}

	b, msg := payArg.PlatFormValid()
	if !b {
		fmt.Printf("form invalid %v\n", msg)
		return
	}
	data, err, _ := service.GetPayEntryService().CreateCollectOrder(54, payArg)
	fmt.Println(data)
	fmt.Println(err)
}

func TestPayFlashPlatPayout(t *testing.T) {
	boostrap.InitConf()
	//payArg := validator.PayOutValidator{
	//	Uid:        54,
	//	AppOrderId: "apporder_id-out" + time.Now().Format("2006-01-02T15:04:05Z07:00"),
	//	AppId:      "jazzcash_appid",
	//	Amount:     5,
	//	UserId:     "10859",
	//	PayChannel: model.PAYFLASH,
	//	PayType:    model.PT_BANK,
	//
	//	UserName: "DHANUSH B B",
	//	BankCard: "64186818245",
	//	Phone:    "6361406697",
	//	IFSC:     "SBIN0040241",
	//	PayTm:    "9999999999",
	//}

	payArg := validator.PayOutValidator{
		Uid:        54,
		AppOrderId: "apporder_id-out" + time.Now().Format("2006-01-02T15:04:05Z07:00"),
		AppId:      "jazzcash_appid",
		Amount:     5,
		UserId:     "10859",
		PayChannel: model.PAYFLASH,
		PayType:    model.PT_UPI,

		UserName: "DHANUSH B B",
		Phone:    "6361406697",
		//VPA:      "johndhanu123-1@okicici",
		PayTm: "9999999999",
	}

	b, msg := payArg.PlatFormValid()
	if !b {
		fmt.Printf("form invalid %v\n", msg)
		return
	}

	payOut, resKey := service.GetPayOutService().PlatFromOrder(54, payArg)
	fmt.Printf("response--->%v\n", resKey)
	fmt.Printf("resp out--->%v\n", payOut)
}

func TestPayFlashPlatQuery(t *testing.T) {
	boostrap.InitConf()
	//crontab.PayFlashOrderInquiry()
	//crontab.PayoutStatus()
}

func TestPayEntryZW(t *testing.T) {
	boostrap.InitConf()
	initConf()
	payArg := validator.PayEntryValidator{
		Uid:      45,
		AppId:    "test2-rummy",
		Amount:   90,
		Email:    "payzwpay@gmail.com",
		Phone:    "7264433607",
		UserName: "zwpay-552085",
	}
	data, err, _ := service.GetPayEntryService().CreateCollectOrder(45, payArg)
	fmt.Println(data)
	fmt.Println(err)
}

func TestPayOUTZW(t *testing.T) {
	boostrap.InitConf()

	payArg := validator.PayOutValidator{
		Uid:        45,
		AppOrderId: "apporder_id-out" + time.Now().Format("2006-01-02T15:04:05Z07:00"),
		AppId:      "test2",
		Amount:     30,
		UserId:     "10859",
		//PayChannel: model,
		PayType: model.PT_UPI,

		UserName: "DHANUSH B B",
		Phone:    "6361406697",
		VPA:      "john123-1@okicici",
	}

	b, msg := payArg.PlatFormValid()
	if !b {
		fmt.Printf("form invalid %v\n", msg)
		return
	}

	payOut, resKey := service.GetPayOutService().PlatFromOrder(45, payArg)
	fmt.Printf("response--->%v\n", resKey)
	fmt.Printf("resp out--->%v\n", payOut)
}

func TestPayQueryZW(t *testing.T) {
	boostrap.InitConf()

	//ins, _ := service.PayAccountConfServe.GetPayInstance(45, model.PAY_IN, 50)
	//data, err := ins.Inquiry("ZWIN166728352502947226e1", "")
	//fmt.Printf("\n\npayin--%v--%v\n\n\n", data, err)
	//
	//data2, err := ins.InquiryPayout("ZWOUT16672923480279040bcb")
	//fmt.Printf("payout--%v--%v\n\n\n", data2, err)
	//
	//data, err = ins.Inquiry("ZWOUT16672922350668292572", "")
	//fmt.Printf("payin--%v--%v\n\n\n", data, err)
}

func TestPayEntryFM(t *testing.T) {
	boostrap.InitConf()
	payArg := validator.PayEntryValidator{
		Uid:      146,
		AppId:    "test2",
		Amount:   9000,
		Email:    "pay-zwpay@gmail.com",
		Phone:    "12312312131",
		UserName: "zwpay-test-552085",
	}
	data, err, _ := service.GetPayEntryService().CreateCollectOrder(146, payArg)
	fmt.Println(data)
	fmt.Println(err)
}

func TestPayOUTFW(t *testing.T) {
	boostrap.InitConf()

	payArg := validator.PayOutValidator{
		Uid:        146,
		AppId:      "test2",
		Amount:     701,
		BankCard:   "123131231231231",
		BankCode:   "1",
		AppOrderId: "apporder_id-out" + time.Now().Format("2006-01-02T15:04:05Z07:00"),
		UserName:   "DHANUB",
		Phone:      "63614697",
	}

	b, msg := payArg.PlatFormValid()
	if !b {
		fmt.Printf("form invalid %v\n", msg)
		return
	}

	payOut, resKey := service.GetPayOutService().PlatFromOrder(146, payArg)
	fmt.Printf("response--->%v\n", resKey)
	fmt.Printf("resp out--->%v\n", payOut)
}

func TestPayQueryFW(t *testing.T) {
	boostrap.InitConf()

	//ins, _ := service.PayAccountConfServe.GetPayInstance(146, model.PAY_IN, 50)
	////data, err := ins.Inquiry("FMIN166752818309545113a6", "")
	////fmt.Printf("\n\npayin1--%v--%v\n\n\n", data, err)
	//
	////data2, err := ins.InquiryPayout("FMOUT166755372301736306de")
	////fmt.Printf("\n\npayout2--%v--%v\n\n\n", data2, err)
	//
	//data2, err := ins.InquiryPayout("FMOUT166755383503806a7")
	//fmt.Printf("\n\npayout2--%v--%v\n\n\n", data2, err)

	//
	//data, err = ins.Inquiry("FMIN166752818309545113a", "")
	//fmt.Printf("payin3--%v--%v\n\n\n", data, err)
}

func initConf() {
	boostrap.InitConf()
	service.Init()
}

func TestPayLoog(t *testing.T) {
	initConf()
	var uid int64 = 146

	raw := "{\"amount\":\"50.00\",\"createTime\":\"1692079664000\",\"merTradeNo\":\"HO169207965911dfbd06cd\",\"merchantNo\":\"6668688605\",\"orderNo\":\"p2_463100337748905984\",\"orderStatus\":\"2\",\"poundage\":\"7.50\",\"sign\":\"687F21807BAFFA5E20342C8A918F655E\",\"type\":\"ifsc\"}"

	var cb hopepay.HopeOutCallBack
	err := json.Unmarshal([]byte(raw), &cb)
	if err != nil {
		logger.Error("PayOutService_HopePayayOutCallback %v", err)
		return
	}

	ins, err := service.PayAccountConfServe.GetSpecificPayInstance(model.HOPEPAY, "hdpay1")
	if err != nil {
		logger.Error("PayOutService_HopePayayOutCallback Channel Not Exist")
		return
	}

	hd, ok := ins.(*hopepay.HopePayService)
	if !ok {
		return
	}

	ret, err := hopepay.QueryBalance(hd.HOST, hd.CLIENT_ID,  hd.OUT_SK)
	fmt.Printf("result--->%v:%v\n\n", ret, err)
	//payOut, err := ins.InquiryPayout("HO169207965911dfbd06cd", "")
	//fmt.Printf("query:%v:%v", payOut, err)
	//if !ins.OutSignature(cb.Map()) {
	//	logger.Error("PayOutService_HopePayayOutCallback Check PayOut Callback Sign Fail")
	//	return
	//}

	return

	// collect
	//payArg := validator.PayEntryValidator{
	//	Uid:      uid,
	//	AppId:    "test2",
	//	Amount:   90,
	//	Email:    "pay@gmail.com",
	//	Phone:    "12312312131",
	//	UserName: "paytest-552085",
	//}
	//data, err, _ := service.GetPayEntryService().CreateCollectOrder(146, payArg)
	//fmt.Printf("%v--%v\n", data, err)

	// query
	//"orderId":"1600848247447113728","orderNo":"LOIN16705069360011251e21",
	//payInstance, _, _ := service.PayAccountConfServe.GetPayInstance(uid, model.PAY_IN, 10)
	//data, err := payInstance.InquiryPayout("LOUT1670567735017270218b", "1601104037747642368")
	//fmt.Printf("%v--%v\n", data, err)
	//
	//data2, err := payInstance.InquiryPayout("LOUT16705671660274581b83", "1601100867489738752")
	//fmt.Printf("%v--%v\n", data2, err)

	// payout
	payOutArg := validator.PayOutValidator{
		Uid:        uid,
		AppId:      "test2",
		Amount:     50,
		PayType:    model.PT_BANK,
		BankCard:   "123131231231231",
		IFSC:       "KVBL0001769",
		Email: time.Now().Format("2006-01-02T15:04") + "@gmail.com",
		AppOrderId: "apporder_id-out" + time.Now().Format("2006-01-02T15:04:05Z07:00"),
		UserName:   "DNUB",
		Phone:      "8763616970",
	}

	payOut, resKey := service.GetPayOutService().PlatFromOrder(uid, payOutArg)
	fmt.Printf("response--->%v\n", resKey)
	fmt.Printf("resp out--->%v\n", payOut)
}

func TestXPay(t *testing.T) {
	initConf()
	var uid int64 = 146
	//collect
	payArg := validator.PayEntryValidator{
		Uid:      uid,
		AppId:    "test2",
		Amount:   90,
		Email:    "pay@gmail.com",
		Phone:    "12312312131",
		UserName: "paytest-552085",
	}
	data, err, _ := service.GetPayEntryService().CreateCollectOrder(146, payArg)
	fmt.Printf("%v--%v\n", data, err)

	// query
	//"orderId":"1600848247447113728","orderNo":"LOIN16705069360011251e21",
	//payInstance, _, _ := service.PayAccountConfServe.GetPayInstance(uid, model.PAY_IN, 10)
	//data, err := payInstance.InquiryPayout("LOUT1670567735017270218b", "1601104037747642368")
	//fmt.Printf("%v--%v\n", data, err)
	//
	//data2, err := payInstance.InquiryPayout("LOUT16705671660274581b83", "1601100867489738752")
	//fmt.Printf("%v--%v\n", data2, err)

	// payout
	//payOutArg := validator.PayOutValidator{
	//	Uid:        uid,
	//	AppId:      "test2",
	//	Amount:     30,
	//	BankCard:   "123131231231231",
	//	IFSC:       "KVBL0001769",
	//	AppOrderId: "apporder_id-out" + time.Now().Format("2006-01-02T15:04:05Z07:00"),
	//	UserName:   "DNUB",
	//	Phone:      "8763616970",
	//}
	//
	//payOut, resKey := service.GetPayOutService().PlatFromOrder(uid, payOutArg)
	//fmt.Printf("response--->%v\n", resKey)
	//fmt.Printf("resp out--->%v\n", payOut)
}

func genWebToken(merchantId, orderId, amount, userId, userMobile, userEmail, saltKey string) string {
	//consumerData.merchantId|
	//consumerData.txnId|
	//totalamount|
	//consumerData.accountNo|
	//consumerData.consumerId|
	//consumerData.consumerMobileNo|
	//consumerData.consumerEmailId|
	//consumerData.debitStartDate|
	//consumerData.debitEndDate|
	//consumerData.maxAmount|
	//consumerData.amountType|
	//consumerData.frequency|
	//consumerData.cardNumber|
	//consumerData.expMonth|
	//consumerData.expYear|
	//consumerData.cvvCode|
	//SALT [Salt will be given by Worldline]

	source := fmt.Sprintf("%v|%v|%v|%v|%v|%v|%v|%v|%v|%v|%v|%v|%v|%v|%v|%v|%v", merchantId, orderId, amount, "", userId, userMobile, userEmail, "", "", "", "", "", "", "", "", "",saltKey)
	sha_512 := sha512.New()
	sha_512.Write([]byte(source))
	token := sha_512.Sum(nil)

	//6fd8c92e3036bc2c3766c54ecab3fb2afa3d46247c1a62ae2c98932b7b217e12604864a9470ea50a87018e17e5f412344a093bddfd8daead7c13a213dc573906
	//4c6c655aa5b6cb482cf4112a71f58d97ae84202b16a7a51d965970a0b9f9e740c37e9e73aabdafa0cc2f4a78206ac24577eefbf12331292c9453ab1a85637bdf
	//84ad5dfdd5265d37a3732986ffdee98fc0e635dbffa16f8771f5421cfb9c13dee61183d2ab8c90e647cf00daaa4e6141d2c2e40c4c39cbd42e3629c9076bff42
	//bcfe9847ec33da4f7fbf539489aeb458805a67fe8b88c1362a32dfeafd0099aaa8097502329ba9c65278e136f46de8fc093fbc41e605b8d57766c1f1fb5542eb
	//4c6c655aa5b6cb482cf4112a71f58d97ae84202b16a7a51d965970a0b9f9e740c37e9e73aabdafa0cc2f4a78206ac24577eefbf12331292c9453ab1a85637bdf
	return fmt.Sprintf("%x", token)
}

func TestWorldLine(t *testing.T) {
	merchantId := "T874610"
	orderId := "CIB7771760611123"
	amount := "10.1"
	userId := "1001"
	mobile := "8506234781"
	email := "8506234781@gmail.com"
	salt := "3848844415GNQYHB"
	token := genWebToken(merchantId, orderId, amount,
		userId, mobile, email, salt)

	f := "merchantId=%v&amount=%v&consumerId=%v&consumerMobileNo=%v&consumerEmailId=%v&txnId=%v&token=%v"

	fmt.Printf(f, merchantId, amount, userId, mobile, email, orderId, token)

	params := url.Values{}
	params.Add("mid", merchantId) // merchantId
	params.Add("am", amount)      // amount
	params.Add("cid", userId)     // consumerId
	params.Add("mo", mobile)      // consumerMobileNo
	params.Add("em", email)       // consumerEmailId
	params.Add("txn", orderId)    // txnId
	params.Add("token", token)
	fmt.Printf("\n\n\n%v\n\n\n", params.Encode())

	/*

		PAY_ACCOUNT_ID string
			PAY_CHANNEL    string
			COUNTRY_CODE   int

			MerchantCode    string
			KEY             string
			IV              string
			SALT            string
			//SchemeCode      string
			//ShowAllResponse string

			InHost      string
			InReturnUrl string
	*/
	wdl := worldline.WDLineService{
		PAY_ACCOUNT_ID: "wdlpay1",
		PAY_CHANNEL:    "wdlpay",
		COUNTRY_CODE:   356,
		MerchantCode:   "T874610",
		KEY:            "3848844415GNQYHB",
		IV:             "8166905130LKGDIY",
		SALT:           "3848844415GNQYHB",
	}

	//wdl.Inquiry("sfadfafafa", "")
	//msg=
	// 0300|success|NA|1687771760611|470|67358578|10.00|{email:8506234781@gmail.com}{mob:8506234781}|28-06-2023+18:06:43|NA|||||b136e537-0471-40e7-b8cc-d45a05e59b51|6c4c44b35b707568243c1e864edc260fd8c726fe8ff7cce8c46cdabbdca04f9064b3210fe6f02305bc8056f5f18d28b5d5928375e26368f681f887e4ebc6c100"
	// 0399|failure|TPPGE069|WIN168803376406382921fa|470|E31208570|10.10|{email:pay@gmail.com}{mob:12312312131}|29-06-2023+15:48:41|NA|||||68e9c662-98fc-4b02-982e-28429a543bd0|10af395898ad965a48f32568ad89772f42c29a8f75c8e3098366a0254293390c17f0fc74ffd8dd66e25ae96924d390f3ab42c527e848ee60271f1b355686e131
	wdl.DualVerification("E31208570", "29-06-2023")
	wdl.Inquiry("WIN168803376406382921fa", "29-06-2023")

	initConf()
	var uid int64 = 146
	//collect
	payArg := validator.PayEntryValidator{
		Uid:      uid,
		AppId:    "test2",
		Amount:   10,
		Email:    email,
		Phone:    mobile,
		UserId:   "111",
		UserName: "paytest-552085",
	}
	data, err, _ := service.GetPayEntryService().CreateCollectOrder(146, payArg)
	fmt.Printf("%v--%v\n", data, err)
}

func TestPhonePay(t *testing.T) {
	//raw := "{\n  \"merchantId\": \"MERCHANTUAT\",\n  \"merchantTransactionId\": \"MT7850590068188104\",\n  \"merchantUserId\": \"MUID123\",\n  \"amount\": 10000,\n  \"redirectUrl\": \"https://webhook.site/redirect-url\",\n  \"redirectMode\": \"POST\",\n  \"callbackUrl\": \"https://webhook.site/callback-url\",\n  \"mobileNumber\": \"9999999999\",\n  \"paymentInstrument\": {\n    \"type\": \"PAY_PAGE\"\n  }\n}"
	//token := phonepay.XVerify([]byte(raw), "/pg/v1/pay", "099eb0cd-02cf-4e2a-8aca-3e6c6aff0399", "1")
	//fmt.Printf("token=%v\n", token)
	//return

	phonp := phonepay.PhonePayService {
		PAY_ACCOUNT_ID: "wdlpay1",
		PAY_CHANNEL:    "wdlpay",
		COUNTRY_CODE:   356,
		MerchantCode:   "SOFTUAT",
		SALT:           "449fdf88-a61b-4b47-9166-f60d9d14961f",
		SALT_INDEX: "1",
		API_HOST: "",
	}

	phonp.Inquiry("PIN1689586333020436122d", "")

	return


	initConf()

	var uid int64 = 165
	//collect
	payArg := validator.PayEntryValidator{
		Uid:      uid,
		AppId:    "test2",
		Amount:   100,
		Email:    "9999999999@gmail.com",
		Phone:    "9999999999",
		UserId:   "MUID123",
		UserName: "paytest-552085",
	}
	data, err, _ := service.GetPayEntryService().CreateCollectOrder(uid, payArg)
	fmt.Printf("%v--%v\n", data, err)



}

func TestUpPayPlatformSign(t *testing.T) {

	timestamp := "1651895932127"
	secret := "50d692a0af31b8bd71ac12d7040bac6428f4262dd347335de6cce4547214a972f45591f1916abdef9bab589f7f91e2e6"

	ext := uppay.CreditCardExt{
		FirstName:     "Travis",
		LastName:      "Pastrana",
		UserEmail:     "travis@example.com",
		UserPhone:     "+1987987987987",
		Address1:      "Muster Str. 12",
		Address2:      "130",
		City:          "Los Angeles",
		State:         "CA",
		Country:       "US",
		ZipCode:       "10178",
		DocumentType:  "****",
		DocumentValue: "****",
	}

	extBytes, err := json.Marshal(ext)
	if err != nil {
		logger.Error("UpPayService_GenData_Error | err=%v | data=%v", err, ext)
		return
	}

	data := uppay.CollectOrder{
		MerchantTradeId: "10000005101231",
		ConsumerId:      "1003",
		Currency:        "USD",
		Amount:          100, // 分
		Feature:         "CREDIT-CARD",
		TradeType:       "authorize", // PIX情况下是非必须参数
		NotifyUrl:       "https://payin-api-stage.uppaynow.com/credit/card/pay/callback/aigElectronic",
		ReturnUrl:       "https://payin-api-stage.uppaynow.com/credit/card/pay/callback/aigElectronic",
		Ext:             string(extBytes),
	}

	bts, err := json.Marshal(data)
	if err != nil {
		logger.Error("UpPayService_GenData_Error_2 | err=%v | data=%v", err, "")
		return
	}

	src := fmt.Sprintf("%v%v", string(bts), timestamp)
	sign := utils.HmacSha256ToXStringWithKey(src, secret)

	fmt.Printf("\n\n%v\n\n", string(extBytes))
	fmt.Printf("%v\n\n", src)
	fmt.Printf("%v\n\n", sign)
}

func TestUpPayPlatform(t *testing.T) {
	initConf()
	var uid int64 = 52

	//collect ----------------------
	if true {
		payArg := validator.PayEntryValidator{
			Uid:           uid,
			AppId:         "test2",
			AppOrderId: fmt.Sprintf("%v", time.Now().Unix()),
			Amount:        15,
			Email:         "pay@gmail.com",
			Phone:         "8796852374",
			UserName:      "paytest-552085",
			//DocumentType:  "CPF",
			//DocumentValue: "123456789012",
		}

		b, str := payArg.PlatFormValid()
		if !b {
			fmt.Printf("platform %v-%v\n", b, str)
			return
		}

		payEntry, err, _ := service.GetPayEntryService().CreateCollectOrder(uid, payArg)

		fmt.Printf("%v--%v\n%v\n", payEntry, err, payEntry.PaymentLinkHost)
	}

	if false {
		//orderId := "UIN16789487290463240810"
		//tradeId := "1636255685021929472"
		//orderId := "UIN1678948667045605080f"
		//tradeId := "1636255425650102272"

		// {"merchantTradeId":"UIN16800839780862952368","tradeId":"1641017291865722880",
		orderId := "UIN16808468890994001c04"
		tradeId := "1644217150932389888"
		payInstance, _, _ := service.PayAccountConfServe.GetPayInstance(uid, model.PAY_IN, 10, validator.PayEntryValidator{})
		ret, retErr := payInstance.Inquiry(orderId, tradeId)

		fmt.Printf("inquiry:%v--%v\n", ret, retErr)
	}
	// ------------------------

	if false {
		payOutArg := validator.PayOutValidator{
			Uid:           uid,
			AppOrderId:    fmt.Sprintf("%v", time.Now().Format("2006-01-02 15:04:05.999")),
			AppId:         "test2",
			UserId:        "2006-01-02",
			Amount:        50,
			Country:       model.Brazil,
			PayType:       model.PT_BANK,
			BankCard:      "99999999999",
			BankCode:      "1001",
			//BankName:      "test-bank",
			CardType:      model.CardTypePixCPF,
			Email:         "pay@gmail.com",
			Phone:         "8796852374",
			UserName:      "paytest-552085",
			//Birthday:      "1997-10-01",
			//DocumentType:  "CPF",
			DocumentValue: "12345678901",
		}

		b, str := payOutArg.PlatFormValid()
		if !b {
			fmt.Printf("platform %v-%v\n", b, str)
			return
		}
		payEntry, msg := service.GetPayOutService().PlatFromOrder(uid, payOutArg)

		fmt.Printf("%v--%v\n", payEntry, msg)
	}

	if false {
		orderId := "UOUT16808545390691211515"
		tradeId := "T2023040716022772504744"
		payInstance, _ := service.PayAccountConfServe.GetPayOutInstance(validator.PayOutValidator{Uid: uid, Amount: 100})
		ret, retErr := payInstance.InquiryPayout(orderId, tradeId)

		fmt.Printf("inquiry:%v--%v\n", ret, retErr)
	}
}

func TestHDPayOutCallback(t *testing.T) {
	/*
	   	curl --location --request POST 'https://payment.rummybuffett.com/v1/platform/collect_order' \
	      --header 'AppId: Bgrummy111' \
	      --header 'Signature: MTZiYjhkZTFhYThmZWNiNTM4ZWIxNjIzODNhNmMyOGU2NzRiYWYwZTM5NzQwNDVlNWI2NDRiOGJjNTcxY2U3Mw==' \
	      --header 'Content-Type: application/json' \
	      --data-raw '{
	          "app_order_id":"testpayu1",
	          "amount":100,
	          "phone":"7203964072",
	          "user_name":"john"
	      }'


	   	    {
	            "country": "IN",                   // string - 默认印度,取值:IN,PK,BR,MX
	            "ifsc": "testpay",                 // string - ifsc-只有印度银行卡提现需要. 注意：非印度银行卡该字段不要赋值
	            "bank_card":"14434233123123",      // string - 提现目标银行卡id/pix账号/clabe卡号，其他提现方式可以忽略参数
	            "card_type": "CPF",                // string - 巴西pix账号提现的必须参数,其他支付可以忽略不传
	                                               //        - 枚举类型，五种取值：CPF/CNPJ/PHONE/EMAIL/EVP
	            "bank_code":"9871",                // string - 银行编码-墨西哥clabe提现/巴基斯坦银行卡提现需要，其他国家忽略
	            "vpa": "xxxx",                     // string - vpa号,印度upi提现方式需要提供，其他的忽略该参数
	            "app_order_id":"test-123131asd",   // string - 调用方生成的order_id
	            "amount": 10,                      // integer - 金额
	            "user_name":"john",                // string - 用户名称
	            "phone": "999999999",              // string - 用户手机号
	            "pay_type":"bank",                 // string - 提现类型(枚举值) bank:提现到银行卡(印度、巴基斯坦地区支持)
	                                               //                        clabe:墨西哥特有的方式，入前确认
	                                               //                        PIX: 巴西特有方式
	                                               //                        upi: 仅印度地区

	            "birthday": "2000-11-06"           // 巴西出款需要，出生日
	            "document_type":"",                // 巴西出款需要，客户证件类型
	                                               //             CPF:个人税号11位数字
	                                               //             CNPJ:企业税号14位数字

	            "document_id":"",                  // 巴西出款需要,客户证件数值
	          }



	      Acc no 64186818245
	      Name DHANUSH B B
	      Ifsc no SBIN0040241
	        Phone number- 6361406697
	      Mail id- johndhanu123@gmail.com

	*/

	var (
		formData            map[string]interface{}
	)
	formData = map[string]interface{}{}
	formData["app_order_id"] = fmt.Sprintf("PureT%v", time.Now().Format("2006-01-02#15:04:05.999999999"))
	formData["user_id"] = "10001"
	formData["amount"] = 20
	formData["email"] = "sani@gmail.com"
	formData["user_name"] = "Sanih"
	formData["phone"] = "8506032349"

	formData["ifsc"] = "SBIN0040241"
	formData["bank_card"] = "64186818245123"
	formData["pay_type"] = "bank"

	postRaw, _ := json.Marshal(formData)

	srcKey := "axMfG2WcIFwyZ3XyPD9tbl3wg5HloBc79D5Bc/2PhsZ1elM0uG1y2XT+Hnx53LlgSRr+WN73AneZPtWRNCJyiiZxVtNlylOpiA3LotYHEN8="
	sk, _ := utils.AesDecryptString(srcKey)

	//sign = utils.ComputeHmacSha256(string(pData), sk)
	h := hmac.New(sha256.New, []byte(sk))
	h.Write(postRaw)
	sha := hex.EncodeToString(h.Sum(nil))
	sign := base64.StdEncoding.EncodeToString([]byte(sha))
	host := "https://payment.firstpayer.com/v1/platform/payout"

	//req, err := http.NewRequest("POST", host, bytes.NewBuffer(postRaw))
	//if err != nil {
	//	return
	//}

	//req := http.NewRequestWithContext()
	//header := new(http.Header)
	//header.Set("Content-Type", "application/json")
	//header.Set("AppId", "Bgrummy111")
	//header.Set("Signature", sign)

	fmt.Printf("%v\n%v\n%v\n", host, sign, string(postRaw))


	/*
curl --location --request POST 'https://payment.firstpayer.com/v1/platform/payout' --header 'AppId: Bgrummy111'  --header 'Content-Type: application/json' --header 'Signature: NzkwODc1OWU1NzhlYTNmNDlmMDkzY2M3Njk1ZTM3NWJmM2ZiNGMwMTAwNzQwOWZmNDFiNGE0MDU2OTg5NjkzMA==' --data-raw '{"amount":20,"app_order_id":"PureT2023-04-13#15:33:01.2097234","bank_card":"64186818245123","email":"sani@gmail.com","ifsc":"SBIN0040241","pay_type":"bank","phone":"8506032349","user_id":"10001","user_name":"Sanih"}'
	*/
}

func TestWarning(t *testing.T) {
	rand.Seed(time.Now().UnixNano())
	num := rand.Intn(99)
	fmt.Println(num)
	return
	//service.SmsWarning("FunPayChargeWarn")
	user := service.UserServ.GetUserInfo(12)
	fmt.Println(user)
}

func TestCF(t *testing.T) {
	//payInstance, _ := service.PayAccountConfServe.GetPayInstance(12, model.PAY_IN, 10)
	//payEntry, err := payInstance.CreateOrder(validator.PayEntryValidator{
	//	AppId:      "pubg",
	//	AppOrderId: "test123123",
	//	PayChannel: model.CASHFREE,
	//	Amount:     10,
	//	UserId:     "1",
	//	UserName:   "test",
	//	Email:      "test@cashfree.com",
	//	Phone:      "9000012345",
	//})
	//fmt.Println(payEntry)
	//fmt.Println(err)
}

func TestOnionPay(t *testing.T) {
	payInstance, _ := service.PayAccountConfServe.GetSpecificPayInstance(model.ONIONPAY, "onionpay4")
	//payEntry, err := payInstance.CreateOrder(validator.PayEntryValidator{
	//	AppId:      "pubg",
	//	AppOrderId: "test123123",
	//	PayChannel: model.ONIONPAY,
	//	Amount:     5000,
	//	UserId:     "1",
	//	UserName:   "test",
	//	Email:      "test@cashfree.com",
	//	Phone:      "9000012345",
	//})
	//fmt.Println(payEntry)
	//fmt.Println(err)
	//return

	//s := payInstance.InSignature(&model.OnionInCallback{
	//
	//	Sign:   "4df3a81fd418e538382d912dcddfe0194ff058d26ba10331346bee77ffd0a560",
	//	OrgStr: "processCurrency=INR&amount=5000.00&merTransNo=OPIN1635076862056001&sign=4df3a81fd418e538382d912dcddfe0194ff058d26ba10331346bee77ffd0a560&updateTime=2021-10-24 17:32:06&transNo=20211024173103698949239733010912&createTime=2021-10-24 17:31:03&processAmount=5000.00&currency=INR&merCustomize=&transStatus=success",
	//})
	//fmt.Println(s)
	//return

	payEntry, err := payInstance.PayOut(0, validator.PayOutValidator{
		AppId:      "rummy",
		AppOrderId: fmt.Sprintf("testout%v", time.Now().Unix()),
		PayType:    model.PT_BANK,
		PayChannel: model.ONIONPAY,
		Amount:     100,
		UserId:     "10050",
		UserName:   "Thaneesh V",
		BankCard:   "1769155000016710",
		Phone:      "7845286022",
		Email:      "7845286022@cashfree.com",
		IFSC:       "KVBL0001769",
		Address:    "ABC Street",
		PayTm:      "9999999999",
	})
	fmt.Println(payEntry)
	fmt.Println(err)

}

func TestInquiryOnionPay(t *testing.T) {
	payInstance, _ := service.PayAccountConfServe.GetSpecificPayInstance(model.SERPAY, "serpay1")

	payOut, err := payInstance.InquiryPayout("SPOUT1657434813015105", "")
	fmt.Println(payOut)
	fmt.Println(err)
}

func TestInquiryRZPay(t *testing.T) {
	conf := make(map[string]interface{})
	/**
	PAY_ACCOUNT_ID = "razorpay2"
	    PAY_CHANNEL = "razorpay"

	    #充值配置
	    HOST = "https://api.razorpay.com/v1/"
	    SKID = "rzp_live_F0Kj2r09Rmq7iz"
	    SK = "ZIBkWCZ7nvq321tgee15XZG3"
	    COLLECT_HOST = "http://pay.rummygulu.com/#/"

	    #提现配置
	    PAYOUTS_ACCOUNT = "4564563458670519"
	    PAYOUTS_SKID = "rzp_live_F0Kj2r09Rmq7iz"
	    PAYOUTS_SK = "ZIBkWCZ7nvq321tgee15XZG3"
	*/
	conf["PAY_ACCOUNT_ID"] = "razorpay2"
	conf["PAY_CHANNEL"] = "razorpay"
	conf["COUNTRY_CODE"] = 1
	conf["HOST"] = "https://api.razorpay.com/v1/"
	conf["SKID"] = "rzp_live_F0Kj2r09Rmq7iz"
	conf["SK"] = "ZIBkWCZ7nvq321tgee15XZG3"
	conf["COLLECT_HOST"] = "http://pay.rummygulu.com/#/"
	conf["PAYOUTS_ACCOUNT"] = "4564563458670519"
	conf["PAYOUTS_SKID"] = "rzp_live_F0Kj2r09Rmq7iz"
	conf["PAYOUTS_SK"] = "ZIBkWCZ7nvq321tgee15XZG3"
	payIns, err := channel.GetPayObj(model.RAZORPAY, conf)

	payOut, err := payIns.InquiryPayout("RZOUT16693500650265091503", "")
	fmt.Println(payOut)
	fmt.Println(err)
}

func TestInquiryYBPay(t *testing.T) {
	conf := make(map[string]interface{})
	/**
	  PAY_ACCOUNT_ID = "ybpay1"
	  PAY_CHANNEL = "ybpay"

	  MID = "21040760685101"
	  SK = "0ff1637d278d4f1059dc47c6bad82a10"
	  HOST = "https://pay.ybmerchantpay.in"

	  NOTIFY_URL = "https://payment.rummybuffett.com/callback/yb_callback"
	  RETURN_URL = ""

	  PAYOUT_URL = "https://payment.rummybuffett.com/callback/fm_payout"
	*/
	conf["PAY_ACCOUNT_ID"] = "ybpay1"
	conf["PAY_CHANNEL"] = "ybpay"
	conf["COUNTRY_CODE"] = 366
	conf["HOST"] = "https://pay.ybmerchantpay.in"
	conf["MID"] = "21040760685101"
	conf["SK"] = "0ff1637d278d4f1059dc47c6bad82a10"
	conf["NOTIFY_URL"] = "https://payment.rummybuffett.com/callback/yb_callback"
	conf["RETURN_URL"] = ""
	payIns, err := channel.GetPayObj(model.YBPAY, conf)

	payArg := validator.PayEntryValidator{
		Uid:      146,
		AppId:    "test2",
		Amount:   9000,
		Email:    "test@gmail.com",
		Phone:    "12312312131",
		UserName: "test-552085",
	}

	payOut, err := payIns.CreateOrder(payArg)
	fmt.Println(payOut)
	fmt.Println(err)
}

func TestInquiryYDGPay(t *testing.T) {
	conf := make(map[string]interface{})
	/**
	    PAY_ACCOUNT_ID = "ydgpay1"
	  PAY_CHANNEL = "ydgpay"

	  MID = "6776766213"
	  SK = "VHvZuSmtZGSFsFThyhGGmoMEeTtuW0FX"

	  HOST = "https://www.yodugame.com"
	  IN_NOTIFY_URL = "https://payment.rummybuffett.com/callback/ydg_callback"

	  OUT_NOTIFY_URL = "https://payment.rummybuffett.com/callback/ydg_payout"
	  PAYOUT_SK = "2OfXmTnU2YPmW7yv9nkPF9XzEvyN7URH"
	*/
	conf["PAY_ACCOUNT_ID"] = "ydgpay1"
	conf["PAY_CHANNEL"] = "ydgpay"
	conf["COUNTRY_CODE"] = 366
	conf["HOST"] = "https://www.yodugame.com"
	conf["MID"] = "6776766213"
	conf["SK"] = "VHvZuSmtZGSFsFThyhGGmoMEeTtuW0FX"
	conf["IN_NOTIFY_URL"] = "https://payment.rummybuffett.com/callback/ydg_callback"
	conf["OUT_NOTIFY_URL"] = "https://payment.rummybuffett.com/callback/ydg_payout"
	conf["PAYOUT_SK"] = "2OfXmTnU2YPmW7yv9nkPF9XzEvyN7URH"
	payIns, err := channel.GetPayObj(model.YDGPAY, conf)

	payArg := validator.PayEntryValidator{
		Uid:      146,
		AppId:    "test2",
		Amount:   100,
		Email:    "test@gmail.com",
		Phone:    "12312312131",
		UserName: "test-552085",
	}

	payOut, err := payIns.CreateOrder(payArg)
	fmt.Println(payOut)
	fmt.Println(err)
}

func TestInquiryYDGPayout(t *testing.T) {
	conf := make(map[string]interface{})
	/**
	    PAY_ACCOUNT_ID = "ydgpay1"
	  PAY_CHANNEL = "ydgpay"

	  MID = "6776766213"
	  SK = "VHvZuSmtZGSFsFThyhGGmoMEeTtuW0FX"

	  HOST = "https://www.yodugame.com"
	  IN_NOTIFY_URL = "https://payment.rummybuffett.com/callback/ydg_callback"

	  OUT_NOTIFY_URL = "https://payment.rummybuffett.com/callback/ydg_payout"
	  PAYOUT_SK = "2OfXmTnU2YPmW7yv9nkPF9XzEvyN7URH"
	*/
	conf["PAY_ACCOUNT_ID"] = "ydgpay1"
	conf["PAY_CHANNEL"] = "ydgpay"
	conf["COUNTRY_CODE"] = 366
	conf["HOST"] = "https://www.yodugame.com"
	conf["MID"] = "6776766213"
	conf["SK"] = "VHvZuSmtZGSFsFThyhGGmoMEeTtuW0FX"
	conf["IN_NOTIFY_URL"] = "https://payment.rummybuffett.com/callback/ydg_callback"
	conf["OUT_NOTIFY_URL"] = "https://payment.rummybuffett.com/callback/ydg_payout_callback"
	conf["PAYOUT_SK"] = "2OfXmTnU2YPmW7yv9nkPF9XzEvyN7URH"
	//payIns, err := channel.GetPayObj(model.YDGPAY, conf)
	//
	//payOut, err := payIns.PayOut(validator.PayOutValidator{
	//	AppId:      "rummy",
	//	AppOrderId: fmt.Sprintf("testout%v", time.Now().Unix()),
	//	PayType:    model.PT_BANK,
	//	PayChannel: model.YDGPAY,
	//	Amount:     10,
	//	UserId:     "10050",
	//	UserName:   "ThaneeshV",
	//	BankCard:   "1769155000016710",
	//	Phone:      "7845286022",
	//	Email:      "7845286022@cashfree.com",
	//	IFSC:       "KVBL0001769",
	//	Address:    "ABC Street",
	//	PayTm:      "9999999999",
	//})
}

func TestBHTPayout(t *testing.T) {
	conf := make(map[string]interface{})
	conf["PAY_ACCOUNT_ID"] = "bhtpay1"
	conf["PAY_CHANNEL"] = "bhtpay"
	conf["COUNTRY_CODE"] = 366
	conf["HOST"] = "https://api.bharatpays.in"
	conf["TOKEN"] = "a2475fa54c6575862b2387c7c8ca6721"

	args := validator.PayOutValidator{
		UserName: "NAMEABDURRAHAMAN",
		BankCard: "19110100017165",
		IFSC:     "BARB0KASHIA",
		Phone:    "7203964072",
		Email:    "NAMEABDURRAHAMAN@gmail.com",
		Amount:   1,
		PayType:  model.PT_BANK,
	}
	instance, _ := channel.GetPayObj(model.BHTPAY, conf)
	//_, beneId, _ := instance.AddBeneficiary(args)

	payOut, err := instance.PayOut(0, args)
	fmt.Println(payOut)
	fmt.Println(err)
}

func TestAirPayPlatform(t *testing.T) {
	initConf()
	var uid int64 = 146

	//collect ----------------------
	//payArg := validator.PayEntryValidator{
	//	Uid:      uid,
	//	AppId:    "test2",
	//	Amount:   15,
	//	Email:    "pay@gmail.com",
	//	Phone:    "12312312131",
	//	UserName: "paytest-552085",
	//}
	//payEntry, err, _ := service.GetPayEntryService().CreateCollectOrder(146, payArg)
	//
	//params := url.Values{}
	//params.Set("channel", "upi2")
	//params.Set("pay_app_id", payEntry.PayAppId)
	//params.Set("order_id", payEntry.OrderId)
	//params.Set("order_token", payEntry.PaymentOrderId)
	//params.Set("amount", fmt.Sprint(payEntry.Amount))
	//params.Set("user_name", payEntry.UserName)
	//params.Set("phone", payEntry.Phone)
	//params.Set("email", payEntry.Email)
	//params.Set("signature", payEntry.Signature)
	//params.Set("signature_new", payEntry.SignatureNew)
	//params.Set("pk", payEntry.PK)
	//params.Set("mid", payEntry.Mid)
	//u, _ := url.Parse(payEntry.PaymentLinkHost)
	//u1 := fmt.Sprintf("https://%v", strings.Replace(u.Host, "pay.", "payment.", -1))
	//params.Set("host", u1)
	//payLink := fmt.Sprintf("%v?%v", payEntry.PaymentLinkHost, params.Encode())



	//params.Set("channel", string(payEntry.PayChannel))
	//params.Set("pay_app_id", payEntry.PayAppId)
	//params.Set("order_id", payEntry.OrderId)
	//params.Set("order_token", payEntry.PaymentOrderId)
	//params.Set("amount", fmt.Sprint(payEntry.Amount))
	//params.Set("user_name", payEntry.UserName)
	//params.Set("phone", payEntry.Phone)
	//params.Set("email", payEntry.Email)
	//params.Set("signature", payEntry.Signature)
	//params.Set("signature_new", payEntry.SignatureNew)
	//params.Set("pk", payEntry.PK)
	//params.Set("mid", payEntry.Mid)
	//u, _ := url.Parse(payEntry.PaymentLinkHost)
	//u1 := fmt.Sprintf("https://%v", strings.Replace(u.Host, "pay.", "payment.", -1))
	//params.Set("host", u1)
	//payLink := fmt.Sprintf("%v?%v", payEntry.PaymentLinkHost, params.Encode())

	//fmt.Printf("%v--%v\n%v\n", payEntry, err, payLink)
	// ------------------------

	// query
	// "orderId":"1600848247447113728","orderNo":"LOIN16705069360011251e21",
	//orderId := payEntry.OrderId
	orderId := "AIN216858011330317797526"
	payInstance, _, _ := service.PayAccountConfServe.GetPayInstance(uid, model.PAY_IN, 50, validator.PayEntryValidator{})
	ret, retErr := payInstance.Inquiry(orderId, "")

	fmt.Printf("inquiry:%v--%v\n", ret, retErr)

	data2, err := payInstance.InquiryPayout("LOUT16705671660274581b83", "1601100867489738752")
	fmt.Printf("%v--%v\n", data2, err)

	// payout
	//payOutArg := validator.PayOutValidator{
	//	Uid:        uid,
	//	AppId:      "test2",
	//	Amount:     30,
	//	BankCard:   "123131231231231",
	//	IFSC:       "KVBL0001769",
	//	AppOrderId: "apporder_id-out" + time.Now().Format("2006-01-02T15:04:05Z07:00"),
	//	UserName:   "DNUB",
	//	Phone:      "8763616970",
	//}
	//
	//payOut, resKey := service.GetPayOutService().PlatFromOrder(uid, payOutArg)
	//fmt.Printf("response--->%v\n", resKey)
	//fmt.Printf("resp out--->%v\n", payOut)
}

func TestInquiryAirPay(t *testing.T) {
	conf := make(map[string]interface{})
	/**
	  MID = "271754"
	  USERNAME = "6490498"
	  PWD = "Vb3zS6zm"
	  SK = "vCZA4pY9fYnN2aBh"
	  COLLECT_HOST = "http://pay.rummyshine.com/#/"
	*/
	conf["PAY_ACCOUNT_ID"] = "airpay1"
	conf["PAY_CHANNEL"] = "airpay"
	conf["COUNTRY_CODE"] = 366
	conf["MID"] = "271754"
	conf["USERNAME"] = "6490498"
	conf["PWD"] = "Vb3zS6zm"
	conf["SK"] = "vCZA4pY9fYnN2aBh"
	conf["COLLECT_HOST"] = "http://pay.rummyshine.com/#/"
	payIns, err := channel.GetPayObj(model.AIRPAY, conf)

	payArg := validator.PayEntryValidator{
		Uid:      146,
		AppId:    "test2",
		Amount:   99.00,
		Email:    "test@gmail.com",
		Phone:    "2112232112",
		UserName: "test",
	}

	payOut, err := payIns.CreateOrder(payArg)
	fmt.Println(payOut)
	fmt.Println(err)
}

func TestCashPayPlatform(t *testing.T) {
	initConf()
	var uid int64 = 52

	payArg := validator.PayEntryValidator{
		Country: "BR",
		Uid:      uid,
		AppId:    "test2",
		Amount:   15,
		Email:    "pay@gmail.com",
		Phone:    "12312312131",
		UserName: "paytest-552085",
	}

	value := fmt.Sprintf("%v:%v", "696860738596", "sk1ei4uocvvd8nqmjrydzvhi32641lyz8r")
	authorization := fmt.Sprintf("Basic %v", base64.StdEncoding.EncodeToString([]byte(value)))
	fmt.Printf("-->%v\n", authorization)


	payEntry, _, msg := service.GetPayEntryService().CreateCollectOrder(uid, payArg)
	if payEntry == nil {
		fmt.Printf("pay entry is nil, %v", msg)
		return
	}

	fmt.Printf("order:%v\n", payEntry)
}

func TestClickNCash(t *testing.T) {
	conf := make(map[string]interface{})
	/**
	  HOST = "https://gateway.clickncash.in"
	    KEY = "RNTea8859cbefbdc669749cffdb"
	    TOKEN = "264694f434ad8bf67468c613a20cb4"

	    NOTIFY_URL = "https://payment.rummygulu.com/callback/clickncash_callback"
	    COLLECT_HOST = "http://pay.rummygulu.com/#/"
	*/
	conf["PAY_ACCOUNT_ID"] = "clickncash"
	conf["PAY_CHANNEL"] = "clickncash1"
	conf["COUNTRY_CODE"] = 356
	conf["HOST"] = "https://gateway.clickncash.in"
	conf["KEY"] = "RNTea8859cbefbdc669749cffdb"
	conf["TOKEN"] = "264694f434ad8bf67468c613a20cb4"
	conf["NOTIFY_URL"] = "https://payment.rummygulu.com/callback/clickncash_callback"
	conf["COLLECT_HOST"] = "http://pay.rummygulu.com/#/"
	payIns, err := channel.GetPayObj(model.CLICKNCASH, conf)

	payArg := validator.PayEntryValidator{
		Uid:      146,
		AppId:    "test2",
		Amount:   100,
		Email:    "test@gmail.com",
		Phone:    "2112232112",
		UserName: "test",
	}

	payOut, err := payIns.CreateOrder(payArg)
	fmt.Println(payOut)
	fmt.Println(err)
}

func TestBlackList(t *testing.T) {
	common.InitAlarmRoutine()
	initConf()
	phone := "9368856511"
	bankCard := ""
	vpa := ""
	email := ""
	ok := service.BlackListServ.CheckIsInBlack("测试", phone, bankCard, vpa, email)

	m := "normal"
	if ok {
		m = "black"
	}

	time.Sleep(time.Second * 10)
	fmt.Printf("--------\n%v-->%v-%v-%v-%v\n-----------------\n", m, phone, bankCard, vpa, email)
}

func TestPayu4(t *testing.T) {
	payu.GetPayU4().SetCallback()
}

func TestPayUOut(t *testing.T) {
	payInstance, _ := service.PayAccountConfServe.GetSpecificPayInstance(model.SERPAY, "serpay1")

	//orgNo:8211000787
	//custId:21100900002351
	//custOrderNo:SPOUT1634141373056817
	//prdOrdNo:A20211014000935408763
	//ordStatus:07
	//payAmt:10000
	//sign:7B5A4B239EB3A839C31878372217CE91
	s := payInstance.OutSignature(&model.SerOutCallback{
		OrgNo:       "8211000787",
		CustId:      "21100900002351",
		CustOrderNo: "SPOUT1634141373056817",
		PrdOrdNo:    "A20211014000935408763",
		OrdStatus:   "07",
		PayAmt:      "10000",
		Sign:        "7B5A4B239EB3A839C31878372217CE91",
		OrgStr:      "orgNo=8211000787&custId=21100900002351&custOrderNo=SPOUT1634141373056817&prdOrdNo=A20211014000935408763&ordStatus=07&payAmt=10000&sign=7B5A4B239EB3A839C31878372217CE91",
	})
	fmt.Println(s)
	return
	tt, err := payInstance.CreateOrder(validator.PayEntryValidator{
		Amount:   60,
		UserId:   "10050",
		UserName: "ThaneeshV",
		Phone:    "7845286022",
		Email:    "7845286022@cashfree.com",
	})
	fmt.Println(tt)
	fmt.Println(err)
	return
	payEntry, err := payInstance.PayOut(0, validator.PayOutValidator{
		AppId:      "rummy",
		AppOrderId: fmt.Sprintf("testout%v", time.Now().Unix()),
		PayType:    model.PT_BANK,
		PayChannel: model.PAYU,
		Amount:     10,
		UserId:     "10050",
		UserName:   "Thaneesh V",
		BankCard:   "1769155000016710",
		Phone:      "7845286022",
		Email:      "7845286022@cashfree.com",
		IFSC:       "KVBL0001769",
		Address:    "ABC Street",
		PayTm:      "9999999999",
	})
	fmt.Println(payEntry)
	fmt.Println(err)
}

func TestPaytm(t *testing.T) {
	payInstance, _ := service.PayAccountConfServe.GetSpecificPayInstance(model.PAYTM, "paytm1")
	payEntry, err := payInstance.Inquiry("", "PTMIN1620278925029063")
	fmt.Println(payEntry)
	fmt.Println(err)
}

func TestOnionPayInquiry(t *testing.T) {
	payInstance, _ := service.PayAccountConfServe.GetSpecificPayInstance(model.ONIONPAY, "onionpay5")
	payEntry, err := payInstance.Inquiry("OPIN1655127611083412", "")
	fmt.Println(payEntry)
	fmt.Println(err)
}

func TestCFOut(t *testing.T) {
	payInstance, _ := service.PayAccountConfServe.GetSpecificPayInstance(model.FIDYPAY, "fidypay1")
	payEntry, err := payInstance.PayOut(0, validator.PayOutValidator{
		AppId:      "rummy",
		AppOrderId: fmt.Sprintf("testout%v", time.Now().Unix()),
		PayType:    model.PT_BANK,
		PayChannel: model.FIDYPAY,
		Amount:     10,
		UserId:     "10050",
		UserName:   "ThaneeshV",
		BankCard:   "881005554065",
		Phone:      "7845286022",
		Email:      "7845286022@cashfree.com",
		IFSC:       "DBSS0IN0811",
		Address:    "ABC Street",
		PayTm:      "9999999999",
	})
	fmt.Println(payEntry)
	fmt.Println(err)
}

func TestEaseBuzz(t *testing.T) {
	payInstance, _ := service.PayAccountConfServe.GetSpecificPayInstance(model.EASEBUZZ, "easebuzz1")
	data, err := payInstance.InquiryPayout("EBOUT1619017961061516", "")
	fmt.Println(data)
	fmt.Println(err)
}

func TestCron(t *testing.T) {
	crontab.PayoutStatus()
}

func TestRedis(t *testing.T) {
	//service.CollectSettlement()
	return
	boostrap.InitConf()
	dao.RedisSet("test", "123213", 100)

	fmt.Println(dao.RedisGet("test"))
}

func TestAccountService_AddCollect(t *testing.T) {
	_ = service.AccountServ.AddCollect(&model.PayEntry{
		Id:           662,
		Uid:          10,
		PayChannel:   model.PAYU,
		PayAccountId: "payu1",
		Amount:       500,
		Status:       model.PAYSUCCESS,
	})
}

func TestAccountService_CallBackPayOut(t *testing.T) {
	_ = service.AccountServ.CallBackPayOut(&model.PayOut{
		Id:           68,
		Uid:          10,
		PayChannel:   model.PAYU,
		PayAccountId: "payu1",
		Amount:       1000,
		Status:       model.PAY_OUT_SUCCESS,
	})
}

func TestUPI(t *testing.T) {
	payIns, _ := service.PayAccountConfServe.GetSpecificUPIPayInstance(model.FIDYPAY, "fidypay1")
	data := payIns.VerifyVPA("7845286022@paytm")
	fmt.Println(data)
}

func TestUPIPay(t *testing.T) {
	payIns, _ := service.PayAccountConfServe.GetSpecificUPIPayInstance(model.FIDYPAY, "fidypay1")
	data := payIns.DoPay("test123123124", 100, "7845286022@paytm")
	fmt.Println(data)
}

func TestUPIPayStatus(t *testing.T) {
	payIns, _ := service.PayAccountConfServe.GetSpecificUPIPayInstance(model.FIDYPAY, "fidypay1")
	data := payIns.GetPayStatus("test123123124")
	fmt.Println(data)
}

func TestFidyPayOut(t *testing.T) {
	payIns, _ := service.PayAccountConfServe.GetSpecificPayInstance(model.FIDYPAY, "fidypay1")
	data, _ := payIns.PayOut(0, validator.PayOutValidator{
		AppId:      "rummy",
		AppOrderId: fmt.Sprintf("testout%v", time.Now().Unix()),
		PayType:    model.PT_BANK,
		PayChannel: model.FIDYPAY,
		Amount:     100,
		UserId:     "10050",
		UserName:   "ThaneeshV",
		BankCard:   "0537301000121914",
		Phone:      "7845286022",
		Email:      "7845286022@cashfree.com",
		IFSC:       "LAVB0000537",
		Address:    "ABC Street",
		PayTm:      "9999999999",
	})
	fmt.Println(data)
}

// globpay india
func TestGlobPayEntryService_CreateCollectOrder(t *testing.T) {
	boostrap.InitConf()
	payArg := validator.PayEntryValidator{
		Uid:      55,
		AppId:    "glob_pay_test",
		Amount:   10000,
		Email:    "test-globpay@gmail.com",
		Phone:    "12312312131",
		UserName: "test-globpay",
	}
	fmt.Println(111)
	data, err, _ := service.GetPayEntryService().CreateCollectOrder(55, payArg)
	fmt.Println(data)
	fmt.Println(err)
}

func TestGlobPayOutPlatFromOrder(t *testing.T) {
	boostrap.InitConf()
	payArg := validator.PayOutValidator{
		Uid:           55,
		AppOrderId:    "test-globpay-appid-01",
		AppId:         "glob_pay_test",
		Amount:        100,
		UserName:      "Thaneesh V",
		Account:       "50284414727",
		AccountType:   "CPF",
		DocumentValue: "50284414727",
		//DocumentType:  "CPF",
		UserId:        "11001",
		BankCard:      "0537301000121914",
		Phone:         "9999999999",
		Email:         "test@cashfree.com",
		IFSC:          "LAVB0000537",
		Address:       "ABC Street",
		PayTm:         "9999999999",
	}
	data, err := service.GetPayOutService().PlatFromOrder(55, payArg)
	fmt.Println(data)
	fmt.Println(err)
}

//
func TestGlobInquiry(t *testing.T) {
	payInstance, _ := service.PayAccountConfServe.GetSpecificPayInstance(model.GLOBPAY, "globpay_india_test")
	data, err := payInstance.InquiryPayout("GPOUT1646814925077183", "")
	fmt.Println(data)
	fmt.Println(err)
}

func TestQuerySettlementCount(t *testing.T) {
	fmt.Println(service.QuerySettlementCount("onionpay5"))
}
