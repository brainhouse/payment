package controller

import (
	"encoding/json"
	"funzone_pay/app/service"
	"funzone_pay/model"
	"github.com/gin-gonic/gin"
	"github.com/wonderivan/logger"
)

func OrderCallback(c *gin.Context) {
	//data, error := c.Request.GetBody()
	//data
	rawData, _ := c.GetRawData()
	webHook := new(model.CallBackWebHook)
	err := json.Unmarshal(rawData, webHook)
	if err != nil {
		logger.Error("OrderCallback_JsonUnmarshal_Error | err=%v", err)
		c.JSON(400, &map[string]string{"msg": "json error"})
		return
	}
	logger.Debug("OrderCallback_Data json=%v", string(rawData))
	service.GetPayEntryService().WebHookCallback(webHook)
	c.JSON(200, &map[string]string{"msg": "success"})
}

func RZPayOutCallback(c *gin.Context) {
	rawData, _ := c.GetRawData()
	logger.Debug("RZPayOutCallback_Data json=%v", string(rawData))
	webHook := new(model.PayoutCallBackWebHook)
	err := json.Unmarshal(rawData, webHook)
	if err != nil {
		logger.Error("RZPayOutCallback_JsonUnmarshal_Error | err=%v", err)
		c.JSON(400, &map[string]string{"msg": "json error"})
		return
	}
	service.GetPayOutService().RazorPayOutCallback(webHook)
	c.JSON(200, &map[string]string{"msg": "success"})
}
