package controller

import (
	"funzone_pay/app/service"
	"github.com/gin-gonic/gin"
)

func Index(c *gin.Context) {
	c.JSON(200, &map[string]string{"test": "1"})
}

/**
 * 创建充值订单
 * param: *gin.Context c
 */

// Deprecated: CreateInOrder
func CreateInOrder(c *gin.Context) {
	//payEntryArg := new(validator.PayEntryValidator)
	//jsonData, _ := c.GetRawData()
	//err := json.Unmarshal(jsonData, payEntryArg)
	//if err != nil {
	//	logger.Error("CreateInOrder_Json_Error | err=%v | data=%v", err, string(jsonData))
	//	c.JSON(http.StatusBadRequest, gin.H{"error": "json Unmarshal Error"})
	//	return
	//}
	//if vs, msg := payEntryArg.Valid(); !vs {
	//	c.JSON(http.StatusBadRequest, gin.H{"error": msg})
	//	return
	//}
	//orderData := make(map[string]interface{})
	//var (
	//	payEntry *model.PayEntry
	//	resKey   string
	//)
	//payEntry, resKey = service.GetPayEntryService().NewCreateInOrder(*payEntryArg)
	////if payEntryArg.AppId == "rummy_josh" {
	////	//内部应用走新的方法
	////	payEntry, resKey = service.GetPayEntryService().NewCreateInOrder(*payEntryArg)
	////} else {
	////	//老的rummybank逻辑
	////	payEntry, resKey = service.GetPayEntryService().CreateInOrder(*payEntryArg)
	////}
	//if resKey != "" {
	//	orderData["code"] = "500"
	//	orderData["msg"] = resKey
	//	c.JSON(500, orderData)
	//	return
	//}
	//
	//var payLink string
	//params := url.Values{}
	//if payEntry.PayChannel == model.EAGLEPAY || payEntry.PayChannel == model.SERPAY || payEntry.PayChannel == model.PAGSMILE ||
	//	payEntry.PayChannel == model.GLOBPAY || payEntry.PayChannel == model.PAYFLASH || payEntry.PayChannel == model.ZWPAY ||
	//	payEntry.PayChannel == model.FMPAY || payEntry.PayChannel == model.LOOGPAY || payEntry.PayChannel == model.OEPAY ||
	//	payEntry.PayChannel == model.UPPAY {
	//	payLink = payEntry.PaymentLinkHost
	//} else {
	//	token := payEntry.PaymentOrderId
	//	params.Set("channel", string(payEntry.PayChannel))
	//	params.Set("pay_app_id", payEntry.PayAppId)
	//	params.Set("order_id", payEntry.OrderId)
	//	params.Set("order_token", token)
	//	params.Set("amount", fmt.Sprint(payEntry.Amount))
	//	params.Set("user_name", payEntry.UserName)
	//	params.Set("phone", payEntry.Phone)
	//	params.Set("email", payEntry.Email)
	//	params.Set("signature", payEntry.Signature)
	//	params.Set("signature_new", payEntry.SignatureNew)
	//	payLink = fmt.Sprintf("%v?%v", payEntry.PaymentLinkHost, params.Encode())
	//}
	//payEntry.PaymentOrderId = payLink
	//
	//orderData["code"] = "200"
	//orderData["data"] = payEntry
	//
	//buffer := &bytes.Buffer{}
	//encoder := json.NewEncoder(buffer)
	//encoder.SetEscapeHTML(false)
	//_ = encoder.Encode(orderData)
	//c.Data(200, "application/json", buffer.Bytes())

	return
}

// Deprecated: GetOrderStatus
func GetOrderStatus(c *gin.Context) {
	//paymentOrderId := c.PostForm("payment_order_id")
	//paymentId := c.PostForm("payment_id")
	//sign := c.PostForm("signature")
	//if paymentOrderId == "" {
	//	c.JSON(http.StatusBadRequest, gin.H{"error": "payment_order_id is empty!"})
	//	return
	//}
	//if sign == "" {
	//	c.JSON(http.StatusBadRequest, gin.H{"error": "payment_order_id is empty!"})
	//	return
	//}
	//if paymentOrderId == "" {
	//	c.JSON(http.StatusBadRequest, gin.H{"error": "payment_order_id is empty!"})
	//	return
	//}
	//orderData := make(map[string]interface{})
	//status, resKey := service.GetPayEntryService().GetPayOrderStatus(paymentId, paymentOrderId)
	//if resKey != "" {
	//	orderData["code"] = "500"
	//	orderData["msg"] = resKey
	//	c.JSON(500, orderData)
	//	return
	//}
	//orderData["code"] = "200"
	//orderData["msg"] = "success"
	//orderData["data"] = map[string]interface{}{
	//	"status":           status,
	//	"payment_order_id": paymentOrderId,
	//	"payment_id":       paymentId,
	//}
	//c.JSON(500, orderData)
	return
}

func GetOrderStatus2(c *gin.Context) {
	orderId := c.Query("order")
	if orderId == "" {
		c.JSON(400, map[string]interface{}{"code": 400, "message": "invalid request"})
		return
	}

	status, message := service.GetPayEntryService().GetPayOrderStatus2(orderId)
	if message != "" {
		c.JSON(500, map[string]interface{}{"code": 500, "status": 0}) // pending
		return
	}

	c.JSON(200, map[string]interface{}{"code":200, "status": status})
}
/**
 * 创建提现订单
 * param: *gin.Context c
 */

// Deprecated: CreatePayOutOrder
func CreatePayOutOrder(c *gin.Context) {
	//var payOutArg = new(validator.PayOutValidator)
	//jsonData, _ := c.GetRawData()
	//err := json.Unmarshal(jsonData, payOutArg)
	//if err != nil {
	//	logger.Error("CreateOutOrder_Json_Error | err=%v | data=%v", err, string(jsonData))
	//	c.JSON(http.StatusBadRequest, gin.H{"error": "json Unmarshal Error"})
	//	return
	//}
	//if vs, msg := payOutArg.Valid(); !vs {
	//	c.JSON(http.StatusBadRequest, gin.H{"error": msg})
	//	return
	//}
	//orderData := make(map[string]interface{})
	//
	//payOut, resKey := service.GetPayOutService().NewCreateOutOrder(*payOutArg)
	////payOut, resKey := service.GetPayOutService().CreateOutOrder(*payOutArg)
	//if resKey != "" {
	//	orderData["code"] = "500"
	//	orderData["msg"] = resKey
	//	c.JSON(500, orderData)
	//	return
	//}
	//orderData["code"] = "200"
	//orderData["data"] = payOut
	//c.JSON(200, orderData)
	return
}
