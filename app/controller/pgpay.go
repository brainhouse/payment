package controller

import (
	"encoding/json"
	"fmt"
	"funzone_pay/app/service"
	"funzone_pay/common"
	"funzone_pay/model"
	"github.com/gin-gonic/gin"
	"github.com/wonderivan/logger"
)

func PgPayCallback(c *gin.Context) {
	rawData, _ := c.GetRawData()
	/**
	YOURCALLBACKURL?OrderId=202301040008&txn_id=RPAYXXXXXXXXXXb6b99&Message=success&Statu
	s=success&Amount=10.00&Mode=UPI&ProviderTxnId=16525509955&bank_ref_num=300XXXXXXX898
	&PG_TYPE=UPI-PG&bankcode=UPI&payeeUpi=maXXXXXXXXXX11-
	2@okicici&payeeName=MXXXXXXXXXXai
	*/
	logger.Debug("PgPayCallback_Data data=%v", string(rawData))
	collectData := new(model.PGPayCollect)
	err := json.Unmarshal(rawData, collectData)
	if err != nil {
		logger.Error("PgPayCallback_Data_Error | err=%v | data=%v", err, rawData)
		c.JSON(400, &map[string]string{"msg": "parse data error"})
		return
	}
	service.GetPayEntryService().PGPayCallback(collectData)
	c.String(200, "success")
}

func BHTPayOutCallback(c *gin.Context) {
	rawData, _ := c.GetRawData()

	logger.Debug("BHTPayOutCallback_Data data=%v", string(rawData))
	collectData := new(model.BHTPayout)
	err := json.Unmarshal(rawData, collectData)
	if err != nil {
		logger.Error("BHTPayOutCallback_Data_Error | err=%v | data=%v", err, rawData)
		c.JSON(400, &map[string]string{"msg": "parse data error"})
		return
	}
	service.GetPayOutService().BHTPayOutCallback(collectData)
	c.String(200, "success")
}

func OEPayOutCallback(c *gin.Context) {
	rawData, _ := c.GetRawData()

	logger.Debug("OEPayOutCallback_Data data=%v", string(rawData))
	collectData := new(model.OEPayout)

	success := false
	resKey := ""
	defer func() {
		if !success {
			common.PushAlarmEvent(common.Warning, "no",
				fmt.Sprintf("OEPay/OEPayOutCallback/%v", collectData.OrderNo), "",
				fmt.Sprintf("Payout[%v] Order Error %v, Please check...", resKey, collectData.OrderNo))
		}
	}()

	err := json.Unmarshal(rawData, collectData)
	if err != nil {
		logger.Error("OEPayOutCallback_Data_Error | err=%v | data=%v", err, rawData)
		c.JSON(400, &map[string]string{"msg": "parse data error"})
		return
	}
	resKey = service.GetPayOutService().OEPayOutCallback(collectData)

	if resKey != "" {
		c.String(404, "failed")
	} else {
		success = true
		c.String(200, "success")
	}

}

/*
{
	"event":"TRANSFER_STATUS_UPDATE",
	"status":"SUCCESS",
	"data":{
		"payout_id":"H11092304141240208848912",
		"amount":"100.00",
		"remarks":"payout","created_at":"2023-04-14T12:40:20",
		"payment_mode":"IMPS","transfer_date":"Null","beneficiary_bank_name":"ptm bank",
		"beneficiary_account_ifsc":"SBIN0040241","beneficiary_account_name":"DHANUSH B B",
		"beneficiary_account_number":"64186818245","beneficiary_upi_handle":null,
		"UTR":"310418502377",
		"reference":"HOUT168147602003674131b0",
		"message":"Credited to beneficiary Mr  DHANUSH  B B on 14-04-2023 18:10:52",
		"checksum":"6a97e5306042adddc90e53c4fafba2a68067ca946fcc3e1682d13ce352566824"
	}
}
*/

func HDPayOutCallback(c *gin.Context) {
	rawData, _ := c.GetRawData()

	logger.Debug("HDPayOutCallback_Data data=%v", string(rawData))
	collectData := new(model.HDPayout)
	err := json.Unmarshal(rawData, collectData)
	if err != nil {
		logger.Error("HDPayOutCallback_Data_Error | err=%v | data=%v", err, rawData)
		c.JSON(400, &map[string]string{"msg": "parse data error"})
		return
	}

	success := false
	ret := ""
	defer func() {
		if !success {
			common.PushAlarmEvent(common.Warning, "no",
				fmt.Sprintf("HDPay/HDPayOutCallback/%v", collectData.Data.Reference), "",
				fmt.Sprintf("Payout[%v] Order Callback Error %v, Please check...", ret, collectData.Data.Reference))
		}
	}()

	ret = service.GetPayOutService().HDPayOutCallback(collectData)
	if ret != "" {
		logger.Error("PayOutService_HDPayOutCallback_result|%v|%v", ret, collectData.Data.Reference)
		return
	}

	success = true
	c.JSON(200, map[string]interface{}{"status": "success", "message": "data received"})
}
