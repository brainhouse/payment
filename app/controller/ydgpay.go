package controller

import (
	"encoding/json"
	"funzone_pay/app/service"
	"funzone_pay/model"
	"github.com/gin-gonic/gin"
	"github.com/wonderivan/logger"
)

func YDGCallback(c *gin.Context) {
	rawData, _ := c.GetRawData()
	webHook := new(model.YDGCollectCallback)
	err := json.Unmarshal(rawData, webHook)
	if err != nil {
		logger.Error("YDGCallback_JsonUnmarshal_Error | err=%v", err)
		c.JSON(400, &map[string]string{"msg": "json error"})
		return
	}
	logger.Debug("YDGCallback_Data json=%v", string(rawData))
	service.GetPayEntryService().YDGInCallback(webHook)
	c.String(200, "success")
}

func YDGPayOutCallback(c *gin.Context) {
	rawData, _ := c.GetRawData()
	logger.Debug("YDGPayOutCallback_Data json=%v", string(rawData))
	webHook := new(model.YDGPayoutCallback)
	err := json.Unmarshal(rawData, webHook)
	if err != nil {
		logger.Error("YDGPayOutCallback_JsonUnmarshal_Error | err=%v", err)
		c.JSON(400, &map[string]string{"msg": "json error"})
		return
	}
	service.GetPayOutService().YDGPayOutCallback(webHook)
	c.String(200, "success")
}
