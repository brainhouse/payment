package controller

import (
	"funzone_pay/app/service"
	"funzone_pay/channel/payflash"
	"funzone_pay/model"
	"github.com/gin-gonic/gin"
	"github.com/wonderivan/logger"
	"net/http"
	"net/url"
)

func PayFlashCollectCallback(c *gin.Context) {
	//order_id=PFIN1662373818046966204b
	//&amount=11.25
	//&currency=INR&description=no+description
	//&name=Test&email=778983212%40gmail.com&phone=778983212&address_line_1=&address_line_2=&city=Bangalore&state=&country=India&zip_code=560001
	//&udf1=jazzcash_pk
	//&udf2=&udf3=&udf4=&udf5=
	//&transaction_id=IOUPII89729492632&payment_mode=UPI
	//&payment_channel=Unified+Payments+Intent+Interface&payment_datetime=2022-09-05+16%3A03%3A07&response_code=0
	//&response_message=Transaction+successful&error_desc=
	//&cardmasked=
	//&hash=2DD01FF4B03E64C6D7BD07764CCAE24353FD9329AC8B187D2D6C56C735FD00AA97477393412C68AB932BE5EC572D5B89B925B63A518FB4144E64AFCAF85394B7
	rawData, err := c.GetRawData()
	if err != nil {
		logger.Error("PayFlashCollect get raw data error %v", err)
	}
	logger.Debug("PayFlashCollectCallback_Data | data=%v | size %v| header=%v", string(rawData), len(rawData), c.Request.Header)

	vs, err := url.ParseQuery(string(rawData))
	if err != nil {
		logger.Error("PayFlashCollect ParseQuery Error %v", err)
	}

	orderId := vs.Get(payflash.FieldOrderId)   // 订单号
	payAccountId := vs.Get(payflash.FieldUDF1) // extra

	ins, err := service.PayAccountConfServe.GetSpecificPayInstance(model.PAYFLASH, payAccountId)
	if err != nil {
		logger.Error("PayFlashCollectCallback_Invalid PayAccountId=%v | error=%v ", payAccountId, err)
		c.JSON(http.StatusBadRequest, "fail")
		return
	}

	data := make(map[string]string)
	for key, _ := range vs {
		data[key] = vs.Get(key)
	}

	if !ins.InSignature(data) {
		logger.Error("PayFlashCollectCallback_Signature Error")
		c.JSON(http.StatusBadRequest, map[string]interface{}{"msg": "invalid sign"})
		return
	}

	cb := model.PayFlashNotify{
		TransactionId:   vs.Get(payflash.FieldTransactionId),
		PaymentMode:     vs.Get(payflash.FieldPaymentMode),
		PaymentChannel:  vs.Get(payflash.FieldPaymentChannel),
		ResponseCode:    vs.Get(payflash.FieldResponseCode),
		ResponseMessage: vs.Get(payflash.FieldResponseMessage),
		OrderId:         orderId,
		Amount:          vs.Get(payflash.FieldAmount),
	}

	service.GetPayEntryService().PayFlashInCallback(cb)
	c.JSON(http.StatusOK, map[string]interface{}{"msg": "success"})
}

func PayFlashPayOutCallback(c *gin.Context) {
	/*
		transaction_reference_number=
		&status=SUCCESS
		&error_message=
		&bank_reference_number=224915646732
		&merchant_reference_number=PFOUT16624581230409110b31
		&api_key=97549c3a-ba4e-44c3-8c0a-d29d848b056a
		&transfer_date=2022-09-06+15%3A25%3A25
		&transfer_amount=5.00
		&hash=5F134B945FFB5E9ED64308EB20BB831410D82434BAF4353ECEF1389706BF088AF25C6C66D98F05840F85C425E04908545EF63819EB7E3C795CF19DE23C34DB24
	*/
	rawData, err := c.GetRawData()
	if err != nil {
		logger.Error("PayFlashPayout get raw data error %v", err)
	}
	logger.Debug("PayFlashPayoutCallback_Data | data=%v | size %v| header=%v", string(rawData), len(rawData), c.Request.Header)

	vs, err := url.ParseQuery(string(rawData))
	if err != nil {
		logger.Error("PayFlashPayoutCallback ParseQuery Error %v", err)
	}

	ret := service.GetPayOutService().PayFlashOutCallback(vs)
	if len(ret) < 1 {
		c.JSON(200, "success")
		return
	}

	c.JSON(http.StatusInternalServerError, ret)
}
