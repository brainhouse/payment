package controller

import (
	"fmt"
	"funzone_pay/app/service"
	"github.com/gin-gonic/gin"
	"github.com/wonderivan/logger"
	"net/url"
	"strings"
	"time"
)

func RetryCollectCallback(c *gin.Context) {
	uidInterface, _ := c.Get("UID")
	uid, ok := uidInterface.(int64)
	if !ok || uid == 0 {
		logger.Error("RetryCollectCallback_Uid_Error | err=%v", uid)
		c.JSON(400, &map[string]string{"code": "400", "msg": "illegal user"})
		return
	}
	order_id := c.Query("order_id")
	if order_id == "" {
		logger.Error("RetryCollectCallback_OrderId_Error | order_id=%v", order_id)
		c.JSON(400, &map[string]string{"code": "400", "msg": "order_id is empty"})
		return
	}

	err := service.GetPayEntryService().RetryPlatFromCallback(uid, order_id)
	if err != nil {
		logger.Error("RetryCollectCallback_Error | order_id=%v | err=%v", order_id, err)
	}
	c.JSON(200, &map[string]string{"code": "200", "msg": "success"})
}

func RetryPayoutCallback(c *gin.Context) {
	uidInterface, _ := c.Get("UID")
	uid, ok := uidInterface.(int64)
	if !ok || uid == 0 {
		logger.Error("RetryPayoutCallback_Uid_Error | err=%v", uid)
		c.JSON(400, &map[string]string{"code": "400", "msg": "illegal user"})
		return
	}
	order_id := c.Query("order_id")
	if order_id == "" {
		logger.Error("RetryPayoutCallback_OrderId_Error | order_id=%v", order_id)
		c.JSON(400, &map[string]string{"code": "400", "msg": "order_id is empty"})
		return
	}

	err := service.GetPayOutService().RetryPlatFormCallback(uid, order_id)
	if err != nil {
		logger.Error("RetryPayoutCallback_Error | order_id=%v", order_id)
	}
	c.JSON(200, &map[string]string{"code": "200", "msg": "success"})
}

func SuccessCallback(c *gin.Context) {
	param, _ := url.ParseQuery(c.Request.URL.RawQuery)
	ch := param.Get("ch")
	f := GetFunc(PayChannel(ch))

	var raw []byte
	if c.Request.Method == "POST" {
		raw, _ = c.GetRawData()
	}

	result, orderId, message := f(raw, param)

	orderHost := url.QueryEscape(fmt.Sprintf("https://%v", c.Request.Host))

	logger.Debug("SuccessCallback_data |%v|%v| message=%v | method=%v | data=%v | status=%v | host=%v | query=%v",
		orderHost, orderId, message, c.Request.Method, string(raw), result, c.Request.Host, c.Request.URL.RawQuery)

	host := strings.Replace(c.Request.Host, "payment.", "", -1)

	if ch == "phonepay" {
		//c.Redirect(301, fmt.Sprintf("http://pay.%v/#/callback_?type=3&link=%v&order=%v", host, orderHost, orderId))
		//return
		time.Sleep(time.Second * 2) // 延迟
	}

	switch result {
	case 1:
		c.Redirect(301, fmt.Sprintf("http://pay.%v/#/callback_?type=1&link=%v&order=%v", host, orderHost, orderId))

	case -1:
		c.Redirect(301, fmt.Sprintf("http://pay.%v/#/callback_?type=2&link=%v&order=%v", host, orderHost, orderId))

	default:
		c.Redirect(301, fmt.Sprintf("http://pay.%v/#/callback_?type=3&link=%v&order=%v", host, orderHost, orderId))
	}
}

func FailedCallback(c *gin.Context) {
	host := strings.Replace(c.Request.Host, "payment.", "", -1)
	c.Redirect(301, fmt.Sprintf("http://pay.%v/#/callback_?type=2", host))
}
