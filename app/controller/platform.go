package controller

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"funzone_pay/app/middlewares"
	"funzone_pay/app/service"
	"funzone_pay/app/validator"
	"funzone_pay/centerlog"
	"funzone_pay/dao"
	"funzone_pay/errdef"
	"funzone_pay/model"
	"net/http"
	"net/url"
	"strings"

	"github.com/gin-gonic/gin"
	"github.com/wonderivan/logger"
)

/**
 * 创建充值订单
 * param: *gin.Context c
 */
func CollectOrder(c *gin.Context) {
	payEntryArg := new(validator.PayEntryValidator)
	jsonData, _ := c.Get(middlewares.ContextDataKey)
	err := json.Unmarshal(jsonData.([]byte), payEntryArg)
	if err != nil {
		logger.Error("CreateInOrder_Json_Error | err=%v | data=%v", err, string(jsonData.([]byte)))
		c.JSON(http.StatusBadRequest, gin.H{"code": "400", "error": "json Unmarshal Error"})
		return
	}
	appId, _ := c.Get("APP_ID")
	uid, _ := c.Get("UID")
	payEntryArg.Uid = uid.(int64)
	payEntryArg.AppId = appId.(string)
	if vs, msg := payEntryArg.PlatFormValid(); !vs {
		c.JSON(http.StatusBadRequest, gin.H{"code": "400", "error": msg})
		return
	}

	if service.BlackListServ.CheckIsInBlack(payEntryArg.AppId, payEntryArg.Phone, "", "", payEntryArg.Email) {
		logger.Error("BlackListService_IsBlack | appid= %v | uid=%v | %v", payEntryArg.AppId, payEntryArg.Uid, payEntryArg)
		c.JSON(http.StatusBadRequest, gin.H{"code": "400", "error": "ACCESS_FORBID"})
		return
	}

	orderData := make(map[string]interface{})
	payEntry, resCode, resMsg := service.GetPayEntryService().CreateCollectOrder(uid.(int64), *payEntryArg)
	if resCode != errdef.CodeOK {
		orderData["code"] = fmt.Sprintf("%d", resCode)
		orderData["msg"] = resMsg
		c.JSON(http.StatusInternalServerError, orderData)

		var clog map[string]interface{}
		if payEntry != nil {
			clog = map[string]interface{}{
				centerlog.FieldStatus:     resCode,
				centerlog.FieldStatusDesc: resMsg,
				centerlog.FieldAppId:      payEntryArg.AppId,
				centerlog.FieldAccountId:  payEntry.PayAccountId,
				centerlog.FieldOrderId:    payEntry.OrderId,
				centerlog.FieldAppOrderId: payEntry.AppOrderId,
				centerlog.FieldAmount:     payEntry.Amount,
				centerlog.FieldThirdDesc:  payEntry.ThirdDesc,
			}
		} else {
			clog = map[string]interface{}{
				centerlog.FieldStatus:     resCode,
				centerlog.FieldStatusDesc: resMsg,
				centerlog.FieldAppId:      payEntryArg.AppId,
				centerlog.FieldAppOrderId: payEntryArg.AppOrderId,
			}
		}
		centerlog.Info(centerlog.MsgPayInCreateFail, clog)
		return
	}
	var payLink string
	params := url.Values{}
	if payEntry.PayChannel == model.EAGLEPAY || payEntry.PayChannel == model.SERPAY || payEntry.PayChannel == model.PAGSMILE ||
		payEntry.PayChannel == model.GLOBPAY || payEntry.PayChannel == model.PAYFLASH || payEntry.PayChannel == model.ZWPAY ||
		payEntry.PayChannel == model.FMPAY || payEntry.PayChannel == model.YBPAY || payEntry.PayChannel == model.LOOGPAY ||
		payEntry.PayChannel == model.YDGPAY || payEntry.PayChannel == model.BHTPAY || payEntry.PayChannel == model.OEPAY ||
		payEntry.PayChannel == model.UPPAY || payEntry.PayChannel == model.OPAY || payEntry.PayChannel == model.PhonePay ||
		payEntry.PayChannel == model.HOPEPAY ||
		(payEntry.PayChannel == model.CASHPAY && payEntry.PayAccountId != model.CashPayAccountIDCustomizeH5) {
		payLink = payEntry.PaymentLinkHost
	} else {
		appID := payEntry.PayAppId
		orderID := payEntry.OrderId
		orderToken := payEntry.PaymentOrderId
		userName := payEntry.UserName
		signature := payEntry.Signature
		signatureNew := payEntry.SignatureNew

		channel := string(payEntry.PayChannel)
		if channel == string(model.AIRPAY) { //&& payEntryArg.AppId != airpay.AirPaySDKAccount { // 普通air pay会走h5跳转黑科技
			channel = "upi2"
			params.Set("pk", payEntry.PK)
			params.Set("mid", payEntry.Mid)
		} else if channel == string(model.CASHPAY) {
			channel = "pix"
			orderID = payEntry.AppOrderId
			orderToken = ""
		}

		params.Set("channel", channel)
		params.Set("pay_app_id", appID)
		params.Set("order_id", orderID)
		params.Set("order_token", orderToken)
		params.Set("amount", fmt.Sprint(payEntry.Amount))
		params.Set("user_name", userName)
		params.Set("phone", payEntry.Phone)
		params.Set("email", payEntry.Email)
		params.Set("signature", signature)
		params.Set("signature_new", signatureNew)

		u, _ := url.Parse(payEntry.PaymentLinkHost)
		u1 := fmt.Sprintf("https://%v", strings.Replace(u.Host, "pay.", "payment.", -1))
		params.Set("host", u1)
		payLink = fmt.Sprintf("%v?%v", payEntry.PaymentLinkHost, params.Encode())
	}

	if payEntry.PayChannel == model.PhonePay {
		payLink = fmt.Sprintf("%v?target=%v", "https://pay.rummyshine.com/static/ppe.html", base64.StdEncoding.EncodeToString([]byte(payEntry.PaymentLinkHost)))
	}

	orderData["code"] = "200"
	orderData["data"] = &model.PlatCollectData{
		AppOrderId:  payEntry.AppOrderId,
		OrderId:     payEntry.OrderId,
		Amount:      payEntry.Amount,
		PaymentLink: payLink,
	}
	buffer := &bytes.Buffer{}
	encoder := json.NewEncoder(buffer)
	encoder.SetEscapeHTML(false)
	_ = encoder.Encode(orderData)
	c.Data(200, "application/json", buffer.Bytes())
	return
}

/**
 * 创建提现订单
 * param: *gin.Context c
 */
func PayOutOrder(c *gin.Context) {
	var payOutArg = new(validator.PayOutValidator)
	jsonData, _ := c.Get(middlewares.ContextDataKey)
	err := json.Unmarshal(jsonData.([]byte), payOutArg)
	if err != nil {
		logger.Error("CreatePayOutOrder_Json_Error | err=%v | data=%v", err, string(jsonData.([]byte)))
		c.JSON(http.StatusBadRequest, gin.H{"code": "400", "error": "json Unmarshal Error", "msg": "json Unmarshal Error"})
		return
	}
	if vs, msg := payOutArg.PlatFormValid(); !vs {
		c.JSON(http.StatusBadRequest, gin.H{"code": "400", "error": msg, "msg": msg})
		return
	}
	appId, _ := c.Get("APP_ID")
	uid, _ := c.Get("UID")
	payOutArg.Uid = uid.(int64)
	payOutArg.AppId = appId.(string)

	if service.BlackListServ.CheckIsInBlack(payOutArg.AppId, payOutArg.Phone, payOutArg.BankCard, payOutArg.VPA, payOutArg.Email) {
		logger.Error("BlackListService_IsBlack | appid= %v | %v", payOutArg.AppId, payOutArg)
		c.JSON(http.StatusBadRequest, gin.H{"code": "400", "error": "ACCESS_FORBID", "msg": "ACCESS_FORBID"})
		return
	}

	resCode := 0
	orderData := make(map[string]interface{})
	payOut, resKey := service.GetPayOutService().PlatFromOrder(uid.(int64), *payOutArg)
	if resKey == "balance_insufficient" || resKey == "AMOUNT_TOO_SMALL" {
		resCode = 400
	} else if resKey != "" {
		resCode = 500
	}

	// != 0 表示错误
	if resCode != 0 {
		orderData["code"] = fmt.Sprintf("%v", resCode)
		orderData["msg"] = resKey
		c.JSON(resCode, orderData)

		var clog map[string]interface{}
		if payOut != nil {
			clog = map[string]interface{}{
				centerlog.FieldStatus:     resCode,
				centerlog.FieldStatusDesc: resKey,
				centerlog.FieldAppId:      payOutArg.AppId,
				centerlog.FieldAccountId:  payOut.PayAccountId,
				centerlog.FieldOrderId:    payOut.OrderId,
				centerlog.FieldAppOrderId: payOut.AppOrderId,
				centerlog.FieldAmount:     payOut.Amount,
				centerlog.FieldThirdDesc:  payOut.ThirdDesc + resKey,
			}
		} else {
			clog = map[string]interface{}{
				centerlog.FieldStatus:     resCode,
				centerlog.FieldStatusDesc: resKey,
				centerlog.FieldAppId:      payOutArg.AppId,
				centerlog.FieldAppOrderId: payOutArg.AppOrderId,
			}
		}

		centerlog.Info(centerlog.MsgPayOutCreateFail, clog)
		return
	}
	if payOut != nil {
		orderData["data"] = &model.PlatPayoutData{
			AppOrderId: payOut.AppOrderId,
			OrderId:    payOut.OrderId,
			Amount:     payOut.Amount,
			Status:     payOut.Status,
		}
		orderData["code"] = "200"
		c.JSON(200, orderData)
		return
	} else {
		orderData["code"] = "500"
		orderData["msg"] = resKey
		c.JSON(500, orderData)
		return
	}
}

func InquiryPayOutStatus(c *gin.Context) {
	orderId := c.Query("order_id")
	appOrderId := c.Query("app_order_id")
	if orderId == "" && appOrderId == "" {
		logger.Error("InquiryPayOutStatus_Error | orderId=%v", orderId)
		c.JSON(400, &map[string]string{"code": "400", "msg": "illegal orderId"})
		return
	}
	uid, _ := c.Get("UID")
	orderData := make(map[string]interface{})
	payOut, err := service.GetPayOutService().GetPayoutByOrderId(uid.(int64), orderId, appOrderId)
	if err != nil {
		orderData["code"] = "500"
		orderData["msg"] = err.Error()
	} else {
		orderData["code"] = "200"
		orderData["data"] = &model.PlatPayoutData{
			AppOrderId: payOut.AppOrderId,
			OrderId:    payOut.OrderId,
			Amount:     payOut.Amount,
			Status:     payOut.Status,
		}
	}
	c.JSON(200, orderData)
	return
}

// 查询账号余额
func InquiryAccountDetail(c *gin.Context) {
	uid, _ := c.Get("UID")
	ret := make(map[string]interface{})
	account, err := service.AccountServ.InquiryAccount(uid.(int64))
	if err != nil {
		ret["code"] = "500"
		ret["msg"] = err.Error()
	} else {
		ret["code"] = "200"
		ret["data"] = map[string]interface{}{
			"balance":          account.Balance,          // 可用余额
			"unsettledBalance": account.UnsettledBalance, // 等待结算
		}
	}
	c.JSON(200, ret)
	return
}

func ClickOrderLinkOption(c *gin.Context) {
	c.JSON(200, gin.H{"msg": "success"})
}

type ClickOrderLinkReq struct {
	OrderId string `json:"order_id"`
}

// 点击支付链接
func ClickOrderLink(c *gin.Context) {
	ip := c.ClientIP()
	orderId := c.Query("order_id")
	if orderId == "" {
		var req ClickOrderLinkReq
		err := c.ShouldBindJSON(&req)
		orderId = req.OrderId
		if orderId == "" || err != nil {
			logger.Error("click order order is empty | %v | %v", ip, err)
			c.JSON(400, map[string]interface{}{"code": "400", "msg": "invalid request"})
			return
		}
	}

	orderKey := fmt.Sprintf("orderLink:%s", orderId)
	val, err := dao.RedisIncr(orderKey, 1)
	if err != nil {
		logger.Error("click order[%v] link error %v | %v", orderId, err, ip)
		c.JSON(200, map[string]interface{}{"code": "200", "msg": "success 0"})
		return
	}

	dao.RedisExpire(orderKey, 86400*30)

	if val > 1 {
		logger.Error("click order[%v] link %v more than 1 | %v", orderId, val, ip)
		c.JSON(400, map[string]interface{}{"code": "400", "msg": "more than 1"})
		return
	}

	logger.Debug("success click order[%v] link | %v", orderId, ip)
	c.JSON(200, map[string]interface{}{"code": "200", "msg": "success 100"})
}
