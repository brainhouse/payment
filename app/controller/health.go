package controller

import (
	"fmt"
	"funzone_pay/common"
	"funzone_pay/dao"
	"github.com/gin-gonic/gin"
	"time"
)

var lastCheck int64 = 0

func Health(c *gin.Context) {
	if time.Now().Unix()-lastCheck < 10 {
		c.JSON(400, map[string]interface{}{
			"code": 400,
			"msg":  "invalid request",
		})
		return
	}

	var code int
	var resp map[string]interface{}
	defer func() {
		lastCheck = time.Now().Unix()
		if code != 200 {
			common.SendAlarmByLevel(common.Warning, fmt.Sprintf("FunZonePay Health Check Error %v-%v", c.ClientIP(), resp["msg"]))
		}

		c.JSON(code, resp)
	}()

	engine, err := dao.GetMysql()
	if err != nil {
		code = 500
		resp = map[string]interface{}{
			"code": 500,
			"msg":  "get engine error :" + err.Error(),
		}
		return
	}

	result, err := engine.Query("select count(id) as total from pay_out")
	if err != nil {
		code = 500
		resp = map[string]interface{}{
			"code": 500,
			"msg":  "exec sql error:" + err.Error(),
		}
		return
	}

	if len(result[0]) < 1 {
		code = 500
		resp = map[string]interface{}{
			"code": 500,
			"msg":  "exec sql error: no result",
		}
		return
	}

	//var intTotal int64 = 0
	//total, ok := result[0]["total"]
	//if !ok {
	//	intTotal, err = strconv.ParseInt(string(total), 10, 64)
	//}

	code = 200
	resp = map[string]interface{}{
		"code":  200,
		"total": string(result[0]["total"]),
	}
}
