package controller

import (
	"github.com/gin-gonic/gin"
	"github.com/wonderivan/logger"
	"time"
)
const uuuid = "#uuuid"
const uuuidTime = "#uuuidTime"

func timeLog(c *gin.Context, msg string) {
	start := c.GetInt64(uuuidTime)
	logger.Info("speed-uuuid:%s,dur:%.3f,msg:[%s]", c.GetHeader(uuuid),  float64(time.Now().UnixNano() - start) / 1000000, msg)
}