package controller

import (
	"funzone_pay/app/service"
	"funzone_pay/model"
	"github.com/gin-gonic/gin"
	"github.com/wonderivan/logger"
)

/**
 * 充值回调
 * param: *gin.Context c
 */
func CFOrderCallback(c *gin.Context) {
	orderId := c.Request.PostFormValue("orderId")
	orderAmount := c.Request.PostFormValue("orderAmount")
	referenceId := c.Request.PostFormValue("referenceId")
	txStatus := c.Request.PostFormValue("txStatus")
	paymentMode := c.Request.PostFormValue("paymentMode")
	txMsg := c.Request.PostFormValue("txMsg")
	txTime := c.Request.PostFormValue("txTime")
	signature := c.Request.PostFormValue("signature")
	webHook := &model.CFOrderCallBack{
		OrderId:     orderId,
		OrderAmount: orderAmount,
		ReferenceId: referenceId,
		TxStatus:    txStatus,
		PaymentMode: paymentMode,
		TxMsg:       txMsg,
		TxTime:      txTime,
		Signature:   signature,
	}
	logger.Debug("CFOrderCallback  | cb=%+v", webHook)
	//处理回调
	service.GetPayEntryService().CashFreePayInCallback(webHook)
	c.JSON(200, &map[string]string{"msg": "success"})
}

/**
 * 提现回调
 * param: *gin.Context c
 * event=TRANSFER_SUCCESS&transferId=CFOUT1585581836034664&referenceId=100386&acknowledged=1&eventTime=2020-03-30+20:55:06&utr=W1585580871&signature=fd/gES1C7d49bqmvJb8L1geXGTZppuzLkWL+iFNq6bs=
 */
func CFPayOutCallback(c *gin.Context) {
	event := c.Request.PostFormValue("event")
	transferId := c.Request.PostFormValue("transferId")
	referenceId := c.Request.PostFormValue("referenceId")
	reason := c.Request.PostFormValue("reason")
	acknowledged := c.Request.PostFormValue("acknowledged")
	eventTime := c.Request.PostFormValue("eventTime")
	utr := c.Request.PostFormValue("utr")
	signature := c.Request.PostFormValue("signature")
	webHook := &model.CFPayOutCallBack{
		Event:        event,
		TransferId:   transferId,
		ReferenceId:  referenceId,
		Reason:       reason,
		Acknowledged: acknowledged,
		EventTime:    eventTime,
		Utr:          utr,
		Signature:    signature,
	}

	service.GetPayOutService().CashFreePayOutCallback(webHook)
	c.JSON(200, &map[string]string{"msg": "success"})
}

//func CashfreeTest(c *gin.Context) {
//	pO, err := cashfree.GetCashFree().PayOut(validator.PayOutValidator{
//		AppId:      "rummy",
//		AppOrderId: fmt.Sprintf("testout%v", time.Now().Unix()),
//		PayType:    model.PT_BANK,
//		PayChannel: model.CASHFREE,
//		Amount:     10,
//		UserId:     "10050",
//		UserName:   "Thaneesh V",
//		BankCard:   "1769155000016710",
//		Phone:      "7845286022",
//		Email:      "7845286022@cashfree.com",
//		IFSC:       "KVBL0001769",
//		Address:    "ABC Street",
//		PayTm:      "9999999999",
//	})
//	logger.Debug("CashfreeTest_Info | data=%v | err=%v", pO, err)
//	c.JSON(200, pO)
//}
