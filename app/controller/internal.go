package controller

import (
	"encoding/json"
	"fmt"
	"funzone_pay/app/service"
	"funzone_pay/model"
	"strconv"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/wonderivan/logger"
)

var querySettleData []byte = nil
var lastQuerySettleTime int64 = 0
var querySettleLoading bool = false

func CollectSettlement(c *gin.Context) {
	payAccountId := c.Query("pay_account_id")
	settleAmount := c.Query("amount")
	adminUser := c.Query("admin_user")
	sAmount, _ := strconv.ParseFloat(settleAmount, 64)

	if !service.IsInvalidAccountID(payAccountId) {
		c.JSON(400, gin.H{"msg": fmt.Sprintf("invalid pay account %v", payAccountId)})
		return
	}

	if len(adminUser) > 1 && payAccountId != "" {
		go service.CollectSettlement(payAccountId, sAmount, adminUser)
		c.JSON(200, gin.H{"msg": "success"})
	} else {
		c.JSON(200, gin.H{"msg": "invalid param"})
	}
	return
}

// 暂时屏蔽部分功能
func QuerySettlement(c *gin.Context) {
	dataMap := &struct {
		Total  float64            `json:"total"`
		Detail map[string]float64 `json:"detail"`
	}{}

	dur := time.Now().Unix() - lastQuerySettleTime
	if querySettleData == nil {
		c.JSON(200, dataMap)
	} else {
		c.Data(200, "application/json; charset=utf-8", querySettleData)
	}

	if dur < 300 {
		return
	}

	// 超过缓存时间则重新loading最新数据
	// loading 状态则不重复执行
	if querySettleLoading {
		return
	}

	querySettleLoading = true
	go func() {
		defer func() {
			querySettleLoading = false
		}()

		detail := make(map[string]float64)
		payAccountIds := []string{
			//"cashfree1",
			//"cashfree2",
			//"cashfree3",
			//"cashfree7",
			//"cashfree9",
			//"cashfree10",
			//"cashfree11",
			//"cashfree13",
			//"cashfree14",
			//"cashfree15",
			//"cashfree16",
			//"serpay1",
			"serpay2",
			//"globpay1",
			//"onionpay4",
			//"onionpay5",
			//"onionpay6",
			//"onionpay7",
			//"payflash1",
			//"serpay_br",
			// "zwpay1",
			//"fmpay1",
			//"razorpay2",
			//"ybpay1",
			// "loogpay1",
			//"ydgpay1",
			// "airpay1",
			// "airpay2",
			"airpay3",
			//"pgpay1",
			//"bhtpay1",
			"oepay1",
			"phonepay1",
			"hopepay1",
			// "cashpay_br",
		}
		for _, v := range payAccountIds {
			amounts, err := service.QuerySettlementCount(v)
			if err != nil {
				logger.Error("QuerySettlement_CountErr | err=%v | v=%v", err, v)
				continue
			}
			//amount - fee
			detail[v] = amounts[0] - amounts[1]
		}

		dataMap.Detail = detail
		bts, err := json.Marshal(dataMap)
		if err != nil {
			logger.Error("QuerySettlement_MarshalErr | err=%v", err)
			return
		}

		querySettleData = bts
		lastQuerySettleTime = time.Now().Unix()
		logger.Info("QuerySettlement_Success | %v", string(bts))
	}()
}

func UserCollectSettlement(c *gin.Context) {
	uidStr := c.Query("uid")
	payAccountId := c.Query("pay_account_id")
	if !service.IsInvalidAccountID(payAccountId) {
		c.JSON(400, gin.H{"msg": fmt.Sprintf("invalid pay account %v", payAccountId)})
		return
	}

	settleAmount := c.Query("amount")
	adminUser := c.Query("admin_user")
	startStr := c.Query("start")
	endStr := c.Query("end")
	sAmount, _ := strconv.ParseFloat(settleAmount, 64)
	uid, _ := strconv.ParseInt(uidStr, 10, 64)
	start, _ := strconv.ParseInt(startStr, 10, 64)
	end, _ := strconv.ParseInt(endStr, 10, 64)
	if len(adminUser) > 1 && payAccountId != "" && uid > 0 {
		go service.UserCollectSettlement(payAccountId, uid, sAmount, adminUser, start, end)
		c.JSON(200, gin.H{"msg": "success"})
	} else {
		c.JSON(200, gin.H{"msg": "invalid param"})
	}
	return
}

/*
*
客户提现付款
*/
func PayWithdraw(c *gin.Context) {
	amountStr := c.Query("amount")
	amount, _ := strconv.ParseFloat(amountStr, 64)

	payChannel := model.PayChannel(c.Query("pay_channel"))
	payAccountId := c.Query("pay_account_id")
	idStr := c.Query("id")
	ids := strings.Split(idStr, ",")
	for _, v := range ids {
		id, _ := strconv.ParseInt(v, 10, 64)
		go func(id int64) {
			if id <= 0 {
				logger.Error("PayWithdraw_Info_Id_Empty | payChannel=%v | payAccountId=%v | id=%v | amount=%v | err=%v", payChannel, payAccountId, id, amount)
				return
			}
			//发起提现
			err := service.WithdrawPayment(payChannel, payAccountId, amount, id)
			logger.Debug("PayWithdraw_Info | payChannel=%v | payAccountId=%v | id=%v | amount=%v | err=%v", payChannel, payAccountId, id, amount, err)
		}(id)
	}
	c.JSON(200, gin.H{"code": "200", "msg": "success"})
	return
}

func ChargeRollback(c *gin.Context) {
	idStr := c.Query("id")
	id, _ := strconv.ParseInt(idStr, 10, 64)
	if id <= 0 {
		c.JSON(200, gin.H{"code": "400", "msg": "id is empty"})
		return
	}
	//充值回滚
	err := service.GetPayEntryService().PayEntryRollBack(id, false)
	if err != nil {
		c.JSON(200, gin.H{"code": "500", "msg": err.Error()})
		return
	}
	c.JSON(200, gin.H{"code": "200", "msg": "success"})
	return
}

func ChargeRollbackBat(c *gin.Context) {
	type rollBackBatReq struct {
		IDS      []int64  `json:"ids"`
		OrderIds []string `json:"order_ids"`
	}

	type rollBackSimple struct {
		ID      int64  `json:"id"`
		OrderId string `json:"order_id"`
		Desc    string `json:"desc"`
	}

	req := new(rollBackBatReq)
	err := c.BindJSON(&req)
	if err != nil {
		c.JSON(200, gin.H{"code": "400", "msg": fmt.Sprintf("invalid req format %v", err)})
		return
	}

	ret := make([]rollBackSimple, 0, len(req.IDS)+len(req.OrderIds))

	//充值回滚
	for _, id := range req.IDS {
		err = service.GetPayEntryService().PayEntryRollBack(id, false)
		if err != nil {
			ret = append(ret, rollBackSimple{id, "", fmt.Sprintf("fail: [%v]", err)})
		} else {
			ret = append(ret, rollBackSimple{id, "", "success"})
		}
	}

	for _, oid := range req.OrderIds {
		err = service.GetPayEntryService().PayEntryRollBackByOrderId(oid, false)
		if err != nil {
			ret = append(ret, rollBackSimple{0, oid, fmt.Sprintf("fail: [%v]", err)})
		} else {
			ret = append(ret, rollBackSimple{0, oid, "success"})
		}
	}

	c.JSON(200, gin.H{"code": "200", "msg": "success", "data": ret})
	return
}

func PayoutRollback(c *gin.Context) {
	orderId := c.Query("order_id")
	if orderId == "" {
		c.JSON(200, gin.H{"code": "400", "msg": "order id is empty"})
		return
	}

	admin := c.Query("admin")
	if admin == "" {
		c.JSON(200, gin.H{"code": "400", "msg": "admin id is empty"})
		return
	}
	//充值回滚
	ret := service.GetPayOutService().PayOutRollBackCallbackById(orderId, admin)
	c.JSON(200, gin.H{"code": "200", "msg": ret})
	return
}

func PayoutToFail(c *gin.Context) {
	orderIds := c.Query("order_ids")
	if orderIds == "" {
		c.JSON(200, gin.H{"code": "400", "msg": "order id is empty"})
		return
	}

	admin := c.Query("admin")
	if admin == "" {
		c.JSON(200, gin.H{"code": "400", "msg": "o is empty"})
		return
	}

	ids := strings.Split(orderIds, ",")
	fails := make([]string, 0, 128)
	for _, orderId := range ids {
		ret := service.GetPayOutService().PayOutToFailById(orderId, admin)
		if ret != "" {
			fails = append(fails, orderId)
		}
	}

	logger.Debug("ManualOptPayout ids=%v, fail=%v", orderIds, fails)
	c.JSON(200, gin.H{"code": "200", "fails": fails})
	return
}

// 手动设置订单成功

func ManualCheckCallback(c *gin.Context) {
	orderIds := c.Query("order_ids")
	if orderIds == "" {
		c.JSON(200, gin.H{"code": "400", "msg": "order id is empty"})
		return
	}

	isSettingSuccess := c.Query("success") == "true"

	ids := strings.Split(orderIds, ",")
	fails := make([]string, 0, 128)
	for _, orderId := range ids {
		ret := service.GetPayEntryService().ManualCheckCallback(orderId, isSettingSuccess)
		if ret != "" {
			fails = append(fails, orderId)
		}
	}

	logger.Debug("ManualSuccessCallback ids=%v, fail=%v", orderIds, fails)
	c.JSON(200, gin.H{"code": "200", "fails": fails})
}

func InternalRetryCallback(c *gin.Context) {
	idStr := c.Query("uid")
	id, _ := strconv.ParseInt(idStr, 10, 64)
	if id <= 0 {
		c.JSON(200, gin.H{"code": "400", "msg": "uid is empty"})
		return
	}
	tp := c.Query("type")
	orderId := c.Query("order_id")
	orderIds := strings.Split(orderId, ",")
	for _, oid := range orderIds {
		if tp == "collect" {
			err := service.GetPayEntryService().RetryPlatFromCallback(id, oid)
			if err != nil {
				logger.Error("InternalRetryCallback_Error | order_id=%v | err=%v", oid, err)
			}
		} else {
			err := service.GetPayOutService().RetryPlatFormCallback(id, oid)
			if err != nil {
				logger.Error("InternalRetryCallback_Error | order_id=%v | err=%v", oid, err)
			}
		}
	}
	c.JSON(200, gin.H{"code": "200", "msg": "success"})
	return
}

func InternalReleaseTransaction(c *gin.Context) {
	dataIDs := c.Query("data_ids")
	ids := strings.Split(dataIDs, ",")
	fail := make([]string, 0, 128)
	for _, id := range ids {
		dataId, _ := strconv.ParseInt(id, 10, 64)
		if dataId <= 0 {
			fail = append(fail, id)
			continue
		}

		ret := service.GetPayOutService().ReleaseFrozenTransaction(dataId)
		if !ret { // 失败
			fail = append(fail, id)
		}
	}
	c.JSON(200, gin.H{"code": "200", "fail": fail})
	return
}
