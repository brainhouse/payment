package controller

import (
	"encoding/json"
	"fmt"
	"funzone_pay/app/service"
	"funzone_pay/model"
	"funzone_pay/utils"
	"github.com/gin-gonic/gin"
	"github.com/wonderivan/logger"
	"net/http"
	"strings"
)

// https://developer.luxpag.com/cn/reference/ipn.html
func PagSmilePayCollectCallback(c *gin.Context) {
	rawData, _ := c.GetRawData()
	logger.Debug("PagSmilePayCollectCallback_Data | data=%v | header=%v", string(rawData), c.Request.Header)

	notify := model.PagSmilePayNotify{}
	err := json.Unmarshal(rawData, &notify)
	if err != nil {
		logger.Error("PagSmilePayCollectCallback_Json_Error | err=%v | data=%v", err, string(rawData))
		c.JSON(http.StatusBadRequest, gin.H{"code": "400", "error": "json Unmarshal Error"})
		return
	}

	sign := c.Request.Header.Get("Luxpag-Signature")

	aid := c.Query("id")
	appCfg, err := service.PayAccountConfServe.GetLocalPaymentConfigByAccountID(aid)
	if err != nil {
		logger.Error("PayEntryService_PagSmilePayCallback_NOAPPConf | account_id=%v | cb=%v", aid)
		c.JSON(http.StatusBadRequest, gin.H{"code": "400", "error": fmt.Sprintf("unsupported id[%v]", aid)})
		return
	}

	appsk, ok := appCfg["APPSK"].(string)
	if !ok {
		logger.Error("PayEntryService_PagSmilePayCallback_NOMerchantKey | account_id=%v | cb=%v", aid)
		c.JSON(http.StatusBadRequest, gin.H{"code": "400", "error": fmt.Sprintf("unsupported id[%v]", aid)})
	}

	rawDataStr := string(rawData)
	hmac := utils.HmacSha256ToXStringWithKey(rawDataStr, appsk)
	if sign != hmac {
		logger.Error("PayEntryService_PagSmilePayCallback_Signature | account=%v | hmc=%v | sign=%v", aid, hmac, sign)
		c.JSON(400, &map[string]string{"result": "success"})
		return
	}

	//处理回调
	service.GetPayEntryService().PagSmilePayInCallback(notify)
	c.JSON(200, &map[string]string{"result": "success"})
}

// https://docs.transfersmile.com/payout/security
func PagSmilePayPayOutCallback(c *gin.Context) {
	rawData, _ := c.GetRawData()
	//orgNo=8211000787&custId=21100900002351&custOrderNo=SPOUT1634141373056817&prdOrdNo=A20211014000935408763&ordStatus=07&payAmt=10000&sign=7B5A4B239EB3A839C31878372217CE91
	logger.Debug("PagSmilePayPayOutCallback_Data | json=%v | header=%v", string(rawData), c.Request.Header)

	notify := model.PagSmilePayOutNotify{}
	err := json.Unmarshal(rawData, &notify)
	if err != nil {
		logger.Error("PagSmilePayPayOutCallback_Json_Error | err=%v | data=%v", err, string(rawData))
		c.JSON(http.StatusBadRequest, gin.H{"code": "400", "error": "json Unmarshal Error"})
		return
	}

	m := make(map[string]interface{})
	d := json.NewDecoder(strings.NewReader(string(rawData)))
	d.UseNumber()
	err = d.Decode(&m)
	if err != nil {
		logger.Error("PagSmilePayPayOutCallback_Json_map_Error | err=%v | data=%v", err, string(rawData))
		c.JSON(http.StatusBadRequest, gin.H{"code": "400", "error": "json Unmarshal Error"})
		return
	}

	aid := c.Query("id")
	appCfg, err := service.PayAccountConfServe.GetLocalPaymentConfigByAccountID(aid)
	if err != nil {
		logger.Error("PagSmilePayPayOutCallback_NOAPPConf | account_id=%v | cb=%v", aid)
		c.JSON(http.StatusBadRequest, gin.H{"code": "400", "error": fmt.Sprintf("unsupported id[%v]", aid)})
		return
	}

	merchantKey, ok := appCfg["MERCHANT_KEY"].(string)
	if !ok {
		logger.Error("PagSmilePayPayOutCallback_NOMerchantKey | account_id=%v | cb=%v", aid)
		c.JSON(http.StatusBadRequest, gin.H{"code": "400", "error": fmt.Sprintf("unsupported id[%v]", aid)})
	}

	sortedParam := utils.SortedAndBuild(m)
	hmac := utils.HmacSha256ToXString(sortedParam+merchantKey)
	sign := c.Request.Header.Get("Authorization")
	if hmac != sign {
		logger.Error("PagSmilePayPayOutCallback_Signature | account=%v | hmc=%v | sign=%v", aid, hmac, sign)
		c.JSON(400, &map[string]string{"result": "success"})
		return
	}


	service.GetPayOutService().PagSmilePayOutCallback(notify)
	c.JSON(200, "success")
}
