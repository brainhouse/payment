package controller

import (
	"encoding/json"
	"funzone_pay/app/service"
	"funzone_pay/model"
	"github.com/gin-gonic/gin"
	"github.com/wonderivan/logger"
)

func YBCallback(c *gin.Context) {
	rawData, _ := c.GetRawData()
	webHook := new(model.YBCollectCallback)
	err := json.Unmarshal(rawData, webHook)
	if err != nil {
		logger.Error("YBCallback_JsonUnmarshal_Error | err=%v", err)
		c.JSON(400, &map[string]string{"msg": "json error"})
		return
	}
	logger.Debug("OrderCallback_Data json=%v", string(rawData))
	service.GetPayEntryService().YBInCallback(webHook)
	c.String(200, "success")
}

func YBPayOutCallback(c *gin.Context) {
	rawData, _ := c.GetRawData()
	logger.Debug("YBPayOutCallback_Data json=%v", string(rawData))
	webHook := new(model.YBPayoutCallback)
	err := json.Unmarshal(rawData, webHook)
	if err != nil {
		logger.Error("YBPayOutCallback_JsonUnmarshal_Error | err=%v", err)
		c.JSON(400, &map[string]string{"msg": "json error"})
		return
	}
	service.GetPayOutService().YBPayOutCallback(webHook)
	c.String(200, "success")
}
