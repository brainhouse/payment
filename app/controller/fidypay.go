package controller

import (
	"encoding/json"
	"funzone_pay/app/service"
	"funzone_pay/model"
	"github.com/gin-gonic/gin"
	"github.com/wonderivan/logger"
)

func FidyPayCollectCallback(c *gin.Context) {
	rawData, _ := c.GetRawData()
	logger.Debug("FidyPayCollectCallback | data=%v | header=%v", string(rawData), c.Request.Header)

	webHook := &model.FidyPayCollectCallback{}
	err := json.Unmarshal(rawData, webHook)
	if err != nil {
		logger.Error("FidyPayCollectCallbackJsonErr | data=%v | err=%v", string(rawData), err)
		c.JSON(200, &map[string]string{"msg": "failed"})
		return
	}
	//处理回调
	service.GetPayEntryService().FidyPayInCallback(webHook)
	c.JSON(200, &map[string]string{"msg": "success"})
}

func FidyPayPayOutCallback(c *gin.Context) {
	rawData, _ := c.GetRawData()
	logger.Debug("FidyPayPayOutCallback | json=%v | header=%v", string(rawData), c.Request.Header)

	webHook := &model.FidyPayPayoutCallback{}
	err := json.Unmarshal(rawData, webHook)
	if err != nil {
		logger.Error("FidyPayPayOutCallbackJsonErr | data=%v | err=%v", string(rawData), err)
		c.JSON(200, &map[string]string{"msg": "failed"})
		return
	}
	service.GetPayOutService().FidyPayOutCallback(webHook)
	c.JSON(200, &map[string]string{"msg": "success"})
}
