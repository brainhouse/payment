package controller

import (
	"funzone_pay/centerlog"
	"github.com/gin-gonic/gin"
	"time"
)

func CollectLog(c *gin.Context) {
	key := c.Query("key")
	if key != "collect-log" {
		c.JSON(400, gin.H{
			"code":    400,
			"success": "invalid param",
		})
		return
	}

	message := c.Query("message")
	if message == "" {
		message = "NO"
	}

	data := make(map[string]interface{})
	err := c.BindJSON(&data)
	if err != nil {
		c.JSON(400, gin.H{
			"code":    400,
			"success": false,
		})
		return
	}

	centerlog.Info(message, data)
	if c.Query("t") != "" {
		d := map[string]interface{}{
			"tp__":      "测试类型",
			"content__": "内容文本",
			"time__":    time.Now().Unix(),
		}
		centerlog.Info(message, d)
	}
	c.JSON(200, gin.H{
		"code":    200,
		"success": true,
	})
}