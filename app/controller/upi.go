package controller

import (
	"encoding/json"
	"funzone_pay/app/service"
	"funzone_pay/model"
	"github.com/gin-gonic/gin"
	"github.com/wonderivan/logger"
)

func VerifyVPA(c *gin.Context) {
	orderId := c.Query("order_id")
	VPA := c.Query("vpa")
	orderData := make(map[string]interface{})

	if orderId == "" || VPA == "" {
		orderData["code"] = "400"
		orderData["msg"] = "params error"
		c.JSON(200, orderData)
		return
	}
	res, err := service.GetUPIService().VerifyVPA(orderId, VPA)
	if err != nil {
		orderData["code"] = "500"
		orderData["msg"] = "internal error"
		c.JSON(200, orderData)
		return
	}
	orderData["code"] = "200"
	orderData["data"] = map[string]interface{}{"status": res}
	orderData["msg"] = "success"
	c.JSON(200, orderData)
	return
}

func UpiCollect(c *gin.Context) {
	rawData, _ := c.GetRawData()
	logger.Debug("UpiCollect_Data data=%v | header=%v", string(rawData), c.Request.Header)
	req := &struct {
		OrderId string `json:"order_id"`
		VPA     string `json:"vpa"`
	}{}
	orderData := make(map[string]interface{})
	err := json.Unmarshal(rawData, req)
	if err != nil {
		orderData["code"] = "400"
		orderData["msg"] = "json error"
		c.JSON(200, orderData)
		return
	}
	if req.OrderId == "" || req.VPA == "" {
		orderData["code"] = "400"
		orderData["msg"] = "params error"
		c.JSON(200, orderData)
		return
	}
	status, err := service.GetUPIService().DoCollect(req.OrderId, req.VPA)
	if status == model.PAYING && err != nil {
		orderData["code"] = "500"
		orderData["msg"] = err.Error()
		c.JSON(200, orderData)
		return
	}
	if err != nil {
		orderData["code"] = "500"
		orderData["msg"] = "internal error"
		c.JSON(200, orderData)
		return
	}
	orderData["code"] = "200"
	orderData["data"] = map[string]interface{}{
		"order_id": req.OrderId,
	}
	orderData["msg"] = "success"
	c.JSON(200, orderData)
	return
}

func UpiPayInStatus(c *gin.Context) {
	orderId := c.Query("order_id")
	orderData := make(map[string]interface{})
	if orderId == "" {
		orderData["code"] = "400"
		orderData["msg"] = "params error"
		c.JSON(200, orderData)
		return
	}
	res, err := service.GetUPIService().GetPayStatus(orderId)
	if err != nil {
		orderData["code"] = "500"
		orderData["msg"] = "internal error"
		c.JSON(200, orderData)
		return
	}
	orderData["code"] = "200"
	orderData["data"] = map[string]interface{}{
		"order_id": orderId,
		"status":   res,
	}
	logger.Debug("UpiPayInStatus_Info | status=%v | orderId=%v", res, orderId)
	orderData["msg"] = "success"
	c.JSON(200, orderData)
	return
}
