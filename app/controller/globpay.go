package controller

import (
	"fmt"
	"funzone_pay/app/service"
	"funzone_pay/channel/globpay"
	"funzone_pay/model"
	"github.com/gin-gonic/gin"
	"github.com/wonderivan/logger"
	"net/http"
	"strconv"
)

func GlobPayCollectCallback(c *gin.Context) {
	err := c.Request.ParseForm()
	if err != nil {
		logger.Error("GlobPayCollectCallback_ParseForm Error=%v ", err)
		c.JSON(http.StatusBadRequest, gin.H{"code": "400", "error": "Parse Form Error"})
		return
	}

	params := c.Request.Form
	extra := params.Get("extra")

	data := globpay.GlobExtraData{}
	err = data.ParseFromString(extra)
	logger.Debug("GlobPayCollectCallback_Data | data=%v | header=%v | err=%v", params.Encode(), extra, data, c.Request.Header, err)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"code": "400", "error": "parse Error"})
		return
	}

	ins, err := service.PayAccountConfServe.GetSpecificPayInstance(model.GLOBPAY, data.PayAccountId)
	if err != nil {
		logger.Error("GlobPayCollectCallback_Invalid PayAccountId=%v | error=%v ", data.PayAccountId, err)
		c.JSON(http.StatusBadRequest, gin.H{"code": "400", "error": fmt.Sprintf("unsupported")})
		return
	}

	//
	if !ins.InSignature(params) {
		logger.Error("GlobPayCollectCallback_Signature Error")
		c.JSON(http.StatusBadRequest, gin.H{"code": "400", "error": fmt.Sprintf("unsupported")})
		return
	}

	mountStr := params.Get("orderAmount")
	mount, _ := strconv.ParseInt(mountStr, 10, 64)
	notify := model.GlobPayNotify{
		Code:           params.Get("code"),
		MchId:          params.Get("mchId"),
		MchOrderNo:     params.Get("mchOrderNo"),
		ProductId:      params.Get("productId"),
		OrderAmount:    mount,
		PayOrderId:     params.Get("payOrderId"),
		PaySuccessTime: params.Get("paySuccessTime"),
		Message:        params.Get("message"),
	}

	//处理回调
	service.GetPayEntryService().GlobPayInCallback(notify)
	//c.JSON(200, &map[string]string{"result": "success"})
	c.String(200, "success")
}


func GlobPayPayOutCallback(c *gin.Context) {
	err := c.Request.ParseForm()
	if err != nil {
		logger.Error("GlobPayPayOutCallback_ParseForm Error=%v ", err)
		c.JSON(http.StatusBadRequest, gin.H{"code": "400", "error": "Parse Form Error"})
		return
	}

	params := c.Request.Form
	extra := params.Get("extra")

	data := globpay.GlobExtraData{}
	err = data.ParseFromString(extra)
	logger.Debug("GlobPayOutCallback_Data | data=%v | header=%v | err=%v", params.Encode(), extra, data, c.Request.Header, err)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"code": "400", "error": "parse Error"})
		return
	}

	ins, err := service.PayAccountConfServe.GetSpecificPayInstance(model.GLOBPAY, data.PayAccountId)
	if err != nil {
		logger.Error("GlobPayOutCallback_Invalid PayAccountId=%v | error=%v ", data.PayAccountId, err)
		c.JSON(http.StatusBadRequest, gin.H{"code": "400", "error": fmt.Sprintf("unsupported")})
		return
	}

	//
	if !ins.InSignature(params) {
		logger.Error("GlobPayOutCallback_Signature Error")
		c.JSON(http.StatusBadRequest, gin.H{"code": "400", "error": fmt.Sprintf("unsupported")})
		return
	}

	mountStr := params.Get("orderAmount")
	mount, _ := strconv.ParseInt(mountStr, 10, 64)
	notify := model.GlobPayNotify {
		Code:           params.Get("code"),
		MchId:          params.Get("mchId"),
		MchOrderNo:     params.Get("mchOrderNo"),
		ProductId:      params.Get("productId"),
		OrderAmount:    mount,
		PayOrderId:     params.Get("payOrderId"),
		PaySuccessTime: params.Get("paySuccessTime"),
		Message:        params.Get("message"),
	}

	service.GetPayOutService().GlobPayOutCallback(notify)
	c.JSON(200, "success")
}
