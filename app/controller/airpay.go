package controller

import (
	"funzone_pay/app/service"
	"github.com/gin-gonic/gin"
	"github.com/wonderivan/logger"
	"net/url"
)

func AirPayCallback(c *gin.Context) {
	rawData, _ := c.GetRawData()
	logger.Debug("AirPayCallback_Data data=%v", string(rawData))
	//TRANSACTIONPAYMENTSTATUS=FAIL&MERCID=271754&TRANSACTIONID=APIN1673857803084684234a&APTRANSACTIONID=148633236&TXN_MODE=LIVE&CHMOD=upi&AMOUNT=100.00&CURRENCYCODE=356&TRANSACTIONSTATUS=400&MESSAGE=Fail&CUSTOMER=JOHN JOHN&CUSTOMERPHONE=999999999&CUSTOMEREMAIL=999999999@GMAIL.COM&TRANSACTIONTYPE=320&RISK=0&IPNID=175878525&CUSTOMVAR=0&TRANSACTIONTIME=16-01-2023 14:02:23&TOKEN=&UID=&BILLEDAMOUNT=100.00&RRN=&MERCHANT_NAME=Mindgeek&CARDTYPE=&CUSTOMERVPA=&ap_SecureHash=980212625
	urlData, err := url.ParseQuery(string(rawData))
	if err != nil {
		logger.Error("AirPayCallback_Param_Error | err=%v | data=%v", err, rawData)
		c.JSON(400, &map[string]string{"msg": "params error"})
		return
	}
	service.GetPayEntryService().AirInCallback(urlData)
	c.String(200, "success")
}
