package controller

import (
	"funzone_pay/channel/cashfree"
	"funzone_pay/channel/onionpay"
	"funzone_pay/channel/payflash"
	"funzone_pay/channel/phonepay"
	"net/url"
)

type PayChannel string

const (
	PayTm    PayChannel = "paytm"
	PayU     PayChannel = "payu"
	EaglePay PayChannel = "eaglepay"
	SerPay   PayChannel = "serpay"
	PagSmile PayChannel = "pagsmile"
	FidyPay  PayChannel = "fidypay"
	CashFree PayChannel = "cashfree"
	OnionPay PayChannel = "onionpay"
	PayFlash PayChannel = "payflash"
	BhtPay   PayChannel = "bhtpay"
	PhonePay PayChannel = "phonepay"
)

type Func func(rawBody []byte, params url.Values) (int, string, string)

var returnChannels = map[PayChannel]Func{
	CashFree: cashfree.CheckReturn,
	OnionPay: onionpay.CheckReturnResponse,
	PayFlash: payflash.CheckReturnResponse,
	PhonePay: phonepay.CheckReturnResponse,
	BhtPay: func(rawBody []byte, params url.Values) (b int, order string, s string) {
		return 1, "", ""
	},
}

func GetFunc(ch PayChannel) Func {
	f, b := returnChannels[ch]
	if b {
		return f
	}
	return defaultFunc
}

func defaultFunc(raw []byte, params url.Values) (int, string, string) {
	return -1, "", "no supported channel return"
}
