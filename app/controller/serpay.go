package controller

import (
	"funzone_pay/app/service"
	"funzone_pay/model"
	"github.com/gin-gonic/gin"
	"github.com/wonderivan/logger"
	"net/url"
)

/**
serpay充值回调
//orgNo=8211000787
//custId=21100900002351
//custOrderNo=SPIN1634047113052607
//prdOrdNo=20211012215835252172KqiVtchV7
//ordStatus=01
//ordAmt=6000
//payAmt=6000
//ordTime=20211012215835
//version=2.1
//sign=3631F5377AC387BC2AF0D41F05E2E3BE*/
func SerPayCollectCallback(c *gin.Context) {
	rawData, _ := c.GetRawData()
	logger.Debug("SerPayCollectCallback_Data | data=%v | header=%v", string(rawData), c.Request.Header)

	values, _ := url.ParseQuery(string(rawData))
	orgNo := values.Get("orgNo")
	custId := values.Get("custId")
	custOrderNo := values.Get("custOrderNo")
	prdOrdNo := values.Get("prdOrdNo")
	ordStatus := values.Get("ordStatus")
	ordAmt := values.Get("ordAmt")
	payAmt := values.Get("payAmt")
	ordTime := values.Get("ordTime")
	version := values.Get("version")
	// --- MX支付回调参数
	orderDesc := values.Get("orderDesc")
	clabe := values.Get("clabe")
	// ---
	sign := values.Get("sign")
	webHook := &model.SerInCallback{
		OrgNo:       orgNo,
		CustId:      custId,
		CustOrderNo: custOrderNo,
		PrdOrdNo:    prdOrdNo,
		OrdStatus:   ordStatus,
		OrdAmt:      ordAmt,
		PayAmt:      payAmt,
		OrdTime:     ordTime,
		Version:     version,
		OrderDesc:   orderDesc,
		Clabe:       clabe,
		Sign:        sign,
		OrgStr:      string(rawData),
	}
	logger.Debug("SerPayCollectCallback  | cb=%+v", webHook)
	//处理回调
	service.GetPayEntryService().SerPayInCallback(webHook)
	c.JSON(200, &map[string]string{"msg": "SC000000"})
}

func SerPayPayOutCallback(c *gin.Context) {
	rawData, _ := c.GetRawData()
	//orgNo=8211000787&custId=21100900002351&custOrderNo=SPOUT1634141373056817&prdOrdNo=A20211014000935408763&ordStatus=07&payAmt=10000&sign=7B5A4B239EB3A839C31878372217CE91
	logger.Debug("SerPayPayOutCallback_Data | json=%v | header=%v", string(rawData), c.Request.Header)

	values, _ := url.ParseQuery(string(rawData))
	orgNo := values.Get("orgNo")
	custId := values.Get("custId")
	custOrderNo := values.Get("custOrderNo")
	prdOrdNo := values.Get("prdOrdNo")
	ordStatus := values.Get("ordStatus")
	payAmt := values.Get("payAmt")
	sign := values.Get("sign")
	webHook := &model.SerOutCallback{
		OrgNo:       orgNo,
		CustId:      custId,
		CustOrderNo: custOrderNo,
		PrdOrdNo:    prdOrdNo,
		OrdStatus:   ordStatus,
		PayAmt:      payAmt,
		Sign:        sign,
		OrgStr:      string(rawData),
	}
	service.GetPayOutService().SerPayOutCallback(webHook)
	c.JSON(200, &map[string]string{"msg": "SC000000"})
}
