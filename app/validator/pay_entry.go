package validator

import (
	"fmt"
	"funzone_pay/model"
	"strings"
)

type PayEntryValidator struct {
	Uid        int64            `form:"uid" json:"uid"`
	AppId      string           `form:"app_id" json:"app_id"`
	Country    string           `form:"country" json:"country"`
	AppOrderId string           `form:"app_order_id" json:"app_order_id"`
	PayChannel model.PayChannel `form:"pay_channel" json:"pay_channel"`
	Amount     float64          `form:"amount" json:"amount"`
	UserId     string           `form:"uid" json:"user_id"`
	UserName   string           `form:"user_name" json:"user_name"`
	Phone      string           `form:"phone" json:"phone"`
	Email      string           `form:"email" json:"email"`
	IP         string           `form:"ip" json:"ip"`
	ReturnUrl  string           `form:"return_url" json:"return_url"` // glob 支付成功跳转页面

	// 巴西pix支付需要
	//DocumentType  string `form:"document_type" json:"document_type"` // CPF-个人税号11位 / CNPJ-企业税号-14位
	//DocumentValue string `form:"document_value" json:"document_value"`

	// serpay-墨西哥支付的必要字段
	UserCitizenId string `form:"user_citizen_id" json:"user_citizen_id"`
	UserDeviceId  string `form:"user_device_id" json:"user_device_id"`
	City          string `form:"city" json:"city"`
	Street        string `form:"street" json:"street"`
	HouseNumber   string `form:"house_number" json:"house_number"`
}

// Deprecated: FunctionName is deprecated
func (rs *PayEntryValidator) Valid() (res bool, msg string) {
	if rs.AppId == "" {
		return false, "app_id is empty"
	}
	if rs.AppId != string(model.PUBG) &&
		rs.AppId != string(model.RUMMY) &&
		rs.AppId != string(model.JHRUMMY) &&
		rs.AppId != string(model.GameBR) &&
		rs.AppId != string(model.GameMX) &&
		rs.AppId != string(model.GameCL) &&
		rs.AppId != string(model.FUN) {
		return false, "app_id is wrong !"
	}
	//if rs.PayChannel == "" {
	//	return false, "pay_channel is empty"
	//}
	//if rs.PayChannel != model.CASHFREE && rs.PayChannel != model.RAZORPAY && rs.PayChannel != model.PAYTM && rs.PayChannel != model.PAYU && rs.PayChannel != model.PAYU2 && rs.PayChannel != model.PAYU3 {
	//	return false, "pay_channel is wrong"
	//}
	if rs.AppOrderId == "" {
		return false, "app_order_id is empty"
	}
	if rs.Amount <= 0 {
		return false, "amount is empty"
	}
	//
	rs.Amount = float64(int64(rs.Amount*100)) / 100.0
	if rs.Phone == "" {
		rs.Phone = "9876543210"
	}
	if rs.UserName == "" {
		rs.UserName = "rummybank"
	}
	if rs.Email == "" {
		rs.Email = rs.Phone + "@rummybank.com"
	}
	switch rs.Country {
	case model.Brazil:
		//if rs.DocumentType == "" {
		//	rs.DocumentType = "CPF"
		//}
		//if rs.DocumentValue == "" {
		//	rs.DocumentValue = rs.Phone
		//}
	}
	if rs.UserCitizenId == "" {
		rs.UserCitizenId = "citizen-" + rs.Phone
	}
	if rs.UserDeviceId == "" {
		rs.UserDeviceId = "device-" + rs.Phone
	}
	if rs.City == "" {
		rs.City = "city-" + rs.UserName
	}
	if rs.Street == "" {
		rs.Street = "street-" + rs.UserName
	}
	if rs.HouseNumber == "" {
		rs.HouseNumber = "housenumber-" + rs.UserName
	}
	return true, ""
}

func (rs *PayEntryValidator) PlatFormValid() (res bool, msg string) {
	if rs.AppOrderId == "" {
		return false, "app_order_id is empty"
	}
	if rs.Amount <= 0 {
		return false, "amount is empty"
	}
	if rs.Amount > 100000 {
		return false, "amount too big"
	}

	// 两位小数
	rs.Amount = float64(int64(rs.Amount*100)) / 100.0
	//if rs.Amount < 10 {
	//	return false, "amount too small"
	//}
	rs.Phone = strings.TrimSpace(rs.Phone)
	if len(rs.Phone) < 6 {
		return false, "phone invalid"
	}
	rs.Email = strings.TrimSpace(rs.Email)
	if rs.Email == "" || rs.Email == "@rummy.com" {
		rs.Email = fmt.Sprintf("%v@gmail.com", rs.Phone)
	}

	rs.UserName = strings.TrimSpace(rs.UserName)
	if rs.UserName == "" {
		return false, "user_name is empty"
	}
	if rs.IP == "" {
		rs.IP = "1.0.0.1"
	}
	//if rs.DocumentType == "" {
	//	rs.DocumentType = "CPF"
	//}
	//if rs.DocumentValue == "" {
	//	rs.DocumentValue = rs.Phone
	//}
	return true, ""
}
