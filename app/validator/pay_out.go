package validator

import (
	"fmt"
	"funzone_pay/model"
	"regexp"
	"strings"
)

type PayOutValidator struct {
	AppId      string           `form:"app_id" json:"app_id"`
	AppOrderId string           `form:"app_order_id" json:"app_order_id"`
	Country    string           `form:"country" json:"country"`
	PayType    model.PayType    `form:"pay_type" json:"pay_type"`
	PayChannel model.PayChannel `form:"pay_channel" json:"pay_channel"`
	Amount     float64          `form:"amount" json:"amount"`
	UserId     string           `form:"user_id" json:"user_id"`
	UserName   string           `form:"user_name" json:"user_name"`
	Phone      string           `form:"phone" json:"phone"`
	Email      string           `form:"email" json:"email"`
	BankCard   string           `form:"bank_card" json:"bank_card"` // 银行卡
	CardType   model.CardType   `form:"card_type" json:"card_type"`
	IFSC       string           `form:"ifsc" json:"ifsc"`
	//BankName   string           `form:"bank_name" json:"bank_name"` // 巴西需要 银行名称
	BankCode   string           `form:"bank_code" json:"bank_code"` // 银行、支行编码
	PayTm      string           `form:"paytm" json:"paytm"`
	VPA        string           `form:"vpa" json:"vpa"`
	Address    string           `form:"address" json:"address"`
	Uid        int64

	// 巴西银行代付必要字段
	//Birthday      string `form:"birthday" json:"birthday"`           //
	//DocumentType  string `form:"document_type" json:"document_type"` // CPF-个人税号11位 / CNPJ-企业税号-14位
	DocumentValue string `form:"document_value" json:"document_value"`

	// 墨西哥需要
	Account     string `form:"account" json:"account"`
	AccountType string `form:"account_type" json:"account_type"`
}

// Deprecated: FunctionName is deprecated
func (rs *PayOutValidator) Valid() (res bool, msg string) {
	if rs.AppId == "" {
		return false, "app_id is empty"
	}
	if rs.AppOrderId == "" {
		return false, "app_order_id is empty"
	}
	if rs.Amount == 0 {
		return false, "amount is empty"
	}
	if rs.UserId == "" {
		return false, "uid is empty"
	}
	if rs.UserName == "" {
		return false, "user_name is empty"
	}
	if rs.Phone == "" {
		return false, "phone is empty"
	}
	if rs.PayType == "" {
		return false, "pay_type is empty"
	}
	if rs.PayType == model.PT_PayTm && rs.PayTm == "" {
		return false, "paytm is empty"
	}
	if rs.PayType == model.PT_UPI && rs.VPA == "" {
		return false, "vpa is empty"
	}
	if rs.PayType == model.PT_BANK {
		if rs.BankCard == "" {
			return false, "bankcard is empty"
		}

		switch rs.Country {
		case model.Brazil:
			if rs.DocumentValue == "" {
				return false, "document is empty"
			}

			if rs.BankCode == "" {
				return false, "bank code is empty"
			}

			//if rs.BankName == "" {
			//	return false, "bank name is empty"
			//}

		case model.Mexico:

		case model.Pakistan:
			if rs.BankCode == "" {
				return false, "bank code is empty"
			}
		default:
			// 默认印度
			if rs.IFSC == "" {
				return false, "ifsc is empty"
			}

			reg, _ := regexp.Compile("[a-zA-Z]{4}0[a-zA-Z0-9]{6}")
			if !reg.MatchString(rs.IFSC) {
				return false, "ifsc is invalid"
			}
			regCard, _ := regexp.Compile("^\\d{9,18}$")
			if !regCard.MatchString(rs.BankCard) {
				return false, "bank_card is invalid"
			}
		}
	}
	if rs.PayType == model.PT_CLABE && (rs.BankCard == "" || rs.BankCode == "") {
		return false, "bankcard or code is empty"
	}
	if rs.PayType == model.PT_PIX {
		if rs.BankCard == "" {
			return false, "bankcard id is empty"
		}

		if rs.CardType != model.CardTypePixCPF &&
			rs.CardType != model.CardTypePixCNPJ &&
			rs.CardType != model.CardTypePixPHONE &&
			rs.CardType != model.CardTypePixEMAIL &&
			rs.CardType != model.CardTypePixEVP {
			return false, fmt.Sprintf("invalid card type %v", rs.CardType)
		}

		//if rs.CardType == model.CardTypePixPHONE {
		//	if rs.BankCard[0] != '+' {
		//		rs.BankCard = "+55" + rs.BankCard
		//	}
		//}
	}
	if rs.Email == "" {
		rs.Email = fmt.Sprintf("%v.rummyjosh.com", rs.Phone)
	}
	if rs.Address == "" {
		return false, "address is empty"
	}
	return true, ""
}

func (rs *PayOutValidator) PlatFormValid() (res bool, msg string) {
	if rs.AppOrderId == "" {
		return false, "app_order_id is empty"
	}
	if rs.Amount == 0 {
		return false, "amount is empty"
	}
	if rs.Amount < 20 && rs.PayType != model.PT_PIX { // 巴西PIX提现不检测最小值
		return false, "amount too small"
	}
	if rs.Amount > 50000 {
		return false, "amount too big"
	}
	rs.UserName = strings.TrimSpace(rs.UserName)
	if rs.UserName == "" {
		return false, "user_name is empty"
	}
	rs.Phone = strings.TrimSpace(rs.Phone)
	if len(rs.Phone) < 6 {
		return false, "phone invalid"
	}
	if rs.PayType == "" {
		return false, "pay_type is empty"
	}
	if rs.PayType == model.PT_BANK {
		if rs.BankCard == "" {
			return false, "bankcard is empty"
		}

		switch rs.Country {
		case model.Brazil:
			if rs.DocumentValue == "" {
				return false, "document is empty"
			}

			if rs.BankCode == "" {
				return false, "bank code is empty"
			}

			//if rs.BankName == "" {
			//	return false, "bank name is empty"
			//}

			//if rs.Birthday == "" {
			//	rs.Birthday = "2000-11-06"
			//}

		case model.Mexico:

		case model.Pakistan:
			if rs.BankCode == "" {
				return false, "bank code is empty"
			}
		default:
			// 默认印度
			if rs.IFSC == "" {
				return false, "ifsc is empty"
			}

			reg, _ := regexp.Compile("[a-zA-Z]{4}0[a-zA-Z0-9]{6}")
			if !reg.MatchString(rs.IFSC) {
				return false, "ifsc is invalid"
			}
			regCard, _ := regexp.Compile("^\\d{9,18}$")
			if !regCard.MatchString(rs.BankCard) {
				return false, "bank_card is invalid"
			}
		}
	}
	if rs.PayType == model.PT_UPI && rs.VPA == "" {
		return false, "vpa is empty"
	}
	if rs.PayType == model.PT_CLABE && (rs.BankCard == "" || rs.BankCode == "") {
		return false, "bankcard or code is empty"
	}
	if rs.PayType == model.PT_PIX {
		if rs.BankCard == "" {
			return false, "bankcard id is empty"
		}

		if rs.CardType != model.CardTypePixCPF &&
			rs.CardType != model.CardTypePixCNPJ &&
			rs.CardType != model.CardTypePixPHONE &&
			rs.CardType != model.CardTypePixEMAIL &&
			rs.CardType != model.CardTypePixEVP {
			return false, fmt.Sprintf("invalid card type %v", rs.CardType)
		}

		//if rs.CardType == model.CardTypePixPHONE {
		//	if rs.BankCard[0] != '+' {
		//		rs.BankCard = "+55" + rs.BankCard
		//	}
		//}
	}
	rs.Email = strings.TrimSpace(rs.Email)
	if rs.Email == "" {
		rs.Email = rs.Phone + "@gmail.com"
	}
	if rs.Address == "" {
		rs.Address = rs.UserName
	}
	return true, ""
}
