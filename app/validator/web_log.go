package validator

type WebLogValidator struct {
	Action   string `json:"action"`
	Channel  string `json:"channel"`
	PayAppId string `json:"pay_app_id"`
	OrderId  string `json:"order_id"`
	IP       string `json:"ip"`
}

func (rs *WebLogValidator) Valid() (res bool, msg string) {
	if rs.Action == "" {
		return false, "action is empty"
	}
	if rs.Channel == "" {
		return false, "channel is empty"
	}
	//if rs.PayAppId == "" {
	//	return false, "pay_app_id is empty"
	//}
	if rs.OrderId == "" {
		return false, "order is empty"
	}
	return true, ""
}
