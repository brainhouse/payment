package service

import (
	"fmt"
	"funzone_pay/dao"
	"funzone_pay/model"
	"github.com/wonderivan/logger"
	"time"
	"xorm.io/xorm"
)

type UPIService struct {
	DB *xorm.Engine
}

var UPIServ *UPIService

func GetUPIService() *UPIService {
	if UPIServ == nil {
		db, _ := dao.GetMysql()
		UPIServ = &UPIService{
			DB: db,
		}

	}
	return UPIServ
}

func (us *UPIService) VerifyVPA(orderId string, VPA string) (bool, error) {
	payEntry := new(model.PayEntry)
	exist, err := us.DB.Where("order_id=?", orderId).Get(payEntry)
	if err != nil {
		return false, err
	}
	if !exist {
		logger.Error("UPIService_VerifyVPA_OrderNotExist | orderId=%v", orderId)
		return false, nil
	}
	fmt.Println(payEntry)
	payIns, err := PayAccountConfServe.GetSpecificUPIPayInstance(payEntry.PayChannel, payEntry.PayAccountId)
	if err != nil {
		return false, err
	}
	upiInfo := payIns.VerifyVPA(VPA)
	return upiInfo.Status, nil
}

func (us *UPIService) DoCollect(orderId string, VPA string) (model.PayStatus, error) {
	payEntry := new(model.PayEntry)
	exist, err := us.DB.Where("order_id=?", orderId).Get(payEntry)
	if err != nil {
		return model.PAYFAILD, err
	}
	if !exist {
		logger.Error("UPIService_DoCollect_OrderNotExist | orderId=%v", orderId)
		return model.PAYFAILD, nil
	}
	payIns, err := PayAccountConfServe.GetSpecificUPIPayInstance(payEntry.PayChannel, payEntry.PayAccountId)
	if err != nil {
		return model.PAYFAILD, err
	}
	err = payIns.DoPay(orderId, payEntry.Amount, VPA)
	return model.PAYING, err
}

func (us *UPIService) GetPayStatus(orderId string) (model.PayStatus, error) {
	payEntry := new(model.PayEntry)
	exist, err := us.DB.Where("order_id=?", orderId).Get(payEntry)
	if err != nil {
		return model.PAYFAILD, err
	}
	if !exist {
		logger.Error("UPIService_DoCollect_OrderNotExist | orderId=%v", orderId)
		return model.PAYFAILD, nil
	}
	if payEntry.Updated > (time.Now().Unix() - 10) {
		return model.PAYING, nil
	}
	if payEntry.Status == model.PAYSUCCESS {
		return model.PAYSUCCESS, nil
	}
	//payIns, err := PayAccountConfServe.GetSpecificUPIPayInstance(payEntry.PayChannel, payEntry.PayAccountId)
	//if err != nil {
	//	return model.PAYFAILD, err
	//}
	//resEntry := payIns.GetPayStatus(orderId)
	return model.PAYING, nil
}
