package service

import (
	"encoding/json"
	"errors"
	"fmt"
	"funzone_pay/app/validator"
	"funzone_pay/dao"
	"funzone_pay/model"
	"math/rand"
	"sync"
	"time"

	"github.com/wonderivan/logger"
	"xorm.io/xorm"
)

var SettlementLock sync.Mutex

type settleStruct struct {
	SettleMoney float64
	CountryCode int
}

func UserCollectSettlement(payAccountId string, uid int64, settleAmount float64, adminUser string, start, end int64) {
	SettlementLock.Lock()
	defer SettlementLock.Unlock()
	logger.Debug("UserCollectSettlement_start | uid=%v|%v | settleAmount=%v", uid, payAccountId, settleAmount)

	list := QueryUserSettlementList(uid, payAccountId, start, end)
	if len(list) == 0 {
		return
	}

	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("CollectUserSettlement_DBError | err=%v", err)
		return
	}
	logger.Debug("S_CollectUserSettlementStart | len=%v", len(list))
	settleData := make(map[string]float64)
	userSettleData := make(map[int64]settleStruct)             //用户结算额
	userSettleDataDetail := make(map[int64]map[string]float64) //用户结算额详细
	var (
		total float64
		num   int64
	)
	for _, v := range list {
		if v.Uid == 0 {
			continue
		}
		if settleAmount > 0 && total > settleAmount {
			//如果只结算指定金额
			logger.Warn("CollectUserSettlement_Transaction ToMax | %v | %v | %v > %v", adminUser, uid, total, settleAmount)
			break
		}
		//开始结算
		_, err := engine.Transaction(func(session *xorm.Session) (i interface{}, e error) {
			var exist bool
			account := new(model.Account)
			exist, e = session.Where("uid=?", v.Uid).ForUpdate().Get(account)
			if e != nil {
				return
			}
			if !exist {
				e = errors.New(fmt.Sprintf("AccountNotExist"))
				return
			}
			settleMoney := v.Amount - v.Fee
			//未结算的不能减到负
			if (account.UnsettledBalance - settleMoney) < 0 {
				e = errors.New(fmt.Sprintf("UnsettledBalanceErr"))
				return
			}
			//修改为已结算
			rowNum, e := session.Where("id=?", v.Id).Update(&model.PayEntry{
				SettleStatus: 1,
			})
			if e != nil {
				return
			}
			if rowNum == 0 {
				e = errors.New(fmt.Sprintf("RepeatSettlement"))
				return
			}
			unsettleBalance := account.UnsettledBalance - settleMoney
			balance := account.Balance + settleMoney
			//修改用户账户余额
			_, e = session.Where("uid=?", v.Uid).Incr("balance", settleMoney).Decr("unsettled_balance", settleMoney).Update(&model.Account{})
			if e != nil {
				return
			}
			_, e = session.Insert(&model.AccountTransactions{
				Uid:                  v.Uid,
				DataId:               v.Id,
				Type:                 model.TransSettlement,
				Amount:               settleMoney,
				StartUnSettleBalance: account.UnsettledBalance,
				EndUnSettleBalance:   unsettleBalance,
				StartBalance:         account.Balance,
				EndBalance:           balance,
			})

			total += settleMoney
			num += 1
			settleData[fmt.Sprint(v.PayAccountId)] += settleMoney
			vd, ok := userSettleData[v.Uid]
			if ok {
				vd.SettleMoney += settleMoney
			} else {
				vd = settleStruct{
					CountryCode: v.CountryCode,
					SettleMoney: settleMoney,
				}
			}
			userSettleData[v.Uid] = vd

			if _, ok := userSettleDataDetail[v.Uid]; !ok {
				userSettleDataDetail[v.Uid] = make(map[string]float64)
			}
			userSettleDataDetail[v.Uid][fmt.Sprint(v.PayAccountId)] += settleMoney
			return
		})

		logger.Debug("CollectUserSettlement_Transaction | err=%v | data=%v", err, v)
	}
	//增加结算记录
	ext, _ := json.Marshal(settleData)
	_, err = engine.Insert(&model.SettlementHistory{
		Total:        total,
		Nums:         num,
		PayAccountId: fmt.Sprintf("-%d", uid),
		AdminUser:    adminUser,
		Ext:          string(ext),
	})
	if err != nil {
		logger.Error("CollectUserSettlement_InsertHistoryErr | err=%v", err)
	}
	//增加用户结算记录
	for u, v := range userSettleData {
		ext, _ := json.Marshal(userSettleDataDetail[u])
		userSettle := &model.UserSettlementHistory{
			Uid:         u,
			Total:       v.SettleMoney,
			AdminUser:   adminUser,
			Ext:         string(ext),
			CountryCode: v.CountryCode,
		}
		_, err = engine.Insert(userSettle)
		if err != nil {
			logger.Error("CollectUserSettlement_UserInsertHistoryErr | err=%v | data=%v", err, userSettle)
		}
	}
	logger.Debug("S_CollectUserSettlementEnd | uid=%v", uid)
}

func QueryUserSettlementList(uid int64, payAccountId string, start, end int64) (list []*model.PayEntry) {
	if uid < 1 {
		return nil
	}

	session := UserSettleQuerySession(uid, payAccountId, start, end)
	if session == nil {
		logger.Error("CollectUserSettlement_Session_Nil | uid=%v", uid)
		return
	}
	err := session.Limit(3000).Find(&list)
	if err != nil {
		logger.Error("CollectUserSettlement_Query_Error | err=%v | uid = %d", err, uid)
		return
	}
	return
}

func CollectSettlement(payAccountId string, settleAmount float64, adminUser string) {
	SettlementLock.Lock()
	defer SettlementLock.Unlock()
	logger.Debug("CollectSettlement_start | payAccountId=%v | settleAmount=%v", payAccountId, settleAmount)
	//周末不结算
	//if time.Now().Weekday() == time.Saturday || time.Now().Weekday() == time.Sunday {
	//	logger.Error("CollectSettlement_Ignore | day=%v", time.Now().Weekday())
	//	//	return
	//}
	//获得结算列表
	list := QuerySettlementList(payAccountId)
	if len(list) == 0 {
		return
	}
	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("CollectSettlement_DBError | err=%v", err)
		return
	}
	logger.Debug("S_CollectSettlementStart | len=%v", len(list))
	settleData := make(map[string]float64)
	userSettleData := make(map[int64]settleStruct)             //用户结算额
	userSettleDataDetail := make(map[int64]map[string]float64) //用户结算额详细
	var (
		total float64
		num   int64
	)
	for _, v := range list {
		if v.Uid == 0 {
			continue
		}
		if settleAmount > 0 && total > settleAmount {
			//如果只结算指定金额
			logger.Warn("CollectSettlement_Transaction ToMax | %v | %v | %v > %v", adminUser, payAccountId, total, settleAmount)
			break
		}
		//开始结算
		_, err := engine.Transaction(func(session *xorm.Session) (i interface{}, e error) {
			var exist bool
			account := new(model.Account)
			exist, e = session.Where("uid=?", v.Uid).ForUpdate().Get(account)
			if e != nil {
				return
			}
			if !exist {
				e = errors.New(fmt.Sprintf("AccountNotExist"))
				return
			}
			settleMoney := v.Amount - v.Fee
			//未结算的不能减到负
			if (account.UnsettledBalance - settleMoney) < 0 {
				e = errors.New(fmt.Sprintf("UnsettledBalanceErr"))
				return
			}
			//修改为已结算
			rowNum, e := session.Where("id=?", v.Id).Update(&model.PayEntry{
				SettleStatus: 1,
			})
			if e != nil {
				return
			}
			if rowNum == 0 {
				e = errors.New(fmt.Sprintf("RepeatSettlement"))
				return
			}
			unsettleBalance := account.UnsettledBalance - settleMoney
			balance := account.Balance + settleMoney
			//修改用户账户余额
			_, e = session.Where("uid=?", v.Uid).Incr("balance", settleMoney).Decr("unsettled_balance", settleMoney).Update(&model.Account{})
			if e != nil {
				return
			}
			_, e = session.Insert(&model.AccountTransactions{
				Uid:                  v.Uid,
				DataId:               v.Id,
				Type:                 model.TransSettlement,
				Amount:               settleMoney,
				StartUnSettleBalance: account.UnsettledBalance,
				EndUnSettleBalance:   unsettleBalance,
				StartBalance:         account.Balance,
				EndBalance:           balance,
			})

			total += settleMoney
			num += 1
			settleData[fmt.Sprint(v.PayAccountId)] += settleMoney
			vd, ok := userSettleData[v.Uid]
			if ok {
				vd.SettleMoney += settleMoney
			} else {
				vd = settleStruct{
					CountryCode: v.CountryCode,
					SettleMoney: settleMoney,
				}
			}
			userSettleData[v.Uid] = vd

			if _, ok := userSettleDataDetail[v.Uid]; !ok {
				userSettleDataDetail[v.Uid] = make(map[string]float64)
			}
			userSettleDataDetail[v.Uid][fmt.Sprint(v.PayAccountId)] += settleMoney
			return
		})

		logger.Debug("CollectSettlement_Transaction | err=%v | data=%v", err, v)
	}
	//增加结算记录
	ext, _ := json.Marshal(settleData)
	_, err = engine.Insert(&model.SettlementHistory{
		Total:        total,
		Nums:         num,
		PayAccountId: payAccountId,
		AdminUser:    adminUser,
		Ext:          string(ext),
	})
	if err != nil {
		logger.Error("CollectSettlement_InsertHistoryErr | err=%v", err)
	}
	//增加用户结算记录
	for u, v := range userSettleData {
		ext, _ := json.Marshal(userSettleDataDetail[u])
		userSettle := &model.UserSettlementHistory{
			Uid:         u,
			Total:       v.SettleMoney,
			AdminUser:   adminUser,
			Ext:         string(ext),
			CountryCode: v.CountryCode,
		}
		_, err = engine.Insert(userSettle)
		if err != nil {
			logger.Error("CollectSettlement_UserInsertHistoryErr | err=%v | data=%v", err, userSettle)
		}
	}
	logger.Debug("S_CollectSettlementEnd | payAccountId=%v", payAccountId)
}

func QuerySettlementList(payAccountId string) (list []*model.PayEntry) {
	session := SettleQuerySession(payAccountId)
	if session == nil {
		logger.Error("CollectSettlement_Session_Nil | payAccountId=%v", payAccountId)
		return
	}
	err := session.Limit(30000).Find(&list)
	if err != nil {
		logger.Error("CollectSettlement_Query_Error | err=%v", err)
		return
	}
	return
}

func QuerySettlementCount(payAccountId string) ([]float64, error) {
	session := SettleQuerySession(payAccountId)
	var (
		res []float64
		err error
	)
	res, err = session.Sums(&model.PayEntry{}, "amount", "fee")
	return res, err
}

type AccountType string

const (
	N0 AccountType = "No"
	D0 AccountType = "D0"
	T0 AccountType = "T0"
	T1 AccountType = "T1"
	T2 AccountType = "T2"
)

var t = map[string]AccountType{
	//"cashfree1":  T0,
	//"cashfree2":  T0,
	//"cashfree13": T0,
	//"cashfree14": T0,
	//"globpay1":   T0,
	//"serpay1":    T0,
	"serpay2": T0,
	//"serpay_br":  T0,
	// "zwpay1": T0,
	//"razorpay2":  T0,
	//"ybpay1":     T0,
	// "loogpay1": T0,

	//"cashfree3":  T1,
	//"cashfree7":  T1,
	//"cashfree9":  T1,
	//"cashfree10": T1,
	//"cashfree11": T1,
	//"cashfree15": T1,
	//"cashfree16": T1,
	//"onionpay4":  T1,
	//"onionpay5":  T1,
	//"onionpay6":  T1,
	//"onionpay7":  T1,
	//"payflash1":  T1,
	//"fmpay1":     T1,
	// "airpay1": T0,
	// "airpay2": T0,
	"airpay3": T0,
	//"pgpay1":     T0,
	//"bhtpay1":    T0,
	"oepay1":    T0,
	"phonepay1": T1,
	"hopepay1":  D0,
	// "cashpay_br": D0,
}

func IsInvalidAccountID(accountId string) bool {
	_, ok := t[accountId]
	if ok {
		return true
	}

	return false
}

func GetPayAccountType(accountId string) AccountType {
	d, ok := t[accountId]
	if !ok {
		return T2
	}

	return d
}

func UserSettleQuerySession(uid int64, payAccountId string, start, end int64) *xorm.Session {
	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("UserSettleQuerySession_DBError | err=%v", err)
		return nil
	}

	timeStr := time.Now().Format("2006-01-02")
	t, _ := time.Parse("2006-01-02", timeStr)
	if end == 0 {
		end = t.Unix() - 86400
	}

	if start == 0 {
		start = end - 12*30*86400
	}

	session := engine.Where("created between ? AND ? AND status=1 AND settle_status=0 AND uid=?", start, end, uid).OrderBy("id")
	if payAccountId != "" {
		session.Where("pay_account_id=?", payAccountId)
	}
	return session
}

func SettleQuerySession(payAccountId string) *xorm.Session {
	if payAccountId == "" {
		logger.Error("SettleQuerySession_PayAccountId is empty")
		return nil
	}

	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("SettleQuerySession_DBError | err=%v", err)
		return nil
	}
	timeStr := time.Now().Format("2006-01-02")
	t, _ := time.Parse("2006-01-02", timeStr)
	var end int64
	accountType := GetPayAccountType(payAccountId)
	switch accountType {
	case T0, D0:
		//T0 || D0
		end = time.Now().Unix()

	case T1:
		//T+1
		end = t.Unix()

	default:
		//T+2
		end = t.Unix() - 86400
	}

	start := end - 6*30*86400
	if payAccountId == "razorpay2" { //
		start = end - 12*30*86400
	} else if payAccountId == "airpay1" || payAccountId == "airpay2" {
		start = end - 18*30*86400
	}
	session := engine.Where("created between ? AND ? AND status=1 AND settle_status=0 AND uid!=0", start, end).OrderBy("id")
	if payAccountId != "" {
		session.Where("pay_account_id=?", payAccountId)
	}
	return session
}

/*
*
客户提现支付
*/
func WithdrawPayment(payChannel model.PayChannel, payAccountId string, amount float64, id int64) error {
	//加个锁防止并发执行
	withDrawKey := fmt.Sprintf("RDLK_WITHDRAW_%v", id)
	err := dao.RedisLock(withDrawKey, "1", 60)
	if err != nil {
		logger.Error("WithdrawPayment_RedisLockErr | id=%v | err=%v", id, err)
		return err
	}
	defer dao.RedisDel(withDrawKey)

	payInstance, _ := PayAccountConfServe.GetSpecificPayInstance(payChannel, payAccountId)
	if payInstance == nil {
		logger.Error("WithdrawPayment_PayChannelError | payAccountId=%v", payAccountId)
		return errors.New("pay channel error")
	}
	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("WithdrawPayment_DBError | err=%v", err)
		return err
	}
	//查询提现请求
	withdraw := new(model.UserWithdrawFlow)
	exist, err := engine.Where("id=?", id).Get(withdraw)
	if err != nil {
		logger.Error("WithdrawPayment_GetDBError | err=%v", err)
		return err
	}
	if !exist {
		return errors.New("NotExists")
	}
	wAmount := withdraw.WithdrawAmount - withdraw.WithdrawFee //可提现金额
	surAmount := wAmount - withdraw.PayAmount                 //剩余可提现金额
	if amount > 0 && surAmount < amount {
		return errors.New(fmt.Sprintf("PayAmountError surplusAmount:%v", surAmount))
	} else {
		//如果小于等于0那就取剩余可提现金额
		amount = surAmount
	}

	//随机取数据库中数据
	var randList []*model.PayOut
	_ = engine.Where("created>? AND uid!=0 AND status=1", time.Now().Unix()-7*86400).Limit(100).OrderBy("id").Find(&randList)

	var (
		userName string
		phone    string
		email    string
	)
	if payChannel != model.ONIONPAY && len(randList) > 0 {
		rand.Seed(time.Now().UnixNano())
		num := rand.Intn(len(randList))
		userName = randList[num].UserName
		phone = randList[num].Phone
	} else {
		userName = withdraw.ReceiveName
		phone = "09345060962"
	}

	email = fmt.Sprintf("%v@gmail.com", phone)

	payout, err := payInstance.PayOutBatch(-1, validator.PayOutValidator{
		AppId:      "ff_app_pay",
		AppOrderId: fmt.Sprintf("withdraw_%v_%v_%v", withdraw.Uid, id, time.Now().Unix()),
		PayType:    model.PT_BANK,
		PayChannel: payChannel,
		Amount:     amount,
		UserId:     fmt.Sprint(id),
		BankCard:   withdraw.Account,
		IFSC:       withdraw.IFSC,
		UserName:   userName,
		Phone:      phone,
		Email:      email,
		Address:    "ABC Street",
	})
	if err != nil {
		logger.Error("WithdrawPayment_Error| channel=%v, payAccountId=%v, amount=%v, id=%v | err=%v", payChannel, payAccountId, amount, id, err)
		return err
	}
	PayId := withdraw.PayId
	if payout != nil {
		PayId = fmt.Sprintf("%v%v", PayId, payout.OrderId)
	}
	//更新已提现金额
	_, err = engine.Where("id=?", id).Incr("pay_amount", amount).Update(model.UserWithdrawFlow{
		PayId: PayId,
	})
	if err != nil {
		return err
	}
	return nil
}
