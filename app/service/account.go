package service

import (
	"encoding/json"
	"errors"
	"fmt"
	"funzone_pay/dao"
	"funzone_pay/model"
	"github.com/wonderivan/logger"
	"time"
	"xorm.io/xorm"
)

var AccountServ *AccountService

func NewAccountService() {
	once.Do(func() {
		AccountServ = new(AccountService)
	})
}

const AdminUid = 1

func ServiceInit() {
	NewAccountService()
	PayAccountInit()
	UserServiceInit()
	WebLogInit()
	BlackListServiceInit()
}

type AccountService struct{}

/**
充值计费流程
*/
func (as *AccountService) AddCollect(entry *model.PayEntry) error {
	if entry.Status != model.PAYSUCCESS {
		return nil
	}
	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("AccountService_AddCollect_Err | err=%v | data=%v", err, entry)
		return err
	}
	//取出用户配置
	settings, ok := GetAccountPaymentSettingsFromCache(entry.Uid)
	if !ok {
		err = fmt.Errorf("account payment setting not exist")
		logger.Error("AccountService_AddCollect_GetSettingErr | data=%v", entry)
		return err
	}
	//四舍五入
	orgFee := entry.Amount * settings.FeeRate
	if settings.UserLessMin > 0 { // 已经设置了新的控制条件
		if entry.Amount <= settings.UserLessMin {
			orgFee = settings.UserLessFee
		}
	} else {
		appCfg, ok := PayAccountConfServe.GetAppConfigStructByAccountID(entry.PayAccountId)
		if ok {
			if entry.Amount <= appCfg.PayInMin {
				orgFee = appCfg.PayInMinFee
			}
		} else {
			// 没有按印度算
			if entry.Amount < 100 {
				orgFee = 6
			}
		}
	}

	fee := orgFee
	addMoney := entry.Amount - fee
	_, err = engine.Transaction(func(session *xorm.Session) (i interface{}, e error) {
		account := new(model.Account)
		_, e = session.Where("uid=?", entry.Uid).ForUpdate().Get(account)
		if e != nil {
			return
		}
		unsettleBalance := account.UnsettledBalance + addMoney
		//增加账户流水
		_, e = session.Insert(&model.AccountTransactions{
			Uid:                  entry.Uid,
			DataId:               entry.Id,
			Type:                 model.TransIn,
			Amount:               addMoney,
			StartBalance:         account.Balance,
			EndBalance:           account.Balance,
			StartUnSettleBalance: account.UnsettledBalance,
			EndUnSettleBalance:   unsettleBalance,
		})
		if e != nil {
			return
		}
		//增加账户余额
		_, e = session.Where("uid=?", entry.Uid).Incr("unsettled_balance", addMoney).Update(&model.Account{})
		if e != nil {
			return
		}
		//是否有代理分成
		var agtSettings []*model.RecommendSettings
		e = session.Where("target_uid=?", entry.Uid).Find(&agtSettings)
		if e != nil {
			return
		}
		var totalAgentFee float64
		if len(agtSettings) > 0 {
			for _, v := range agtSettings {
				if v.RecommendUid == AdminUid {
					continue
				}
				agentFee := entry.Amount * v.ChargeRate
				if agentFee == 0 {
					continue
				}
				//没得分了
				if fee < agentFee {
					continue
				}
				totalAgentFee += agentFee
				//增加分成账号收益
				_, e = session.Where("uid=?", v.RecommendUid).Incr("balance", agentFee).Update(&model.Account{})
				if e != nil {
					return
				}
				//增加账号收益流水
				_, e = session.Insert(&model.AccountTransactions{
					Uid:    v.RecommendUid,
					DataId: entry.Id,
					Type:   model.TransAgentInFee,
					Amount: agentFee,
				})
				if e != nil {
					return
				}
				//剩余的给admin
				fee = fee - agentFee
			}
		}

		/****是否有分成规则 - 要拆分收益fee****/
		var coSettings []*model.CorporationSetting
		e = session.Where("pay_channel=? AND pay_account_id=?", entry.PayChannel, entry.PayAccountId).Find(&coSettings)
		if e != nil {
			return
		}
		var totalCoFee float64 //分成总额
		if len(coSettings) > 0 {
			for _, v := range coSettings {
				if v.Uid == AdminUid {
					continue
				}
				coFee := orgFee * v.FeeRate
				if coFee == 0 {
					continue
				}
				////没得分了
				if fee < coFee {
					continue
				}
				totalCoFee += coFee
				//增加分成账号收益
				_, e = session.Where("uid=?", v.Uid).Incr("balance", coFee).Update(&model.Account{})
				if e != nil {
					return
				}
				//增加账号收益流水
				_, e = session.Insert(&model.AccountTransactions{
					Uid:    v.Uid,
					DataId: entry.Id,
					Type:   model.TransCoInFee,
					Amount: coFee,
				})
				if e != nil {
					return
				}
				fee = fee - coFee
			}
		}
		//计费
		_, e = session.Where("id=?", entry.Id).Update(&model.PayEntry{
			SuccessTime:  time.Now().Unix(),
			Fee:          orgFee, //原始总共收取的费用
			CooperateFee: totalCoFee,
			AgentFee:     totalAgentFee,
		})
		if e != nil {
			return
		}
		if fee > 0 {
			var uid int64 = AdminUid
			if entry.CountryCode > 0 {
				user := new(model.User)
				q := "parent_id=? and country_code=?"
				exist := false
				exist, e = session.Where(q, AdminUid, entry.CountryCode).Get(user)
				if e != nil {
					return
				}
				if !exist {
					logger.Error("GetUser NotExist %v", q)
					e = errors.New("UserChannelNotExist")
					return
				}

				uid = user.Id
			}
			//增加平台收益
			_, e = session.Where("uid=?", uid).Incr("balance", fee).Update(&model.Account{})
			//增加平台收益流水
			_, e = session.Insert(&model.AccountTransactions{
				Uid:    uid,
				DataId: entry.Id,
				Type:   model.TransAdminInFee,
				Amount: fee,
			})
			if e != nil {
				return
			}
		}
		return
	})
	if err != nil {
		logger.Error("AccountService_AddCollect_Update_Err | err=%v | data=%v", err, entry)
	}
	return nil
}

/**
创建提现
*/
func (as *AccountService) PayOut(uid int64, amount float64) (int64, error) {
	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("AccountService_PayOut_Err | err=%v | uid=%v | amount=%v", err, uid, amount)
		return 0, err
	}
	frozenId, err := engine.Transaction(func(session *xorm.Session) (i interface{}, e error) {
		//判断用户账户额度是否充足
		account := new(model.Account)
		exist, e := session.Where("uid=?", uid).ForUpdate().Get(account)
		if !exist || e != nil {
			return 0, err
		}
		if account.Balance-amount < 0 {
			return 0, errors.New("balance_insufficient")
		}

		//// 开始写入流水记录，然后冻结金额
		//frozen := model.AccountTransactions{
		//	Uid:                  uid,
		//	DataId:               0,
		//	Amount:               amount,
		//	Type:                 model.TransFrozen,
		//	StartBalance:         account.Balance,
		//	EndBalance:           account.Balance - amount,
		//	StartUnSettleBalance: account.UnsettledBalance,
		//	EndUnSettleBalance:   account.UnsettledBalance,
		//}
		//
		//_, e = session.Insert(frozen)
		//if e != nil {
		//	return
		//}

		//扣除账户余额
		_, e = session.Where("uid=?", uid).Decr("balance", amount).Update(&model.Account{})
		if e != nil {
			return 0, e
		}
		//插入冻结记录
		frozenData := new(model.AccountFrozenTransactions)
		frozenData.Uid = uid
		frozenData.Amount = amount
		frozenData.Status = model.FrozenDefault
		_, e = session.Insert(frozenData)
		return frozenData.Id, e
	})
	if err != nil {
		return 0, err
	}
	return frozenId.(int64), nil
}

/**
提现回调修改
*/
func (as *AccountService) CallBackPayOut(payOut *model.PayOut) error {
	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("AccountService_CallBackPayOut_Err | err=%v | data=%v", err, payOut)
		return err
	}
	if payOut.Status == model.PAY_OUT_FAILD {
		_, err = engine.Transaction(func(session *xorm.Session) (i interface{}, e error) {
			//冻结记录修改
			var rows int64
			rows, e = session.Where("data_id=? and status = ?", payOut.Id, model.FrozenDefault).Update(&model.AccountFrozenTransactions{
				Status: model.FrozenRelease,
			})
			if e != nil {
				e = errors.New(fmt.Sprintf("RELEASE Status Error ：%v ", err.Error()))
				return
			}
			if rows == 0 {
				e = errors.New("REPEAT_RELEASE")
				return
			}

			// 开始写入流水记录，然后自动回滚冻结金额
			account := new(model.Account)
			_, e = session.Where("uid=?", payOut.Uid).ForUpdate().Get(account)
			if e != nil {
				return
			}

			reverse := model.AccountTransactions{
				Uid:                  payOut.Uid,
				DataId:               payOut.Id,
				Amount:               payOut.Amount,
				Type:                 model.TransAutoReversed,
				StartBalance:         account.Balance,
				EndBalance:           account.Balance + payOut.Amount,
				StartUnSettleBalance: account.UnsettledBalance,
				EndUnSettleBalance:   account.UnsettledBalance,
			}

			_, e = session.Insert(reverse)
			if e != nil {
				return
			}

			//回滚用户额度
			_, e = session.Where("uid=?", payOut.Uid).Incr("balance", payOut.Amount).Update(&model.Account{})
			return
		})
		if err != nil {
			return err
		}
	} else if payOut.Status == model.PAY_OUT_SUCCESS {
		//取出用户配置
		settings, ok := GetAccountPaymentSettingsFromCache(payOut.Uid)
		if !ok {
			logger.Error("AccountService_CallBackPayOut_GetSettingErr | account payment setting not exist | data=%v", payOut)
			err = fmt.Errorf("account payment setting not exist")
			return err
		}
		//四舍五入
		orgFee := payOut.Amount * settings.PayoutFeeRate
		if settings.UserWithdrawLess > 0 { // 新的配置条件
			if payOut.Amount <= settings.UserWithdrawLess {
				orgFee = settings.UserWithdrawLessFee
			}
		} else {
			appCfg, ok := PayAccountConfServe.GetAppConfigStructByAccountID(payOut.PayAccountId)
			if ok {
				if payOut.Amount <= appCfg.PayOutMin {
					orgFee = appCfg.PayOutMinFee
				}
			} else {
				//没有配置，就按印度，小于100固定收6卢比
				if payOut.Amount < 200 {
					orgFee = 6
				}
			}
		}

		feeDesc := model.FeeDesc{}
		// 提现额外添加一个小尾巴
		if settings.ClientWithdrawFix > 0 {
			orgFee += settings.ClientWithdrawFix
			feeDesc.Fixed = settings.ClientWithdrawFix
		}

		fee := orgFee
		feeDesc.Fee = fee
		_, err = engine.Transaction(func(session *xorm.Session) (i interface{}, e error) {
			account := new(model.Account)
			_, e = session.Where("uid=?", payOut.Uid).ForUpdate().Get(account)
			if e != nil {
				return
			}
			//增加账户提现流水
			_, e = session.InsertMulti([]model.AccountTransactions{
				{
					Uid:                  payOut.Uid,
					DataId:               payOut.Id,
					Amount:               payOut.Amount, //提现金额
					Type:                 model.TransOut,
					StartBalance:         0,
					EndBalance:           account.Balance,
					StartUnSettleBalance: account.UnsettledBalance,
					EndUnSettleBalance:   account.UnsettledBalance,
				},
				{
					Uid:                  payOut.Uid,
					Amount:               fee,
					DataId:               payOut.Id,
					Type:                 model.TransOutFee, //提现费用
					StartBalance:         account.Balance,
					EndBalance:           account.Balance - fee,
					StartUnSettleBalance: account.UnsettledBalance,
					EndUnSettleBalance:   account.UnsettledBalance,
				},
			})
			if e != nil {
				return
			}
			//余额扣除手续费
			_, e = session.Where("uid=?", account.Uid).Decr("balance", fee).Update(&model.Account{})
			if e != nil {
				return
			}
			//是否有代理分成
			agtSettings := []*model.RecommendSettings{}
			e = session.Where("target_uid=?", payOut.Uid).Find(&agtSettings)
			if e != nil {
				return
			}
			var totalAgentFee float64
			if len(agtSettings) > 0 {
				for _, v := range agtSettings {
					if v.RecommendUid == AdminUid {
						continue
					}
					agentFee := payOut.Amount * v.WithdrawRate
					if agentFee == 0 {
						continue
					}
					//没得分了
					if fee < agentFee {
						continue
					}
					totalAgentFee += agentFee
					//增加分成账号收益
					_, e = session.Where("uid=?", v.RecommendUid).Incr("balance", agentFee).Update(&model.Account{})
					if e != nil {
						return
					}
					//增加账号收益流水
					_, e = session.Insert(&model.AccountTransactions{
						Uid:    v.RecommendUid,
						DataId: payOut.Id,
						Type:   model.TransAgentOutFee,
						Amount: agentFee,
					})
					if e != nil {
						return
					}
					//剩余的给admin
					fee = fee - agentFee
				}
			}

			/****是否有分成规则 - 要拆分收益fee****/
			coSettings := []*model.CorporationSetting{}
			e = session.Where("pay_channel=? AND pay_account_id=?", payOut.PayChannel, payOut.PayAccountId).Find(&coSettings)
			if e != nil {
				return
			}
			var totalCoFee float64
			if len(coSettings) > 0 {
				for _, v := range coSettings {
					if v.Uid == AdminUid {
						continue
					}
					coFee := orgFee * v.PayoutFeeRate
					if coFee == 0 {
						continue
					}
					if fee < coFee {
						continue
					}
					totalCoFee += coFee
					//增加分成账号收益
					_, e = session.Where("uid=?", v.Uid).Incr("balance", coFee).Update(&model.Account{})
					if e != nil {
						return
					}
					//增加账号收益流水
					_, e = session.Insert(&model.AccountTransactions{
						Uid:    v.Uid,
						DataId: payOut.Id,
						Type:   model.TransCoOutFee,
						Amount: coFee,
					})
					if e != nil {
						return
					}
					//剩余的给admin
					fee = fee - coFee
				}
			}

			desc, _ := json.Marshal(feeDesc)
			if len(desc) > 254 {
				desc = desc[0:254]
			}
			//费用更新
			_, e = session.Where("id=?", payOut.Id).Update(&model.PayOut{
				SuccessTime:  time.Now().Unix(),
				Fee:          orgFee, //原始费用
				FeeDesc:      string(desc),
				CooperateFee: totalCoFee,
				AgentFee:     totalAgentFee,
			})

			if fee > 0 {
				var uid int64 = AdminUid
				if payOut.CountryCode > 0 {
					user := new(model.User)
					q := "parent_id=? and country_code=?"
					exist := false
					exist, e = session.Where(q, AdminUid, payOut.CountryCode).Get(user)
					if e != nil {
						return
					}
					if !exist {
						logger.Error("GetUser NotExist %v", q)
						e = errors.New("UserChannelNotExist")
						return
					}

					uid = user.Id
				}

				//增加平台收益
				_, e = session.Where("uid=?", uid).Incr("balance", fee).Update(&model.Account{})
				if e != nil {
					return
				}
				//增加平台收益流水
				_, e = session.Insert(&model.AccountTransactions{
					Uid:    uid,
					DataId: payOut.Id,
					Type:   model.TransAdminOutFee,
					Amount: fee,
				})
				if e != nil {
					return
				}
			}
			return
		})
		if err != nil {
			logger.Error("AccountService_CallBackPayOut_Settle_Error | err = %v | data=%v ", err, payOut)
			return err
		}

	}
	return nil
}

/**
 * 提现成功后回滚
 * @Description:
 * @receiver as
 * @param payout
 * @return error
 */
func (as *AccountService) ReversedPayout(payout *model.PayOut) error {
	//支付成功的，又回退的回滚
	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("AccountService_ReversedPayout_Err | err=%v | data=%v", err, payout)
		return err
	}
	_, err = engine.Transaction(func(session *xorm.Session) (i interface{}, e error) {
		total := payout.Amount + payout.Fee
		_, e = session.Where("uid=?", payout.Uid).Incr("balance", total).Update(&model.Account{})
		if e != nil {
			return
		}
		//商户变动流水
		upList := []model.AccountTransactions{
			{
				Uid:    payout.Uid,
				DataId: payout.Id,
				Amount: total,
				Type:   model.TransReversed,
			},
		}
		var transList []model.AccountTransactions
		e = session.Where("data_id=?", payout.Id).In("type", []model.AccountTSType{model.TransCoOutFee, model.TransAgentOutFee, model.TransAdminOutFee}).Find(&transList)
		if e != nil {
			return
		}
		for _, v := range transList {
			var sType model.AccountTSType
			switch v.Type {
			case model.TransCoOutFee:
				sType = model.TransCoOutFeeRev
			case model.TransAgentOutFee:
				sType = model.TransAgentOutFeeRev
			case model.TransAdminOutFee:
				sType = model.TransAdminReversedFee
			}
			_, e = session.Where("uid=?", v.Uid).Decr("balance", v.Amount).Update(&model.Account{})
			if e != nil {
				return
			}
			//插入流水
			upList = append(upList, model.AccountTransactions{
				Uid:    v.Uid,
				Amount: v.Amount,
				DataId: payout.Id,
				Type:   sType,
			})
		}
		_, e = session.InsertMulti(upList)
		return
	})
	//logger.Error("AccountService_ReversedPayout_Update_Err | err=%v | data=%v", err, payout)
	return err
}

func (as *AccountService) ReversedPayEntry(payEntry *model.PayEntry, isRebackFee bool) error {
	//支付成功的，又回退的回滚
	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("AccountService_ReversedPayEntry_Err | err=%v | data=%v", err, payEntry)
		return err
	}
	thirdDesc := "manual"
	if isRebackFee {
		thirdDesc = ""
	}
	_, err = engine.Transaction(func(session *xorm.Session) (i interface{}, e error) {
		_, e = session.Where("id=? and status=?", payEntry.Id, model.PAYSUCCESS).Update(&model.PayEntry{
			Status:    model.PAYROLLBACK,
			ThirdDesc: thirdDesc,
		})
		if e != nil {
			return
		}
		total := payEntry.Amount
		if isRebackFee { // 退手续费-从客户扣减金额的时候，不算手续费
			total = payEntry.Amount - payEntry.Fee
		}
		if payEntry.SettleStatus == int(model.Settled) { // 结算-从balance里面扣除
			_, e = session.Where("uid=?", payEntry.Uid).Decr("balance", total).Update(&model.Account{})
		} else { // 未结算，从unsettle-balance 里面扣除
			_, e = session.Where("uid=?", payEntry.Uid).Decr("unsettled_balance", total).Update(&model.Account{})
		}

		if e != nil {
			return
		}
		//商户变动流水
		accTransType := model.TransEntryManualReversed
		if isRebackFee { // 退手续费
			accTransType = model.TransEntryReversed
		}
		upList := []model.AccountTransactions{
			{
				Uid:    payEntry.Uid,
				DataId: payEntry.Id,
				Amount: total,
				Type:   accTransType,
			},
		}
		var transList []model.AccountTransactions
		e = session.Where("data_id=?", payEntry.Id).In("type", []model.AccountTSType{model.TransCoInFee, model.TransAgentInFee, model.TransAdminInFee}).Find(&transList)
		if e != nil {
			return
		}
		for _, v := range transList {
			var sType model.AccountTSType
			switch v.Type {
			case model.TransCoInFee:
				sType = model.TransCoEntryFeeRev
			case model.TransAgentInFee:
				sType = model.TransAgentEntryFeeRev
			case model.TransAdminInFee:
				sType = model.TransAdminEntryRevFee
			}
			_, e = session.Where("uid=?", v.Uid).Decr("balance", v.Amount).Update(&model.Account{})
			if e != nil {
				return
			}
			//插入流水
			upList = append(upList, model.AccountTransactions{
				Uid:    v.Uid,
				Amount: v.Amount,
				DataId: payEntry.Id,
				Type:   sType,
			})
		}
		_, e = session.InsertMulti(upList)
		return
	})
	return err
}

func (as *AccountService) InquiryAccount(uid int64) (*model.Account, error) {
	unix := time.Now().Unix()
	if !CheckSpeed("inquiry-account", unix, 20) {
		return nil, errors.New("call too frequency, wait a ment")
	}

	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("AccountService_Inquiry_Err | err=%v | uid=%v", err, uid)
		return nil, err
	}
	account := new(model.Account)
	exist, err := engine.Where("uid=?", uid).Get(account)
	if err != nil {
		logger.Error("AccountService_Inquiry_DB_Err | err=%v | uid=%v", err, uid)
		return nil, err
	}
	if !exist {
		logger.Error("AccountService_Inquiry_NotExist | uid=%v", uid)
		return nil, errors.New("AccountNotExist")
	}

	return account, nil
}
