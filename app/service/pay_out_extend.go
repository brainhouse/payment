package service

import (
	"fmt"
	"funzone_pay/channel/cashpay"
	"funzone_pay/channel/hopepay"
	"funzone_pay/common"
	"funzone_pay/dao"
	"funzone_pay/model"
	"funzone_pay/utils"
	"github.com/wonderivan/logger"
	"time"
)

func (po *PayOutService) YBPayOutCallback(cb *model.YBPayoutCallback) (resKey string) {
	//签名校验 todo

	//更新状态
	payOut := new(model.PayOut)
	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("PayOutService_YBPayOutCallback_DBError | err=%v | cb=%+v", err, cb)
		return "DB_ERROR"
	}
	exist, err := engine.Where("order_id=?", cb.MerOrderNo).Get(payOut)
	if err != nil {
		logger.Error("PayOutService_YBPayOutCallback_Query_DBError | err=%v | cb=%v", err, cb)
		return "DB_ERROR"
	}
	if !exist {
		logger.Error("PayOutService_YBPayOutCallback_NotExist | err=%v | cb=%v", err, cb)
		return "DB_ERROR"
	}
	//签名校验 todo
	//if payOut.Uid != 0 || payOut.AppId == "funzone_pay" {
	//	payIns, err := PayAccountConfServe.GetSpecificPayInstance(model.ONIONPAY, payOut.PayAccountId)
	//	if err != nil {
	//		logger.Error("PayOutService_YBPayOutCallback_PlatformInstance | err=%v | cb=%v", err, cb)
	//		return "DB_ERROR"
	//	}
	//	if !payIns.OutSignature(cb) {
	//		logger.Error("PayOutService_YBPayOutCallback_PlatformOutSignature_Error | cb=%+v", cb)
	//		return "SIGN_ERROR"
	//	}
	//} else {
	//	return "SIGN_ERROR"
	//}
	if payOut.Status == model.PAY_OUT_SUCCESS && cb.OrderStatus == "5" {
		//要回退金额
		payOut.Status = model.PAY_OUT_REVERSED
		err = AccountServ.ReversedPayout(payOut)
		if err != nil {
			logger.Error("PayOutService_YBPayOutCallback_Reversed_err | err=%v | cb=%v", err, cb)
		}
	} else {
		/**
		订单状态:-1=订单异常,0=待处理,1=转账处理中,2=转账拒绝,3=转账失败,4=转账成功,5=转账撤销(5状态特殊，会先返回4 ，再返回5；)
		*/
		//已经修改不再变化
		if payOut.Status != model.PAY_OUT_ING {
			logger.Error("PayOutService_YBPayOutCallback_Repeat | err=%v | cb=%v", err, cb)
			return "REPEAT_CALLBACK"
		}
		if cb.OrderStatus == "4" {
			payOut.Status = model.PAY_OUT_SUCCESS
			payOut.PaymentId = cb.OrderNo
			payOut.FinishTime = time.Now().Unix()
		} else if cb.OrderStatus == "2" || cb.OrderStatus == "3" {
			payOut.Status = model.PAY_OUT_FAILD
			payOut.FinishTime = time.Now().Unix()
		}
	}
	rowCount, err := engine.Where("id=?", payOut.Id).Update(payOut)
	if err != nil {
		logger.Error("PayOutService_YBPayOutCallback_Update_DBError | err=%v", err, cb)
		return "DB_ERROR"
	}
	logger.Debug("PayOutService_YBPayOutCallback_Info | payOut=%v | rowCount=%v", payOut, rowCount)
	if rowCount > 0 {
		//发起回调
		go po.SendPayOutCallback(payOut)
	}
	return ""
}

func (po *PayOutService) YDGPayOutCallback(cb *model.YDGPayoutCallback) (resKey string) {
	//签名校验 todo

	//更新状态
	payOut := new(model.PayOut)
	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("PayOutService_YDGPayOutCallback_DBError | err=%v | cb=%+v", err, cb)
		return "DB_ERROR"
	}
	exist, err := engine.Where("order_id=?", cb.MerTradeNo).Get(payOut)
	if err != nil {
		logger.Error("PayOutService_YDGPayOutCallback_Query_DBError | err=%v | cb=%v", err, cb)
		return "DB_ERROR"
	}
	if !exist {
		logger.Error("PayOutService_YDGPayOutCallback_NotExist | err=%v | cb=%v", err, cb)
		return "DB_ERROR"
	}
	//签名校验 todo
	//if payOut.Uid != 0 || payOut.AppId == "funzone_pay" {
	//	payIns, err := PayAccountConfServe.GetSpecificPayInstance(model.ONIONPAY, payOut.PayAccountId)
	//	if err != nil {
	//		logger.Error("PayOutService_YBPayOutCallback_PlatformInstance | err=%v | cb=%v", err, cb)
	//		return "DB_ERROR"
	//	}
	//	if !payIns.OutSignature(cb) {
	//		logger.Error("PayOutService_YBPayOutCallback_PlatformOutSignature_Error | cb=%+v", cb)
	//		return "SIGN_ERROR"
	//	}
	//} else {
	//	return "SIGN_ERROR"
	//}
	//暂时没有这种状态
	if payOut.Status == model.PAY_OUT_SUCCESS && cb.OrderStatus == "111111" {
		//要回退金额
		payOut.Status = model.PAY_OUT_REVERSED
		err = AccountServ.ReversedPayout(payOut)
		if err != nil {
			logger.Error("PayOutService_YDGPayOutCallback_Reversed_err | err=%v | cb=%v", err, cb)
		}
	} else {
		/**
		订单状态:
		0	处理中
		1	成功
		2	失败
		*/
		//已经修改不再变化
		if payOut.Status != model.PAY_OUT_ING {
			logger.Error("PayOutService_YDGPayOutCallback_Repeat | err=%v | cb=%v", err, cb)
			return "REPEAT_CALLBACK"
		}
		if cb.OrderStatus == "1" {
			payOut.Status = model.PAY_OUT_SUCCESS
			payOut.PaymentId = cb.OrderNo
			payOut.FinishTime = time.Now().Unix()
		} else if cb.OrderStatus == "2" {
			payOut.Status = model.PAY_OUT_FAILD
			payOut.FinishTime = time.Now().Unix()
		}
	}
	rowCount, err := engine.Where("id=?", payOut.Id).Update(payOut)
	if err != nil {
		logger.Error("PayOutService_YDGPayOutCallback_Update_DBError | err=%v", err, cb)
		return "DB_ERROR"
	}
	logger.Debug("PayOutService_YDGPayOutCallback_Info | payOut=%v | rowCount=%v", payOut, rowCount)
	if rowCount > 0 {
		//发起回调
		go po.SendPayOutCallback(payOut)
	}
	return ""
}

func (po *PayOutService) BHTPayOutCallback(cb *model.BHTPayout) (resKey string) {
	//签名校验 todo
	if cb.Data.RefID == "" {
		return "NOT_EXIST"
	}
	//更新状态
	payOut := new(model.PayOut)
	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("PayOutService_BHTPayOutCallback_DBError | err=%v | cb=%+v", err, cb)
		return "DB_ERROR"
	}
	exist, err := engine.Where("id=? and pay_channel='bhtpay'", cb.Data.RefID).Get(payOut)
	if err != nil {
		logger.Error("PayOutService_BHTPayOutCallback_Query_DBError | err=%v | cb=%v", err, cb)
		return "DB_ERROR"
	}
	if !exist {
		logger.Error("PayOutService_BHTPayOutCallback_NotExist | err=%v | cb=%v", err, cb)
		return "NOT_EXIST"
	}
	//签名校验 todo
	//if payOut.Uid != 0 || payOut.AppId == "funzone_pay" {
	//	payIns, err := PayAccountConfServe.GetSpecificPayInstance(model.ONIONPAY, payOut.PayAccountId)
	//	if err != nil {
	//		logger.Error("PayOutService_YBPayOutCallback_PlatformInstance | err=%v | cb=%v", err, cb)
	//		return "DB_ERROR"
	//	}
	//	if !payIns.OutSignature(cb) {
	//		logger.Error("PayOutService_YBPayOutCallback_PlatformOutSignature_Error | cb=%+v", cb)
	//		return "SIGN_ERROR"
	//	}
	//} else {
	//	return "SIGN_ERROR"
	//}

	//已经修改不再变化
	if payOut.Status != model.PAY_OUT_ING {
		logger.Error("PayOutService_BHTPayOutCallback_Repeat | err=%v | cb=%v", err, cb)
		return "REPEAT_CALLBACK"
	}
	if cb.Data.Status == "SUCCESS" {
		payOut.Status = model.PAY_OUT_SUCCESS
		payOut.PaymentId = cb.Data.OrderID
		payOut.FinishTime = time.Now().Unix()
	} else if cb.Data.Status == "FAILED" {
		payOut.Status = model.PAY_OUT_FAILD
		payOut.FinishTime = time.Now().Unix()
	}
	rowCount, err := engine.Where("id=?", payOut.Id).Update(payOut)
	if err != nil {
		logger.Error("PayOutService_BHTPayOutCallback_Update_DBError | err=%v", err, cb)
		return "DB_ERROR"
	}
	logger.Debug("PayOutService_BHTPayOutCallback_Info | payOut=%v | rowCount=%v", payOut, rowCount)
	if rowCount > 0 {
		//发起回调
		go po.SendPayOutCallback(payOut)
	}
	return ""
}

func (po *PayOutService) OEPayOutCallback(cb *model.OEPayout) (resKey string) {
	//签名校验 todo
	//更新状态
	payOut := new(model.PayOut)
	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("PayOutService_OEPayOutCallback_DBError | err=%v | cb=%+v", err, cb)
		return "DB_ERROR"
	}
	exist, err := engine.Where("order_id=?", cb.OrderNo).Get(payOut)
	if err != nil {
		logger.Error("PayOutService_OEPayOutCallback_Query_DBError | err=%v | cb=%v", err, cb)
		return "DB_ERROR"
	}
	if !exist {
		logger.Error("PayOutService_OEPayOutCallback_NotExist | err=%v | cb=%v", err, cb)
		return "NOT_EXIST"
	}

	// 状态相同不需要更新
	if payOut.Status == model.PAY_OUT_SUCCESS && cb.Status == "1" || payOut.Status == model.PAY_OUT_FAILD && cb.Status == "0" {
		return ""
	}

	oldStatus := payOut.Status

	// 先成功后失败，需要回滚
	if payOut.Status == model.PAY_OUT_SUCCESS && cb.Status == "0" {
		payOut.Status = model.PAY_OUT_REVERSED
		payOut.ThirdDesc = "revert success to fail, base new callback"
		err = AccountServ.ReversedPayout(payOut)
		if err != nil {
			logger.Error("PayOutService_OEPayOutCallback_Reversed_err | err=%v | cb=%v", err, cb)
			return fmt.Sprintf("Reversed[%v] fail", payOut.OrderId)
		}

		logger.Debug("OEPay_ReversedPayout_Success | data=%v", cb)
	} else if payOut.Status == model.PAY_OUT_FAILD && cb.Status == "1" {
		// 先失败后成功
		common.PushAlarmEvent(common.Warning, fmt.Sprintf("%v", payOut.AppId),
			fmt.Sprintf("OEPay/OEPayOutCallback/%v", payOut.OrderId), "",
			fmt.Sprintf("%v-%v Payout[%v] From Fail To Success", payOut.AppId, payOut.OrderId, payOut.Amount))

		logger.Error("PayOutService_OEPayOutCallback_FailToSuccess_err | err=%v | cb=%v", err, cb)
		return fmt.Sprintf("fail-->success [%v] fail", payOut.OrderId)
	} else {
		if cb.Status == "1" {
			payOut.Status = model.PAY_OUT_SUCCESS
			payOut.PaymentId = cb.UtrNo
			payOut.FinishTime = time.Now().Unix()
		} else if cb.Status == "0" {
			payOut.Status = model.PAY_OUT_FAILD
			payOut.FinishTime = time.Now().Unix()
		}
	}

	// 状态没更新
	if payOut.Status == oldStatus {
		return ""
	}

	rowCount, err := engine.Where("id=?", payOut.Id).Update(payOut)
	if err != nil {
		logger.Error("PayOutService_OEPayOutCallback_Update_DBError | err=%v", err, cb)
		return "DB_ERROR"
	}
	logger.Debug("PayOutService_OEPayOutCallback_Info | payOut=%v | rowCount=%v", payOut, rowCount)
	if rowCount > 0 {
		//发起回调
		go po.SendPayOutCallback(payOut)
	}
	return ""
}

func (po *PayOutService) HDPayOutCallback(cb *model.HDPayout) (resKey string) {
	//$concatenate = $payout_id.$payout_reference.$account_number.$key.$account_ifsc.$amount.$UTR;
	//$checksum = hash_hmac('sha256', $concatenate, $key);
	sk := "aafz202304F"
	raw := fmt.Sprintf("%v%v%v%v%v%v%v", cb.Data.PayoutID, cb.Data.Reference, cb.Data.BeneficiaryAccountNumber, sk, cb.Data.BeneficiaryAccountIfsc, cb.Data.Amount, cb.Data.UTR)
	if utils.HmacSha256ToXStringWithKey(raw, sk) != cb.Data.Checksum {
		logger.Error("PayOutService_HDPayOutCallback_CheckSign[%v|%v] err", cb.Data.PayoutID, cb.Data.Reference)
		return "invalid sign"
	}

	//更新状态
	payOut := new(model.PayOut)
	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("PayOutService_HDPayOutCallback_DBError | err=%v | cb=%+v", err, cb)
		return "DB_ERROR"
	}
	exist, err := engine.Where("order_id=?", cb.Data.Reference).Get(payOut)
	if err != nil {
		logger.Error("PayOutService_HDPayOutCallback_Query_DBError | err=%v | cb=%v", err, cb)
		return "DB_ERROR"
	}
	if !exist {
		logger.Error("PayOutService_HDPayOutCallback_NotExist | err=%v | cb=%v", err, cb)
		return "NOT_EXIST"
	}

	// 多次同状态通知不处理
	if payOut.Status == model.PAY_OUT_SUCCESS && (cb.Status == "success" || cb.Status == "SUCCESS") ||
		payOut.Status == model.PAY_OUT_FAILD && (cb.Status == "failed" || cb.Status == "FAILED") {
		return ""
	}

	if payOut.Status != model.PAY_OUT_ING {
		logger.Error("PayOutService_HDPayOutCallback_Repeated | cb=%v", cb)
		return "REPEATED"
	}

	oldStatus := payOut.Status
	if cb.Status == "success" || cb.Status == "SUCCESS"{
		payOut.Status = model.PAY_OUT_SUCCESS
		payOut.Utr = cb.Data.UTR
		payOut.FinishTime = time.Now().Unix()
	} else if cb.Status == "failed" || cb.Status == "FAILED" {
		payOut.Status = model.PAY_OUT_FAILD
		payOut.ThirdCode = cb.Status
		payOut.ThirdDesc = cb.Data.Message
		payOut.FinishTime = time.Now().Unix()
		logger.Error("PayOutService_HDPayOutCallback_failed | cb=%v", cb)
	}

	// 状态没更新
	if payOut.Status == oldStatus {
		return "REPEATED 2"
	}

	rowCount, err := engine.Where("id=?", payOut.Id).Update(payOut)
	if err != nil {
		logger.Error("PayOutService_HDPayOutCallback_Update_DBError | err=%v", err, cb)
		return "DB_ERROR"
	}
	logger.Debug("PayOutService_HDPayOutCallback_Info | payOut=%v | rowCount=%v", payOut, rowCount)
	if rowCount > 0 {
		//发起回调
		go po.SendPayOutCallback(payOut)
	}
	return ""
}

func (po *PayOutService) HopePayOutCallbackNew(cb *hopepay.HPOutCallBack) (resKey string) {
	//更新状态
	payOut := new(model.PayOut)
	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("PayOutService_HopePayOutCallback_DBError | err=%v | cb=%+v", err, cb)
		return "DB_ERROR"
	}
	exist, err := engine.Where("order_id=?", cb.MerTradeNo).Get(payOut)
	if err != nil {
		logger.Error("PayOutService_HopePayOutCallback_Query_DBError | err=%v | cb=%v", err, cb)
		return "DB_ERROR"
	}
	if !exist {
		logger.Error("PayOutService_HopePayOutCallback_NotExist | err=%v | cb=%v", err, cb)
		return "NOT_EXIST"
	}

	ins, err := PayAccountConfServe.GetSpecificPayInstance(model.HOPEPAY, payOut.PayAccountId)
	if err != nil {
		logger.Error("PayOutService_HopePayOutCallback Channel Not Exist")
		return fmt.Sprintf("Channel[%v-%v] Not Exist", model.HOPEPAY, payOut.PayAccountId)
	}

	if !ins.OutSignature(cb.Map()) {
		logger.Error("PayOutService_HopePayOutCallback Check PayOut Callback Sign Fail")
		return "Sign Not Match"
	}

	// 多次同状态通知不处理
	if payOut.Status == model.PAY_OUT_SUCCESS && cb.OrderStatus == hopepay.PayStatusSuccess ||
		payOut.Status == model.PAY_OUT_FAILD && cb.OrderStatus == hopepay.PayStatusFail ||
		payOut.Status == model.PAY_OUT_REVERSED && cb.OrderStatus == hopepay.PayStatusFail ||
		cb.OrderStatus == hopepay.PayStatusPending {
		return ""
	}

	if payOut.Status != model.PAY_OUT_ING {
		logger.Error("PayOutService_HopePayOutCallback_Repeated | cb=%v", cb)
		return "REPEATED"
	}

	oldStatus := payOut.Status
	if cb.OrderStatus == hopepay.PayStatusSuccess {
		payOut.Status = model.PAY_OUT_SUCCESS
		payOut.PaymentId = cb.OrderNo
		payOut.FinishTime = time.Now().Unix()
	} else if cb.OrderStatus == hopepay.PayStatusFail {
		payOut.Status = model.PAY_OUT_FAILD
		payOut.FinishTime = time.Now().Unix()
		logger.Error("PayOutService_HopePayOutCallback_failed | cb=%v", cb)
	}

	// 状态没更新
	if payOut.Status == oldStatus {
		return ""
	}

	rowCount, err := engine.Where("id=?", payOut.Id).Update(payOut)
	if err != nil {
		logger.Error("PayOutService_HopePayOutCallback_Update_DBError | err=%v", err, cb)
		return "DB_ERROR"
	}
	logger.Debug("PayOutService_HopePayOutCallback_Info | payOut=%v | rowCount=%v", payOut, rowCount)
	if rowCount > 0 {
		//发起回调
		go po.SendPayOutCallback(payOut)
	}
	return ""
}

func (po *PayOutService) CashPayoutCallback(cb *cashpay.Notification) (resKey string) {
	payOut := new(model.PayOut)
	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("PayOutService_CashPayOutCallback_DBError | err=%v | cb=%+v", err, cb)
		return "DB_ERROR"
	}
	exist, err := engine.Where("order_id=?", cb.MerchantOrderId).Get(payOut)
	if err != nil {
		logger.Error("PayOutService_CashPayOutCallback_Query_DBError | err=%v | cb=%v", err, cb)
		return "DB_ERROR"
	}
	if !exist {
		logger.Error("PayOutService_CashPayOutCallback_NotExist | err=%v | cb=%v", err, cb)
		return "NOT_EXIST"
	}

	isAlarm := true
	defer func() {
		if isAlarm {
			common.PushAlarmEvent(common.Warning, fmt.Sprintf("%v", payOut.AppId), "CashPayoutCallback", "", fmt.Sprintf("%v-%v-%v CashPayoutCallback error: %v", payOut.AppId, payOut.Uid, cb.MerchantOrderId, err))
		}
	}()

	//
	ins, err := PayAccountConfServe.GetInstance(payOut.PayAccountId, model.CASHPAY)
	if err != nil {
		return fmt.Sprintf("Unsupported AccountId %v, %v", payOut.PayAccountId,cb.MerchantOrderId)
	}

	cashPay, ok := ins.(*cashpay.CashPayService)
	if !ok {
		return fmt.Sprintf("assert AccountId %v, %v", payOut.PayAccountId,cb.MerchantOrderId)
	}

	res, err := cashPay.Query(cb.MerchantOrderId)
	if err != nil {
		return fmt.Sprintf("query cashpay error %v, %v", payOut.PayAccountId, cb.MerchantOrderId)
	}


	status := res.Status

	// pending
	if status == cashpay.StatusPending || status == cashpay.StatusWaiting {
		isAlarm = false
		return ""
	}

	//
	oldOrderStatus := payOut.Status

	if oldOrderStatus == model.PAY_OUT_SUCCESS { // 先成功，后失败
		if status == cashpay.StatusFail || status == cashpay.StatusExpired || status == cashpay.StatusRefund {
			// 退款
			payOut.Status = model.PAY_OUT_REVERSED
			payOut.ThirdDesc = fmt.Sprintf("revert success to fail, base new callback-->%v", res.TraceId)
			err = AccountServ.ReversedPayout(payOut)
			if err != nil {
				logger.Error("CashPay_ReversedPayout_Error| err=%v,data=%v|%v|%v|%v", err, res.Status, cb.OrderId, cb.MerchantOrderId, cb.Amount)
				return fmt.Sprintf("Reversed[%v] fail", payOut.OrderId)
			}

			logger.Debug("CashPay_ReversedPayout_Success | data=%v|%v|%v|%v", res.Status, cb.OrderId, cb.MerchantOrderId, cb.Amount)
		}
	} else if oldOrderStatus == model.PAY_OUT_ING { // pending
		if status == cashpay.StatusSuccess {
			payOut.Status = model.PAY_OUT_SUCCESS
			payOut.ThirdDesc = res.TraceId
		} else if status == cashpay.StatusFail || status == cashpay.StatusExpired || status == cashpay.StatusRefund {
			payOut.Status = model.PAY_OUT_FAILD
			payOut.ThirdCode = res.Status
			payOut.ThirdDesc = fmt.Sprintf("%v-->%v", res.TraceId, res.Msg)
		}
	} else { //
		if oldOrderStatus == model.PAY_OUT_FAILD && status == cashpay.StatusSuccess { // 先失败再成功
			err = fmt.Errorf("invalid order status,order=%v,%v | cb=%v,%v", payOut.OrderId, payOut.Status, cb.OrderId, res.Status)
			logger.Error("PayOutService_CashPayOutCallback_Error Invalid Notify | err=%v", err)
			return ""
		}
	}

	if oldOrderStatus == payOut.Status {
		isAlarm = false
		return ""
	}

	payOut.FinishTime = time.Now().Unix()
	rowCount, err := engine.Where("id=? and status=?", payOut.Id, oldOrderStatus).Update(payOut)
	if err != nil {
		logger.Error("PayOutService_CashPayOutCallback_Update_DBError | err=%v", err, cb)
		return "DB_ERROR"
	}
	logger.Debug("PayOutService_CashPayOutCallback_Info | payOut=%v | rowCount=%v", payOut, rowCount)
	if rowCount > 0 {
		//发起回调
		go po.SendPayOutCallback(payOut)
	}

	isAlarm = false
	return ""
}