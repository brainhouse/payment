package service

import (
	"fmt"
	"funzone_pay/channel/cashpay"
	"funzone_pay/channel/hopepay"
	"funzone_pay/channel/phonepay"
	"funzone_pay/channel/worldline"
	"funzone_pay/common"
	"funzone_pay/dao"
	"funzone_pay/model"
	"net/url"
	"strconv"
	"time"

	"github.com/wonderivan/logger"
)

func (ps *PayEntryService) YBInCallback(hook *model.YBCollectCallback) string {
	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("PayEntryService_YBInCallback_DBError | err=%v | cb =%v", err, hook)
		return "DB_ERROR"
	}
	payEntry := new(model.PayEntry)
	exist, err := engine.Where("order_id=?", hook.MerOrderNo).Get(payEntry)
	if err != nil {
		logger.Error("PayEntryService_YBInCallback_Query_DBError | err=%v | cb=%v", err, hook)
		return "DB_ERROR"
	}
	if !exist {
		logger.Error("PayEntryService_YBInCallback_NotExist | err=%v | cb=%v", err, hook)
		return "DB_ERROR"
	}

	logger.Info("PayEntryService_YBInCallback | data=%v | cb=%v", payEntry, hook)
	// 退款
	//if hook.OrderStatus == model.RefundsProcessed {
	//	if payEntry.Status == model.PAYSUCCESS {
	//		err = AccountServ.ReversedPayEntry(payEntry) //要回退金额
	//		if err != nil {
	//			logger.Error("PayEntryService_WebHookCallback_Refund | id=%v | err=%v | cb=%v", payEntry.Id, err, hook)
	//			return "Reverse Failure"
	//		}
	//	}
	//已经修改不再变化
	oldStatus := payEntry.Status
	if oldStatus == model.PAYSUCCESS && hook.OrderStatus == "3" {
		logger.Error("PayEntryService_YBInCallback_Repeat | id=%v | err=%v | cb=%v", payEntry.Id, err, hook)
		msg := fmt.Sprintf("YBIn callback repeated order[%v] success->fail", payEntry.OrderId)
		common.PushAlarmEvent(common.Warning, "YBIn", "YBInCallback", hook.MerNo, msg)

		return "REPEAT_CALLBACK"
	}
	switch hook.OrderStatus {
	//订单付款成功
	case "4":
		payEntry.PaymentId = hook.OrderNo
		payEntry.Status = model.PAYSUCCESS
		payEntry.FinishTime = time.Now().Unix()
	case "3":
		payEntry.Status = model.PAYFAILD
		payEntry.FinishTime = time.Now().Unix()
	default:
		return ""
	}

	if oldStatus == payEntry.Status {
		return ""
	}

	//发起回调
	_, err = engine.Where("id=?", payEntry.Id).Update(payEntry)
	if err != nil {
		logger.Error("PayEntryService_YBInCallback_Err | id = %v | err=%v", payEntry.Id, err)
		return "DB_ERROR"
	}
	go ps.SendEntryCallback(payEntry)

	return ""
}

func (ps *PayEntryService) YDGInCallback(hook *model.YDGCollectCallback) string {
	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("PayEntryService_YDGInCallback_DBError | err=%v | cb =%v", err, hook)
		return "DB_ERROR"
	}
	payEntry := new(model.PayEntry)
	exist, err := engine.Where("order_id=?", hook.MerTradeNo).Get(payEntry)
	if err != nil {
		logger.Error("PayEntryService_YDGInCallback_Query_DBError | err=%v | cb=%v", err, hook)
		return "DB_ERROR"
	}
	if !exist {
		logger.Error("PayEntryService_YDGInCallback_NotExist | err=%v | cb=%v", err, hook)
		return "DB_ERROR"
	}

	logger.Info("PayEntryService_YDGInCallback | data=%v | cb=%v", payEntry, hook)
	// 退款
	//if hook.OrderStatus == model.RefundsProcessed {
	//	if payEntry.Status == model.PAYSUCCESS {
	//		err = AccountServ.ReversedPayEntry(payEntry) //要回退金额
	//		if err != nil {
	//			logger.Error("PayEntryService_WebHookCallback_Refund | id=%v | err=%v | cb=%v", payEntry.Id, err, hook)
	//			return "Reverse Failure"
	//		}
	//	}
	//已经修改不再变化
	if payEntry.Status != model.PAYING {
		logger.Error("PayEntryService_YDGInCallback_Repeat | id=%v | err=%v | cb=%v", payEntry.Id, err, hook)
		return "REPEAT_CALLBACK"
	}
	switch hook.OrderStatus {
	//订单付款成功
	case "1":
		payEntry.PaymentId = hook.OrderNo
		payEntry.Status = model.PAYSUCCESS
		payEntry.FinishTime = time.Now().Unix()
	case "2":
		payEntry.Status = model.PAYFAILD
		payEntry.FinishTime = time.Now().Unix()
	default:
		return ""
	}

	//发起回调
	_, err = engine.Where("id=?", payEntry.Id).Update(payEntry)
	if err != nil {
		logger.Error("PayEntryService_YDGInCallback_Err | id = %v | err=%v", payEntry.Id, err)
		return "DB_ERROR"
	}
	go ps.SendEntryCallback(payEntry)

	return ""
}

func (ps *PayEntryService) AirInCallback(hook url.Values) string {
	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("PayEntryService_AirInCallback_DBError | err=%v | cb =%v", err, hook)
		return "DB_ERROR"
	}
	orderId := hook.Get("TRANSACTIONID")
	payEntry := new(model.PayEntry)
	exist, err := engine.Where("order_id=?", orderId).Get(payEntry)
	if err != nil {
		logger.Error("PayEntryService_AirInCallback_Query_DBError | err=%v | cb=%v", err, hook)
		return "DB_ERROR"
	}
	if !exist {
		logger.Error("PayEntryService_AirInCallback_NotExist | err=%v | cb=%v", err, hook)
		return "DB_ERROR"
	}
	orderStatus := hook.Get("TRANSACTIONPAYMENTSTATUS")
	paymentId := hook.Get("APTRANSACTIONID")
	logger.Info("PayEntryService_AirInCallback | data=%v | cb=%v", payEntry, hook)

	ins, err := PayAccountConfServe.GetSpecificPayInstance(model.AIRPAY, payEntry.PayAccountId)
	if err != nil {
		logger.Error("PayEntryService_AirInCallback_PayInstance[%v] Error | err=%v | cb=%v", payEntry.PayAccountId, err, hook)
		return "Instance ERROR"
	}

	// check hash fail
	if !ins.InSignature(hook) {
		logger.Error("PayEntryService_AirInCallback_CheckSign err | cb=%v", hook)
		msg := fmt.Sprintf("air_callback_sign_error [%v][%v] amount[%v] sign error", payEntry.Uid, payEntry.OrderId, payEntry.Amount)
		common.PushAlarmEvent(common.Warning, "airpay", "air_callback_sign_error", hook.Encode(), msg)
		//return "Sign ERROR"
	}

	// 退款
	//if hook.OrderStatus == model.RefundsProcessed {
	//	if payEntry.Status == model.PAYSUCCESS {
	//		err = AccountServ.ReversedPayEntry(payEntry) //要回退金额
	//		if err != nil {
	//			logger.Error("PayEntryService_WebHookCallback_Refund | id=%v | err=%v | cb=%v", payEntry.Id, err, hook)
	//			return "Reverse Failure"
	//		}
	//	}
	//已经修改
	oldStatus := payEntry.Status

	if payEntry.Status != model.PAYING {
		eq := false
		if payEntry.Status == model.PAYSUCCESS && orderStatus == "SUCCESS" || payEntry.Status == model.PAYFAILD && orderStatus == "FAIL" {
			eq = true
		}
		logger.Error("PayEntryService_AirInCallback_Repeat | id=%v | amount=%v,status=%v | cb=%v | eq=%v", payEntry.Id, payEntry.Amount, payEntry.Status, hook, eq)
	}

	amount := hook.Get("AMOUNT")
	amountFloat, _ := strconv.ParseFloat(amount, 64)
	if int64(amountFloat*100) != int64(payEntry.Amount*100) { // 订单金额不匹配
		logger.Error("PayEntryService_AirInCallback_Error | data=%v | cb=%v", payEntry, hook)
		msg := fmt.Sprintf("air callback order[%v] amount[%v] != %v", payEntry.OrderId, payEntry.Amount, amount)
		common.PushAlarmEvent(common.Warning, "airpay", "air_callback", hook.Encode(), msg)
		return "ORDER_AMOUNT_ERROR"
	}

	// 先成功后失败，alarm提醒
	if payEntry.Status == model.PAYSUCCESS {
		logger.Error("PayEntryService_AirInCallback_Repeated | data=%v | cb=%v", payEntry, orderStatus)
		if orderStatus == "FAIL" || orderStatus == "REFUNDED" { // 回滚退款
			err = AccountServ.ReversedPayEntry(payEntry, true)
			if err != nil {
				msg := fmt.Sprintf("air callback to rollback order[%v] reverse [%v]", payEntry.OrderId, err.Error())
				common.PushAlarmEvent(common.Warning, "airpay", "air_callback", hook.Encode(), msg)
				return "Reverse Fail"
			}

			if orderStatus == "REFUNDED" {
				payEntry.ThirdDesc = "refunded"
			}

			payEntry.Status = model.PAYROLLBACK
		} else {
			msg := fmt.Sprintf("air callback more than one times [%v]->%v", payEntry.OrderId, orderStatus)
			common.PushAlarmEvent(common.Warning, "airpay", "air_callback_more", hook.Encode(), msg)
			return ""
		}
	} else if payEntry.Status == model.PAYROLLBACK {
		logger.Error("PayEntryService_AirInCallback_Reverse Twice | data=%v | cb=%v", payEntry, orderStatus)
		msg := fmt.Sprintf("air callback error, order[%v] reverse twice[%v]", payEntry.OrderId, orderStatus)
		common.PushAlarmEvent(common.Warning, "airpay", "air_callback", hook.Encode(), msg)
		return "reverse twice"
	} else {
		switch orderStatus {
		//订单付款成功
		case "SUCCESS":
			payEntry.PaymentId = paymentId
			payEntry.Status = model.PAYSUCCESS
			payEntry.FinishTime = time.Now().Unix()
		case "FAIL", "REFUNDED":
			payEntry.Status = model.PAYFAILD
			payEntry.FinishTime = time.Now().Unix()
			if orderStatus == "REFUNDED" {
				payEntry.ThirdDesc = "refunded"
			}
		default:
			return ""
		}
	}

	// 状态没有变化
	if oldStatus == payEntry.Status {
		return ""
	}

	//发起回调
	if payEntry.Status != model.PAYROLLBACK {
		_, err = engine.Where("id=? and status=?", payEntry.Id, oldStatus).Update(payEntry)
		if err != nil {
			logger.Error("PayEntryService_AirInCallback__Err | id = %v | err=%v", payEntry.Id, err)
			return "DB_ERROR"
		}
	}
	go ps.SendEntryCallback(payEntry)

	return ""
}

func (ps *PayEntryService) ClickNCashCallback(hook url.Values) string {
	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("PayEntryService_ClickNCashCallback_DBError | err=%v | cb =%v", err, hook)
		return "DB_ERROR"
	}
	orderId := hook.Get("OrderId")
	status := hook.Get("Status")
	bankRef := hook.Get("bank_ref_num")
	if orderId == "" {
		return "ORDER_ID_EMPTY"
	}
	payEntry := new(model.PayEntry)
	exist, err := engine.Where("order_id=?", orderId).Get(payEntry)
	if err != nil {
		logger.Error("PayEntryService_ClickNCashCallback_Query_DBError | err=%v | cb=%v", err, hook)
		return "DB_ERROR"
	}
	if !exist {
		logger.Error("PayEntryService_ClickNCashCallback_NotExist | err=%v | cb=%v", err, hook)
		return "DB_ERROR"
	}

	logger.Info("PayEntryService_ClickNCashCallback | data=%v | cb=%v", payEntry, hook)
	// 退款
	//if hook.OrderStatus == model.RefundsProcessed {
	//	if payEntry.Status == model.PAYSUCCESS {
	//		err = AccountServ.ReversedPayEntry(payEntry) //要回退金额
	//		if err != nil {
	//			logger.Error("PayEntryService_WebHookCallback_Refund | id=%v | err=%v | cb=%v", payEntry.Id, err, hook)
	//			return "Reverse Failure"
	//		}
	//	}
	//已经修改不再变化
	if payEntry.Status != model.PAYING {
		logger.Error("PayEntryService_ClickNCashCallback_Repeat | id=%v | err=%v | cb=%v", payEntry.Id, err, hook)
		return "REPEAT_CALLBACK"
	}
	switch status {
	//订单付款成功
	case "success":
		payEntry.PaymentId = bankRef
		payEntry.Status = model.PAYSUCCESS
		payEntry.FinishTime = time.Now().Unix()
	case "fail":
		payEntry.Status = model.PAYFAILD
		payEntry.FinishTime = time.Now().Unix()
	default:
		return ""
	}

	//发起回调
	_, err = engine.Where("id=?", payEntry.Id).Update(payEntry)
	if err != nil {
		logger.Error("PayEntryService_ClickNCashCallbacks_Err | id = %v | err=%v", payEntry.Id, err)
		return "DB_ERROR"
	}
	go ps.SendEntryCallback(payEntry)

	return ""
}

func (ps *PayEntryService) PGPayCallback(hook *model.PGPayCollect) string {
	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("PayEntryService_PGPayCallback_DBError | err=%v | cb =%v", err, hook)
		return "DB_ERROR"
	}
	payEntry := new(model.PayEntry)
	exist, err := engine.Where("order_id=?", hook.UniqueRequestID).Get(payEntry)
	if err != nil {
		logger.Error("PayEntryService_PGPayCallback_Query_DBError | err=%v | cb=%v", err, hook)
		return "DB_ERROR"
	}
	if !exist {
		logger.Error("PayEntryService_PGPayCallback_NotExist | err=%v | cb=%v", err, hook)
		return "DB_ERROR"
	}

	logger.Info("PayEntryService_PGPayCallback | data=%v | cb=%v", payEntry, hook)
	// 退款
	//if hook.OrderStatus == model.RefundsProcessed {
	//	if payEntry.Status == model.PAYSUCCESS {
	//		err = AccountServ.ReversedPayEntry(payEntry) //要回退金额
	//		if err != nil {
	//			logger.Error("PayEntryService_WebHookCallback_Refund | id=%v | err=%v | cb=%v", payEntry.Id, err, hook)
	//			return "Reverse Failure"
	//		}
	//	}
	//已经修改不再变化
	if payEntry.Status != model.PAYING {
		logger.Error("PayEntryService_PGPayCallback_Repeat | id=%v | err=%v | cb=%v", payEntry.Id, err, hook)
		return "REPEAT_CALLBACK"
	}
	switch hook.OrderStatus {
	//订单付款成功
	case "1":
		payEntry.PaymentId = hook.PaymentTransactionID
		payEntry.Status = model.PAYSUCCESS
		payEntry.FinishTime = time.Now().Unix()
	case "2":
	case "5":
	case "6":
	case "7":
		payEntry.Status = model.PAYFAILD
		payEntry.FinishTime = time.Now().Unix()
	default:
		return ""
	}

	//发起回调
	_, err = engine.Where("id=?", payEntry.Id).Update(payEntry)
	if err != nil {
		logger.Error("PayEntryService_PGPayCallback_Err | id = %v | err=%v", payEntry.Id, err)
		return "DB_ERROR"
	}
	go ps.SendEntryCallback(payEntry)

	return ""
}

func (ps *PayEntryService) BHTUpiPayCallback(hook *model.BHTUPICollect) string {
	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("PayEntryService_PGPayCallback_DBError | err=%v | cb =%v", err, hook)
		return "DB_ERROR"
	}
	payEntry := new(model.PayEntry)
	exist, err := engine.Where("order_id=?", fmt.Sprintf("BHT%v", hook.Data.OrderID)).Get(payEntry)
	if err != nil {
		logger.Error("PayEntryService_BHTUpiPayCallback_Query_DBError | err=%v | cb=%v", err, hook)
		return "DB_ERROR"
	}
	if !exist {
		logger.Error("PayEntryService_BHTUpiPayCallback_NotExist | err=%v | cb=%v", err, hook)
		return "DB_ERROR"
	}

	logger.Info("PayEntryService_BHTUpiPayCallback | data=%v | cb=%v", payEntry, hook)
	// 退款
	//if hook.OrderStatus == model.RefundsProcessed {
	//	if payEntry.Status == model.PAYSUCCESS {
	//		err = AccountServ.ReversedPayEntry(payEntry) //要回退金额
	//		if err != nil {
	//			logger.Error("PayEntryService_WebHookCallback_Refund | id=%v | err=%v | cb=%v", payEntry.Id, err, hook)
	//			return "Reverse Failure"
	//		}
	//	}
	//已经修改不再变化
	if payEntry.Status != model.PAYING {
		logger.Error("PayEntryService_BHTUpiPayCallback_Repeat | id=%v | err=%v | cb=%v", payEntry.Id, err, hook)
		return "REPEAT_CALLBACK"
	}
	switch hook.Data.Status {
	//订单付款成功
	case "SUCCESS":
		payEntry.PaymentId = fmt.Sprint(hook.Data.UpiTxnID)
		payEntry.Status = model.PAYSUCCESS
		payEntry.FinishTime = time.Now().Unix()
	case "FAILED":
		payEntry.Status = model.PAYFAILD
		payEntry.FinishTime = time.Now().Unix()
	default:
		return ""
	}

	//发起回调
	_, err = engine.Where("id=?", payEntry.Id).Update(payEntry)
	if err != nil {
		logger.Error("PayEntryService_BHTUpiPayCallback_Err | id = %v | err=%v", payEntry.Id, err)
		return "DB_ERROR"
	}
	go ps.SendEntryCallback(payEntry)

	return ""
}

func (ps *PayEntryService) OEPayCallback(hook *model.OECollect) string {
	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("PayEntryService_OEPayCallback_DBError | err=%v | cb =%v", err, hook)
		return "DB_ERROR"
	}
	payEntry := new(model.PayEntry)
	exist, err := engine.Where("order_id=?", hook.OrderNo).Get(payEntry)
	if err != nil {
		logger.Error("PayEntryService_OEPayCallback_Query_DBError | err=%v | cb=%v", err, hook)
		return "DB_ERROR"
	}
	if !exist {
		logger.Error("PayEntryService_OEPayCallback_NotExist | err=%v | cb=%v", err, hook)
		return "DB_ERROR"
	}

	logger.Info("PayEntryService_OEPayCallback | data=%v | cb=%v", payEntry, hook)
	// 退款
	//if hook.OrderStatus == model.RefundsProcessed {
	//	if payEntry.Status == model.PAYSUCCESS {
	//		err = AccountServ.ReversedPayEntry(payEntry) //要回退金额
	//		if err != nil {
	//			logger.Error("PayEntryService_WebHookCallback_Refund | id=%v | err=%v | cb=%v", payEntry.Id, err, hook)
	//			return "Reverse Failure"
	//		}
	//	}
	//已经修改不再变化
	if payEntry.Status != model.PAYING {
		logger.Error("PayEntryService_OEPayCallback_Repeat | id=%v | err=%v | cb=%v", payEntry.Id, err, hook)
		return "REPEAT_CALLBACK"
	}
	switch hook.Status {
	//订单付款成功
	case "1":
		payEntry.PaymentId = hook.PlatFormOrderNo
		payEntry.Status = model.PAYSUCCESS
		payEntry.FinishTime = time.Now().Unix()
	case "0":
		payEntry.Status = model.PAYFAILD
		payEntry.FinishTime = time.Now().Unix()
	default:
		return ""
	}

	//发起回调
	_, err = engine.Where("id=?", payEntry.Id).Update(payEntry)
	if err != nil {
		logger.Error("PayEntryService_OEPayCallback_Err | id = %v | err=%v", payEntry.Id, err)
		return "DB_ERROR"
	}
	go ps.SendEntryCallback(payEntry)

	return ""
}

func (po *PayEntryService) HopePayEntryCallback(cb *hopepay.HPCallBack) (resKey string) {
	//更新状态
	payEntry := new(model.PayEntry)
	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("PayEntryService_HopePayIntCallback_DBError | err=%v | cb=%+v", err, cb)
		return "DB_ERROR"
	}
	exist, err := engine.Where("order_id=?", cb.MerTradeNo).Get(payEntry)
	if err != nil {
		logger.Error("PayEntryService_HopePayInCallback_Query_DBError | err=%v | cb=%v", err, cb)
		return "DB_ERROR"
	}
	if !exist {
		logger.Error("PayEntryService_HopePayInCallback_NotExist | err=%v | cb=%v", err, cb)
		return "NOT_EXIST"
	}

	ins, err := PayAccountConfServe.GetSpecificPayInstance(model.HOPEPAY, payEntry.PayAccountId)
	if err != nil {
		logger.Error("PayEntryService_HopePayInCallback Channel Not Exist")
		return fmt.Sprintf("Channel[%v-%v] Not Exist", model.HOPEPAY, payEntry.PayAccountId)
	}

	if !ins.InSignature(cb.Map()) {
		logger.Error("PayEntryService_HopePayInCallback Check PayEntry Callback Sign Fail")
		return "Sign Not Match"
	}

	// 多次同状态通知不处理
	if payEntry.Status == model.PAYSUCCESS && cb.OrderStatus == hopepay.PayStatusSuccess ||
		payEntry.Status == model.PAYFAILD && cb.OrderStatus == hopepay.PayStatusFail ||
		cb.OrderStatus == hopepay.PayStatusPending {
		return ""
	}

	isAlarm := true
	defer func() {
		if isAlarm {
			common.PushAlarmEvent(common.Warning, fmt.Sprintf("%v", payEntry.AppId), "HopePayInCallback", "",
				fmt.Sprintf("%v-%v-%v HopePayInCallback Error: %v", payEntry.AppId, payEntry.Uid, cb.MerchantNo, err))
		}
	}()

	oldStatus := payEntry.Status
	switch payEntry.Status {
	case model.PAYING:
		if cb.OrderStatus == hopepay.PayStatusSuccess {
			payEntry.Status = model.PAYSUCCESS
			payEntry.PaymentId = cb.OrderNo
			payEntry.FinishTime = time.Now().Unix()
		} else if cb.OrderStatus == hopepay.PayStatusFail {
			payEntry.Status = model.PAYFAILD
			payEntry.FinishTime = time.Now().Unix()
		}

	case model.PAYSUCCESS:
		// from success --> fail, need rollback
		if cb.OrderStatus == hopepay.PayStatusFail {
			payEntry.Status = model.PAYROLLBACK
			payEntry.ThirdDesc = fmt.Sprintf("revert success to fail, base new callback-->%v", cb.MerTradeNo)
			err = AccountServ.ReversedPayEntry(payEntry, true)
			if err != nil {
				logger.Error("HopePay_ReversedPayIn_Error| err=%v,data=%v|%v|%v|%v", err, cb.OrderStatus, cb.MerTradeNo, cb.Amount, cb.ActualAmount)
				err = fmt.Errorf("HopePay Reverse Payentry Error:%v", err.Error())
				return fmt.Sprintf("Reversed[%v] fail", payEntry.OrderId)
			}
		}

	case model.PAYFAILD:
		// from fail --> success
		if cb.OrderStatus == hopepay.PayStatusSuccess {
			payEntry.Status = model.PAYSUCCESS
		}

	default:

	}

	// 状态没更新
	if payEntry.Status == oldStatus {
		isAlarm = false
		return ""
	}

	if payEntry.Status != model.PAYROLLBACK {
		payEntry.FinishTime = time.Now().Unix()
		_, err = engine.Where("id=? and status =?", payEntry.Id, oldStatus).Update(payEntry)
		if err != nil {
			logger.Error("PayEntryService_HopePayOutCallback_Update_DBError | err=%v", err, cb)
			return "DB_ERROR"
		}
	}

	logger.Debug("PayEntryService_HopePayOutCallback_Info | payEntry=%v|%v|%v", payEntry.OrderId, payEntry.Amount, payEntry.Status)
	go po.SendEntryCallback(payEntry)
	isAlarm = false
	return ""
}

func (ps *PayEntryService) CashPayInCallback(hook *cashpay.Notification) string {
	payEntry := new(model.PayEntry)
	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("CashPayInCallback_DBError | err=%v | cb=%v|%v|%v|%v", err, hook.MerchantOrderId, hook.OrderId, hook.Amount, hook.Status)
		return "DB_ERROR"
	}
	exist, err := engine.Where("order_id=?", hook.MerchantOrderId).Get(payEntry)
	if err != nil {
		logger.Error("CashPayInCallback_Query_DBError | err=%v | cb=%v|%v|%v|%v", err, hook.MerchantOrderId, hook.OrderId, hook.Amount, hook.Status)
		return "DB_ERROR"
	}
	if !exist {
		logger.Error("CashPayInCallback_NotExist | err=%v | cb=%v|%v|%v|%v", err, hook.MerchantOrderId, hook.OrderId, hook.Amount, hook.Status)
		return "NOT_EXIST"
	}

	isAlarm := true
	defer func() {
		if isAlarm {
			common.PushAlarmEvent(common.Warning, fmt.Sprintf("%v", payEntry.AppId), "CashPayInCallback", "", fmt.Sprintf("%v-%v-%v CashPayoutCallback error: %v", payEntry.AppId, payEntry.Uid, hook.MerchantOrderId, err))
		}
	}()

	//
	ins, err := PayAccountConfServe.GetInstance(payEntry.PayAccountId, model.CASHPAY)
	if err != nil {
		return fmt.Sprintf("Unsupported AccountId %v, %v", payEntry.PayAccountId, hook.MerchantOrderId)
	}

	cashPay, ok := ins.(*cashpay.CashPayService)
	if !ok {
		return fmt.Sprintf("assert AccountId %v, %v", payEntry.PayAccountId, hook.MerchantOrderId)
	}

	res, err := cashPay.Query(hook.MerchantOrderId)
	if err != nil {
		return fmt.Sprintf("query cashpay error %v, %v", payEntry.PayAccountId, hook.MerchantOrderId)
	}

	status := res.Status

	// pending
	if status == cashpay.StatusPending || status == cashpay.StatusWaiting {
		isAlarm = false
		return ""
	}

	//
	oldOrderStatus := payEntry.Status

	if oldOrderStatus == model.PAYFAILD {
		if status == cashpay.StatusSuccess { // 先失败再成功
			payEntry.Status = model.PAYSUCCESS
			payEntry.ThirdDesc = res.TraceId
		}
	} else if oldOrderStatus == model.PAYING { // pending
		if status == cashpay.StatusSuccess {
			payEntry.Status = model.PAYSUCCESS
			payEntry.ThirdDesc = res.TraceId
		} else if status == cashpay.StatusFail || status == cashpay.StatusExpired || status == cashpay.StatusRefund {
			payEntry.Status = model.PAYFAILD
			payEntry.ThirdDesc = fmt.Sprintf("%v-->%v", res.TraceId, res.Msg)
			payEntry.ThirdCode = res.Status
		}
	} else if oldOrderStatus == model.PAYROLLBACK {
		logger.Error("CashPay_Callback Reverse twice | data=%v|%v|%v|%v", res.Status, hook.OrderId, hook.MerchantOrderId, hook.Amount)
		err = fmt.Errorf("CashPay_Callback Reverse Twice[%v]", status)
		return ""
	} else { //
		// 先成功后失败
		if oldOrderStatus == model.PAYSUCCESS && (status == cashpay.StatusFail || status == cashpay.StatusExpired || status == cashpay.StatusRefund) {
			// 退款
			payEntry.Status = model.PAYROLLBACK
			payEntry.ThirdDesc = fmt.Sprintf("revert success to fail, base new callback-->%v", res.TraceId)
			err = AccountServ.ReversedPayEntry(payEntry, true)
			if err != nil {
				logger.Error("CashPay_ReversedPayIn_Error| err=%v,data=%v|%v|%v|%v", err, res.Status, hook.OrderId, hook.MerchantOrderId, hook.Amount)
				return fmt.Sprintf("Reversed[%v] fail", payEntry.OrderId)
			}

			logger.Debug("CashPay_ReversedPayIn_Success | data=%v|%v|%v|%v", res.Status, hook.OrderId, hook.MerchantOrderId, hook.Amount)
		}
	}

	if oldOrderStatus == payEntry.Status {
		isAlarm = false
		return ""
	}

	if payEntry.Status != model.PAYROLLBACK { // rollback 情况下已经下进行了退款ReversedPayEntry
		payEntry.FinishTime = time.Now().Unix()
		_, err = engine.Where("id=? and status=?", payEntry.Id, oldOrderStatus).Update(payEntry)
		if err != nil {
			logger.Error("CashPayInCallback_Update_DBError | err=%v | cb=%v|%v|%v|", err, hook.MerchantOrderId, hook.Amount, hook.Status)
			return "DB_ERROR"
		}
	}

	logger.Debug("CashPayInCallback_Info | payEntry=%v|%v|%v", hook.MerchantOrderId, hook.Amount, hook.Status)
	go ps.SendEntryCallback(payEntry)

	isAlarm = false
	return ""
}

func (ps *PayEntryService) WDLPayInCallback(back worldline.WDLCallBack, data string) string {
	payEntry := new(model.PayEntry)
	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("WDLPayInCallback_DBError | err=%v | cb=%v|%v|%v", err, back.ClntTxnRef, back.TxnStatus, back.TxnMsg)
		return "DB_ERROR"
	}
	exist, err := engine.Where("order_id=?", back.ClntTxnRef).Get(payEntry)
	if err != nil {
		logger.Error("WDLPayInCallback_Query_DBError | err=%v | cb=%v|%v|%v", err, back.ClntTxnRef, back.TxnStatus, back.TxnMsg)
		return "DB_ERROR"
	}
	if !exist {
		logger.Error("WDLPayInCallback_NotExist | err=%v | cb=%v|%v|%v", err, back.ClntTxnRef, back.TxnStatus, back.TxnMsg)
		return "NOT_EXIST"
	}

	isAlarm := true
	defer func() {
		if isAlarm {
			common.PushAlarmEvent(common.Warning, fmt.Sprintf("%v", payEntry.AppId), "WDLPayInCallback", "",
				fmt.Sprintf("%v-%v-%v WDLPayinCallback error: %v", payEntry.AppId, payEntry.Uid, back.ClntTxnRef, err))
		}
	}()

	ins, err := PayAccountConfServe.GetInstance(payEntry.PayAccountId, model.WDLPAY)
	if err != nil {
		return fmt.Sprintf("Unsupported AccountId %v, %v", payEntry.PayAccountId, back.ClntTxnRef)
	}

	wdlPay, ok := ins.(*worldline.WDLineService)
	if !ok {
		return fmt.Sprintf("assert AccountId %v, %v", payEntry.PayAccountId, back.ClntTxnRef)
	}

	// check sign
	if !wdlPay.InSignature(data) {
		return fmt.Sprintf("check signature fail %v, %v", payEntry.AppId, data)
	}

	// new status
	status := back.TxnStatus
	if worldline.IsSuccess(back.TxnStatus) {
		// dual verification 28-06-2023+16:33:22
		tm, err := time.Parse("02-01-2006 15:04:05", back.TpslTxnTime)
		if err != nil {
			tm = time.Now()
		}

		orderResult, err := wdlPay.DualVerification(back.ClntTxnRef, tm.Format("02-01-2006"))
		if err != nil {
			return fmt.Sprintf("DualVerification error %v, %v", payEntry.PayAccountId, back.ClntTxnRef)
		}

		if !worldline.IsSuccess(orderResult) {
			logger.Error("DualVerification check fail %v|%v|%v", payEntry.AppId, orderResult, data)
		}

		status = orderResult
	}

	//
	oldOrderStatus := payEntry.Status

	if oldOrderStatus == model.PAYFAILD {
		if worldline.IsSuccess(status) { // 先失败再成功
			payEntry.Status = model.PAYSUCCESS
			payEntry.ThirdDesc = ""
		}
	} else if oldOrderStatus == model.PAYING { // pending
		if worldline.IsSuccess(status) {
			payEntry.Status = model.PAYSUCCESS

		} else if worldline.IsFailure(status) {
			payEntry.Status = model.PAYFAILD
			payEntry.ThirdDesc = fmt.Sprintf("%v-->%v", back.TxnMsg, back.TxnErrMsg)
			payEntry.ThirdCode = back.TxnStatus
		}
	} else if oldOrderStatus == model.PAYROLLBACK {
		logger.Error("WDLPay_Callback Reverse twice | data=%v|%v|%v|%v", back.ClntTxnRef, back.TxnStatus, back.TxnAmt, back.TxnMsg)
		err = fmt.Errorf("WDLPay_Callback Reverse Twice[%v]", status)
		return ""
	} else { //
		// 先成功后失败
		if oldOrderStatus == model.PAYSUCCESS && worldline.IsFailure(status) {
			// 退款
			payEntry.Status = model.PAYROLLBACK
			payEntry.ThirdDesc = fmt.Sprintf("revert success to fail,%v|%v", back.TxnMsg, back.TxnErrMsg)
			err = AccountServ.ReversedPayEntry(payEntry, true)
			if err != nil {
				logger.Error("WDLPay_ReversedPayIn_Error| err=%v,data=%v|%v|%v|%v", err, back.ClntTxnRef, back.TxnStatus, back.TxnAmt, back.TxnMsg)
				return fmt.Sprintf("Reversed[%v] fail", payEntry.OrderId)
			}

			logger.Debug("WDLPay_ReversedPayIn_Success | data=%v|%v|%v|%v", back.ClntTxnRef, back.TxnStatus, back.TxnAmt, back.TxnMsg)
		}
	}

	if oldOrderStatus == payEntry.Status {
		isAlarm = false
		return ""
	}

	if payEntry.Status != model.PAYROLLBACK { // rollback 情况下已经下进行了退款ReversedPayEntry
		payEntry.FinishTime = time.Now().Unix()
		_, err = engine.Where("id=? and status=?", payEntry.Id, fmt.Sprintf("%v", oldOrderStatus)).Update(payEntry)
		if err != nil {
			logger.Error("WDLPayInCallback_Update_DBError | err=%v | cb=%v|%v|%v", err, back.ClntTxnRef, back.TxnStatus, back.TxnAmt)
			return "DB_ERROR"
		}
	}

	logger.Debug("WDLPayInCallback_Info | payEntry=%v|%v|%v", back.ClntTxnRef, back.TxnStatus, back.TxnAmt)
	go ps.SendEntryCallback(payEntry)

	isAlarm = false
	return ""
}

func (ps *PayEntryService) PhonePayInCallback(back phonepay.InquiryResponse, base64Payload string, xverify string) string {
	payEntry := new(model.PayEntry)
	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("PhonePayInCallback_DBError | err=%v | cb=%v|%v|%v", err, back.Data.MerchantTransactionId, back.Code, back.Message)
		return "DB_ERROR"
	}
	orderId := back.Data.MerchantTransactionId
	exist, err := engine.Where("order_id=?", orderId).Get(payEntry)
	if err != nil {
		logger.Error("PhonePayInCallback_Query_DBError | err=%v | cb=%v|%v|%v", err, orderId, back.Code, back.Message)
		return "DB_ERROR"
	}
	if !exist {
		logger.Error("PhonePayInCallback_NotExist | err=%v | cb=%v|%v|%v", err, orderId, back.Code, back.Message)
		return "NOT_EXIST"
	}

	isAlarm := true
	defer func() {
		if isAlarm {
			common.PushAlarmEvent(common.Warning, fmt.Sprintf("%v", payEntry.AppId), "PhonePayInCallback", "",
				fmt.Sprintf("%v-%v-%v PhonePayinCallback error: %v", payEntry.AppId, payEntry.Uid, orderId, err))
		}
	}()

	if int(payEntry.Amount*100) != back.Data.Amount {
		return fmt.Sprintf("amout not equal")
	}

	ins, err := PayAccountConfServe.GetInstance(payEntry.PayAccountId, model.PhonePay)
	if err != nil {
		return fmt.Sprintf("Unsupported AccountId %v, %v", payEntry.PayAccountId, orderId)
	}

	phonePay, ok := ins.(*phonepay.PhonePayService)
	if !ok {
		return fmt.Sprintf("assert AccountId %v, %v", payEntry.PayAccountId, orderId)
	}

	// check sign
	if phonepay.XVerify(base64Payload, "", phonePay.SALT, phonePay.SALT_INDEX) != xverify {
		return fmt.Sprintf("check signature fail %v, %v", payEntry.AppId, orderId)
	}

	//
	oldOrderStatus := payEntry.Status
	if oldOrderStatus == model.PAYFAILD {
		if phonepay.IsSuccess(back.Code) { // 先失败再成功
			payEntry.Status = model.PAYSUCCESS
			payEntry.PaymentId = back.Data.PaymentInstrument.Utr
			payEntry.ThirdDesc = ""
		}
	} else if oldOrderStatus == model.PAYING { // pending
		if phonepay.IsSuccess(back.Code) {
			payEntry.Status = model.PAYSUCCESS
			payEntry.PaymentId = back.Data.PaymentInstrument.Utr
		} else if phonepay.IsFail(back.Code) {
			payEntry.Status = model.PAYFAILD
			payEntry.ThirdDesc = back.Message
			payEntry.ThirdCode = back.Code
		}
	} else if oldOrderStatus == model.PAYROLLBACK {
		logger.Error("PhonePay_Callback Reverse twice | data=%v|%v|%v|%v", orderId, back.Code, back.Data.Amount, back.Message)
		err = fmt.Errorf("PhonePay_Callback Reverse Twice[%v]", back.Code)
		return ""
	} else { //
		// 先成功后失败
		if oldOrderStatus == model.PAYSUCCESS && phonepay.IsFail(back.Code) {
			// 退款
			payEntry.Status = model.PAYROLLBACK
			payEntry.ThirdDesc = fmt.Sprintf("revert success to fail,%v|%v", back.Message, back.Data.ResponseCode)
			err = AccountServ.ReversedPayEntry(payEntry, true)
			if err != nil {
				logger.Error("PhonePay_ReversedPayIn_Error| err=%v,data=%v|%v|%v|%v", err, orderId, back.Code, back.Data.Amount, back.Message)
				return fmt.Sprintf("Reversed[%v] fail", payEntry.OrderId)
			}

			logger.Debug("PhonePay_ReversedPayIn_Success | data=%v|%v|%v|%v", orderId, back.Code, back.Data.Amount, back.Message)
		}
	}

	if oldOrderStatus == payEntry.Status {
		isAlarm = false
		return ""
	}

	if payEntry.Status != model.PAYROLLBACK { // rollback 情况下已经下进行了退款ReversedPayEntry
		payEntry.FinishTime = time.Now().Unix()
		_, err = engine.Where("id=? and status=?", payEntry.Id, fmt.Sprintf("%v", oldOrderStatus)).Update(payEntry)
		if err != nil {
			logger.Error("PhonePayInCallback_Update_DBError | err=%v | cb=%v|%v|%v", err, orderId, back.Code, back.Data.Amount)
			return "DB_ERROR"
		}
	}

	logger.Debug("PhonePayInCallback_Info | payEntry=%v|%v|%v", orderId, back.Code, back.Data.Amount)
	go ps.SendEntryCallback(payEntry)

	isAlarm = false
	return ""
}
