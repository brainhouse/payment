package service

import (
	"fmt"
	"funzone_pay/app/validator"
	"funzone_pay/boostrap"
	"funzone_pay/dao"
	"funzone_pay/model"
	"testing"
	"time"
)

func TestPayEntryService_CreateInOrder(t *testing.T) {
	boostrap.InitConf()
	payArg := validator.PayEntryValidator{
		PayChannel: model.RAZORPAY,
		AppId:      string(model.RUMMY),
		Amount:     100,
	}
	data, err := GetPayEntryService().CreateInOrder(payArg)
	fmt.Println(data)
	fmt.Println(err)
}

func TestPayEntryService_CreateCollectOrder(t *testing.T) {
	boostrap.InitConf()
	payArg := validator.PayEntryValidator{
		Uid:    4,
		AppId:  "payu-test",
		Amount: 100,
	}
	data, err := GetPayEntryService().CreateCollectOrder(3, payArg)
	fmt.Println(data)
	fmt.Println(err)
}

func TestGetPayEntryService(t *testing.T) {
	engine, _ := dao.GetMysql()
	obj := &model.PayEntry{}
	_, _ = engine.Where("order_id=?", "RZrummy-test1609507476094718").Get(obj)
	obj.Status = model.PAYSUCCESS
	GetPayEntryService().SendEntryCallback(obj)
}

func TestPayEntryService_GetPayOrderStatus(t *testing.T) {
	status, key := GetPayEntryService().GetPayOrderStatus("pay_ERmJm0047GqQXz", "order_EQ9gSTJDxmWAXG")
	fmt.Println(status)
	fmt.Println(key)
}

func TestName(t *testing.T) {
	boostrap.InitLog()
	idleDuration := 3 * time.Second
	idleDelay := time.NewTimer(idleDuration)
	defer idleDelay.Stop()
	for i := 0; i < 3; i++ {
		idleDelay.Reset(idleDuration)
		select {
		case <-idleDelay.C:
			fmt.Println(11)
		}
	}
}

func TestPayChannel(t *testing.T) {
	payServ := new(PayAccountConfService)
	payServ.SetConfig()
}
