package service

import (
	"fmt"
	"funzone_pay/app/validator"
	"funzone_pay/boostrap"
	"funzone_pay/dao"
	"funzone_pay/model"
	"testing"
)

func TestPayOutRisk(t *testing.T) {
	payout := validator.PayOutValidator{
		Amount:   5000,
		PayType:  model.PT_PayTm,
		PayTm:    "",
		BankCard: "12323",
	}
	res, err := RSK.PayOutRisk(payout)
	fmt.Println(res)
	fmt.Println(err)
}

func TestPlatFromOrder(t *testing.T) {
	boostrap.InitConf()
	payArg := validator.PayOutValidator{
		Uid:        3,
		AppOrderId: "test12333",
		AppId:      "rummy-test",
		Amount:     25000,
		UserId:     "11",
		UserName:   "Ganesh balakrishna",
		BankCard:   "10050232835",
		Phone:      "9999999999",
		Email:      "test@cashfree.com",
		IFSC:       "IDFB0080172",
		Address:    "ABC Street",
		PayTm:      "9999999999",
	}
	data, err := GetPayOutService().PlatFromOrder(3, payArg)
	fmt.Println(data)
	fmt.Println(err)
}

func TestPayoutCallback(t *testing.T) {
	engine, _ := dao.GetMysql()
	obj := &model.PayOut{}
	_, _ = engine.Where("order_id=?", "RZ1609509977015306").Get(obj)
	obj.Status = model.PAY_OUT_FAILD
	GetPayOutService().SendPlatFormCallback(obj)
}
