package service

import (
	"errors"
	"funzone_pay/app/validator"
	"funzone_pay/dao"
	"funzone_pay/model"
	"github.com/spf13/viper"
	"github.com/wonderivan/logger"
	"strconv"
	"time"
)

var RSK *RiskControl

type RiskControl struct{}

func init() {
	if RSK == nil {
		RSK = &RiskControl{}
	}
}

/**
 * 提现风控校验
 * param: validator.PayOutValidator payout
 * return: bool
 * return: error
 */
func (rc *RiskControl) PayOutRisk(payout validator.PayOutValidator) (res bool, err error) {
	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("PayOutService_CreateOutOrder_EngineError | err=%v | data=%v", err, payout)
		return false, err
	}
	times := viper.GetInt64("risk.payout_daily_times")
	maxAmount := viper.GetFloat64("risk.payout_max_amount")
	today := time.Date(time.Now().Year(), time.Now().Month(), time.Now().Day(), 0, 0, 0, 0, time.Local).Unix()
	if payout.Amount > maxAmount {
		logger.Error("PayOutRisk_Paytm_Max_Amount | data=%v", payout)
		return false, errors.New("Max_Amount")
	}
	if payout.PayType == model.PT_PayTm {
		poTimes, err := engine.Where("paytm=? AND created>?", payout.PayTm, today).Count(&model.PayOut{})
		if err != nil {
			logger.Error("PayOutRisk_Paytm_DB_ERROR | err=%v | data=%v", err, payout)
			return false, err
		}
		if poTimes > times {
			logger.Error("PayOutRisk_Paytm_Over_Times | data=%v", payout)
			return false, errors.New("Over_Times")
		}
	}
	if payout.PayType == model.PT_BANK {
		cardTimes, err := engine.Where("bank_card=? AND created>?", payout.BankCard, today).Count(&model.PayOut{})
		if err != nil {
			logger.Error("PayOutRisk_BankCard_DB_ERROR | err=%v | data=%v", err, payout)
			return false, err
		}
		if cardTimes > times {
			logger.Error("PayOutRisk_BankCard_Over_Times | data=%v", payout)
			return false, errors.New("Over_Times")
		}
		totalAmount, err := engine.Where("bank_card=? AND created>?", payout.BankCard, today).Sum(&model.PayOut{}, "amount")
		if err != nil {
			logger.Error("PayOutRisk_BankCard_Amount_ERROR | err=%v | data=%v", err, payout)
			return false, err
		}
		if totalAmount > maxAmount {
			logger.Error("PayOutRisk_BankCard_TotalAmountErr | err=%v | data=%v", err, payout)
			return false, err
		}
	}
	return true, nil
}

func (rc *RiskControl) CustomerRisk(payout validator.PayOutValidator, conf []model.RiskControlConfig) (bool, error) {
	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("RiskControl_CustomerRisk_EngineError | err=%v | data=%v", err, payout)
		return false, err
	}
	//判断用户有没有自定义风控
	for _, v := range conf {
		switch v.Type {
		case "bank_card":
			today := time.Date(time.Now().Year(), time.Now().Month(), time.Now().Day(), 0, 0, 0, 0, time.Local).Unix()
			if v.TimeRange == "daily" {
				totalAmount, err := engine.Where("bank_card=? AND created>?", payout.BankCard, today).Sum(&model.PayOut{}, "amount")
				if err != nil {
					logger.Error("PayOutRisk_CustomerRisk_Bank_Amount_ERROR | err=%v | data=%v", err, payout)
					return false, err
				}
				maxAmount, _ := strconv.ParseInt(v.Threshold, 10, 64)
				if int64(totalAmount) > maxAmount {
					logger.Error("PayOutRisk_CustomerRisk_Bank_TotalAmountErr | err=%v | data=%v", err, payout)
					return false, errors.New("PayOutRisk_CustomerRisk_Bank_TotalAmountErr")
				}
			} else if v.TimeRange == "week" {

			}
		case "white_bank_card":
			if payout.BankCard == v.Threshold {
				return true, nil
			}
		}
	}
	return true, nil
}

func (rc *RiskControl) CheckRisk(payout validator.PayOutValidator) (bool, error) {
	var (
		res = true
	)
	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("RiskControl_CheckRisk_EngineError | err=%v | data=%v", err, payout)
		return false, err
	}
	//判断用户有没有自定义风控
	var customConf []model.RiskControlConfig
	err = engine.Where("uid=?", payout.Uid).OrderBy("sort").Find(&customConf)
	if err != nil {
		logger.Error("RiskControl_CheckRisk_EngineError | err=%v | data=%v", err, payout)
		return false, err
	}
	if len(customConf) > 0 {
		res, err = rc.CustomerRisk(payout, customConf)
	} else {
		res, err = rc.PayOutRisk(payout)
	}
	return res, err
}
