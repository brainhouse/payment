package service

import (
	"funzone_pay/dao"
	"funzone_pay/model"
	"github.com/wonderivan/logger"
	"strings"
)

var BlackListServ *BlackListService

type BlackType string

const (
	BlackTypeIP       = "ip"
	BlackTypeBankCard = "bank_card"
	BlackTypeID       = "id"
	BlackTypePhone    = "phone"
)

func BlackListServiceInit() {
	once.Do(func() {
		BlackListServ = new(BlackListService)
	})
}
// NOTE : 数据库中 email、vpa只存储前缀且字母小写

type BlackListService struct {
}

func (bs *BlackListService) IsBlack(data string, Stype BlackType) bool {
	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("BlackListService_Init_Err | err=%v", err)
		return false
	}
	session := engine.Where("data=?", data)
	if Stype != "" {
		session.Where("type=?", Stype)
	}
	exist, err := session.Get(&model.BlackList{})
	if exist || err != nil {
		logger.Error("BlackListService_IsBlack | data=%v | type=%v | err=%v", data, Stype, err)
		return true
	}
	logger.Debug("BlackListService_CheckNormal | data=%v | type=%v", data, Stype)
	return false
}

func (bs *BlackListService) CheckIsInBlack(appId, phone, bankCard, vpa, email string) bool {
	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("BlackListService_Init_Err | err=%v", err)
		return false
	}

	idx := strings.Index(phone, "+")
	if idx != -1 { // 带有+91，+xx前缀
		phone = phone[idx+3:]
	}

	phone = strings.TrimSpace(phone)
	if len(phone) < 3 {
		logger.Error("BlackListService phone[%v] is error", phone)
		return true
	}

	session := engine.Where("data=?", phone)

	if bankCard != "" {
		bankCard = strings.TrimSpace(bankCard)
		if bankCard != "" {
			session.Or("data=?", strings.ToLower(bankCard))
		}
	}

	if vpa != "" {
		values := strings.Split(vpa, "@")
		vpa = strings.TrimSpace(values[0])
		if vpa != "" {
			session.Or("data=?", strings.ToLower(vpa))
		}
	}

	if email != "" {
		values := strings.Split(email, "@")
		email = strings.TrimSpace(values[0])
		if email != "" {
			session.Or("data=?", strings.ToLower(email))
		}
	}

	exist, err := session.Get(&model.BlackList{})
	if exist || err != nil {
		//m := fmt.Sprintf("%v-%v-%v-%v", phone, bankCard, vpa, email)
		//msg := fmt.Sprintf("BlackUser from [%v]--> %v", appId, m)
		//common.PushAlarmEvent(common.Warning, m, "CheckIsInBlack", m,  msg)
		logger.Error("BlackListService Trigger [%v-%v-%v-%v-%v] | err=%v", appId, phone, email, vpa, bankCard, err)
		return true
	}

	return false
}

func (bs *BlackListService) CheckBlackInDB(data string, Stype BlackType) bool {
	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("BlackListService_Init_Err | err=%v", err)
		return false
	}
	session := engine.Where("data=?", data)
	if Stype != "" {
		session.Where("type=?", Stype)
	}
	exist, err := session.Get(&model.BlackList{})
	if exist || err != nil {
		logger.Error("BlackListService_IsBlack | data=%v | type=%v | err=%v", data, Stype, err)
		return true
	}

	return false
}

func (bs *BlackListService) CheckBlackInCache(data string, Stype BlackType) bool {
	// TODO 使用布隆过滤器
	return false
}

// 刷新mysql数据到redis
func (bs *BlackListService) refreshCache() error {
	// TODO
	engine, err := dao.GetMysql()
	if err != nil {
		return err
	}

	list := make([]model.BlackList, 0, 128)
	err = engine.Desc("id").Limit(512).Find(&list)
	return nil
}

//func (bs *BlackListService) AddBlack(data string, Stype string) bool {
//	engine, err := dao.GetMysql()
//	if err != nil {
//		logger.Error("BlackListService_Init_Err | err=%v", err)
//		return false
//	}
//	blackList := new(model.BlackList)
//	blackList.Data = data
//	blackList.Type = Stype
//	_, err = engine.Insert(blackList)
//	if err != nil {
//		return false
//	}
//	return true
//}
