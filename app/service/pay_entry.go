package service

import (
	"encoding/json"
	"errors"
	"fmt"
	"funzone_pay/app/validator"
	"funzone_pay/centerlog"
	"funzone_pay/channel"
	"funzone_pay/channel/cashfree"
	"funzone_pay/channel/fmpay"
	"funzone_pay/channel/loogpay"
	"funzone_pay/channel/payu"
	"funzone_pay/channel/pdkpay"
	"funzone_pay/channel/uppay"
	"funzone_pay/channel/xpay"
	"funzone_pay/channel/zwpay"
	"funzone_pay/common"
	"funzone_pay/dao"
	"funzone_pay/errdef"
	"funzone_pay/model"
	"funzone_pay/utils"
	"github.com/wonderivan/logger"
	"net/http"
	"net/url"
	"strconv"
	"time"
)

const RUMMY_JOSH_PAY_ACCOUNTID = "cashfree2"

var payEntryService *PayEntryService

type PayEntryService struct {
}

func GetPayEntryService() *PayEntryService {
	if payEntryService == nil {
		payEntryService = new(PayEntryService)
	}
	return payEntryService
}

/*
*
入金
*/
func (ps *PayEntryService) CreateInOrder(payEntryArg validator.PayEntryValidator) (*model.PayEntry, string) {
	//根据支付渠道创建付款对象
	payInstance := channel.GetPayInstance(payEntryArg.PayChannel)
	if payInstance == nil {
		return nil, "CHANNEL_ERROR"
	}
	pEntry := new(model.PayEntry)
	pEntry, err := payInstance.CreateOrder(payEntryArg)
	if pEntry == nil || err != nil {
		return nil, "INTERNAL_ERROR"
	}
	return pEntry, ""
}

/*
*
入金 - 内部应用调用 - 可根据渠道取切换
*/
func (ps *PayEntryService) NewCreateInOrder(payEntryArg validator.PayEntryValidator) (*model.PayEntry, string) {
	//根据支付渠道创建付款对象 - 可根据来源做切换
	var (
		payInstance channel.PaymentContract
		err         error
	)
	if payEntryArg.AppId == string(model.RUMMY) {
		payEntryArg.Uid = 78
		payEntryArg.AppId = "rummybank123"
		payInstance, _, err = PayAccountConfServe.GetPayInstance(78, model.PAY_IN, payEntryArg.Amount, payEntryArg)
	} else {
		payChannel, payAccountId := PayAccountConfServe.GetPayChannelAccountId(payEntryArg.AppId, payEntryArg.PayChannel, "order")
		payInstance, err = PayAccountConfServe.GetSpecificPayInstance(payChannel, payAccountId)
	}
	if err != nil {
		return nil, err.Error()
	}
	if payInstance == nil {
		return nil, "CHANNEL_ERROR"
	}
	pEntry := new(model.PayEntry)
	pEntry, err = payInstance.CreateOrder(payEntryArg)
	if pEntry == nil || err != nil {
		return nil, "INTERNAL_ERROR"
	}
	return pEntry, ""
}

/*
*
入金 - 平台用户
*/
func (ps *PayEntryService) CreateCollectOrder(uid int64, payEntryArg validator.PayEntryValidator) (*model.PayEntry, int, string) {
	//app_order_id幂等校验
	//根据支付渠道创建付款对象
	payInstance, code, err := PayAccountConfServe.GetPayInstance(uid, model.PAY_IN, payEntryArg.Amount, payEntryArg)
	if err != nil {
		errMsg := err.Error()
		if code == errdef.Code400 {
			logger.Error("CreateCollectOrder ins-1 error | uid=%+v | err=%v", uid, errMsg)
			return nil, errdef.Code400, errMsg
		}

		logger.Error("CreateCollectOrder ins-2 error | uid=%+v | err=%v", uid, errMsg)
		return nil, errdef.Code500, "INTERNAL_ERROR_1"
	}
	if payInstance == nil {
		logger.Error("CreateCollectOrder channel error | %v | uid=%+v", code, uid)
		return nil, errdef.Code500, "CHANNEL_ERROR_2"
	}
	//engine, err := dao.GetMysql()
	//if err != nil {
	//	logger.Error("CreateCollectOrder GetMysql | err=%v | paymentId=%+v", err, uid)
	//	return nil, errdef.Code500, "INTERNAL_ERROR_3"
	//}
	//timeStr := time.Now().Format("2006-01-02")
	//t, _ := time.Parse("2006-01-02", timeStr)
	//start := t.Unix()
	//total, err := engine.Where("uid=? AND created>? AND status=1", uid, start).Sum(&model.PayEntry{}, "amount")
	//if err != nil {
	//	logger.Error("CreateCollectOrder | err=%v | paymentId=%+v", err, uid)
	//	return nil, errdef.Code500, "INTERNAL_ERROR_4"
	//}
	//if total > 3000000 {
	//	//报警
	//	SmsWarning("FunPayChargeWarn")
	//}
	pEntry := new(model.PayEntry)
	pEntry, err = payInstance.CreateOrder(payEntryArg)
	if pEntry == nil || err != nil {
		logger.Error("CreateCollectOrder_CreateOrder | err=%v | paymentId=%+v", err, uid)
		code, ok := errdef.GetErrorCode(err)
		if ok {
			return nil, code, err.Error()
		}

		return nil, errdef.Code500, "INTERNAL_ERROR_5"
	}
	//centerlog.Info(centerlog.MsgPayInOrder, map[string]interface{}{
	//	centerlog.FieldAppId:          pEntry.AppId,
	//	centerlog.FieldAccountId:      pEntry.PayAccountId,
	//	centerlog.FieldOrderId:        pEntry.OrderId,
	//	centerlog.FieldAppOrderId:     pEntry.AppOrderId,
	//	centerlog.FieldPaymentOrderID: pEntry.PaymentOrderId,
	//	centerlog.FieldAmount:         pEntry.Amount,
	//	centerlog.FieldThirdDesc:      pEntry.ThirdDesc,
	//})
	return pEntry, errdef.CodeOK, ""
}

/**
 * 获得付款订单状态
 * param: string           orderId
 * param: model.PayChannel payChannel
 * return: interface{}
 * return: string
 */
func (ps *PayEntryService) GetPayOrderStatus(paymentId string, orderId string) (status model.PayStatus, resKey string) {
	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("PayEntryService_GetPayOrderStatus_DBError | err=%v | paymentId=%+v", err, paymentId)
		return model.PAYING, "DB_ERROR"
	}
	payEntry := new(model.PayEntry)
	exist, err := engine.Where("payment_order_id=?", orderId).Get(payEntry)
	if err != nil {
		logger.Error("PayEntryService_GetPayOrderStatus_QueryError | err=%v | orderId=%+v", err, paymentId)
		return model.PAYING, "DB_ERROR"
	}
	if !exist {
		return model.PAYFAILD, "NOT_EXIST"
	}
	//根据支付渠道创建付款对象
	payInstance := channel.GetPayInstance(payEntry.PayChannel)
	if payInstance == nil {
		return model.PAYING, "CHANNEL_ERROR"
	}
	//status, err = payInstance.Inquiry(paymentId, orderId)
	//if err != nil {
	//	logger.Error("PayEntryService_GetPayOrderStatus_Error | err=%v | orderId=%+v", err, paymentId)
	//	return model.PAYING, "REQUEST_ERROR"
	//}
	return status, ""
}

// 用于临时查看订单状态，phonepay用

func (ps *PayEntryService) GetPayOrderStatus2(orderId string) (status model.PayStatus, resKey string) {
	key := fmt.Sprintf("orderstatus:%v", orderId)
	statusStr := dao.RedisGet(key)
	if statusStr != "" {
		statusInt, _ := strconv.ParseInt(statusStr, 10, 32)
		return model.PayStatus(statusInt), ""
	}

	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("PayEntryService_GetPayOrderStatus2_DBError | err=%v | orderId=%+v", err, orderId)
		return model.PAYING, "DB_ERROR"
	}
	payEntry := new(model.PayEntry)
	exist, err := engine.Where("order_id=?", orderId).Get(payEntry)
	if err != nil {
		logger.Error("PayEntryService_GetPayOrderStatus2_QueryError | err=%v | orderId=%+v", err, orderId)
		return model.PAYING, "DB_ERROR"
	}
	if !exist {
		return model.PAYFAILD, "NOT_EXIST"
	}

	dao.RedisSet(key, fmt.Sprintf("%v", payEntry.Status), 30)
	return payEntry.Status, ""
}

func (ps *PayEntryService) ManualCheckCallback(orderId string, isSettingSuccess bool) string {
	if orderId == "" {
		return ""
	}

	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("PayEntryService_ManualCallback_DBError | err=%v | cb =%v", err, orderId)
		return "DB_ERROR"
	}

	payEntry := new(model.PayEntry)
	exist, err := engine.Where("order_id=?", orderId).Get(payEntry)
	if err != nil {
		logger.Error("PayEntryService_ManualCallback_Query_DBError | err=%v | cb=%v", err, orderId)
		return "DB_ERROR"
	}
	if !exist {
		logger.Error("PayEntryService_ManualCallback_NotExist | err=%v | cb=%v", err, orderId)
		return "DB_ERROR"
	}

	logger.Info("PayEntryServiceManualCallback | data=%v | cb=%v", payEntry, orderId)

	var data *model.PayEntry
	if !isSettingSuccess {
		payInstance, _ := PayAccountConfServe.GetSpecificPayInstance(payEntry.PayChannel, payEntry.PayAccountId)
		data, err = payInstance.Inquiry(payEntry.OrderId, payEntry.PaymentOrderId)
		if err != nil {
			logger.Error("ManualInquiry_Inquiry_Error | err=%v | data=%%v", err, payEntry.OrderId)
			return err.Error()
		}
	} else {
		if payEntry.Status != model.PAYING && payEntry.Status != model.PAYFAILD {
			return fmt.Sprintf("PayEntryServiceManualCallback invalid status | order=%v | status=%v", orderId, payEntry.Status)
		}

		// 不用查询手动设置成功
		tmp := *payEntry
		tmp.Status = model.PAYSUCCESS
		data = &tmp

		logger.Info("PayEntryServiceManualCallback, manual set[order:%v] status success", orderId)
	}

	oldStatus := payEntry.Status
	if data.Status == oldStatus || oldStatus == model.PAYROLLBACK || data.Status == model.PAYING { // 没状态变化,不做处理
		return fmt.Sprintf("status is %v, no update", oldStatus)
	}

	// 支付先成功后失败，需要退款
	if oldStatus == model.PAYSUCCESS && data.Status == model.PAYFAILD {
		err = AccountServ.ReversedPayEntry(data, true)
		if err != nil {
			msg := fmt.Sprintf("rollback order[%v] reverse [%v]", data.OrderId, err.Error())
			common.PushAlarmEvent(common.Warning, data.AppId, "manual", "", msg)
			return fmt.Sprintf("Reverse[%v] Fail", data.OrderId)
		}

		logger.Debug("PayEntryServiceManualCheckOrder Reverse Order[%v]", orderId)
		data.Status = model.PAYROLLBACK
	}

	// 下面四种情况，直接更新订单状态
	// pending--->success
	// pending--->fail
	// fail------>sucdess
	// fail------>pending

	rowNum, err := engine.Where("id=?", data.Id).Cols("status,payment_id,third_code,third_desc,finish_time").Update(&model.PayEntry{
		Status:     data.Status,
		PaymentId:  data.PaymentId,
		ThirdCode:  data.ThirdCode,
		ThirdDesc:  data.ThirdDesc,
		FinishTime: time.Now().Unix(),
	})
	if err != nil {
		logger.Error("ManualPayOrderInquiry_Update_Error | order=%v | rowNum=%v | err=%v", data.OrderId, rowNum, err)
		return fmt.Sprintf("")
	}
	go ps.SendEntryCallback(data)

	return ""
}

/**
 * serpay充值回调
 * param: *model.CFOrderCallBack cb
 * return: string
 */
func (ps *PayEntryService) SerPayInCallback(cb *model.SerInCallback) (resKey string) {
	//更新状态
	payEntry := new(model.PayEntry)
	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("PayEntryService_SerPayInCallback_DBError | err=%v | cb=%+v", err, cb)
		return "DB_ERROR"
	}

	// 单个订单，多次支付
	// 比如发起一个100的订单，然后用户先支付40，再支付60，这种情况，服务方会返回三次
	//第一次：原始100的订单，支付失败，
	//第二次: 支付成功40的订单，但是orderid不再是原始100的订单的id，是服务方自己生成的，但是payment_order_id是一样的,每次调用发起支付订单，生成的payment_order_id（callback的clabe字段）是唯一的，如果不发起生成支付订单，可以一直使用老的id支付，服务方会一直给回调
	//第三次：支付成功60的订单
	if cb.IsMXMultiPay() {
		exist, err := engine.Where("payment_order_id=? and status <> ?", cb.Clabe, model.PAYSUCCESS).Get(payEntry)
		if err != nil || !exist {
			logger.Error("PayEntryService_SerPayInCallback_NotExist_MX | err=%v | cb=%v", err, cb)
			return "DB_ERROR"
		}

		//oldOrderId := payEntry.OrderId
		amt, _ := strconv.ParseFloat(cb.OrdAmt, 64)
		payEntry.Id = 0
		payEntry.OrderId = cb.CustOrderNo // 平台给的新的orderid
		payEntry.PaymentOrderId = cb.Clabe
		payEntry.Status = model.PAYSUCCESS
		payEntry.Amount = amt
		payEntry.Created = time.Now().Unix()
		payEntry.FinishTime = payEntry.Created
		payEntry.ThirdDesc = cb.FunZoneThirdDesc()

		id, err := engine.Insert(payEntry)
		if err != nil {
			logger.Error("PayEntryService_SerPayInCallback_MX_Insert | err=%v | cb=%v", err, cb)
			return "DB_ERROR"
		}

		payEntry.Id = id
		payEntry.MultiPay = true
	} else {
		exist, err := engine.Where("order_id=?", cb.CustOrderNo).Get(payEntry)
		if err != nil {
			logger.Error("PayEntryService_SerPayInCallback_Query_DBError | err=%v | cb=%v", err, cb)
			return "DB_ERROR"
		}
		if !exist {
			logger.Error("PayEntryService_SerPayInCallback_NotExist | err=%v | cb=%v", err, cb)
			return "DB_ERROR"
		}
	}
	//签名校验
	if payEntry.Uid != 0 {
		payIns, err := PayAccountConfServe.GetSpecificPayInstance(model.SERPAY, payEntry.PayAccountId)
		if err != nil {
			logger.Error("PayEntryService_SerPayInCallback_PlatformInstance | err=%v | cb=%v", err, cb)
			return "DB_ERROR"
		}
		if !payIns.InSignature(cb) {
			logger.Error("PayEntryService_SerPayInCallback_PlatformInSignature_Error | cb=%+v", cb)
			return "SIGN_ERROR"
		}
	} else {
		//先不做操作
	}

	var rowCount int64 = 0
	if !cb.IsMXMultiPay() {
		//已经修改不再变化
		if payEntry.Status == model.PAYSUCCESS ||
			payEntry.Status == model.PAYFAILD && cb.OrdStatus != "01" {
			logger.Error("PayEntryService_SerPayInCallback_Repeat | err=%v | cb=%v", err, cb)
			centerlog.Info(centerlog.MsgRepeatCallBack, map[string]interface{}{
				centerlog.FieldUid:         payEntry.Uid,
				centerlog.FieldAppId:       payEntry.AppId,
				centerlog.FieldAppOrderId:  payEntry.AppOrderId,
				centerlog.FieldOrderId:     payEntry.OrderId,
				centerlog.FieldOrderStatus: payEntry.Status,
				centerlog.FieldThirdStatus: cb.OrdStatus,
				centerlog.FieldThirdDesc:   cb.OrderDesc,
			})
			return "REPEAT_CALLBACK"
		}
		if cb.OrdStatus == "01" {
			payEntry.Status = model.PAYSUCCESS
			payEntry.FinishTime = time.Now().Unix()
		} else if cb.OrdStatus == "02" {
			payEntry.Status = model.PAYFAILD
			payEntry.ThirdCode = cb.OrdStatus
			payEntry.ThirdDesc = cb.OrderDesc
			payEntry.FinishTime = time.Now().Unix()
		} else if cb.OrdStatus == "05" {
			payEntry.Status = model.PAYCANCELLED
			payEntry.FinishTime = time.Now().Unix()
		} else {
			//暂不处理
			return ""
		}
		rowCount, err = engine.Where("id=?", payEntry.Id).Update(payEntry)
		if err != nil {
			logger.Error("PayEntryService_SerPayInCallback_Query_DBError | err=%v | cb=%v", err, cb)
			return "DB_ERROR"
		}
	} else {
		rowCount = 1
	}
	logger.Debug("PayEntryService_SerPayInCallback_Info | payEntry=%+v | rowCount=%v | cb=%v", payEntry, rowCount, cb)
	if rowCount > 0 {
		//发起回调
		go ps.SendEntryCallback(payEntry)
	}
	return ""
}

func (ps *PayEntryService) OnionPayInCallback(cb *model.OnionInCallback) (resKey string) {
	//更新状态
	payEntry := new(model.PayEntry)
	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("PayEntryService_OnionPayInCallback_DBError | err=%v | cb=%+v", err, cb)
		return "DB_ERROR"
	}
	exist, err := engine.Where("order_id=?", cb.MerTransNo).Get(payEntry)
	if err != nil {
		logger.Error("PayEntryService_OnionPayInCallback_Query_DBError | err=%v | cb=%v", err, cb)
		return "DB_ERROR"
	}
	if !exist {
		logger.Error("PayEntryService_OnionPayInCallback_NotExist | err=%v | cb=%v", err, cb)
		return "DB_ERROR"
	}
	//签名校验
	if payEntry.Uid != 0 {
		payIns, err := PayAccountConfServe.GetSpecificPayInstance(model.ONIONPAY, payEntry.PayAccountId)
		if err != nil {
			logger.Error("PayEntryService_OnionPayInCallback_PlatformInstance | err=%v | cb=%v", err, cb)
			return "DB_ERROR"
		}
		if !payIns.InSignature(cb) {
			logger.Error("PayEntryService_OnionPayInCallback_PlatformInSignature_Error | cb=%+v", cb)
			return "SIGN_ERROR"
		}
	} else {
		//先不做操作
		logger.Error("PayEntryService_OnionPayInCallback_Uid = 0 | cb=%+v", cb)
	}
	//已经修改不再变化
	if payEntry.Status != model.PAYING {
		logger.Error("PayEntryService_OnionPayInCallback_Repeat | err=%v | cb=%v", err, cb)
		return "REPEAT_CALLBACK"
	}
	if cb.TransStatus == "success" {
		payEntry.Status = model.PAYSUCCESS
		payEntry.FinishTime = time.Now().Unix()
	} else if cb.TransStatus == "failure" {
		payEntry.Status = model.PAYFAILD
		payEntry.FinishTime = time.Now().Unix()
	} else {
		//暂不处理
		return ""
	}
	rowCount, err := engine.Where("id=?", payEntry.Id).Update(payEntry)
	if err != nil {
		logger.Error("PayEntryService_OnionPayInCallback_Query_DBError | err=%v | cb=%v", err, cb)
		return "DB_ERROR"
	}
	logger.Debug("PayEntryService_OnionPayInCallback_Info | payEntry=%+v | rowCount=%v | cb=%v", payEntry, rowCount, cb)
	if rowCount > 0 {
		//发起回调
		go ps.SendEntryCallback(payEntry)
	}
	return ""
}

func (ps *PayEntryService) FidyPayInCallback(cb *model.FidyPayCollectCallback) (resKey string) {
	//更新状态
	payEntry := new(model.PayEntry)
	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("PayEntryService_FidyPayInCallback_DBError | err=%v | cb=%+v", err, cb)
		return "DB_ERROR"
	}
	exist, err := engine.Where("order_id=?", cb.MerchantTrxnRefId).Get(payEntry)
	if err != nil {
		logger.Error("PayEntryService_FidyPayInCallback_Query_DBError | err=%v | cb=%v", err, cb)
		return "DB_ERROR"
	}
	if !exist {
		logger.Error("PayEntryService_FidyPayInCallback_NotExist | err=%v | cb=%v", err, cb)
		return "DB_ERROR"
	}
	//签名校验
	//if payEntry.Uid != 0 {
	//	payIns, err := PayAccountConfServe.GetSpecificPayInstance(model.FIDYPAY, payEntry.PayAccountId)
	//	if err != nil {
	//		logger.Error("PayEntryService_FidyPayInCallback_PlatformInstance | err=%v | cb=%v", err, cb)
	//		return "DB_ERROR"
	//	}
	//	if !payIns.InSignature(cb) {
	//		logger.Error("PayEntryService_FidyPayInCallback_PlatformInSignature_Error | cb=%+v", cb)
	//		return "SIGN_ERROR"
	//	}
	//} else {
	//	//先不做操作
	//}
	//已经修改不再变化
	if payEntry.Status != model.PAYING {
		logger.Error("PayEntryService_FidyPayInCallback_Repeat | err=%v | cb=%v", err, cb)
		return "REPEAT_CALLBACK"
	}
	if cb.Code == "0x0200" && cb.Description == "Transaction success" {
		payEntry.Status = model.PAYSUCCESS
		payEntry.FinishTime = time.Now().Unix()
	} else if cb.Code == "0x0202" {
		payEntry.Status = model.PAYFAILD
		payEntry.FinishTime = time.Now().Unix()
	} else {
		//暂不处理
		return ""
	}
	rowCount, err := engine.Where("id=?", payEntry.Id).Update(payEntry)
	if err != nil {
		logger.Error("PayEntryService_FidyPayInCallback_Query_DBError | err=%v | cb=%v", err, cb)
		return "DB_ERROR"
	}
	logger.Debug("PayEntryService_FidyPayInCallback_Info | payEntry=%v | rowCount=%v | cb=%v", payEntry, rowCount, cb)
	if rowCount > 0 {
		//发起回调
		go ps.SendEntryCallback(payEntry)
	}
	return ""
}

/**
 * razorpay充值回调
 * param: *model.CallBackWebHook hook
 * return: string
 */
func (ps *PayEntryService) WebHookCallback(hook *model.CallBackWebHook) (resKey string) {
	if hook.Event != model.RefundsProcessed && hook.Event != model.OrderPaid {
		logger.Warn("PayEntryService_WebHookCallback_Not_Support_Event[%v] | cb=%v", hook.Event, hook)
		return fmt.Sprintf("no support event[%v]", hook.Event)
	}

	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("PayEntryService_WebHookCallback_DBError | err=%v | cb =%v", err, hook)
		return "DB_ERROR"
	}
	payEntry := new(model.PayEntry)
	exist, err := engine.Where("payment_order_id=?", hook.PayLoad.Payment.Entity.OrderId).Get(payEntry)
	if err != nil {
		logger.Error("PayEntryService_WebHookCallback_Query_DBError | err=%v | cb=%v", err, hook)
		return "DB_ERROR"
	}
	if !exist {
		logger.Error("PayEntryService_WebHookCallback_NotExist | err=%v | cb=%v", err, hook)
		return "DB_ERROR"
	}

	sourceStatus := payEntry.Status
	logger.Info("PayEntryService_WebHookCallback_Event[%v] | id=%v | uid=%v | source=%v | cb=%v", hook.Event, payEntry.Id, payEntry.Uid, payEntry.Status, hook)
	// 退款
	if hook.Event == model.RefundsProcessed {
		if payEntry.Status == model.PAYSUCCESS {
			err = AccountServ.ReversedPayEntry(payEntry, true) //要回退金额
			if err != nil {
				logger.Error("PayEntryService_WebHookCallback_Refund | id=%v | err=%v | cb=%v", payEntry.Id, err, hook)
				return "Reverse Failure"
			}
		}
		payEntry.Status = model.PAYROLLBACK
	} else { // 充值
		//已经修改不再变化
		if payEntry.Status != model.PAYING {
			logger.Error("PayEntryService_WebHookCallback_Repeat | id=%v | err=%v | cb=%v", payEntry.Id, err, hook)
			return "REPEAT_CALLBACK"
		}
		switch hook.Event {
		//订单付款成功
		case model.OrderPaid:
			if hook.PayLoad.Payment.Entity.Status == "captured" {
				payEntry.PaymentId = hook.PayLoad.Payment.Entity.Id
				payEntry.Status = model.PAYSUCCESS
				payEntry.FinishTime = time.Now().Unix()
			} else {
				return ""
			}
		//订单付款失败
		//`
		//	payEntry.Status = model.PAYFAILD
		//	payEntry.ThirdCode = hook.PayLoad.Payment.Entity.ErrorCode
		//	payEntry.ThirdDesc = hook.PayLoad.Payment.Entity.ErrorDescription
		//	_, err := engine.Where("payment_order_id=?", hook.PayLoad.Payment.Entity.OrderId).Update(payEntry)
		//	if err != nil {
		//		logger.Error("PayEntryService_WebHookCallback_Err | err=%v", err)
		//		return "DB_ERROR"
		//	}
		default:
			return ""
		}
	}

	if sourceStatus == payEntry.Status {
		return ""
	}

	//发起回调
	if payEntry.Status != model.PAYROLLBACK {
		_, err = engine.Where("id=?", payEntry.Id).Update(payEntry)
		if err != nil {
			logger.Error("PayEntryService_WebHookCallback_Err | id = %v | err=%v", payEntry.Id, err)
			return "DB_ERROR"
		}
	}

	go ps.SendEntryCallback(payEntry)
	return ""
}

/**
 * paytm支付充值回调
 * param: string dataString
 * return: string
 */
func (ps *PayEntryService) PaytmOrderCallback(dataString string) (resKey string, host string) {
	//签名校验
	//data := paytm.GetPayTm().CheckSign(dataString)
	values, _ := url.ParseQuery(dataString)
	var cbData = make(map[string]string)
	for k, v := range values {
		cbData[k] = v[0]
	}
	orderId := cbData["ORDERID"]
	payEntry := new(model.PayEntry)
	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("PayEntryService_PaytmOrderCallback_DBError | err=%v | cb=%+v", err, cbData)
		return "DB_ERROR", ""
	}
	exist, err := engine.Where("order_id=?", orderId).Get(payEntry)
	if err != nil {
		logger.Error("PayEntryService_PaytmOrderCallback_Query_DBError | err=%v | cb=%v", err, cbData)
		return "DB_ERROR", ""
	}
	if !exist {
		logger.Error("PayEntryService_PaytmOrderCallback_NotExist | err=%v | cb=%v", err, cbData)
		return "DB_ERROR", ""
	}
	//签名校验 - 先去掉
	/*if payEntry.Uid != 0 {
		payIns, err := PayAccountConfServe.GetSpecificPayInstance(model.PAYTM, payEntry.PayAccountId)
		if err != nil {
			logger.Error("PayEntryService_PaytmOrderCallback_PlatformInstance | err=%v | cb=%v", err, cbData)
			return "DB_ERROR"
		}
		if !payIns.InSignature(data) {
			logger.Error("PayEntryService_PaytmOrderCallback_PlatformInSignature_Error | cb=%+v", data)
			return "SIGN_ERROR"
		}
	} else {
		if !paytm.GetPayTm().InSignature(data) {
			logger.Error("PayEntryService_InSignature_Error | cb=%+v", cbData)
			return "SIGN_ERROR"
		}
	}*/
	logger.Debug("PayEntryService_PaytmOrderCallback_Info | cb=%v", cbData)
	//已经修改不再变化
	if payEntry.Status != model.PAYING {
		logger.Error("PayEntryService_PaytmOrderCallback_Repeat | err=%v | data=%v", err, cbData)
		return "REPEAT_CALLBACK", ""
	}
	switch cbData["STATUS"] {
	//订单付款成功
	case "TXN_SUCCESS":
		payEntry.PaymentId = cbData["TXNID"]
		payEntry.Status = model.PAYSUCCESS
		payEntry.FinishTime = time.Now().Unix()
		_, err := engine.Where("order_id=?", cbData["ORDERID"]).Update(payEntry)
		if err != nil {
			logger.Error("PayEntryService_PaytmOrderCallback_Err | err=%v", err)
			return "DB_ERROR", ""
		}
	case "TXN_FAILURE":
		return "SUCCESS", ""
		//先不处理失败回调
		payEntry.PaymentId = cbData["TXNID"]
		//RESPMSG
		payEntry.Status = model.PAYFAILD
		payEntry.ThirdDesc = cbData["RESPMSG"]
		payEntry.FinishTime = time.Now().Unix()
		_, err := engine.Where("order_id=?", cbData["ORDERID"]).Update(payEntry)
		if err != nil {
			logger.Error("PayEntryService_PaytmOrderCallback_Err | err=%v", err)
			return "DB_ERROR", ""
		}
	default:
		return "SUCCESS", ""
	}
	//发起回调
	go ps.SendEntryCallback(payEntry)

	conf, _ := PaymentConfig.Load(payEntry.PayAccountId)
	u, _ := url.Parse(conf.(map[string]interface{})["COLLECT_HOST"].(string))
	if payEntry.Status == model.PAYFAILD {
		return "FAILED", u.Host
	} else {
		return "SUCCESS", u.Host
	}
}

/**
 * payu充值回调
 * param: string dataString
 * return: string
 */
func (ps *PayEntryService) PayUOrderCallback(data *model.PayUOrderData, header http.Header) (resKey string) {
	//token校验
	checkRes := payu.GetPayU().CheckSign(header.Get("Token"))
	if !checkRes {
		logger.Error("PayEntryService_PayUOrderCallback_Sign_Err | data=%v | header=%v", data, header)
		//return "DATA_ERROR"
	}
	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("PayEntryService_PayUOrderCallback_DBError | err=%v", err)
		return "DB_ERROR"
	}
	payEntry := new(model.PayEntry)
	exist, err := engine.Where("order_id=?", data.MerchantTransactionId).Get(payEntry)
	if err != nil {
		logger.Error("PayEntryService_PayUOrderCallback_Query_DBError | err=%v | data=%v", err, data)
		return "DB_ERROR"
	}
	if !exist {
		logger.Error("PayEntryService_PayUOrderCallback_NotExist | err=%v | data=%v", err, data)
		return "DB_ERROR"
	}
	//已经修改不再变化
	if payEntry.Status != model.PAYING {
		logger.Error("PayEntryService_PayUOrderCallback_Repeat | err=%v | data=%v", err, data)
		return "REPEAT_CALLBACK"
	}
	switch data.Status {
	//订单付款成功
	case "Success":
		payEntry.PaymentId = data.PaymentId
		payEntry.Status = model.PAYSUCCESS
		payEntry.FinishTime = time.Now().Unix()
		_, err := engine.Where("order_id=?", data.MerchantTransactionId).Update(payEntry)
		if err != nil {
			logger.Error("PayEntryService_PayUOrderCallback_Err | err=%v", err)
			return "DB_ERROR"
		}
	default:
		return ""
	}
	//发起回调
	go ps.SendEntryCallback(payEntry)
	return ""
}

/**
 * cashfree充值回调
 * param: *model.CFOrderCallBack cb
 * return: string
 */
func (ps *PayEntryService) CashFreePayInCallback(cb *model.CFOrderCallBack) (resKey string) {
	//更新状态
	payEntry := new(model.PayEntry)
	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("PayEntryService_CashFreePayInCallback_DBError | err=%v | cb=%+v", err, cb)
		return "DB_ERROR"
	}
	exist, err := engine.Where("order_id=?", cb.OrderId).Get(payEntry)
	if err != nil {
		logger.Error("PayEntryService_CashFreePayInCallback_Query_DBError | err=%v | cb=%v", err, cb)
		return "DB_ERROR"
	}
	if !exist {
		logger.Error("PayEntryService_CashFreePayInCallback_NotExist | err=%v | cb=%v", err, cb)
		return "DB_ERROR"
	}
	//签名校验
	if payEntry.Uid != 0 {
		payIns, err := PayAccountConfServe.GetSpecificPayInstance(model.CASHFREE, payEntry.PayAccountId)
		if err != nil {
			logger.Error("PayEntryService_CashFreePayInCallback_PlatformInstance | err=%v | cb=%v", err, cb)
			return "DB_ERROR"
		}
		if !payIns.InSignature(cb) {
			logger.Error("CashFreePayInCallback_PlatformInSignature_Error | cb=%+v", cb)
			return "SIGN_ERROR"
		}
	} else {
		if !cashfree.GetCashFree().InSignature(cb) {
			logger.Error("CashFreePayInCallback_InSignature_Error | cb=%+v", cb)
			return "SIGN_ERROR"
		}
	}
	//已经修改不再变化
	if payEntry.Status != model.PAYING {
		logger.Error("PayEntryService_CashFreePayInCallback_Repeat | err=%v | cb=%v", err, cb)
		return "REPEAT_CALLBACK"
	}
	if cb.TxStatus == "SUCCESS" {
		payEntry.Status = model.PAYSUCCESS
		payEntry.FinishTime = time.Now().Unix()
	} else if cb.TxStatus == "CANCELLED" {
		/*else if cb.TxStatus == "FAILED" {
			payEntry.Status = model.PAYFAILD
			payEntry.FinishTime = time.Now().Unix()
		} */
		payEntry.Status = model.PAYCANCELLED
		payEntry.FinishTime = time.Now().Unix()
	} else {
		//暂不处理
		return ""
	}
	rowCount, err := engine.Where("id=?", payEntry.Id).Update(payEntry)
	if err != nil {
		logger.Error("PayEntryService_CashFreePayInCallback_Query_DBError | err=%v | cb=%v", err, cb)
		return "DB_ERROR"
	}
	logger.Debug("CashFreePayInCallback_Info | payEntry=%+v | rowCount=%v | cb=%v", payEntry, rowCount, cb)
	if rowCount > 0 {
		//发起回调
		go ps.SendEntryCallback(payEntry)
	}
	return ""
}

/**
 * PagSmilePay充值回调
 * param: *model.PagSmilePayNotify cb
 * return: string
 */
func (ps *PayEntryService) PagSmilePayInCallback(cb model.PagSmilePayNotify) (resKey string) {
	//更新状态
	payEntry := new(model.PayEntry)
	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("PayEntryService_PagSmilePayInCallback_DBError | err=%v | cb=%+v", err, cb)
		return "DB_ERROR"
	}
	exist, err := engine.Where("order_id=?", cb.OutTradeNO).Get(payEntry)
	if err != nil {
		logger.Error("PayEntryService_PagSmilePayInCallback_Query_DBError | err=%v | cb=%v", err, cb)
		return "DB_ERROR"
	}
	if !exist {
		logger.Error("PayEntryService_PagSmilePayCallback_NotExist | err=%v | cb=%v", err, cb)
		return "DB_ERROR"
	}

	//已经修改不再变化
	if payEntry.Status != model.PAYING {
		logger.Error("PayEntryService_PagSmilePayInCallback_Repeat | err=%v | cb=%v", err, cb)
		return "REPEAT_CALLBACK"
	}
	if cb.TradeStatus == "SUCCESS" {
		payEntry.Status = model.PAYSUCCESS
		payEntry.FinishTime = time.Now().Unix()
	} else if cb.TradeStatus == "REFUSED" || cb.TradeStatus == "CHARGEBACK" || cb.TradeStatus == "REFUNDED" {
		payEntry.Status = model.PAYFAILD
		payEntry.ThirdDesc = payEntry.ThirdDesc + "#notify->" + cb.TradeStatus
		payEntry.FinishTime = time.Now().Unix()
	} else if cb.TradeStatus == "CANCEL" {
		payEntry.Status = model.PAYCANCELLED
		payEntry.FinishTime = time.Now().Unix()
	} else {
		//暂不处理
		return ""
	}
	rowCount, err := engine.Where("id=?", payEntry.Id).Update(payEntry)
	if err != nil {
		logger.Error("PayEntryService_PagSmilePayInCallback_Query_DBError | err=%v | cb=%v", err, cb)
		return "DB_ERROR"
	}
	logger.Debug("PagSmilePayInCallback_Info | payEntry=%+v | rowCount=%v | cb=%v", payEntry, rowCount, cb)
	if rowCount > 0 {
		//发起回调
		go ps.SendEntryCallback(payEntry)
	}
	return ""
}

/**
 * GlobPay充值回调
 * param: *model.GlobPayNotify cb
 * return: string
 */
func (ps *PayEntryService) GlobPayInCallback(cb model.GlobPayNotify) (resKey string) {
	//更新状态
	payEntry := new(model.PayEntry)
	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("PayEntryService_GlobPayInCallback_DBError | err=%v | cb=%+v", err, cb)
		return "DB_ERROR"
	}
	exist, err := engine.Where("order_id=?", cb.MchOrderNo).Get(payEntry)
	if err != nil {
		logger.Error("PayEntryService_GlobPayInCallback_Query_DBError | err=%v | cb=%v", err, cb)
		return "DB_ERROR"
	}
	if !exist {
		logger.Error("PayEntryService_GlobPayCallback_NotExist | err=%v | cb=%v", err, cb)
		return "DB_ERROR"
	}

	//已经修改不再变化
	if payEntry.Status != model.PAYING {
		logger.Error("PayEntryService_GlobPayInCallback_Repeat | err=%v | cb=%v", err, cb)
		return "REPEAT_CALLBACK"
	}
	if cb.Code == "1" {
		payEntry.Status = model.PAYSUCCESS
		payEntry.FinishTime = time.Now().Unix()
		// in case,when an order is not paid in full,
		// such as u create an order 10000, but user paid only 5000, the notified orderAmount is 5000
		// so should modify the order and record info
		if int64(payEntry.Amount*100) != cb.OrderAmount {
			payEntry.ThirdDesc = fmt.Sprintf("origin:%f,real:%f", payEntry.Amount, float64(cb.OrderAmount)/100)
			payEntry.Amount = float64(cb.OrderAmount) / 100
		}
	} else {
		payEntry.Status = model.PAYFAILD
		payEntry.ThirdDesc = fmt.Sprintf("%v#notify->%v:%v", payEntry.ThirdDesc, cb.Code, cb.Message)
		payEntry.FinishTime = time.Now().Unix()
	}

	rowCount, err := engine.Where("id=?", payEntry.Id).Update(payEntry)
	if err != nil {
		logger.Error("PayEntryService_GlobPayInCallback_Query_DBError | err=%v | cb=%v", err, cb)
		return "DB_ERROR"
	}
	logger.Debug("GlobPayInCallback_Info | payEntry=%+v | rowCount=%v | cb=%v", payEntry, rowCount, cb)
	if rowCount > 0 {
		//发起回调
		go ps.SendEntryCallback(payEntry)
	}
	return ""
}

/**
 * PayFlash充值回调
 * param: *model.PayNotify cb
 * return: string
 */
func (ps *PayEntryService) PayFlashInCallback(cb model.PayFlashNotify) (resKey string) {
	//更新状态
	payEntry := new(model.PayEntry)
	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("PayEntryService_PayFlashInCallback_DBError | err=%v | cb=%+v", err, cb)
		return "DB_ERROR"
	}
	exist, err := engine.Where("order_id=?", cb.OrderId).Get(payEntry)
	if err != nil {
		logger.Error("PayEntryService_PayFlashInCallback_Query_DBError | err=%v | cb=%v", err, cb)
		return "DB_ERROR"
	}
	if !exist {
		logger.Error("PayEntryService_PayFlashCallback_NotExist | err=%v | cb=%v", err, cb)
		return "DB_ERROR"
	}

	//已经修改不再变化
	if payEntry.Status != model.PAYING {
		logger.Error("PayEntryService_PayFlashInCallback_Repeat | err=%v | cb=%v", err, cb)
		return "REPEAT_CALLBACK"
	}

	ret := "SUCCESS"
	if cb.ResponseCode == "0" {
		payEntry.Status = model.PAYSUCCESS
		payEntry.ThirdDesc = cb.ResponseMessage
		payEntry.FinishTime = time.Now().Unix()
	} else if cb.ResponseCode == "1006" || cb.ResponseCode == "1088" || cb.ResponseCode == "1030" {
		//暂不处理
		// processing
		return ""
	} else {
		payEntry.Status = model.PAYFAILD
		payEntry.ThirdCode = cb.ResponseCode
		payEntry.ThirdDesc = cb.ResponseMessage + "#->" + cb.ErrorDesc + "#" + payEntry.ThirdDesc
		payEntry.FinishTime = time.Now().Unix()
		ret = cb.ResponseMessage + ":" + cb.ErrorDesc
	}
	rowCount, err := engine.Where("id=?", payEntry.Id).Update(payEntry)
	if err != nil {
		logger.Error("PayEntryService_PayFlashInCallback_Query_DBError | err=%v | cb=%v", err, cb)
		return "DB_ERROR"
	}
	logger.Debug("PayFlashInCallback_Info | payEntry=%+v | rowCount=%v | cb=%v", payEntry, rowCount, cb)
	if rowCount > 0 {
		//发起回调
		go ps.SendEntryCallback(payEntry)
	}
	return ret
}

func (ps *PayEntryService) PDKPayInCallback(cb pdkpay.PayNotify) (resKey string) {
	status := cb.Data.TradeStatus
	if status != pdkpay.PayInStatusSuccess &&
		status != pdkpay.PayInStatusCancel {
		// 中间状态不做更新和通知
		logger.Warn("PayEntryService_PDKPayInCallback_Warn | unsupported status=%+v", status)
		return ""
	}

	//更新状态
	payEntry := new(model.PayEntry)
	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("PayEntryService_PDKPayInCallback_DBError | err=%v | cb=%+v", err, cb)
		return "DB_ERROR"
	}
	exist, err := engine.Where("order_id=?", cb.Data.OrderSn).Get(payEntry)
	if err != nil {
		logger.Error("PayEntryService_PDKPayInCallback_Query_DBError | err=%v | cb=%v", err, cb)
		return "DB_ERROR"
	}
	if !exist {
		logger.Error("PayEntryService_PDKPayCallback_NotExist | err=%v | cb=%v", err, cb)
		return "DB_ERROR"
	}

	//
	ins, err := PayAccountConfServe.GetSpecificPayInstance(model.PDKPAY, payEntry.PayAccountId)
	if err != nil {
		logger.Error("PDKPayInCallback_Invalid PayAccountId=%v | error=%v ", payEntry.PayAccountId, err)
		return "unsupported PayAccountId"
	}

	cbData := cb.DataMap()
	if !ins.InSignature(cbData) {
		logger.Error("PDKPayOutCallback_Invalid Signature %v | %v ", payEntry.PayAccountId, cbData)
		return "invalid sign"
	}

	//已经修改不再变化
	if payEntry.Status != model.PAYING {
		logger.Error("PayEntryService_PDKPayInCallback_Repeat | err=%v | cb=%v", err, cb)
		return "REPEAT_CALLBACK"
	}

	tradeStatus := cb.Data.TradeStatus
	if tradeStatus == pdkpay.PayInStatusSuccess {
		payEntry.Status = model.PAYSUCCESS
		payEntry.FinishTime = time.Now().Unix()
	} else if tradeStatus == pdkpay.PayInStatusCancel {
		payEntry.Status = model.PAYFAILD
		payEntry.ThirdCode = fmt.Sprintf("%v", tradeStatus)
		payEntry.ThirdDesc = fmt.Sprintf("%v-%v", payEntry.ThirdDesc, tradeStatus)
		payEntry.FinishTime = time.Now().Unix()
	} else {
		//暂不处理
		// processing
		return ""
	}

	rowCount, err := engine.Where("id=?", payEntry.Id).Update(payEntry)
	if err != nil {
		logger.Error("PService_PDKPayInCallback_Query_DBError | err=%v | cb=%v", err, cb)
		return "DB_ERROR"
	}
	logger.Debug("PDKPayInCallback_Info | payEntry=%+v | rowCount=%v | cb=%v", payEntry, rowCount, cb)
	if rowCount > 0 {
		//发起回调
		go ps.SendEntryCallback(payEntry)
	}
	return ""
}

func (ps *PayEntryService) ZWPayInCallback(cb zwpay.PayInNotify) (resKey string) {
	status := cb.Data.Status
	if status != zwpay.ZWPayInSuccess && status != zwpay.ZWPayInFailure && status != zwpay.ZWPayInRefund {
		logger.Warn("PayEntryService_ZWPayInCallback_Not Deal Status=%v | cb=%+v", status, cb)
		return ""
	}

	//更新状态
	payEntry := new(model.PayEntry)
	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("PayEntryService_ZWPayInCallback_DBError | err=%v | cb=%+v", err, cb)
		return "DB_ERROR"
	}
	exist, err := engine.Where("order_id=?", cb.Data.MerchantOrderNo).Get(payEntry)
	if err != nil {
		logger.Error("PayEntryService_ZWPayInCallback_Query_DBError | err=%v | cb=%v", err, cb)
		return "DB_ERROR"
	}
	if !exist {
		logger.Error("PayEntryService_ZWPayCallback_NotExist | err=%v | cb=%v", err, cb)
		return "DB_ERROR"
	}

	//已经成功的不再变化
	//如果是失败的，可能还会返回成功状态
	if payEntry.Status == model.PAYSUCCESS && cb.Data.Status != zwpay.ZWPayInSuccess {
		logger.Error("PayEntryService_ZWPayInCallback_Repeat | err=%v | cb=%v", err, cb)
		msg := fmt.Sprintf("ZWIn callback repeated order[%v] success->fail", payEntry.OrderId)
		common.PushAlarmEvent(common.Warning, "ZWIn", "ZWInCallback", cb.Data.OrderNo, msg)

		return "REPEAT_CALLBACK"
	}

	oldStatus := payEntry.Status
	tradeStatus := cb.Data.Status
	if tradeStatus == zwpay.ZWPayInSuccess {
		payEntry.Status = model.PAYSUCCESS
		payEntry.FinishTime = time.Now().Unix()
	} else if tradeStatus == zwpay.ZWPayInFailure || tradeStatus == zwpay.ZWPayInRefund {
		payEntry.Status = model.PAYFAILD
		payEntry.ThirdCode = fmt.Sprintf("%v", tradeStatus)
		payEntry.ThirdDesc = fmt.Sprintf("%v-%v:%v", payEntry.ThirdDesc, tradeStatus, cb.Data.Message)
		payEntry.FinishTime = time.Now().Unix()
	} else {
		//暂不处理
		// processing
		return ""
	}

	// 状态没用更新, 不更新数据库
	if oldStatus == payEntry.Status {
		return ""
	}

	rowCount, err := engine.Where("id=? and status =?", payEntry.Id, oldStatus).Update(payEntry)
	if err != nil {
		logger.Error("PService_ZWPayInCallback_Query_DBError | err=%v | cb=%v", err, cb)
		return "DB_ERROR"
	}
	logger.Debug("ZWPayInCallback_Info | payEntry=%+v | rowCount=%v | cb=%v", payEntry, rowCount, cb)
	if rowCount > 0 {
		//发起回调
		go ps.SendEntryCallback(payEntry)
	}
	return ""
}

func (ps *PayEntryService) FMPayInCallback(cb fmpay.PayInNotify) (resKey string) {
	//更新状态
	payEntry := new(model.PayEntry)
	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("PayEntryService_FMPayInCallback_DBError | err=%v | cb=%+v", err, cb)
		return "DB_ERROR"
	}
	exist, err := engine.Where("order_id=?", cb.OrderNo).Get(payEntry)
	if err != nil {
		logger.Error("PayEntryService_FMPayInCallback_Query_DBError | err=%v | cb=%v", err, cb)
		return "DB_ERROR"
	}
	if !exist {
		logger.Error("PayEntryService_FMPayCallback_NotExist | err=%v | cb=%v", err, cb)
		return "DB_ERROR"
	}

	//已经成功的不再变化
	//如果是失败的，可能还会返回成功状态
	if payEntry.Status != model.PAYING {
		logger.Error("PayEntryService_FMPayInCallback_Repeat | err=%v | cb=%v", err, cb)
		centerlog.Info(centerlog.MsgRepeatCallBack, map[string]interface{}{
			centerlog.FieldAppId:          payEntry.AppId,
			centerlog.FieldAccountId:      payEntry.PayAccountId,
			centerlog.FieldOrderId:        payEntry.OrderId,
			centerlog.FieldAppOrderId:     payEntry.AppOrderId,
			centerlog.FieldPaymentOrderID: payEntry.PaymentOrderId,
			centerlog.FieldAmount:         payEntry.Amount,
			centerlog.FieldOrderStatus:    payEntry.Status,
			centerlog.FieldThirdDesc:      payEntry.ThirdDesc,
			centerlog.FieldCBStatus:       cb.State,
		})
		return "REPEAT_CALLBACK"
	}

	oldStatus := payEntry.Status
	tradeStatus := cb.State
	if tradeStatus == fmpay.PayInSuccess {
		payEntry.Status = model.PAYSUCCESS
		//actualPay, err := strconv.ParseFloat(cb.ActualPayAmount, 64)
		//if err != nil {
		//	logger.Error("PayEntryService_FMPayInCallback cal discount error[%v]-[%v]", err, cb.ActualPayAmount)
		//} else {
		//	payEntry.Discount = payEntry.Amount - actualPay
		//}
		payEntry.FinishTime = time.Now().Unix()
	} else {
		//暂不处理
		// processing
		logger.Error("PayEntryService_FMPayInCallback Invalid Status | cb=%v", err, cb)
		return ""
	}

	// 状态没用更新, 不更新数据库
	if oldStatus == payEntry.Status {
		return ""
	}

	rowCount, err := engine.Where("id=? and status =?", payEntry.Id, oldStatus).Update(payEntry)
	if err != nil {
		logger.Error("PService_FMPayInCallback_DBError | err=%v | cb=%v", err, cb)
		return "DB_ERROR"
	}
	logger.Debug("FMPayInCallback_Info | payEntry=%+v | rowCount=%v | cb=%v", payEntry, rowCount, cb)
	if rowCount > 0 {
		//发起回调
		go ps.SendEntryCallback(payEntry)
	}
	return ""
}

func (ps *PayEntryService) LoogPayInCallback(cb url.Values) (resKey string) {
	//更新状态
	payEntry := new(model.PayEntry)
	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("PayEntryService_LoogPayInCallback_DBError | err=%v | cb=%+v", err, cb)
		return "DB_ERROR"
	}

	orderId := cb.Get(loogpay.FieldOrderNo)
	exist, err := engine.Where("order_id=?", orderId).Get(payEntry)
	if err != nil {
		logger.Error("PayEntryService_LoogPayInCallback_Query_DBError | err=%v | cb=%v", err, cb)
		return "DB_ERROR"
	}
	if !exist {
		logger.Error("PayEntryService_LoogPayCallback_NotExist | err=%v | cb=%v", err, cb)
		return "DB_ERROR"
	}

	ins, err := PayAccountConfServe.GetSpecificPayInstance(model.LOOGPAY, payEntry.PayAccountId)
	if err != nil {
		logger.Error("LoogPayInCallback_Invalid PayAccountId=%v | error=%v ", payEntry.PayAccountId, err)
		return "unsupported PayAccountId"
	}

	if !ins.InSignature(cb) {
		logger.Error("LoogPayOutCallback_Invalid Signature %v | %v ", payEntry.PayAccountId, cb)
		return "invalid sign"
	}

	//已经修改不再变化
	oldStatus := payEntry.Status
	tradeStatus := cb.Get(loogpay.FieldTradeState)
	if payEntry.Status == model.PAYSUCCESS && tradeStatus != loogpay.Success {
		logger.Error("PayEntryService_LoogPayInCallback_Repeat | err=%v | cb=%v", err, cb)
		msg := fmt.Sprintf("loogpay callback repeated order[%v] success->fail", payEntry.OrderId)
		common.PushAlarmEvent(common.Warning, "loogpay", "loog_callback", cb.Encode(), msg)

		return "REPEAT_CALLBACK"
	}

	if tradeStatus == loogpay.Success {
		payEntry.Status = model.PAYSUCCESS
		payEntry.FinishTime = time.Now().Unix()
	} else if tradeStatus == loogpay.Cancel || tradeStatus == loogpay.Failed {
		payEntry.Status = model.PAYFAILD
		payEntry.ThirdCode = fmt.Sprintf("%v", tradeStatus)
		payEntry.ThirdDesc = fmt.Sprintf("%v-%v-%v", cb.Get(loogpay.FieldRespDesc), payEntry.ThirdDesc, cb.Get(loogpay.FieldBankMsg))
		payEntry.FinishTime = time.Now().Unix()
	} else {
		//暂不处理
		// processing
		return ""
	}

	//
	if oldStatus == payEntry.Status {
		return ""
	}

	rowCount, err := engine.Where("id=?", payEntry.Id).Update(payEntry)
	if err != nil {
		logger.Error("PService_LoogPayInCallback_Query_DBError | err=%v | cb=%v", err, cb)
		return "DB_ERROR"
	}
	logger.Debug("LoogPayInCallback_Info | payEntry=%+v | rowCount=%v | cb=%v", payEntry, rowCount, cb)
	if rowCount > 0 {
		//发起回调
		go ps.SendEntryCallback(payEntry)
	}
	return ""
}

func (ps *PayEntryService) XPayInCallback(cb url.Values) (resKey string) {
	//更新状态
	payEntry := new(model.PayEntry)
	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("PayEntryService_XPayInCallback_DBError | err=%v | cb=%+v", err, cb)
		return "DB_ERROR"
	}

	orderId := cb.Get(xpay.FieldMchNo)
	exist, err := engine.Where("order_id=?", orderId).Get(payEntry)
	if err != nil {
		logger.Error("PayEntryService_XPayInCallback_Query_DBError | err=%v | cb=%v", err, cb)
		return "DB_ERROR"
	}
	if !exist {
		logger.Error("PayEntryService_XPayCallback_NotExist | err=%v | cb=%v", err, cb)
		return "DB_ERROR"
	}

	ins, err := PayAccountConfServe.GetSpecificPayInstance(model.XPAY, payEntry.PayAccountId)
	if err != nil {
		logger.Error("XPayInCallback_Invalid PayAccountId=%v | error=%v ", payEntry.PayAccountId, err)
		return "unsupported PayAccountId"
	}

	if !ins.InSignature(cb) {
		logger.Error("XPayOutCallback_Invalid Signature %v | %v ", payEntry.PayAccountId, cb)
		return "invalid sign"
	}

	//已经修改不再变化
	if payEntry.Status != model.PAYING {
		logger.Error("PayEntryService_XPayInCallback_Repeat | err=%v | cb=%v", err, cb)
		return ""
	}

	tradeStatus := cb.Get(xpay.FieldState)
	if tradeStatus == xpay.PaySuccessStr {
		payEntry.Status = model.PAYSUCCESS
		payEntry.FinishTime = time.Now().Unix()
	} else if tradeStatus == xpay.PayFailedStr {
		payEntry.Status = model.PAYFAILD
		payEntry.ThirdCode = cb.Get(xpay.FieldErrCode)
		payEntry.ThirdDesc = fmt.Sprintf("%v-%v", cb.Get(xpay.FieldErrMsg), payEntry.ThirdDesc)
		payEntry.FinishTime = time.Now().Unix()
	} else {
		//暂不处理
		// processing
		return ""
	}

	rowCount, err := engine.Where("id=?", payEntry.Id).Update(payEntry)
	if err != nil {
		logger.Error("PService_XPayInCallback_Query_DBError | err=%v | cb=%v", err, cb)
		return "DB_ERROR"
	}
	logger.Debug("XPayInCallback_Info | payEntry=%+v | rowCount=%v | cb=%v", payEntry, rowCount, cb)
	if rowCount > 0 {
		//发起回调
		go ps.SendEntryCallback(payEntry)
	}
	return ""
}

func (ps *PayEntryService) UpPayInCallback(cb uppay.PayInNotify) (resKey string) {
	//更新状态
	payEntry := new(model.PayEntry)
	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("PayEntryService_UpPayInCallback_DBError | err=%v | cb=%+v", err, cb)
		return "DB_ERROR"
	}

	exist, err := engine.Where("order_id=?", cb.MerchantTradeId).Get(payEntry)
	if err != nil {
		logger.Error("PayEntryService_UpPayInCallback_Query_DBError | err=%v | cb=%v", err, cb)
		return "DB_ERROR"
	}
	if !exist {
		logger.Error("PayEntryService_UpPayCallback_NotExist | err=%v | cb=%v", err, cb)
		return "DB_ERROR"
	}
	if payEntry.PaymentOrderId != cb.TradeId {
		logger.Error("PayEntryService_UpPayCallback_TradeID Fail |%v| err=%v | cb=%v", payEntry.PaymentOrderId, err, cb)
		return "check trade id fail"
	}

	ins, err := PayAccountConfServe.GetSpecificPayInstance(model.UPPAY, payEntry.PayAccountId)
	if err != nil {
		logger.Error("UPayInCallback_Invalid PayAccountId=%v | error=%v ", payEntry.PayAccountId, err)
		return "unsupported PayAccountId"
	}

	if !ins.InSignature(cb) {
		logger.Error("UPayOutCallback_Invalid Signature %v | %v ", payEntry.PayAccountId, cb)
		return "invalid sign"
	}

	//已经修改不再变化
	if payEntry.Status != model.PAYING {
		logger.Error("PayEntryService_UpPayInCallback_Repeat | err=%v | cb=%v", err, cb)
		return ""
	}

	if cb.Status == uppay.PayApprovedStr {
		payEntry.Status = model.PAYSUCCESS
		payEntry.FinishTime = time.Now().Unix()
	} else if cb.Status == uppay.PayDeclinedStr || cb.Status == uppay.PayErrorStr {
		payEntry.Status = model.PAYFAILD
		payEntry.ThirdCode = cb.Status
		payEntry.ThirdDesc = fmt.Sprintf("%v-->%v", cb.Message, cb.TechnicalMessage)
		payEntry.FinishTime = time.Now().Unix()
	} else {
		//暂不处理
		// processing
		return ""
	}

	rowCount, err := engine.Where("id=?", payEntry.Id).Update(payEntry)
	if err != nil {
		logger.Error("Service_UpPayInCallback_Query_DBError | err=%v | cb=%v", err, cb)
		return "DB_ERROR"
	}
	logger.Debug("UpPayInCallback_Info | payEntry=%+v | rowCount=%v | cb=%v", payEntry, rowCount, cb)
	if rowCount > 0 {
		//发起回调
		go ps.SendEntryCallback(payEntry)
	}
	return ""
}

/**
 * 通知业务方
 */
func (ps *PayEntryService) SendEntryCallback(payEntry *model.PayEntry) {
	if payEntry.Uid != 0 {
		//平台业务
		//账户记账 13.235.162.105
		err := AccountServ.AddCollect(payEntry)
		if err != nil {
			logger.Error("SendEntryCallback_AddCollect_Err | err=%v | data=%v", err, payEntry)
			return
		}
		if payEntry.Status != model.PAYING {
			centerlog.Info(centerlog.MsgPayInCb, map[string]interface{}{
				centerlog.FieldAppId:          payEntry.AppId,
				centerlog.FieldAccountId:      payEntry.PayAccountId,
				centerlog.FieldOrderId:        payEntry.OrderId,
				centerlog.FieldAppOrderId:     payEntry.AppOrderId,
				centerlog.FieldPaymentOrderID: payEntry.PaymentOrderId,
				centerlog.FieldAmount:         payEntry.Amount,
				centerlog.FieldOrderStatus:    payEntry.Status,
				centerlog.FieldThirdDesc:      payEntry.ThirdDesc,
			})
		}

		ps.SendEntryPlatFromCallback(payEntry)
		return
	}
	//url := viper.GetString(fmt.Sprintf("callback.%v.order_url", payEntry.AppId))
	////fmt.Println(fmt.Sprintf("callback.%v.order_url", payEntry.AppId))
	////发起回调
	//callbackData := &model.OrderCallbackData{
	//	PayChannel:     payEntry.PayChannel,
	//	AppOrderId:     payEntry.AppOrderId,
	//	OrderId:        payEntry.OrderId,
	//	PaymentOrderId: payEntry.PaymentOrderId,
	//	ThirdDesc:      payEntry.ThirdDesc,
	//	PaymentId:      payEntry.PaymentId,
	//	Status:         payEntry.Status,
	//	Amount:         payEntry.Amount,
	//	UserId:         payEntry.UserId,
	//	Phone:          payEntry.Phone,
	//	Email:          payEntry.Email,
	//}
	//header := map[string][]string{
	//	"Content-Type": {"application/json"},
	//}
	////状态码判断
	//var status int
	//var response string
	//i := 1
	//idleDuration := 10 * time.Second
	//idleDelay := time.NewTimer(idleDuration)
	//defer idleDelay.Stop()
	////尝试回调3次
	//for ; i < 4; i++ {
	//	res, status, err := utils.HttpRequest(url, "POST", "JSON", callbackData, header)
	//	if err != nil {
	//		logger.Error("PayEntryService_SendEntryCallback_Error | err=%v | data=%+v", err, payEntry)
	//	}
	//	response = string(res)
	//	//状态码判断
	//	if status == http.StatusOK {
	//		break
	//	}
	//	select {
	//	case <-idleDelay.C:
	//		logger.Debug("PayEntryService_endEntryCallback_time_%v | cb=%v", i, callbackData)
	//	}
	//	idleDelay.Reset(idleDuration)
	//}
	//cbList := new(model.PayEntryCallbackList)
	//cbList.PayChannel = payEntry.PayChannel
	//cbList.AppId = payEntry.AppId
	//cbList.OrderId = payEntry.OrderId
	//cbList.AppOrderId = payEntry.AppOrderId
	//cbList.Status = payEntry.Status
	//cbList.ReturnCode = fmt.Sprint(status)
	//cbList.Times = i
	//cbList.Response = response
	//cbList.PaymentOrderId = payEntry.PaymentOrderId
	//engine, err := dao.GetMysql()
	//if err != nil {
	//	logger.Error("PayEntryService_SendEntryCallback_DBError | err=%v | cb=%+v", err, cbList)
	//}
	//_, err = engine.Insert(cbList)
	//if err != nil {
	//	logger.Error("PayEntryService_SendEntryCallback_InsertDBError | err=%v | cb=%+v", err, cbList)
	//}
}

const Target = "Target"
const Safe = "P$#k"
const NotifyHost = "https://notify-payment.firstpayer.com"

/**
 * 通知业务方
 */
func (ps *PayEntryService) SendEntryPlatFromCallback(payEntry *model.PayEntry) {
	settings, ok := GetAccountDeveloperSettingsFromCache(payEntry.AppId)
	if !ok {
		logger.Error("SendEntryPlatFromCallback_DevelopSettings_Err | data=%v", payEntry)
		return
	}
	hosturl := settings.CollectCallbackUrl
	paymentOrderId := url.QueryEscape(payEntry.PaymentOrderId)
	//发起回调
	callbackData := &model.OrderCallbackData{
		AppOrderId:     payEntry.AppOrderId,
		OrderId:        payEntry.OrderId,
		PaymentOrderId: paymentOrderId,
		Status:         payEntry.Status,
		Amount:         payEntry.Amount,
	}

	// 墨西哥电汇多次支付情况--兼容老版本客户签名校验这里分开写
	if payEntry.MultiPay {
		callbackData.PaymentOrderId = paymentOrderId
		callbackData.ThirdDesc = payEntry.ThirdDesc
	}
	jsData, _ := json.Marshal(callbackData)
	sk, _ := utils.AesDecryptString(settings.SecretEncrypt)
	Signature := utils.ComputeHmacSha256(string(jsData), sk)
	header := map[string][]string{
		"Content-Type": {"application/json"},
		"Signature":    {Signature},
		Target:         {hosturl},
		Safe:           {"1"},
	}
	logger.Debug("PayEntryService_SendEntryPlatFromCallback_Info | data=%v | sk=%v | Signature=%v", string(jsData), settings.AppSecret, Signature)
	//状态码判断
	var (
		status   int
		response string
		res      []byte
		err      error
	)
	i := 1
	idleDuration := 10 * time.Second
	idleDelay := time.NewTimer(idleDuration)
	defer idleDelay.Stop()
	//尝试回调3次
	for ; i < 4; i++ {
		res, status, err = utils.HttpRequest(NotifyHost, "POST", "JSON", callbackData, header)
		if err != nil || status != http.StatusOK {
			logger.Error("PayEntryService_SendEntryPlatFromCallback_Error | err=%v|%v | url:%v | order:%v|%v", status, err, hosturl, callbackData.OrderId, callbackData.AppOrderId)
		}
		response = string(res)
		//状态码判断
		if status == http.StatusOK {
			break
		}
		select {
		case <-idleDelay.C:
			logger.Debug("PayEntryService_SendEntryPlatFromCallback_time_%v | cb=%v", i, callbackData)
		}
		idleDelay.Reset(idleDuration)
	}
	cbList := new(model.PayEntryCallbackList)
	cbList.PayChannel = payEntry.PayChannel
	cbList.AppId = payEntry.AppId
	cbList.OrderId = payEntry.OrderId
	cbList.AppOrderId = payEntry.AppOrderId
	cbList.Status = 1
	cbList.ReturnCode = fmt.Sprint(status)
	cbList.Times = i
	cbList.Response = response
	cbList.PaymentOrderId = payEntry.PaymentOrderId
	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("PayEntryService_SendEntryPlatFromCallback_DBError | err=%v | cb=%+v", err, cbList)
	}
	payCb := &model.PayEntryCallbackList{}
	exist, err := engine.Where("order_id=?", payEntry.OrderId).Get(payCb)
	if err != nil {
		logger.Error("PayEntryService_SendEntryPlatFromCallback_GetDBError | err=%v | cb=%+v", err, cbList)
		return
	}
	if exist {
		_, err = engine.Where("id=?", payCb.Id).Update(&model.PayEntryCallbackList{
			Status:   cbList.Status,
			Response: response,
		})
		if err != nil {
			logger.Error("PayEntryService_SendEntryPlatFromCallback_UpdateDBError | err=%v | cb=%+v", err, cbList)
		}
	} else {
		_, err = engine.Insert(cbList)
		if err != nil {
			logger.Error("PayEntryService_SendEntryPlatFromCallback_InsertDBError | err=%v | cb=%+v", err, cbList)
		}
	}
}

func (ps *PayEntryService) RetryPlatFromCallback(uid int64, orderId string) error {
	go func() {
		engine, err := dao.GetMysql()
		if err != nil {
			logger.Error("PayEntryService_RetryPlatFromCallback_DBError | err=%v", err)
			return
		}
		payEntry := new(model.PayEntry)
		rdsKey := fmt.Sprintf("payEentry:%v:%v", uid, orderId)
		data := dao.RedisGetBytes(rdsKey)
		if len(data) < 1 { // nodata
			exist, err := engine.Where("uid=? AND order_id=?", uid, orderId).Get(payEntry)
			if err != nil {
				logger.Error("PayEntryService_RetryPlatFromCallback_Query_DBError | err=%v | orderId=%v", err, orderId)
				return
			}
			if !exist {
				logger.Error("PayEntryService_RetryPlatFromCallback_Query_DBError | err=NotExist | orderId=%v", orderId)
				return
			}

			// 缓存一下订单状态
			var exp int = 30 // 默认30s
			delay := time.Now().Unix() - payEntry.Created
			if delay > 3600 { // 订单成功、失败状态延迟 超过1小时了
				exp = 600
			} else if delay > 1800 { // 超过半小时
				exp = 300
			}

			b, _ := json.Marshal(payEntry)
			if len(b) > 0 {
				dao.RedisSetBytes(rdsKey, b, exp)
			}
		} else {
			err = json.Unmarshal(data, &payEntry)
			if err != nil {
				logger.Error("PayEntryService_RetryPlatFromCallback_Unmarshal_Error | err=%v | orderId=%v", err, orderId)
				return
			}
		}
		ps.SendEntryPlatFromCallback(payEntry)

		// 兼容一下墨西哥电汇多次回调情况
		if payEntry.Status == model.PAYFAILD && payEntry.PayAccountId == "serpay_mx" {
			li := make([]model.PayEntry, 0, 8)
			engine.Where("uid=? AND payment_order_id=? AND status = ?", uid, payEntry.PaymentOrderId, model.PAYSUCCESS).Get(li)
			for _, item := range li {
				ps.SendEntryPlatFromCallback(&item)
			}
		}
	}()

	return nil
}

func (ps *PayEntryService) PayEntryRollBack(id int64, isRebackFee bool) error {
	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("PayEntryService_PayEntryRollBack_DBError | err=%v", err)
		return err
	}
	payEntry := new(model.PayEntry)
	exist, err := engine.Where("id=?", id).Get(payEntry)
	if err != nil {
		logger.Error("PayEntryService_PayEntryRollBack_Query_DBError | err=%v | id=%v", err, id)
		return err
	}
	if !exist {
		return errors.New("NotExist")
	}
	if payEntry.Status != model.PAYSUCCESS {
		return fmt.Errorf("the order status[%v] is not success", payEntry.Status)
	}
	err = AccountServ.ReversedPayEntry(payEntry, isRebackFee)
	if err != nil {
		logger.Error("PayEntryService_PayEntryRollBack_ReversedError | err=%v | id=%v", err, id)
	} else {
		go func() {
			payEntry.Status = model.PAYROLLBACK
			ps.SendEntryPlatFromCallback(payEntry)
		}()
	}
	return err
}

func (ps *PayEntryService) PayEntryRollBackByOrderId(orderId string, isRebackFee bool) error {
	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("PayEntryService_PayEntryRollBackByOrderId_DBError | err=%v", err)
		return err
	}
	payEntry := new(model.PayEntry)
	exist, err := engine.Where("order_id=?", orderId).Get(payEntry)
	if err != nil {
		logger.Error("PayEntryService_PayEntryRollBackByOrderId__Query_DBError | err=%v | order_id=%v", err, orderId)
		return err
	}
	if !exist {
		return errors.New("NotExist")
	}
	if payEntry.Status != model.PAYSUCCESS {
		return fmt.Errorf("the order status[%v] is not success", payEntry.Status)
	}
	err = AccountServ.ReversedPayEntry(payEntry, isRebackFee)
	if err != nil {
		logger.Error("PayEntryService_PayEntryRollBackByOrderId__ReversedError | err=%v | order_id=%v", err, orderId)
	} else {
		go func() {
			payEntry.Status = model.PAYROLLBACK
			ps.SendEntryPlatFromCallback(payEntry)
		}()
	}
	return err
}

func (ps *PayEntryService) CheckSpeed(id string, opt string, speed int64) bool {
	rdsKey := fmt.Sprintf("%v:%v:%v", id, opt, time.Now().Unix())
	value, err := dao.RedisIncr(rdsKey, 1)
	if err != nil { // redis错误就允许
		logger.Error("PayEntryService_CheckSpeed | err=%v", err)
		return true
	}

	if value <= 2 {
		dao.RedisExpire(rdsKey, 5)
	}

	return value < speed
}
