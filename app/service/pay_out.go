package service

import (
	"encoding/json"
	"errors"
	"fmt"
	"funzone_pay/app/validator"
	"funzone_pay/centerlog"
	"funzone_pay/channel/cashfree"
	"funzone_pay/channel/fmpay"
	"funzone_pay/channel/loogpay"
	"funzone_pay/channel/payflash"
	"funzone_pay/channel/pdkpay"
	"funzone_pay/channel/uppay"
	"funzone_pay/channel/xpay"
	"funzone_pay/channel/zwpay"
	"funzone_pay/dao"
	"funzone_pay/model"
	"funzone_pay/utils"
	"net/http"
	"net/url"
	"time"

	"github.com/wonderivan/logger"
	"xorm.io/xorm"
)

var payOutService *PayOutService

type PayOutService struct{}

func GetPayOutService() *PayOutService {
	if payOutService == nil {
		payOutService = new(PayOutService)
	}
	return payOutService
}

/**
 * 创建提现订单
 */
//func (po *PayOutService) CreateOutOrder(payOutArg validator.PayOutValidator) (*model.PayOut, string) {
//	//根据支付渠道创建付款对象
//	engine, err := dao.GetMysql()
//	if err != nil {
//		logger.Error("PayOutService_CreateOutOrder_EngineError | err=%v | data=%v", err, payOutArg)
//		return nil, "DB_ERROR"
//	}
//	/*//基础风控策略 1.单日支付次数 2.最大放款数
//	rskRes, err := RSK.PayOutRisk(payOutArg)
//	if err != nil {
//		logger.Error("PayOutService_CreateOutOrder_RSKError | err=%v | data=%v", err, payOutArg)
//		return nil, "RISK_ERROR"
//	}
//	if !rskRes {
//		//拉黑
//		SmsWarning("AddBlackWarn")
//		BlackListServ.AddBlack(payOutArg.BankCard, "bank_card")
//		logger.Error("PayOutService_CreateOutOrder_RSK_NO_PASS | data=%v", err, payOutArg)
//		return nil, "RISK_NO_PASS"
//	}*/
//	//幂等校验
//	exist, err := engine.Where("app_order_id=?", payOutArg.AppOrderId).Get(&model.PayOut{})
//	if err != nil {
//		logger.Error("PayOutService_CreateOutOrder_DBError | err=%v", err)
//		return nil, "DB_ERROR"
//	}
//	if exist {
//		return nil, "APP_ORDER_ID_EXIST"
//	}
//	//路由
//	payOutArg.PayChannel = model.CASHFREE
//
//	payInstance := channel.GetPayInstance(payOutArg.PayChannel)
//	if payInstance == nil {
//		return nil, "CHANNEL_ERROR"
//	}
//	pOut := new(model.PayOut)
//	pOut, err = payInstance.PayOut(payOutArg)
//	if err != nil {
//		return nil, err.Error()
//	}
//	return pOut, ""
//}

/*
*
出金 - 内部应用调用 - 可根据渠道取切换
*/
//func (po *PayOutService) NewCreateOutOrder(payOutArg validator.PayOutValidator) (*model.PayOut, string) {
//	//根据支付渠道创建付款对象 - 可根据来源做切换
//	var (
//		payInstance channel.PaymentContract
//		err         error
//	)
//	if payOutArg.AppId == "rummy" {
//		payOutArg.Uid = 78
//		payOutArg.AppId = "rummybank123"
//	} else if payOutArg.AppId == "rummy_josh" || payOutArg.AppId == "rummyjosh123456" {
//		payOutArg.Uid = 40
//		payOutArg.AppId = "rummyjosh123456"
//	} else {
//		return nil, "APP_ERROR"
//	}
//	payInstance, err = PayAccountConfServe.GetPayOutInstance(payOutArg)
//	if err != nil {
//		return nil, "CHANNEL_ERROR"
//	}
//	pOut := new(model.PayOut)
//	pOut, err = payInstance.PayOut(payOutArg)
//	if pOut == nil || err != nil {
//		return nil, "INTERNAL_ERROR"
//	}
//	return pOut, ""
//}

/**
 * 创建平台提现订单
 */
func (po *PayOutService) PlatFromOrder(uid int64, payOutArg validator.PayOutValidator) (*model.PayOut, string) {
	//银行卡在不在黑名单
	//if payOutArg.PayType == model.PT_BANK && BlackListServ.IsBlack(payOutArg.BankCard, BlackTypeBankCard) {
	//	logger.Error("PayOutService_PlatFromOrder_Forbid | data=%v", payOutArg)
	//	return nil, "ACCESS_FORBID"
	//}
	//根据支付渠道创建付款对象
	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("PayOutService_PlatFromOrder_EngineError | err=%v | data=%v", err, payOutArg)
		return nil, "DB_ERROR"
	}
	//基础风控策略 1.单日支付次数 2.最大放款数
	//rskRes, err := RSK.CheckRisk(payOutArg)
	//if err != nil {
	//	logger.Error("PayOutService_CreateOutOrder_RSKError | err=%v | data=%v", err, payOutArg)
	//	return nil, "RISK_ERROR"
	//}
	//if !rskRes {
	//	//拉黑
	//	SmsWarning("AddBlackWarn")
	//	BlackListServ.AddBlack(payOutArg.BankCard, "bank_card")
	//	logger.Error("PayOutService_CreateOutOrder_RSK_NO_PASS | data=%v", err, payOutArg)
	//	return nil, "RISK_NO_PASS"
	//}
	if payOutArg.Uid == 1 || payOutArg.Uid == 45 {
		return nil, "INVALID_REQUEST"
	}
	//幂等校验
	exist, err := engine.Where("app_order_id=? AND uid=?", payOutArg.AppOrderId, payOutArg.Uid).Get(&model.PayOut{})
	if err != nil {
		logger.Error("PayOutService_PlatFromOrder_DBError | err=%v", err)
		return nil, "DB_ERROR"
	}
	if exist {
		return nil, "APP_ORDER_ID_EXIST"
	}
	//路由
	payInstance, err := PayAccountConfServe.GetPayOutInstance(payOutArg)
	if err != nil {
		if err.Error() == "AMOUNT_TOO_SMALL" {
			return nil, err.Error()
		}
		logger.Error("PayOutService_PlatFromOrder_Error | err=%v | uid=%v", err, uid)
		return nil, "DB_ERROR"
	}
	if payInstance == nil {
		return nil, "CHANNEL_ERROR"
	}
	//判断账户余额，冻结金额
	frozenId, err := AccountServ.PayOut(uid, payOutArg.Amount)
	if err != nil {
		if err.Error() == "balance_insufficient" {
			return nil, err.Error()
		}
		logger.Error("PayOutService_PlatFromOrder_AccountErr | err=%v | payout=%v", err, payOutArg)
		return nil, "INTERNAL_ERROR"
	}

	pOut := new(model.PayOut)
	pOut, err = payInstance.PayOut(frozenId, payOutArg)
	if pOut != nil && pOut.Id != 0 {
		//更新冻结记录
		_, err = engine.Transaction(func(session *xorm.Session) (i interface{}, e error) {
			fStatus := model.FrozenDefault
			if pOut.Status == model.PAY_OUT_FAILD {
				fStatus = model.FrozenRelease
				account := new(model.Account)
				_, e = session.Where("uid=?", pOut.Uid).ForUpdate().Get(account)
				if e != nil {
					logger.Error("PayOutService_UpdateTransaction_FrozenErr | err=%v | id=%v", e, pOut.Id)
					return
				}

				// 开始写入流水记录，然后自动回滚冻结金额
				reverse := model.AccountTransactions{
					Uid:                  pOut.Uid,
					DataId:               pOut.Id,
					Amount:               pOut.Amount,
					Type:                 model.TransAutoReversed,
					StartBalance:         account.Balance,
					EndBalance:           account.Balance + pOut.Amount,
					StartUnSettleBalance: account.UnsettledBalance,
					EndUnSettleBalance:   account.UnsettledBalance,
				}

				_, e = session.Insert(reverse)
				if e != nil {
					logger.Error("PayOutService_UpdateTransaction_InsertErr | err=%v | id=%v", e, pOut.Id)
					return
				}

				//回滚用户额度
				_, e = session.Where("uid=?", pOut.Uid).Incr("balance", pOut.Amount).Update(&model.Account{})
				if e != nil {
					logger.Error("PayOutService_UpdateTransaction_reverse_Err | err=%v | id=%v", e, pOut.Id)
					return
				}
			}
			_, e = session.Where("id=? and status = ?", frozenId, model.FrozenDefault).Update(&model.AccountFrozenTransactions{
				DataId: pOut.Id,
				Status: fStatus,
			})
			if err != nil {
				logger.Error("PayOutService_UpdateTransaction_updateDataID_Err | err=%v | id=%v", e, pOut.Id)
			}
			return
		})
		if err != nil {
			logger.Error("PayOutService_PlatFromOrder_FrozenErr | err=%v | data=%v", err, payOutArg)
		}
	}
	if err != nil {
		logger.Error("PayOutService_PlatFromOrder_PaymentErr | err=%v | data=%v", err, payOutArg)
		return pOut, err.Error()
	}
	if pOut != nil {
		centerlog.Info(centerlog.MsgPayOutOrder, map[string]interface{}{
			centerlog.FieldAppId:          pOut.AppId,
			centerlog.FieldAccountId:      pOut.PayAccountId,
			centerlog.FieldOrderId:        pOut.OrderId,
			centerlog.FieldAppOrderId:     pOut.AppOrderId,
			centerlog.FieldPaymentOrderID: pOut.PaymentId,
			centerlog.FieldAmount:         pOut.Amount,
			centerlog.FieldThirdDesc:      pOut.ThirdDesc,
		})
	}

	return pOut, ""
}

func (po *PayOutService) PayOutRollBackCallbackById(orderId, admin string) (resKey string) {
	payOut := new(model.PayOut)
	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("PayOutService_PayoutRollback_DBError | err=%v | cb=%+v", err, orderId)
		return "DB_ERROR"
	}
	exist, err := engine.Where("order_id=?", orderId).Get(payOut)
	if err != nil {
		logger.Error("PayOutService_PayoutRollback_Query_DBError | err=%v | cb=%v", err, orderId)
		return "DB_ERROR"
	}
	if !exist {
		logger.Error("PayOutService_PayoutRollback_NotExist | err=%v | cb=%v", err, orderId)
		return "DB_ERROR"
	}

	if payOut.Status != model.PAY_OUT_SUCCESS {
		return "STATUS Error"
	}

	//AccountService
	payOut.Status = model.PAY_OUT_REVERSED
	err = AccountServ.ReversedPayout(payOut)
	if err != nil {
		logger.Error("PayOutService_PayOutRollback_err | err=%v | order=%v | %v", err, orderId, admin)
		return fmt.Sprintf("revert[%v] error", orderId)
	}

	payOut.ThirdCode = fmt.Sprintf("%v", "manul")
	payOut.ThirdDesc = fmt.Sprintf("admin[%v] roll back", admin)

	rowCount, err := engine.Where("id=?", payOut.Id).Update(payOut)
	if err != nil {
		logger.Error("PayOutService_PayOutRollback_Update_DBError | err=%v | %v | %v", err, orderId, admin)
		return "DB_ERROR"
	}
	logger.Debug("PayOutService_PayOutRollback success| order=%v | %v | rowCount=%v", orderId, admin, rowCount)
	if rowCount > 0 {
		//发起回调
		go po.SendPayOutCallback(payOut)
	}
	return ""
}

func (po *PayOutService) PayOutToFailById(orderId, admin string) (resKey string) {
	payOut := new(model.PayOut)
	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("PayOutService_PayoutToFail_DBError | err=%v | cb=%+v", err, orderId)
		return "DB_ERROR"
	}
	exist, err := engine.Where("order_id=?", orderId).Get(payOut)
	if err != nil {
		logger.Error("PayOutService_PayoutToFail_Query_DBError | err=%v | cb=%v", err, orderId)
		return "DB_ERROR"
	}
	if !exist {
		logger.Error("PayOutService_PayoutToFail_NotExist | err=%v | cb=%v", err, orderId)
		return "DB_ERROR"
	}

	if payOut.Status != model.PAY_OUT_ING {
		return "STATUS Error"
	}

	//AccountService
	payOut.Status = model.PAY_OUT_FAILD
	payOut.ThirdCode = fmt.Sprintf("%v", "manul")
	payOut.ThirdDesc = fmt.Sprintf("admin[%v] ToFail", admin)

	rowCount, err := engine.Where("id=?", payOut.Id).Update(payOut)
	if err != nil {
		logger.Error("PayOutService_PayOutToFail_Update_DBError | err=%v | %v | %v", err, orderId, admin)
		return "DB_ERROR"
	}
	logger.Debug("PayOutService_PayOutToFail success| order=%v | %v | rowCount=%v", orderId, admin, rowCount)
	if rowCount > 0 {
		//发起回调
		go po.SendPayOutCallback(payOut)
	}
	return ""
}

/**
 * cashfree提现回调
 * param: *model.CFPayOutCallBack cb
 * return: string
 */
func (po *PayOutService) CashFreePayOutCallback(cb *model.CFPayOutCallBack) (resKey string) {
	//更新状态
	payOut := new(model.PayOut)
	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("PayOutService_CashFreePayOutCallback_DBError | err=%v | cb=%+v", err, cb)
		return "DB_ERROR"
	}
	exist, err := engine.Where("order_id=?", cb.TransferId).Get(payOut)
	if err != nil {
		logger.Error("PayOutService_CashFreePayOutCallback_Query_DBError | err=%v | cb=%v", err, cb)
		return "DB_ERROR"
	}
	if !exist {
		logger.Error("PayOutService_CashFreePayOutCallback_NotExist | err=%v | cb=%v", err, cb)
		return "DB_ERROR"
	}
	//签名校验
	if payOut.Uid != 0 || payOut.AppId == "funzone_pay" {
		payIns, err := PayAccountConfServe.GetSpecificPayInstance(model.CASHFREE, payOut.PayAccountId)
		if err != nil {
			logger.Error("PayOutService_CashFreePayOutCallback_PlatformInstance | err=%v | cb=%v", err, cb)
			return "DB_ERROR"
		}
		if !payIns.OutSignature(cb) {
			logger.Error("PayOutService_CashFreePayOutCallback_PlatformOutSignature_Error | cb=%+v", cb)
			return "SIGN_ERROR"
		}
	} else {
		if !cashfree.GetCashFree().OutSignature(cb) {
			logger.Error("PayOutService_CashFreePayOutCallback_InSignature_Error | cb=%+v", cb)
			return "SIGN_ERROR"
		}
	}
	if payOut.Status == model.PAY_OUT_SUCCESS && cb.Event == "TRANSFER_REVERSED" {
		//要回退金额
		payOut.Status = model.PAY_OUT_REVERSED
		err = AccountServ.ReversedPayout(payOut)
		if err != nil {
			logger.Error("PayOutService_CashFreePayOutCallback_Reversed_err | err=%v | cb=%v", err, cb)
		}
	} else {
		//已经修改不再变化
		if payOut.Status != model.PAY_OUT_ING {
			logger.Error("PayOutService_CashFreePayOutCallback_Repeat | err=%v | cb=%v", err, cb)
			return "REPEAT_CALLBACK"
		}
		if cb.Event == "TRANSFER_SUCCESS" {
			payOut.Status = model.PAY_OUT_SUCCESS
			payOut.FinishTime = time.Now().Unix()
		} else if cb.Event == "TRANSFER_FAILED" || cb.Event == "TRANSFER_REVERSED" {
			payOut.Status = model.PAY_OUT_FAILD
			payOut.ThirdDesc = cb.Reason
			payOut.FinishTime = time.Now().Unix()
		}
	}
	rowCount, err := engine.Where("id=?", payOut.Id).Update(payOut)
	if err != nil {
		logger.Error("PayOutService_CashFreePayOutCallback_Update_DBError | err=%v", err, cb)
		return "DB_ERROR"
	}
	logger.Debug("CashFreePayOutCallback_Info | payOut=%+v | rowCount=%v", payOut, rowCount)
	if rowCount > 0 {
		//发起回调
		go po.SendPayOutCallback(payOut)
	}
	return ""
}

func (po *PayOutService) SerPayOutCallback(cb *model.SerOutCallback) (resKey string) {
	//更新状态
	payOut := new(model.PayOut)
	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("PayOutService_SerPayOutCallback_DBError | err=%v | cb=%+v", err, cb)
		return "DB_ERROR"
	}
	exist, err := engine.Where("order_id=?", cb.CustOrderNo).Get(payOut)
	if err != nil {
		logger.Error("PayOutService_SerPayOutCallback_Query_DBError | err=%v | cb=%v", err, cb)
		return "DB_ERROR"
	}
	if !exist {
		logger.Error("PayOutService_SerPayOutCallback_NotExist | err=%v | cb=%v", err, cb)
		return "DB_ERROR"
	}
	//签名校验
	if payOut.Uid != 0 || payOut.AppId == "funzone_pay" {
		payIns, err := PayAccountConfServe.GetSpecificPayInstance(model.SERPAY, payOut.PayAccountId)
		if err != nil {
			logger.Error("PayOutService_SerPayOutCallback_PlatformInstance | err=%v | cb=%v", err, cb)
			return "DB_ERROR"
		}
		if !payIns.OutSignature(cb) {
			logger.Error("PayOutService_SerPayOutCallback_PlatformOutSignature_Error | cb=%+v", cb)
			return "SIGN_ERROR"
		}
	} else {
		return "SIGN_ERROR"
	}
	if payOut.Status == model.PAY_OUT_SUCCESS && cb.OrdStatus == "09" {
		//要回退金额
		payOut.Status = model.PAY_OUT_REVERSED
		err = AccountServ.ReversedPayout(payOut)
		if err != nil {
			logger.Error("PayOutService_SerPayOutCallback_Reversed_err | err=%v | cb=%v", err, cb)
		}
	} else {
		//已经修改不再变化
		if payOut.Status != model.PAY_OUT_ING {
			logger.Error("PayOutService_SerPayOutCallback_Repeat | err=%v | cb=%v", err, cb)
			return "REPEAT_CALLBACK"
		}
		if cb.OrdStatus == "07" {
			payOut.Status = model.PAY_OUT_SUCCESS
			payOut.FinishTime = time.Now().Unix()
		} else if cb.OrdStatus == "08" || cb.OrdStatus == "09" {
			payOut.Status = model.PAY_OUT_FAILD
			payOut.FinishTime = time.Now().Unix()
		}
	}
	rowCount, err := engine.Where("id=?", payOut.Id).Update(payOut)
	if err != nil {
		logger.Error("PayOutService_SerPayOutCallback_Update_DBError | err=%v", err, cb)
		return "DB_ERROR"
	}
	logger.Debug("SerPayOutCallback_Info | payOut=%+v | rowCount=%v", payOut, rowCount)
	if rowCount > 0 {
		//发起回调
		go po.SendPayOutCallback(payOut)
	}
	return ""
}

func (po *PayOutService) OnionPayOutCallback(cb *model.OnionOutCallback) (resKey string) {
	//更新状态
	payOut := new(model.PayOut)
	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("PayOutService_OnionPayOutCallback_DBError | err=%v | cb=%+v", err, cb)
		return "DB_ERROR"
	}
	exist, err := engine.Where("order_id=?", cb.MerTransNo).Get(payOut)
	if err != nil {
		logger.Error("PayOutService_OnionPayOutCallback_Query_DBError | err=%v | cb=%v", err, cb)
		return "DB_ERROR"
	}
	if !exist {
		logger.Error("PayOutService_OnionPayOutCallback_NotExist | err=%v | cb=%v", err, cb)
		return "DB_ERROR"
	}
	//签名校验
	if payOut.Uid != 0 || payOut.AppId == "funzone_pay" {
		payIns, err := PayAccountConfServe.GetSpecificPayInstance(model.ONIONPAY, payOut.PayAccountId)
		if err != nil {
			logger.Error("PayOutService_OnionPayOutCallback_PlatformInstance | err=%v | cb=%v", err, cb)
			return "DB_ERROR"
		}
		if !payIns.OutSignature(cb) {
			logger.Error("PayOutService_OnionPayOutCallback_PlatformOutSignature_Error | cb=%+v", cb)
			return "SIGN_ERROR"
		}
	} else {
		return "SIGN_ERROR"
	}
	if payOut.Status == model.PAY_OUT_SUCCESS && cb.TransStatus == "failure" {
		//要回退金额
		payOut.Status = model.PAY_OUT_REVERSED
		err = AccountServ.ReversedPayout(payOut)
		if err != nil {
			logger.Error("PayOutService_OnionPayOutCallback_Reversed_err | err=%v | cb=%v", err, cb)
		}
	} else {
		//已经修改不再变化
		if payOut.Status != model.PAY_OUT_ING {
			logger.Error("PayOutService_OnionPayOutCallback_Repeat | err=%v | cb=%v", err, cb)
			return "REPEAT_CALLBACK"
		}
		if cb.TransStatus == "success" && cb.Message != "Transaction timed out" {
			payOut.Status = model.PAY_OUT_SUCCESS
			payOut.PaymentId = cb.Utr
			payOut.FinishTime = time.Now().Unix()
		} else if cb.TransStatus == "failure" {
			payOut.Status = model.PAY_OUT_FAILD
			payOut.FinishTime = time.Now().Unix()
		}
	}
	rowCount, err := engine.Where("id=?", payOut.Id).Update(payOut)
	if err != nil {
		logger.Error("PayOutService_OnionPayOutCallback_Update_DBError | err=%v", err, cb)
		return "DB_ERROR"
	}
	logger.Debug("OnionPayOutCallback_Info | payOut=%+v | rowCount=%v", payOut, rowCount)
	if rowCount > 0 {
		//发起回调
		go po.SendPayOutCallback(payOut)
	}
	return ""
}

func (po *PayOutService) FidyPayOutCallback(cb *model.FidyPayPayoutCallback) (resKey string) {
	//更新状态
	payOut := new(model.PayOut)
	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("PayOutService_FidyPayOutCallback_DBError | err=%v | cb=%+v", err, cb)
		return "DB_ERROR"
	}
	exist, err := engine.Where("order_id=?", cb.MerchantTrxnRefId).Get(payOut)
	if err != nil {
		logger.Error("PayOutService_FidyPayOutCallback_Query_DBError | err=%v | cb=%v", err, cb)
		return "DB_ERROR"
	}
	if !exist {
		logger.Error("PayOutService_FidyPayOutCallback_NotExist | err=%v | cb=%v", err, cb)
		return "DB_ERROR"
	}
	//签名校验
	//if payOut.Uid != 0 || payOut.AppId == "funzone_pay" {
	//	payIns, err := PayAccountConfServe.GetSpecificPayInstance(model.FIDYPAY, payOut.PayAccountId)
	//	if err != nil {
	//		logger.Error("PayOutService_FidyPayOutCallback_PlatformInstance | err=%v | cb=%v", err, cb)
	//		return "DB_ERROR"
	//	}
	//	if !payIns.OutSignature(cb) {
	//		logger.Error("PayOutService_FidyPayOutCallback_PlatformOutSignature_Error | cb=%+v", cb)
	//		return "SIGN_ERROR"
	//	}
	//} else {
	//	return "SIGN_ERROR"
	//}
	//已经修改不再变化
	if payOut.Status != model.PAY_OUT_ING {
		logger.Error("PayOutService_FidyPayOutCallback_Repeat | err=%v | cb=%v", err, cb)
		return "REPEAT_CALLBACK"
	}
	if cb.Status == "SettlementCompleted" {
		payOut.Status = model.PAY_OUT_SUCCESS
		payOut.FinishTime = time.Now().Unix()
	} else if cb.Status == "FAILED" {
		payOut.Status = model.PAY_OUT_FAILD
		payOut.FinishTime = time.Now().Unix()
	}
	rowCount, err := engine.Where("id=?", payOut.Id).Update(payOut)
	if err != nil {
		logger.Error("PayOutService_FidyPayOutCallback_Update_DBError | err=%v", err, cb)
		return "DB_ERROR"
	}
	logger.Debug("FidyPayOutCallback_Info | payOut=%+v | rowCount=%v", payOut, rowCount)
	if rowCount > 0 {
		//发起回调
		go po.SendPayOutCallback(payOut)
	}
	return ""
}

/**
 * razorpay提现回调
 * param: *model.CFPayOutCallBack cb
 * return: string
 */
func (po *PayOutService) RazorPayOutCallback(cb *model.PayoutCallBackWebHook) (resKey string) {
	//签名校验
	//if !cashfree.GetCashFree().OutSignature(cb) {
	//	logger.Error("CashFreePayOutCallback_InSignature_Error | cb=%+v", cb)
	//	return "SIGN_ERROR"
	//}
	//更新状态
	payOut := new(model.PayOut)
	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("PayOutService_RazorPayOutCallback_DBError | err=%v | cb=%+v", err, cb)
		return "DB_ERROR"
	}
	exist, err := engine.Where("order_id=?", cb.PayLoad.Payout.Entity.ReferenceId).Get(payOut)
	if err != nil {
		logger.Error("PayOutService_RazorPayOutCallback_Query_DBError | err=%v | cb=%v", err, cb)
		return "DB_ERROR"
	}
	if !exist {
		logger.Error("PayOutService_RazorPayOutCallback_NotExist | err=%v | cb=%v", err, cb)
		return "DB_ERROR"
	}
	//已经修改不再变化
	if payOut.Status != model.PAY_OUT_ING {
		logger.Error("PayOutService_RazorPayOutCallback_Repeat | err=%v | cb=%v", err, cb)
		return "REPEAT_CALLBACK"
	}
	if cb.Event == model.PayoutProcessed {
		payOut.Status = model.PAY_OUT_SUCCESS
		payOut.FinishTime = time.Now().Unix()
	} else if cb.Event == model.PayoutFailed || cb.Event == model.PayoutRejected || cb.Event == model.PayoutReversed {
		payOut.Status = model.PAY_OUT_FAILD
		payOut.ThirdDesc = cb.PayLoad.Payout.Entity.FailureReason
		payOut.FinishTime = time.Now().Unix()
	} else {
		return
	}
	rowCount, err := engine.Where("id=?", payOut.Id).Update(payOut)
	if err != nil {
		logger.Error("PayOutService_RazorPayOutCallback_Update_DBError | err=%v", err, cb)
		return "DB_ERROR"
	}
	logger.Debug("PayOutService_RazorPayOutCallback_Info | payOut=%+v | rowCount=%v", payOut, rowCount)
	if rowCount > 0 {
		//发起回调
		go po.SendPayOutCallback(payOut)
	}
	return ""
}

/**
 * easebuzz提现回调
 * param: *model.CFPayOutCallBack cb
 * return: string
 */
func (po *PayOutService) EaseBuzzPayOutCallback(cb *model.EaseBuzzPayoutCallback) (resKey string) {
	//签名校验
	//if !cashfree.GetCashFree().OutSignature(cb) {
	//	logger.Error("CashFreePayOutCallback_InSignature_Error | cb=%+v", cb)
	//	return "SIGN_ERROR"
	//}
	//更新状态
	payOut := new(model.PayOut)
	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("PayOutService_EaseBuzzPayOutCallback_DBError | err=%v | cb=%+v", err, cb)
		return "DB_ERROR"
	}
	exist, err := engine.Where("order_id=?", cb.Data.UniqueRequestNumber).Get(payOut)
	if err != nil {
		logger.Error("PayOutService_EaseBuzzPayOutCallback_Query_DBError | err=%v | cb=%v", err, cb)
		return "DB_ERROR"
	}
	if !exist {
		logger.Error("PayOutService_EaseBuzzPayOutCallback_NotExist | err=%v | cb=%v", err, cb)
		return "DB_ERROR"
	}
	//已经修改不再变化
	if payOut.Status != model.PAY_OUT_ING {
		logger.Error("PayOutService_EaseBuzzPayOutCallback_Repeat | err=%v | cb=%v", err, cb)
		return "REPEAT_CALLBACK"
	}
	if cb.Event == "TRANSFER_STATUS_UPDATE" {
		if cb.Data.Status == "success" {
			payOut.Status = model.PAY_OUT_SUCCESS
			payOut.PaymentId = cb.Data.UniqueTransactionReference
			payOut.FinishTime = time.Now().Unix()
		} else if cb.Data.Status == "failure" || cb.Data.Status == "rejected" {
			payOut.Status = model.PAY_OUT_FAILD
			payOut.ThirdDesc = cb.Data.FailureReason
			payOut.FinishTime = time.Now().Unix()
		}
	} else {
		return
	}
	rowCount, err := engine.Where("id=?", payOut.Id).Update(payOut)
	if err != nil {
		logger.Error("PayOutService_EaseBuzzPayOutCallback_Update_DBError | err=%v", err, cb)
		return "DB_ERROR"
	}
	logger.Debug("PayOutService_EaseBuzzPayOutCallback_Info | payOut=%+v | rowCount=%v", payOut, rowCount)
	if rowCount > 0 {
		//发起回调
		go po.SendPayOutCallback(payOut)
	}
	return ""
}

/**
 * paytm提现回调
 * param: *model.CFPayOutCallBack cb
 * return: string
 */
func (po *PayOutService) PayTmPayOutCallback(cb *model.CFPayOutCallBack) (resKey string) {
	//签名校验
	if !cashfree.GetCashFree().OutSignature(cb) {
		logger.Error("CashFreePayOutCallback_InSignature_Error | cb=%+v", cb)
		return "SIGN_ERROR"
	}
	//更新状态
	payOut := new(model.PayOut)
	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("PayOutService_CashFreePayOutCallback_DBError | err=%v | cb=%+v", err, cb)
		return "DB_ERROR"
	}
	exist, err := engine.Where("order_id=?", cb.TransferId).Get(payOut)
	if err != nil {
		logger.Error("PayOutService_CashFreePayOutCallback_Query_DBError | err=%v | cb=%v", err, cb)
		return "DB_ERROR"
	}
	if !exist {
		logger.Error("PayOutService_CashFreePayOutCallback_NotExist | err=%v | cb=%v", err, cb)
		return "DB_ERROR"
	}
	//已经修改不再变化
	if payOut.Status != model.PAY_OUT_ING {
		logger.Error("PayOutService_CashFreePayOutCallback_Repeat | err=%v | cb=%v", err, cb)
		return "REPEAT_CALLBACK"
	}
	if cb.Event == "TRANSFER_SUCCESS" {
		payOut.Status = model.PAY_OUT_SUCCESS
		payOut.FinishTime = time.Now().Unix()
	} else if cb.Event == "TRANSFER_FAILED" || cb.Event == "TRANSFER_REVERSED" {
		payOut.Status = model.PAY_OUT_FAILD
		payOut.ThirdDesc = cb.Reason
		payOut.FinishTime = time.Now().Unix()
	}
	rowCount, err := engine.Where("id=?", payOut.Id).Update(payOut)
	if err != nil {
		logger.Error("PayOutService_CashFreePayOutCallback_Update_DBError | err=%v", err, cb)
		return "DB_ERROR"
	}
	logger.Debug("CashFreePayOutCallback_Info | payOut=%+v | rowCount=%v", payOut, rowCount)
	if rowCount > 0 {
		//发起回调
		go po.SendPayOutCallback(payOut)
	}
	return ""
}

/**
 * payu提现回调
 * param: *model.CFPayOutCallBack cb
 * return: string
 */
func (po *PayOutService) PayuPayOutCallback(cb *model.PayUOutCallback) (resKey string) {
	//更新状态
	payOut := new(model.PayOut)
	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("PayOutService_PayuPayOutCallback_DBError | err=%v | cb=%+v", err, cb)
		return "DB_ERROR"
	}
	exist, err := engine.Where("order_id=?", cb.MerchantReferenceId).Get(payOut)
	if err != nil {
		logger.Error("PayOutService_PayuPayOutCallback_Query_DBError | err=%v | cb=%v", err, cb)
		return "DB_ERROR"
	}
	if !exist {
		logger.Error("PayOutService_PayuPayOutCallback_NotExist | err=%v | cb=%v", err, cb)
		return "DB_ERROR"
	}
	//已经修改不再变化
	if payOut.Status != model.PAY_OUT_ING {
		logger.Error("PayOutService_PayuPayOutCallback_Repeat | err=%v | cb=%v", err, cb)
		return "REPEAT_CALLBACK"
	}
	if cb.Event == model.PayUSuccess {
		payOut.Status = model.PAY_OUT_SUCCESS
		payOut.Utr = cb.BankReferenceId
		payOut.FinishTime = time.Now().Unix()
	} else if cb.Event == model.PayUFailed || cb.Event == model.PayUReserved || cb.Event == model.PayUProcessingFailed {
		payOut.Status = model.PAY_OUT_FAILD
		payOut.ThirdDesc = cb.Msg
		payOut.FinishTime = time.Now().Unix()
	} else {
		return
	}
	rowCount, err := engine.Where("id=?", payOut.Id).Update(payOut)
	if err != nil {
		logger.Error("PayOutService_PayuPayOutCallback_Update_DBError | err=%v", err, cb)
		return "DB_ERROR"
	}
	logger.Debug("PayOutService_PayuPayOutCallback_Info | payOut=%+v | rowCount=%v", payOut, rowCount)
	if rowCount > 0 {
		//发起回调
		go po.SendPayOutCallback(payOut)
	}
	return ""
}

/**
 * pagsmile提现回调
 * param: *model.PagSmilePayOutNotify cb
 * return: string
 */
func (po *PayOutService) PagSmilePayOutCallback(cb model.PagSmilePayOutNotify) (resKey string) {
	//更新状态
	payOut := new(model.PayOut)
	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("PayOutService_PagSmilePayOutCallback_DBError | err=%v | cb=%+v", err, cb)
		return "DB_ERROR"
	}
	exist, err := engine.Where("order_id=?", cb.CustomCode).Get(payOut)
	if err != nil {
		logger.Error("PayOutService_PagSmilePayOutCallback_Query_DBError | err=%v | cb=%v", err, cb)
		return "DB_ERROR"
	}
	if !exist {
		logger.Error("PayOutService_PagSmilePayOutCallback_NotExist | err=%v | cb=%v", err, cb)
		return "DB_ERROR"
	}

	//已经修改不再变化
	if payOut.Status != model.PAY_OUT_ING {
		logger.Error("PayOutService_PagSmilePayOutCallback_Repeat | err=%v | cb=%v", err, cb)
		return "REPEAT_CALLBACK"
	}
	if cb.Status == model.PagSmilePayStatusPAID {
		payOut.Status = model.PAY_OUT_SUCCESS
		payOut.FinishTime = time.Now().Unix()
	} else {
		payOut.Status = model.PAY_OUT_FAILD
		payOut.ThirdDesc = cb.Msg
		payOut.FinishTime = time.Now().Unix()
	}

	rowCount, err := engine.Where("id=?", payOut.Id).Update(payOut)
	if err != nil {
		logger.Error("PayOutService_PagSmilePayOutCallback_Update_DBError | err=%v", err, cb)
		return "DB_ERROR"
	}
	logger.Debug("PayOutService_PagSmilePayOutCallback_Info | payOut=%+v | rowCount=%v", payOut, rowCount)
	if rowCount > 0 {
		//发起回调
		go po.SendPayOutCallback(payOut)
	}
	return ""
}

/**
 * globpay提现回调
 * param: *model.PagSmilePayOutNotify cb
 * return: string
 */
func (po *PayOutService) GlobPayOutCallback(cb model.GlobPayNotify) (resKey string) {
	//更新状态
	payOut := new(model.PayOut)
	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("PayOutService_GlobPayOutCallback_DBError | err=%v | cb=%+v", err, cb)
		return "DB_ERROR"
	}
	exist, err := engine.Where("order_id=?", cb.MchOrderNo).Get(payOut)
	if err != nil {
		logger.Error("PayOutService_GlobPayOutCallback_Query_DBError | err=%v | cb=%v", err, cb)
		return "DB_ERROR"
	}
	if !exist {
		logger.Error("PayOutService_GlobPayOutCallback_NotExist | err=%v | cb=%v", err, cb)
		return "DB_ERROR"
	}

	//已经修改不再变化
	if payOut.Status != model.PAY_OUT_ING {
		logger.Error("PayOutService_GlobPayOutCallback_Repeat | err=%v | cb=%v", err, cb)
		return "REPEAT_CALLBACK"
	}
	if cb.Code == "1" {
		payOut.Status = model.PAY_OUT_SUCCESS
		payOut.FinishTime = time.Now().Unix()
	} else {
		payOut.Status = model.PAY_OUT_FAILD
		payOut.ThirdDesc = fmt.Sprintf("%v-%v", cb.Code, cb.Message)
		payOut.FinishTime = time.Now().Unix()
	}

	rowCount, err := engine.Where("id=?", payOut.Id).Update(payOut)
	if err != nil {
		logger.Error("PayOutService_GlobPayOutCallback_Update_DBError | err=%v", err, cb)
		return "DB_ERROR"
	}
	logger.Debug("PayOutService_GlobPayOutCallback_Info | payOut=%+v | rowCount=%v", payOut, rowCount)
	if rowCount > 0 {
		//发起回调
		go po.SendPayOutCallback(payOut)
	}
	return ""
}

/**
 * payflash提现回调
 * return: string
 */
func (po *PayOutService) PayFlashOutCallback(cb url.Values) (resKey string) {
	/*
		transaction_reference_number=
		&status=SUCCESS
		&error_message=
		&bank_reference_number=224915646732
		&merchant_reference_number=PFOUT16624581230409110b31
		&api_key=97549c3a-ba4e-44c3-8c0a-d29d848b056a
		&transfer_date=2022-09-06+15%3A25%3A25
		&transfer_amount=5.00
		&hash=5F134B945FFB5E9ED64308EB20BB831410D82434BAF4353ECEF1389706BF088AF25C6C66D98F05840F85C425E04908545EF63819EB7E3C795CF19DE23C34DB24
	*/
	status := cb.Get(payflash.FieldStatus)
	if status != payflash.DisbursementStatusSuccess &&
		status != payflash.DisbursementStatusFailed &&
		status != payflash.DisbursementStatusFailure &&
		status != payflash.DisbursementStatusReject {
		// 中间状态不做更新和通知
		logger.Warn("PayOutService_PayFlashOutCallback_Warn | unsupported status=%+v", status)
		return ""
	}

	orderId := cb.Get(payflash.FieldMerchantReferenceNumber)
	if len(orderId) < 1 {
		logger.Error("PayOutService_PayFlashOutCallback_Error | invalid orderId")
		return "empty merchant_reference_number[orderId]"
	}

	//更新状态
	payOut := new(model.PayOut)
	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("PayOutService_PayFlashOutCallback_DBError | err=%v | cb=%+v", err, cb)
		return "DB_ERROR"
	}
	exist, err := engine.Where("order_id=?", orderId).Get(payOut)
	if err != nil {
		logger.Error("PayOutService_PayFlashOutCallback_Query_DBError | err=%v | cb=%v", err, cb)
		return "DB_ERROR"
	}
	if !exist {
		logger.Error("PayOutService_PayFlashOutCallback_NotExist | err=%v | cb=%v", err, cb)
		return "DB_ERROR"
	}

	ins, err := PayAccountConfServe.GetSpecificPayInstance(model.PAYFLASH, payOut.PayAccountId)
	if err != nil {
		logger.Error("PayFlashPayOutCallback_Invalid PayAccountId=%v | error=%v ", payOut.PayAccountId, err)
		//c.JSON(http.StatusBadRequest, gin.H{"code": "400", "error": fmt.Sprintf("unsupported")})
		return "unsupported PayAccountId"
	}

	cbData := make(map[string]string)
	for k, _ := range cb {
		cbData[k] = cb.Get(k)
	}
	if !ins.OutSignature(cbData) {
		logger.Error("PayFlashPayOutCallback_Invalid Signature %v | %v ", payOut.PayAccountId, cb.Encode())
		return "invalid sign"
	}

	//已经修改不再变化
	if payOut.Status != model.PAY_OUT_ING {
		logger.Error("PayOutService_PayFlashOutCallback_Repeat | err=%v | cb=%v", err, cb)
		return "REPEAT_CALLBACK"
	}

	if status == payflash.DisbursementStatusSuccess {
		payOut.Status = model.PAY_OUT_SUCCESS
		payOut.FinishTime = time.Now().Unix()
	} else if status == payflash.DisbursementStatusFailed ||
		status == payflash.DisbursementStatusFailure || status == payflash.DisbursementStatusReject {
		payOut.Status = model.PAY_OUT_FAILD
		payOut.ThirdDesc = fmt.Sprintf("%v-%v", status, cb.Get(payflash.FieldErrorMessage))
		payOut.FinishTime = time.Now().Unix()
	}

	rowCount, err := engine.Where("id=?", payOut.Id).Update(payOut)
	if err != nil {
		logger.Error("PayOutService_PayFlashOutCallback_Update_DBError | err=%v", err, cb)
		return "DB_ERROR"
	}
	logger.Debug("PayOutService_PayFlashOutCallback_Info | payOut=%+v | rowCount=%v", payOut, rowCount)
	if rowCount > 0 {
		//发起回调
		go po.SendPayOutCallback(payOut)
	}
	return ""
}

func (po *PayOutService) PDKPayOutCallback(cb pdkpay.PayNotify) (resKey string) {
	status := cb.Data.TradeStatus
	if status != pdkpay.PayOutStatusSuccess &&
		status != pdkpay.PayOutStatusFail {
		// 中间状态不做更新和通知
		logger.Warn("PayOutService_PDKPayOutCallback_Warn | unsupported status=%+v", status)
		return ""
	}

	//更新状态
	payOut := new(model.PayOut)
	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("PayOutService_PDKPayOutCallback_DBError | err=%v | cb=%+v", err, cb)
		return "DB_ERROR"
	}
	exist, err := engine.Where("order_id=?", cb.Data.OrderSn).Get(payOut)
	if err != nil {
		logger.Error("PayOutService_PDKPayOutCallback_Query_DBError | err=%v | cb=%v", err, cb)
		return "DB_ERROR"
	}
	if !exist {
		logger.Error("PayOutService_PDKPayOutCallback_NotExist | err=%v | cb=%v", err, cb)
		return "DB_ERROR"
	}

	ins, err := PayAccountConfServe.GetSpecificPayInstance(model.PDKPAY, payOut.PayAccountId)
	if err != nil {
		logger.Error("PDKPayOutCallback_Invalid PayAccountId=%v | error=%v ", payOut.PayAccountId, err)
		//c.JSON(http.StatusBadRequest, gin.H{"code": "400", "error": fmt.Sprintf("unsupported")})
		return "unsupported PayAccountId"
	}

	cbData := cb.DataMap()
	if !ins.OutSignature(cbData) {
		logger.Error("PDKPayOutCallback_Invalid Signature %v | %v ", payOut.PayAccountId, cbData)
		return "invalid sign"
	}

	//已经修改不再变化
	if payOut.Status != model.PAY_OUT_ING {
		logger.Error("PayOutService_PDKPayOutCallback_Repeat | err=%v | cb=%v", err, cb)
		return "REPEAT_CALLBACK"
	}

	if status == pdkpay.PayOutStatusSuccess {
		payOut.Status = model.PAY_OUT_SUCCESS
		payOut.FinishTime = time.Now().Unix()
	} else if status == pdkpay.PayOutStatusFail {
		payOut.Status = model.PAY_OUT_FAILD
		payOut.ThirdCode = fmt.Sprintf("%v", status)
		payOut.ThirdDesc = fmt.Sprintf("%v", status)
		payOut.FinishTime = time.Now().Unix()
	}

	rowCount, err := engine.Where("id=?", payOut.Id).Update(payOut)
	if err != nil {
		logger.Error("PayOutService_PDKPayOutCallback_Update_DBError | err=%v", err, cb)
		return "DB_ERROR"
	}
	logger.Debug("PayOutService_PDKPayOutCallback_Info | payOut=%+v | rowCount=%v", payOut, rowCount)
	if rowCount > 0 {
		//发起回调
		go po.SendPayOutCallback(payOut)
	}
	return ""
}

func (po *PayOutService) ZWPayOutCallback(cb zwpay.PayOutNotify) (resKey string) {
	status := cb.Data.Status
	if status != zwpay.ZWPayOutSuccess && status != zwpay.ZWPayOutFailure && status != zwpay.ZWPayOutReverse {
		logger.Warn("PayEntryService_ZWPayoutCallback_Not Deal Status=%v | cb=%+v", status, cb)
		return ""
	}

	//更新状态
	payOut := new(model.PayOut)
	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("PayOutService_ZWPayOutCallback_DBError | err=%v | cb=%+v", err, cb)
		return "DB_ERROR"
	}
	exist, err := engine.Where("order_id=?", cb.Data.MerchantOrderNo).Get(payOut)
	if err != nil {
		logger.Error("PayOutService_ZWPayOutCallback_Query_DBError | err=%v | cb=%v", err, cb)
		return "DB_ERROR"
	}
	if !exist {
		logger.Error("PayOutService_ZWPayOutCallback_NotExist | err=%v | cb=%v", err, cb)
		return "DB_ERROR"
	}

	//已经修改不再变化
	if payOut.Status != model.PAY_OUT_ING {
		logger.Error("PayOutService_ZWPayOutCallback_Repeat | err=%v | cb=%v", err, cb)
		centerlog.Info(centerlog.MsgRepeatCallBack, map[string]interface{}{
			centerlog.FieldAppId:       payOut.AppId,
			centerlog.FieldAccountId:   payOut.PayAccountId,
			centerlog.FieldOrderId:     payOut.OrderId,
			centerlog.FieldAppOrderId:  payOut.AppOrderId,
			centerlog.FieldAmount:      payOut.Amount,
			centerlog.FieldOrderStatus: payOut.Status,
			centerlog.FieldThirdDesc:   payOut.ThirdDesc,
			centerlog.FieldCBStatus:    cb.Data.Status,
		})

		if payOut.Status == model.PAY_OUT_SUCCESS && status != zwpay.ZWPayOutSuccess ||
			payOut.Status == model.PAY_OUT_FAILD && status != zwpay.ZWPayOutFailure {
			return "REPEAT_CALLBACK_Different_status"
		}

		return ""
	}

	if status == zwpay.ZWPayOutSuccess {
		payOut.Status = model.PAY_OUT_SUCCESS
		payOut.FinishTime = time.Now().Unix()
	} else if status == zwpay.ZWPayOutFailure || status == zwpay.ZWPayOutReverse {
		payOut.Status = model.PAY_OUT_FAILD
		payOut.ThirdCode = status
		payOut.ThirdDesc = cb.Data.Message
		payOut.FinishTime = time.Now().Unix()
	}

	rowCount, err := engine.Where("id=?", payOut.Id).Update(payOut)
	if err != nil {
		logger.Error("PayOutService_ZWPayOutCallback_Update_DBError | err=%v", err, cb)
		return "DB_ERROR"
	}
	logger.Debug("PayOutService_ZWPayOutCallback_Info | payOut=%+v | rowCount=%v", payOut, rowCount)
	if rowCount > 0 {
		//发起回调
		go po.SendPayOutCallback(payOut)
	}
	return ""
}

func (po *PayOutService) FMPayOutCallback(cb fmpay.PayOutNotify) (resKey string) {
	if cb.OrderState != fmpay.PayOutStatusSuccess && cb.OrderState != fmpay.PayOutStatusFailure {
		logger.Warn("PayOutService_FMPayoutCallback_Not Deal Status=%v | cb=%+v", cb.OrderState, cb)
		return ""
	}

	//更新状态
	payOut := new(model.PayOut)
	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("PayOutService_FMPayOutCallback_DBError | err=%v | cb=%+v", err, cb)
		return "DB_ERROR"
	}
	exist, err := engine.Where("order_id=?", cb.MerchantOrderId).Get(payOut)
	if err != nil {
		logger.Error("PayOutService_FMPayOutCallback_Query_DBError | err=%v | cb=%v", err, cb)
		return "DB_ERROR"
	}
	if !exist {
		logger.Error("PayOutService_FMPayOutCallback_NotExist | err=%v | cb=%v", err, cb)
		return "DB_ERROR"
	}

	//已经修改不再变化
	if payOut.Status != model.PAY_OUT_ING {
		logger.Error("PayOutService_FMPayOutCallback_Repeat | err=%v | cb=%v", err, cb)
		centerlog.Info(centerlog.MsgRepeatCallBack, map[string]interface{}{
			centerlog.FieldAppId:       payOut.AppId,
			centerlog.FieldAccountId:   payOut.PayAccountId,
			centerlog.FieldOrderId:     payOut.OrderId,
			centerlog.FieldAppOrderId:  payOut.AppOrderId,
			centerlog.FieldAmount:      payOut.Amount,
			centerlog.FieldOrderStatus: payOut.Status,
			centerlog.FieldThirdDesc:   payOut.ThirdDesc,
			centerlog.FieldCBStatus:    cb.OrderState,
		})
		return "REPEAT_CALLBACK"
	}

	oldStatus := payOut.Status
	if cb.OrderState == fmpay.PayOutStatusSuccess {
		payOut.Status = model.PAY_OUT_SUCCESS
		payOut.FinishTime = time.Now().Unix()
	} else if cb.OrderState == fmpay.PayOutStatusFailure {
		payOut.Status = model.PAY_OUT_FAILD
		payOut.ThirdCode = cb.OrderState
		payOut.ThirdDesc = cb.OrderMsg
		payOut.FinishTime = time.Now().Unix()
	} else {
		return ""
	}

	rowCount, err := engine.Where("id=? and status =?", payOut.Id, oldStatus).Update(payOut)
	if err != nil {
		logger.Error("PayOutService_FMPayOutCallback_Update_DBError | err=%v", err, cb)
		return "DB_ERROR"
	}
	logger.Debug("PayOutService_FMPayOutCallback_Info | payOut=%+v | rowCount=%v", payOut, rowCount)
	if rowCount > 0 {
		//发起回调
		go po.SendPayOutCallback(payOut)
	}
	return ""
}

func (po *PayOutService) LoogPayOutCallback(cb url.Values) (resKey string) {
	//更新状态
	payOut := new(model.PayOut)
	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("PayOutService_LoogPayOutCallback_DBError | err=%v | cb=%+v", err, cb)
		return "DB_ERROR"
	}
	orderId := cb.Get(loogpay.FieldOrderNo)
	exist, err := engine.Where("order_id=?", orderId).Get(payOut)
	if err != nil {
		logger.Error("PayOutService_LoogPayOutCallback_Query_DBError | err=%v | cb=%v", err, cb)
		return "DB_ERROR"
	}
	if !exist {
		logger.Error("PayOutService_LoogPayOutCallback_NotExist | err=%v | cb=%v", err, cb)
		return "DB_ERROR"
	}

	ins, err := PayAccountConfServe.GetSpecificPayInstance(model.LOOGPAY, payOut.PayAccountId)
	if err != nil {
		logger.Error("LoogPayOutCallback_Invalid PayAccountId=%v | error=%v ", payOut.PayAccountId, err)
		return "unsupported PayAccountId"
	}

	if !ins.OutSignature(cb) {
		logger.Error("LoogPayOutCallback_Invalid Signature %v | %v ", payOut.PayAccountId, cb)
		return "invalid sign"
	}

	status := cb.Get(loogpay.FieldTradeState)
	if payOut.Status == model.PAY_OUT_ING { // 待处理
		if status == loogpay.Success {
			payOut.Status = model.PAY_OUT_SUCCESS
			payOut.FinishTime = time.Now().Unix()
		} else if status == loogpay.Failed || status == loogpay.Cancel {
			payOut.Status = model.PAY_OUT_FAILD
			payOut.ThirdCode = fmt.Sprintf("%v", status)
			payOut.ThirdDesc = fmt.Sprintf("%v-%v", cb.Get(loogpay.FieldRespDesc), cb.Get(loogpay.FieldBankMsg))
			payOut.FinishTime = time.Now().Unix()
		} else {
			return ""
		}
	} else if payOut.Status == model.PAY_OUT_SUCCESS && (status == loogpay.Failed || status == loogpay.Cancel) { // 先成功 后失败，退款
		payOut.Status = model.PAY_OUT_REVERSED
		err = AccountServ.ReversedPayout(payOut)
		if err != nil {
			logger.Error("PayOutService_LoogPayOutCallback_Reversed_err | err=%v | cb=%v", err, cb)
			return fmt.Sprintf("revert[%v] error", orderId)
		}

		payOut.ThirdCode = fmt.Sprintf("%v", status)
		payOut.ThirdDesc = fmt.Sprintf("%v-%v", cb.Get(loogpay.FieldRespDesc), cb.Get(loogpay.FieldBankMsg))
	} else {
		logger.Error("PayOutService_LoogPayOutCallback_Repeat | err=%v | cb=%v", err, cb)
		return ""
	}

	rowCount, err := engine.Where("id=?", payOut.Id).Update(payOut)
	if err != nil {
		logger.Error("PayOutService_LoogPayOutCallback_Update_DBError | err=%v", err, cb)
		return "DB_ERROR"
	}
	logger.Debug("PayOutService_LoogPayOutCallback_Info | payOut=%+v | rowCount=%v", payOut, rowCount)
	if rowCount > 0 {
		//发起回调
		go po.SendPayOutCallback(payOut)
	}
	return ""
}

func (po *PayOutService) XPayOutCallback(cb url.Values) (resKey string) {
	//更新状态
	payOut := new(model.PayOut)
	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("PayOutService_XPayOutCallback_DBError | err=%v | cb=%+v", err, cb)
		return "DB_ERROR"
	}
	orderId := cb.Get(xpay.FieldMchNo)
	exist, err := engine.Where("order_id=?", orderId).Get(payOut)
	if err != nil {
		logger.Error("PayOutService_XPayOutCallback_Query_DBError | err=%v | cb=%v", err, cb)
		return "DB_ERROR"
	}
	if !exist {
		logger.Error("PayOutService_XPayOutCallback_NotExist | err=%v | cb=%v", err, cb)
		return "DB_ERROR"
	}

	ins, err := PayAccountConfServe.GetSpecificPayInstance(model.XPAY, payOut.PayAccountId)
	if err != nil {
		logger.Error("XPayOutCallback_Invalid PayAccountId=%v | error=%v ", payOut.PayAccountId, err)
		return "unsupported PayAccountId"
	}

	if !ins.OutSignature(cb) {
		logger.Error("XPayOutCallback_Invalid Signature %v | %v ", payOut.PayAccountId, cb)
		return "invalid sign"
	}

	//已经修改不再变化
	if payOut.Status != model.PAY_OUT_ING {
		logger.Error("PayOutService_XPayOutCallback_Repeat | err=%v | cb=%v", err, cb)
		return ""
	}

	status := cb.Get(xpay.FieldState)
	if status == xpay.PaySuccessStr {
		payOut.Status = model.PAY_OUT_SUCCESS
		payOut.FinishTime = time.Now().Unix()
	} else if status == xpay.PayFailedStr {
		payOut.Status = model.PAY_OUT_FAILD
		payOut.ThirdCode = cb.Get(xpay.FieldErrCode)
		payOut.ThirdDesc = cb.Get(xpay.FieldErrMsg)
		payOut.FinishTime = time.Now().Unix()
	} else {
		return ""
	}

	rowCount, err := engine.Where("id=?", payOut.Id).Update(payOut)
	if err != nil {
		logger.Error("PayOutService_XPayOutCallback_Update_DBError | err=%v", err, cb)
		return "DB_ERROR"
	}
	logger.Debug("PayOutService_XPayOutCallback_Info | payOut=%+v | rowCount=%v", payOut, rowCount)
	if rowCount > 0 {
		//发起回调
		go po.SendPayOutCallback(payOut)
	}
	return ""
}

func (po *PayOutService) UpPayOutCallback(cb uppay.PayoutNotify) (resKey string) {
	//更新状态
	payOut := new(model.PayOut)
	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("PayOutService_UpPayOutCallback_DBError | err=%v | cb=%+v", err, cb)
		return "DB_ERROR"
	}
	orderId := cb.Data.MerchantTradeId
	exist, err := engine.Where("order_id=?", orderId).Get(payOut)
	if err != nil {
		logger.Error("PayOutService_UpPayOutCallback_Query_DBError | err=%v | cb=%v", err, cb)
		return "DB_ERROR"
	}
	if !exist {
		logger.Error("PayOutService_UpPayOutCallback_NotExist | err=%v | cb=%v", err, cb)
		return "DB_ERROR"
	}

	if payOut.PaymentId != cb.Data.TradeId {
		logger.Error("PayOutService_UpPayOutCallback_CheckPaymentId Fail| %v | %v |", payOut.PaymentId, cb)
		//return "check payment id error"
	}

	//ins, err := PayAccountConfServe.GetSpecificPayInstance(model.UPPAY, payOut.PayAccountId)
	//if err != nil {
	//	logger.Error("UpPayOutCallback_Invalid PayAccountId=%v | error=%v ", payOut.PayAccountId, err)
	//	return "unsupported PayAccountId"
	//}
	//
	//if !ins.OutSignature(cb) {
	//	logger.Error("UpPayOutCallback_Invalid Signature %v | %v ", payOut.PayAccountId, cb)
	//	return "invalid sign"
	//}

	//已经修改不再变化
	if payOut.Status == model.PAY_OUT_FAILD {
		logger.Error("PayOutService_UpPayOutCallback_Repeat | err=%v | cb=%v", err, cb)
		return ""
	}

	status := cb.Data.Status
	if payOut.Status == model.PAY_OUT_SUCCESS && status == uppay.PayoutReversed { // 退款
		payOut.Status = model.PAY_OUT_REVERSED

		err = AccountServ.ReversedPayout(payOut)
		if err != nil {
			logger.Error("PayOutService_OnionPayOutCallback_Reversed_err | err=%v | cb=%v", err, cb)
		}

	} else if payOut.Status == model.PAY_OUT_ING {
		if status == uppay.PayoutSuccess {
			payOut.Status = model.PAY_OUT_SUCCESS
			payOut.FinishTime = time.Now().Unix()
		} else if status == uppay.PayoutFail || status == uppay.PayoutDeclined || status == uppay.PayoutError {
			payOut.Status = model.PAY_OUT_FAILD
			payOut.ThirdCode = cb.Code
			payOut.ThirdDesc = cb.Msg
			payOut.FinishTime = time.Now().Unix()
		} else if status == uppay.PayoutReversed {
			payOut.Status = model.PAY_OUT_FAILD
			payOut.ThirdCode = cb.Code
		} else {
			return ""
		}
	} else {
		logger.Error("PayOutService_UpPayOutCallback_Repeat Status | err=%v | cb=%v", err, cb)
		return ""
	}

	rowCount, err := engine.Where("id=?", payOut.Id).Update(payOut)
	if err != nil {
		logger.Error("PayOutService_UpPayOutCallback_Update_DBError | err=%v", err, cb)
		return "DB_ERROR"
	}
	logger.Debug("PayOutService_UpPayOutCallback_Info | payOut=%+v | rowCount=%v", payOut, rowCount)
	if rowCount > 0 {
		//发起回调
		go po.SendPayOutCallback(payOut)
	}
	return ""
}

/**
 * 提现通知业务方
 */
func (po *PayOutService) SendPayOutCallback(payOut *model.PayOut) {
	if payOut.AppId == "funzone_pay" {
		return
	}
	if payOut.Uid != 0 {
		//平台用户
		err := AccountServ.CallBackPayOut(payOut)
		if err != nil {
			logger.Error("PayOutService_CallBackPayOut_Err | err=%v | data=%v", err, payOut)
			return
		}
		if payOut.Status != model.PAY_OUT_ING {
			centerlog.Info(centerlog.MsgPayOutCb, map[string]interface{}{
				centerlog.FieldAppId:          payOut.AppId,
				centerlog.FieldAccountId:      payOut.PayAccountId,
				centerlog.FieldOrderType:      payOut.PayType,
				centerlog.FieldOrderId:        payOut.OrderId,
				centerlog.FieldAppOrderId:     payOut.AppOrderId,
				centerlog.FieldPaymentOrderID: payOut.PaymentId,
				centerlog.FieldUTR:            payOut.Utr,
				centerlog.FieldAmount:         payOut.Amount,
				centerlog.FieldOrderStatus:    payOut.Status,
				centerlog.FieldThirdDesc:      payOut.ThirdDesc,
			})
		}

		po.SendPlatFormCallback(payOut)
		//清除缓存
		go func() {
			po.DelPayoutCache(payOut.Uid, payOut.OrderId)
			po.DelPayoutCache(payOut.Uid, payOut.AppOrderId)
		}()
		return
	}
	//engine, err := dao.GetMysql()
	//if err != nil {
	//	logger.Error("PayOutService_SendPayOutCallbackk_DBError | err=%v", err)
	//}
	//if payOut.AppId == "funzone_pay" {
	//	if payOut.Status == model.PAY_OUT_SUCCESS || payOut.Status == model.PAY_OUT_FAILD {
	//		_, _ = engine.Where("id=?", payOut.UserId).Update(&model.UserWithdrawFlow{
	//			Status: int(payOut.Status),
	//		})
	//	}
	//	//内部转账的直接返回
	//	return
	//}
	//url := viper.GetString(fmt.Sprintf("callback.%v.payout_url", payOut.AppId))
	////fmt.Println(fmt.Sprintf("callback.%v.order_url", payEntry.AppId))
	////发起回调
	//callbackData := &model.PayOutCallbackData{
	//	PayChannel: payOut.PayChannel,
	//	AppOrderId: payOut.AppOrderId,
	//	OrderId:    payOut.OrderId,
	//	PaymentId:  payOut.PaymentId,
	//	Status:     payOut.Status,
	//	Amount:     payOut.Amount,
	//	UserId:     payOut.UserId,
	//	Phone:      payOut.Phone,
	//	Email:      payOut.Email,
	//}
	//header := map[string][]string{
	//	"Content-Type": {"application/json"},
	//}
	//var httpStatus string
	//var response string
	//i := 1
	//idleDuration := 20 * time.Second
	//idleDelay := time.NewTimer(idleDuration)
	//defer idleDelay.Stop()
	////尝试回调3次
	//for ; i < 4; i++ {
	//	res, status, err := utils.HttpRequest(url, "POST", "JSON", callbackData, header)
	//	if err != nil {
	//		logger.Error("PayOutService_SendPayOutCallback_Error | err=%v | data=%+v", err, payOut)
	//	}
	//	response = string(res)
	//	httpStatus = fmt.Sprint(status)
	//	//状态码判断
	//	if status == http.StatusOK {
	//		break
	//	}
	//	select {
	//	case <-idleDelay.C:
	//		logger.Debug("PayOutService_SendPayOutCallback_time_%v | cb=%v", i, callbackData)
	//	}
	//	idleDelay.Reset(idleDuration)
	//}
	//cbList := new(model.PayOutCallbackList)
	//cbList.PayChannel = payOut.PayChannel
	//cbList.AppId = payOut.AppId
	//cbList.OrderId = payOut.OrderId
	//cbList.Status = payOut.Status
	//cbList.ReturnCode = httpStatus
	//cbList.Response = response
	//cbList.Times = i
	//cbList.AppOrderId = payOut.AppOrderId
	//_, err = engine.Insert(cbList)
	//if err != nil {
	//	logger.Error("PayOutService_SendPayOutCallback_InsertDBError | err=%v | cb=%+v", err, cbList)
	//}
	//return
}

func (po *PayOutService) SendPlatFormCallback(payOut *model.PayOut) {
	settings, ok := GetAccountDeveloperSettingsFromCache(payOut.AppId)
	if !ok {
		logger.Error("SendPlatFormCallback_Err_DevelopSettings_Err | data=%v", payOut)
		return
	}
	url := settings.PayoutCallbackUrl
	//fmt.Println(fmt.Sprintf("callback.%v.order_url", payEntry.AppId))
	//发起回调
	callbackData := &model.PayOutCallbackData{
		AppOrderId: payOut.AppOrderId,
		OrderId:    payOut.OrderId,
		Status:     payOut.Status,
		Amount:     payOut.Amount,
	}
	jsData, _ := json.Marshal(callbackData)
	sk, _ := utils.AesDecryptString(settings.SecretEncrypt)
	Signature := utils.ComputeHmacSha256(string(jsData), sk)
	header := map[string][]string{
		"Content-Type": {"application/json"},
		"Signature":    {Signature},
		Target:         {url},
		Safe:           {"1"},
	}
	logger.Debug("PayOutService_SendPlatFormCallback_Info | data=%v | secret=%v | Signature=%v", string(jsData), sk, Signature)
	var httpStatus string
	var response string
	i := 1
	idleDuration := 20 * time.Second
	idleDelay := time.NewTimer(idleDuration)
	defer idleDelay.Stop()
	//尝试回调3次
	for ; i < 4; i++ {
		res, status, err := utils.HttpRequest(NotifyHost, "POST", "JSON", callbackData, header)
		if err != nil {
			logger.Error("PayOutService_SendPlatFormCallback_Error | err=%v | data=%+v", err, payOut)
		}
		response = string(res)
		httpStatus = fmt.Sprint(status)
		//状态码判断
		if status == http.StatusOK {
			break
		}
		select {
		case <-idleDelay.C:
			logger.Debug("PayOutService_SendPlatFormCallback_time_%v | cb=%v", i, callbackData)
		}
		idleDelay.Reset(idleDuration)
	}
	cbList := new(model.PayOutCallbackList)
	cbList.PayChannel = payOut.PayChannel
	cbList.AppId = payOut.AppId
	cbList.OrderId = payOut.OrderId
	cbList.Status = payOut.Status
	cbList.ReturnCode = httpStatus
	cbList.Response = response
	cbList.Times = i
	cbList.AppOrderId = payOut.AppOrderId
	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("PayOutService_SendPlatFormCallback_DBError | err=%v | cb=%+v", err, cbList)
	}
	_, err = engine.Insert(cbList)
	if err != nil {
		logger.Error("PayOutService_SendPlatFormCallback_InsertDBError | err=%v | cb=%+v", err, cbList)
	}
	return
}

func (po *PayOutService) RetryPlatFormCallback(uid int64, orderId string) error {
	go func() {
		engine, err := dao.GetMysql()
		if err != nil {
			logger.Error("PayOutService_RetryPlatFormCallback_DBError | err=%v", err)
			return
		}
		payOut := new(model.PayOut)
		exist, err := engine.Where("uid=? AND order_id=?", uid, orderId).Get(payOut)
		if err != nil {
			logger.Error("PayOutService_RetryPlatFormCallback_Query_DBError | err=%v | orderId=%v", err, orderId)
			return
		}
		if !exist {
			logger.Error("PayOutService_RetryPlatFormCallback_Query_DBError | err=NotExist | orderId=%v", orderId)
			return
		}

		po.SendPlatFormCallback(payOut)
	}()
	return nil
}

func (po *PayOutService) GetPayoutByOrderId(uid int64, orderId string, appOrderId string) (*model.PlatPayoutData, error) {
	var (
		redisKey = fmt.Sprintf("FZP-%v-%v", uid, orderId)
		data     = new(model.PlatPayoutData)
	)
	if orderId == "" {
		redisKey = fmt.Sprintf("FZP-%v-%v", uid, appOrderId)
	}
	cacheData := dao.RedisGet(redisKey)
	if cacheData == "" {
		//查询数据库
		engine, err := dao.GetMysql()
		if err != nil {
			logger.Error("PayOutService_RetryPlatFormCallback_EngineError | err=%v", err)
			return nil, err
		}
		payOut := new(model.PayOut)
		session := engine.Where("uid=?", uid)
		if orderId != "" {
			session.Where("order_id=?", orderId)
		} else if appOrderId != "" {
			session.Where("app_order_id=?", appOrderId)
		}
		exist, err := session.Get(payOut)
		if err != nil {
			logger.Error("PayOutService_RetryPlatFormCallback_DBError | err=%v", err)
			return nil, err
		}
		if !exist {
			return nil, errors.New("OrderIdNotExist")
		}
		data = &model.PlatPayoutData{
			AppOrderId: payOut.AppOrderId,
			OrderId:    payOut.OrderId,
			Amount:     payOut.Amount,
			Status:     payOut.Status,
		}
		//写入缓存
		jsonByte, _ := json.Marshal(data)
		dao.RedisSet(redisKey, string(jsonByte), 120)
	} else {
		err := json.Unmarshal([]byte(cacheData), data)
		if err != nil {
			return nil, err
		}
	}
	return data, nil
}

func (po *PayOutService) DelPayoutCache(uid int64, orderId string) {
	redisKey := fmt.Sprintf("FZP-%v-%v", uid, orderId)
	dao.RedisDel(redisKey)
}

// 释放由于出款订单失败，没有成功释放的冻结金额

func (po *PayOutService) ReleaseFrozenTransaction(dataId int64) bool {
	payOut := new(model.PayOut)
	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("ReleaseFrozenTransaction_DBError | err=%v | id=%+v", err, dataId)
		return false
	}
	exist, err := engine.Where("id=?", dataId).Get(payOut)
	if err != nil {
		logger.Error("ReleaseFrozenTransaction_QueryError | err=%v | id=%v", err, dataId)
		return false
	}
	if !exist {
		logger.Error("ReleaseFrozenTransaction_NotExist | err=%v | id=%v", err, dataId)
		return false
	}

	if payOut.Status != model.PAY_OUT_FAILD {
		logger.Error("ReleaseFrozenTransaction No Need Release payout [%v-->%v] ", payOut.Id, payOut.Status)
		return true
	}

	transaction := new(model.AccountFrozenTransactions)
	exist, err = engine.Where("data_id=?", dataId).Get(transaction)
	if err != nil {
		logger.Error("ReleaseFrozenTransaction_QueryError | err=%v | id=%v", err, dataId)
		return false
	}

	if !exist {
		logger.Error("ReleaseFrozenTransaction_NotExist | err=%v | id=%v", err, dataId)
		return false
	}

	// 冻结
	if transaction.Status != model.FrozenDefault {
		logger.Error("ReleaseFrozenTransaction No Need Release Transaction[%v-->%v] ", dataId, transaction.Status)
		return true
	}

	err = AccountServ.CallBackPayOut(payOut)
	if err != nil {
		logger.Error("ReleaseFrozenTransaction_Fail | id=%v | err=%v", dataId, err)
		return false
	}

	return true
}
