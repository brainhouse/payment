package service

import "sync"

var limit sync.Map

func CheckSpeed(key string, nowUnix, internal int64) bool {
	value, ok := limit.Load(key)
	if !ok {
		limit.Store(key, nowUnix)
		return true
	}

	valueInt, _ := value.(int64)
	// 
	if (nowUnix - valueInt) > internal {
		limit.Store(key, nowUnix)
		return true
	}

	return false
}