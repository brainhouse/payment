package service

import (
	"fmt"
	"funzone_pay/model"
	"github.com/wonderivan/logger"
	"time"
)

var last int64 = 0
var accountSettings = make(map[string]model.AccountDeveloperSettings)
var paymentSettings = make(map[int64]model.AccountPaymentSettings)
var channelSettings = make(map[string]model.Channel)

func load() error {
	var err error = nil
	defer func() {
		if err != nil {
			logger.Debug("load cache error--->%v", err)
		} else {
			logger.Debug("load cache success")
		}
	}()

	now := time.Now().Unix()
	if now - last < 60 {
		err = fmt.Errorf("update so frequency")
		return err
	}

	last = now
	dData, err := PayAccountConfServe.GetAllDeveloperSettings()
	if err != nil {
		return err
	}

	sdData := make(map[string]model.AccountDeveloperSettings)
	for i, item := range dData {
		sdData[item.AppId] = dData[i]
	}

	//
	pData, err := PayAccountConfServe.GetAllUserPaySettings()
	if err != nil {
		return err
	}

	spData := make(map[int64]model.AccountPaymentSettings)
	for i, item := range pData {
		spData[item.Uid] = pData[i]
	}

	cData, err := PayAccountConfServe.GetAllChannelSettings()
	if err != nil {
		return err
	}

	logger.Debug("load channel success %v", len(cData))
	scData := make(map[string]model.Channel)
	for i, item := range cData {
		if item.Status != 0 {
			continue
		}
		scData[item.PayAccountID] = cData[i]
	}

	//
	accountSettings = sdData
	paymentSettings = spData
	channelSettings = scData

	return nil
}

func Init() error {
	go func() {
		tic := time.NewTicker(time.Minute * 5)
		for range tic.C {
			load()
		}
	}()
	return load()
}

func Reload() error {
	return load()
}

func GetAccountDeveloperSettingsFromCache(appId string) (model.AccountDeveloperSettings, bool) {
	d, ok := accountSettings[appId]
	return d, ok
}

func GetAccountPaymentSettingsFromCache(uid int64) (model.AccountPaymentSettings, bool) {
	d, ok := paymentSettings[uid]
	return d, ok
}

func GetChannelSettingsFromCache(payAccountId string) (model.Channel, bool) {
	d, ok := channelSettings[payAccountId]
	return d, ok
}
