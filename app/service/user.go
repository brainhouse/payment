package service

import (
	"funzone_pay/dao"
	"funzone_pay/model"
	"github.com/wonderivan/logger"
)

var UserServ *UserService

func UserServiceInit() {
	once.Do(func() {
		UserServ = new(UserService)
	})
}

type UserService struct{}

func (us *UserService) GetUserInfo(uid int64) *model.User {
	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("UserService_GetUserInfo_EngineError | err=%v | uid=%v", err, uid)
		return nil
	}
	user := new(model.User)
	_, err = engine.Where("id=?", uid).Get(user)
	if err != nil {
		logger.Error("UserService_GetUserInfo_GetError | err=%v | uid=%v", err, uid)
		return nil
	}
	return user
}
