package service

import (
	"errors"
	"fmt"
	"funzone_pay/app/validator"
	"funzone_pay/channel"
	"funzone_pay/dao"
	"funzone_pay/errdef"
	"funzone_pay/model"
	"github.com/spf13/viper"
	"github.com/wonderivan/logger"
	"sync"
)

var (
	PaymentConfig       sync.Map
	once                sync.Once
	PayAccountConfServe *PayAccountConfService
)

var appAccountPayment = make(map[string]AppAccountPaymentConfig)

type AppAccountPaymentConfig struct {
	PayInMin     float64 // 最小支付金额
	PayInMinFee  float64 // 最小费率
	PayOutMin    float64
	PayOutMinFee float64
}

func genAppAccountPaymentConfig(conf map[string]interface{}) (c AppAccountPaymentConfig, ok bool) {
	if conf["PAY_IN_MIN"] == nil {
		return c, false
	}
	c.PayInMin = conf["PAY_IN_MIN"].(float64)

	if conf["PAY_IN_MIN_FEE"] == nil {
		return c, false
	}
	c.PayInMinFee = conf["PAY_IN_MIN_FEE"].(float64)

	if conf["PAY_OUT_MIN"] == nil {
		return c, false
	}
	c.PayOutMin = conf["PAY_OUT_MIN"].(float64)

	if conf["PAY_OUT_MIN_FEE"] == nil {
		return c, false
	}
	c.PayOutMinFee = conf["PAY_OUT_MIN_FEE"].(float64)

	return c, true
}

func PayAccountInit() {
	once.Do(func() {
		PayAccountConfServe = new(PayAccountConfService)
	})
	PayAccountConfServe.SetConfig()
}

type PayAccountConfService struct{}

/*
*
支付账号配置
*/
func (as *PayAccountConfService) SetConfig() {
	confList := viper.Get("pay_channel_account").([]interface{})
	for _, v := range confList {
		payData := v.(map[string]interface{})
		accountID, ok := payData["PAY_ACCOUNT_ID"].(string)
		fmt.Printf("%v---%v--%v--%v\n", ok, accountID, payData["PAY_ACCOUNT_ID"], payData)
		if !ok {
			continue
		}

		PaymentConfig.Store(accountID, payData)

		c, ok := genAppAccountPaymentConfig(payData)
		if ok {
			appAccountPayment[accountID] = c
		}
	}
}

/*
*
支付账号配置 - 合并数据库里面国家码和本地配置文件内容
*/
const countryCodeField = "COUNTRY_CODE"

func (as *PayAccountConfService) GetLocalPaymentConfigByAccountID(accountId string) (ret map[string]interface{}, err error) {
	channelDBConf, ok := GetChannelSettingsFromCache(accountId)
	if !ok {
		logger.Error("GetChannelSetting_Err | Channel[%v] not exist!", accountId)
		err = fmt.Errorf("channel setting not exist")
		return
	}

	//读取支付配置
	conf, ok := PaymentConfig.Load(accountId)
	if !ok {
		err = errors.New("ChannelNotExist")
		logger.Error("GetPayInstance_PayChannel_Err |%v|", accountId)
		return
	}
	channelLocalConf, ok := conf.(map[string]interface{})
	if !ok {
		err = errors.New("channel configure Error")
		logger.Error("Convert_PayChannel_Configure_Err |%v|", accountId)
		return
	}

	// 合并本地配置、数据库配置
	ret = make(map[string]interface{})
	for k, v := range channelLocalConf {
		if k == countryCodeField {
			logger.Error("local payment configure[%v] use system field[%v] ", accountId, countryCodeField)
			return ret, fmt.Errorf("local configure should not use field [%v] ", countryCodeField)
		}

		ret[k] = v
	}

	ret[countryCodeField] = channelDBConf.CountryCode

	return ret, nil
}

func (as *PayAccountConfService) GetAppConfigStructByAccountID(accountId string) (AppAccountPaymentConfig, bool) {
	c, ok := appAccountPayment[accountId]
	return c, ok
}

/*
*
获得用户充值支付配置
*/
func (as *PayAccountConfService) GetAllUserPaySettings() (all []model.AccountPaymentSettings, err error) {
	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("PayAccountConfService_GetUserPayChannel_Err | err=%v", err)
		return nil, err
	}

	all = make([]model.AccountPaymentSettings, 0, 256)
	err = engine.Find(&all)
	if err != nil {
		logger.Error("PayAccountConfService_GetUserPayChannel_DB_Err | err=%v", err)
		return nil, err
	}

	return
}

func (as *PayAccountConfService) GetAllChannelSettings() (all []model.Channel, err error) {
	if dao.HelpayRedisGetSuspendChannel() { // ok ---> suspend all channel
		return nil, nil
	}

	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("PayAccountConfService_GetUserChannel_Err | err=%v", err)
		return nil, err
	}

	all = make([]model.Channel, 0, 256)
	err = engine.Find(&all)
	if err != nil {
		logger.Error("PayAccountConfService_GetUserChannel_DB_Err | err=%v", err)
		return nil, err
	}

	return
}

/*
*
获得用户开发者配置
*/
func (as *PayAccountConfService) GetAllDeveloperSettings() (all []model.AccountDeveloperSettings, err error) {
	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("PayAccountConfService_GetDeveloperSettings_Err | err=%v", err)
		return nil, err
	}

	all = make([]model.AccountDeveloperSettings, 0, 256)
	err = engine.Find(&all)
	if err != nil {
		logger.Error("PayAccountConfService_GetDeveloperSettings_QueryAllErr | err=%v", err)
		return nil, err
	}

	return
}

func (as *PayAccountConfService) GetInstance(payAccountId string, payChannel model.PayChannel) (payIns interface{}, err error) {
	conf, err := as.GetLocalPaymentConfigByAccountID(payAccountId)
	if err != nil {
		logger.Error("GetInstance_PayChannel_Err | err=%v", err)
		return
	}

	payIns, _ = channel.GetPayObj(payChannel, conf)
	if payIns == nil {
		err = fmt.Errorf("unsupported %v-%v", payAccountId, payChannel)
		logger.Error("GetInstance_GetPayConf_Err | err=%v", err)
		return
	}

	return
}

/*
*
提现对象
*/
func (as *PayAccountConfService) GetPayOutInstance(payOutArg validator.PayOutValidator) (payIns channel.PaymentContract, err error) {
	//读取支付配置
	var (
		payoutChannel   model.PayChannel
		payoutAccountId string
	)
	settings, ok := GetAccountPaymentSettingsFromCache(payOutArg.Uid)
	if !ok {
		logger.Error("GetPayOutInstance_GetUserPaySettings_Err | uid=%v", payOutArg.Uid)
		err = errors.New("account payment not exist")
		return
	}
	//最小提现金额判断
	if settings.UserWithdrawMin != 0 && payOutArg.Amount < settings.UserWithdrawMin {
		return nil, errors.New("AMOUNT_TOO_SMALL")
	}
	if settings.PayoutChannel != "" {
		if settings.ThirdPayMin > 0 && payOutArg.Amount < settings.ThirdPayMin {
			//如果启用了三级，最小金额判断，切换到备用通道
			payoutChannel = settings.ThirdPayChannel
			payoutAccountId = settings.ThirdPayAccountId
		} else if settings.BackupWithdrawMin > 0 && payOutArg.Amount < settings.BackupWithdrawMin {
			//如果启用了二级，最小金额判断，切换到备用通道
			payoutChannel = settings.BackupPayoutChannel
			payoutAccountId = settings.BackupPayoutAccountId
		} else {
			payoutChannel = settings.PayoutChannel
			payoutAccountId = settings.PayoutAccountId
		}
	} else {
		//路由策略 todo
		payoutChannel = model.CASHFREE
		payoutAccountId = "cashfree1"
		//payoutChannel = model.PAYU
		//payoutAccountId = "payu3"
	}

	conf, err := as.GetLocalPaymentConfigByAccountID(payoutAccountId)
	if err != nil {
		logger.Error("GetPayOutInstance_PayChannel_Err | err=%v", err)
		return
	}
	payIns, err = channel.GetPayObj(payoutChannel, conf)
	if err != nil {
		logger.Error("GetPayOutInstance_GetPayConf_Err | err=%v", err)
		return
	}
	return
}

func (as *PayAccountConfService) GetPayInstance(uid int64, Type model.PayMethodType, amount float64, args validator.PayEntryValidator) (payIns channel.PaymentContract, code int, err error) {
	//根据用户获得配置
	paySettings, ok := GetAccountPaymentSettingsFromCache(uid)
	if !ok {
		logger.Error("GetPayInstance_Err | user[%v] not exist!", uid)
		return nil, errdef.Code500, fmt.Errorf("user setting not exist")
	}

	switch paySettings.CountryCode {
	case 76: // 巴西不限制最小金额
	default:
		if amount < 10 {
			return nil, errdef.Code400, errors.New("AMOUNT_TOO_SMALL_1 < 10")
		}
	}

	//最小充值金额判断
	if paySettings.UserChargeMin != 0 && amount < paySettings.UserChargeMin {
		return nil, errdef.Code400, fmt.Errorf("AMOUNT_TOO_SMALL[%v<%v]", amount, paySettings.UserChargeMin)
	}
	if paySettings.UserChargeMax != 0 && amount > paySettings.UserChargeMax {
		return nil, errdef.Code400, fmt.Errorf("AMOUNT_TOO_BIG[%v>%v]", amount, paySettings.UserChargeMax)
	}

	if (paySettings.PayChannel == model.PAYG || paySettings.PayChannel == model.CLICKNCASH) && len(args.Phone) < 10 {
		return nil, errdef.Code400, fmt.Errorf("MOBILE_AT_LEAST_10_LENGTH")
	}

	conf, err := as.GetLocalPaymentConfigByAccountID(paySettings.PayAccountId)
	if err != nil {
		return nil, errdef.Code500, err
	}

	payIns, err = channel.GetPayObj(paySettings.PayChannel, conf)
	if err != nil {
		logger.Error("GetPayInstance_GetPayConf_Err | err=%v - |%v|", err, paySettings.PayChannel)
		return nil, errdef.Code500, err
	}
	return payIns, errdef.CodeOK, nil
}

/*
*
获得指定支付对象
*/
func (as *PayAccountConfService) GetSpecificPayInstance(payChannel model.PayChannel, payAccountId string) (payIns channel.PaymentContract, err error) {
	//根据用户渠道获得配置
	conf, err := as.GetLocalPaymentConfigByAccountID(payAccountId)
	if err != nil {
		logger.Error("GetSpecificPayInstance_PayChannel_Err | err=%v | payAccountId=%v | payChannel=%v", err, payAccountId, payChannel)
		return
	}

	payIns, err = channel.GetPayObj(payChannel, conf)
	if err != nil {
		logger.Error("GetSpecificPayInstance_GetPayConf_Err | err=%v | payAccountId=%v | payChannel=%v", err, payAccountId, payChannel)
		return
	}
	return
}

func (as *PayAccountConfService) GetSpecificPayInstanceTest(payChannel model.PayChannel, payAccountId string) (payIns channel.PaymentContract, err error) {
	//根据用户渠道获得配置
	conf, err := as.GetLocalPaymentConfigByAccountID(payAccountId)
	if err != nil {
		logger.Error("GetSpecificPayInstance_PayChannel_Err | err=%v | payAccountId=%v | payChannel=%v", err, payAccountId, payChannel)
		return
	}

	payIns, err = channel.GetPayObj(payChannel, conf)
	if err != nil {
		logger.Error("GetSpecificPayInstance_GetPayConf_Err | err=%v | payAccountId=%v | payChannel=%v", err, payAccountId, payChannel)
		return
	}
	return
}

/*
*
获取指定的UPI实例
*/
func (as *PayAccountConfService) GetSpecificUPIPayInstance(payChannel model.PayChannel, payAccountId string) (payIns channel.UPIContract, err error) {
	//根据用户渠道获得配置
	conf, err := as.GetLocalPaymentConfigByAccountID(payAccountId)
	if err != nil {
		logger.Error("GetSpecificUPIPayInstance_PayChannel_Err | err=%v | payAccountId=%v | payChannel=%v", err, payAccountId, payChannel)
		return
	}

	payIns, err = channel.GetUPIObj(payChannel, conf)
	if err != nil {
		logger.Error("GetSpecificUPIPayInstance_GetPayConf_Err | err=%v | payAccountId=%v | payChannel=%v", err, payAccountId, payChannel)
		return
	}
	return
}

/*
*
内部应用渠道获取
*/
func (as *PayAccountConfService) GetPayChannelAccountId(appId string, pChannel model.PayChannel, Type string) (payChannel model.PayChannel, payAccountId string) {
	if Type == "order" {
		//充值配置
		switch appId {
		case string(model.RUMMY):
			if pChannel == model.PAYTM {
				payChannel = model.PAYTM
				payAccountId = "paytm1"
			} else {
				payChannel = model.PAYU
				payAccountId = "payu1"
			}
			payChannel = model.PAYU
			payAccountId = "payu1"
		case string(model.JHRUMMY):
			payChannel = model.SERPAY
			payAccountId = "serpay1"

		case string(model.GameBR):
			payChannel = model.SERPAY
			payAccountId = "serpay_br"

		case string(model.GameMX):
			payChannel = model.SERPAY
			payAccountId = "serpay_mx"

		case string(model.GameCL):
			payChannel = model.SERPAY
			payAccountId = "serpay_cl"
		}
	} else {
		//提现配置
		switch appId {
		case string(model.RUMMY):
			payChannel = model.PAYU
			payAccountId = "payu1"
		case string(model.JHRUMMY):
			payChannel = model.EASEBUZZ
			payAccountId = "easebuzz1"
		}
	}
	return
}
