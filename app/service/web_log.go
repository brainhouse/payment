package service

import (
	"funzone_pay/app/validator"
	"funzone_pay/dao"
	"funzone_pay/model"
	"github.com/wonderivan/logger"
)

var WebLogServ *WebLogService

func WebLogInit() {
	once.Do(func() {
		WebLogServ = new(WebLogService)
	})
}

type WebLogService struct{}

func (us *WebLogService) Record(args *validator.WebLogValidator) {
	engine, err := dao.GetMysql()
	if err != nil {
		logger.Error("WebLogService_Record_EngineError | err=%v | data=%v", err, args)
		return
	}
	log := new(model.PayWebLog)
	log.Action = args.Action
	log.PayAppId = args.PayAppId
	log.Channel = args.Channel
	log.IP = args.IP
	log.OrderId = args.OrderId
	_, err = engine.Insert(log)
	if err != nil {
		logger.Error("WebLogService_Record_GetError | err=%v | args=%v", err, args)
	}
}
