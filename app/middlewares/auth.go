package middlewares

import (
	"fmt"
	"funzone_pay/app/service"
	"funzone_pay/centerlog"
	"funzone_pay/model"
	"funzone_pay/utils"
	"github.com/gin-gonic/gin"
	"github.com/wonderivan/logger"
	"os"
	"strings"
)

const ContextDataKey string = "POST_DATA"

/*
*
平台用户校验
*/
func PlatFormAuth(c *gin.Context) {
	var (
		pData    []byte //提交数据
		err      error
		sign     string                         //签名
		settings model.AccountDeveloperSettings //开发者配置
		ok       bool
		resData  map[string]interface{}
		env      string
		ips      []string
		ipPass   bool
	)
	appId := c.GetHeader("AppId")
	signature := c.GetHeader("Signature")
	if appId == "" {
		resData = map[string]interface{}{
			"code": "400",
			"msg":  "app_id is invalid!",
		}
		c.JSON(400, resData)
		goto Error
	}

	pData, err = c.GetRawData()
	if err != nil {
		resData = map[string]interface{}{
			"code": "500",
			"msg":  "get data err!",
		}
		c.JSON(500, resData)
		goto Error
	}
	settings, ok = service.GetAccountDeveloperSettingsFromCache(appId)
	if !ok {
		resData = map[string]interface{}{
			"code": "500",
			"msg":  "app internal error!",
		}
		c.JSON(400, resData)
		goto Error
	}
	//ip白名单判断
	ips = strings.Split(settings.IpWhiteList, ",")
	for _, v := range ips {
		if c.ClientIP() == v {
			ipPass = true
		}
	}
	if settings.IpWhiteList == "" || !ipPass {
		resData = map[string]interface{}{
			"code": "400",
			"msg":  fmt.Sprintf("ip white list error! IP:%v", c.ClientIP()),
		}

		centerlog.Fatal(centerlog.MsgWhiteList, map[string]interface{}{
			centerlog.FieldClientIP:  c.ClientIP(),
			centerlog.FieldAppId:     appId,
			centerlog.FieldWhiteList: settings.IpWhiteList,
		})
		logger.Error("PlatFormAuth_NotInIpWhiteList | app_id=%v|uid=%v | customSign=%v, | rawData=%v| ip=%v | header=%v", appId, settings.Uid, signature, string(pData), c.ClientIP(), c.Request.Header)
		c.JSON(400, resData)
		goto Error
	}
	//签名校验
	env = os.Getenv("PAYMENT_ENV")
	if env == "" {
		//ip黑名单判断
		if service.BlackListServ.IsBlack(c.ClientIP(), service.BlackTypeIP) {
			logger.Debug("PlatFormAuth_Forbid | app_id=%v | customSign=%v | rawData=%v | ip=%v | header=%v", appId, signature, string(pData), c.ClientIP(), c.Request.Header)
			c.JSON(200, resData)
			goto Error
		}

		sk := "123"
		// 收款两个都可以, 出款只用第二个
		if strings.Index(c.Request.URL.Path, "platform/payout") < 0 {
			sk, _ = utils.AesDecryptString(settings.SecretEncrypt)
		} else {
			sk, _ = utils.AesDecryptString2(settings.SecretEncrypt)
		}

		sign = utils.ComputeHmacSha256(string(pData), sk)
		logger.Debug("PlatFormAuth_Info | app_id=%v|uid=%v | customSign=%v | rawData=%v | sign=%v | ip=%v | header=%v", appId, settings.Uid, signature, string(pData), sign, c.ClientIP(), c.Request.Header)
		if signature != sign {
			resData = map[string]interface{}{
				"code": "400",
				"msg":  "sign error!",
			}
			c.JSON(400, resData)
			goto Error
		}
		//用户是否封禁
		user := service.UserServ.GetUserInfo(settings.Uid)
		if user == nil {
			resData = map[string]interface{}{
				"code": "400",
				"msg":  "user not exist!",
			}
			c.JSON(400, resData)
			goto Error
		}
		if user.Status != 0 {
			resData = map[string]interface{}{
				"code": "400",
				"msg":  "user is not available!",
			}
			c.JSON(400, resData)
			goto Error
		}
	}
	c.Set("UID", settings.Uid)
	c.Set("APP_ID", settings.AppId)
	c.Set(ContextDataKey, pData)
	c.Next()
Error:
	c.Abort()
	return
}
