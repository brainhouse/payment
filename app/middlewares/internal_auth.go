package middlewares

import (
	"github.com/gin-gonic/gin"
	"github.com/wonderivan/logger"
)

func InternalAuth(c *gin.Context) {
	cip := c.ClientIP()
	logger.Debug("InternalAuth | ip=%v", cip)
	if cip != "43.204.236.6" && cip != "172.31.8.187" && cip != "127.0.0.1" {
		logger.Error("InternalAuthError | ip=%v", c.ClientIP())
		c.Abort()
		return
	}
	c.Next()
}
