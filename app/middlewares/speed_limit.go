package middlewares

import (
	"fmt"
	"funzone_pay/app/service"
	"github.com/gin-gonic/gin"
	"strings"
)

func SpeedLimit(limit map[string]int64) gin.HandlerFunc {
	limitPath := make(map[string]int64)
	for k, v := range limit {
		limitPath[k] = v
	}

	return func(c *gin.Context) {
		var speed int64 = 0
		var opt = ""
		for key, val := range limitPath {
			idx := strings.Index(c.Request.URL.Path, key)
			if idx != -1 { // path need be limited
				opt = key
				speed = val
				break
			}
		}

		// if setting limit，then check interface
		if speed > 0 {
			appId := c.GetHeader("AppId")
			if !service.GetPayEntryService().CheckSpeed(appId, opt, speed) {
				c.JSON(400, &map[string]string{"code": "400", "msg": fmt.Sprintf("%v: request[%v] so frequently", appId, c.Request.URL.Path)})
				c.Abort()
				return
			}
		}

		c.Next()
	}
}