package common

import (
	"fmt"
	"funzone_pay/centerlog"
	"funzone_pay/centerlog/graylog"
	"funzone_pay/utils"
	"net/http"
	"runtime"
	"time"

	"github.com/wonderivan/logger"
)

// 无参数安全(loop variable)
func SafeGoroutine(f func()) {
	defer func() {
		if panicErr := recover(); panicErr != nil {
			var stack string
			for i := 1; ; i++ {
				_, file, line, ok := runtime.Caller(i)
				if !ok {
					break
				}
				stack = stack + fmt.Sprintln(fmt.Sprintf("%s:%d", file, line))
			}
			data := make(graylog.M)
			errInfo := fmt.Sprintf("[run time panic]: %v", panicErr)
			data["errInfo"] = errInfo
			data["stack"] = stack
			data["fileMark"] = "safe-goroutine"
			centerlog.InfoAPI("Panic", data)
		}
	}()

	//目标函数
	f()
}

var PublicIP = "0.0.0.0"
var ServerName = "none"

func InitPublicIP() {
	unix := time.Now().Unix()
	param := map[string]interface{}{
		"id":   "fpay",
		"unix": unix,
	}
	code, res, err := utils.Get("https://ip.rummyshine.com/api/raw", http.Header{}, param)
	if err != nil || code != 200 {
		msg := fmt.Sprintf("%v-%v", err, code)
		logger.Error("init global error %v", msg)
		//code, res, err = Get("https://myexternalip.com/raw", http.Header{}, nil)
		//if err != nil || code != 200 {
		//	logger.Error("init global2 %v-%v", err, code)
		//	return
		//}
		go func() {
			for i := 0; i < 20; i++ {
				PushAlarmEvent(Warning, "fpay", "/ip/raw", "", msg)
				time.Sleep(time.Second)
			}
		}()
	}

	resIp := string(res)
	if len(resIp) > 16 {
		resIp = resIp[:16]
	}
	PublicIP = resIp
	logger.Debug("global->ip:%v\n", PublicIP)
}
