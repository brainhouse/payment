package common

import (
	"crypto/md5"
	"fmt"
	"funzone_pay/utils"
	jsoniter "github.com/json-iterator/go"
	"github.com/wonderivan/logger"
	"time"
)

type LevelType int

const (
	Info    LevelType = 0
	Warning LevelType = 1
)

type AlarmEvent struct {
	Level   LevelType `json:"level"`
	Path    string    `json:"path"`
	Raw     string    `json:"raw"`
	AppId   string    `json:"app_id"`
	Message string    `json:"message"`
}

func (event *AlarmEvent) Key() string {
	key := fmt.Sprintf("%s-%s-%s", event.AppId, event.Path, event.Raw)
	return fmt.Sprintf("%X", md5.Sum([]byte(key)))
}

type AlarmData struct {
	Data map[string]AlarmEvent
}

func (alarm *AlarmData) AddEvent(event AlarmEvent) {
	key := event.Key()
	_, ok := alarm.Data[key]
	if ok {
		return
	}

	alarm.Data[key] = event
}

func (alarm *AlarmData) SendAlarm() {
	cnt := 0
	for _, event := range alarm.Data {
		urlPath := getUrl(event.Level)
		SendAlarm(urlPath, event.Message)
		cnt += 1
		if cnt > 64 { // 限制一次发送的最大数量
			break
		}
	}

	if len(alarm.Data) > 0 {
		alarm.Data = make(map[string]AlarmEvent)
	}
}

const EventChainMax = 128

var eventChan = make(chan AlarmEvent, EventChainMax*2)
var alarmData = AlarmData{Data: make(map[string]AlarmEvent)}

func PushAlarmEvent(level LevelType, appId string, path string, raw string, message string) {
	if len(eventChan) > EventChainMax {
		return
	}

	eventChan <- AlarmEvent{
		Level:   level,
		AppId:   appId,
		Path:    path,
		Raw:     raw,
		Message: message,
	}
}

func InitAlarmRoutine() {
	go func() {
		defer func() {
			if r := recover(); r != nil {
				logger.Error("recovered from ", r)
			}
		}()

		tic := time.NewTicker(time.Minute * 2)
		for {
			select {
			case event := <-eventChan:
				alarmData.AddEvent(event)

			case <-tic.C:
				alarmData.SendAlarm()
			}
		}
	}()
}

func getUrl(level LevelType) string {
	switch level {
	case Info:
		return urlInfo

	case Warning:
		return urlWarn
	}

	return urlInfo
}

func SendAlarmByLevel(level LevelType, msg string) error {
	targetUrl := getUrl(level)
	return SendAlarm(targetUrl, msg)
}

func SendAlarm(targetUrl string, msg string) error {
	packData := map[string]interface{}{
		"msg_type": "text",
		"content":  fmt.Sprintf("{\"text\":\"<at user_id=\\\"all\\\">所有人</at>%s,快来救火!!!\"}", msg),
	}

	bts, err := jsoniter.Marshal(packData)
	if err != nil {
		logger.Error("send alarm marshal error [%v]", err)
		return err
	}

	return send(targetUrl, bts)
}

const urlInfo = "https://open.feishu.cn/open-apis/bot/v2/hook/fb60a833-22ca-4e2d-a870-916690c98a3a"
const urlWarn = "https://open.feishu.cn/open-apis/bot/v2/hook/8bfe966c-19f0-4911-a1f2-96cfe1c9cfe2"

func send(targetUrl string, bts []byte) error {
	//curl -X POST -H "Content-Type: application/json" -d '{"msg_type":"text","text":{"text":"@seal ads 账户超支！！！"}}' https://open.feishu.cn/open-apis/bot/v2/hook/6821a577-8fb2-43cd-855f-2b2270b5628b
	//curl -X POST -H "Content-Type: application/json" -d '{"msg_type":"text","content":" "}' https://open.feishu.cn/open-apis/bot/v2/hook/6821a577-8fb2-43cd-855f-2b2270b5628b
	code, res, err := utils.Post(targetUrl, bts, map[string][]string{"Content-Type": []string{"application/json"}})
	if err != nil {
		logger.Error("send alarm [%v] error [%v]", string(bts), err)
		return err
	}

	logger.Debug("send alarm [%v], code:%v, res:%v", string(bts), code, string(res))
	return nil
}
