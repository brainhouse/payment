package dao

import (
	_ "github.com/go-sql-driver/mysql"
	"github.com/spf13/viper"
	"github.com/wonderivan/logger"
	"xorm.io/xorm"
)

var mysql *xorm.Engine

func GetMysql() (*xorm.Engine, error) {
	if mysql != nil {
		return mysql, nil
	}
	mysql, err := xorm.NewEngine("mysql", viper.GetString("mysql.url"))
	if err != nil {
		logger.Fatal("Fail to create xorm engine: %v\n", err)
		return nil, err
	}
	mysql.ShowSQL(true)
	return mysql, nil
}
