package dao

import (
	"fmt"
	"github.com/garyburd/redigo/redis"
	"github.com/spf13/viper"
	"github.com/wonderivan/logger"
	"time"
)

var rdPool *redis.Pool
var helpayRdPool *redis.Pool

func RedisInit() {
	addr := viper.GetString("redis.addr")
	db := viper.GetInt("redis.db")
	pwd := viper.GetString("redis.pwd")
	if rdPool == nil {
		rdPool = Init(addr, db, pwd)
	}

	helpAddr := viper.GetString("helpay_redis.addr")
	helpDB := viper.GetInt("helpay_redis.db")
	helpPwd := viper.GetString("helpay_redis.pwd")
	if helpayRdPool == nil {
		helpayRdPool = Init(helpAddr, helpDB, helpPwd)
	}
}

func Init(addr string, db int, pwd string) *redis.Pool {
	options := make([]redis.DialOption, 0, 8)
	options = append(options, redis.DialDatabase(db))
	if len(pwd) > 0 {
		options = append(options, redis.DialPassword(pwd))
	}

	return &redis.Pool{
		MaxIdle:     100,
		MaxActive:   1000,
		IdleTimeout: 10 * time.Second,
		Dial: func() (redis.Conn, error) {
			return redis.Dial("tcp", addr, options...)
		},
	}
}

func RedisGet(key string) string {
	c := rdPool.Get()
	defer c.Close()
	if c == nil {
		logger.Error("RedisGetConnectErr")
		return ""
	}
	s, _ := redis.String(c.Do("GET", key))
	return s
}

func RedisLock(key string, value string, expire int) error {
	c := rdPool.Get()
	defer c.Close()

	_, err := c.Do("SET", key, value, "EX", expire, "NX")
	if err != nil {
		logger.Error("RedisSetNX | key=%v | value=%v | err=%v", key, value, err)
		return err
	}
	return nil
}

func RedisSet(key string, value string, expire int) {
	if expire == 0 {
		expire = -1
	}
	c := rdPool.Get()
	defer c.Close()
	_, err := c.Do("SET", key, value, "EX", fmt.Sprint(expire))
	if err != nil {
		logger.Error("RedisSet | key=%v | value=%v | err=%v", key, value, err)
		return
	}
	return
}

func RedisSetBytes(key string, value []byte, expire int) {
	if expire == 0 {
		expire = -1
	}
	c := rdPool.Get()
	defer c.Close()
	_, err := c.Do("SET", key, value, "EX", fmt.Sprint(expire))
	if err != nil {
		logger.Error("RedisSet | key=%v | value=%v | err=%v", key, value, err)
		return
	}
	return
}

func RedisGetBytes(key string) []byte {
	c := rdPool.Get()
	defer c.Close()
	if c == nil {
		logger.Error("RedisGetConnectErr")
		return []byte("")
	}
	s, _ := redis.Bytes(c.Do("GET", key))
	return s
}


func RedisDel(key string) {
	c := rdPool.Get()
	defer c.Close()
	_, err := c.Do("DEL", key)
	if err != nil {
		logger.Error("RedisDel | key=%v | err=%v", key, err)
		return
	}
	return
}

func RedisTTL(key string) (value int64, err error) {
	c := rdPool.Get()
	defer c.Close()
	value, err = redis.Int64(c.Do("TTL", key))
	if err != nil {
		logger.Error("RedisTTL | key=%v | err=%v", key, err)
	}
	return value, err
}

func RedisIncr(key string, incr int64) (value int64, err error) {
	c := rdPool.Get()
	defer c.Close()
	value, err = redis.Int64(c.Do("INCRBY", key, incr))
	if err != nil {
		logger.Error("RedisIncr | key=%v | err=%v", key, err)
	}

	return value, err
}

func RedisExpire(key string, value int64) (err error) {
	c := rdPool.Get()
	defer c.Close()
	_, err = c.Do("EXPIRE", key, value)
	if err != nil {
		logger.Error("RedisExpire | key=%v | err=%v", key, err)
	}

	return err
}

func HelpayRedisGetSuspendChannel() bool {
	//"suspend_channel_key"
	c := helpayRdPool.Get()
	defer c.Close()

	reply, err := redis.Int(c.Do("EXISTS", "suspend_channel_key"))
	if err != nil {
		return false
	}

	return reply > 0
}
