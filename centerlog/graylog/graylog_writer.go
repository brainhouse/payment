package graylog

import (
	"bytes"
	"compress/zlib"
	"io/ioutil"
	"sync"
)

var writerPool sync.Pool
var buffPool sync.Pool

func init() {
	writerPool = sync.Pool{
		New: func() interface{} {
			w := zlib.NewWriter(ioutil.Discard)
			if w == nil {
				return nil
			}
			buff := bytes.NewBuffer(make([]byte, 0, 1536)) // 1.5k
			return &Writer{
				Writer: w,
				Buffer: buff,
			}
		},
	}

	buffPool = sync.Pool{
		New: func() interface{} {
			return bytes.NewBuffer(make([]byte, 0, 1536)) // 至少是 defaultMaxChunkSizeWan = 1420
		},
	}
}

func getWriter() (*Writer, bool) {
	w, ok := writerPool.Get().(*Writer)
	if !ok {
		return nil, false
	}

	w.Reset()
	return w, true
}

func putWriter(w *Writer) {
	writerPool.Put(w)
}

func getBuffer() *bytes.Buffer {
	b := buffPool.Get().(*bytes.Buffer)
	b.Reset()
	return b
}

func putBuffer(b *bytes.Buffer) {
	buffPool.Put(b)
}

type Writer struct {
	*zlib.Writer
	*bytes.Buffer
}

func (w *Writer) Write(p []byte) (n int, err error) {
	return w.Writer.Write(p)
}

func (w *Writer) Flush() error {
	return w.Writer.Flush()
}

func (w *Writer) Close() error {
	return w.Writer.Close()
}

func (w *Writer) Reset() {
	w.Buffer.Reset()
	w.Writer.Reset(w.Buffer)
}

func (w *Writer) Len() int {
	return w.Buffer.Len()
}

func (w *Writer) Bytes() []byte {
	return w.Buffer.Bytes()
}

func (w *Writer) Next(n int) []byte {
	return w.Buffer.Next(n)
}
