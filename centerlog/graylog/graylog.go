package graylog

import (
	"fmt"
	jsoniter "github.com/json-iterator/go"
	"math"
	"math/rand"
	"net"
	"os"
	"strings"
	"sync"
	"time"
)

const (
	defaultConnection      = "wan"
	defaultMaxChunkSizeWan = 1420
	defaultMaxChunkSizeLan = 8154
	chunkedHeaderLen = 12
)

var (
	magicChunked = []byte{0x1e, 0x0f}
)

var now time.Time
var local *time.Location

func init() {
	now = time.Now().Round(time.Second)
	local, _ = time.LoadLocation("Local")
	go refresh()
}

// 精确到 100ms
func refresh() {
	intel := 100 * time.Millisecond
	for {
		now = time.Now().Round(time.Second)
		time.Sleep(intel)
	}
}

// M 为了简化代码
type M map[string]interface{}

type grayConfig struct {
	Application     string
	GraylogHost     string
	GraylogPort     string
	Connection      string
	MaxChunkSizeWan int
	MaxChunkSizeLan int
	HostName        string
}

type Gelf struct {
	grayConfig
	sync.Pool
}

func InitGraylog(application, host, grayLogHost, grayLogPort, mode string) (g *Gelf) {
	config := grayConfig{
		Application: application,
		HostName:    host,
		GraylogHost: grayLogHost,
		GraylogPort: grayLogPort,
		Connection:  mode,
	}

	//
	if config.Connection == "" {
		config.Connection = defaultConnection
	}
	if config.MaxChunkSizeWan == 0 {
		config.MaxChunkSizeWan = defaultMaxChunkSizeWan
	}
	if config.MaxChunkSizeLan == 0 {
		config.MaxChunkSizeLan = defaultMaxChunkSizeLan
	}
	if config.HostName == "" {
		config.HostName, _ = os.Hostname()
	}

	g = &Gelf{
		grayConfig: config,
		Pool: sync.Pool{
			New: func() interface{} {
				var addr = fmt.Sprintf("%s:%s", config.GraylogHost, config.GraylogPort)
				udpAddr, err := net.ResolveUDPAddr("udp", addr)
				if err != nil {
					return nil
				}
				conn, err := net.DialUDP("udp", nil, udpAddr)
				if err != nil {
					return nil
				}

				return conn
			},
		},
	}
	return g
}

func (g *Gelf) getConn() (*net.UDPConn, bool) {
	conn, ok := g.Pool.Get().(*net.UDPConn)
	return conn, ok
}

func (g *Gelf) putConn(conn *net.UDPConn) {
	g.Pool.Put(conn)
}

func (g *Gelf) Log(level uint, message string, data M) {
	if g == nil {
		fmt.Printf("GrayLog Is Nil, %d-%s-%v\n", level, message, data)
		return
	}

	obj := M{
		"version":       "1.0",
		"host":          g.Application,
		"_server":       g.HostName,
		"short_message": message,
		"level":         level,
		"__tms":          now.Unix(),
	}
	for k, v := range data {
		obj[fmt.Sprint("_", k)] = v
	}

	raw, err := jsoniter.Marshal(obj)
	if err != nil {
		fmt.Printf("GrayLog Marshal Error: %v-->[%v]\n", err, obj)
		return
	}

	w, ok := getWriter()
	if !ok {
		fmt.Printf("GrayLog Get Writer Error\n")
		return
	}

	// 压缩写入msg
	w.Write(raw)
	w.Close()

	conn, ok := g.getConn()
	if !ok {
		fmt.Printf("GrayLog Get UDP Conn Error\n")
		return
	}

	n := 0
	size := w.Len()
	chunkSize := g.ChunkSize()
	if size > chunkSize { // 多个块
		chunkDataSize := chunkSize - chunkedHeaderLen                        // 每一块数据长度
		chunkCount := int(math.Ceil(float64(size) / float64(chunkDataSize))) // 分多次发送需要头信息，头信息 12 字节,
		if chunkCount > 128 {
			putWriter(w) // 基于性能考虑，没使用defer
			g.putConn(conn)
			fmt.Printf("GrayLog Message Too Long, %d-%d-%d-->%s\n",  size, g.ChunkSize(), chunkCount, message)
			return
		}

		// 创建内存，随机一个数据,协议需要
		id := make([]byte, 8)
		rand.Read(id)

		packet := getBuffer()
		for i, index := 0, 0; i < size; i, index = i+chunkDataSize, index+1 {
			packet.Reset()
			packet.Write(magicChunked)
			packet.Write(id)
			packet.WriteByte(byte(index))
			packet.WriteByte(byte(chunkCount))
			packet.Write(w.Next(chunkDataSize))

			n, err = conn.Write(packet.Bytes())
			if err != nil || n != packet.Len() {
				fmt.Printf("GrayLog Write Message Error Multi, %v-%d-%d-->%s:%s\n",  err, n, packet.Len(), message, string(raw))
				break
			}
		}

		putBuffer(packet) // 基于性能考虑，没有使用defer
	} else {
		n, err = conn.Write(w.Bytes())
		if err != nil || n != w.Len() {
			fmt.Printf("GrayLog Write Message Error, %v-%d-%d-->%s:%s\n",  err, n, w.Len(), message, string(raw))
		}
	}
	putWriter(w) // 基于性能考虑，没使用defer
	g.putConn(conn)

	// TODO Debug
	if size > defaultMaxChunkSizeWan {
		fmt.Printf("GrayLog Message ToMax, %d-->%s\n",  size, message)
	}

	if strings.Contains(message, "Panic") || strings.Contains(message, "Error"){
		fmt.Printf("GrayLog Message Alarm, %s-->%s\n", message, string(raw))
	}
}

func (g *Gelf) ChunkSize() int {
	if g.Connection == "lan" {
		return g.MaxChunkSizeLan
	}
	return g.MaxChunkSizeWan
}
