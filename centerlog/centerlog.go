package centerlog

import (
	"fmt"
	"funzone_pay/centerlog/graylog"
	"runtime"
)

const (
	INFO      = iota
	PANIC     // 毁天灭地，大于1条就告警
	FATAL     // 致命，大于10条就告警
	IMPORTANT // 严重，大于50条就告警
	ERROR     // 错误，大于100条就告警
	WARNING   // 警告，大于500条就告警
)

const (
	MsgWhiteList         = "notWhiteList"
	MsgAPI               = "api"
	MsgErrorResp         = "errorResp"
	MsgPayOutCb          = "outCb"
	MsgPayOutOrder       = "outOrder"
	MsgPayOutCreateFail  = "outCreateFail"
	MsgPayInCb           = "inCb"
	MsgPayInOrder        = "inOrder"
	MsgPayInCreateFail   = "inCreateFail"
	MsgThirdPlatformFail = "thirdPlatformFail"
	MsgRepeatCallBack    = "repeatCallBack"
)

const (
	FieldUid            = "uid"
	FieldAppId          = "appId"
	FieldUUUID          = "uuuid"
	FieldStatus         = "status"
	FieldStatusDesc     = "statusDesc"
	FieldClientIP       = "clientIP"
	FieldDur            = "dur"
	FieldPath           = "path"
	FieldReqBody        = "reqBody"
	FieldWhiteList      = "whiteList"
	FieldRaw            = "raw"
	FieldAccountId      = "accountId"
	FieldOrderType      = "orderType"
	FieldOrderId        = "orderID"
	FieldAppOrderId     = "appOrderId"
	FieldPaymentOrderID = "paymentOrderId"
	FieldBankCard       = "bankCard"
	FieldVPA            = "vpa"
	FieldUTR            = "utr"
	FieldAmount         = "amount"
	FieldOrderStatus    = "orderStatus"
	FieldThirdDesc      = "thirdDesc"
	FieldThirdStatus    = "thirdStatus"
	FieldCBStatus       = "cbStatus"
)

var gLog, gAPI *graylog.Gelf

func Init(host, port, apiHost, apiPort string) {
	gLog = graylog.InitGraylog("FunZonePay", "", host, port, "wan")
	gAPI = graylog.InitGraylog("FunZonePayApi", "", apiHost, apiPort, "wan")
}

func safeGoroutine(f func()) {
	defer func() {
		if panicErr := recover(); panicErr != nil {
			var stack string
			for i := 1; ; i++ {
				_, file, line, ok := runtime.Caller(i)
				if !ok {
					break
				}
				stack = stack + fmt.Sprintln(fmt.Sprintf("%s:%d", file, line))
			}
			errInfo := fmt.Sprintf("[run time panic]: %v", panicErr)
			fmt.Printf("safe-go error %v-%v\n", errInfo, stack)
		}
	}()

	//目标函数
	f()
}

func InfoAPI(message string, data map[string]interface{}) {
	safeGoroutine(func() {
		gAPI.Log(INFO, message, data)
	})
}

func Info(message string, data map[string]interface{}) {
	safeGoroutine(func() {
		gLog.Log(INFO, message, data)
	})
}

func Warn(message string, data map[string]interface{}) {
	safeGoroutine(func() {
		gLog.Log(WARNING, message, data)
	})
}

func OrderError(message, accountId, appId, appOrderId, orderId, status, desc string) {
	Fatal(message, map[string]interface{}{
		FieldAccountId:   accountId,
		FieldAppId:       appId,
		FieldAppOrderId:  appOrderId,
		FieldOrderId:     orderId,
		FieldOrderStatus: status,
		FieldThirdDesc:   desc,
	})
}

func Error(message string, data map[string]interface{}) {
	safeGoroutine(func() {
		gLog.Log(ERROR, message, data)
	})
}

func Fatal(message string, data map[string]interface{}) {
	safeGoroutine(func() {
		gLog.Log(FATAL, message, data)
	})
}

func CheckErrSendWarning(err error, message string, data map[string]interface{}) {
	if err != nil {
		data["err"] = err
		safeGoroutine(func() {
			gLog.Log(WARNING, message, data)
		})
	}
}
