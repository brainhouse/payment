package errdef

import "errors"

const (
	CodeOK = 0
	Code500 = 500
	Code400 = 400
	InternalMsg = "internal error"

	MaintainTimeCode = 10001
	MaintainTimeMsg = "maintain time"
)

var ErrMaintainTime = errors.New(MaintainTimeMsg)
var ErrInterval = errors.New(InternalMsg)

var errorsMap = map[error]int {
	ErrMaintainTime: MaintainTimeCode,
	ErrInterval: Code500,
}

func GetErrorCode(err error) (int, bool) {
	code, ok := errorsMap[err]
	if ok {
		return code, true
	}

	return Code500, false
}