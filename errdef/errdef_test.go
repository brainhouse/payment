package errdef

import (
	"errors"
	"fmt"
	"testing"
)

func TestFunc(t *testing.T) {
	err := errors.New("random")
	code, msg := GetErrorCode(err)
	fmt.Printf("%v--->%v--%v\n", code, msg, err.Error())

	code, msg = GetErrorCode(ErrInterval)
	fmt.Printf("%v--->%v--%v\n", code, msg, ErrInterval.Error())


	code, msg = GetErrorCode(ErrMaintainTime)
	fmt.Printf("%v--->%v--%v\n", code, msg, ErrMaintainTime.Error())
}
