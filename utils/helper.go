package utils

import (
	"bytes"
	"crypto/aes"
	"crypto/cipher"
	"crypto/md5"
	"encoding/base64"
	"fmt"
	"strings"
)

const PUBKEY = "YDtgZ*nWu$j8#XLQ1IcS^Mx8oJZB$1DR"
const PUBKEY2 = "Qjyr0Nx6H7MJfNf+b+Ctp5TKLD1C3dhx"

func PKCS7Padding(ciphertext []byte, blockSize int) []byte {
	padding := blockSize - len(ciphertext)%blockSize
	padtext := bytes.Repeat([]byte{byte(padding)}, padding)
	return append(ciphertext, padtext...)
}

func PKCS7UnPadding(origData []byte) []byte {
	length := len(origData)
	unpadding := int(origData[length-1])
	return origData[:(length - unpadding)]
}

func AesEncrypt(origData, key []byte) ([]byte, error) {
	block, err := aes.NewCipher(key)
	if err != nil {
		return nil, err
	}
	blockSize := block.BlockSize()
	origData = PKCS7Padding(origData, blockSize)
	blockMode := cipher.NewCBCEncrypter(block, key[:blockSize])
	crypted := make([]byte, len(origData))
	blockMode.CryptBlocks(crypted, origData)
	return crypted, nil
}

func AesDecrypt(crypted, key []byte) (origData []byte, err error) {
	block, err := aes.NewCipher(key)
	if err != nil {
		return nil, err
	}
	defer func() {
		if e := recover(); e != nil {
			origData = []byte("")
			err = fmt.Errorf("%v", e)
		}
	}()

	blockSize := block.BlockSize()
	blockMode := cipher.NewCBCDecrypter(block, key[:blockSize])
	origData = make([]byte, len(crypted))
	blockMode.CryptBlocks(origData, crypted)
	origData = PKCS7UnPadding(origData)
	return origData, nil
}

func AesEncryptString(crypted string) (string, error) {
	res, err := AesEncrypt([]byte(crypted), aesEncryptKeyPreprocess(PUBKEY2))
	if err != nil {
		return "", err
	}
	return base64.StdEncoding.EncodeToString(res), nil
}

func AesDecryptString(crypted string) (string, error) {
	index := strings.Index(crypted, "2_")
	if index == 0 { // 新
		return AesDecryptString2(crypted)
	}

	cryptedTmp, err := base64.StdEncoding.DecodeString(crypted)
	if err != nil {
		return "", err
	}
	res, err := AesDecrypt(cryptedTmp, aesEncryptKeyPreprocess(PUBKEY))
	if err != nil {
		return "", err
	}
	return string(res), nil
}

func AesDecryptString2(crypted string) (string, error) {
	index := strings.Index(crypted, "2_")
	if index != 0 {
		return "", fmt.Errorf("not 2")
	}

	cryptedTmp, err := base64.StdEncoding.DecodeString(crypted[2:])
	if err != nil {
		return "", err
	}
	res, err := AesDecrypt(cryptedTmp, aesEncryptKeyPreprocess(PUBKEY2))
	if err != nil {
		return "", err
	}
	return string(res), nil
}

func aesEncryptKeyPreprocess(key string) []byte {
	has := md5.Sum([]byte(key))
	md5str := fmt.Sprintf("%x", has)
	return []byte(md5str[0:16])
}
