package utils

import "time"

var now time.Time
var local *time.Location

func init() {
	now = time.Now().Round(time.Second)
	local, _ = time.LoadLocation("Local")
	go refresh()
}

// 精确到 100ms
func refresh() {
	intel := 100 * time.Millisecond
	for {
		now = time.Now().Round(time.Second)
		time.Sleep(intel)
	}
}

func TimeNow() time.Time {
	return now
}

func TimeNowUnix() int64 {
	return now.Unix()
}

func TimeNowFormat(layout string) string {
	return now.Format(layout)
}