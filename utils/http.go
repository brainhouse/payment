package utils

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/wonderivan/logger"
	"io/ioutil"
	"net/http"
	"strings"
	"time"
)

func HttpRequest(url string, method string, contentType string, sendData interface{}, header map[string][]string) (res []byte, status int, err error) {
	client := &http.Client{}
	var req *http.Request
	if contentType == "JSON" {
		data, _ := json.Marshal(sendData)
		reader := bytes.NewBuffer(data)
		req, err = http.NewRequest(method, url, reader)
		sendData = string(data)
	} else {
		s := sendData.(string)
		reader := strings.NewReader(s)
		req, err = http.NewRequest(method, url, reader)
	}
	//提交请求
	if err != nil {
		logger.Error("HttpRequest_ERROR | url=%v | err=%+v | req=%+v", url, err, sendData)
		return
	}
	req.Header = http.Header(header)
	//处理返回结果
	response, err := client.Do(req)
	if err != nil {
		logger.Error("HttpRequest_ERROR | url=%v | err=%+v | req=%+v", url, err, sendData)
		return
	}
	client.Timeout = 20 * time.Second
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		logger.Error("HttpRequest_ERROR | url=%v | err=%+v | req=%+v", url, err, sendData)
		return
	}
	defer func() {
		_ = response.Body.Close()
	}()
	status = response.StatusCode
	//logger.Debug("HttpRequest | url=%v | err=%v | req=%v | status=%v | response=%v", url, err, sendData, status, string(body))
	return body, status, nil
}

func Get(url string, header http.Header, params map[string]interface{}) (code int, res []byte, err error) {
	if len(params) > 0 {
		url += "?"
		for k, v := range params {
			url += fmt.Sprintf("%s=%v&", k, v)
		}
		url += fmt.Sprintf("ttrand=%d", time.Now().Unix())
	}

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return
	}

	req.Header = header
	hc := &http.Client{}
	response, err := hc.Do(req)
	if err != nil {
		return
	}

	code = response.StatusCode
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return
	}

	defer func() {
		_ = response.Body.Close()
	}()
	return code, body, nil
}

func Post(url string, sendData []byte, header map[string][]string) (code int, res []byte, err error) {
	client := &http.Client{}
	reader := bytes.NewBuffer(sendData)
	req, err := http.NewRequest("POST", url, reader)
	if err != nil {
		return
	}

	if header != nil {
		req.Header = http.Header(header)
	}

	//处理返回结果
	response, err := client.Do(req)
	if err != nil {
		return
	}

	code = response.StatusCode
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return
	}

	defer func() {
		_ = response.Body.Close()
	}()
	return code, body, nil
}