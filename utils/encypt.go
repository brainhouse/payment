package utils

import (
	"crypto/hmac"
	"crypto/md5"
	"crypto/sha256"
	"crypto/sha512"
	"encoding/base64"
	"encoding/hex"
	"fmt"
	"github.com/xxtea/xxtea-go/xxtea"
	"io"
	"sort"
	"strings"
)

func ComputeHmacSha256(message string, secret string) string {
	key := []byte(secret)
	h := hmac.New(sha256.New, key)
	h.Write([]byte(message))
	sha := hex.EncodeToString(h.Sum(nil))
	return base64.StdEncoding.EncodeToString([]byte(sha))
}

func HmacSha256(message string, secret string) string {
	key := []byte(secret)
	h := hmac.New(sha256.New, key)
	h.Write([]byte(message))
	return base64.StdEncoding.EncodeToString(h.Sum(nil))
}

func HmacSha256ToXStringWithKey(message string, secret string) string {
	key := []byte(secret)
	h := hmac.New(sha256.New, key)
	h.Write([]byte(message))
	return fmt.Sprintf("%x", h.Sum(nil))
}

func HmacSha256ToXString(message string) string {
	var w = sha256.New()
	_, _ = io.WriteString(w, message)
	return fmt.Sprintf("%x", w.Sum(nil))
}

func HSha512(message string) string {
	h := sha512.New()
	h.Write([]byte(message))
	return fmt.Sprintf("%x", sha512.Sum512([]byte(message)))
}

func HSha256(message string) string {
	h := sha256.New()
	h.Write([]byte(message))

	return hex.EncodeToString(h.Sum(nil))
}

//var w = sha256.New()
//_, _ = io.WriteString(w, utils.SortedAndBuild(m) + appsk)
//enc := fmt.Sprintf("%x", w.Sum(nil))
//logger.Debug("PagSmilePayPayOutCallback_CheckData| data=%v | header=%v | enc=%v", string(rawData), c.Request.Header, enc)
//// return enc == head
//return true
//}

func SortedAndBuild(m map[string]interface{}) string {
	var b strings.Builder
	l, cap := len(m), 0
	keySet := make([]string, 0, l)
	for k, v := range m {
		keySet = append(keySet, k)
		if _, ok := v.(string); ok {
			cap = len(k) + len(v.(string)) + 2
		} else {
			cap = len(k) + 10 + 2
		}
	}

	b.Grow(cap)
	sort.Strings(keySet)
	for i, k := range keySet {
		if v, ok := m[k]; ok {
			var str string
			if s, ok := v.(string); ok {
				str = s
			} else {
				str = fmt.Sprintf("%v", v)
			}

			if len(str) > 0 {
				b.WriteString(k)
				b.WriteString("=")
				b.WriteString(str)
				if (i + 1) != l {
					b.WriteString("&")
				}
			}
		}
	}

	return b.String()
}

func Md5Str(s string) string {
	hash := md5.New()
	hash.Write([]byte(s))
	value := hash.Sum(nil)
	return hex.EncodeToString(value)
}

var teaKey = []byte("fz2023#@,.")

func XXTeaEnc(data []byte) []byte {
	return xxtea.Encrypt(data, teaKey)
}

func XXTeaDec(data []byte) []byte {
	return xxtea.Decrypt(data, teaKey)
}
