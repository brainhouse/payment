package utils

import (
	"crypto/md5"
	"fmt"
)

func GetMd5(str string) (s string) {
	return fmt.Sprintf("%x", md5.Sum([]byte(str)))
}

func GetMd5Upper(str string) (s string) {
	return fmt.Sprintf("%X", md5.Sum([]byte(str)))
}
