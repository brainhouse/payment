package utils

import (
	"crypto"
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"encoding/base64"
	"errors"
)

func RsaEncrypt1(publicKey string, origData []byte) (string, error) {
	block, err := base64.StdEncoding.DecodeString(publicKey)
	if err != nil {
		return "", errors.New("public key error")
	}
	pubInterface, err := x509.ParsePKIXPublicKey(block)
	if err != nil {
		return "", err
	}
	pub := pubInterface.(*rsa.PublicKey)

	msgLen := len(origData)

	step := 117
	//要分段加密
	var encryptedBytes []byte
	for start := 0; start < msgLen; start += step {
		finish := start + step
		if finish > msgLen {
			finish = msgLen
		}
		encryptedBlockBytes, err := rsa.EncryptPKCS1v15(rand.Reader, pub, origData[start:finish])
		if err != nil {
			return "", err

		}
		encryptedBytes = append(encryptedBytes, encryptedBlockBytes...)
	}
	return base64.StdEncoding.EncodeToString(encryptedBytes), nil
}

func RsaSignEncrypt(privateKey string, origData []byte, hash crypto.Hash) (string, error) {
	block, err := base64.StdEncoding.DecodeString(privateKey)
	if err != nil {
		return "", errors.New("public key error")
	}
	priInterface, err := x509.ParsePKCS8PrivateKey(block)
	if err != nil {
		return "", err
	}
	pri := priInterface.(*rsa.PrivateKey)
	shaNew := hash.New()
	shaNew.Write(origData)
	hashed := shaNew.Sum(nil)
	signature, err := rsa.SignPKCS1v15(rand.Reader, pri, hash, hashed)
	if err != nil {
		return "", err
	}
	return base64.StdEncoding.EncodeToString(signature), nil
}
