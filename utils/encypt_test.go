package utils

import (
	"fmt"
	"testing"
)

func TestComputeHmacSha256(t *testing.T) {
	str := HmacSha256("appId92383b63faa68c9c1749fea0d38329customerEmail999999999@funpay.comcustomerNamejohncustomerPhone999999999notifyUrlhttps://payment.rummybank.com/cashfree/order_callbackorderAmount10orderCurrencyINRorderId1611670458045727returnUrlhttp://pay.rummybank.com", "51d7b346764c9e9b654d8176aa3808a774ee5868")
	fmt.Println(str)
}

func TestHSha512(t *testing.T) {
	str, err := AesEncryptString("0t2u1zqjCrKD5tQFxV82insUA6AFGK4aBtCzyOU7I7X2hmgbZXRkB0ilmNZcl7")
	fmt.Println(str)
	fmt.Println(err)

	data := "Mub+yFxL+JBQqiYSrQpo+hz5PgL73471aMsa0yFQuX5EDdfZB4KGbWsh1nOwJJh2qmjehNjN40PE7zxpW8HAgQ=="
	sD, err := AesDecryptString(data)
	fmt.Println(sD)
	fmt.Println(err)
}

func TestName(t *testing.T) {
	for i := 0; i < 10000; i++ {
		fmt.Println(GetOrderId("OPIN"))
	}
}
