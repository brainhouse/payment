package utils

import (
	"bytes"
	"crypto"
	"crypto/aes"
	"crypto/cipher"
	cryptoRand "crypto/rand"
	"crypto/rsa"
	"crypto/sha1"
	"crypto/sha512"
	"crypto/x509"
	"encoding/base64"
	"encoding/pem"
	"errors"
	"fmt"
	"math/rand"
	"sync"
	"time"
)

/*
*
创建订单ID
*/
var lk sync.Mutex
var r = rand.New(rand.NewSource(time.Now().UnixNano()))
var inc uint32 = uint32(r.Int31n(10000))

func GetOrderId(channel string) string {
	lk.Lock()
	inc += 1
	incVal := inc & 0xffff
	//randNum := r.Int31n(100000)
	lk.Unlock()

	unix := time.Now().Unix()
	suff := fmt.Sprintf("%04x", incVal)
	encData := fmt.Sprintf("%x", XXTeaEnc([]byte(fmt.Sprintf("%x:%v", unix, suff))))

	return fmt.Sprintf("%v%v%v%v", channel, unix, encData[0:6], suff)
}

func GetHashSha512(str string) string {
	hash := sha512.New()
	hash.Write([]byte(str))
	r := hash.Sum(nil)
	return fmt.Sprintf("%x", r)
}

func wrap(start, key, end string) []byte {
	init := make([]byte, 0, len(key)+len(start)+len(end)+16)
	buf := bytes.NewBuffer(init)

	buf.WriteString(start)

	idx := 0
	next := 0
	size := len(key)
	for idx < size {
		next = idx + 64
		if next > size {
			next = size
		}

		buf.WriteString(key[idx:next])
		buf.WriteByte(byte('\n'))

		idx = next
	}

	buf.WriteString(end)
	fmt.Printf("%v", string(buf.Bytes()))
	return buf.Bytes()
}

func WrapPublicKey(key string) []byte {
	return wrap("-----BEGIN PUBLIC KEY-----\n", key, "-----END PUBLIC KEY-----\n")
}

func WrapPrivateKey(key string) []byte {
	return wrap("-----BEGIN PRIVATE KEY-----\n", key, "-----END PRIVATE KEY-----\n")
}

func GenRsaKey() (prvKey, pubKey []byte, err error) {
	// 生成私钥文件
	privateKey, err := rsa.GenerateKey(cryptoRand.Reader, 1024)
	if err != nil {
		return nil, nil, err
	}
	derStream, err := x509.MarshalPKCS8PrivateKey(privateKey)
	if err != nil {
		return nil, nil, err
	}

	block := &pem.Block{
		Type:  "RSA PRIVATE KEY",
		Bytes: derStream,
	}
	prvKey = pem.EncodeToMemory(block)
	publicKey := &privateKey.PublicKey
	derPkix, err := x509.MarshalPKIXPublicKey(publicKey)
	if err != nil {
		return nil, nil, err
	}
	block = &pem.Block{
		Type:  "PUBLIC KEY",
		Bytes: derPkix,
	}
	pubKey = pem.EncodeToMemory(block)
	return
}

func RsaSignWithSha1(data []byte, keyBytes []byte) ([]byte, error) {
	h := sha1.New()
	h.Write(data)
	hashed := h.Sum(nil)
	block, _ := pem.Decode(keyBytes)
	if block == nil {
		return nil, errors.New("private key error")
	}

	privateKey, err := x509.ParsePKCS1PrivateKey(block.Bytes)
	if err != nil {
		pri2, err := x509.ParsePKCS8PrivateKey(block.Bytes)
		if err != nil {
			return nil, err
		}

		ok := false
		privateKey, ok = pri2.(*rsa.PrivateKey)
		if !ok {
			return nil, fmt.Errorf("invalid private key")
		}
	}

	signature, err := rsa.SignPKCS1v15(cryptoRand.Reader, privateKey, crypto.SHA1, hashed)
	if err != nil {
		return nil, fmt.Errorf("sign error %v", err)
	}

	return signature, nil
}

func RsaSignWithSha1Base64(data []byte, keyBytes []byte) (string, error) {
	signature, err := RsaSignWithSha1(data, keyBytes)
	if err != nil {
		return "", err
	}

	return base64.StdEncoding.EncodeToString(signature), nil
}

func RsaVerySignWithSha1(data, signData, keyBytes []byte) (bool, error) {
	block, _ := pem.Decode(keyBytes)
	if block == nil {
		return false, errors.New("public key error")
	}
	pubKey, err := x509.ParsePKIXPublicKey(block.Bytes)
	if err != nil {
		return false, err
	}

	hashed := sha1.Sum(data)
	err = rsa.VerifyPKCS1v15(pubKey.(*rsa.PublicKey), crypto.SHA1, hashed[:], signData)
	if err != nil {
		return false, err
	}
	return true, nil
}

func RsaEncrypt(data, publicKey []byte) (enc []byte, err error) {
	//解密pem格式的公钥
	block, _ := pem.Decode(publicKey)
	if block == nil {
		return nil, errors.New("public key error")
	}
	// 解析公钥
	pubInterface, err := x509.ParsePKIXPublicKey(block.Bytes)
	if err != nil {
		return nil, err
	}
	// 类型断言
	pub := pubInterface.(*rsa.PublicKey)
	//加密
	ciphertext, err := rsa.EncryptPKCS1v15(cryptoRand.Reader, pub, data)
	if err != nil {
		return nil, err
	}
	return ciphertext, nil
}

func RsaDecrypt(ciphertext, privateKey []byte) (result []byte, err error) {
	//获取私钥
	block, _ := pem.Decode(privateKey)
	if block == nil {
		return nil, errors.New("private key error")
	}
	//解析PKCS1格式的私钥
	priv, err := x509.ParsePKCS1PrivateKey(block.Bytes)
	if err != nil {
		return nil, err
	}

	// 解密
	data, err := rsa.DecryptPKCS1v15(cryptoRand.Reader, priv, ciphertext)
	if err != nil {
		return nil, err
	}
	return data, nil
}

func AESEncrypt(data []byte, key, iv []byte) ([]byte, error) {
	b, err := aes.NewCipher(key)
	if err != nil {
		return nil, err
	}

	content := PKCS5Padding(data, b.BlockSize())
	encData := make([]byte, len(content))
	cipher.NewCBCEncrypter(b, iv).CryptBlocks(encData, content)

	return encData, nil
}

func AESEncryptWithBase64(data []byte, key, iv []byte) (string, error) {
	b, err := aes.NewCipher(key)
	if err != nil {
		return "", err
	}

	content := PKCS5Padding(data, b.BlockSize())
	encData := make([]byte, len(content))
	cipher.NewCBCEncrypter(b, iv).CryptBlocks(encData, content)

	return base64.StdEncoding.EncodeToString(encData), nil
}

func AESDecrypt(data []byte, key, iv []byte) ([]byte, error) {
	decrypted := make([]byte, len(data))
	b, err := aes.NewCipher(key)
	if err != nil {
		return nil, err
	}

	cipher.NewCBCDecrypter(b, iv).CryptBlocks(decrypted, data)
	return PKCS5Trimming(decrypted), nil
}

func PKCS5Padding(cipherText []byte, blockSize int) []byte {
	padding := blockSize - len(cipherText)%blockSize
	pendText := bytes.Repeat([]byte{byte(padding)}, padding)
	return append(cipherText, pendText...)
}

func PKCS5Trimming(encrypt []byte) []byte {
	padding := encrypt[len(encrypt)-1]
	return encrypt[:len(encrypt)-int(padding)]
}
