package utils

import (
	"bytes"
	"crypto/aes"
	"crypto/cipher"
	"encoding/base64"
	"fmt"
)

var commonIV = []byte{0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f}
var keyText = "YDtgZ*nWu$j8#XLQ1IcS^Mx8oJZB$1DR"

// 创建加密算法aes
var c, _ = aes.NewCipher([]byte(keyText))

//加密字符串
func AESEncrypte(plaintext []byte) string {
	cfb := cipher.NewCFBEncrypter(c, commonIV)
	ciphertext := make([]byte, len(plaintext))
	cfb.XORKeyStream(ciphertext, plaintext)
	return base64.RawURLEncoding.EncodeToString(ciphertext)
}

// 解密字符串
func AESDecrypte(ciphertext string) string {
	bs, err := base64.RawURLEncoding.DecodeString(ciphertext)
	if err != nil {
		return ""
	}
	cfbdec := cipher.NewCFBDecrypter(c, commonIV)
	plaintext := make([]byte, len(ciphertext))
	cfbdec.XORKeyStream(plaintext, bs)
	str := bytes.TrimSpace(plaintext)
	return fmt.Sprintf("%s", str)
}

// uid 转 token
//func Uid2Token(uid int64) string {
//	str := fmt.Sprintf("%09d", uid)
//	bs := AESEncrypte(bytes.NewBufferString(str).Bytes())
//	return base64.RawURLEncoding.EncodeToString(bs)
//}

// token 转 uid
//func Token2Uid(token string) (uid int64, err error) {
//	bs, err := base64.RawURLEncoding.DecodeString(token)
//	if err != nil {
//		return
//	}
//	uidStr := bytes.TrimSpace(AESDecrypte(bs))
//	return strconv.ParseInt(fmt.Sprintf("%s", uidStr), 10, 64)
//}
