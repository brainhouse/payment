package centerlog

import (
	"funzone_pay/centerlog"
	"testing"
)

func TestLog(t *testing.T) {
	//
	centerlog.Init("65.0.22.22", "12201", "65.0.22.22", "12201")
	for i := 0; i< 10; i++ {
		centerlog.Info("测试数据", map[string]interface{}{
			"uid": 102,
			"data": "hahaha--",
		})
		centerlog.Info("测试数据-Panic", map[string]interface{}{
			"uid": 102,
			"data": "hahaha--",
		})
		//fmt.Printf("----------------------\n")
	}
}