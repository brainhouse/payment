package main

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"funzone_pay/utils"
	"github.com/wonderivan/logger"
	"github.com/xxtea/xxtea-go/xxtea"
	"golang.org/x/net/html"
	"io/ioutil"
	"math/rand"
	"net/http"
	"strings"
	"time"
)

func request(url string, method string, contentType string, sendData interface{}, header map[string][]string) (res []byte, err error) {
	client := &http.Client{}
	var req *http.Request
	if contentType == "JSON" {
		data, _ := json.Marshal(sendData)
		reader := bytes.NewBuffer(data)
		req, err = http.NewRequest(method, url, reader)
	} else {
		s := sendData.(string)
		reader := strings.NewReader(s)
		req, err = http.NewRequest(method, url, reader)
	}
	//提交请求
	if err != nil {
		logger.Error("EaglePayService_Create_order | url=%v | err=%+v | req=%+v", url, err, sendData)
		return
	}
	req.Header = http.Header(header)
	//处理返回结果
	response, err := client.Do(req)
	if err != nil {
		logger.Error("EaglePayService_request_err | url=%v | err=%+v | req=%+v", url, err, sendData)
		return
	}
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		logger.Error("EaglePayService_response_read_err | url=%v | err=%+v | req=%+v", url, err, sendData)
		return
	}
	defer func() {
		_ = response.Body.Close()
	}()
	return body, nil
}

func get(header http.Header, url string) ([]byte, error) {
	client := &http.Client{}
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}

	req.Header = header

	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return body, err
	}

	defer func() {
		_ = resp.Body.Close()
	}()

	return body, nil
}

func parseHtml(data []byte) [][]string {
	ret := make([][]string, 0, 512)
	buff := bytes.NewBuffer(data)
	z := html.NewTokenizer(buff)
	fund := false
	for {
		tt := z.Next()
		if tt == html.ErrorToken {
			break
		}

		//fmt.Printf("z token=%s--%s--%s--%s\n", z.Token().Data, z.Token().Type, z.Token().Attr, string(z.Raw()))
		if !fund && tt == html.StartTagToken && z.Token().Data == "tbody" {
			fund = true
		}

		if fund && tt == html.StartTagToken && z.Token().Data == "tr" {
			//fmt.Printf("tbody-->tr:[%s]--[%s]--[%s]\n", z.Token().Data, z.Token().Type, z.Token().Attr)
			item := make([]string, 0, 8)
			for {
				td := z.Next()
				if td == html.EndTagToken && z.Token().Data == "tr" {
					break
				}

				// customer, customer_id, spend_limit, allocated, cost, remain, anance, status
				//td:xinmiaomedia-plz loan-02
				//td:574-078-3556
				//td:1.00
				//td:1.00
				//td:No Spend
				//td:1.00
				//td:
				//td:OPEN
				//td:

				if td == html.StartTagToken && z.Token().Data == "td" {
					if z.Next() == html.TextToken {
						txt := strings.TrimSpace((string)(z.Text()))
						//fmt.Printf("td:%s\n", txt)
						item = append(item, txt)
					}
				}
			}

			ret = append(ret, item)
		}

		if tt == html.EndTagToken && (z.Token().Data == "tbody" || z.Token().Data == "html") {
			break
		}
	}

	return ret
}

func norteDigitalTask(token string) {
	//tic := time.NewTicker(time.Minute*2)
	tic := time.NewTicker(time.Minute * 5)
	head := map[string][]string{
		"User-Agent":      []string{"Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:89.0) Gecko/20100101 Firefox/89.0"},
		"Accept":          []string{"text/html", "application/xhtml+xml", "application/xml;q=0.9", "image/webp", "*/*;q=0.8"},
		"Accept-Language": []string{"gzip", "deflate", "br"},
		"Referer":         []string{"https://opt.norte.digital/partners/home"},
		//"Cookie":[]string{"csrftoken=NA9m5bcy0lN5cRpEHafivTqWxj42tlPbyWeK84aBr5ZmopTj9TaDo3YUzsh1kjVh; sessionid=hzmscjdmw2900u8linfcmfs1yvau1m6u"},
		"Cookie": []string{token},
	}

	for tm := range tic.C {
		resp, err := get(head, "https://opt.norte.digital/partners/accounts/")
		if err != nil {
			fmt.Printf("->%v\n", err)
			break
		}

		ret := parseHtml(resp)
		fmt.Printf("time:%s\n", tm.Format("2006-01-02T15:04:05Z07:00"))
		fmt.Printf("->%d\n", len(ret))
		for _, item := range ret {
			// customer, customer_id, spend_limit, allocated, cost, remain, anance, status
			/*
			  allocated 大于spend limit的，一个cost接近spend limit的
			*/
			fmt.Printf("->%s\n", item)
		}

		fmt.Printf("\n\n\n\n")
	}
}

func testRandom() {
	//unix := time.Now().UnixNano()
	for i := 0; i < 10; i++ {
		a := rand.New(rand.NewSource(time.Now().UnixNano())).Int31n(100000)
		fmt.Printf("random --->%d\n", a)
	}

}

func TestNotify() {
	callbackData := map[string]interface{}{"data": "hello pix", "amount": 100}
	jsData, _ := json.Marshal(callbackData)
	sk, _ := utils.AesDecryptString("enc-key")
	Signature := utils.ComputeHmacSha256(string(jsData), sk)
	header := map[string][]string{
		"Content-Type": {"application/json"},
		"Signature":    {Signature},
	}

	url := "http://localhost:8080/t"
	res, status, err := utils.HttpRequest(url, "POST", "JSON", callbackData, header)
	if err != nil {
		logger.Error("PayEntryService_SendEntryPlatFromCallback_Error | err=%v | data=%+v", err, callbackData)
	}
	response := string(res)
	//状态码判断
	fmt.Printf("response --->%v-%v\n", status, response)
}

func xxTea() {
	key := "540"
	src := ""
	encData := xxtea.Encrypt([]byte(src), []byte(key))
	fmt.Printf("%v\n", base64.StdEncoding.EncodeToString(encData))
}

func main() {
	//data := map[string]interface{}{
	//	"amount": 6042.00, "app_order_id": "246881F220317060518",
	//	"phone": "919998214076", "user_name": "Ketankumar Padhiyar",
	//}
	//head := map[string][]string{
	//	"Appid":     []string{"glob_pay_test"},
	//	"Signature": []string{},
	//}
	//
	//resp, err := request("http://test.funzonepay.com/v1/platform/collect_order", "POST", "JSON", data, head)

	//head := map[string][]string {
	//	"User-Agent":[]string{"Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:89.0) Gecko/20100101 Firefox/89.0"},
	//	"Accept":[]string{"text/html","application/xhtml+xml","application/xml;q=0.9","image/webp","*/*;q=0.8"},
	//	"Accept-Language": []string{"gzip", "deflate", "br"},
	//	"Referer":[]string{"https://opt.norte.digital/partners/home"},
	//	"Cookie":[]string{"csrftoken=NA9m5bcy0lN5cRpEHafivTqWxj42tlPbyWeK84aBr5ZmopTj9TaDo3YUzsh1kjVh; sessionid=hzmscjdmw2900u8linfcmfs1yvau1m6u"},
	//}

	//csrftoken=NA9m5bcy0lN5cRpEHafivTqWxj42tlPbyWeK84aBr5ZmopTj9TaDo3YUzsh1kjVh;
	//Domain=.norte.digital; expires=Thu, 04 May 2023 03:52:34 GMT;
	//Max-Age=31449600;
	//Path=/; SameSite=Lax
	//resp, err := get(head, "https://opt.norte.digital/partners/accounts/")
	//fmt.Printf("--->%v---%v\n", string(resp), err)

	//ret := parseHtml(resp)
	//fmt.Printf("size=%d\n", len(ret))
	//token := "csrftoken=NA9m5bcy0lN5cRpEHafivTqWxj42tlPbyWeK84aBr5ZmopTj9TaDo3YUzsh1kjVh; sessionid=hzmscjdmw2900u8linfcmfs1yvau1m6u"
	//norteDigitalTask(token)

	//testRandom()
	//TestNotify()
}
