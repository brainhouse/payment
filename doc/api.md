
# FirstPay Pay接口文档 V1.1

-----

## 域名地址

 - 生产环境：https://payment.firstpayer.com/

----
## <span id="header">Header</span>

| 参数名称  | 必须参数 | 说明                       |
| :-------- | -------- | -------------------------- |
| AppId     | 是       | 应用标识，通过关联后台获取 |
| Signature | 是       | 数据签名                   |

加密方式(伪代码)：base64_encode(hmac_sha256(body))
```base64_encode(hmac_sha256('{"app_order_id":"testpayu1","amount":10,"phone":"999999999","user_name":"john",....}', SerectKey))```

SerectKey:`后台获取`

### 示例代码(伪代码)
``` php
// PHP 代码
$Signature = base64_encode(hash_hmac('sha256', $http_body, $SerectKey));

```

``` golang
// golang
key := []byte(secret)
h := hmac.New(sha256.New, key)
h.Write([]byte(httpbody))
sha := hex.EncodeToString(h.Sum(nil))
Signature = base64.StdEncoding.EncodeToString([]byte(sha))

```
`三方验证地址: https://www.idcd.com/tool/encrypt/hmac`
----

### 订单状态有以下状态
- 0: pending
- 1: 成功
- 2: 失败
- 3: 订单取消
- 4: 订单回滚


## API Reference

* ### 创建收款单 
  * ###请求 
    
    ``` URL: /v1/platform/collect_order```  
    ``` Method: POST```  
    ``` Content-Type:  application/json```
  *  ###header  
    参考[header](#header)部分

  * ### body
    ``` json
    {
      "country": "IN",            // string - 默认印度,取值:IN-印度,PK-巴基斯坦,BR-巴西,MX-墨西哥
      "app_order_id": "testpay",  // string - 调用方order_id
      "amount": 10.00,            // **number** - 金额 **最多两位小数**
      "phone": "999999999",       // string - 手机号 **巴基斯坦用户手机号需要唯一**
      "user_name": "john",        // string - 用户名称
      "ip": "10.203.45.12",       // string - 印度用户ip地址(部分银行风控需要上传真实的用户ip地址才能拉起支付)                  
    
      "user_citizen_id": "citizen-idxxxx",  // string - 用户身份id(墨西哥clabe支付需要)
      "user_device_id": "device-id-xxxxx",  // string - 设备id(墨西哥clabe支付需要)
      "city": "city name",                  // string - 城市(墨西哥clabe支付需要)
      "street": "street address",           // string - 街道(墨西哥clabe支付需要)
      "house_number": "house number"        // string - 门牌号(墨西哥clabe支付需要)
    }
    ```

	### 注意：
	- 由于巴基斯坦当地支付系统的原因，巴基斯坦的支付客户需要额外多一个拦截：**同一个用户**且**相同金额**的订单，上一单拉起成功但未汇款的情况下，40分钟内禁止再次拉起相同金额的订单(统一用户相同金额订单短时间内多次拉取可能导致新订单和老订单重复的问题)
	- 每个发起支付的用户的**手机号要唯一**
	
  * ### response
    ``` json
    {
        "code": "200",
        "data": {
            "app_order_id": "testpayu1",                                 // string - 调用方流水id
            "order_id": "ffpay-test1610031609046819",                    // string - 平台流水id
            "amount": 10,                                                // integer - 金额
            "payment_link": "http://sandbox.ffpay.com/#/?amount=10"      // string - 收银台h5地址 **如果没返回payment_link或者返回空字符串表示调用失败**
        }
    }
    ```
    ### `注意：接口如果没返回payment_link或者返回空字符串表示调用失败`
  * ### 示例
    ```bigquery
    
    ```
------

* ### 创建提现接口
  * ###请求

    ``` URL: /v1/platform/payout```  
    ``` Method: POST```  
    ``` Content-Type:  application/json```
  *  ###header
    参考[header](#header)部分

  * ### body
    ``` json
    {
      "country": "IN",                   // string - 默认印度,取值:IN,PK,BR,MX
      "ifsc": "testpay",                 // string - ifsc-只有印度银行卡提现需要. 注意：非印度银行卡该字段不要赋值
      "bank_card":"14434233123123",      // string - 提现目标银行卡id/pix账号/clabe卡号，其他提现方式可以忽略参数
      "card_type": "CPF",                // string - 巴西pix账号提现的必须参数,其他支付可以忽略不传
                                         //        - 枚举类型，五种取值：CPF/CNPJ/PHONE/EMAIL/EVP
      "bank_code":"9871",                // string - 银行编码-墨西哥clabe提现/巴基斯坦银行卡提现需要，其他国家忽略
      "vpa": "xxxx",                     // string - vpa号,印度upi提现方式需要提供，其他的忽略该参数
      "app_order_id":"test-123131asd",   // string - 调用方生成的order_id
      "amount": 10,                      // integer - 金额
      "user_name":"john",                // string - 用户名称
      "phone": "999999999",              // string - 用户手机号
      "pay_type":"bank",                 // string - 提现类型(枚举值) bank:提现到银行卡(印度、巴基斯坦地区支持) 
                                         //                        clabe:墨西哥特有的方式，入前确认
                                         //                        PIX: 巴西特有方式
                                         //                        upi: 仅印度地区
    
      "document_value":"",               // 巴西出款需要,客户CPF证件数值,11位数字
    }
    ```

  * ### response
    ``` json
    {
        "code": "200",                                                   // 400:参数错误，失败
                                                                         // 500:等待回调结果最终状态
                                                                         // 200:等待回调结果确认最终状态，如果返回status=2，则表示支付失败，不发起回调
        "data": {
            "app_order_id": "testpayu1",                                 // string - 调用方流水id，唯一标识
            "order_id": "ffpay-test1610031609046819",                    // string - 平台流水id
            "amount": 10,                                                // integer - 金额
            "status": 0                                                  // integer - 状态：0-支付中，1-成功，2-失败,3-出款撤销，订单回滚
        }
    }
    ```
-----

* ### 充值回调数据结构
  * ###说明
    ```回调配置：请在后台配置回调url地址```  
    ``` Method: POST```  
    ``` Content-Type:  application/json```
  *  ###header
    Signature=base64(hmac_sha256('回调数据', SecertKey)

  * ### body
    ``` json
    {
      "app_order_id": "testpay",        // string - 调用方order_id
      "order_id":"14434233123123",      // string - 平台流水id
      "amount": 10,                     // integer - 金额（实际用户支付金额，部分**印度**渠道可能存在实际支付金额和订单金额不等情况，找商务确认）
      "payment_order_id":"john",        // string - 墨西哥电汇支付唯一id(分多次支付的时候才会存在该字段)
      "third_desc": "999999999",        // string - 墨西哥电汇固定值multiTransferViaClabe(分多次支付的时候才会有该字段)
      "status":0                        // integer - 支付状态 0支付中 1支付成功 2支付失败 3-退款，订单回滚
    }
    ```
    ```
    说明：
    order_id：墨西哥电汇支付存在一个订单分多次支付情况，此情况下，原订单支付会失败，后面拆分成的多个支付订单，返回的order_id为新的id值，客户需要使用payment_order_id来维护支付状态  
    payment_order_id: 墨西哥电汇支付唯一id，每发起一次电汇支付请求生成一次，该id用于一个订单，分多次支付（比如：发起一个100的支付订单请求，用户可以先支付60，再支付30，再支付10，过程中此id值不变）
    third_desc：表示订单是否是墨西哥电汇分多次支付的情况，如果值为-multiTransferViaClabe，表示该通知为一个订单多个支付通知中的一个
    ```
  * ### response
    ``` 
    调用方需要返回HttpCode=200表示回调成功，否则等待一定时间后，会再次回调通知调用方。
    ```
-----

* ### 重试充值回调
  * ###说明
    ```URL: /v1/platform/retry_collect_callback?order_id=xfafi212312ada```  
    ``` Method: POST```  

  *  ###header
    参考[header](#header)部分

    ```
    注意：区别于其他请求，这个请求没有body部分，so这里被加密数据串为空串
    ```

  * ### query
    ``` 
    order_id   string  创建支付订单的时候返回的流水号 
    ```
    
  * ### response
    ``` 
    {
      "code": "200",         // 200: success
      "msg": "success"
    }
    ```
-----

* ### 提现回调数据结构
  * ###说明
    ```回调配置：请在后台配置回调url地址```  
    ``` Method: POST```  
    ``` Content-Type:  application/json```
  *  ###header
    Signature=base64(hmac_sha256('回调数据', SecertKey)

  * ### body
    ``` json
    {
      "app_order_id": "testpay",        // string - 调用方order_id
      "order_id":"14434233123123",      // string - 平台流水id
      "amount": 10,                     // integer - 金额
      "status":0                        // integer - 支付状态 0出款订单支付中 1支付成功 2支付失败 3-出款撤销，订单回滚
    }
    ```

  * ### response
    ``` 
    调用方需要返回HttpCode=200表示回调成功，否则等待一定时间后，会再次回调通知调用方。
    ```
-----

* ### 重试提现回调
  * ###说明
    ```URL: /v1/platform/retry_payout_callback?order_id=xfafi212312ada```  
    ``` Method: POST```

  *  ###header
    参考[header](#header)部分

    ```
    注意：区别于其他请求，这个请求没有body部分，so这里被加密数据串为空串
    ```

  * ### query
    ``` 
    order_id   string  创建提现订单的时候返回的流水号 
    ```

  * ### response
    ``` 
    {
      "code": "200",         // 200: success
      "msg": "success"
    }
    ```
-----

* ### 查询提现结果
  * ###说明
    ```URL: /v1/platform/inquiry_payout_status?order_id=xfafi212312ada&app_order_id=stextadaf```  
    ``` Method: GET```

  *  ###header
    参考[header](#header)部分

    ```
    注意：区别于其他请求，这个请求没有body部分，so这里被加密数据串为空串
    ```

  * ### query
    ``` 
    order_id        string  创建提现订单的时候返回的流水号
    app_order_id    string  调用方id 
    ```

    说明：order_id 和 app_order_id 至少需要一个参数
  
  * ### response
    ``` 
    {
      "code": 200,                          // 200: 成功；其他-失败
      "msg": "err info",                    // 失败信息
      "data": {                             // 成功返回的数据
        "app_order_id": "testsdf1126",      // string - 
        "order_id": "RZ1609946832058200",   //
        "amount": 100,                      
        "status": 0
      }
    }
    ```
-----

* ### 查询账户详情(_废弃_)
  * ###说明
    ```URL: /v1/```  
    ``` Method: GET```

  *  ###header
    参考[header](#header)部分

    ```
    注意：区别于其他请求，这个请求没有body部分，so这里被加密数据串为空串
    ```
  
  * ### response
    ``` 
    {
      "code": 200,                          // 200: 成功；其他-失败
      "msg": "err info",                    // 失败信息
      "data": {                             // 成功返回的数据
        "balance": 1200,                    // 账户余额
        "unsettledBalance": 331200,         // 未结算余额
                                            // 账户总额 = 余额 + 未结算的余额
      }
    }
    ```
-----
















